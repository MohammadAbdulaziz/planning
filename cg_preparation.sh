#!/bin/bash

whereishol=$1
whereisproject=$2

if [[ $whereishol == "" ]] ; then
    # Charles   #
    whereishol="/home/cgretton/Downloads/somewhere/HOL/bin/Holmake"
    
    # Mohammad  #
    #whereishol="../../../../HOL/bin/Holmake"
fi

if [[ $whereisproject == "" ]] ; then
    whereisproject=`pwd`
    whereisproject=`echo "$whereisproject/"`
fi

rootdir=`pwd`


## Dependencies, order is important
claims="SODA sspaceAcyclicity ../thesis diameterUpperBounding diamBoundingFormalisationJAR"

# EDIT THE BUILD SCRIPTS
for p in $claims ; do
    cd `echo "$rootdir/papers/"`
    if [ -a $p ] ; then
	cd $p
	buildlines=`cat build.sh | wc -l | awk  '{print $0}'`
	pwd
	
	echo "NUMBER OF LINES IS $buildlines"
	
	cat build.sh | tail -$(($buildlines - 1)) | sed s/"^projDir"/"#projDir"/g | grep -v Holmake > ~/tmp
	
	
	echo "#!/bin/bash" > build.sh
	echo "projDir=$whereisproject" >> build.sh
	cat ~/tmp >> build.sh
	echo "$whereishol --qof" >> build.sh

	if [[ -x build.sh ]] ; then
	    echo "Build script at:"
	    pwd
	    echo "Executable from GIT"
	else
	    chmod +x build.sh
	fi

	cat build.sh
	
	cd ..
    fi
done

echo "OKAY, EDIT STAGE COMPLETION ------------------------------------------------------"

cd $rootdir
for p in $claims ; do
    cd `echo "$rootdir/papers/"`
    if [ -a $p ] ; then
	cd $p
	pwd
	if [[ -x build.sh ]] ; then
	    ./build.sh
	fi
	cd ..
    fi
done
