\section{Results}
We consider the AI~planning problem defined on factored transition systems, i.e. given an initial state $I\in\uniStates(\delta)$ and a set of goal states $G$, is there an action sequence that reaches one of the goal states if it is executed at the initial state.

Our contribution is a new abstraction of factored transition systems, the result of which we call a {\em descriptive quotient}.
We provide conditions under which searching for a path between the initial state and a goal state (i.e. a plan) in the digraph modelling the state space of the descritptive quotient can be used to synthesise a path between the initial state and some goal state in the original system.
The first condition is that the partition used to obtain the descriptive quotient is induced by the symmetry relation between state variables (a.k.a. orbits of a subgroup of the automorphism group).
The second condition is that the descriptive quotient is isomorphic to a ``sub-system'' of the original factored system.
Informally, those two conditions mean that planning via descriptive quotients is a way to exploit ``repetitive symmetries'' in a factored system, where by repetitive symmetries we mean that the factored system is constituted by a union of isomorphic sub-systems.

Based on that we provide a novel procedure for domain-independent planning with symmetries.
Following, \eg, \citeauthor{pochter2011exploiting}, in a first step we infer knowledge about the symmetries between state variable.
Then departing from existing approaches, our second step uses that knowledge to obtain a quotient of the concrete factored system.
Called the descriptive quotient, this describes any element in the set of isomorphic subsystems which abstractly model the concrete system.
Third, we invoke a planner once to solve the small reachability problem posed by that descriptive quotient.
%%
In the fourth and final step, a concrete plan is synthesized by concatenating instantiations of that plan for each isomorphic subproblem.

The non-existence of a plan for the descriptive quotient does not exclude the possibility of a plan in the concrete system.
Although sound, in that respect our approach is incomplete.
Having an optimal plan for the quotient does not guarantee optimality in the concatenated plan.
%%Thus, our approach is satisficing.
%%
Aside from computing a plan for the descriptive quotient, the computationally challenging steps occur in preprocessing:
\begin{enumerate}
  \item Identification of state variable symmetries from the original description, a problem as hard as graph isomorphism, which is not known to be tractable, and 
  \item Computing an appropriate set of subsystems isomorphic to the quotient.
\end{enumerate} 
We introduce the general version of the latter problem: for an undirected graph $\undirgraph$ and a partition of its vertices $\vtxpartition$, is the quotient $\undirgraph/\vtxpartition$ isomorphic to a subgraph of $\undirgraph$, such that the morphism maps every set of vertices from $\vtxpartition$ to one of its members?
We show that this problem is NP-complete.
We also show that if $\vtxpartition$ is a set of {\em orbits} of a symmetry group for $\undirgraph$, then there is set of morphisms from $\undirgraph/\vtxpartition$ to $\undirgraph$ that covers all the vertices of $\undirgraph$, and whose size is logarithmic in the vertices of $\undirgraph$.
%% We plan with symmetries using the small  {\em descriptive quotient} of the concrete problem description.
%% A concrete plan is obtained by concatenating instantiations of the plan for the {\em descriptive quotient}. 
%% %%

Unlike existing approaches, our search for a plan does not need to reason about symmetries between concrete states and the effects of actions on those.
Plan search can be performed by an off-the-shelf SAT/CSP system, in which case {\em symmetry breaking} constraints are not required.
Alternatively, using a state-based planner we avoid repeated (approximate) solution to the intractable canonicalisation problem, a clear bottleneck of recent planning algorithms.
In this respect, our approach is similar to searching in a {\em counter abstraction}, as surveyed by~\cite{WahlD10}.
Also, viewing our approach as one that decomposes a problem into subproblems, it is related to factored planning~\cite{amir2003factored,brafman2006factored,kelareva2007factored}.


The most important advantage of searching in the digraph modelling the state space of the descriptive quotient is that it is a minor that can be very compact relative to the digraph modelling the state space of the original factored system.
For instance, a counter abstraction has a state space which can be exponentially larger than that of a descriptive quotient---\ie, the descriptive quotient will model 1 object, whereas the quotient transition system models $N$ symmetric objects.
Also, existing state-based methods plan in that relatively large {\em quotient system}, and as just mentioned, face an additional intractable problem for every encountered state.
By employing approximate canonicalisation, such methods face a state space much larger than that posed by the quotient system, which can be exponentially larger than that posed by a descriptive quotient.
To give an idea of how compact can the size of the state space of the descriptive quotient, the descriptive quotient of the aforementioned 42-package \problem{gripper} instance is solved by breadth-first search expanding $6$ states, and a concrete plan is obtained in under a second. 
On the other hand, the approach by \cite{pochter2011exploiting} expands $60.5K$ states and takes about 28 seconds.
