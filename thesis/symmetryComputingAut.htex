\begin{figure}
\begin{center}
\begin{tikzpicture}
\node (3) at (0,0) [varnode] {\scriptsize $\vara\varb\overline{\varc}\vard\vare$} ;
\node (7) at (2,0) [varnode] {\scriptsize $\vara\varb \varc\vard\vare$} ;
\node (1) at (4,1) [varnodegreen] {\scriptsize $\vara\varb\overline{\varc\vard}\vare$} ;
\node (2) at (4,-1) [varnodered] {\scriptsize $\vara\varb\overline{\varc}\vard\overline{\vare}$} ;
\node (5) at (6,1) [varnodegreen] {\scriptsize $\vara\varb \varc\overline{\vard}\vare$} ;
\node (6) at (6,-1) [varnodered] {\scriptsize $\vara\varb \varc\vard\overline{\vare}$} ;
\node (0) at (8,0) [varnode] {\scriptsize $\vara\varb\overline{\varc\vard\vare}$} ;
\node (4) at (10,0) [varnode] {\scriptsize $\vara\varb \varc\overline{\vard\vare}$} ;

\draw [->,ourarrow] (3) -- (7) ;
\draw [->,ourarrow] (7) -- (1) ;
\draw [->,ourarrow] (7) -- (2) ;
\draw [->,ourarrow] (1) -- (5) ;
\draw [->,ourarrow] (2) -- (6) ;
\draw [->,ourarrow] (5) -- (0) ;
\draw [->,ourarrow] (6) -- (0) ;
\draw [<->,ourarrow] (0) -- (4) ;
\end{tikzpicture}
\end{center}
\caption[]{\label{fig:prob1SS} The largest connected component of the state space of the transition system underlying the problem from Example~\ref{eg:probaut}. It shows the presence of symmetries between different states.}
\end{figure}
\begin{figure}
\begin{center}
\begin{tikzpicture}
\node (3) at (0,0) [varnode] {\scriptsize $O_3$} ;
\node (7) at (2,0) [varnode] {\scriptsize $O_7$} ;
\node (1) at (4,0) [varnodegreen] {\scriptsize $O_1$} ;
\node (5) at (6,0) [varnodegreen] {\scriptsize $O_5$} ;
\node (0) at (8,0) [varnode] {\scriptsize $O_0$} ;
\node (4) at (10,0) [varnode] {\scriptsize $O_4$} ;
\draw [->,ourarrow] (3) -- (7) ;
\draw [->,ourarrow] (7) -- (1) ;
\draw [->,ourarrow] (1) -- (5) ;
\draw [->,ourarrow] (5) -- (0) ;
\draw [<->,ourarrow] (0) -- (4) ;
\end{tikzpicture}
\end{center}
\caption[]{\label{fig:prob1SSQuot} The quotient state space of the system from Example~\ref{eg:probaut} in which the traditional orbit search algorithm, ideally, would search for a path.}
\end{figure}
\begin{figure}
\begin{center}
\begin{tikzpicture}
\node (1) at (0,0) [varnode] {\scriptsize $p_1\overline{p_2}p_3$} ;
\node (3) at (2,0) [varnode] {\scriptsize $p_1p_2p_3$} ;
\node (0) at (4,0) [varnode] {\scriptsize $p_1\overline{p_2p_3}$} ;
\node (2) at (6,0) [varnode] {\scriptsize $p_1p_2\overline{p_3}$} ;
\draw [->,ourarrow] (1) -- (3) ;
\draw [->,ourarrow] (3) -- (0) ;
\draw [<->,ourarrow] (0) -- (2) ;
\end{tikzpicture}
\end{center}
\caption[]{\label{fig:prob1QuotSS} The largest connected component in the state space of the transition system underlying the descriptive quotient of the problem in Example~\ref{eg:probaut}.}
\end{figure}

\section{Computing Problem Symmetries}
\label{sec:computingSymmetry}
To exploit problem symmetries we must first discover them.
We follow the discovery approach from~\cite{pochter2011exploiting}, restricting ourselves to Boolean-valued variables in $\dom(\planningproblem)$.
We assume familiarity with  \emph{groups, subgroups, group actions}, and \emph{graph automorphism groups}.
Symmetries in a problem description are defined by an \emph{automorphism group}.
\begin{mydef}[Problem Automorphism Group]
\label{def:Aut}
The automorphism group of $\planningproblem$ is: $Aut(\planningproblem) = \{\perm\mid\Img{\perm}{\planningproblem} = \planningproblem\}$.
Members of $\Aut(\planningproblem)$ are permutations on $\dom(\planningproblem)$ and they induces a partition of $\dom(\planningproblem)$ to which we refer as orbits.
%%
%Problem automorphisms correspond to automorphisms on the equivalent graphs.
%An automorphism group ${\cal G}$ partitions a graph/problem vertices/variables into equivalence classes corresponding to the orbits induced by the action of ${\cal G}$ over the vertices/variables.
%We write $O_{\cal G}$ for the partition corresponding to the set of orbits, and $H\leq {\cal G}$ for a subgroup $H$ of ${\cal G}$.
\end{mydef}
\begin{myeg}
\label{eg:probaut}
Consider the planning problem $\planningproblem_1$ with 
\begin{eqnarray*}
\planningproblem_1.I &=& \{\varc, \vara, \varb, \vard, \vare\}\\
\planningproblem_1.\delta &=& \{(\{\varc, \vara\},\{\negate{\vard}, \negate{\varc} \}), (\{\varc, \varb\},\{\negate{\vare}, \negate{\varc}\}), (\emptyset,\{\varc\})\}\\
\planningproblem_1.G &=& \{\negate{\vard}, \negate{\vare}\}
\end{eqnarray*}
The largest connected component of $\graph(\delta_1)$ is shown in Figure~\ref{fig:prob1SS}.
$\Aut(\planningproblem_1)$ is the closure of this set of permutations under composition: $\{\{\vara\mapsto \varb, \varb\mapsto \vara\}, \{\vard\mapsto \vare, \vare\mapsto \vard\}\}$.
Let $\partition$ be $\{p_1 = \{\vara, \varb\}, p_2 = \{\varc\}, p_3 = \{\vard, \vare\}\}$.
This partition of $\dom(\planningproblem_1)$ is the set of orbits induced by $\Aut(\planningproblem_1)$.
This variable symmetry induces symmetries between the states and accordingly vertices of $\graph(\delta_1)$ as shown in Figure~\ref{fig:prob1SS}, where state 1 is symmetric with 2, and 5 is symmetric with 6.
The quotient system in which, ideally, traditional approaches would search for a path to exploit symmetry is shown in Figure~\ref{fig:prob1SSQuot}, and it is clearly smaller in size than $\graph(\delta_1)$.
\end{myeg}
A graphical representation of $\planningproblem$ is constructed so vertex symmetries in it correspond to variable symmetries in $\planningproblem$.
We follow the graphical representation introduced in \cite{pochter2011exploiting}.

\begin{mydef}[Undirected Graph]
\label{def:graph}
An undirected graph $\undirgraph$ is a tuple $\langle \indices,\edges \rangle$, where $\indices$ is the set of vertices of $\undirgraph$ and $\edges$ is the set edges of $\undirgraph$ which is a set of unordered pairs from $\indices$.
We write $\indices(\graph)$ for the set of vertices, and $\edges(\graph)$ for edges of a graph $\graph$.
\end{mydef}
\begin{mydef}[Problem Description Graph (PDG)]
\label{def:Graphs}
The (undirected) graph $\graph(\planningproblem)$ for a planning problem $\planningproblem$, is defined as follows:
\begin{enumerate}
\item $\graph(\planningproblem)$ has two vertices, $\vertexa^\top$ and $\vertexa^\bot$, for every variable $\v\in \dom(\planningproblem)$; two vertices, $a_p$ and $a_e$, for every action $(p,e) \in \delta$; and vertex $\vertexa_I$ for $I$ and $\vertexa_G$ for $G$.
\item $\graph(\planningproblem)$ has an edge
 between $\vertexa^\top$ and $\vertexa^\bot$ for all $\v \in \dom(\planningproblem)$;
 between $a_p$ and $a_e$ for all $(a,e) \in \delta$;
 between $a_p$ and $v^*$ if $(\v \mapsto *) \in p$, and 
 between $a_e$ and $v^*$ if $(\v \mapsto *)\in e$;
 between $v^*$ and $v_I$ if $(\v \mapsto *)$ occurs in $I$; and
 between $v^*$ and $v_G$ if $(\v \mapsto *)$ occurs in $G$.
\end{enumerate}
\end{mydef}
\noindent 
The automorphism group of the PDG, $\Aut(\graph(\planningproblem))$, is identified by solving an undirected graph isomorphism problem.
The action of a subgroup of $\Aut(\graph(\planningproblem))$ on $V(\graph(\planningproblem))$ induces a {\em partition}, called the \emph{orbits}, of $V(\graph(\planningproblem))$.
%The set of orbits corresponds to a {\em partition} of the PDG vertices.
We can now define our {\em quotient} structures based on partitions $\partition$ of $\dom(\planningproblem)$.
%%In this work members of ${\cal P}$ shall be sets of symmetrically equivalent variables, called {\em equivalence classes}.

\begin{mydef}[Quotient Undirected Graph]
\label{def:quotientUndirGraph}
For graph $\graph$ and a partition $\vtxpartition$ of its vertices, the quotient $\graph/\vtxpartition$ is the digraph:
 \begin{enumerate}
  \item $\indices(\graph/\vtxpartition) = \vtxpartition$, and
  \item $\edges(\graph/\vtxpartition) = \{\{U,W\} \mid U,W \in P \wedge \exists \vertexb \in U, \vertexc \in W. \{\vertexb,\vertexc\} \in \edges(\graph)\}$.
\end{enumerate}
\end{mydef}

\begin{mydef}[Quotient Problem]
\label{def:quotientProb}
Given partition $\partition$ of $\dom(\planningproblem)$, let ${\cal Q}$ map members of $\dom(\planningproblem)$ to their equivalence class in $\partition$.
The descriptive quotient is $\planningproblem/\partition=\Img{{\cal Q}}{\planningproblem}$, the image of $\planningproblem$ under ${\cal Q}$.
This is well-defined if $\partition$ is a set of orbits.
We assume that quotient problems are well-defined.
\end{mydef}

\begin{myeg}
\label{eg:probquotient}
Consider the planning problem $\planningproblem_1$ from Example~\ref{eg:probaut}, and the partition $\partition$.
The descriptive quotient associated with $\partition$ is as follows.
\begin{eqnarray*}
(\planningproblem_1/\partition).I &=& \{p_1, p_2, p_3\}\\
(\planningproblem_1/\partition).\delta &=& \{ (\{p_1, p_2\},\{\negate{p_2}, \negate{p_3}\}), (\emptyset,\{p_2 \}) \}\\
(\planningproblem_1/\partition).G &=& \{\negate{p_3}\}
\end{eqnarray*}
The largest component of the minor $\graph((\planningproblem_1/\partition).\delta)$ is shown in Figure~\ref{fig:prob1QuotSS}.
It is clearly smaller than the orginal state space shown in Figure~\ref{fig:prob1SS} as well as the quotient system shown in Figure~\ref{fig:prob1SSQuot}.
\end{myeg}

To ensure correspondence between PDG symmetries and problem symmetries, we must ensure incompatible descriptive elements do not coalesce in the same orbit.
For example, we cannot have action precondition vertices and state-variables in the same orbit.

%% With problem graphs containing vertices corresponding to actions, as well as the original problem's $I$ and $G$, it is not obvious that a symmetry on a problem graph will necessarily give rise to a partition over just a problem's variables.
%% To ensure this, we require that \nauty{} only produce vertex-partitions that are \emph{well-formed}:

\begin{mydef}[Well-formed Partitions]
A partition of  $V(\graph(\planningproblem))$ is \emph{well-formed} iff: \begin{enumerate}\item Positive ($\vertexa^\top$) and negative ($\vertexa^\bot$) variable assignment vertices only coalesce with ones of the same parity; \item Precondition ($a_p$) and effect ($a_e$) vertices only coalesce with preconditions and effects respectively, and \item Both $\vertexa_I$ and $\vertexa_G$  are always in a singleton.\end{enumerate}
A well-formed partition $\hat{\vtxpartition}$ defines a corresponding partition $\partition$ of $\dom(\planningproblem)$, so that $\graph(\planningproblem)/\hat{\vtxpartition} = \graph(\planningproblem/\partition)$
\end{mydef}

To ensure well-formedness, vertex symmetry is calculated using the {\em coloured} graph-isomorphism procedure (\nauty).
Vertices of distinct colour cannot coalesce in the same orbit.
Vertices of $\graph(\planningproblem)$ are coloured to ensure the orbits correspond to a well-formed partition.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "ijcai2015_submission"
%%% End:
