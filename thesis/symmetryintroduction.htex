%\ 1 What is Reachability
Testing whether one vertex (or a set of vertices) in a digraph can be reached from another vertex is one of the oldest and most well studied problems in computer science and graph theory.
%\ \  -important in AI planning and model checking
In AI~planning and model checking, this problem is of immense importance, where the digraph is taken to model the state space of the problem, and the question is whether a goal state can be reached from a given initial state.
In AI~planning, the problem of reachability between states is the main problem whose solution is sought.
An initial state and a set of goal states are given, and the quest is to find a valid action sequence (a \emph{plan}) that if executed in the initial state will result in one of the goal states.
Similarly, in model~checking, the problem of reachability between states corresponds to the problem of checking safety properties of systems.
An initial state of the system is given, along with a formula characterising desired safety properties, and a sequence of transitions (a bug trace) from the initial state to a state violating the given formula is searched for.
 
%\ \  -complexity in digraphs vs AI planning and model checking


Although many polynomial time algorithms (e.g. Bellman-Ford algorithm) were introduced to solve the reachability problem for digraphs, they all assume that the digraph is explicit.
Accordingly, for propositionally factored transition systems, if one uses any of those algorithms naively, he needs to construct the digraph modelling the state space to solve the reachability problem.
Thus, the complexity of reachability in factored transition systems is PSPACE-complete \cite{sistla1985complexity,bylander:94}, since the digraph modelling the state space can be exponentially larger than the factored representation.
The compositional approach alleviates can alleviate the practical complexity of computing reachability, making the problem feasible to solve in many cases, where it otherwise may not be.
In this approach, digraphs modelling state spaces of abstractions (which are minors of the original state space) are searched for paths, which are then used to synthesise a path in the digraph modelling the state space of the concrete system, a path that connects the required states.
In this chapter we consider applying that approach to compute reachability, where we consider minors of the state space corresponding to the state space of an abstraction of the factored system that we refer to as a ``descriptive quotient''.
In particular, we exploit a certain type of symmetry, which we call \emph{repetitive symmetries}, in the factored system to obtain those abstractions.

\paragraph{State Variable Symmetry}
Informally, in a factored system $\delta$, two state variables $\v_1$ and $\v_2$ are symmetric if they can be swapped by a permutation of the state variables of $\delta$, and the resulting system is the same (up to isomorphism) as $\delta$.
This state variable symmetry relation induces a partition of the set of state variables and a partition of the actions.
We find those partitions most useful for use with quotienting within our work.

\input{symmetrylitrev}
\input{symmetryResultsSummary}





%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ijcai2015_submission"
%%% End: 
