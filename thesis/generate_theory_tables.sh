#!/bin/bash
cd ../hol_sources > /dev/null
echo "\begin{table}"
echo "\begin{tabularx}{1.0\textwidth}{| l | l | X |}"
echo "\hline"
echo "Theory & Size (LOC) & Description \\\\"
echo "\hline"
for file in `ls *Script.sml | grep -v pred_set | grep -v numpair | grep -v basicSize | grep -v operator | grep -v ind_type | grep -v while | grep -v numeral | grep -v arithmetic | grep -v pair | grep -v option | grep -v prim_rec | grep -v num | grep -v sum | grep -v relation | grep -v combin | grep -v sat | grep -v normalForms | grep -v marker | grep -v bool | grep -v min | grep -v list | grep -v rich_list | grep -v sorting | grep -v finite_map | grep -v one | grep -v tightness | grep -v partitioning | grep -v CNF | grep -v utils | grep -v ConseqConv | grep -v quantHeuristics | grep -v patternMatches | grep -v indexedLists | grep -v SCCMainsystemAbstraction | grep -v invariantsScript | grep -v parentTwoChildren`; do
  nlines=`wc -l $file | gawk '{print $1}'`
  touch ${file%Script.sml}Description.htex
  echo "${file%Script.sml} & $nlines & \input{${file%Script.sml}Description} \\\\"
done
echo "\hline"
echo "\end{tabularx}"
echo "\caption{\label{table:theories} A table showing the sizes of different theories and their content.}"
echo "\end{table}"
cd - > /dev/null
