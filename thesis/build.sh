#!/bin/bash

chmod +x ./constructTheoryGraph.sh
dot -Tpng -Gdpi=700 ../hol_sources/theorygraph/minTheoryGraph.dot -o theoryGraph.png
chmod +x ./generate_theory_tables.sh
./generate_theory_tables.sh > theoriesTable.tex
