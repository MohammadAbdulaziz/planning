#!/bin/bash

cd ../hol_sources >/dev/null
$HOLDir/bin/hol < DOT
cat ../hol_sources/theorygraph/theories.dot | grep -vw "pred_set" | grep -vw "numpair" | grep -vw "basicSize" | grep -vw "operator" | grep -vw "ind_type" | grep -vw "while" | grep -vw "numeral" | grep -vw "arithmetic" | grep -vw "pair" | grep -vw "option" | grep -vw "prim_rec" | grep -vw "num" | grep -vw "sum" | grep -vw "relation" | grep -vw "combin" | grep -vw "sat" | grep -vw "normalForms" | grep -vw "marker" | grep -vw "bool" | grep -vw "min" | grep -vw "list" | grep -vw "rich_list" | grep -vw "sorting" | grep -vw "finite_map" | grep -vw "one" | grep -vw "tightness" | grep -vw "partitioning" | grep -vw "CNF" | grep -v "utils" | grep -v "ConseqConv" | grep -v "quantHeuristics" | grep -v "patternMatches" | grep -v "indexedLists" | grep -v SCCMainsystemAbstraction | grep -vw invariants | grep -v sublist
cd - > /dev/null
