open HolKernel Parse boolLib bossLib;
open planningProblemTheory
open factoredSystemTheory
open finite_mapTheory
open pred_setTheory
open systemAbstractionTheory
open fmap_utilsTheory
open listTheory
open set_utilsTheory

val _ = new_theory "instantiation";

val sustainable_vars_def = Define`sustainable_vars planningPROB vs = (DRESTRICT planningPROB.I vs = DRESTRICT planningPROB.G vs)`

val sustainable_init_goal = store_thm("sustainable_init_goal",
``sustainable_vars planningPROB vs
  ==> (FDOM planningPROB.I INTER vs = FDOM planningPROB.G INTER vs)``,
SRW_TAC[][sustainable_vars_def]
THEN METIS_TAC[FDOM_DRESTRICT])

val sustain_imp_precede = store_thm("sustain_imp_precede",
``planning_problem planningPROB1 /\ planning_problem planningPROB2 /\
  agree planningPROB1.I planningPROB2.I /\ 
 (DRESTRICT planningPROB1.G (planning_prob_dom planningPROB2) =
  DRESTRICT planningPROB2.G (planning_prob_dom planningPROB1)) /\
  sustainable_vars planningPROB1 (needed_vars planningPROB2)
    ==> precede planningPROB1 planningPROB2``,
SRW_TAC[][precede_def, sustainable_vars_def]
THEN1(SRW_TAC[][fmap_EXT]
      THEN1(SRW_TAC[][FDOM_DRESTRICT, planning_problem_dom, needed_asses_def]
            THEN METIS_TAC[SUBSET_INTER_ABSORPTION, INTER_COMM, needed_vars_subset, FDOM_DRESTRICT])
      THEN1(`DRESTRICT planningPROB1.I (needed_vars planningPROB2) ' x=
                      DRESTRICT planningPROB1.G (needed_vars planningPROB2) ' x` by
                (`x IN FDOM planningPROB1.G` by METIS_TAC[ FDOM_DRESTRICT, IN_INTER]
                 THEN `x IN needed_vars planningPROB2` by METIS_TAC[ FDOM_DRESTRICT, IN_INTER]
                 THEN `x IN FDOM planningPROB1.I` by METIS_TAC[ FDOM_DRESTRICT, IN_INTER]
                 THEN METIS_TAC[DRESTRICT_DEF, INTER_DEF])
            THEN `DRESTRICT (needed_asses planningPROB2)
                         (planning_prob_dom planningPROB1) ' x =
                     planningPROB2.I ' x` by
                    (`x IN needed_vars planningPROB2` by METIS_TAC[ FDOM_DRESTRICT, IN_INTER]
                     THEN SRW_TAC[][planning_problem_dom, needed_asses_def]
                     THEN `x IN FDOM planningPROB1.I` by METIS_TAC[ FDOM_DRESTRICT, IN_INTER]
                     THEN `x IN (needed_vars planningPROB2 ∩ FDOM planningPROB1.I)` by  METIS_TAC[INTER_DEF, IN_INTER]
                     THEN `x IN FDOM planningPROB2.I` by METIS_TAC[needed_vars_subset, SUBSET_DEF]
                     THEN METIS_TAC[DRESTRICT_DEF, IN_INTER])
            THEN `DRESTRICT planningPROB1.I (needed_vars planningPROB2) ' x =
                        planningPROB2.I ' x` by
                  (`x IN needed_vars planningPROB2` by METIS_TAC[ FDOM_DRESTRICT, IN_INTER]
                   THEN `x IN FDOM planningPROB1.I` by METIS_TAC[ FDOM_DRESTRICT, IN_INTER]
                   THEN `x IN FDOM planningPROB2.I` by METIS_TAC[needed_vars_subset, SUBSET_DEF]
                   THEN METIS_TAC[DRESTRICT_DEF, IN_INTER, agree_def])
            THEN METIS_TAC[]))
THEN METIS_TAC[])

val sustainable_prob_dom_inter = store_thm("sustainable_prob_dom_inter",
``planning_problem planningPROB1 ==> (sustainable_vars planningPROB1 vs = sustainable_vars planningPROB1 (planning_prob_dom planningPROB1 INTER vs))``,
SRW_TAC[][]
THEN EQ_TAC
THEN SRW_TAC[][sustainable_vars_def]
THEN METIS_TAC[planning_problem_dom, SUBSET_REFL, DRESTRICT_DRESTRICT,
                exec_drest_5, planning_problem_def])

val sustainable_subset = store_thm("sustainable_subset",
``vs1 SUBSET vs2 /\ sustainable_vars planningPROB1 vs2 ==> sustainable_vars planningPROB1 vs1``,
SRW_TAC[][sustainable_vars_def]
THEN METIS_TAC[DRESTRICT_SUBSET])

val fun_inv_img = store_thm("fun_inv_img",
``!g vs.
  BIJ g {x | g x IN vs} vs ==>
  ({x | g x IN vs} = IMAGE (LINV g {x | g x IN vs}) vs)``,
SRW_TAC[][EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1(Q.EXISTS_TAC `g x`
      THEN SRW_TAC[][]
      THEN MATCH_MP_TAC (SIMP_RULE(srw_ss())[AND_IMP_INTRO, PULL_FORALL] (GSYM LINV_DEF))
      THEN Q.EXISTS_TAC `vs`
      THEN SRW_TAC[][] THEN METIS_TAC[BIJ_DEF])
THEN1(`g (LINV g {x | g x IN vs} x') = x'` by
          (MATCH_MP_TAC (SIMP_RULE(srw_ss())[AND_IMP_INTRO, PULL_FORALL] (BIJ_LINV_INV))
          THEN METIS_TAC[])
      THEN SRW_TAC[][]))

val fun_inv_img_2 = store_thm("fun_inv_img_2",
``!g vs.
  INJ g vs (IMAGE g vs) ==>
  (vs = IMAGE (LINV g vs) (IMAGE g vs))``,
SRW_TAC[][EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1(Q.EXISTS_TAC `g x`
      THEN SRW_TAC[][]
      THEN1(MATCH_MP_TAC (SIMP_RULE(srw_ss())[AND_IMP_INTRO, PULL_FORALL] (GSYM LINV_DEF))
            THEN Q.EXISTS_TAC `IMAGE g vs`
            THEN SRW_TAC[][] THEN METIS_TAC[BIJ_DEF])
      THEN1 METIS_TAC[])
THEN1(`(LINV g vs (g x'')) = x''` by
           (MATCH_MP_TAC (SIMP_RULE(srw_ss())[AND_IMP_INTRO, PULL_FORALL] (LINV_DEF))
            THEN METIS_TAC[])
       THEN SRW_TAC[][]))

val fun_inv_img_3 = store_thm("fun_inv_img_3",
``!g vs.
  INJ g vs (IMAGE g vs) ==>
  (IMAGE g vs SUBSET {x | (LINV g vs x) IN vs})``,
SRW_TAC[][SUBSET_DEF]
THEN `(LINV g vs (g x')) = x'` by
           (MATCH_MP_TAC (SIMP_RULE(srw_ss())[AND_IMP_INTRO, PULL_FORALL] (LINV_DEF))
            THEN METIS_TAC[BIJ_DEF])
THEN SRW_TAC[][])


val bij_imp_finite_fdom_bck_img = Q.store_thm
("bij_imp_finite_fdom_bck_img",
 `!(g:'a -> 'b).
      (!x y. (g x = g y) = (x = y))
        ==>
      !f:'b|->'c. FINITE { x | g x IN  FDOM f}`,
 GEN_TAC THEN STRIP_TAC THEN
 INDUCT_THEN fmap_INDUCT ASSUME_TAC THENL [
   SRW_TAC [][FDOM_FEMPTY, GSPEC_F],
   SRW_TAC [][FDOM_FUPDATE, GSPEC_OR] THEN
   Cases_on `?y. g y = x` THENL [
     POP_ASSUM (STRIP_THM_THEN SUBST_ALL_TAC o GSYM) THEN
     SRW_TAC [][GSPEC_EQ],
     POP_ASSUM MP_TAC THEN SRW_TAC [][GSPEC_F]
   ]
 ]);

val bij_bck_img = store_thm("bij_bck_img",
 ``!(f:'a -> 'b).
      ((!x y. f x IN vs ==> ((f x = f y) = (x = y))) /\
      (!y. y IN vs ==> ?x. f x = y))
        <=>
      BIJ f { x | f x IN vs} vs``,
 SRW_TAC[][BIJ_DEF, INJ_DEF, SURJ_DEF]
 THEN METIS_TAC[])

val common_vars_def = Define`common_vars fSet s = { v | ?f1 f2. f1 IN fSet /\ f2 IN fSet /\ f1 <> f2 /\ v IN s /\ (f1 v = f2 v) }`

val common_vars_subset = store_thm("common_vars_subset",
``fSet1 SUBSET fSet2 ==> 
   common_vars fSet1 s SUBSET common_vars fSet2 s``,
SRW_TAC[][common_vars_def, SUBSET_DEF]
THEN METIS_TAC[])

val state_image_def = Define`state_image f s = FUN_FMAP (\x. s ' ((LINV f (FDOM s) x))) (IMAGE f (FDOM s))`;

val state_image_def = Define`state_image f s = DRESTRICT (s f_o (LINV f (FDOM s))) (IMAGE f (FDOM s))`;

val valid_inst_def = Define`valid_inst f vs = FINITE {x | LINV f vs x IN vs} /\ INJ f vs (IMAGE f vs)`

val state_image_fdom = store_thm("state_image_fdom",
``valid_inst f (FDOM s) ==>
  (FDOM (state_image f s) = (IMAGE f (FDOM s)))``,
SRW_TAC[][state_image_def, FDOM_DRESTRICT, valid_inst_def]
THEN METIS_TAC[fun_inv_img_3, INTER_COMM, BIJ_FINITE,
               SUBSET_INTER_ABSORPTION, FDOM_f_o,
               FDOM_FINITE])

val valid_inst_def =
    Define`valid_inst f = BIJ f UNIV UNIV`

val fun_INV_def = Define`fun_INV f x = CHOICE {y | f y = x}`

val state_image_def = Define`state_image f s = s f_o (fun_INV f)`;

val BIJ_back_IMG = store_thm("BIJ_back_IMG",
``BIJ f UNIV UNIV ==> ?z. {y | f y = x} = {z}``,
SRW_TAC[][BIJ_DEF,INJ_DEF,SURJ_DEF,EXTENSION]
THEN METIS_TAC[])

val BIJ_INV_DET = store_thm("BIJ_INV_DET",
``BIJ f UNIV UNIV ==> ((fun_INV f x = y) <=> (f y = x))``,
SRW_TAC[][BIJ_DEF,INJ_DEF,SURJ_DEF,EXTENSION, fun_INV_def]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1(`{y | f y = x} <> EMPTY` by
       SRW_TAC[][EXTENSION]
     THEN `CHOICE {y | f y = x} IN {y | f y = x}` by METIS_TAC[CHOICE_DEF]
     THEN FULL_SIMP_TAC(srw_ss())[IN_DEF])
THEN1(`{y' | f y' = f y} <> EMPTY` by
        SRW_TAC[][EXTENSION]
      THEN `CHOICE {y' | f y' = f y} IN {y' | f y' = f y}` by METIS_TAC[CHOICE_DEF]
      THEN FULL_SIMP_TAC(srw_ss())[IN_DEF]))

val fun_INV_works = store_thm("fun_INV_works",
``BIJ f UNIV UNIV ==> (f (fun_INV f x) = x)``,
METIS_TAC[BIJ_INV_DET])

val fun_INV_works_2 = store_thm("fun_INV_works_2",
``BIJ f UNIV UNIV ==> (fun_INV f (f x) = x)``,
METIS_TAC[BIJ_INV_DET])

val fun_INV_INV = store_thm("fun_INV_INV",
``BIJ f UNIV UNIV ==> (fun_INV (fun_INV f) x = f x)``,
SRW_TAC[][Once fun_INV_def]
THEN SRW_TAC[][BIJ_INV_DET])

val valid_inst_finite_fdom_bak_img = store_thm("valid_inst_finite_fdom_bak_img",
``valid_inst f ==> 
  FINITE {x | f x IN FDOM fm}``,
SRW_TAC[][valid_inst_def]
THEN `BIJ f {x | f x IN FDOM fm} (FDOM fm)` by
      (FULL_SIMP_TAC(srw_ss())[BIJ_DEF, INJ_DEF, SURJ_DEF]
       THEN METIS_TAC[])
THEN METIS_TAC[FDOM_FINITE, BIJ_FINITE, BIJ_LINV_BIJ])

val valid_inst_finite_fdom_bak_img = store_thm("valid_inst_finite_fdom_bak_img",
``valid_inst f ==> 
  FINITE {x | fun_INV f x IN FDOM fm}``,
SRW_TAC[][valid_inst_def]
THEN `BIJ (fun_INV f) {x | fun_INV f x IN FDOM fm} (FDOM fm)` by
      (SRW_TAC[][BIJ_DEF, INJ_DEF, SURJ_DEF]
       THEN1 METIS_TAC[fun_INV_works]
       THEN1 METIS_TAC[fun_INV_works_2])
THEN METIS_TAC[FDOM_FINITE, BIJ_FINITE, BIJ_LINV_BIJ])

val fun_INV_back_img = store_thm("fun_INV_back_img",
``valid_inst f ==> 
  ({x | fun_INV f x IN vs} = IMAGE f vs)``,
SRW_TAC[][valid_inst_def, EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1 METIS_TAC[fun_INV_works]
THEN1 METIS_TAC[fun_INV_works_2])

val state_image_fdom = store_thm("state_image_fdom",
``valid_inst f ==>
  (FDOM (state_image f s) = (IMAGE f (FDOM s)))``,
SRW_TAC[][state_image_def, valid_inst_def]
THEN METIS_TAC[FDOM_f_o, valid_inst_finite_fdom_bak_img, fun_INV_back_img, valid_inst_def])

val well_defined_quot_fun_def = Define`well_defined_quot_fun f s = (!x y. (f x = (f y)) ==> (s ' x = s ' y))`;

val action_image_def = Define`action_image f (p,e) = (state_image f p, state_image f e)`;

val system_image_def = Define`system_image f PROB = IMAGE (action_image f) PROB`;

val system_image_2 = store_thm("system_image_2",
``system_image f PROB = IMAGE (\a. action_image f a) PROB``,
METIS_TAC[system_image_def])

val planning_prob_image_def = Define`planning_prob_image f planningPROB = planningPROB with <|I:= (state_image f planningPROB.I); PROB:= (system_image f planningPROB.PROB); G:=(state_image f planningPROB.G)|>`;

val as_image_def = Define`as_image f as = MAP (action_image f) as `;

val as_image_2 = store_thm("as_image_2",
``as_image f as = MAP (\a. action_image f a) as ``,
METIS_TAC[as_image_def])

val valid_instantiations_def = Define`valid_instantiations fSet s = !f1 f2 x y. f1 IN fSet /\ f2 IN fSet /\ x IN s /\ y IN s /\ x <> y ==> (f1 x <> f2 y)`

val action_image_pair = store_thm("action_image_pair",
``action_image f a = (state_image f (FST a),state_image f (SND a))``,
METIS_TAC[pairTheory.PAIR, action_image_def])

val f_o_ASSOC = Q.store_thm(
  "f_o_ASSOC",
  `(!x y. (g x = g y) <=> (x = y)) /\ (!x y. (h x = h y) <=> (x = y)) ==>
   ((f f_o g) f_o h = f f_o (g o h))`,
  SRW_TAC[][FDOM_f_o, bij_imp_finite_fdom_bck_img, FAPPLY_f_o, fmap_EXT])

val f_o_ID = store_thm("f_o_ID",
``(!x. g x = x) ==>
    (f f_o g = f)``,
SRW_TAC[][FDOM_f_o, bij_imp_finite_fdom_bck_img, FAPPLY_f_o, fmap_EXT])

val state_image_INV = store_thm("state_image_INV",
``valid_inst f ==> 
  (state_image (fun_INV f) (state_image f (s:'a |-> 'b)) = s)``,
SRW_TAC[][valid_inst_def, state_image_def]
THEN `(s f_o (fun_INV f)) f_o (fun_INV (fun_INV f)) = s f_o fun_INV f o (fun_INV (fun_INV f))` by 
  (MATCH_MP_TAC f_o_ASSOC
   THEN SRW_TAC[][]
   THEN1 METIS_TAC[fun_INV_works]
   THEN1(SRW_TAC[][fun_INV_INV]
         THEN FULL_SIMP_TAC(srw_ss())[BIJ_DEF, INJ_DEF, SURJ_DEF]
         THEN METIS_TAC[]))
THEN SRW_TAC[][]
THEN MATCH_MP_TAC f_o_ID
THEN SRW_TAC[][]
THEN METIS_TAC[fun_INV_INV, fun_INV_works_2])

val action_image_INV = store_thm("action_image_INV",
``valid_inst f ==> 
  (action_image (fun_INV f) (action_image f (p,e)) = (p,e))``,
SRW_TAC[][action_image_def, state_image_INV])

val system_image_INV = store_thm("system_image_INV",
``valid_inst f /\ a IN system_image f PROB
  ==> action_image (fun_INV f) a IN PROB``,
SRW_TAC[][system_image_def]
THEN METIS_TAC[action_image_INV, GSYM pairTheory.PAIR])

val fun_INV_INV_2 = store_thm("fun_INV_INV_2",
``BIJ f UNIV UNIV ⇒ (fun_INV (fun_INV f) = f)``,
METIS_TAC[fun_INV_INV, FUN_EQ_THM])

val valid_INV = store_thm("valid_INV",
``valid_inst f ==> valid_inst (fun_INV f)``,
SRW_TAC[][valid_inst_def]
THEN SRW_TAC[][BIJ_DEF, INJ_DEF, SURJ_DEF]
THEN1 METIS_TAC[fun_INV_works]
THEN1 METIS_TAC[fun_INV_works_2])

val FAPPLY_f_o_2 = store_thm("FAPPLY_f_o_2",
``(!x y. (f x = f y) <=> (x = y)) /\ x IN {z | f z IN FDOM s}
   ==> ((s f_o f) ' x = s ' (f x))``,
SRW_TAC[][]
THEN `FINITE {x | f x IN FDOM s}` by METIS_TAC[bij_imp_finite_fdom_bck_img]
THEN SRW_TAC[][FDOM_f_o, bij_imp_finite_fdom_bck_img, FAPPLY_f_o, fmap_EXT])

val img_action_dom = store_thm("img_action_dom",
``valid_inst f ==>
 (action_dom (action_image f a) = 
    IMAGE f (action_dom a))``,
SRW_TAC[][action_image_pair, action_dom_pair, state_image_fdom])

val img_prob_dom = store_thm("img_prob_dom",
``valid_inst f ==>
 (prob_dom (system_image f PROB) = 
    IMAGE f (prob_dom PROB))``,
SRW_TAC[][prob_dom_def, EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1(Q.EXISTS_TAC `(fun_INV f) x`
      THEN SRW_TAC[][]
      THEN1 METIS_TAC[fun_INV_works, valid_inst_def]
      THEN1(Q.EXISTS_TAC `action_dom (action_image (fun_INV f) x')`
            THEN SRW_TAC[][]
            THEN1(SRW_TAC[][img_action_dom, valid_INV]
                  THEN METIS_TAC[])
            THEN1(FULL_SIMP_TAC(srw_ss())[system_image_def]
                  THEN METIS_TAC[action_image_INV, pairTheory.PAIR])))
THEN1(Q.EXISTS_TAC `action_dom (action_image f x'')`
      THEN SRW_TAC[][img_action_dom]
      THEN1 METIS_TAC[]
      THEN1(Q.EXISTS_TAC `(action_image f x'')`
            THEN SRW_TAC[][img_action_dom, system_image_def])))

val img_planning_prob_dom = store_thm("img_planning_prob_dom",
``valid_inst f ==> 
   (planning_prob_dom (planning_prob_image f planningPROB) = 
     IMAGE f (planning_prob_dom planningPROB))``,
SRW_TAC[][planning_prob_dom_def, planning_prob_image_def, EXTENSION]
THEN SRW_TAC[][img_prob_dom])

val FAPPLY_state_image = store_thm("FAPPLY_state_image",
``valid_inst f /\ x IN FDOM s ==> 
  (state_image f s ' (f x) = s ' x)``,
SRW_TAC[][state_image_def, valid_inst_def]
THEN `(s f_o fun_INV f) ' (f x) = s ' (fun_INV f (f x))` by
      (MATCH_MP_TAC FAPPLY_f_o_2
       THEN SRW_TAC[][]
       THEN1 METIS_TAC[fun_INV_works]
       THEN1 METIS_TAC[fun_INV_works_2])
THEN METIS_TAC[fun_INV_works_2])
      
val img_needed_vars = store_thm("img_needed_vars",
``valid_inst f ==>
  (needed_vars (planning_prob_image f planningPROB) = 
   IMAGE f (needed_vars planningPROB))``,
SRW_TAC[][planning_prob_image_def, needed_vars_def, EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1(Q.EXISTS_TAC `fun_INV f x`
      THEN SRW_TAC[][]
      THEN1 METIS_TAC[fun_INV_works, valid_inst_def]
      THEN1(`x IN IMAGE f (FDOM planningPROB.I)` by METIS_TAC[state_image_fdom]
            THEN FULL_SIMP_TAC(srw_ss())[IMAGE_DEF]
            THEN METIS_TAC[fun_INV_works_2, valid_inst_def])
      THEN1(Cases_on `(∃a.
                        a IN planningPROB.PROB /\ fun_INV f x IN FDOM (FST a) /\
                         (FST a ' (fun_INV f x) = planningPROB.I ' (fun_INV f x)))`
            THEN SRW_TAC[][]
            THEN`∃a.
                     a IN planningPROB.PROB /\ fun_INV f x IN FDOM (FST a) /\
                     (FST a ' (fun_INV f x) = planningPROB.I ' (fun_INV f x))` by 
                   (Q.EXISTS_TAC `action_image (fun_INV f) a`
                    THEN SRW_TAC[][]
                    THEN1 METIS_TAC[system_image_INV]
                    THEN1 SRW_TAC[][action_image_pair, state_image_fdom, valid_INV]
                    THEN1(SRW_TAC[][action_image_pair]
                          THEN METIS_TAC[FAPPLY_state_image, valid_INV, state_image_INV]))
                   THEN METIS_TAC[]))
THEN1(Q.EXISTS_TAC `fun_INV f x`
      THEN SRW_TAC[][]
      THEN1 METIS_TAC[fun_INV_works, valid_inst_def]
      THEN1(`x IN IMAGE f (FDOM planningPROB.I)` by METIS_TAC[state_image_fdom]
            THEN FULL_SIMP_TAC(srw_ss())[IMAGE_DEF]
            THEN METIS_TAC[fun_INV_works_2, valid_inst_def])
      THEN1(Cases_on `(∃a.
                        a IN planningPROB.PROB /\ fun_INV f x IN FDOM (FST a) /\
                         (FST a ' (fun_INV f x) = planningPROB.I ' (fun_INV f x)))`
            THEN SRW_TAC[][]
            THEN1(`x IN IMAGE f (FDOM planningPROB.G)` by METIS_TAC[state_image_fdom]
                 THEN FULL_SIMP_TAC(srw_ss())[IMAGE_DEF]
                 THEN METIS_TAC[fun_INV_works_2, valid_inst_def])
            THEN1(METIS_TAC[state_image_INV, FAPPLY_state_image, valid_INV])))
THEN1 SRW_TAC[][state_image_fdom]
THEN1(Cases_on `(∃a.
                  a IN system_image f planningPROB.PROB /\ f x' IN FDOM (FST a) /\
                 (FST a ' (f x') = state_image f planningPROB.I ' (f x')))`
      THEN SRW_TAC[][]
      THEN(`∃a.
              a IN system_image f planningPROB.PROB /\ f x' IN FDOM (FST a) /\
               (FST a ' (f x') = state_image f planningPROB.I ' (f x'))` by 
               (Q.EXISTS_TAC `action_image (f) a`
                THEN SRW_TAC[][]
                THEN1 SRW_TAC[][system_image_def]
                THEN1 SRW_TAC[][action_image_pair, state_image_fdom, valid_INV]
                THEN1(SRW_TAC[][action_image_pair]
                      THEN METIS_TAC[FAPPLY_state_image, valid_INV, state_image_INV]))
             THEN METIS_TAC[]))
THEN1 SRW_TAC[][state_image_fdom]
THEN1(Cases_on `(∃a.
                  a IN system_image f planningPROB.PROB /\ f x' IN FDOM (FST a) /\
                 (FST a ' (f x') = state_image f planningPROB.I ' (f x')))`
      THEN SRW_TAC[][]
      THEN1 SRW_TAC[][state_image_fdom]
      THEN1 METIS_TAC[FAPPLY_state_image]))

val valid_instantiations_works = store_thm("valid_instantiations_works",
``valid_instantiations {f1; f2} s /\ 
  f1 <> f2 ==> 
  IMAGE f1 s ∩
  IMAGE f2 s ⊆
IMAGE f1 ( s)``,
SRW_TAC[][common_vars_def, INTER_DEF, SUBSET_DEF, valid_instantiations_def]
THEN METIS_TAC[])

val valid_instantiations_works' = store_thm("valid_instantiations_works'",
``valid_instantiations {f1; f2} s /\ 
  f1 <> f2 ==> 
  IMAGE f1 s ∩
  IMAGE f2 s ⊆
IMAGE f1 (common_vars {f1; f2} s)``,
SRW_TAC[][common_vars_def, INTER_DEF, SUBSET_DEF, valid_instantiations_def]
THEN METIS_TAC[])

val valid_instantiations_works_2 = store_thm("valid_instantiations_works_2",
``valid_instantiations {f1; f2} s1 /\ 
  s2 SUBSET s1 /\
  f1 <> f2 ==> 
  IMAGE f1 s1 ∩
  IMAGE f2 s2 ⊆
IMAGE f1 (s1 INTER s2)``,
SRW_TAC[][common_vars_def, INTER_DEF, SUBSET_DEF, valid_instantiations_def]
THEN METIS_TAC[])

val valid_instantiations_works_2' = store_thm("valid_instantiations_works_2'",
``valid_instantiations {f1; f2} s1 /\ 
  s2 SUBSET s1 /\
  f1 <> f2 ==> 
  IMAGE f1 s1 ∩
  IMAGE f2 s2 ⊆
IMAGE f1 ((common_vars {f1; f2} s1) INTER s2)``,
SRW_TAC[][common_vars_def, INTER_DEF, SUBSET_DEF, valid_instantiations_def]
THEN METIS_TAC[])

val img_dom_inter_subset_comm_vars_1 = store_thm("img_dom_inter_subset_comm_vars_1",
``planning_problem planningPROB /\ valid_inst f1 /\ valid_inst f2 /\
  valid_instantiations {f1; f2} (planning_prob_dom planningPROB) /\
  f1 <> f2 ==> 
     planning_prob_dom (planning_prob_image f1 planningPROB) INTER (needed_vars (planning_prob_image f2 planningPROB)) SUBSET 
           IMAGE f1 (common_vars {f1;f2} (planning_prob_dom planningPROB) INTER needed_vars planningPROB)``,
SRW_TAC[][img_planning_prob_dom, img_needed_vars]
THEN METIS_TAC[valid_instantiations_works_2', needed_vars_subset, planning_problem_dom])

val valid_instantiations_subset = store_thm("valid_instantiations_subset",
``fSet1 SUBSET fSet2 /\ valid_instantiations fSet2 s ==> 
  valid_instantiations fSet1 s``,
SRW_TAC[][valid_instantiations_def, SUBSET_DEF])

val img_dom_inter_subset_comm_vars = store_thm("img_dom_inter_subset_comm_vars",
``planning_problem planningPROB /\ f1 <> f2 /\
  valid_inst f1 /\ valid_inst f2 /\
  valid_instantiations fSet (planning_prob_dom planningPROB) /\
  f1 IN fSet /\ f2 IN fSet ==>
  planning_prob_dom (planning_prob_image f1 planningPROB) INTER (needed_vars (planning_prob_image f2 planningPROB)) SUBSET 
         IMAGE f1 (common_vars fSet (planning_prob_dom planningPROB) INTER needed_vars planningPROB)``,
SRW_TAC[][]
THEN MATCH_MP_TAC SUBSET_TRANS
THEN Q.EXISTS_TAC `IMAGE f1
         (common_vars {f1;f2} (planning_prob_dom planningPROB) INTER
          needed_vars planningPROB)`
THEN SRW_TAC[][]
THEN1(MATCH_MP_TAC img_dom_inter_subset_comm_vars_1
      THEN SRW_TAC[][]
      THEN MATCH_MP_TAC (valid_instantiations_subset |> Q.GEN `fSet2`)
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN1(`{f1; f2} SUBSET fSet` by SRW_TAC[][]
      THEN `(common_vars {f1; f2} (planning_prob_dom planningPROB) ∩
             needed_vars planningPROB) ⊆
              (common_vars fSet (planning_prob_dom planningPROB) ∩
                  needed_vars planningPROB)` by 
              (MP_TAC (common_vars_subset |> INST_TYPE[beta |-> ``:'a``]
                                    |> Q.GENL [`fSet1`, `fSet2`, `s`]
                                    |> Q.SPECL[`{f1; f2}`,
                                               `fSet`, `(planning_prob_dom planningPROB)`])
              THEN SRW_TAC[][]
              THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF, INTER_DEF])
       THEN SRW_TAC[][]))

val img_valid_states = store_thm("img_valid_states",
``valid_inst f ==> 
  (valid_states (system_image f PROB) = 
    IMAGE (state_image f) (valid_states PROB))``,
SRW_TAC[][valid_states_def, EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][img_prob_dom]
THEN1(Q.EXISTS_TAC `state_image (fun_INV f) x` 
      THEN SRW_TAC[][]
      THEN1 METIS_TAC[state_image_INV, fun_INV_INV_2, valid_inst_def, valid_INV]
      THEN1(SRW_TAC[][state_image_fdom, valid_INV]
            THEN METIS_TAC[fun_INV_works, fun_INV_works_2, valid_inst_def]))
THEN1 SRW_TAC[][state_image_fdom, valid_INV])

val img_is_planning_problem = store_thm("img_is_planning_problem",
``valid_inst f /\ 
  planning_problem planningPROB ==>
  planning_problem (planning_prob_image f planningPROB)``,
SRW_TAC[][]
THEN SRW_TAC[][planning_problem_def]
THEN1 FULL_SIMP_TAC(srw_ss())[planning_prob_image_def, img_valid_states, planning_problem_def]
THEN1(SRW_TAC[][planning_prob_image_def, img_planning_prob_dom, state_image_fdom]
      THEN METIS_TAC[planning_problem_def, IMAGE_SUBSET]))

val valid_instantiations_state_images_agree = store_thm("valid_instantiations_state_images_agree",
``valid_inst f1 /\ valid_inst f2 /\
  valid_instantiations {f1; f2} (FDOM s) ==> 
    agree (state_image f1 s) (state_image f2 s)``,
SRW_TAC[][agree_def]
THEN `v IN IMAGE f1 (FDOM s)` by METIS_TAC[state_image_fdom]
THEN `state_image f1 s ' (f1 (fun_INV f1 v)) = s ' (fun_INV f1 v)` by
     (MATCH_MP_TAC FAPPLY_state_image
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[IMAGE_DEF]
      THEN METIS_TAC[fun_INV_works_2, valid_inst_def])
THEN `v IN IMAGE f2 (FDOM s)` by METIS_TAC[state_image_fdom]
THEN `state_image f2 s ' (f2 (fun_INV f2 v)) = s ' (fun_INV f2 v)` by
     (MATCH_MP_TAC FAPPLY_state_image
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[IMAGE_DEF]
      THEN METIS_TAC[fun_INV_works_2, valid_inst_def])
THEN FULL_SIMP_TAC(srw_ss())[valid_instantiations_def]
THEN METIS_TAC[fun_INV_works, fun_INV_works_2, valid_inst_def])

val prob_image_init_agree = store_thm("prob_image_init_agree",
``planning_problem planningPROB /\ valid_inst f1 /\ valid_inst f2 /\
  valid_instantiations {f1; f2} (planning_prob_dom planningPROB) ==> 
    agree (planning_prob_image f1 planningPROB).I
     (planning_prob_image f2 planningPROB).I``,
SRW_TAC[][planning_prob_image_def]
THEN METIS_TAC[valid_instantiations_state_images_agree, planning_problem_dom])

val valid_instantiations_subset_2 = store_thm("valid_instantiations_subset_2",
``valid_instantiations fSet vs1 /\ vs2 SUBSET vs1 ==> 
   valid_instantiations fSet vs2``,
SRW_TAC[][valid_instantiations_def, SUBSET_DEF]
THEN METIS_TAC[])

val valid_instantiations_images_inter = store_thm("valid_instantiations_images_inter",
``valid_inst f1 /\ valid_inst f2 /\
  valid_instantiations {f1; f2} vs /\ 
  vs' SUBSET vs ==> 
     (((IMAGE f1 vs') INTER (IMAGE f2 vs)) = ((IMAGE f2 vs') INTER (IMAGE f1 vs)))``,
MP_TAC(fun_INV_works |> Q.GENL[`f`, `x`])
THEN MP_TAC(fun_INV_works_2|> Q.GENL[`f`, `x`])
THEN SRW_TAC[][EXTENSION, SUBSET_DEF, valid_inst_def, BIJ_DEF, INJ_DEF, SURJ_DEF, valid_instantiations_def]
THEN METIS_TAC[])

val prob_image_goals_same = store_thm("prob_image_goals_same",
``planning_problem planningPROB /\ valid_inst f1 /\ valid_inst f2 /\
  valid_instantiations {f1; f2} (planning_prob_dom planningPROB) ==> 
 (DRESTRICT (planning_prob_image f1 planningPROB).G
  (planning_prob_dom (planning_prob_image f2 planningPROB)) =
  DRESTRICT (planning_prob_image f2 planningPROB).G
    (planning_prob_dom (planning_prob_image f1 planningPROB)))``,
SRW_TAC[][img_planning_prob_dom, planning_prob_image_def]
THEN SRW_TAC[][fmap_EXT]
THEN `FDOM
      (DRESTRICT (state_image f1 planningPROB.G)
         (IMAGE f2 (planning_prob_dom planningPROB))) =
            FDOM
              (DRESTRICT (state_image f2 planningPROB.G)
                (IMAGE f1 (planning_prob_dom planningPROB)))` by
     (SRW_TAC[][state_image_fdom, FDOM_DRESTRICT]
      THEN MATCH_MP_TAC valid_instantiations_images_inter
      THEN SRW_TAC[][] THEN FULL_SIMP_TAC(srw_ss())[planning_problem_def])
THEN1(MATCH_MP_TAC (SIMP_RULE(srw_ss())[AND_IMP_INTRO, PULL_FORALL, agree_def] agree_DRESTRICT_2)
      THEN SRW_TAC[][]
      THEN1(SRW_TAC[][state_image_fdom]
            THEN METIS_TAC[IMAGE_SUBSET, planning_problem_def])
      THEN1(SRW_TAC[][state_image_fdom]
            THEN METIS_TAC[IMAGE_SUBSET, planning_problem_def])
      THEN1(MATCH_MP_TAC (SIMP_RULE(srw_ss())[AND_IMP_INTRO, PULL_FORALL, agree_def] valid_instantiations_state_images_agree)
            THEN SRW_TAC[][] 
            THEN METIS_TAC[valid_instantiations_subset_2, planning_problem_def])
      THEN1 METIS_TAC[]))

val state_image_drestrict = store_thm("state_image_drestrict",
``valid_inst f ==> 
  (state_image f (DRESTRICT s vs) = DRESTRICT (state_image f s) (IMAGE f vs))``,
SRW_TAC[][fmap_EXT]
THEN `FDOM (state_image f (DRESTRICT s vs)) =
        FDOM (DRESTRICT (state_image f s) (IMAGE f vs))` by
         METIS_TAC[state_image_fdom, valid_inst_def, BIJ_DEF, FDOM_DRESTRICT, IMAGE_INTER_EQ]
THEN FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT, DRESTRICT_DEF]
THEN SRW_TAC[][]
THEN1(`f x' IN IMAGE f (FDOM s) ` by METIS_TAC[state_image_fdom, IMAGE_DEF]
      THEN `state_image f (DRESTRICT s vs) ' (f x') = (DRESTRICT s vs) ' x'` by 
         (MATCH_MP_TAC FAPPLY_state_image
          THEN SRW_TAC[][FDOM_DRESTRICT]
          THEN FULL_SIMP_TAC(srw_ss())[IMAGE_DEF]
          THEN METIS_TAC[fun_INV_works, fun_INV_works_2, valid_inst_def])
      THEN `DRESTRICT s vs ' x' = s ' x'` by
           (SRW_TAC[][DRESTRICT_DEF]
            THEN FULL_SIMP_TAC(srw_ss())[IMAGE_DEF]
            THEN METIS_TAC[FAPPLY_state_image, fun_INV_works, fun_INV_works_2, valid_inst_def])
      THEN FULL_SIMP_TAC(srw_ss())[IMAGE_DEF]
      THEN METIS_TAC[FAPPLY_state_image, fun_INV_works, fun_INV_works_2, valid_inst_def])      
THEN1 METIS_TAC[])

val sustainable_img = store_thm("sustainable_img",
``valid_inst f ==>
  (sustainable_vars planningPROB vs
     ==> sustainable_vars (planning_prob_image f planningPROB) (IMAGE f vs))``,
SRW_TAC[][sustainable_vars_def, planning_prob_image_def, GSYM state_image_drestrict])

val sustainable_quotient_preceding_imgs = store_thm("sustainable_quotient_preceding_imgs",
``valid_inst f1 /\ valid_inst f2 /\ valid_instantiations fSet (planning_prob_dom planningPROB)/\
  planning_problem planningPROB /\ f1 IN fSet /\ f2 IN fSet /\ f1<> f2 /\
  sustainable_vars planningPROB ((common_vars fSet (planning_prob_dom planningPROB)) INTER (needed_vars planningPROB)) ==> 
    precede (planning_prob_image f1 planningPROB) (planning_prob_image f2 planningPROB)``,
SRW_TAC[][]
THEN `valid_instantiations {f1; f2} (planning_prob_dom planningPROB)` by
        (FULL_SIMP_TAC(srw_ss())[valid_instantiations_def]
         THEN METIS_TAC[])
THEN MATCH_MP_TAC sustain_imp_precede
THEN SRW_TAC[][]
THEN1 METIS_TAC[img_is_planning_problem]
THEN1 METIS_TAC[img_is_planning_problem]
THEN1(METIS_TAC[prob_image_init_agree])      
THEN1 METIS_TAC[prob_image_goals_same]
THEN1(SRW_TAC[][Once img_is_planning_problem, sustainable_prob_dom_inter]
      THEN MATCH_MP_TAC (sustainable_subset |> Q.GEN `vs2`)
      THEN Q.EXISTS_TAC `IMAGE f1 (common_vars fSet (planning_prob_dom planningPROB) INTER (needed_vars planningPROB))`
      THEN SRW_TAC[][]
      THEN1(MP_TAC img_dom_inter_subset_comm_vars
            THEN SRW_TAC[][]
            THEN MATCH_MP_TAC SUBSET_TRANS
            THEN Q.EXISTS_TAC `planning_prob_dom (planning_prob_image f1 planningPROB) ∩
                         planning_prob_dom (planning_prob_image f2 planningPROB)`
            THEN SRW_TAC[][]
            THEN METIS_TAC[INTER_SUBSET, needed_vars_subset, INTER_COMM, SUBSET_TRANS, planning_problem_dom,
                      img_is_planning_problem])		      
      THEN1(METIS_TAC[sustainable_img])))

val sustainable_quotient_precede_imgs_list = store_thm("sustainable_quotient_precede_imgs_list",
``ALL_DISTINCT instList /\
  (!inst. MEM inst instList ==> valid_inst inst) /\
  valid_instantiations (set instList) (planning_prob_dom planningPROB)/\
  planning_problem planningPROB /\
  sustainable_vars planningPROB ((common_vars (set instList) (planning_prob_dom planningPROB)) INTER (needed_vars planningPROB)) ==> 
    precede_prob_list (MAP (\inst. planning_prob_image inst planningPROB) instList)``,
Induct_on `instList`
THEN SRW_TAC[][precede_prob_list_def]
THEN1(FULL_SIMP_TAC(srw_ss())[MEM_MAP]
      THEN MATCH_MP_TAC (sustainable_quotient_preceding_imgs |> Q.GEN `fSet`)
      THEN Q.EXISTS_TAC `set (h::instList)`
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN1(FIRST_X_ASSUM MATCH_MP_TAC
      THEN SRW_TAC[][]
      THEN1 FULL_SIMP_TAC(srw_ss())[valid_instantiations_def]
      THEN1(MATCH_MP_TAC (sustainable_subset |> Q.GEN `vs2`)
            THEN SRW_TAC[][]
            THEN Q.EXISTS_TAC `(common_vars (h INSERT set instList)
                          (planning_prob_dom planningPROB) ∩ needed_vars planningPROB)`
            THEN SRW_TAC[][]
            THEN `(set instList) SUBSET (h INSERT set instList)` by SRW_TAC[][]
            THEN `common_vars (set instList) (planning_prob_dom planningPROB)
                        SUBSET common_vars (h INSERT set instList) (planning_prob_dom planningPROB)` by METIS_TAC[common_vars_subset]
            THEN METIS_TAC[SUBSET_TRANS, INTER_SUBSET])))

val state_succ_image = store_thm("state_succ_image",
``valid_inst f ==> 
  (((state_image f s1) ⊌ (state_image f s2)) = (state_image f (s1 ⊌ s2)))``,
SRW_TAC[][fmap_EXT]
THEN1 SRW_TAC[][state_image_fdom]
THEN1(SRW_TAC[][FUNION_DEF]
      THEN `x IN IMAGE f (FDOM s1)` by METIS_TAC[state_image_fdom]
      THEN FULL_SIMP_TAC(srw_ss())[IMAGE_DEF]
      THEN SRW_TAC[][FAPPLY_state_image, FUNION_DEF])
THEN1(SRW_TAC[][FUNION_DEF]
      THEN `x IN IMAGE f (FDOM s2)` by METIS_TAC[state_image_fdom]
      THEN FULL_SIMP_TAC(srw_ss())[IMAGE_DEF]
      THEN SRW_TAC[][FAPPLY_state_image, FUNION_DEF]
      THEN1(`f x' IN IMAGE f (FDOM s1)` by METIS_TAC[state_image_fdom]
            THEN FULL_SIMP_TAC(srw_ss())[IMAGE_DEF]
            THEN1 METIS_TAC[fun_INV_works_2, valid_inst_def])
      THEN1(`~(f x' IN IMAGE f (FDOM s1))` by METIS_TAC[state_image_fdom]
            THEN FULL_SIMP_TAC(srw_ss())[IMAGE_DEF]
            THEN1 METIS_TAC[fun_INV_works_2, valid_inst_def])))

val submap_image = store_thm("submap_image",
``valid_inst f ==> 
  (s1 SUBMAP s2 <=> state_image f s1 SUBMAP state_image f s2)``,
SRW_TAC[][]
THEN EQ_TAC
THEN1(SRW_TAC[][SUBMAP_DEF, state_image_fdom]
      THEN1 METIS_TAC[fun_INV_works, valid_inst_def]
      THEN1 METIS_TAC[FAPPLY_state_image])
THEN1(SRW_TAC[][SUBMAP_DEF, state_image_fdom]
      THEN1 METIS_TAC[fun_INV_works, fun_INV_works_2, valid_inst_def]
      THEN1 METIS_TAC[fun_INV_works, fun_INV_works_2, valid_inst_def, FAPPLY_state_image]))

val state_image_state_succ = store_thm("state_image_state_succ",
``valid_inst f ==> 
   (state_succ (state_image f s) (action_image f a) = state_image f (state_succ s a))``,
SRW_TAC[][state_succ_def, action_image_pair,  state_succ_image]
THEN METIS_TAC[submap_image])

val as_image_exec = store_thm("as_image_exec",
``!s. valid_inst f ==> 
   (exec_plan(state_image f s, as_image f as) = state_image f (exec_plan(s,as)))``,
Induct_on `as`
THEN SRW_TAC[][exec_plan_def, as_image_def]
THEN SRW_TAC[][GSYM as_image_def, state_image_state_succ])

val system_image_valid_plans = store_thm("system_image_valid_plans",
`` valid_inst f ==> 
   (valid_plans (system_image f PROB) = IMAGE (as_image f) (valid_plans PROB))``,
SRW_TAC[][EXTENSION, valid_plans_def]
THEN Induct_on `x`
THEN SRW_TAC[][as_image_def]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1(Q.EXISTS_TAC `(action_image (fun_INV f) h)::x'`
      THEN SRW_TAC[][]
      THEN1 METIS_TAC[action_image_INV, pairTheory.PAIR, fun_INV_INV_2, valid_inst_def, valid_INV]
      THEN1 METIS_TAC[system_image_INV])
THEN Cases_on `x' = []`
THEN FULL_SIMP_TAC(srw_ss())[]
THEN `?h l. x' = h::l` by METIS_TAC[list_utilsTheory.non_empty_list]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN1(SRW_TAC[][system_image_def, IMAGE_DEF]
      THEN METIS_TAC[])
THEN1 METIS_TAC[])

val planning_prob_image_plan = store_thm("planning_prob_image_plan",
``valid_inst f /\ plan planningPROB as ==> 
   plan (planning_prob_image f planningPROB) (as_image f as)``,
SRW_TAC[][planning_prob_image_def, plan_def]
THEN1 SRW_TAC[][system_image_valid_plans]
THEN1 SRW_TAC[][as_image_exec, GSYM submap_image])

val getting_burried_fun_in_MAP_2 = store_thm("getting_burried_fun_in_MAP_2",
``(INJ H2 (set fList) UNIV) ==> 
   ?h.
    (MAP (\f. g (H1 f) (H2 f)) fList
      = MAP (\y. g (H1 (h y)) y) (MAP H2 fList)) /\
    (!f. MEM f fList ==> (h (H2 f) = f))``,
SRW_TAC[][]
THEN Q.EXISTS_TAC `(\y.(CHOICE {f | MEM f fList /\ (y = H2 f)}))`
THEN Induct_on `fList`
THEN SRW_TAC[][]
THEN1(`{f | ((f = h) \/ MEM f fList) /\ (H2 h = H2 f)} <> EMPTY` by
         (SRW_TAC[][EXTENSION]
          THEN METIS_TAC[])
      THEN `(CHOICE {f | ((f = h) \/ MEM f fList) /\ (H2 h = H2 f)}) IN 
               {f | ((f = h) \/ MEM f fList) /\ (H2 h = H2 f)}` by
             SRW_TAC[][CHOICE_DEF]
      THEN FULL_SIMP_TAC(srw_ss())[IN_DEF]
      THEN `(CHOICE {f | ((f = h) \/ set fList f) /\ (H2 h = H2 f)}) IN h INSERT set fList` by FULL_SIMP_TAC(srw_ss())[IN_DEF]
      THEN `h IN h INSERT set fList` by SRW_TAC[][]
      THEN METIS_TAC[INJ_DEF])
THEN1(`!y. MEM y (MAP H2 fList) ==> 
                  ({f | ((f = h) \/ MEM f fList) /\ (y = H2 f)} = {f | (MEM f fList) /\ (y = H2 f)})` by
               (SRW_TAC[][EXTENSION, MEM_MAP]
                THEN FULL_SIMP_TAC(bool_ss)[Once INJ_DEF, GSYM MEM]
                THEN EQ_TAC
                THEN SRW_TAC[][]
                (*THEN FULL_SIMP_TAC(bool_ss)[IN_DEF]*)
                THEN `h IN h INSERT set fList` by SRW_TAC[][]
                THEN `y' IN h INSERT set fList` by SRW_TAC[][]
                THEN METIS_TAC[])
      THEN `!y. MEM y (MAP H2 fList) ==> 
               ((\y. g (H1 (CHOICE {f | ((f = h) \/ MEM f fList) /\ (y = H2 f)})) y) y = 
                         (\y. g (H1 ((\y. CHOICE {f | MEM f fList /\ (y = H2 f)}) y)) y) y)` by
            SRW_TAC[][]
      THEN `INJ H2 (set fList) UNIV` by
             (FULL_SIMP_TAC(bool_ss)[INJ_DEF]
              THEN SRW_TAC[][])
      THEN FULL_SIMP_TAC(srw_ss())[GSYM MAP_EQ_f])
THEN1(`{f' | ((f' = f) \/ MEM f' fList) /\ (H2 f = H2 f')} <> EMPTY` by
         (SRW_TAC[][EXTENSION]
          THEN METIS_TAC[])
      THEN `(CHOICE {f' | ((f' = f) \/ MEM f' fList) /\ (H2 f = H2 f')}) IN 
               {f' | ((f' = f) \/ MEM f' fList) /\ (H2 f = H2 f')}` by
             SRW_TAC[][CHOICE_DEF]
      THEN FULL_SIMP_TAC(srw_ss())[IN_DEF]
      THEN `(CHOICE {f' | ((f' = f) \/ MEM f' fList) /\ (H2 f = H2 f')}) IN f INSERT set fList` by FULL_SIMP_TAC(srw_ss())[IN_DEF]
      THEN `f IN f INSERT set fList` by SRW_TAC[][]
      THEN `{f' | ((f' = f) \/ set fList f') /\ (H2 f = H2 f')} =
                  {f' | ((f' = f) \/ MEM f' fList) /\ (H2 f = H2 f')}` by SRW_TAC[][IN_DEF]
      THEN SRW_TAC[][]
      THEN METIS_TAC[INJ_DEF])
THEN1(`{f' | ((f' = h) \/ MEM f' fList) /\ (H2 f = H2 f')} <> EMPTY` by
         (SRW_TAC[][EXTENSION]
          THEN METIS_TAC[])
      THEN `(CHOICE {f' | ((f' = h) \/ MEM f' fList) /\ (H2 f = H2 f')}) IN 
               {f' | ((f' = h) \/ MEM f' fList) /\ (H2 f = H2 f')}` by
             SRW_TAC[][CHOICE_DEF]
      THEN FULL_SIMP_TAC(srw_ss())[IN_DEF]
      THEN `(CHOICE {f' | ((f' = h) \/ MEM f' fList) /\ (H2 f = H2 f')}) IN h INSERT set fList` by FULL_SIMP_TAC(srw_ss())[IN_DEF]
      THEN1(`h IN h INSERT set fList` by SRW_TAC[][]
             THEN `f IN h INSERT set fList` by SRW_TAC[][IN_DEF]
             THEN `{f' | ((f' = h) \/ set fList f') /\ (H2 f = H2 f')} =
                        {f' | ((f' = h) \/ MEM f' fList) /\ (H2 f = H2 f')}` by SRW_TAC[][IN_DEF]
             THEN SRW_TAC[][]
             THEN METIS_TAC[INJ_DEF])
      THEN1(`f IN h INSERT set fList` by SRW_TAC[][IN_DEF]
             THEN `{f' | ((f' = h) \/ set fList f') /\ (H2 f = H2 f')} =
                        {f' | ((f' = h) \/ MEM f' fList) /\ (H2 f = H2 f')}` by SRW_TAC[][IN_DEF]
             THEN SRW_TAC[][]
             THEN METIS_TAC[INJ_DEF])))

val getting_burried_fun_in_MAP = store_thm("getting_burried_fun_in_MAP",
``(INJ f (set fList) UNIV) ==> 
   ?h.
    MAP g fList
     = MAP (\x. g (h x)) (MAP f fList)``,
SRW_TAC[][]
THEN Q.EXISTS_TAC `(\y.(CHOICE {x | MEM x fList /\ (y = f x)}))`
THEN Induct_on `fList`
THEN SRW_TAC[][]
THEN1(`{x | ((x = h) \/ MEM x fList) /\ (f h = f x)} <> EMPTY` by
         (SRW_TAC[][EXTENSION]
          THEN METIS_TAC[])
      THEN `(CHOICE {x | ((x = h) \/ MEM x fList) /\ (f h = f x)}) IN 
               {x | ((x = h) \/ MEM x fList) /\ (f h = f x)}` by
             SRW_TAC[][CHOICE_DEF]
      THEN FULL_SIMP_TAC(srw_ss())[IN_DEF]
      THEN `(CHOICE {x | ((x = h) \/ set fList x) /\ (f h = f x)}) IN h INSERT set fList` by FULL_SIMP_TAC(srw_ss())[IN_DEF]
      THEN `h IN h INSERT set fList` by SRW_TAC[][]
      THEN METIS_TAC[INJ_DEF])
THEN1(`!y. MEM y (MAP f fList) ==> 
                  ({x | ((x = h) \/ MEM x fList) /\ (y = f x)} = {x | (MEM x fList) /\ (y = f x)})` by
               (SRW_TAC[][EXTENSION, MEM_MAP]
                THEN FULL_SIMP_TAC(bool_ss)[Once INJ_DEF, GSYM MEM]
                THEN EQ_TAC
                THEN SRW_TAC[][]
                (*THEN FULL_SIMP_TAC(bool_ss)[IN_DEF]*)
                THEN `h IN h INSERT set fList` by SRW_TAC[][]
                THEN `y' IN h INSERT set fList` by SRW_TAC[][]
                THEN METIS_TAC[])
        THEN FULL_SIMP_TAC(srw_ss())[]
        THEN `!y. MEM y (MAP f fList) ==> 
               ((\x. g (CHOICE {x' | ((x' = h) \/ MEM x' fList) /\ (x = f x')})) y = 
                         (\x. g (CHOICE {x' | MEM x' fList /\ (x = f x')})) y)` by
            SRW_TAC[][]
      THEN `INJ f (set fList) UNIV` by
             (FULL_SIMP_TAC(bool_ss)[INJ_DEF]
              THEN SRW_TAC[][])
      THEN FULL_SIMP_TAC(srw_ss())[GSYM MAP_EQ_f]))

val inj_insts_imp_map_quotprob_to_quotplan = store_thm("inj_insts_imp_map_quotprob_to_quotplan",
``let instQuotProb = (\inst. planning_prob_image inst quotientPROB);
      instQuotPlan = (\inst. as_image inst quotientPlan) in
  INJ instQuotProb (set instList) UNIV 
  ==> ?f.
         ((MAP (\inst. rem_condless_act(needed_asses (instQuotProb inst), [], instQuotPlan inst))
               instList) =
            (MAP (\planningPROB'. rem_condless_act(needed_asses planningPROB', [], f planningPROB'))
                 (MAP instQuotProb instList))) /\
         (!inst. MEM inst instList
                 ==> (f (instQuotProb inst) = instQuotPlan inst))``,
SRW_TAC[][]
THEN MP_TAC(
       SIMP_RULE(srw_ss())[]
           (getting_burried_fun_in_MAP_2
                    |> Q.GENL[`fList`, `H2`, `H1`, `g`]
                    |> INST_TYPE [alpha |-> ``:'a -> 'a``, 
                                  beta |-> ``:(α, β) planningProblem``,
                                  gamma |-> ``:((α |-> β) # (α |-> β)) list``,
                                  delta |-> ``:((α |-> β) # (α |-> β)) list``]
                    |> Q.SPECL[`instList`,
                           `(\inst. planning_prob_image inst (quotientPROB: (α, β) planningProblem))`,
                           `(\inst. as_image inst (quotientPlan:((α |-> β) # (α |-> β)) list))`,
                           `(\as planningPROB.
                               rem_condless_act
                               (needed_asses (planningPROB: (α, β) planningProblem),[],
                                 (as:((α |-> β) # (α |-> β)) list)))`]))
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `(\planningPROB. as_image (h planningPROB) quotientPlan)`
THEN SRW_TAC[SatisfySimps.SATISFY_ss][])

val IJCAI_Theorem_4 = store_thm("IJCAI_Theorem_4",
``ALL_DISTINCT instList /\ 
  INJ (\inst. planning_prob_image inst quotientPROB) (set instList) UNIV /\
  (!inst. MEM inst instList ==> valid_inst inst) /\
  valid_instantiations (set instList) (planning_prob_dom quotientPROB)/\
  planning_problem quotientPROB /\
  sustainable_vars quotientPROB ((common_vars (set instList) (planning_prob_dom quotientPROB)) INTER (needed_vars quotientPROB)) /\
  covers (MAP (\inst. planning_prob_image inst quotientPROB) instList) planningPROB /\
  plan quotientPROB quotientPlan
  ==> plan planningPROB (FLAT (MAP (\inst. rem_condless_act(needed_asses (planning_prob_image inst quotientPROB), [], as_image inst quotientPlan) ) instList))``,
SRW_TAC[][]
THEN MP_TAC inj_insts_imp_map_quotprob_to_quotplan
THEN SRW_TAC[][LET_DEF]
THEN SRW_TAC[][]
THEN HO_MATCH_MP_TAC (SIMP_RULE(srw_ss())[AND_IMP_INTRO, PULL_FORALL] (IJCAI_Theorem_3'))
THEN SRW_TAC[][]
THEN1 METIS_TAC[sustainable_quotient_precede_imgs_list]
THEN FULL_SIMP_TAC(srw_ss())[MEM_MAP]
THEN1 METIS_TAC[img_is_planning_problem]
THEN1 METIS_TAC[planning_prob_image_plan])

val IJCAI_Theorem_4_ITP = store_thm("IJCAI_Theorem_4_ITP",
``
  let instPlans = MAP (\inst. rem_condless_act(needed_asses (planning_prob_image inst quotientPROB), [], as_image inst quotientPlan) ) instList;
      concatenatedPlans = FLAT instPlans in
  ALL_DISTINCT instList /\ 
  INJ (\inst. planning_prob_image inst quotientPROB) (set instList) UNIV /\
  (!inst. MEM inst instList ==> valid_inst inst) /\
  valid_instantiations (set instList) (planning_prob_dom quotientPROB)/\
  planning_problem quotientPROB /\
  sustainable_vars quotientPROB ((common_vars (set instList) (planning_prob_dom quotientPROB)) INTER (needed_vars quotientPROB)) /\
  covers (MAP (\inst. planning_prob_image inst quotientPROB) instList) planningPROB /\
  plan quotientPROB quotientPlan
 ==> 
     plan planningPROB concatenatedPlans``,
SRW_TAC[][IJCAI_Theorem_4, LET_DEF])


val augmented_prob_planning_problem = store_thm("augmented_prob_planning_problem",
``planning_problem planningPROB ==>
  planning_problem (planningPROB with G:= (FUNION (DRESTRICT planningPROB.I ((common_vars (set instList) (planning_prob_dom planningPROB)) INTER (needed_vars planningPROB))) planningPROB.G))``,
SRW_TAC[][planning_problem_def, planning_prob_dom_def, FDOM_DRESTRICT, valid_states_def])

val augmented_planning_prob_dom = store_thm("augmented_planning_prob_dom",
``planning_problem planningPROB ==>
  (planning_prob_dom 
     (planningPROB with G:= (FUNION (DRESTRICT planningPROB.I ((common_vars (set instList) (planning_prob_dom planningPROB)) INTER (needed_vars planningPROB))) planningPROB.G))
        = planning_prob_dom planningPROB)``,
SRW_TAC[][planning_problem_def, planning_prob_dom_def])

val augmented_prob_sustains_vars_2 = store_thm("augmented_prob_sustains_vars_2",
``(let augmentedQuotientPROB = 
         (quotientPROB with G:=
             (FUNION (DRESTRICT quotientPROB.I 
                                ((common_vars (set instList) (planning_prob_dom quotientPROB))
                                  INTER (needed_vars quotientPROB)))
                      quotientPROB.G))
   in
      (sustainable_vars augmentedQuotientPROB
                        (common_vars (set instList)
                         (planning_prob_dom augmentedQuotientPROB) ∩
                          needed_vars augmentedQuotientPROB)))``,
SRW_TAC[][sustainable_vars_def]
THEN SRW_TAC[][fmap_EXT]
THEN `FDOM
      (DRESTRICT augmentedQuotientPROB.I
         (common_vars (set instList)
            (planning_prob_dom augmentedQuotientPROB) INTER
          needed_vars augmentedQuotientPROB)) =
    FDOM
      (DRESTRICT augmentedQuotientPROB.G
         (common_vars (set instList)
            (planning_prob_dom augmentedQuotientPROB) INTER
          needed_vars augmentedQuotientPROB))` by
         (SRW_TAC[][FDOM_DRESTRICT]
          THEN SRW_TAC[][Abbr `augmentedQuotientPROB`]
          THEN SRW_TAC[][FDOM_DRESTRICT, INTER_DEF, needed_vars_def,
                         FUNION_DEF, EXTENSION, DRESTRICT_DEF,
                         common_vars_def, planning_prob_dom_def,
                         prob_dom_def, action_dom_pair, UNION_DEF,
                         IMAGE_DEF]
          THEN SRW_TAC[][]
          THEN FULL_SIMP_TAC(srw_ss())[])
THEN SRW_TAC[][DRESTRICT_DEF]
THEN(FULL_SIMP_TAC(srw_ss())[Abbr `augmentedQuotientPROB`]
     THEN FULL_SIMP_TAC(srw_ss())
                    [FDOM_DRESTRICT, INTER_DEF, needed_vars_def,
                     FUNION_DEF, EXTENSION, DRESTRICT_DEF,
                     common_vars_def, planning_prob_dom_def,
                     prob_dom_def, action_dom_pair, UNION_DEF,
                     IMAGE_DEF]
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[]
      THEN METIS_TAC[]))

val IJCAI_goal_augmentation_works = store_thm("IJCAI_goal_augmentation_works",
``let augmentedQuotientPROB = 
         (quotientPROB with G:=
             (FUNION (DRESTRICT quotientPROB.I 
                                ((common_vars (set instList) (planning_prob_dom quotientPROB))
                                  INTER (needed_vars quotientPROB)))
                      quotientPROB.G))
    in
  ( ALL_DISTINCT instList /\ 
    (!inst. MEM inst instList ==> valid_inst inst) /\
    planning_problem quotientPROB ==>
    (INJ (\inst. planning_prob_image inst augmentedQuotientPROB) (set instList) UNIV /\
      valid_instantiations (set instList) (planning_prob_dom augmentedQuotientPROB)/\
      covers (MAP (\inst. planning_prob_image inst augmentedQuotientPROB) instList) planningPROB /\
      plan augmentedQuotientPROB augmentedQuotientPlan
      ==> 
         plan planningPROB (FLAT (MAP (\inst. rem_condless_act(needed_asses (planning_prob_image inst augmentedQuotientPROB), [], as_image inst augmentedQuotientPlan) ) instList))))``,
SRW_TAC[][]
THEN MATCH_MP_TAC IJCAI_Theorem_4
THEN SRW_TAC[][]
THEN1 METIS_TAC[augmented_prob_planning_problem]
THEN1 METIS_TAC[augmented_prob_sustains_vars_2])


val IJCAI_goal_augmentation_works_ITP = store_thm("IJCAI_goal_augmentation_works_ITP",
``let augmentedQuotientPROB = 
         (quotientPROB with G:=
             (FUNION (DRESTRICT quotientPROB.I 
                                ((common_vars (set instList) (planning_prob_dom quotientPROB))
                                  INTER (needed_vars quotientPROB)))
                      quotientPROB.G));
      instPlans = MAP (\inst. rem_condless_act(needed_asses (planning_prob_image inst augmentedQuotientPROB), [], as_image inst augmentedQuotientPlan)) instList;
      concatenatedPlans = FLAT instPlans      
    in
  ( ALL_DISTINCT instList /\ 
    (!inst. MEM inst instList ==> valid_inst inst) /\
    planning_problem quotientPROB /\
    INJ (\inst. planning_prob_image inst augmentedQuotientPROB) (set instList) UNIV /\
      valid_instantiations (set instList) (planning_prob_dom augmentedQuotientPROB)/\
      covers (MAP (\inst. planning_prob_image inst augmentedQuotientPROB) instList) planningPROB /\
      plan augmentedQuotientPROB augmentedQuotientPlan
      ==> 
         plan planningPROB concatenatedPlans)``,
SRW_TAC[][LET_DEF]
THEN MP_TAC(IJCAI_goal_augmentation_works)
THEN METIS_TAC[]
)

(*
``(!inst. inst IN instSet ==> (state_image f s) SUBMAP s2)
   ==> valid_instantiations instSet (planning_prob_dom planningPROB)``
SRW_TAC[][subprob_def, valid_instantiations_def]

*)

val _ = export_theory();
