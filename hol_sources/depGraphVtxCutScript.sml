open HolKernel Parse boolLib bossLib;
open systemAbstractionTheory;
open factoredSystemTheory;
open finite_mapTheory;
open pairTheory;
open listTheory;
open sublistTheory;
open pred_setTheory;
open rich_listTheory;
open actionSeqProcessTheory
val _ = new_theory "depGraphVtxCut";

val replace_projected_single_def = 
 Define 
`(replace_projected_single(P, a :: as, a_1::as_1) = 
               if (P(a, a_1)) then
                 a :: replace_projected_single(P, as, as_1)
               else replace_projected_single(P, as, a_1 :: as_1))
/\ (replace_projected_single(P, [], as_1) = [])
/\ (replace_projected_single(P, as, []) = [])`;

val replace_projected_dual_def = 
 Define 
`(replace_projected_dual(P_1, P_2, a :: as, a_1::as_1, a_2::as_2) = 
               if (P_1(a, a_1) /\ P_2 (a, a_2)) then
                 a::replace_projected_dual(P_1, P_2, as, as_1, as_2)
               else if (P_1(a, a_1)) then
                 a::replace_projected_dual(P_1, P_2, as, as_1, a_2::as_2)
               else if (P_2( a, a_2)) then
                 a::replace_projected_dual(P_1, P_2, as, a_1::as_1, as_2)
               else replace_projected_dual(P_1, P_2, as, a_1::as_1, a_2::as_2))
/\ (replace_projected_dual(P_1, P_2, [], as_1, as_2) = [])
/\ (replace_projected_dual(P_1, P_2, as, as_1, []) = replace_projected_single(P_1, as, as_1))
/\ (replace_projected_dual(P_1, P_2, as, [], as_2) = replace_projected_single(P_2, as, as_2))`;

val len_replace_proj_single_le = store_thm("len_replace_proj_single_le",
``!P as_1 as. 
     LENGTH 
         (replace_projected_single( 
             P,
             as, as_1)) <= LENGTH(as_1)``,
STRIP_TAC
THEN Induct_on `as`
THEN1 SRW_TAC[][replace_projected_single_def]
THEN SRW_TAC[][replace_projected_single_def]
THEN Cases_on `as_1`
THEN SRW_TAC[][replace_projected_dual_def, replace_projected_single_def]
THEN METIS_TAC[no_effectless_act_def, listTheory.LENGTH])

val len_replace_proj_dual = store_thm("len_replace_proj_dual",
``!P_1 P_2  as_1 as_2 as. 
    LENGTH (replace_projected_dual( P_1, P_2, as, as_1, as_2))
                          <= LENGTH(as_1) + LENGTH(as_2)``,
NTAC 2 STRIP_TAC
THEN Induct_on `as`
THEN SRW_TAC[][replace_projected_dual_def]
THEN Cases_on `as_1` 
THEN Cases_on `as_2`
THEN1(SRW_TAC[][replace_projected_dual_def, replace_projected_single_def])
THEN1(SRW_TAC[][replace_projected_dual_def, replace_projected_single_def]
      THEN METIS_TAC[len_replace_proj_single_le, no_effectless_act_def, listTheory.LENGTH])
THEN1(SRW_TAC[][replace_projected_dual_def, replace_projected_single_def]
      THEN METIS_TAC[len_replace_proj_single_le, no_effectless_act_def, listTheory.LENGTH])
THEN1(SRW_TAC[][replace_projected_dual_def, replace_projected_single_def]
      THEN1(`LENGTH
             (replace_projected_dual
                 (P_1,P_2,as,t,
                     t')) ≤  (LENGTH t) +  (LENGTH t')` by METIS_TAC[len_replace_proj_single_le, no_effectless_act_def, listTheory.LENGTH]
            THEN DECIDE_TAC)
      THEN1(`LENGTH
              (replace_projected_dual
                  (P_1, P_2, as, t,
                     h''::t')) ≤  (LENGTH t) + SUC (LENGTH t')` by METIS_TAC[len_replace_proj_single_le,no_effectless_act_def, listTheory.LENGTH]
           THEN DECIDE_TAC)
      THEN1(`LENGTH
                (replace_projected_dual
                    (P_1, P_2,as,h'::t,
                       t')) ≤  SUC (LENGTH t) + (LENGTH t')` by METIS_TAC[len_replace_proj_single_le,no_effectless_act_def, listTheory.LENGTH]
            THEN DECIDE_TAC)
      THEN1( METIS_TAC[len_replace_proj_single_le,no_effectless_act_def, listTheory.LENGTH])))

val replace_proj_single_sublist = store_thm("replace_proj_single_sublist",
``!P as' as. sublist
  (replace_projected_single( P, as, as')) as``,
STRIP_TAC
THEN Induct_on `as`
THEN1 SRW_TAC[][replace_projected_single_def, sublist_def]
THEN Cases_on `as'`
THEN SRW_TAC[][replace_projected_single_def, sublist_def]
THEN METIS_TAC[sublist_cons])

val replace_proj_dual_sublist = store_thm("replace_proj_dual_sublist",
``!P_1 P_2 as' as'' as. sublist
   (replace_projected_dual( P_1, P_2, as, as',as'')) as``,
NTAC 2 STRIP_TAC
THEN Induct_on `as`
THEN SRW_TAC[][replace_projected_dual_def]
THEN Cases_on `as'` 
THEN Cases_on `as''`
THEN SRW_TAC[][replace_projected_dual_def, sublist_def, replace_projected_single_def]
THEN METIS_TAC[replace_proj_single_sublist, sublist_cons])

val replace_proj_dual_comm = store_thm("replace_proj_dual_comm",
``!P_1 P_2 as' as'' as.
     replace_projected_dual (P_1, P_2, as, as',as'') 
           = replace_projected_dual (P_2, P_1, as, as'',as') ``,
NTAC 2 STRIP_TAC
THEN Induct_on `as`
THEN SRW_TAC[][replace_projected_dual_def]
THEN Cases_on `as'` 
THEN Cases_on `as''`
THEN SRW_TAC[][replace_projected_dual_def, sublist_def, replace_projected_single_def]
THEN FULL_SIMP_TAC(srw_ss())[])

val full_ass_def = Define`full_ass(PROB, vs)
 <=> !a. a IN PROB ==> vs SUBSET FDOM (FST a)`

val permutable_def = 
Define
`permutable(PROB)
      <=> ( !s as' as'' as . as IN (valid_plans PROB) /\ sublist as' as /\ sublist as'' as /\ sat_precond_as(s, as')
                          /\ sat_precond_as(s, as'') /\ sat_precond_as(s, as)
                          ==> sat_precond_as(s,
                                 (replace_projected_dual
                                         ((\ (a_1, a_2). a_1 = a_2),
                                          (\ (a_1, a_2). a_1 = a_2),
                                          as, as', as''))))
         /\ full_ass(PROB, prob_dom PROB)`;


val in_ass_union_def = 
Define`in_ass_union(s,s',s'') <=> !x. x IN FDOM s'' ==> ((s'' ' x = s ' x) \/ (s'' ' x = s' ' x))`


val replace_proj_dual_pred_eq = store_thm("replace_proj_dual_pred_eq",
``!as as'. sublist as' as
           ==> (replace_projected_single((\(a_1,a_2). a_1 = a_2), as, as') = as')``,
Induct_on `as`
THEN1 SRW_TAC[][replace_projected_single_def]
THEN Cases_on `as'`
THEN SRW_TAC[][replace_projected_single_def]
THEN FULL_SIMP_TAC(srw_ss())[sublist_def]
THEN METIS_TAC[sublist_cons_3])

val replace_proj_dual_pred_eq_2 = store_thm("replace_proj_dual_pred_eq_2",
``!h h' as' as. ~(h = h') /\ sublist (h'::as') as
                ==> (replace_projected_dual
                       ((\ (a_1, a_2). a_1 = a_2),
                        (\ (a_1, a_2). a_1 = a_2),
                        h::as, h'::as', [h]) = h::h'::as')``,
SRW_TAC[][replace_projected_dual_def]
THEN Cases_on `as`
THEN1 FULL_SIMP_TAC(srw_ss())[sublist_def]
THEN SRW_TAC[][replace_projected_dual_def]
THEN METIS_TAC[replace_proj_dual_pred_eq])

val full_ass_sat_preconds_eq_state = store_thm("full_ass_sat_preconds_eq_state",
``!PROB s s' as.
     ~(as = []) /\ full_ass(PROB, prob_dom PROB)
     /\ as IN (valid_plans PROB) /\ (s IN (valid_states PROB))
     /\ (s' IN (valid_states PROB))
     /\ sat_precond_as(s, as) /\ sat_precond_as(s', as)
     ==> (s = s')``,
SRW_TAC[][]
THEN Cases_on `as`
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[sat_precond_as_def, full_ass_def, valid_states_def, valid_states_def]
THEN METIS_TAC[valid_plan_valid_head, SUBSET_ANTISYM, FDOM_pre_subset_prob_dom_pair, SET_EQ_SUBSET, EQ_FDOM_SUBMAP])

val in_ass_union_comm = store_thm("in_ass_union_comm",
``!s s' s''. in_ass_union(s, s', s'') = in_ass_union(s', s, s'')``,
SRW_TAC[][in_ass_union_def]
THEN METIS_TAC[])

val replace_proj_dual_cons_eq = store_thm("replace_proj_dual_cons_eq",
``!h h' as as' as''.
      (replace_projected_single((\(a_1,a_2). a_1 = a_2),as,h::as') = h'::as'')
      ==> (h = h')``,
Induct_on `as` 
THEN SRW_TAC[][replace_projected_single_def]
THEN METIS_TAC[])

val replace_proj_dual_eq_append = store_thm("replace_proj_dual_eq_append",
``!h h' h'' t as as' as''.
   (replace_projected_dual
       ((\(a_1,a_2). a_1 = a_2),(\(a_1,a_2). a_1 = a_2), as, h'::as', h''::as'') = h::t)
   ==> (?as_dis as_new. (as = as_dis ++ [h] ++ as_new) /\ ~(MEM h' as_dis) /\ ~(MEM h'' as_dis))``,
Induct_on `as`
THEN1(SRW_TAC[][replace_projected_dual_def])
THEN1(SRW_TAC[][replace_projected_dual_def]
     THEN1(Q.EXISTS_TAC `[]`
           THEN SRW_TAC[][])
     THEN1(Q.EXISTS_TAC `[]`
           THEN SRW_TAC[][])
     THEN1(Q.EXISTS_TAC `[]`
           THEN SRW_TAC[][])
     THEN1(FULL_SIMP_TAC(srw_ss())[]
           THEN Q.PAT_X_ASSUM `!x y z a b c. P` (MP_TAC o Q.SPECL[`h'`,`h''`,`h'''`,`t`,`as'`,`as''`])
           THEN SRW_TAC[][]
           THEN Q.EXISTS_TAC `h::as_dis`
           THEN Q.EXISTS_TAC `as_new`
           THEN SRW_TAC[][])))

val replace_proj_dual_cons_eq_or_head = store_thm("replace_proj_dual_cons_eq_or_head",
``!h h' h'' t as as' as''.
    (replace_projected_dual
         ((\(a_1,a_2). a_1 = a_2),(\(a_1,a_2). a_1 = a_2),as,h'::as',h''::as'') =  h::t)
    ==> ((h = h') \/ (h = h''))``,
Induct_on `as`
THEN1(SRW_TAC[][replace_projected_dual_def])
THEN1(SRW_TAC[][replace_projected_dual_def]
      THEN FULL_SIMP_TAC(srw_ss())[]
      THEN METIS_TAC[]))

val replace_proj_dual_eq_append_or_head = store_thm("replace_proj_dual_eq_append_or_head",
``!h h' h'' t as as' as''.
   (replace_projected_dual
       ((\(a_1,a_2). a_1 = a_2),(\(a_1,a_2). a_1 = a_2),as,h'::as',h''::as'') = h::t)
   ==> (?as_dis as_new.
            (as = as_dis ++ [h] ++ as_new) /\ ~(MEM h' as_dis)
            /\ ~(MEM h'' as_dis)) /\ ((h = h') \/ (h = h''))``,
METIS_TAC[replace_proj_dual_eq_append, replace_proj_dual_cons_eq_or_head])

val replace_proj_dual_eq_rpd_append = store_thm("replace_proj_dual_eq_rpd_append",
``!h t as_dis as as' as''. ~(MEM h as_dis)
     /\ (replace_projected_dual
         ((\(a_1,a_2). a_1 = a_2),(\(a_1,a_2). a_1 = a_2),as_dis ++ as,as',as'') = h::t )
        ==>  (replace_projected_dual
                    ((\(a_1,a_2). a_1 = a_2),(\(a_1,a_2). a_1 = a_2),as_dis ++ as,as',as'') =
              replace_projected_dual
                    ((\(a_1,a_2). a_1 = a_2),(\(a_1,a_2). a_1 = a_2), as,as',as'') )``,
Induct_on `as_dis ++ as`
THEN SRW_TAC[][replace_projected_dual_def]
THEN Cases_on `as_dis`
THEN1 (Cases_on `as`
      THEN FULL_SIMP_TAC(srw_ss())[])
THEN FULL_SIMP_TAC(srw_ss())[]
THEN Q.PAT_X_ASSUM `!x y. P` (MP_TAC o Q.SPECL[`t'`, `as`])
THEN SRW_TAC[][]
THEN Q.PAT_X_ASSUM `!a b c d. P` (MP_TAC o Q.SPECL[`h'`, `t`, `as'`, `as''`])
THEN SRW_TAC[][]
THEN `(replace_projected_dual
        ((\(a_1,a_2). a_1 = a_2),(\(a_1,a_2). a_1 = a_2), t' ++ as,
         as',as'') = h'::t)`
       by (Cases_on `as'`
          THEN Cases_on `as''`
          THEN1 FULL_SIMP_TAC(srw_ss())[replace_projected_dual_def, replace_projected_single_def]
          THEN1(FULL_SIMP_TAC(srw_ss())[replace_projected_dual_def, replace_projected_single_def]
                THEN Cases_on `h = h''`
                THEN1(FULL_SIMP_TAC(srw_ss())[])
                THEN1(FULL_SIMP_TAC(srw_ss())[]
                      THEN Cases_on `t' ++ as`
                      THEN1(FULL_SIMP_TAC(srw_ss())[replace_projected_dual_def, replace_projected_single_def])
                      THEN1(FULL_SIMP_TAC(srw_ss())[replace_projected_dual_def, replace_projected_single_def])))
          THEN1(FULL_SIMP_TAC(srw_ss())[replace_projected_dual_def, replace_projected_single_def]
                THEN Cases_on `h = h''`
                THEN1(FULL_SIMP_TAC(srw_ss())[replace_projected_dual_def, replace_projected_single_def])
                THEN1(Cases_on `t'++as`
                      THEN FULL_SIMP_TAC(srw_ss())[replace_projected_dual_def, replace_projected_single_def]))
          THEN1(FULL_SIMP_TAC(srw_ss())[replace_projected_dual_def, replace_projected_single_def]
                THEN Cases_on `(h = h'') /\ (h = h''')`
                THEN FULL_SIMP_TAC(srw_ss())[]
                THEN1(Cases_on `(h'' = h''')`
                      THEN FULL_SIMP_TAC(srw_ss())[])
                THEN1(Cases_on `(h = h''')`
                      THEN FULL_SIMP_TAC(srw_ss())[])
                THEN1(Cases_on `(h = h'')`
                      THEN FULL_SIMP_TAC(srw_ss())[])))
THEN METIS_TAC[])

val full_ass_imp_sat_preonds = store_thm("full_ass_imp_sat_preonds",
``!s s' h as as'.
    full_ass(PROB, prob_dom PROB) /\ (h::as) IN (valid_plans PROB)
    /\ s IN (valid_states PROB) /\ s' IN (valid_states PROB)
    /\ sat_precond_as(s, h::as) /\ sat_precond_as(s', h::as')
    ==> sat_precond_as(s, h::as')``,
SRW_TAC[][]
THEN MP_TAC(full_ass_sat_preconds_eq_state |> Q.SPECL[`PROB`,`s`,`s'`,`[h]`])
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[sat_precond_as_def, valid_plans_def])

val empty_replace_proj_single = store_thm("empty_replace_proj_single",
``!h'  as as'. (replace_projected_single
         ((\(a_1,a_2). a_1 = a_2),as,h'::as') = [])
        ==>  ~(MEM h' as)``,
Induct_on `as`
THEN SRW_TAC[][replace_projected_single_def]
THEN METIS_TAC[])

val empty_replace_proj_dual8 = store_thm("empty_replace_proj_dual8",
``!h' h'' as as' as''. (replace_projected_dual
         ((\(a_1,a_2). a_1 = a_2),(\(a_1,a_2). a_1 = a_2),as,h'::as',h''::as'') = [])
        ==>  (~(MEM h' as) /\ ~(MEM h'' as))``,
Induct_on `as`
THEN SRW_TAC[][replace_projected_dual_def]
THEN METIS_TAC[])

val empty_replace_proj_dual9 = store_thm("empty_replace_proj_dual9",
``!s. in_ass_union(s,s,s)``,
SRW_TAC[][in_ass_union_def])

val ass_union_self = store_thm("ass_union_self",
``!s s'. in_ass_union(s,s',s')``,
SRW_TAC[][in_ass_union_def])

val before_def = Define
`(before(h'::t', h''::t'', h::t) = if (h' = h) then T
                                   else if  ((h'' = h) /\ ~(h' = h)) then F
                                   else before(h'::t', h''::t'', t))
/\ (before([], h''::t'', h::t) = F)
/\ (before(h'::t', h''::t'', []) = F)`

val vtx_cut_main_lemma_xx = store_thm("vtx_cut_main_lemma_xx",
``!PROB as' as'' as s.
         (s IN (valid_states PROB)) /\ as IN (valid_plans PROB) /\ permutable(PROB) /\ sublist as' as /\ sublist as'' as /\ sat_precond_as(s, as')
         /\ sat_precond_as(s, as'') /\ sat_precond_as(s, as)
         ==> in_ass_union(exec_plan(s, as'), exec_plan(s, as''),
                          exec_plan(s, replace_projected_dual
                                          ((\ (a_1, a_2). a_1 = a_2),
                                           (\ (a_1, a_2). a_1 = a_2),
                                           as, as', as'')))``,
NTAC 4 STRIP_TAC
THEN Induct_on `replace_projected_dual ((\ (a_1, a_2). a_1 = a_2),
                                           (\ (a_1, a_2). a_1 = a_2),
                                           as, as', as'')`
THEN1(NTAC 3 STRIP_TAC
      THEN Cases_on `as`
      THEN Cases_on `as'` 
      THEN Cases_on `as''`
      THEN SRW_TAC[][replace_projected_dual_def, replace_projected_single_def, exec_plan_def]
      THEN1 METIS_TAC[empty_replace_proj_dual9]
      THEN1 METIS_TAC[empty_replace_proj_dual9] 
      THEN1 METIS_TAC[sublist_MEM,  ((empty_replace_proj_single) 
                               |> Q.SPECL[`h'`,`t`,`t'`])]
      THEN1 METIS_TAC[sublist_MEM,  ((empty_replace_proj_single) 
                               |> Q.SPECL[`h'`,`t`,`t'`])]
      THEN1 METIS_TAC[sublist_MEM,  ((empty_replace_proj_dual8) 
                               |> Q.SPECL[`h`,`h'`,`as`,`t`,`t'`])])
THEN1(SRW_TAC[][replace_projected_dual_def]
      THEN Q.PAT_X_ASSUM `h::v = x` (ASSUME_TAC o GSYM)
      THEN SRW_TAC[][]
      THEN Cases_on `as'` 
      THEN Cases_on `as''`
      THEN1(Cases_on `as`
            THEN FULL_SIMP_TAC(srw_ss())[replace_projected_dual_def, replace_projected_single_def])
      THEN1(Cases_on `as`
            THEN1 FULL_SIMP_TAC(srw_ss())[replace_projected_dual_def, replace_projected_single_def]
            THEN FULL_SIMP_TAC(srw_ss())[replace_projected_dual_def]
            THEN `h = h'` by METIS_TAC[replace_proj_dual_cons_eq]
            THEN SRW_TAC[][]
            THEN `h::t = h::v ` by METIS_TAC[replace_proj_dual_pred_eq, ass_union_self, sublist_def]
            THEN FULL_SIMP_TAC(srw_ss())[]
            THEN METIS_TAC[ass_union_self])
      THEN1(Cases_on `as`
            THEN1 FULL_SIMP_TAC(srw_ss())[replace_projected_dual_def, replace_projected_single_def]
            THEN FULL_SIMP_TAC(srw_ss())[replace_projected_dual_def, exec_plan_def]
            THEN `h = h'` by METIS_TAC[replace_proj_dual_cons_eq]
            THEN SRW_TAC[][]
            THEN `h::t = h::v ` by METIS_TAC[replace_proj_dual_pred_eq, ass_union_self, sublist_def]
            THEN FULL_SIMP_TAC(srw_ss())[]
            THEN METIS_TAC[ass_union_self, in_ass_union_comm])
      THEN1(MP_TAC((replace_proj_dual_eq_append_or_head |> INST_TYPE[alpha |-> ``:('a |-> 'b) # ('a |-> 'b)``]) |> Q.SPECL[`h`, `h'`, `h''`, `v`, `as`, `t`, `t'`])
            THEN SRW_TAC[][]
            THEN1(MP_TAC((replace_proj_dual_eq_rpd_append |> INST_TYPE[alpha |-> ``:('a |-> 'b) # ('a |-> 'b)``]) |> Q.SPECL[`h`, `v`, `as_dis`, `h::as_new`, `h::t`, `h''::t'`])
                  THEN SRW_TAC[][]
                  THEN FULL_SIMP_TAC(srw_ss())[replace_projected_dual_def]
                  THEN Cases_on `h = h''`
                  THEN FULL_SIMP_TAC(srw_ss())[exec_plan_def]
                  THEN1(Q.PAT_X_ASSUM `!x y z. P` (MP_TAC o Q.SPECL[`as_new`,`t`,`t'`])
                       THEN SRW_TAC[][]
                       THEN Q.PAT_X_ASSUM `!x. P` (MP_TAC o Q.SPECL[`state_succ s h`])
                       THEN SRW_TAC[][]
                       THEN FIRST_X_ASSUM HO_MATCH_MP_TAC
                       THEN SRW_TAC[][]
                       THEN1 (FULL_SIMP_TAC(srw_ss())[valid_plans_def, SUBSET_DEF] THEN METIS_TAC[valid_action_valid_succ])
                       THEN1 (FULL_SIMP_TAC(srw_ss())[valid_plans_def, SUBSET_DEF] THEN METIS_TAC[])
                       THEN1 METIS_TAC[sublist_append_6, sublist_cons_2, CONS_APPEND, APPEND_ASSOC]
                       THEN1 METIS_TAC[sublist_append_6, sublist_cons_2, CONS_APPEND, APPEND_ASSOC]
                       THEN1 FULL_SIMP_TAC(srw_ss())[sat_precond_as_def]
                       THEN1 FULL_SIMP_TAC(srw_ss())[sat_precond_as_def]
                       THEN1 (Q.PAT_X_ASSUM `sat_precond_as (s,as_dis ++ [h] ++ as_new)`
                                                    (ASSUME_TAC o SIMP_RULE(bool_ss)[Once (GSYM APPEND_ASSOC)])
                              THEN FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND]
                              THEN MP_TAC(empty_replace_proj_dual7 |> Q.SPECL[`s`, `as_dis`, `h::as_new`])
                              THEN SRW_TAC[][]
                              THEN `(exec_plan(s, as_dis)) IN (valid_states PROB)` by METIS_TAC[valid_as_valid_exec, valid_append_valid_pref, valid_append_valid_suff]
                              THEN MP_TAC(full_ass_imp_sat_preonds |> Q.SPECL[`s`,`exec_plan (s,as_dis)`,
                                                                             `h`, `t`, `as_new`])
                              THEN SRW_TAC[][]
                              THEN FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND, Once (GSYM APPEND_ASSOC)]
                              THEN `sublist t as_new` by METIS_TAC[sublist_append_6, sublist_cons_2]
                              THEN FULL_SIMP_TAC(srw_ss())[permutable_def]
                              THEN `h::as_new ∈ valid_plans PROB` by METIS_TAC[valid_append_valid_suff |> INST_TYPE[alpha |-> ``:(α |-> β) # (α |-> β)``]]
                              THEN `sat_precond_as (s,h::as_new)` by (FULL_SIMP_TAC(srw_ss())[valid_plans_def] THEN METIS_TAC[sublist_subset, SUBSET_TRANS])
                              THEN METIS_TAC[sat_precond_as_def,
                                             LIST_TO_SET |> INST_TYPE[alpha |-> ``:(α |-> β) # (α |-> β)``, beta |-> ``:(α |-> β) # (α |-> β)``],
                                             valid_append_valid_suff |> INST_TYPE[alpha |-> ``:(α |-> β) # (α |-> β)``],
                                             valid_action_valid_succ |> INST_TYPE[alpha |-> ``:(α |-> β) # (α |-> β)``]]))
                  THEN1(Q.PAT_X_ASSUM `!x y z. P` (MP_TAC o Q.SPECL[`as_new`,`t`,`h''::t'`])
                        THEN SRW_TAC[][]
                        THEN `exec_plan(state_succ s h'',t') = exec_plan(s,h''::t')` by METIS_TAC[exec_plan_def]
                        THEN SRW_TAC[][]
                        THEN `sat_precond_as(state_succ s h, h''::t')`
                                  by (`sat_precond_as(s,
                                             replace_projected_dual
                                               ((\(a_1,a_2). a_1 = a_2),(\(a_1,a_2). a_1 = a_2),h::as_new,
                                               h''::t',[h]))` 
                                         by (Q.PAT_X_ASSUM `permutable PROB` (ASSUME_TAC o SIMP_RULE(srw_ss())[permutable_def])
                                             THEN FULL_SIMP_TAC(bool_ss)[]
                                             THEN FIRST_ASSUM HO_MATCH_MP_TAC
                                             THEN SRW_TAC[][]
                                             THEN1(FULL_SIMP_TAC(bool_ss)[GSYM APPEND_ASSOC, GSYM CONS_APPEND]
                                                   THEN METIS_TAC[sublist_append_6, sublist_def, valid_append_valid_suff |> INST_TYPE[alpha |-> ``:(α |-> β) # (α |-> β)``]])
                                             THEN1(Q.PAT_X_ASSUM `sublist (h''::t') (as_dis ++ [h] ++ as_new)` 
                                                              (ASSUME_TAC o SIMP_RULE(bool_ss)[Once (GSYM APPEND_ASSOC)])
                                                   THEN FULL_SIMP_TAC(bool_ss)[GSYM APPEND_ASSOC, GSYM CONS_APPEND]
                                                   THEN METIS_TAC[sublist_append_6, sublist_def, valid_append_valid_suff |> INST_TYPE[alpha |-> ``:(α |-> β) # (α |-> β)``]])
                                             THEN1 FULL_SIMP_TAC(srw_ss())[sat_precond_as_def]
                                             THEN Q.PAT_X_ASSUM `sat_precond_as (s,as_dis ++ [h] ++ as_new)`
                                                      (ASSUME_TAC o SIMP_RULE(bool_ss)[Once (GSYM APPEND_ASSOC)])
                                             THEN FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND]
                                             THEN MP_TAC(empty_replace_proj_dual7 |> Q.SPECL[`s`, `as_dis`, `h::as_new`])
                                             THEN SRW_TAC[][]
                                             THEN `(exec_plan(s, as_dis)) IN valid_states PROB` by METIS_TAC[valid_as_valid_exec, valid_append_valid_pref]
                                             THEN MP_TAC(full_ass_imp_sat_preonds |> Q.SPECL[`s`,`exec_plan (s,as_dis)`,
                                                                                `h`, `t`, `as_new`])
                                             THEN SRW_TAC[][]
                                             THEN FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND, Once (GSYM APPEND_ASSOC)]
                                             THEN `sublist t as_new` by METIS_TAC[sublist_append_6, sublist_cons_2]
                                             THEN METIS_TAC[sublist_subset, SUBSET_TRANS, sat_precond_as_def, permutable_def,
                                                            LIST_TO_SET, FDOM_state_succ, sublist_valid_plan])
                                    THEN `sublist (h''::t') as_new ` by METIS_TAC[sublist_append_6, sublist_def, CONS_APPEND, APPEND_ASSOC]
                                    THEN MP_TAC(replace_proj_dual_pred_eq_2 |> INST_TYPE [alpha |-> ``:(α |-> β) # (α |-> β)``] |> Q.SPECL[`h`, `h''`, `t'`, `as_new`])
                                    THEN SRW_TAC[][]
                                    THEN FULL_SIMP_TAC(bool_ss)[sat_precond_as_def])
                        THEN `s = state_succ s h` 
                                by (ASSUME_TAC(full_ass_sat_preconds_eq_state |> Q.SPECL[`PROB`, `s`, `state_succ s h`, `h''::t'`])
                                    THEN `(h''::t') IN (valid_plans PROB)`
                                         by (`(as_dis ++ [h] ++ as_new) IN (valid_plans PROB)` by SRW_TAC[][]
                                             THEN METIS_TAC[sublist_subset, SUBSET_TRANS, SUBSET_DEF, sublist_valid_plan])
                                    THEN FIRST_X_ASSUM HO_MATCH_MP_TAC
                                    THEN SRW_TAC[][]
                                    THEN1 FULL_SIMP_TAC(bool_ss)[permutable_def]
                                    THEN1 METIS_TAC[sublist_valid_plan, valid_plan_valid_head, valid_action_valid_succ])
                        THEN `in_ass_union
                                  (exec_plan (state_succ s h,t),exec_plan (state_succ s h,h''::t'),
                                       exec_plan
                                           (state_succ s h,
                                            replace_projected_dual
                                                ((\(a_1,a_2). a_1 = a_2),(\(a_1,a_2). a_1 = a_2),as_new,t,
                                                  h''::t')))`
                          by (FIRST_X_ASSUM MATCH_MP_TAC
                              THEN SRW_TAC[][]
                              THEN1 METIS_TAC[sublist_valid_plan, valid_plan_valid_head, valid_action_valid_succ]
                              THEN1 (FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND, GSYM APPEND_ASSOC] THEN METIS_TAC[valid_plan_valid_tail, valid_append_valid_suff])
                              THEN1 METIS_TAC[sublist_append_6, sublist_def, CONS_APPEND, sublist_cons_2, APPEND_ASSOC]
                              THEN1 METIS_TAC[sublist_append_6, sublist_def, CONS_APPEND, sublist_cons_2, APPEND_ASSOC]
                              THEN1 FULL_SIMP_TAC(srw_ss())[sat_precond_as_def]
                              THEN1(Q.PAT_X_ASSUM `sat_precond_as (s,as_dis ++ [h] ++ as_new)`
                                                      (ASSUME_TAC o SIMP_RULE(bool_ss)[Once (GSYM APPEND_ASSOC)])
                                    THEN FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND]
                                    THEN MP_TAC(empty_replace_proj_dual7 |> Q.SPECL[`s`, `as_dis`, `h::as_new`])
                                    THEN SRW_TAC[][]
                                    THEN `as_dis ∈ valid_plans PROB` by (FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND, GSYM APPEND_ASSOC] THEN METIS_TAC[valid_append_valid_pref])
                                    THEN `(exec_plan(s, as_dis)) IN valid_states PROB` by METIS_TAC[valid_as_valid_exec]
                                    THEN MP_TAC(full_ass_imp_sat_preonds |> Q.SPECL[`s`,`exec_plan (s,as_dis)`,
                                                                          `h`, `t`, `as_new`])
                                    THEN SRW_TAC[][]
                                    THEN FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND, Once (GSYM APPEND_ASSOC)]
                                    THEN `sublist t as_new` by METIS_TAC[sublist_append_6, sublist_cons_2]
                                    THEN METIS_TAC[sublist_subset, SUBSET_TRANS, sat_precond_as_def, permutable_def,
                                                   LIST_TO_SET, sublist_valid_plan]))
                        THEN METIS_TAC[]))
            THEN1(MP_TAC((replace_proj_dual_eq_rpd_append |> INST_TYPE[alpha |-> ``:('a |-> 'b) # ('a |-> 'b)``])
                                         |> Q.SPECL[`h`, `v`, `as_dis`, `h::as_new`, `h'::t`, `h::t'`])
                  THEN SRW_TAC[][]
                  THEN FULL_SIMP_TAC(srw_ss())[replace_projected_dual_def]
                  THEN Cases_on `h = h'`
                  THEN FULL_SIMP_TAC(srw_ss())[exec_plan_def]
                  THEN1(Q.PAT_X_ASSUM `!x y z. P` (MP_TAC o Q.SPECL[`as_new`,`t`,`t'`])
                       THEN SRW_TAC[][]
                       THEN Q.PAT_X_ASSUM `!x. P` (MP_TAC o Q.SPECL[`state_succ s h`])
                       THEN SRW_TAC[][]
                       THEN FIRST_X_ASSUM HO_MATCH_MP_TAC
                       THEN SRW_TAC[][]
                       THEN1 (FULL_SIMP_TAC(srw_ss())[valid_plans_def, SUBSET_DEF] THEN METIS_TAC[valid_action_valid_succ])
                       THEN1 (FULL_SIMP_TAC(srw_ss())[valid_plans_def, SUBSET_DEF] THEN METIS_TAC[])
                       THEN1 METIS_TAC[sublist_append_6, sublist_cons_2, CONS_APPEND, APPEND_ASSOC]
                       THEN1 METIS_TAC[sublist_append_6, sublist_cons_2, CONS_APPEND, APPEND_ASSOC]
                       THEN1 FULL_SIMP_TAC(srw_ss())[sat_precond_as_def]
                       THEN1 FULL_SIMP_TAC(srw_ss())[sat_precond_as_def]
                       THEN1 (Q.PAT_X_ASSUM `sat_precond_as (s,as_dis ++ [h] ++ as_new)`
                                                    (ASSUME_TAC o SIMP_RULE(bool_ss)[Once (GSYM APPEND_ASSOC)])
                              THEN FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND]
                              THEN MP_TAC(empty_replace_proj_dual7 |> Q.SPECL[`s`, `as_dis`, `h::as_new`])
                              THEN SRW_TAC[][]
                              THEN `(exec_plan(s, as_dis)) IN (valid_states PROB)` by METIS_TAC[valid_as_valid_exec, valid_append_valid_pref, valid_append_valid_suff]
                              THEN MP_TAC(full_ass_imp_sat_preonds |> Q.SPECL[`s`,`exec_plan (s,as_dis)`,
                                                                             `h`, `t`, `as_new`])
                              THEN SRW_TAC[][]
                              THEN FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND, Once (GSYM APPEND_ASSOC)]
                              THEN `sublist t as_new` by METIS_TAC[sublist_append_6, sublist_cons_2]
                              THEN FULL_SIMP_TAC(srw_ss())[permutable_def]
                              THEN `h::as_new ∈ valid_plans PROB` by METIS_TAC[valid_append_valid_suff |> INST_TYPE[alpha |-> ``:(α |-> β) # (α |-> β)``]]
                              THEN `sat_precond_as (s,h::as_new)` by (FULL_SIMP_TAC(srw_ss())[valid_plans_def] THEN METIS_TAC[sublist_subset, SUBSET_TRANS])
                              THEN METIS_TAC[sat_precond_as_def,
                                             LIST_TO_SET |> INST_TYPE[alpha |-> ``:(α |-> β) # (α |-> β)``, beta |-> ``:(α |-> β) # (α |-> β)``],
                                             valid_append_valid_suff |> INST_TYPE[alpha |-> ``:(α |-> β) # (α |-> β)``],
                                             valid_action_valid_succ |> INST_TYPE[alpha |-> ``:(α |-> β) # (α |-> β)``]]))
                  THEN1(Q.PAT_X_ASSUM `!x y z. P` (MP_TAC o Q.SPECL[`as_new`,`h'::t`,`t'`])
                        THEN SRW_TAC[][]
                        THEN `exec_plan(state_succ s h',t) = exec_plan(s,h'::t)` by METIS_TAC[exec_plan_def]
                        THEN SRW_TAC[][]
                        THEN `sat_precond_as(state_succ s h, h'::t)`
                                  by (`sat_precond_as(s,
                                             replace_projected_dual
                                               ((\(a_1,a_2). a_1 = a_2),(\(a_1,a_2). a_1 = a_2),h::as_new,
                                               h'::t,[h]))` 
                                         by (
                                             Q.PAT_X_ASSUM `permutable PROB` (ASSUME_TAC o SIMP_RULE(srw_ss())[permutable_def])
                                             THEN FULL_SIMP_TAC(bool_ss)[]
                                             THEN FIRST_X_ASSUM HO_MATCH_MP_TAC
                                             THEN SRW_TAC[][]
                                             THEN1(FULL_SIMP_TAC(bool_ss)[GSYM APPEND_ASSOC, GSYM CONS_APPEND]
                                                   THEN METIS_TAC[sublist_append_6, sublist_def, valid_append_valid_suff |> INST_TYPE[alpha |-> ``:(α |-> β) # (α |-> β)``]])
                                             THEN1(Q.PAT_X_ASSUM `sublist (h'::t) (as_dis ++ [h] ++ as_new)` 
                                                              (ASSUME_TAC o SIMP_RULE(bool_ss)[Once (GSYM APPEND_ASSOC)])
                                                   THEN METIS_TAC[sublist_append_6, sublist_def, CONS_APPEND])
                                             THEN1 FULL_SIMP_TAC(srw_ss())[sat_precond_as_def]
                                             THEN Q.PAT_X_ASSUM `sat_precond_as (s,as_dis ++ [h] ++ as_new)`
                                                      (ASSUME_TAC o SIMP_RULE(bool_ss)[Once (GSYM APPEND_ASSOC)])
                                             THEN FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND]
                                             THEN MP_TAC(empty_replace_proj_dual7 |> Q.SPECL[`s`, `as_dis`, `h::as_new`])
                                             THEN SRW_TAC[][]
                                             THEN `(exec_plan(s, as_dis)) IN valid_states PROB` by METIS_TAC[valid_as_valid_exec, valid_append_valid_pref]
                                             THEN MP_TAC(full_ass_imp_sat_preonds |> Q.SPECL[`s`,`exec_plan (s,as_dis)`,
                                                                                `h`, `t'`, `as_new`])
                                             THEN SRW_TAC[][]
                                             THEN FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND, Once (GSYM APPEND_ASSOC)]
                                             THEN `sublist t' as_new` by METIS_TAC[sublist_append_6, sublist_cons_2]
                                             THEN METIS_TAC[sublist_subset, SUBSET_TRANS, sat_precond_as_def, permutable_def,
                                                            LIST_TO_SET, FDOM_state_succ, sublist_valid_plan])
                                    THEN `sublist (h'::t) as_new ` by METIS_TAC[sublist_append_6, sublist_def, CONS_APPEND, APPEND_ASSOC]
                                    THEN MP_TAC((replace_proj_dual_pred_eq_2 |> INST_TYPE[alpha |-> ``:('a |-> 'b) # ('a |-> 'b)``])  |> Q.SPECL[`h`, `h'`, `t`, `as_new`])
                                    THEN SRW_TAC[][]
                                    THEN FULL_SIMP_TAC(bool_ss)[sat_precond_as_def])
                        THEN `s = state_succ s h` 
                                by (ASSUME_TAC(full_ass_sat_preconds_eq_state |> Q.SPECL[`PROB`, `s`, `state_succ s h`, `h'::t`])
                                    THEN `(h'::t) IN (valid_plans PROB)`
                                         by (`(as_dis ++ [h] ++ as_new) IN (valid_plans PROB)` by SRW_TAC[][]
                                             THEN METIS_TAC[sublist_subset, SUBSET_TRANS, SUBSET_DEF, sublist_valid_plan])
                                    THEN FIRST_X_ASSUM HO_MATCH_MP_TAC
                                    THEN SRW_TAC[][]
                                    THEN1 FULL_SIMP_TAC(bool_ss)[permutable_def]
                                    THEN1 METIS_TAC[sublist_valid_plan, valid_plan_valid_head, valid_action_valid_succ])
                        THEN `in_ass_union
                                  (exec_plan (state_succ s h,h'::t) ,exec_plan (state_succ s h,t'),
                                       exec_plan
                                           (state_succ s h,
                                            replace_projected_dual
                                                ((\(a_1,a_2). a_1 = a_2),(\(a_1,a_2). a_1 = a_2),as_new,h'::t,
                                                  t')))`
                          by (FIRST_X_ASSUM MATCH_MP_TAC
                              THEN SRW_TAC[][]
                              THEN1 METIS_TAC[sublist_valid_plan, valid_plan_valid_head, valid_action_valid_succ]
                              THEN1 (FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND, GSYM APPEND_ASSOC] THEN METIS_TAC[valid_plan_valid_tail, valid_append_valid_suff])
                              THEN1 METIS_TAC[sublist_append_6, sublist_def, CONS_APPEND, sublist_cons_2, APPEND_ASSOC]
                              THEN1 METIS_TAC[sublist_append_6, sublist_def, CONS_APPEND, sublist_cons_2, APPEND_ASSOC]
                              THEN1 FULL_SIMP_TAC(srw_ss())[sat_precond_as_def]
                              THEN Q.PAT_X_ASSUM `sat_precond_as (s,as_dis ++ [h] ++ as_new)`
                                        (ASSUME_TAC o SIMP_RULE(bool_ss)[Once (GSYM APPEND_ASSOC)])
                              THEN FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND]
                              THEN MP_TAC(empty_replace_proj_dual7 |> Q.SPECL[`s`, `as_dis`, `h::as_new`])
                              THEN MP_TAC(full_ass_imp_sat_preonds |> Q.SPECL[`s`,`exec_plan (s,as_dis)`,
                                                                          `h`, `t'`, `as_new`])
                              THEN SRW_TAC[][]
                              THEN SRW_TAC[][]
                              THEN `as_dis ∈ valid_plans PROB` by (FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND, GSYM APPEND_ASSOC] THEN METIS_TAC[valid_append_valid_pref])
                              THEN `(exec_plan(s, as_dis)) IN valid_states PROB` by METIS_TAC[valid_as_valid_exec]
                              THEN SRW_TAC[][]
                              THEN FULL_SIMP_TAC(bool_ss)[GSYM CONS_APPEND, Once (GSYM APPEND_ASSOC)]
                              THEN `sublist t' as_new` by METIS_TAC[sublist_append_6, sublist_cons_2]
                              THEN METIS_TAC[sublist_subset, SUBSET_TRANS, sat_precond_as_def, permutable_def,
                                                   LIST_TO_SET, sublist_valid_plan])
                        THEN METIS_TAC[])))
))

val _ = export_theory();


