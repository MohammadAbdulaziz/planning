open HolKernel Parse boolLib bossLib;
open pred_setTheory
open rich_listTheory;
open listTheory;
open arithmeticTheory;
open lcsymtacs

val _ = new_theory "list_utils";

val LENGTH_0 = store_thm("LENGTH_0",
``(LENGTH l = 0) = (l = [])``,
SRW_TAC[][])

val IS_SUFFIX_MEM = store_thm(
  "IS_SUFFIX_MEM",
  ``IS_SUFFIX s (h::t) ==> MEM h s``,
  SRW_TAC [][rich_listTheory.IS_SUFFIX_APPEND] THEN SRW_TAC[][]);

val IS_PREFIX_MEM = store_thm(
  "IS_PREFIX_MEM",
  ``!y. x <<= y ==> !e. MEM e x ==> MEM e y``,
  Induct_on `x` THEN SRW_TAC [][] THEN Cases_on `y` THEN FULL_SIMP_TAC (srw_ss()) []);

val MEM_LAST' = store_thm(
  "MEM_LAST'",
  ``seq <> [] ==> MEM (LAST seq) seq``,
  Cases_on `seq` THEN SRW_TAC[][rich_listTheory.MEM_LAST]);

val neq_len_imp_neq = store_thm("neq_len_imp_neq",
``!l1 l2. (LENGTH(l1) >(LENGTH(l2))) ==> ~(l1 =l2)``,
REPEAT STRIP_TAC THEN FULL_SIMP_TAC (srw_ss()) []);

val LENGTH_INJ_PREFIXES = store_thm(
  "LENGTH_INJ_PREFIXES",
  ``!x1 x2. x1 <<= y /\ x2 <<= y ==> ((LENGTH x1 = LENGTH x2) <=> (x1 = x2))``,
  Induct_on `y` THEN SRW_TAC [][rich_listTheory.IS_PREFIX_NIL] THEN
  Cases_on `x1` THEN Cases_on `x2` THEN FULL_SIMP_TAC (srw_ss()) []);

val nempty_has_front = store_thm ("in_state_set_imp_eq_exec_prefix_4",
``! l. l<>[] ==> FRONT l <<= l``,
Induct_on`l`
THEN1 SRW_TAC[][]
THEN SRW_TAC[][]
THEN Cases_on`l`
THEN1 SRW_TAC[][]
THEN SRW_TAC[][]);

val non_empty_list = store_thm("non_empty_list",
``!l. l <> [] ==> ?h t. l = h :: t``,
Cases_on `l`
THEN SRW_TAC[][])

val len_ge_0 = store_thm("len_ge_0",
``!l. LENGTH l >= 0``,
Induct_on`l`
THEN SRW_TAC[][]);

val len_gt_pref_is_pref = store_thm("len_gt_pref_is_pref",
``! l l1 l2. (LENGTH(l2) > LENGTH(l1)) /\ (l1 <<= l) /\ (l2 <<= l) ==> (l1 <<= l2)``,
Induct_on`l2`
THEN1 SRW_TAC[ARITH_ss][]
THEN Induct_on`l1`
THEN SRW_TAC[][]
THENL
[
	Cases_on`l`
	THEN SRW_TAC[][]
	THEN FULL_SIMP_TAC(srw_ss()) []
	,
	Cases_on`l`
	THEN FULL_SIMP_TAC(srw_ss()) []
	THEN `LENGTH(l2)>LENGTH(l1)` by METIS_TAC[LESS_MONO_REV, GREATER_DEF]
	THEN Cases_on`t`
	THEN1 METIS_TAC[IS_PREFIX_NIL]
	THEN `(l2  <> [])` by
	     (SRW_TAC[][]
	     THEN `LENGTH l1 >= 0` by METIS_TAC [len_ge_0]
	     THEN `LENGTH l2 > 0` by DECIDE_TAC
	     THEN METIS_TAC[LENGTH_NOT_NULL, NULL_DEF, GREATER_DEF])
        THEN `h'''::t' <> []` by SRW_TAC[][]
	THEN METIS_TAC[]
]);

val nempty_list_append_length_add = store_thm("nempty_list_append_length_add",
``!l1 l2 l3. (l2 <> []) ==> LENGTH(l1++l3) < LENGTH(l1++l2++l3)``,
Induct_on`l2`
THEN SRW_TAC[][]);


val list_set_subset_mem_in = store_thm("list_set_subset_mem_in",
``!as s. set as SUBSET s ==> (!a. MEM a as ==> a IN s)``,
Induct_on `as` 
THEN SRW_TAC[][]
THEN SRW_TAC[][]);

val twosorted_list_length = store_thm(
  "twosorted_list_length",
  ``!P1 P2 l k1 k2.
      (∀x. MEM x l ⇒ (~P1 x <=> P2 x)) ∧ LENGTH (FILTER P1 l) < k1 ∧
      (∀l'. (?pfx sfx. pfx ++ l' ++ sfx = l) /\ (∀x. MEM x l' ⇒ P2 x) ⇒ LENGTH l' < k2)
     ==>
      LENGTH l < k1 * k2``,
  NTAC 3 GEN_TAC >> Induct_on `FILTER P1 l`
  >- (simp[listTheory.FILTER_EQ_NIL] >> rpt strip_tac >>
      fs[listTheory.EVERY_MEM] >>
      first_x_assum (qspec_then `l` mp_tac) >> strip_tac >>
      `LENGTH l < k2` suffices_by
        (`k2 ≤ k1 * k2` by simp[] >> decide_tac) >>
      first_x_assum match_mp_tac >> simp[] >>
      rpt (qexists_tac `[]`) >> simp[]) >>
  map_every qx_gen_tac [`h`, `P1`, `l`] >>
  simp[listTheory.FILTER_EQ_CONS] >>
  disch_then (qx_choosel_then [`pfx`, `sfx`] strip_assume_tac) >>
  simp[DISJ_IMP_THM, FORALL_AND_THM, listTheory.FILTER_APPEND_DISTRIB] >>
  map_every qx_gen_tac [`k1`, `k2`] >> rpt strip_tac >> fs[] >>
  first_x_assum (qspecl_then [`P1`, `sfx`] mp_tac) >> simp[] >>
  disch_then (qspecl_then [`k1 - 1`, `k2`] mp_tac) >>
  asm_simp_tac (srw_ss() ++ ARITH_ss) [arithmeticTheory.LEFT_SUB_DISTRIB] >>
  strip_tac >>
  `LENGTH pfx < k2`
    by (first_x_assum match_mp_tac >>
        fs[listTheory.FILTER_EQ_NIL, listTheory.EVERY_MEM] >>
        map_every qexists_tac [`[]`, `h::sfx`] >> simp[]) >>
  `LENGTH sfx < k1 * k2 - k2` suffices_by decide_tac >>
  first_x_assum match_mp_tac >> qx_gen_tac `ll` >>
  disch_then
    (CONJUNCTS_THEN2 (qx_choosel_then [`pp`, `ss`] (SUBST_ALL_TAC o SYM))
                     assume_tac) >>
  first_x_assum match_mp_tac >> simp[] >>
  map_every qexists_tac [`pfx ++ [h] ++ pp`, `ss`] >> simp[])

val set_subset_filter_subset = store_thm("set_subset_filter_subset",
``!P l s. set l SUBSET s ==>
       	  set (FILTER P l) SUBSET s``,
 METIS_TAC [LIST_TO_SET_FILTER, INTER_SUBSET, SUBSET_TRANS]);

val append_filter = store_thm("append_filter",
``! f1 f2 as1 as2 p. (as1 ++ as2 = FILTER f1 (MAP f2  p))
                     ==> ?p_1 p_2. (p_1 ++ p_2 = p) /\ (as1 = FILTER f1 (MAP f2 p_1))
                                   /\ (as2 = FILTER f1 (MAP f2 p_2))``,
Induct_on`p`
THEN SRW_TAC[][]
THENL
[
  Cases_on`as1`
  THENL
  [
    FULL_SIMP_TAC(srw_ss())[]
    THEN Q.EXISTS_TAC `[]`
    THEN Q.EXISTS_TAC `h::p`
    THEN SRW_TAC[][]
    ,
    FULL_SIMP_TAC(srw_ss())[]
    THEN `∃p_1 p_2.
            (p_1 ++ p_2 = p) /\ (as2 = FILTER f1 (MAP f2 p_2)) /\ (t = FILTER f1 (MAP f2 p_1))` by METIS_TAC[]
    THEN Q.EXISTS_TAC `h::p_1`
    THEN Q.EXISTS_TAC `p_2`
    THEN SRW_TAC[][]
  ]
  ,	
  `∃p_1 p_2.
      (p_1 ++ p_2 = p) /\ (as2 = FILTER f1 (MAP f2 p_2)) /\ (as1 = FILTER f1 (MAP f2 p_1))` by METIS_TAC[]
  THEN Q.EXISTS_TAC `h::p_1`
  THEN Q.EXISTS_TAC `p_2`
  THEN SRW_TAC[][]
]);

val append_eq_as_proj_1 = store_thm("append_eq_as_proj_1",
``! f1 f2 as1 as2 as3 p. (as1 ++ as2 ++ as3 = FILTER f1 (MAP f2  p)) 
       	      	      	 ==>
			     ?p_1 p_2 p_3. (p_1 ++ p_2 ++ p_3 = p) /\ (as1 = FILTER f1 (MAP f2 p_1)) 
			     	      	   /\ (as2 = FILTER f1 (MAP f2 p_2)) /\ (as3 = FILTER f1 (MAP f2 p_3))``,
SRW_TAC[][]
THEN `∃p_1 p_2.
          (p_1 ++ p_2 = p) /\ ((as1) = FILTER f1 (MAP f2 p_1)) /\ (as2++as3 = FILTER f1 (MAP f2 p_2))` by METIS_TAC[append_filter, APPEND_ASSOC] 
THEN `∃p_a p_b.
          (p_a ++ p_b = p_2) /\ ((as2) = FILTER f1 (MAP f2 p_a)) /\ (as3 = FILTER f1 (MAP f2 p_b))` by METIS_TAC[append_filter] 
THEN Q.EXISTS_TAC `p_1`
THEN Q.EXISTS_TAC `p_a`
THEN Q.EXISTS_TAC `p_b`
THEN SRW_TAC[][]);

val append_filter_light = store_thm("append_filter_light",
``! f1 f2 as1 as2 p. (as1 ++ as2 =
       (MAP f2  p)) ==>
	      ?p_1 p_2. (p_1 ++ p_2 = p) /\ (as1 = (MAP f2 p_1)) /\ (as2 = (MAP f2 p_2))``,
Induct_on`p`
THEN SRW_TAC[][]
THEN Cases_on`as1`
THENL
[
	FULL_SIMP_TAC(srw_ss())[]
	THEN Q.EXISTS_TAC `[]`
	THEN Q.EXISTS_TAC `h::p`
	THEN SRW_TAC[][]
	,
	FULL_SIMP_TAC(srw_ss())[]
	THEN `∃p_1 p_2.
        	   (p_1 ++ p_2 = p) /\ (as2 = (MAP f2 p_2)) /\ (t = (MAP f2 p_1))` by METIS_TAC[]
	THEN Q.EXISTS_TAC `h::p_1`
	THEN Q.EXISTS_TAC `p_2`
	THEN SRW_TAC[][]
]);

val append_eq_as_proj_1_light = store_thm("append_eq_as_proj_1_light",
``! f1 f2 as1 as2 as3 p. (as1 ++ as2 ++ as3 = (MAP f2  p)) 
       	      	      	 ==>
			     ?p_1 p_2 p_3. (p_1 ++ p_2 ++ p_3 = p) /\ (as1 = (MAP f2 p_1)) 
			     	      	   /\ (as2 = (MAP f2 p_2)) /\ (as3 = (MAP f2 p_3))``,
SRW_TAC[][]
THEN `∃p_1 p_2.
          (p_1 ++ p_2 = p) /\ ((as1) = (MAP f2 p_1)) /\ (as2++as3 = (MAP f2 p_2))` by METIS_TAC[append_filter_light, APPEND_ASSOC] 
THEN `∃p_a p_b.
          (p_a ++ p_b = p_2) /\ ((as2) = (MAP f2 p_a)) /\ (as3 = (MAP f2 p_b))` by METIS_TAC[append_filter_light] 
THEN Q.EXISTS_TAC `p_1`
THEN Q.EXISTS_TAC `p_a`
THEN Q.EXISTS_TAC `p_b`
THEN SRW_TAC[][]);

val len_3_list_cat_1 = store_thm("len_3_list_cat_1",
``!l1 l2 l3 P. LENGTH (FILTER P l3) < LENGTH (FILTER P l2)
      ==> LENGTH (FILTER P (l1++l3)) < LENGTH (FILTER P (l1++l2))``,
METIS_TAC[LENGTH_APPEND, FILTER_APPEND_DISTRIB, LT_ADD_LCANCEL]);

val len_3_list_cat_2= store_thm("len_3_list_cat_2",
``!l1 l2 l3 P. LENGTH (FILTER P l3) < LENGTH (FILTER P l2)
      ==> LENGTH (FILTER P (l3++l1)) < LENGTH (FILTER P (l2++l1))``,
SRW_TAC[][LENGTH_APPEND, FILTER_APPEND_DISTRIB, LT_ADD_LCANCEL, ADD_SYM]);

val len_3_list_cat = store_thm("len_3_list_cat",
``!l1 l2 l3 l4 P. LENGTH (FILTER P l2) < LENGTH (FILTER P l3)
      ==> LENGTH (FILTER P (l1++l2++l4)) < LENGTH (FILTER P (l1++l3++l4))``,
SRW_TAC[][LENGTH_APPEND, FILTER_APPEND_DISTRIB, LT_ADD_LCANCEL, ADD_SYM, ADD_ASSOC, len_3_list_cat_1, len_3_list_cat_2]);

val every_len_filter_eq_len = store_thm("every_len_filter_eq_len",
``!l P. EVERY P l ==> (LENGTH(FILTER P l) = LENGTH l)``,
SRW_TAC[][GSYM FILTER_EQ_ID]);

val every_conj_eq_conj_every = store_thm("every_conj_eq_conj_every",
``!P1 P2 l. EVERY P1 l /\ EVERY P2 l = EVERY (\a. P1 a /\ P2 a) l``,
Induct_on `l`
THEN SRW_TAC[][]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN METIS_TAC[]);

val len_filter_p_np_eq = store_thm("len_filter_p_np_eq",
``!P l. ((LENGTH (FILTER (\a. P a) l) + (LENGTH (FILTER (\a. ~P a) l))) = LENGTH l)``,
Induct_on `l`
THEN SRW_TAC[][]
THEN Q.PAT_X_ASSUM `∀P.
             LENGTH (FILTER (λa. P a) l) +
             LENGTH (FILTER (λa. ¬P a) l) =
             LENGTH l` (MP_TAC o Q.SPECL [`P`])
THEN SRW_TAC[][]
THEN DECIDE_TAC)

val mem_p_filter_len_lt = store_thm("mem_p_filter_len_lt",
``!P l. (?x. MEM x l /\ P x) 
        ==> LENGTH(FILTER (\x. ~ (P x)) l) < LENGTH(l)``,
Induct_on`l`
THEN SRW_TAC[][]
THEN ASSUME_TAC (Q.SPEC `l` (Q.SPEC `(λx. ¬P x)` rich_listTheory.LENGTH_FILTER_LEQ))
THEN METIS_TAC[LESS_EQ_IMP_LESS_SUC]);

val FILTER_P_NOT_P_EMPTY = store_thm("FILTER_P_NOT_P_EMPTY",
``!P l. (FILTER (\a. P a) (FILTER (\a. ~P a)  l  )) = []``,
Induct_on`l`
THEN SRW_TAC[][]);

val LENGTH_FILTER_COMPL_FILTER = store_thm("LENGTH_FILTER_COMPL_FILTER",
``!P l. LENGTH (FILTER (\a. P a) (FILTER (\a. ~P a)  l  )) = 0``,
Induct_on`l`
THEN SRW_TAC[][FILTER_P_NOT_P_EMPTY]);

val len_filter_filter_lt_len_filter = store_thm("len_filter_filter_lt_len_filter",
``!P P2 l. (?a. (MEM a l /\ P a )) ==> 
                LENGTH (FILTER (\a. P a) 
                            (FILTER (\a. ~P a /\ P2 a)  l))
                    < LENGTH ((FILTER (\a. P a)  l  ))``,
Induct_on`l`
THEN SRW_TAC[][]
THENL
[
	METIS_TAC[]
	,
	FULL_SIMP_TAC(srw_ss())[]
	THEN `(LENGTH (FILTER (λa. P a) (FILTER (λa. ¬P a ∧ P2 a) l))) <= (LENGTH (FILTER (λa. P a) l))` by METIS_TAC[rich_listTheory.LENGTH_FILTER_LEQ, FILTER_COMM]
     	THEN DECIDE_TAC
	,	
	FULL_SIMP_TAC(srw_ss())[]
	THEN `(LENGTH (FILTER (λa. P a) (FILTER (λa. ¬P a ∧ P2 a) l))) <= (LENGTH (FILTER (λa. P a) l))` by METIS_TAC[rich_listTheory.LENGTH_FILTER_LEQ, FILTER_COMM]
     	THEN DECIDE_TAC
	,
	FULL_SIMP_TAC(srw_ss())[]
	,
	METIS_TAC[]
]);

val filter_empty_every_not = store_thm("filter_empty_every_not",
``!P l. (FILTER (\x. P x) l = []) ⇔ EVERY (λx. ¬P x) l``,
Induct_on `l`
THEN SRW_TAC[][]);

val list_frag_def = Define `list_frag(l, frag) <=> ?pfx sfx. pfx ++ frag ++ sfx = l` ;

val list_frag_rec_def = 
Define `(list_frag_rec(h::l, h'::l', h_orig :: l_original) =
         if(h = h') then
	      list_frag_rec(l, l', h_orig :: l_original)
	 else if (h = h_orig) then 
	      	 list_frag_rec(l, l_original, h_orig :: l_original)
	      else 
	      	 list_frag_rec(l, h_orig :: l_original, h_orig :: l_original)) /\
	(list_frag_rec(l, [], l') = T) /\
	(list_frag_rec([], h::l, l') = F) /\
	(list_frag_rec(l, l', []) = T)` ;

val LIST_FRAG_DICHOTOMY = store_thm("LIST_FRAG_DICHOTOMY",
``!l la x lb P. list_frag(la ++ [x] ++ lb, l) /\ (~ MEM x l) 
       ==>
       	  list_frag(la, l) \/ list_frag(lb, l)``,
SRW_TAC[][list_frag_def]
THEN SRW_TAC[][GSYM EXISTS_OR_THM]
THEN ASSUME_TAC(Q.SPEC `l` (Q.SPEC `x` MEM_SPLIT))
THEN FULL_SIMP_TAC(srw_ss())[]
THEN ASSUME_TAC( Q.SPEC `(l++sfx)` (GEN_ALL APPEND_EQ_APPEND_MID))
THEN FULL_SIMP_TAC(srw_ss())[]
THEN ASSUME_TAC( Q.SPEC `l` (Q.SPEC `sfx`(GEN_ALL APPEND_EQ_APPEND_MID)))
THEN FULL_SIMP_TAC(srw_ss())[]
THEN METIS_TAC[]);

val LIST_FRAG_DICHOTOMY_2 = store_thm("LIST_FRAG_DICHOTOMY_2",
``!l la x lb P. list_frag(la ++ [x] ++ lb, l) /\ (~ P x) /\ EVERY P l
       ==>
       	  list_frag(la, l) \/ list_frag(lb, l)``,
SRW_TAC[][]
THEN Q_TAC SUFF_TAC `!l la x lb P. list_frag(la ++ [x] ++ lb, l) /\ (~ MEM x l) 
       ==>
       	  list_frag(la, l) \/ list_frag(lb, l)`
THENL
[
	SRW_TAC[][]
	THEN FULL_SIMP_TAC(srw_ss())[EVERY_MEM]
	THEN `~MEM x l` by METIS_TAC[]
	THEN METIS_TAC[]
	,
	METIS_TAC[LIST_FRAG_DICHOTOMY]
]);

val len_frag_leq = store_thm("len_frag_leq",
``!l' l. list_frag(l, l') ==> LENGTH (l') <= LENGTH (l)``,
SRW_TAC[][list_frag_def]
THEN SRW_TAC[][]
THEN DECIDE_TAC); 


val replace_every_parent_fragment_with_bound = store_thm("replace_every_parent_fragment_with_bound",
``!Ch k1  Par f s l PProbs PProbl. PProbs(s) /\  PProbl(l)
                              /\ (!l s. PProbs(s) /\  PProbl(l) /\ EVERY Par l ==> ?l'. (f(s, l') = f(s, l)) 
                              /\ (LENGTH l' <= k1) /\ (EVERY Par l') /\ PProbl(l'))
                              /\ (!a l. PProbl (l) /\ MEM a l ==> (Ch a <=> ~Par a))
                              /\ (!s l1 l2. (f(f(s, l1), l2) = f(s, l1 ++ l2)))
                              /\ (!l1 l2. PProbl(l1 ++ l2) <=> (PProbl(l1) /\ PProbl(l2)))
			      /\ (!s l. PProbs(s) /\ PProbl(l) ==> PProbs(f(s, l)))
                         ==>
                      ?l'. (f(s, l') = f(s, l)) /\ (LENGTH (FILTER Ch l') = LENGTH (FILTER Ch l)) 
                                         /\ (!l''. list_frag (l', l'') /\ EVERY Par l''
                                        ==>
                                 LENGTH l'' <= k1) /\ PProbl(l')``,
STRIP_TAC
THEN Induct_on`FILTER Ch l`
THEN SRW_TAC[][]
THENL
[
	FULL_SIMP_TAC(srw_ss())[FILTER_EQ_NIL, list_frag_def]
	THEN FULL_SIMP_TAC(srw_ss())[EVERY_MEM]
 	THEN `∀x. MEM x l ⇒ Par x` by METIS_TAC[] 
	THEN FULL_SIMP_TAC(srw_ss()) [GSYM EVERY_MEM]
 	THEN FIRST_X_ASSUM (Q.SPECL_THEN [`l`, `s`] MP_TAC)
 	THEN SRW_TAC[][]
 	THEN Q.EXISTS_TAC `l''` 
 	THEN SRW_TAC[][]
 	THENL
 	[
		FULL_SIMP_TAC(srw_ss())[EVERY_MEM]
   		THEN `∀e. MEM e l'' ⇒ ~Ch e` by METIS_TAC[]
             	THEN  FULL_SIMP_TAC(srw_ss())[GSYM EVERY_MEM]
                THEN FULL_SIMP_TAC(srw_ss()) [(GSYM FILTER_EQ_NIL)]
        	,
      		FULL_SIMP_TAC(srw_ss())[LENGTH_APPEND]
       		THEN DECIDE_TAC
       ]
       ,
       FULL_SIMP_TAC(srw_ss())[FILTER_EQ_CONS]
       THEN FIRST_ASSUM (Q.SPECL_THEN  [`Ch`, `l2`] MP_TAC)
       THEN SRW_TAC[][]
       THEN FIRST_ASSUM (Q.SPECL_THEN  [`k1`, `Par`, `f`, `f(s, l1++[h])`] MP_TAC)
       THEN SRW_TAC[][]
       THEN `∃l'.
            (f (f (s,l1 ++ [h]),l') = f (f (s,l1 ++ [h]),l2)) ∧
                   (LENGTH (FILTER Ch l') = LENGTH (FILTER Ch l2)) ∧ PProbl(l') /\
                   ∀l''.
			list_frag(l', l'') ∧ EVERY Par l'' ⇒  LENGTH l'' ≤ k1` by 
				      (`PProbs (f (s,l1 ++ [h])) /\ PProbl(l2)` by FULL_SIMP_TAC(srw_ss())[Once (GSYM APPEND_ASSOC)]
                              	      THEN FIRST_X_ASSUM  (Q.SPECL_THEN [`PProbs`, `PProbl`] MP_TAC) 
                                      THEN SRW_TAC[SatisfySimps.SATISFY_ss][]
                               	      THEN Q.EXISTS_TAC `l'` 
                                      THEN SRW_TAC[SatisfySimps.SATISFY_ss][])
       THEN REWRITE_TAC[GSYM APPEND_ASSOC]
       THEN REWRITE_TAC[FILTER_APPEND]
       THEN SRW_TAC[][]
       THEN `?l. (f(s, l) = f(s, l1)) /\ LENGTH(l) <= k1 /\ EVERY Par l /\ PProbl(l)` by 
            (FULL_SIMP_TAC(srw_ss())[]
            THEN `EVERY Par l1` by (FULL_SIMP_TAC(srw_ss())[FILTER_EQ_NIL] THEN FULL_SIMP_TAC(srw_ss())[EVERY_MEM] 
            THEN `∀x. MEM x l1 ⇒ Par x` by METIS_TAC[]) 
            THEN FIRST_ASSUM (Q.SPECL_THEN [`l1`, `s`]  MP_TAC)  
            THEN STRIP_TAC
            THEN FIRST_ASSUM MATCH_MP_TAC
            THEN SRW_TAC[][])
       THEN Q.EXISTS_TAC `l ++ [h] ++ l'`
       THEN SRW_TAC[][]
       THENL
       [
           Q.PAT_X_ASSUM `∀s l1 l2. f (f (s,l1),l2) = f (s,l1 ++ l2)` (MP_TAC o GSYM) THEN STRIP_TAC
           THEN REWRITE_TAC[GSYM APPEND_ASSOC]
           THEN ASM_SIMP_TAC(bool_ss)[] 
           THEN FULL_SIMP_TAC(srw_ss())[]
           ,
           FULL_SIMP_TAC(srw_ss())[EVERY_MEM]
           THEN `∀e. MEM e l ⇒ ~ Ch e` by METIS_TAC[]
           THEN FULL_SIMP_TAC(srw_ss())[GSYM EVERY_MEM]
           THEN FULL_SIMP_TAC(srw_ss())[GSYM FILTER_EQ_NIL]
           THEN REWRITE_TAC[FILTER_APPEND]
           THEN REWRITE_TAC[LENGTH_APPEND]
           THEN SRW_TAC[][]
           , 
           Q.PAT_X_ASSUM `∀a l. PProbl l ∧ MEM a l ⇒ (Ch a ⇔ ¬Par a)` (MP_TAC o Q.SPEC `[h]` o Q.SPEC `h`)
           THEN SRW_TAC[][] 
           THEN MP_TAC((Q.SPEC `Par`
                        (Q.SPEC `l'` 
                         ( Q.SPEC `h` 
                          ( Q.SPEC `l` (Q.SPEC `l''`LIST_FRAG_DICHOTOMY_2))))))
           THEN FULL_SIMP_TAC(srw_ss())[]
           THEN SRW_TAC[][]  
           THENL
           [
		MP_TAC (Q.SPEC`l` (Q.SPEC `l''`  len_frag_leq))
                THEN SRW_TAC[][]
                THEN DECIDE_TAC     
                ,
                SRW_TAC[][]
           ]
           ,
           FULL_SIMP_TAC(bool_ss)[]  
      ]
]);  

val replace_every_parent_fragment_with_bound' = store_thm("replace_every_parent_fragment_with_bound'",
``!k1 k2 s Par Ch  f l PProbs PProbl. PProbs(s) /\ PProbl(l) /\
      	      	       (!l: 'a list s: 'b. PProbs(s) /\ PProbl(l) /\ EVERY Par l ==> ?l'. (f(s, l') = f(s, l)) 
      	      	       	     	       /\ (LENGTH l' <= k1) /\ (EVERY Par l') /\ PProbl(l'))
		       /\ (!l s. PProbs(s) /\ PProbl(l) ==>( ?l'. (f(s, l') = f(s, l)) 
		       	      	 /\ LENGTH (FILTER Ch l') <= k2 /\ PProbl (l')))
		       /\ (∀(a :α) l:'a list. PProbl (l) /\ MEM a l 
		       	       ⇒ ((Ch :α -> bool) a ⇔ ¬(Par :α -> bool) a))
		       /\ (!s l1 l2. (f(f(s, l1), l2) = f(s, l1 ++ l2)))
		       /\ (!l1 l2. PProbl(l1 ++ l2) <=> (PProbl(l1) /\ PProbl(l2)))
		       /\ (!s l. PProbs(s) /\ PProbl(l) ==> PProbs(f(s, l)))
		       ==>
			      ?l'. (f(s, l') = f(s, l)) /\ (LENGTH (FILTER Ch l') <= k2) 
			           /\ (!l''. list_frag (l', l'') /\ EVERY Par l''
				      ==>
					 LENGTH l'' <= k1) /\ PProbl(l')``,
SRW_TAC[][]
THEN Q.PAT_X_ASSUM `∀l s. PProbs s ∧ PProbl l ==> (∃l'. (f (s,l') = f (s,l)) ∧ LENGTH (FILTER Ch l') ≤ k2 /\ PProbl(l'))`
	    	(MP_TAC o Q.SPEC `s` o Q.SPEC `l`)
THEN SRW_TAC[][]
THEN MP_TAC
(Q.SPEC `PProbl`
   (Q.SPEC `PProbs`
      (Q.SPEC `l'` 
         (Q.SPEC `s` 
            (Q.SPEC `f` 
	        (Q.SPEC `Par` (Q.SPEC `k1` (Q.SPEC `Ch` replace_every_parent_fragment_with_bound))))))))
THEN SRW_TAC[][]
THEN `∃l''.
        (f (s,l'') = f (s,l)) ∧
        (LENGTH (FILTER Ch l'') = LENGTH (FILTER Ch l')) ∧
        (∀l'''. list_frag (l'',l''') ∧ EVERY Par l''' ⇒ LENGTH l''' ≤ k1)
	/\ PProbl(l'')` by METIS_TAC[]
THEN Q.EXISTS_TAC `l''`
THEN SRW_TAC[][]);

val EVERY_ABSORB = store_thm("EVERY_ABSORB",
``!P1 P2 l. EVERY P2 l 
      	    ==> (FILTER (\x.  P1 x /\ P2 x) l = FILTER (\x.  P1 x) l)``,
SRW_TAC[][]
THEN REWRITE_TAC[GSYM FILTER_FILTER]
THEN METIS_TAC[FILTER_EQ_ID]);

val len_filter_p_np_zero = store_thm("len_filter_p_np_zero",
``!P1 P2. (!x. P1 x ==> P2 x)
          ==> !l. LENGTH (FILTER (\a. P1 a) (FILTER (\a. ~ P2 a) l)) = 0``,
NTAC 3 STRIP_TAC
THEN Induct_on `l`
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN METIS_TAC[])

val len_filter_p_np_zero_weak = store_thm("len_filter_p_np_zero_weak",
``!P1 P2 P3. (!x. P1 x /\ P2 x ==> P3 x) 
            ==> !l. EVERY P2 l ==> (LENGTH (FILTER (λa. P1 a) (FILTER (λa. ¬P3 a) l)) = 0)``,
NTAC 4 STRIP_TAC
THEN Induct_on `l`
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN METIS_TAC[])

val subset_every_in = store_thm("subset_every_in",
``!l s. set l SUBSET s ==> EVERY (\x. x IN s) l``,
Induct_on `l`
THEN SRW_TAC[][])

val frag_len_filter_le = store_thm("frag_len_filter_le",
``∀P l' l. list_frag (l,l') ⇒ LENGTH (FILTER P l') ≤ LENGTH (FILTER P l)``,
SRW_TAC[][list_frag_def]
THEN SRW_TAC[][FILTER_APPEND]
THEN DECIDE_TAC)

val threesorted_list_length_1 = store_thm("threesorted_list_length_1",
``!l. list_frag(l,l)``,
SRW_TAC[][list_frag_def]
THEN Q.EXISTS_TAC `[]`
THEN Q.EXISTS_TAC `[]`
THEN SRW_TAC[][])

val threesorted_list_length_2 = store_thm("threesorted_list_length_2",
``!l l'. list_frag(l++l',l)``,
SRW_TAC[][list_frag_def]
THEN Q.EXISTS_TAC `[]`
THEN Q.EXISTS_TAC `l'`
THEN SRW_TAC[][])

val threesorted_list_length_3 = store_thm("threesorted_list_length_3",
``!l l' l''. list_frag(l',l) ==> list_frag(l''++l',l)``,
SRW_TAC[][list_frag_def]
THEN Q.EXISTS_TAC `l''++pfx`
THEN Q.EXISTS_TAC `sfx`
THEN SRW_TAC[][])

val threesorted_list_length = store_thm("threesorted_list_length",
``!P1 P2 l k1 k2. LENGTH (FILTER P1 l) <= k1 /\ (!x. MEM x l ==> (P1 x ==> ~P2 x))
                  /\ (!l'. list_frag(l,l') /\ EVERY (\x. ~P1 x) l'
                            ==> LENGTH (FILTER P2 l') <= k2)
                  ==> LENGTH (FILTER P2 l) <= (k1 + 1) * k2``,
Induct_on `FILTER P1 l`
THEN1(SRW_TAC[][FILTER_EQ_NIL]
      THEN REWRITE_TAC[GSYM ADD1]
      THEN SRW_TAC[][MULT]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`l`])
      THEN SRW_TAC[][threesorted_list_length_1]
      THEN DECIDE_TAC)
THEN1(SRW_TAC[][FILTER_EQ_CONS]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`P1`,`l2`])
      THEN FULL_SIMP_TAC(srw_ss())[FILTER_APPEND_DISTRIB]
      THEN SRW_TAC[][]
      THEN `LENGTH (FILTER P2 l1) <= k2` by
           (FIRST_X_ASSUM MATCH_MP_TAC
           THEN SRW_TAC[][]
           THEN1(METIS_TAC[threesorted_list_length_2, APPEND_ASSOC])
           THEN1(FULL_SIMP_TAC(srw_ss())[FILTER_EQ_NIL]))
      THEN `LENGTH (FILTER P2 l2) <= k1*k2` by
           (`1 + LENGTH (FILTER P1 l2) ≤ k1` by METIS_TAC[LENGTH, ADD1, ADD_COMM, ADD_0, ADD_ASSOC]
           THEN `LENGTH (FILTER P1 l2) ≤ k1 − 1` by DECIDE_TAC
           THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`P2`,`k1-1`,`k2`])
           THEN SRW_TAC[][]
           THEN `(∀l'.
                   list_frag (l2,l') ∧ EVERY (λx. ¬P1 x) l' ⇒
                   LENGTH (FILTER P2 l') ≤ k2)` by 
                (SRW_TAC[][]
                THEN FIRST_X_ASSUM MATCH_MP_TAC
                THEN SRW_TAC[][]
                THEN METIS_TAC[threesorted_list_length_3, APPEND_ASSOC])
           THEN `LENGTH (FILTER P2 l2) ≤ (k1 − 1 + 1) * k2` by METIS_TAC[]
           THEN FULL_SIMP_TAC(srw_ss())[ GSYM PRE_SUB1]
           THEN `k1 > 0` by DECIDE_TAC
           THEN FULL_SIMP_TAC(srw_ss())[GSYM ADD1]
           THEN MP_TAC( SUC_PRE |> Q.GEN `m` |> Q.SPEC `k1`)
           THEN SRW_TAC[][]
           THEN FULL_SIMP_TAC(bool_ss)[]
           THEN METIS_TAC[SUC_PRE, GREATER_DEF])
      THEN REWRITE_TAC[GSYM ADD1]
      THEN SRW_TAC[][MULT]
      THEN DECIDE_TAC))

val set_list_works = store_thm("set_list_works",
``!x l. MEM x l <=> x IN set l``,
SRW_TAC[][])

val foldl_with_acc = store_thm("foldl_with_acc",
``!P acc l. FOLDL (\x y. x /\ (P y)) acc l ==> acc``,
STRIP_TAC
THEN Induct_on `l`
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC`acc /\ P h`) 
THEN SRW_TAC[][])

val foldl_conj_with_acc = store_thm("foldl_conj_with_acc",
``!P acc acc' l. (acc ==> acc') /\ FOLDL (\x y. x /\ (P y)) acc l
                  ==> FOLDL (\x y. x /\ (P y)) acc' l``,
STRIP_TAC
THEN Induct_on `l`
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL [`acc /\ P h`, `acc'/\P h`]) 
THEN SRW_TAC[][])

val foldl_conj_with_acc_eq_T = store_thm("foldl_conj_with_acc_eq_T",
``!P acc l. FOLDL (\x y. x /\ (P y)) T l /\ acc
            ==> FOLDL (\x y. x /\ (P y)) acc l ``,
STRIP_TAC
THEN Induct_on `l`
THEN SRW_TAC[][])

val foldl_conj_with_acc_eq_T_cong = store_thm("foldl_conj_with_acc_eq_T_cong",
``!P P' l. (!x. P x ==> P' x) /\ FOLDL (\x y. x /\ (P y)) T l
            ==> FOLDL (\x y. x /\ (P' y)) T l``,
NTAC 2 STRIP_TAC
THEN Induct_on `l`
THEN SRW_TAC[][]
THEN MP_TAC(foldl_with_acc |> Q.SPECL[`P`, `P h`, `l`])
THEN SRW_TAC[][]
THEN MP_TAC(foldl_conj_with_acc |> Q.SPECL[`P'`, `P h`, `l`])
THEN SRW_TAC[][]
THEN METIS_TAC[foldl_conj_with_acc |> Q.SPECL[`P'`] ])

val neq_mem_dichotomy = store_thm("neq_mem_dichotomy",
``!x x' l. MEM x l /\ MEM x' l /\ x <> x'
           ==> ((?pfx mid sfx. l = pfx ++ [x] ++ mid ++ [x'] ++ sfx) \/
                (?pfx mid sfx. l = pfx ++ [x'] ++ mid ++ [x] ++ sfx))``,
NTAC 2 STRIP_TAC
THEN Induct_on `l`
THEN SRW_TAC[][]
THEN1(FULL_SIMP_TAC(bool_ss)[Once MEM_SPLIT_APPEND_first]
      THEN SRW_TAC[][]
      THEN DISJ1_TAC
      THEN Q.EXISTS_TAC `[]`
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN1(FULL_SIMP_TAC(bool_ss)[Once MEM_SPLIT_APPEND_first]
      THEN SRW_TAC[][]
      THEN DISJ2_TAC
      THEN Q.EXISTS_TAC `[]`
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN1(FULL_SIMP_TAC(srw_ss())[]
      THEN1(DISJ1_TAC
            THEN Q.EXISTS_TAC `h::pfx`
            THEN SRW_TAC[][]
            THEN METIS_TAC[])
      THEN1(DISJ2_TAC
            THEN Q.EXISTS_TAC `h::pfx`
            THEN SRW_TAC[][]
            THEN METIS_TAC[])))

val every_cong = store_thm("every_cong",
``!P P' lvs. (!vs. MEM vs lvs /\ P(vs) ==> P'(vs)) /\ EVERY (\vs'. P(vs')) lvs
             ==> EVERY (\vs'. P'(vs')) lvs``,
NTAC 2 STRIP_TAC
THEN Induct_on `lvs`
THEN SRW_TAC[][])

val N_eq_problem_plan_bound_mult_sum_2 = store_thm("N_eq_problem_plan_bound_mult_sum_2",
``!f1 f2 P l. EVERY (\x. ~P x) l ==> ((MAP (\x. if (P x) then f1(x) else f2(x)) l) 
                             = (MAP (\x. f2(x)) l))``,
NTAC 3 STRIP_TAC
THEN Induct_on `l`
THEN SRW_TAC[][])

val filter_true = store_thm("filter_true",
``!l. FILTER (\x. T) l = l``,
Induct_on `l`
THEN SRW_TAC[][])

val nondistinct_repeated_segment = store_thm(
  "nondistinct_repeated_segment",
  ``∀l. ¬ALL_DISTINCT l ⇒ ∃e l1 l2 l3. l = l1 ++ (e::l2) ++ (e::l3)``,
  Induct >> simp[] >> rpt strip_tac
  >- (fs[listTheory.MEM_SPLIT] >> Q.RENAME1_TAC `l = l1 ++ [h] ++ l2` >>
      map_every qexists_tac [`h`, `[]`, `l1`, `l2`] >> simp[]) >>
  res_tac >> Q.RENAME1_TAC `l = l1 ++ e::l2 ++ e::l3` >> Cases_on `h::l = _` >>
  map_every qexists_tac [`e`, `h::l1`, `l2`, `l3`] >> simp[])

val TAKE_isPREFIX = store_thm(
  "TAKE_isPREFIX",
  ``∀m n l. m ≤ n ∧ n ≤ LENGTH l ⇒ TAKE m l <<= TAKE n l``,
  Induct_on `n` >> simp[] >> qx_genl_tac [`m`, `l`] >>
  Cases_on `l` >> simp[] >> Cases_on `m` >> simp[]);

val N_gen_valid_thm_2 = store_thm("N_gen_valid_thm_2",
``!f1 f2 l. (!x. f1 x <= f2 x)
          ==> SUM (MAP f1 l) <= SUM (MAP f2 l)``,
NTAC 2 STRIP_TAC
THEN Induct_on `l`
THEN SRW_TAC[][]
THEN METIS_TAC[LESS_EQ_LESS_EQ_MONO])

val N_super_gen_valid_thm_2 = store_thm("N_super_gen_valid_thm_2",
``!f1 f2 l. (!x. MEM x l ==> f1 x <= f2 x)
          ==> SUM (MAP f1 l) <= SUM (MAP f2 l)``,
NTAC 2 STRIP_TAC
THEN Induct_on `l`
THEN SRW_TAC[][]
THEN METIS_TAC[LESS_EQ_LESS_EQ_MONO])

val MAP_LAMBDA_ABSTRACTION_thm = store_thm("MAP_LAMBDA_ABSTRACTION_thm",
``MAP f l = MAP (\x. f x) l``,
Induct_on `l`
THEN SRW_TAC[][])

val FOLDL_TRUE_eq_EVERY_TRUE = store_thm("FOLDL_TRUE_eq_EVERY_TRUE",
``FOLDL (\x y. x /\ (P y)) T l = EVERY (\y. P y ) l``,
Induct_on `l`
THEN SRW_TAC[][]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1 METIS_TAC[foldl_with_acc]
THEN `(P h) = T` by METIS_TAC[foldl_with_acc]
THEN FULL_SIMP_TAC(srw_ss())[])

val _ = export_theory();
