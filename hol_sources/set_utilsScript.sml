open HolKernel Parse boolLib bossLib;
open pred_setTheory;
open lcsymtacs
open arith_utilsTheory
open arithmeticTheory

val _ = new_theory "set_utils";

val card_union' = store_thm("card_union'",
  ``FINITE s /\ FINITE t /\ DISJOINT s t ==> (CARD (s UNION t) = CARD s + CARD t)``,
  METIS_TAC [DISJOINT_DEF, CARD_EMPTY, arithmeticTheory.ADD_CLAUSES, CARD_UNION]);

val CARD_INJ_IMAGE_2 = store_thm(
  "CARD_INJ_IMAGE_2",
  ``!f s. FINITE s ==> (!x y. ((x IN s) /\ (y IN s)) ==> ((f x = f y) <=> (x = y))) ==>
          (CARD (IMAGE f s) = CARD s)``,
  REWRITE_TAC [GSYM AND_IMP_INTRO] THEN NTAC 2 STRIP_TAC THEN
  Q.ID_SPEC_TAC `s` THEN HO_MATCH_MP_TAC FINITE_INDUCT THEN
  SRW_TAC [][] THEN METIS_TAC[]);

val scc_main_lemma_x = store_thm("scc_main_lemma_x",
``!s t x. x IN s /\ ~(x IN t) ==> ~(s = t)``,
METIS_TAC[])

val inter_diff_contains = store_thm("inter_diff_contains",
``s INTER (s DIFF t)  = (s DIFF t)``,
SRW_TAC[][INTER_DEF, DIFF_DEF, EXTENSION]
THEN METIS_TAC[])

val SUBSET_DIIFF_DISJ = store_thm("SUBSET_DIIFF_DISJ",
``!s t u. s SUBSET t /\ s SUBSET (t DIFF u)
          ==> DISJOINT s u``,
SRW_TAC[][SUBSET_DEF, DISJOINT_DEF, INTER_DEF, EXTENSION]
THEN METIS_TAC[])

val NOT_IN_IMP_NEQ = store_thm("NOT_IN_IMP_NEQ",
``!s S'. ~(s IN S') ==> (!s'. s' IN S' ==> ~(s = s'))``,
FULL_SIMP_TAC(srw_ss())[IN_DEF]
THEN METIS_TAC[])

val DISJ_IN_IMP_NEQ = store_thm("DISJ_IN_IMP_NEQ",
``!vs vs' v v'. DISJOINT vs vs' /\ (v IN vs) /\ (v' IN vs')
                ==> ~(v = v')``,
SRW_TAC[][DISJOINT_DEF, INTER_DEF, EXTENSION]
THEN METIS_TAC[])

val nsubset_imp_neq = store_thm("nsubset_imp_neq",
``!s t. ~(s SUBSET t) ==> ~(s = t)``,
SRW_TAC[][SUBSET_DEF, EXTENSION]
THEN METIS_TAC[])

val subset_bigun_subset_mem = store_thm("subset_bigun_subset_mem",
``!S t. (!x y. x IN S /\ y IN S /\ ~(x = y) ==> DISJOINT x y)
        /\(!y. y IN S /\ ~(t = y) ==> DISJOINT t y)
        /\ t SUBSET BIGUNION S /\ ~(S = {})
        ==> (?u. u IN S /\ t SUBSET u)``,
SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[BIGUNION, SUBSET_DEF, DISJOINT_DEF, INTER_DEF, EXTENSION]
THEN METIS_TAC[])

val eq_imp_subset = store_thm("eq_imp_subset",
``!s t. (s = t) ==> s SUBSET t``,
METIS_TAC[nsubset_imp_neq])

val NEMPTY_SUBSET_NDISJ = store_thm("NEMPTY_SUBSET_NDISJ",
``!s t. ~(s = {}) /\ s SUBSET t ==> ~DISJOINT s t``,
SRW_TAC[][SUBSET_DEF, DISJOINT_DEF, INTER_DEF, EXTENSION]
THEN METIS_TAC[])

val subset_imp_subset_union = store_thm("subset_imp_subset_union",
``!s t u. (s SUBSET t) ==> s SUBSET (t UNION u)``,
SRW_TAC[][SUBSET_DEF, UNION_DEF])

val disj_big_union_not_mem = store_thm("disj_big_union_not_mem",
``!s S. DISJOINT s (BIGUNION S) /\ ~(s = {}) ==> ~(s IN S)``,
SRW_TAC[][DISJOINT_BIGUNION, DISJOINT_DEF, INTER_DEF, EXTENSION, IN_DEF]
THEN METIS_TAC[])

val not_in_imp_empty = store_thm("not_in_imp_empty",
``!s t. (!x. ~(x IN s) ==> ~(x IN t)) ==> (t DIFF s = {})``,
SRW_TAC[][IN_DEF, DIFF_DEF, GSPEC_ETA, EXTENSION]
THEN METIS_TAC[]);

val insert_in = store_thm("insert_in",
``!x s t. (x INSERT s = t) ==> x IN t``,
SRW_TAC[][INSERT_DEF, GSPEC_ETA]
THEN SRW_TAC[][]);

val not_in_del_not_in = store_thm("not_in_del_not_in",
``!s t x y. (!y. ~(y IN s) ==> ~(y IN t)) /\ ~(x IN t)
            ==> (~(y IN (s DELETE x)) ==> ~(y IN t))``,
SRW_TAC[][DELETE_DEF]
THEN METIS_TAC[])

val scc_vs_imp_mem_leaves_del0 = store_thm("scc_vs_imp_mem_leaves_del0",
``!s t u v x. u SUBSET (s UNION t) /\ ~(x IN v) /\ (t DELETE x) SUBSET v
              /\ v SUBSET (u DIFF s)
            ==> (v =  (t DELETE x))``,
SRW_TAC[][SUBSET_DEF, INTER_DEF, DELETE_DEF, EXTENSION, DIFF_DEF]
THEN  METIS_TAC[])

val subset_imp_diff_subset = store_thm("subset_imp_diff_subset",
``!s t u. s SUBSET t ==> (s DIFF u) SUBSET (t DIFF u)``,
SRW_TAC[][SUBSET_DEF, DIFF_DEF, GSPEC_ETA])

val bigunion_diff_eq_diff_bigunion = store_thm("bigunion_diff_eq_diff_bigunion",
``!s t. (!x y. x IN s /\ y IN t /\ ~(x = y) ==> DISJOINT x y)
        ==> (BIGUNION (s DIFF t) = (BIGUNION s) DIFF (BIGUNION t))``,
SRW_TAC[][BIGUNION, DIFF_DEF, DISJOINT_DEF, GSPEC_ETA, INTER_DEF, EMPTY_DEF]
THEN METIS_TAC[])

val eq_funs_eq_images = store_thm("eq_funs_eq_images",
``(!x. x IN s ==> (f1 x = f2 x)) ==> (IMAGE f1 s = IMAGE f2 s)``,
SRW_TAC[][EXTENSION]
THEN METIS_TAC[])

val neq_funs_neq_images = store_thm("neq_funs_neq_images",
``(!x. x IN s ==> (!y. y IN s ==> f1 x <> f2 y)) /\ (?x. x IN s) ==> (IMAGE f1 s <> IMAGE f2 s)``,
SRW_TAC[][EXTENSION]
THEN METIS_TAC[])

val disj_diff = store_thm("disj_diff",
``!s t. DISJOINT (s DIFF t) t``,
SRW_TAC[][DISJOINT_DEF, INTER_DEF, EXTENSION]
THEN METIS_TAC[])

val subset_diff_diff_eq = store_thm("subset_diff_diff_eq",
``!s t. t SUBSET s ==> ((s DIFF (s DIFF t)) = t)``,
SRW_TAC[][SUBSET_DEF, DIFF_DEF, GSPEC_ETA, EXTENSION]
THEN METIS_TAC[])

val two_children_parent_bound_main_lemma_6 = store_thm("two_children_parent_bound_main_lemma_6",
``!vs vs'. DISJOINT vs vs'
                ==> ((vs DIFF vs') = vs)``,
SRW_TAC[][DISJOINT_DEF, DIFF_DEF, INTER_DEF, EXTENSION]
THEN METIS_TAC[])

val SUBSET_ABS = store_thm("SUBSET_ABS",
``!a b. a SUBSET b ==> (a INTER b = a)``,
SRW_TAC[][SUBSET_DEF, INTER_DEF, EXTENSION]
THEN METIS_TAC[]);

val disj_imp_diff_inter_eq_inter = store_thm("disj_imp_diff_inter_eq_inter",
``!s t u. DISJOINT s t ==> (((u DIFF t) INTER s) = u INTER s)``,
SRW_TAC[][DISJOINT_DEF, DIFF_DEF, INTER_DEF, GSPEC_ETA, SPECIFICATION, EXTENSION]
THEN METIS_TAC[])

val disj_diff_eq = store_thm("disj_diff_eq",
``!s t. DISJOINT s t ==> (s DIFF t = s)``,
SRW_TAC[][DISJOINT_DEF, DIFF_DEF, INTER_DEF, EXTENSION]
THEN METIS_TAC[])

val disj_imp_union_diff_union_eq_diff = store_thm("disj_imp_union_diff_union_eq_diff",
``! x y z. DISJOINT x y ==> ((x UNION y) DIFF (x UNION z) = y DIFF z)``,
SRW_TAC[][UNION_DEF, DIFF_DEF, EXTENSION, DISJOINT_DEF]
THEN METIS_TAC[])

val n_bigunion_le_sum_2 = store_thm("n_bigunion_le_sum_2",
``!s t. s SUBSET t ==> BIGUNION s SUBSET BIGUNION t``,
SRW_TAC[][SUBSET_DEF, BIGUNION, EXTENSION]
THEN METIS_TAC[])

val invariant_prob_reachable_states_thm_9 = store_thm("invariant_prob_reachable_states_thm_9",
``!x s. s SUBSET (x INSERT s)``,
SRW_TAC[][SUBSET_DEF, INSERT_DEF])

val invariant_prob_reachable_states_thm_3 = store_thm("invariant_prob_reachable_states_thm_3",
``!P f x s. (!x y. P x /\ P y /\ ~(x = y) ==> ~(f x = f y)) /\ P x /\ ~(x IN s) 
          /\ (!x. x IN s ==> P x)
          ==> ~((f x) IN (IMAGE f s)) ``,
SRW_TAC[][IMAGE_DEF]
THEN METIS_TAC[])

(*Sets of numbers*)

val mems_le_finite = store_thm("mems_le_finite",
``!s k. (!x. x IN s ==> x <= k) ==> FINITE s``,
rpt strip_tac >> match_mp_tac SUBSET_FINITE_I >> qexists_tac `count (k + 1)` >>
simp[SUBSET_DEF, DECIDE ``x < y + 1 ⇔ x ≤ y``]);

val mem_le_imp_MIN_le = store_thm("mem_le_imp_MIN_le",
``!s: ( num -> bool) k: num. (?x. x IN s /\ x <= k) ==> MIN_SET(s) <= k``,
rpt strip_tac >> DEEP_INTRO_TAC MIN_SET_ELIM >> rpt strip_tac >- fs[] >>
metis_tac[DECIDE ``x:num ≤ y ∧ y ≤ z ⇒ x ≤ z``])

val mem_lt_imp_MIN_lt = store_thm("mem_lt_imp_MIN_lt",
 ``!s: ( num -> bool) k: num. (?x. x IN s /\ x < k) ==> MIN_SET(s) < k``,
SRW_TAC[][]
THEN  ASSUME_TAC ((MATCH_MP (AND1_THM) (REWRITE_RULE[EQ_IMP_THM](MEMBER_NOT_EMPTY
		       |> Q.SPEC `s`))) |> INST_TYPE [alpha |-> ``:num``])
THEN `s <> EMPTY` by (FIRST_ASSUM MATCH_MP_TAC THEN Q.EXISTS_TAC `x` THEN SRW_TAC[][])
THEN MP_TAC (MIN_SET_LEM
	    |> Q.SPEC `s`)
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]
THEN Q.PAT_X_ASSUM `∀x. x ∈ s ⇒ MIN_SET s ≤ x` (MP_TAC o Q.SPEC `x`)
THEN SRW_TAC[][]
THEN DECIDE_TAC);

val bound_child_parent_neq_mems_state_set_neq_len = store_thm("bound_child_parent_neq_mems_state_set_neq_len",
``!s k. (!x. x IN s ==> x < k) ==> FINITE s``,
SRW_TAC[][]
THEN `∀x. x ∈ s ⇒ x < k + 1` by (SRW_TAC[][] THEN Q.PAT_X_ASSUM `∀x. x ∈ s ⇒ x < k` (MP_TAC o Q.SPEC `x`) THEN SRW_TAC[][] THEN DECIDE_TAC)
THEN Q.PAT_X_ASSUM  `∀x. x ∈ s ⇒ x < k + 1` (MP_TAC o
     		       	       	       	  	  REWRITE_RULE [Once ((GSYM COUNT_applied) |> Q.SPEC `m` |> Q.SPEC `k + 1` |> Q.GEN `m`)])
THEN STRIP_TAC
THEN MP_TAC (MATCH_MP (AND1_THM) (REWRITE_RULE[EQ_IMP_THM] ((GSYM SUBSET_DEF)
     	   	  |> Q.SPEC `s`
		  |> Q.ISPEC `(count (k + 1))`) ))
THEN FULL_SIMP_TAC(srw_ss())[GSYM IN_DEF]
THEN STRIP_TAC
THEN SRW_TAC[SatisfySimps.SATISFY_ss][FINITE_COUNT, SUBSET_FINITE]);

val bounded_imp_finite = store_thm("bounded_imp_finite",
``!s k. (!x. x IN s ==> x <= k) ==> FINITE s``,
METIS_TAC[LE_LT1, bound_child_parent_neq_mems_state_set_neq_len])

val bound_main_lemma_2 = store_thm("bound_main_lemma_2",
``!s k.  (s <> EMPTY) /\ (!x. x IN s ==> x <= k) ==> MAX_SET(s) <= k ``,
SRW_TAC[][]
THEN MP_TAC (mems_le_finite
			|> Q.SPEC `s`
			|> Q.SPEC `k`)
THEN SRW_TAC[][]
THEN MP_TAC (MAX_SET_DEF
		     |> Q.SPEC `s`)
THEN SRW_TAC[][]);

val bound_on_all_plans_bounds_problem_plan_bound_2 = store_thm("bound_on_all_plans_bounds_problem_plan_bound_2",
``!s k.  (s <> EMPTY) /\ (!x. x IN s ==> x < k) ==> MAX_SET(s) < k ``,
SRW_TAC[][]
THEN MP_TAC (bound_child_parent_neq_mems_state_set_neq_len
			|> Q.SPEC `s`
			|> Q.SPEC `k`)
THEN SRW_TAC[][]
THEN MP_TAC (MAX_SET_DEF
		     |> Q.SPEC `s`)
THEN SRW_TAC[][]);

val MAX_SET_LEQ_MAX_SET = store_thm("MAX_SET_LEQ_MAX_SET",
``s <> EMPTY /\ FINITE t /\ 
  (!x. x IN s ==> (?y. y IN t /\ x <= y)) ==>
  MAX_SET s <= MAX_SET t``,
SRW_TAC[][]
THEN `!x. x IN s ==> x <= MAX_SET t` by
     (SRW_TAC[][]
     THEN METIS_TAC[LESS_EQ_TRANS, in_max_set])
THEN METIS_TAC[bounded_imp_finite, bound_main_lemma_2])

(*Sets of lists*)

val FINITE_ALL_DISTINCT_LISTS = store_thm(
  "FINITE_ALL_DISTINCT_LISTS",
  ``FINITE P ⇒ FINITE { p | ALL_DISTINCT p ∧ set p ⊆ P }``,
  Induct_on `CARD P` >> rpt strip_tac
  >- (`P = ∅` by metis_tac[CARD_EQ_0] >> csimp[]) >>
  `{ p | ALL_DISTINCT p ∧ set p ⊆ P } =
   {[]} ∪ BIGUNION
           (IMAGE (λe. { e :: p0 | p0 | ALL_DISTINCT p0 ∧ set p0 ⊆ P DELETE e })
                  P)`
    by (dsimp[Once EXTENSION] >> qx_gen_tac `p` >>
        `(p = []) ∨ ∃h t. p = h::t` by (Cases_on `p` >> simp[]) >>
        simp[] >> eq_tac >> strip_tac
        >- fs[SUBSET_DEF] >>
        fs[SUBSET_DEF] >> metis_tac[]) >>
  pop_assum SUBST1_TAC >> dsimp[] >>
  rpt strip_tac >> Q.RENAME1_TAC `SUC n = CARD P` >> Q.RENAME1_TAC `e ∈ P` >>
  fs[AND_IMP_INTRO] >>
  `{e::p0 | p0 | ALL_DISTINCT p0 ∧ set p0 ⊆ P DELETE e} =
   IMAGE (λp. e :: p) { p | ALL_DISTINCT p ∧ set p ⊆ P DELETE e}`
    by simp[EXTENSION] >>
  pop_assum SUBST1_TAC >> match_mp_tac IMAGE_FINITE >>
  first_assum match_mp_tac >> simp[])

val DISJOINT_UNION_thm = store_thm("DISJOINT_UNION_thm",
``!s t u. DISJOINT s t /\ DISJOINT s u
          ==> DISJOINT s (t UNION u)``,
SRW_TAC[][DISJOINT_DEF, INTER_DEF, EXTENSION]
THEN METIS_TAC[])

val CARD_IMAGE = store_thm("CARD_IMAGE",
``!f s. FINITE s ==> CARD (IMAGE f s) <= CARD s``,
  REWRITE_TAC [GSYM AND_IMP_INTRO] THEN NTAC 2 STRIP_TAC THEN
  Q.ID_SPEC_TAC `s` THEN HO_MATCH_MP_TAC FINITE_INDUCT THEN
  SRW_TAC [][] THEN
  Cases_on `∃x. (f e = f x) ∧ x ∈ s` THEN1( SRW_TAC[][] THEN DECIDE_TAC)
  THEN SRW_TAC[][])

val in_not_in_neq = store_thm("in_not_in_neq",
``x IN s /\ ~(y IN s) ==> x <> y``,
FULL_SIMP_TAC(srw_ss())[IN_DEF] THEN METIS_TAC[])

val finite_card_bounded = store_thm("finite_card_bounded",
``FINITE s ==> ?k. CARD s <= k``,
METIS_TAC[CARD_SUBSET, SUBSET_REFL])

val PROD_IMAGE_MONO_LESS_EQ = store_thm("PROD_IMAGE_MONO_LESS_EQ",
``FINITE s ==> (!x. x IN s ==> f x <= g x) ==> (Π f s <= Π g s)``,
Induct_on `s`
THEN SRW_TAC[][PROD_IMAGE_THM, DELETE_NON_ELEMENT_RWT]
THEN METIS_TAC[LESS_MONO_MULT2])

val PROD_IMAGE_MONO_LESS_EQ' = store_thm("PROD_IMAGE_MONO_LESS_EQ'",
``FINITE s /\ (!x. x IN s ==> f x <= g x) ==> (Π f s <= Π g s)``,
SRW_TAC[][PROD_IMAGE_MONO_LESS_EQ])

val CARD_SING_REST_INSERT = store_thm("CARD_SING_REST_INSERT",
``FINITE s ==> ~(e IN s) ==> SING (REST (e INSERT s)) ==> SING s``,
STRIP_TAC
THEN SRW_TAC[][SING_IFF_CARD1]
THEN `CARD (e INSERT s) = SUC (CARD s)` by SRW_TAC[][CARD_INSERT]
THEN `FINITE (e INSERT s)` by SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[CARD_REST])

val TWO_SET_CASES = store_thm("TWO_SET_CASES",
``!a b c d. ({a; b} = {c; d}) ==> (((a=c) /\ (b=d))\/((a=d) /\ (b=c)))``,
SRW_TAC[][EXTENSION]
THEN METIS_TAC[])

val INSERT_EQ_TWO_SET = store_thm("INSERT_EQ_TWO_SET",
``~(e IN s) /\ a <> b /\ (e INSERT s = {a; b}) ==> ((s={a})\/(s={b}))``,
SRW_TAC[][EXTENSION]
THEN METIS_TAC[])

val CARD_TWO_HAS_TWO_MEM = store_thm("CARD_TWO_HAS_TWO_MEM",
``FINITE s ==> ((CARD s = 2) = ?x y. x <> y /\ (s = {x; y}))``,
Induct_on `s`
THEN SRW_TAC[][] 
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1(`SING s` by METIS_TAC[SING_IFF_CARD1]
     THEN FULL_SIMP_TAC(bool_ss)[SING_DEF]
     THEN Q.EXISTS_TAC `e`
     THEN Q.EXISTS_TAC `x`
     THEN SRW_TAC[][]
     THEN METIS_TAC[IN_SING])
THEN METIS_TAC[INSERT_EQ_TWO_SET, SING_IFF_CARD1, SING_DEF])

val CHOICE_OF_TWO = store_thm("CHOICE_OF_TWO",
``!a b. (CHOICE {a; b} = a) \/ (CHOICE {a; b} = b)``,
SRW_TAC[][]
THEN Cases_on `(CHOICE {a; b} = a)`
THEN SRW_TAC[][]
THEN `!x. x IN {a; b} ==> ((x = a) \/ (x = b))` by SRW_TAC[][IN_INSERT]
THEN `{a; b} <> EMPTY` by SRW_TAC[][]
THEN METIS_TAC[CHOICE_DEF])

val CHOICE_OF_THREE = store_thm("CHOICE_OF_THREE",
``!a b c. (CHOICE {a; b; c} = a) \/ (CHOICE {a; b; c} = b) \/ (CHOICE {a; b; c} = c)``,
SRW_TAC[][]
THEN Cases_on `(CHOICE {a; b; c} = a)`
THEN SRW_TAC[][]
THEN Cases_on `(CHOICE {a; b; c} = b)`
THEN SRW_TAC[][]
THEN `!x. x IN {a; b; c} ==> ((x = a) \/ (x = b) \/ (x = c))` by SRW_TAC[][IN_INSERT]
THEN `{a; b; c} <> EMPTY` by SRW_TAC[][]
THEN METIS_TAC[CHOICE_DEF])

val CHOICE_AND_REST_OF_TWO = store_thm("CHOICE_AND_REST_OF_TWO",
``!a b. a <> b ==> (((CHOICE {a; b}) = a) = ((REST {a; b}) = {b}))``,
SRW_TAC[][]
THEN `!x. x IN {a; b} ==> ((x = a) \/ (x = b))` by SRW_TAC[][IN_INSERT]
THEN EQ_TAC
THEN SRW_TAC[][REST_DEF, DELETE_INSERT]
THEN `{a; b} = a INSERT {b}` by SRW_TAC[][]
THEN METIS_TAC[IN_SING, ABSORPTION])

val CHOICE_AND_REST_OF_THREE = store_thm("CHOICE_AND_REST_OF_THREE",
``!a b c. a <> b /\ a <> c /\ b <> c ==> (((CHOICE {a; b; c}) = a) = ((REST {a; b; c}) = {b; c}))``,
SRW_TAC[][]
THEN `!x. x IN {a; b; c} ==> ((x = a) \/ (x = b) \/ (x = c))` by SRW_TAC[][IN_INSERT]
THEN EQ_TAC
THEN SRW_TAC[][REST_DEF, DELETE_INSERT]
THEN1 METIS_TAC[]
THEN SRW_TAC[][EXTENSION]
THEN METIS_TAC[])

val SET_TWO_COMM = store_thm("SET_TWO_COMM",
``{a; b} = {b; a}``,
SRW_TAC[][EXTENSION]
THEN METIS_TAC[])

val SET_THREE_COMM = store_thm("SET_THREE_COMM",
``({a; b; c} = {b; a ; c}) /\ ({a; b; c} = {b; c ; a}) /\ ({a; b; c} = {a; c ; b})``,
SRW_TAC[][EXTENSION]
THEN METIS_TAC[])

val CARD_NZERO_EMPTY = store_thm("CARD_NZERO_EMPTY",
``FINITE s ==> (s <> EMPTY) ==> (CARD s > 0)``,
Induct_on `s`
THEN SRW_TAC[][])

(*val ITSET_IAMGE_COMPOSE = store_thm("ITSET_IAMGE_COMPOSE",
``FINITE s ==> (!x y. y <= f x y /\ x <= f x y) ==> (!a b c d. a <= c /\ b <= d ==> (f a b <= f c d /\ f a b <= f d c /\  f b a <= f c d)) ==> (ITSET f (IMAGE g s) x <= ITSET (f o g) s x)``,
``FINITE s ==> (!a b c. a <= c ==> (a <= f b c /\ a <= f c b)) ==> (!a b c d. a <= c /\ b <= d ==> f a b <= f c d) ==> (ITSET f (IMAGE g s) x <= ITSET (f o g) s x)``,
Induct_on `s`
THEN SRW_TAC[][ITSET_THM]
THEN SRW_TAC[][]
THEN1(`s = EMPTY` by
        (`SING (e INSERT s)` by METIS_TAC[SING_IFF_EMPTY_REST,NOT_INSERT_EMPTY]
         THEN FULL_SIMP_TAC(srw_ss())[SING_DEF, IN_DEF]
         THEN FULL_SIMP_TAC(srw_ss())[])
      THEN SRW_TAC[][])
THEN1(`SING s` by
        (`SING (REST (e INSERT s))` by METIS_TAC[SING_IFF_EMPTY_REST,NOT_INSERT_EMPTY]
         THEN METIS_TAC[CARD_SING_REST_INSERT])
      THEN FULL_SIMP_TAC(srw_ss())[SING_DEF]
      THEN ASSUME_TAC(CHOICE_OF_TWO |> Q.SPECL[`e`, `x'`])
      THEN ASSUME_TAC(CHOICE_OF_TWO |> INST_TYPE [alpha |-> ``:num``] |> Q.SPECL[`(g :α -> num) e`, `(g :α -> num) x'`])
      THEN FULL_SIMP_TAC(srw_ss())[]
      THEN1(MP_TAC(CHOICE_AND_REST_OF_TWO |> Q.SPECL [`e`, `x'`])
            THEN SRW_TAC[][])
      THEN1(MP_TAC(CHOICE_AND_REST_OF_TWO |> Q.SPECL [`x'`, `e`])
            THEN FULL_SIMP_TAC(srw_ss())[SET_TWO_COMM]))
THEN1(`CARD s = 2` by
       (`CARD (REST (REST (REST (e INSERT s)))) = 0` by METIS_TAC[CARD_EMPTY]
        THEN `CARD (REST (REST (REST (e INSERT s)))) = CARD (REST (REST (e INSERT s))) - 1` by METIS_TAC[CARD_REST, FINITE_REST, FINITE_INSERT]
        THEN `CARD (REST (REST (e INSERT s))) = CARD (REST (e INSERT s)) - 1`  by METIS_TAC[CARD_REST, FINITE_REST, FINITE_INSERT]
        THEN `CARD (REST (e INSERT s)) = CARD (e INSERT s) - 1` by METIS_TAC[CARD_REST, FINITE_REST, FINITE_INSERT,NOT_INSERT_EMPTY]
        THEN `CARD (REST (REST (e INSERT s))) > 0` by METIS_TAC[CARD_NZERO_EMPTY, NOT_ZERO_LT_ZERO,FINITE_REST, FINITE_INSERT]
        THEN `CARD (e INSERT s) = 3` by DECIDE_TAC
        THEN `CARD (e INSERT s) = SUC (CARD s)` by METIS_TAC[CARD_INSERT]
        THEN DECIDE_TAC)
      THEN MP_TAC(CARD_TWO_HAS_TWO_MEM)
      THEN SRW_TAC[][]
      THEN SRW_TAC[][IMAGE_DEF]
      THEN `(e :α) ≠ (x' :α) /\ (e:'a) ≠ (y :α) /\ (x' :α) ≠ (e :α) /\ (y:'a) ≠ (e :α)` by FULL_SIMP_TAC(srw_ss())[IN_DEF]
      THEN MP_TAC(CHOICE_OF_THREE |> Q.SPECL[`e:'a`, `x':'a`, `y:'a`])
      THEN SRW_TAC[][]
      THEN MP_TAC(CHOICE_OF_THREE |> INST_TYPE [alpha |-> ``:num``] |> Q.SPECL[`(g:'a->num) e`, `(g:'a -> num) x'`, `(g:'a->num) y`])
      THEN SRW_TAC[][]
      THEN MP_TAC(CHOICE_AND_REST_OF_THREE |> Q.SPECL[`e`, `x'`, `y`])
      THEN SRW_TAC[][]
      THEN MP_TAC(CHOICE_AND_REST_OF_THREE |> Q.SPECL[`e`, `y`, `x'`])
      THEN SRW_TAC[][]
      THEN MP_TAC(CHOICE_AND_REST_OF_THREE |> Q.SPECL[`x'`, `e`, `y`])
      THEN SRW_TAC[][]
      THEN MP_TAC(CHOICE_AND_REST_OF_THREE |> Q.SPECL[`x'`, `y`, `e`])
      THEN SRW_TAC[][]
      THEN MP_TAC(CHOICE_AND_REST_OF_THREE |> Q.SPECL[`y`, `x'`, `e`])
      THEN SRW_TAC[][]
      THEN MP_TAC(CHOICE_AND_REST_OF_THREE |> Q.SPECL[`y`, `e`, `x'`])
      THEN SRW_TAC[][]
      THEN ASSUME_TAC(CHOICE_OF_TWO |> Q.SPECL[`x'`, `y`])
      THEN MP_TAC(CHOICE_AND_REST_OF_TWO |> Q.SPECL [`x'`, `y`])
      THEN SRW_TAC[][]
      THEN ASSUME_TAC(CHOICE_OF_TWO |> Q.SPECL[`y`, `x'`])
      THEN MP_TAC(CHOICE_AND_REST_OF_TWO |> Q.SPECL [`y`, `x'`])
      THEN SRW_TAC[][]
      THEN ASSUME_TAC(CHOICE_OF_TWO |> Q.SPECL[`e`, `x'`])
      THEN MP_TAC(CHOICE_AND_REST_OF_TWO |> Q.SPECL [`e`, `x'`])
      THEN SRW_TAC[][]
      THEN ASSUME_TAC(CHOICE_OF_TWO |> Q.SPECL[`x'`, `e`])
      THEN MP_TAC(CHOICE_AND_REST_OF_TWO |> Q.SPECL [`x'`, `e`])
      THEN SRW_TAC[][]
      THEN ASSUME_TAC(CHOICE_OF_TWO |> Q.SPECL[`e`, `y`])
      THEN MP_TAC(CHOICE_AND_REST_OF_TWO |> Q.SPECL [`e`, `y`])
      THEN SRW_TAC[][]
      THEN ASSUME_TAC(CHOICE_OF_TWO |> Q.SPECL[`y`, `e`])
      THEN MP_TAC(CHOICE_AND_REST_OF_TWO |> Q.SPECL [`y`, `e`])
      THEN SRW_TAC[][]
      THEN(FULL_SIMP_TAC(srw_ss())[SET_TWO_COMM, SET_THREE_COMM] THEN METIS_TAC[LESS_EQ_TRANS, LESS_EQ_REFL]))

THEN1(`!x. x IN s ==> (g x = g e)` by
        (`SING (g e INSERT IMAGE g s)` by METIS_TAC[SING_IFF_EMPTY_REST, NOT_INSERT_EMPTY]
         THEN FULL_SIMP_TAC(srw_ss())[SING_DEF, IMAGE_DEF, EXTENSION]
         THEN METIS_TAC[])
      THEN `(CHOICE (g e INSERT IMAGE g s)) = g e` by
        (`CHOICE (g e INSERT IMAGE g s) IN (g e INSERT IMAGE g s)` by METIS_TAC[CHOICE_DEF, NOT_INSERT_EMPTY]
         THEN FULL_SIMP_TAC(srw_ss())[IN_DEF])
      THEN `(g (CHOICE (e INSERT s))) = g e` by METIS_TAC[IN_INSERT, CHOICE_DEF, NOT_INSERT_EMPTY]
      THEN `f (g e) x <= f (g (CHOICE (REST (REST (REST (e INSERT s))))))
                               (f (g (CHOICE (REST (REST (e INSERT s)))))
                                  (f (g (CHOICE (REST (e INSERT s)))) (f (g e) x)))` by METIS_TAC[LESS_EQ_TRANS, LESS_EQ_REFL]

      THEN `(g (CHOICE (REST (e INSERT s)))) = g e` by PROVE_TAC[REST_SUBSET, IN_INSERT, CHOICE_DEF, SUBSET_DEF]
      THEN SRW_TAC[][]
      THEN)) *)

val INSERT_DELETE = store_thm("INSERT_DELETE",
``((e INSERT s) DELETE e) = s DELETE e``,
SRW_TAC[][EXTENSION]
THEN METIS_TAC[])

val INSERT_DELETE_2 = store_thm("INSERT_DELETE_2",
``e <> e' ==> (((e' INSERT s) DELETE e) = e' INSERT (s DELETE e))``,
SRW_TAC[][EXTENSION]
THEN METIS_TAC[])

val DELETE_DELETE_2 = store_thm("DELETE_DELETE_2",
``~(e' IN s) ==> (s DELETE e DELETE e' = s DELETE e)``,
SRW_TAC[][EXTENSION]
THEN METIS_TAC[])

val PROD_DELET_LEQ_PROD = store_thm("PROD_DELET_LEQ_PROD",
``!f s e. FINITE s ==> (!x. x IN s ==> f x > 0) ==> Π f (s DELETE e) <= Π f s``,
SRW_TAC[][]
THEN Induct_on `s`
THEN SRW_TAC[][PROD_IMAGE_THM, DELETE_NON_ELEMENT_RWT]
THEN Cases_on `e = e'`
THEN SRW_TAC[][INSERT_DELETE, INSERT_DELETE_2]
THEN `Π f (s DELETE e) ≤ Π f s` by METIS_TAC[]
THEN1(`f e > 0` by METIS_TAC[]
     THEN `Π f s <= f e * Π f s` by (SRW_TAC[][LE_MULT_CANCEL_LBARE] THEN DECIDE_TAC)
     THEN DECIDE_TAC)
THEN SRW_TAC[][PROD_IMAGE_THM, DELETE_DELETE_2])

val PRED_IMG_PRED_COMPOSE = store_thm("PRED_IMG_PRED_COMPOSE",
``(!x. x IN s ==> P ((f o g) x)) = (!y. y IN (IMAGE g s) ==> P (f y))``,
SRW_TAC[][IMAGE_DEF]
THEN METIS_TAC[])

val PROD_IMAGE_COMPOSE = store_thm("PROD_IMAGE_COMPOSE",
``FINITE s ==> (!x. x IN s ==> (f o g) x > 0 ) ==> (Π f (IMAGE g s) <= Π (f o g) s)``,
Induct_on `s`
THEN SRW_TAC[][PROD_IMAGE_THM, DELETE_NON_ELEMENT_RWT]
THEN `Π f ((IMAGE g s) DELETE g e) <= Π f (IMAGE g s)` by
       (MP_TAC(PROD_DELET_LEQ_PROD |> INST_TYPE[alpha |-> ``:'b``] |> Q.SPECL[`f`, `IMAGE g s`, `g e`])
       THEN SRW_TAC[][]
       THEN FIRST_X_ASSUM MATCH_MP_TAC
       THEN FULL_SIMP_TAC(srw_ss())[IN_DEF]
       THEN METIS_TAC[])
THEN METIS_TAC[LESS_EQ_TRANS])

val PROD_IMAGE_COMPOSE' = store_thm("PROD_IMAGE_COMPOSE'",
``FINITE s /\ (!x. x IN s ==> (f o g) x > 0 ) ==> (Π f (IMAGE g s) <= Π (f o g) s)``,
METIS_TAC[PROD_IMAGE_COMPOSE])

val CARD_SUBSET' = store_thm("CARD_SUBSET'",
``∀s. FINITE s /\ t ⊆ s ⇒ CARD t ≤ CARD s``,
SRW_TAC[][CARD_SUBSET])

val subset_inter_diff_empty = store_thm("subset_inter_diff_empty",
``s SUBSET t
  ==> (s INTER (u DIFF t) = EMPTY)``,
SRW_TAC[][SUBSET_DEF, INTER_DEF, EXTENSION]
THEN METIS_TAC[])

val UNION_SUBSET_UNION = store_thm("UNION_SUBSET_UNION",
``s SUBSET u ==> (s UNION t) SUBSET (u UNION t)``,
SRW_TAC[][subset_imp_subset_union])

val IMAGE_INTER_EQ = store_thm("IMAGE_INTER_EQ",
``INJ f UNIV UNIV ==> 
  (IMAGE f (s ∩ t) = IMAGE f s ∩ IMAGE f t)``,
SRW_TAC[][INJ_DEF, INTER_DEF, IMAGE_DEF, EXTENSION]
THEN METIS_TAC[])

(*

``(!g1 g2. g1 IN G /\ g2 IN G ==> g1 o g2 IN G) /\
  (!g. g IN G ==> BIJ g (O1 UNION O2) (O1 UNION O2)) /\
  (*(!g1. g1 IN G ==> ?g2. g2 IN G /\ (!x. ((g1 o g2) x = x))) /\*)
  (!x y. x IN O1 /\ y IN O1 ==>  (?g. g IN G /\ (g x = y))) /\
  (O1 = {a;b}) /\ a <> b /\
  (!x y. x IN O2 /\ y IN O2 ==> (?g. g IN G /\ (g x = y))) /\
  (O2 = {c;d}) /\ c <> d(* /\
  (!x y. x IN O3 /\ y IN O3 ==>  (?g. g IN G /\ (g x = y))) /\
  (O3 = {e;f}) /\ e <> f*)
  ==> ?g. g IN G /\ g a <> a /\ g c <> c (*/\ g z <> z*)``

SRW_TAC[][]
THEN `!x y z. x IN {y ; z} = ((x=y) \/ (x = z))` by SRW_TAC[][]
THEN `?g. g IN G /\ (g a = b)` by
     (LAST_X_ASSUM MATCH_MP_TAC
      THEN METIS_TAC[])
THEN `?g'. g' IN G /\ (g' c = d)` by
     (FIRST_X_ASSUM MATCH_MP_TAC
      THEN METIS_TAC[])

THEN Cases_on `(g' a = a) /\ (g' b = b) /\ (g' c = c) /\ (g' d = d) /\ 
               (g a = a) /\ (g b = b) /\ (g c = c) /\ (g d = d)`
THEN FULL_SIMP_TAC(srw_ss())[]

THEN1(Q.EXISTS_TAC `g'`
      THEN SRW_TAC[][])

THEN1(`g' a = b`
      Q.EXISTS_TAC `g o g'` 
      THEN SRW_TAC[][])

      THEN Cases_on `(g c = c)`
      THEN1 METIS_TAC[]
      THEN FULL_SIMP_TAC(srw_ss())[]
      THEN1 METIS_TAC[]
      THEN METIS_TAC[]

THEN METIS_TAC[]

THEN CONV_TAC(SWAP_EXISTS_CONV)
THEN Q.EXISTS_TAC `a`
       PROVE_TAC[]*)



val _ = export_theory();
