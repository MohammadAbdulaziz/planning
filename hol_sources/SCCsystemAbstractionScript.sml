open HolKernel Parse boolLib bossLib;
open finite_mapTheory;
open arithmeticTheory;
open pred_setTheory;
open rich_listTheory;
open listTheory;
open utilsTheory;
open factoredSystemTheory
open sublistTheory;
open systemAbstractionTheory;
open relationTheory;
open SCCTheory;
open parentChildStructureTheory;
open rel_utilsTheory;
open set_utilsTheory;
open dependencyTheory

val _ = new_theory "SCCsystemAbstraction";

val ancestors_def = Define
 `ancestors(PROB, vs) =
    {v: 'a | (?v': 'a. v' IN vs /\ ((dep_tc(PROB)) v v') ) /\ (!v': 'a. v' IN vs ==> ~((dep_tc(PROB)) v' v))}`;

val scc_vs_def = Define
`scc_vs(PROB, vs)
    = vs SUBSET (prob_dom PROB) /\ SCC (\v1' v2'. dep(PROB, v1', v2')) vs`;

val scc_set_def = Define
`scc_set PROB S = !vs. vs IN S ==> scc_vs(PROB, vs)`;

val sum_scc_parents_def
  = Define` sum_scc_parents(PROB, S)
     = SIGMA (\vs. problem_plan_bound((prob_proj(PROB, vs UNION (ancestors(PROB, vs)))))) S + 1`;

val childless_vs_def = Define`
      childless_vs(PROB, vs) = !vs'. ~(dep_var_set(PROB, vs, vs'))`

val childless_svs_def = Define`
      childless_svs(PROB, S) = !vs. vs IN S ==> (childless_vs(PROB, vs))`;

val problem_scc_set_def = Define`
      problem_scc_set(PROB) = {vs | (scc_vs(PROB, vs)) }`;

val childless_problem_scc_set_def = Define`
      childless_problem_scc_set(PROB) = {vs | (scc_vs(PROB, vs)) /\ (childless_vs(PROB, vs))}`;

val single_child_parents_def = Define
`single_child_parents(PROB, vs)
           = {vs' | vs' SUBSET (ancestors(PROB, vs))
                       /\ childless_vs(prob_proj(PROB, (prob_dom PROB) DIFF vs), vs')
                            /\scc_vs(PROB, vs')}`;

val single_child_ancestors_def = Define`single_child_ancestors PROB vs
= {vs' | ~(vs' SUBSET vs) /\ scc_vs (PROB, vs')
         /\ (\vs vs'. dep_var_set(PROB, vs, vs') /\ scc_vs(PROB, vs'))^+ vs' vs
         /\  (!vs''. (\vs vs'. dep_var_set(PROB, vs, vs') /\ scc_vs(PROB, vs'))^+ vs' vs''
                     /\ childless_vs(PROB, vs'')
                     ==>  (vs'' SUBSET vs))}`;

val member_leaves_def = Define
     `member_leaves(PROB, S)
           =  (\vs. (scc_vs(PROB, vs) /\ childless_vs(PROB, vs))) INTER S`;

val problem_wo_vs_ancestors_def = Define
  `problem_wo_vs_ancestors(PROB, vs) =
              prob_proj(PROB, (prob_dom PROB) DIFF (vs UNION BIGUNION (single_child_ancestors PROB vs)))`;

val neq_scc_imp_disj = store_thm("neq_scc_imp_disj",
``!PROB vs vs'. scc_vs(PROB, vs) /\ scc_vs(PROB, vs') /\ ~(vs = vs') ==> DISJOINT vs vs'``,
METIS_TAC[scc_disjoint_lemma, scc_vs_def])

val finite_leaves = store_thm("finite_leaves",
``!PROB S. FINITE(S) ==> FINITE((member_leaves(PROB, S)))``,
SRW_TAC[][member_leaves_def]
THEN METIS_TAC[FINITE_INTER]);

val leaf_is_scc = store_thm("leaf_is_scc",
``!PROB vs S. vs IN member_leaves(PROB, S) ==> scc_vs(PROB, vs)``,
SRW_TAC[][member_leaves_def]);

val leaf_is_S = store_thm("leaf_is_S",
``!PROB vs S. vs IN member_leaves(PROB, S) ==> vs IN S``,
SRW_TAC[][member_leaves_def]);

val leaf_is_childless = store_thm("leaf_is_childless",
``!PROB vs S. vs IN member_leaves(PROB, S) ==> childless_vs(PROB, vs)``,
SRW_TAC[][member_leaves_def]);

val scc_vs_imp_disj_bigU_ancestors = store_thm("scc_vs_imp_disj_bigU_ancestors",
``!PROB vs vs'. scc_vs(PROB, vs') /\ childless_vs(PROB, vs')
                ==> DISJOINT  vs' (BIGUNION (single_child_ancestors PROB vs))``,
SRW_TAC[][childless_vs_def, single_child_ancestors_def]
THEN MP_TAC(TC_CASES1_E 
              |> Q.ISPEC `(λvs vs'. dep_var_set (PROB,vs,vs')
                                    /\ scc_vs(PROB, vs'))`
              |> Q.SPECL[`s'`, `vs`])
THEN SRW_TAC[][]
THEN Cases_on `s' = vs'`
THEN METIS_TAC[scc_disjoint_lemma, scc_vs_def])

val SCC_IMP_DEP_TC = store_thm("SCC_IMP_DEP_TC",
``!PROB vs v v'. v IN vs /\ v' IN vs  /\ scc_vs(PROB, vs)
              ==> ((\v v'. dep(PROB, v, v') /\ v IN vs /\ v' IN vs)^+ v v')``,
SRW_TAC[][scc_vs_def]
THEN MP_TAC(DEP_REFL |> Q.SPEC `PROB`)
THEN SRW_TAC[][]
THEN MP_TAC(scc_tc_inclusion |> Q.ISPEC `(λv1' v2'. dep (PROB,v1',v2'))` |> Q.SPECL[`vs`, `v`, `v'`])
THEN ASM_SIMP_TAC(srw_ss())[]
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN  SRW_TAC[SatisfySimps.SATISFY_ss][])

val SCC_DISJ_SCC_PROJ = store_thm("SCC_DISJ_SCC_PROJ",
``!PROB vs vs'. scc_vs(PROB, vs') /\ DISJOINT vs vs'
                ==> scc_vs (prob_proj (PROB, (prob_dom PROB) DIFF vs),vs')``,
SRW_TAC[][scc_vs_def, SCC_def]
THENL
[
   SRW_TAC[][PROB_DOM_PROJ_DIFF, SUBSET_DIFF, DISJOINT_SYM]
   ,
   MP_TAC(IN_DISJ_DEP_IMP_DEP_DIFF |> Q.SPECL[`PROB`, `vs`, `vs'`])
   THEN SRW_TAC[][]
   THEN MP_TAC(REWRITE_RULE[scc_vs_def, SCC_def] SCC_IMP_DEP_TC |> Q.SPECL[`PROB`, `vs'`])
   THEN SRW_TAC[][]
   THEN MP_TAC(CONJ_THEN_R_IMP_CONJ_THEN_TC_R |> Q.SPECL[`(\v v'. dep(PROB, v, v'))`,
                                           `(\v v'. dep(prob_proj(PROB, (prob_dom PROB) DIFF vs), v, v'))`,
                                           `(\v. v IN vs')`])
   THEN SRW_TAC[][]
   ,
   MP_TAC(IN_DISJ_DEP_IMP_DEP_DIFF |> Q.SPECL[`PROB`, `vs`, `vs'`])
   THEN SRW_TAC[][]
   THEN MP_TAC(REWRITE_RULE[scc_vs_def, SCC_def] SCC_IMP_DEP_TC |> Q.SPECL[`PROB`, `vs'`])
   THEN SRW_TAC[][]
   THEN MP_TAC(CONJ_THEN_R_IMP_CONJ_THEN_TC_R |> Q.SPECL[`(\v v'. dep(PROB, v, v'))`,
                                           `(\v v'. dep(prob_proj(PROB, (prob_dom PROB) DIFF vs), v, v'))`,
                                           `(\v. v IN vs')`])
   THEN SRW_TAC[][]
   ,
   Q.PAT_X_ASSUM `!x y. P x y ==> Q x y \/ N x y` (MP_TAC o Q.SPECL[`v`, `v'`])
   THEN SRW_TAC[][]
   THENL
   [
     `¬(λv1' v2'. dep (prob_proj (PROB, (prob_dom PROB) DIFF vs),v1',v2'))⁺ v v'`
        by(REWRITE_TAC[TC_DEF]
        THEN SRW_TAC[][]
        THEN Q.PAT_X_ASSUM `¬(λv1' v2'. dep (PROB,v1',v2'))⁺ v v'` (MP_TAC o REWRITE_RULE[TC_DEF])
        THEN SRW_TAC[][]
        THEN Q.EXISTS_TAC `P`
        THEN METIS_TAC[PROJ_DEP_IMP_DEP])
     THEN SRW_TAC[][]
     ,
     `¬(λv1' v2'. dep (prob_proj (PROB, (prob_dom PROB) DIFF vs),v1',v2'))⁺ v' v`
        by(REWRITE_TAC[TC_DEF]
        THEN SRW_TAC[][]
        THEN Q.PAT_X_ASSUM `¬(λv1' v2'. dep (PROB,v1',v2'))⁺ v' v` (MP_TAC o REWRITE_RULE[TC_DEF])
        THEN SRW_TAC[][]
        THEN Q.EXISTS_TAC `P`
        THEN METIS_TAC[PROJ_DEP_IMP_DEP])
     THEN SRW_TAC[][]
   ]
])

val childless_imp_childless_proj = store_thm("childless_imp_childless_proj",
``!PROB vs vs'. childless_vs(PROB, vs')
                ==> childless_vs (prob_proj (PROB, (prob_dom PROB) DIFF vs),vs')``,
METIS_TAC[childless_vs_def, NDEP_PROJ_NDEP])

val in_leaves_imp_in_proj_leaves = store_thm("in_leaves_imp_in_proj_leaves",
``!PROB vs vs' S. DISJOINT vs vs' /\ vs' IN member_leaves(PROB, S)
                  ==> vs' IN (member_leaves(prob_proj(PROB, (prob_dom PROB) DIFF vs), S))``,
SRW_TAC[][member_leaves_def]
THEN METIS_TAC[SCC_DISJ_SCC_PROJ, childless_imp_childless_proj])

val in_leaves_in_leaves_wo_ancestors = store_thm("in_leaves_in_leaves_wo_ancestors",
``!PROB vs vs' S. scc_vs (PROB, vs) /\ ~(vs = vs')
                  /\ vs' IN member_leaves(PROB, S)
                  ==> vs' IN (member_leaves(problem_wo_vs_ancestors(PROB, vs), S))``,
SRW_TAC[][]
THEN MP_TAC(leaf_is_childless |> Q.SPECL [`PROB`, `vs'`, `S'`])
THEN SRW_TAC[][]
THEN MP_TAC(scc_vs_imp_disj_bigU_ancestors |> Q.SPECL[`PROB`, `vs`, `vs'`])
THEN `scc_vs (PROB,vs')` by FULL_SIMP_TAC(srw_ss())[member_leaves_def]
THEN ASM_SIMP_TAC(bool_ss)[]
THEN STRIP_TAC
THEN `DISJOINT vs vs'` by METIS_TAC[neq_scc_imp_disj, leaf_is_scc]
THEN REWRITE_TAC[problem_wo_vs_ancestors_def]
THEN METIS_TAC[in_leaves_imp_in_proj_leaves,  GSYM DISJOINT_UNION, DISJOINT_SYM])

val scc_vs_imp_mem_leaves_del = store_thm("scc_vs_imp_mem_leaves_del",
``!PROB S vs. scc_vs(PROB, vs) ==> (member_leaves(PROB, S) DELETE vs)
SUBSET (member_leaves(problem_wo_vs_ancestors(PROB, vs), S))``,
SRW_TAC[][in_leaves_in_leaves_wo_ancestors, Once (GSYM IN_DELETE), SUBSET_DEF]);

val SCC_SUBSET_DOM = store_thm("SCC_SUBSET_DOM",
``!PROB vs. scc_vs(PROB, vs) ==> vs SUBSET (prob_dom PROB)``,
SRW_TAC[][scc_vs_def])

val scc_vs_proj_imp_scc_vs = store_thm("scc_vs_proj_imp_scc_vs",
``!PROB S vs. scc_set PROB S /\ scc_vs(prob_proj (PROB, (prob_dom PROB) DIFF (BIGUNION S)), vs)
              ==> scc_vs(PROB, vs)``,
SRW_TAC[][]
THEN Cases_on `S' = {}`
THEN1(FULL_SIMP_TAC(srw_ss())[]
THEN METIS_TAC[PROJ_DOM_IDEMPOT])
THEN `DISJOINT vs (BIGUNION S')`
   by METIS_TAC[SCC_SUBSET_DOM, SUBSET_PROJ_DOM_DISJ, two_pow_n_is_a_bound_2]
THEN FULL_SIMP_TAC(srw_ss())[scc_vs_def, SCC_def]
THEN SRW_TAC[][]
THENL
[
   FULL_SIMP_TAC(srw_ss())[PROB_DOM_PROJ_DIFF]
   THEN METIS_TAC[SUBSET_TRANS, DIFF_SUBSET]
   ,
   MP_TAC(PROJ_DEP_IMP_DEP |> Q.SPECL[`PROB`, `BIGUNION S'`])
   THEN SRW_TAC[][]
   THEN MP_TAC(((Q.GEN `R` o Q.GEN `Q` o Q.GEN `x` o Q.GEN `y`) TC_MONOTONE)
             |> Q.ISPECL [`(\v v'. dep (prob_proj (PROB, (prob_dom PROB) DIFF (BIGUNION S')),v,v'))`,
                          `(\v v'. dep (PROB,v,v'))`])
   THEN SRW_TAC[][]
   ,
   MP_TAC(PROJ_DEP_IMP_DEP |> Q.SPECL[`PROB`, `BIGUNION S'`])
   THEN SRW_TAC[][]
   THEN MP_TAC(((Q.GEN `R` o Q.GEN `Q` o Q.GEN `x` o Q.GEN `y`) TC_MONOTONE)
             |> Q.ISPECL [`(\v v'. dep (prob_proj (PROB,(prob_dom PROB) DIFF (BIGUNION S')),v,v'))`,
                          `(\v v'. dep (PROB,v,v'))`])
   THEN SRW_TAC[][]
   ,
   Q.PAT_X_ASSUM `!x y. P x y ==> Q x y \/ N x y` (MP_TAC o Q.SPECL[`v`, `v'`])
   THEN SRW_TAC[][]
   THENL
   [
      Cases_on `v' IN BIGUNION S'`
      THENL
      [
         `!s. s IN S' ==> ~(v IN s)`
            by (FULL_SIMP_TAC(bool_ss)[DISJOINT_DEF, INTER_DEF, EXTENSION, GSPEC_ETA, EMPTY_DEF, IN_DEF]
               THEN METIS_TAC[])
         THEN FULL_SIMP_TAC(bool_ss)[IN_BIGUNION]
         THEN `scc_vs(PROB, s)` by FULL_SIMP_TAC(srw_ss())[scc_set_def]
         THEN FULL_SIMP_TAC(srw_ss())[scc_vs_def, SCC_def]
         THEN METIS_TAC[]
         ,
         Cases_on `(λv1' v2'.
                    dep (prob_proj (PROB, (prob_dom PROB) DIFF BIGUNION S'),v1',v2'))⁺
                       v' v`
         THENL
         [
            `(λv1' v2'. dep (PROB,v1',v2'))⁺ v' v` by METIS_TAC[PROJ_DEP_IMP_DEP,
                                                                (((Q.GEN `R` o Q.GEN `Q` o Q.GEN `x` o Q.GEN `y`) TC_MONOTONE)
                                                                       |> Q.ISPECL [`(\v v'. dep (prob_proj (PROB, (prob_dom PROB) DIFF (BIGUNION S')),v,v'))`,
                                                                                    `(\v v'. dep (PROB,v,v'))`])]
            THEN SRW_TAC[][]
            THEN FULL_SIMP_TAC(srw_ss())[childless_vs_def, dep_var_set_def]
            THEN MP_TAC(PROJ_NDEP_TC_IMP_NDEP_TC_OR |> Q.SPECL[`PROB`,`BIGUNION S'`, `v`, `v'`])
            THEN SRW_TAC[][]
            THEN `(λv1' v2'. dep (PROB,v1',v2'))⁺ v'' v` by METIS_TAC[TC_RULES |> Q.SPEC `(λv1' v2'. dep (PROB,v1',v2'))`]
            THEN `!s. s IN S' ==> ~(v' IN s)`
                  by (FULL_SIMP_TAC(bool_ss)[DISJOINT_DEF, INTER_DEF, EXTENSION, GSPEC_ETA, EMPTY_DEF, IN_DEF]
                     THEN METIS_TAC[])
            THEN FULL_SIMP_TAC(bool_ss)[IN_BIGUNION]
            THEN `scc_vs(PROB, s)` by FULL_SIMP_TAC(srw_ss())[scc_set_def]
            THEN FULL_SIMP_TAC(srw_ss())[scc_vs_def, SCC_def]
            THEN METIS_TAC[]
            ,
            MP_TAC(PROJ_NDEP_TC_IMP_NDEP_TC_OR |> Q.SPECL[`PROB`,`BIGUNION S'`, `v`, `v'`])
            THEN SRW_TAC[][]
            THEN1 METIS_TAC[]
            THEN MP_TAC(PROJ_NDEP_TC_IMP_NDEP_TC_OR |> Q.SPECL[`PROB`,`BIGUNION S'`, `v'`, `v`])
            THEN SRW_TAC[][]
            THEN1 METIS_TAC[]
            THEN `(λv1' v2'. dep (PROB,v1',v2'))⁺ v' v''` by METIS_TAC[TC_RULES |> Q.SPEC `(λv1' v2'. dep (PROB,v1',v2'))`]
            THEN `(λv1' v2'. dep (PROB,v1',v2'))⁺ v'' v'` by METIS_TAC[TC_RULES |> Q.SPEC `(λv1' v2'. dep (PROB,v1',v2'))`]
            THEN `!s. s IN S' ==> ~(v' IN s)`
                  by (FULL_SIMP_TAC(srw_ss())[DISJOINT_DEF, INTER_DEF, EXTENSION, GSPEC_ETA, EMPTY_DEF, IN_DEF]
                     THEN METIS_TAC[])
            THEN FULL_SIMP_TAC(bool_ss)[IN_BIGUNION]
            THEN `scc_vs(PROB, s)` by FULL_SIMP_TAC(srw_ss())[scc_set_def]
            THEN FULL_SIMP_TAC(srw_ss())[scc_vs_def, SCC_def]
            THEN METIS_TAC[]
         ]
      ]
      ,
      Cases_on `v' IN BIGUNION S'`
      THENL
      [
         `!s. s IN S' ==> ~(v IN s)`
            by (FULL_SIMP_TAC(bool_ss)[DISJOINT_DEF, INTER_DEF, EXTENSION, GSPEC_ETA, EMPTY_DEF, IN_DEF]
               THEN METIS_TAC[])
         THEN FULL_SIMP_TAC(bool_ss)[IN_BIGUNION]
         THEN `scc_vs(PROB, s)` by FULL_SIMP_TAC(srw_ss())[scc_set_def]
         THEN FULL_SIMP_TAC(srw_ss())[scc_vs_def, SCC_def]
         THEN METIS_TAC[]
         ,
         Cases_on `(λv1' v2'.
                     dep (prob_proj (PROB, (prob_dom PROB) DIFF BIGUNION S'),v1',v2'))⁺
                        v v'`
         THENL
         [
            `(λv1' v2'. dep (PROB,v1',v2'))⁺ v v'` by METIS_TAC[PROJ_DEP_IMP_DEP,
                                                                (((Q.GEN `R` o Q.GEN `Q` o Q.GEN `x` o Q.GEN `y`) TC_MONOTONE)
                                                                       |> Q.ISPECL [`(\v v'. dep (prob_proj (PROB, (prob_dom PROB) DIFF (BIGUNION S')),v,v'))`,
                                                                                    `(\v v'. dep (PROB,v,v'))`])]
            THEN SRW_TAC[][]
            THEN FULL_SIMP_TAC(srw_ss())[childless_vs_def, dep_var_set_def]
            THEN MP_TAC(PROJ_NDEP_TC_IMP_NDEP_TC_OR |> Q.SPECL[`PROB`,`BIGUNION S'`, `v'`, `v`])
            THEN SRW_TAC[][]
            THEN `(λv1' v2'. dep (PROB,v1',v2'))⁺ v'' v'`
	          by METIS_TAC[TC_RULES |> Q.SPEC `(λv1' v2'. dep (PROB,v1',v2'))`]
            THEN `!s. s IN S' ==> ~(v' IN s)`
                  by (FULL_SIMP_TAC(bool_ss)[DISJOINT_DEF, INTER_DEF, EXTENSION, GSPEC_ETA, EMPTY_DEF, IN_DEF]
                     THEN METIS_TAC[])
            THEN FULL_SIMP_TAC(bool_ss)[IN_BIGUNION]
            THEN `scc_vs(PROB, s)` by FULL_SIMP_TAC(srw_ss())[scc_set_def]
            THEN FULL_SIMP_TAC(srw_ss())[scc_vs_def, SCC_def]
            THEN METIS_TAC[]
            ,
            MP_TAC(PROJ_NDEP_TC_IMP_NDEP_TC_OR |> Q.SPECL[`PROB`,`BIGUNION S'`, `v`, `v'`])
            THEN SRW_TAC[][]
            THEN1 METIS_TAC[]
            THEN MP_TAC(PROJ_NDEP_TC_IMP_NDEP_TC_OR |> Q.SPECL[`PROB`,`BIGUNION S'`, `v'`, `v`])
            THEN SRW_TAC[][]
            THEN1 METIS_TAC[]
            THEN `(λv1' v2'. dep (PROB,v1',v2'))⁺ v' v''` by METIS_TAC[TC_RULES |> Q.SPEC `(λv1' v2'. dep (PROB,v1',v2'))`]
            THEN `(λv1' v2'. dep (PROB,v1',v2'))⁺ v'' v'` by METIS_TAC[TC_RULES |> Q.SPEC `(λv1' v2'. dep (PROB,v1',v2'))`]
            THEN `!s. s IN S' ==> ~(v' IN s)`
                  by (FULL_SIMP_TAC(srw_ss())[DISJOINT_DEF, INTER_DEF, EXTENSION, GSPEC_ETA, EMPTY_DEF, IN_DEF]
                     THEN METIS_TAC[])
            THEN FULL_SIMP_TAC(bool_ss)[IN_BIGUNION]
            THEN `scc_vs(PROB, s)` by FULL_SIMP_TAC(srw_ss())[scc_set_def]
            THEN FULL_SIMP_TAC(srw_ss())[scc_vs_def, SCC_def]
            THEN METIS_TAC[]
         ]
      ]
   ]
])

val proj_prob_scc_set_subset = store_thm("proj_prob_scc_set_subset",
``!PROB S. scc_set PROB S
           ==> (problem_scc_set (prob_proj(PROB, (prob_dom PROB) DIFF BIGUNION S)))
                 SUBSET (problem_scc_set PROB) ``,
SRW_TAC[][problem_scc_set_def, SUBSET_DEF]
THEN METIS_TAC[scc_vs_proj_imp_scc_vs])

val SCC_DISJ_SCC_SET = store_thm("SCC_DISJ_SCC_SET",
``!PROB S vs. scc_vs(PROB, vs) /\ scc_set PROB S /\ ~(vs IN S)
              ==> DISJOINT vs (BIGUNION S)``,
SRW_TAC[][scc_set_def] THEN METIS_TAC[neq_scc_imp_disj, DISJOINT_SYM,NOT_IN_IMP_NEQ])

val not_in_scc_sos_proj_ndep_imp_ndep = store_thm("not_in_scc_sos_proj_ndep_imp_ndep",
``!PROB S vs. scc_set PROB S /\ scc_vs(PROB, vs) /\ ~(vs IN S)
              ==> ! vs'. (scc_vs(PROB, vs') /\ ~(vs' IN S))
                         ==> ~ dep_var_set(prob_proj(PROB, (prob_dom PROB) DIFF BIGUNION S), vs, vs')
                             ==> ~ dep_var_set(PROB, vs, vs')``,
SRW_TAC[][]
THEN METIS_TAC [DISJ_PROJ_NDEP_IMP_NDEP, SCC_DISJ_SCC_SET, DISJOINT_SYM])

val every_child_in_sccset = store_thm("every_child_in_sccset",
``!PROB vs S. childless_vs (prob_proj(PROB, (prob_dom PROB) DIFF (BIGUNION S)), vs)
              /\ scc_set PROB S /\ scc_vs(PROB, vs) /\ ~(vs IN S)
                ==> (!vs'. dep_var_set (PROB,vs,vs') /\ scc_vs(PROB, vs')
                                  ==> vs' IN S)``,
SRW_TAC[][]
THEN MP_TAC(not_in_scc_sos_proj_ndep_imp_ndep |> Q.SPECL[ `PROB`, `S'`, `vs`])
THEN SRW_TAC[][]
THEN MP_TAC(NP_NR_THEN_NR'_IMP_NP
                   |> Q.ISPECL [`(\ vs vs'. dep_var_set(prob_proj(PROB, BIGUNION S'), vs, vs'))`,
                                `(\ vs vs'. dep_var_set(PROB, vs, vs'))`,
                                `(\ vs. (scc_vs(PROB, vs) /\ ~(vs IN S')))`]
                   |> Q.SPEC `vs`)
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[childless_vs_def]
THEN METIS_TAC[])

val DEP_SCC_SET = store_thm("DEP_SCC_SET",
``!PROB vs vs' S. dep_var_set(PROB, vs, vs') /\ scc_set PROB S /\ vs' IN S
                  /\ ~(vs IN S) /\ scc_vs(PROB, vs)
                  ==> dep_var_set(PROB, vs, BIGUNION S)``,
SRW_TAC[][dep_var_set_def]
THEN MP_TAC(SCC_DISJ_SCC_SET |> Q.SPECL [`PROB`, `S'`, `vs`])
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `v1`
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `v2`
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `vs'`
THEN SRW_TAC[][])

val DEP_IMP_DEP_SUPER_SOSETS = store_thm("DEP_IMP_DEP_SUPER_SOSETS",
``!PROB vs vs' S. dep_var_set(PROB, vs, vs') /\ scc_set PROB S /\ vs IN S
                  /\ ~(vs' IN S) /\ scc_vs(PROB, vs')
                  ==> dep_var_set(PROB, BIGUNION S, vs')``,
SRW_TAC[][dep_var_set_def]
THEN MP_TAC(SCC_DISJ_SCC_SET |> Q.SPECL [`PROB`, `S'`, `vs'`])
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `v1`
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `v2`
THEN SRW_TAC[][]
THEN1( Q.EXISTS_TAC `vs`
THEN SRW_TAC[][])
THEN METIS_TAC[DISJOINT_SYM])

val SCC_DEP_IN_SCC_SET = store_thm("SCC_DEP_IN_SCC_SET",
``!PROB S vs vs'. scc_set PROB S /\ childless_vs(PROB, BIGUNION S)
                 /\ dep_var_set(PROB, vs, vs') /\ scc_vs(PROB, vs')
                 /\ vs IN S
                 ==> vs' IN S ``,
SRW_TAC[][]
THEN SPOSE_NOT_THEN STRIP_ASSUME_TAC
THEN FULL_SIMP_TAC(srw_ss())[childless_vs_def]
THEN METIS_TAC[DEP_IMP_DEP_SUPER_SOSETS])

val IN_DOM_IMP_IN_SOME_SCC = store_thm("IN_DOM_IMP_IN_SOME_SCC",
``!PROB v. v IN (prob_dom PROB)
           ==> ?vs. scc_vs(PROB, vs) /\ v IN vs``,
SRW_TAC[][scc_vs_def, SCC_def]
THEN Q.EXISTS_TAC
   `{v' | (λv1' v2'. dep (PROB,v1',v2'))⁺ v v' ∧ (λv1' v2'. dep (PROB,v1',v2'))⁺ v' v}`
THEN SRW_TAC[][]
THENL
[
   SRW_TAC[][SUBSET_DEF, GSPEC_ETA]
   THEN MP_TAC(NEQ_DEP_IMP_IN_DOM |> Q.SPEC `PROB`)
   THEN SRW_TAC[][]
   THEN MP_TAC(TC_REL_IN_SET
                |> Q.ISPEC `(\v v'. dep(PROB, v, v') /\ ~(v = v'))`
                |> Q.SPEC `prob_dom PROB`
                |> Q.SPEC `v`)
   THEN SRW_TAC[][]
   THEN `∀y.
        (λv v'. dep (PROB,v,v') ∧ v ≠ v')⁺ v y ⇒
        (λx' y'. (dep (PROB,x',y') ∧ x' ≠ y') ∧ y' ∈ (prob_dom PROB))⁺ v y` by PROVE_TAC[]
   THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `x`)
   THEN SRW_TAC[][]
   THEN Cases_on `v = x`
   THEN1 SRW_TAC[][]
   THEN `(λx' y'. (dep (PROB,x',y') ∧ x' ≠ y') ∧ y' ∈ (prob_dom PROB))⁺ v x` by
      METIS_TAC[TC_IMP_TC_OR_EQ |> Q.ISPEC `(λv1' v2'. dep (PROB,v1',v2'))`]
   THEN FIRST_X_ASSUM (MP_TAC o MATCH_MP TC_CASES2_E)
   THEN SRW_TAC[][]
   ,
   PROVE_TAC[REWRITE_RULE[transitive_def] TC_TRANSITIVE]
   ,
   PROVE_TAC[REWRITE_RULE[transitive_def] TC_TRANSITIVE]
   ,
   PROVE_TAC[REWRITE_RULE[transitive_def] TC_TRANSITIVE]
   ,
   PROVE_TAC[REWRITE_RULE[transitive_def] TC_TRANSITIVE]
   ,
   SRW_TAC[][EXTENSION]
   THEN Q.EXISTS_TAC `v`
   THEN SRW_TAC[][]
   THEN METIS_TAC[TC_RULES |> Q.SPEC `(λv1' v2'. dep (PROB,v1',v2'))`, REWRITE_RULE[reflexive_def] DEP_REFL]
   ,
   METIS_TAC[TC_RULES |> Q.SPEC `(λv1' v2'. dep (PROB,v1',v2'))`, REWRITE_RULE[reflexive_def] DEP_REFL]
])

val DEP_SCC_VS_EXIS_NDISJ = store_thm("DEP_SCC_VS_EXIS_NDISJ",
``!PROB vs vs'. dep_var_set(PROB, vs, vs') /\ scc_vs(PROB, vs)
                 ==> ?vs''. scc_vs(PROB, vs'') /\ dep_var_set(PROB, vs, vs'') /\ ~ DISJOINT vs' vs''``,
SRW_TAC[][dep_var_set_def, DISJOINT_DEF, INTER_DEF, EXTENSION]
THEN `~(v1 = v2)` by METIS_TAC[DISJ_IN_IMP_NEQ]
THEN MP_TAC(NEQ_DEP_IMP_IN_DOM |> Q.SPECL [`PROB` , `v1`, `v2`])
THEN SRW_TAC[][]
THEN MP_TAC(IN_DOM_IMP_IN_SOME_SCC |> Q.SPECL [`PROB`, `v2`])
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `vs''`
THEN SRW_TAC[][]
THEN1(Q.EXISTS_TAC `v1`
   THEN Q.EXISTS_TAC `v2`
   THEN SRW_TAC[][]
   THEN `~(v2 IN vs)` by METIS_TAC[DISJ_IN_IMP_NEQ]
   THEN METIS_TAC[SIMP_RULE(srw_ss())[DISJOINT_DEF, INTER_DEF, EXTENSION] neq_scc_imp_disj])
THEN METIS_TAC[])

val proj_mem_leaves_subset_mem_leaves_union_ancestors = store_thm("proj_mem_leaves_subset_mem_leaves_union_ancestors",
``!PROB S vs. (childless_vs(PROB, vs) /\ scc_vs(PROB, vs))
                  ==> (member_leaves(prob_proj(PROB, (prob_dom PROB) DIFF vs), S))
                         SUBSET (member_leaves(PROB, S))
                             UNION
                                (single_child_ancestors PROB vs)``,
SRW_TAC[][member_leaves_def, SUBSET_DEF, UNION_DEF, GSPEC_ETA]
THEN `scc_set PROB {vs}` by SRW_TAC[][scc_set_def]
THEN `scc_vs(PROB, x)`
          by (MP_TAC((
                (SIMP_RULE(srw_ss())[] o
                        REWRITE_RULE[SUBSET_DEF, problem_scc_set_def, GSPEC_ETA])
                                 proj_prob_scc_set_subset) |> Q.SPECL[`PROB`, `{vs}`])
              THEN SRW_TAC[][])
THEN SRW_TAC[][]
THEN Cases_on `(x = vs)`
THEN1(`DISJOINT x vs`
   by METIS_TAC[SCC_SUBSET_DOM, SUBSET_PROJ_DOM_DISJ, two_pow_n_is_a_bound_2]
      THEN Cases_on `x = {}`
      THEN1 FULL_SIMP_TAC(srw_ss())[scc_vs_def, SCC_def]
      THEN FULL_SIMP_TAC(srw_ss())[DISJOINT_DEF, INTER_DEF, EXTENSION]
      THEN METIS_TAC[])
THEN MP_TAC(every_child_in_sccset |> Q.SPECL [`PROB`, `x`, `{vs}`])
THEN SRW_TAC[][]
THEN Cases_on `childless_vs(PROB, x)`
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o SIMP_RULE(srw_ss())[childless_vs_def])
THEN SRW_TAC[][single_child_ancestors_def]
THEN1(MP_TAC(neq_scc_imp_disj |> Q.SPECL[`PROB`, `x`, `vs`])
      THEN SRW_TAC[][DISJOINT_DEF, SUBSET_DEF, INTER_DEF, EXTENSION]
      THEN FULL_SIMP_TAC(srw_ss())[scc_vs_def, SCC_def, EXTENSION]
      THEN METIS_TAC[])
THEN1(MP_TAC(DEP_SCC_VS_EXIS_NDISJ |> Q.SPECL[`PROB`, `x`, `vs'`])
      THEN SRW_TAC[][]
      THEN METIS_TAC[TC_RULES |> Q.ISPEC `(λvs vs'. dep_var_set (PROB,vs,vs') ∧ scc_vs (PROB,vs'))`])
THEN1(METIS_TAC[R_imp_eq_R_TC_imp_eq |> Q.ISPEC `(λvs vs'. dep_var_set (PROB,vs,vs') ∧ scc_vs (PROB,vs'))`,
               childless_vs_def, eq_imp_subset]))


val not_in_leaves_not_in_leaves_proj_diff = store_thm("not_in_leaves_not_in_leaves_proj_diff",
``!PROB S vs vs'. scc_vs(PROB, vs) /\ childless_vs(PROB, vs) /\ ~(vs' IN single_child_ancestors PROB (vs))
                 /\ ~(vs' IN member_leaves(PROB, S))
                 ==> ~(vs' IN (member_leaves(prob_proj(PROB, (prob_dom PROB) DIFF vs), S)))``,
SRW_TAC[][]
THEN METIS_TAC[(SIMP_RULE(srw_ss())[] o REWRITE_RULE[SUBSET_DEF, UNION_DEF, GSPEC_ETA]) proj_mem_leaves_subset_mem_leaves_union_ancestors]);

val in_ancestors_imp_disj_nempty = store_thm("in_ancestors_imp_disj_nempty",
``!PROB vs vs'. scc_vs(PROB, vs) /\ vs' IN (single_child_ancestors PROB vs)
                ==> DISJOINT vs vs' /\ ~(vs' = {})``,
SRW_TAC[][single_child_ancestors_def]
THENL
[
   METIS_TAC[nsubset_imp_neq,neq_scc_imp_disj]
   ,
   FULL_SIMP_TAC(srw_ss())[scc_vs_def, SCC_def]
]);

val dep_conj_tc_imp_dep_tc = store_thm("dep_conj_tc_imp_dep_tc",
``!PROB vs vs'.(λvs vs'. dep_var_set (PROB,vs,vs') ∧ scc_vs (PROB,vs'))⁺ vs vs'
               ==> (λvs vs'. dep_var_set (PROB,vs,vs'))⁺ vs vs'``,
METIS_TAC[CONJ_TC |> Q.ISPEC `(λvs vs'. dep_var_set (PROB,vs,vs'))`
                                        |> Q.SPEC `(λvs vs'.scc_vs (PROB,vs'))`])

val sing_child_ancestor_imp_dep_tc = store_thm("sing_child_ancestor_imp_dep_tc",
``!PROB vs vs'. vs' IN single_child_ancestors PROB vs
                     ==> (\ vs vs'. dep_var_set (PROB, vs, vs'))^+ vs' vs``,
SRW_TAC[][single_child_ancestors_def]
THEN METIS_TAC[dep_conj_tc_imp_dep_tc])

val dep_tc_ancestors_imp_dep_tc = store_thm("dep_tc_ancestors_imp_dep_tc",
``!PROB vs vs'. (\vs vs'. dep_var_set (PROB,vs,vs'))^+ vs (vs' ∪ BIGUNION (single_child_ancestors PROB vs'))
         ==> (\vs vs'. dep_var_set (PROB,vs,vs'))^+ vs vs'``,
SRW_TAC[][]
THEN MP_TAC(TC_CASES2_E |> Q.ISPEC `(\vs vs'. dep_var_set (PROB,vs,vs'))`
          |> Q.SPECL[`vs`,
                     `(vs' ∪ BIGUNION (single_child_ancestors PROB vs'))`])
THEN SRW_TAC[][]
THENL
[
   MP_TAC(dep_union_imp_or_dep |> Q.SPECL[`PROB`, `vs`, `vs'`,
                                           `BIGUNION (single_child_ancestors PROB vs')`] )
   THEN SRW_TAC[][]
   THENL
   [
      SRW_TAC[SatisfySimps.SATISFY_ss][TC_SUBSET]
      ,
      MP_TAC(dep_biunion_imp_or_dep |> Q.SPECL[`PROB`, `vs`, `(single_child_ancestors PROB vs')`])
      THEN SRW_TAC[][]
      THEN METIS_TAC[ sing_child_ancestor_imp_dep_tc, TC_RULES|> Q.ISPEC `(λvs vs'. dep_var_set (PROB,vs,vs'))`,
                                    TC_SUBSET |> Q.ISPEC `(λvs vs'. dep_var_set (PROB,vs,vs'))`]
   ]
   ,
   MP_TAC(dep_union_imp_or_dep |> Q.SPECL[`PROB`, `y`, `vs'`,
                                           `BIGUNION (single_child_ancestors PROB vs')`] )
   THEN SRW_TAC[][]
   THENL
   [
      METIS_TAC[TC_RULES|> Q.ISPEC `(λvs vs'. dep_var_set (PROB,vs,vs'))`,
                                    TC_SUBSET |> Q.ISPEC `(λvs vs'. dep_var_set (PROB,vs,vs'))`]
      ,
      MP_TAC(dep_biunion_imp_or_dep |> Q.SPECL[`PROB`, `y`, `(single_child_ancestors PROB vs')`])
      THEN SRW_TAC[][]
      THEN METIS_TAC[ sing_child_ancestor_imp_dep_tc, TC_RULES|> Q.ISPEC `(λvs vs'. dep_var_set (PROB,vs,vs'))`,
                                    TC_SUBSET |> Q.ISPEC `(λvs vs'. dep_var_set (PROB,vs,vs'))`]
   ]
]);

val scc_subset_imp_eq = store_thm("scc_subset_imp_eq",
``!PROB vs vs'. vs SUBSET vs' /\ scc_vs(PROB, vs) /\ scc_vs(PROB, vs')
            ==> (vs = vs')``,
SRW_TAC[][]
THEN `~(vs = {})` by FULL_SIMP_TAC(srw_ss())[scc_vs_def, SCC_def]
THEN `~DISJOINT vs vs'` by METIS_TAC[NEMPTY_SUBSET_NDISJ, DISJOINT_SYM]
THEN METIS_TAC[neq_scc_imp_disj, DISJOINT_SYM])

val scc_subset_ancestors_imp_eq = store_thm("scc_subset_ancestors_imp_eq",
``∀PROB vs vs'.
               childless_vs (PROB,vs') /\ scc_vs (PROB,vs) /\ scc_vs (PROB,vs')
               /\ vs' ⊆ vs ∪ BIGUNION (single_child_ancestors PROB vs)
               ==> (vs' =  vs)``,
SRW_TAC[][]
THEN `(!x y. x IN (vs INSERT (single_child_ancestors PROB vs))
             /\ y IN (vs INSERT (single_child_ancestors PROB vs)) /\ ~(x = y)
             ==> DISJOINT x y)`
   by (SRW_TAC[][]
   THEN FULL_SIMP_TAC(srw_ss())[single_child_ancestors_def]
   THEN METIS_TAC[neq_scc_imp_disj])
THEN `(!y.  y IN (vs INSERT (single_child_ancestors PROB vs)) /\ ~(vs' = y)
             ==> DISJOINT vs' y)`
   by (SRW_TAC[][]
   THEN FULL_SIMP_TAC(srw_ss())[single_child_ancestors_def]
   THEN METIS_TAC[neq_scc_imp_disj])
THEN MP_TAC(subset_bigun_subset_mem |> Q.SPECL [`vs INSERT single_child_ancestors PROB vs`, `vs'`])
THEN SRW_TAC[][]
THEN1 METIS_TAC[scc_subset_imp_eq]
THEN FULL_SIMP_TAC(srw_ss())[single_child_ancestors_def, Once(GSYM TC_CASES1_RW), childless_vs_def]
THEN METIS_TAC[scc_subset_imp_eq])

val scc_set_proj_ancestors_1 = store_thm("scc_set_proj_ancestors_1",
``!PROB vs. scc_vs(PROB, vs) ==> DISJOINT vs (BIGUNION (single_child_ancestors PROB vs))``,
SRW_TAC[][single_child_ancestors_def]
THEN METIS_TAC[neq_scc_imp_disj, nsubset_imp_neq])

val scc_set_proj_ancestors = store_thm("scc_set_proj_ancestors",
``!PROB vs. scc_vs(PROB, vs)
            ==> scc_set (prob_proj (PROB,(prob_dom PROB) DIFF vs)) 
                        (single_child_ancestors PROB vs)``,
MP_TAC(scc_set_proj_ancestors_1)
THEN SRW_TAC[][scc_set_def]
THEN FULL_SIMP_TAC(srw_ss())[single_child_ancestors_def]
THEN METIS_TAC[SCC_DISJ_SCC_PROJ, DISJOINT_SYM])

val proj_dep_scc_vs_imp_dep_scc = store_thm("proj_dep_scc_vs_imp_dep_scc",
``!PROB S vs vs'.  scc_vs(PROB, vs) /\ scc_vs(PROB, vs') /\ ~(vs' = vs)
                  /\ (!vs''. dep_var_set (prob_proj (PROB,(prob_dom PROB) DIFF vs), vs', vs'') /\
                             scc_vs (prob_proj (PROB,(prob_dom PROB) DIFF vs), vs'')
                             ==> vs'' IN S)
                  ==> (!vs''. dep_var_set (PROB,vs',vs'') /\
                              scc_vs (PROB,vs'')
                              ==> vs'' IN vs INSERT S)``,
SRW_TAC[][]
THEN Cases_on `vs = vs''`
THEN SRW_TAC[][]
THEN METIS_TAC[DISJ_PROJ_NDEP_IMP_NDEP, neq_scc_imp_disj, SCC_DISJ_SCC_PROJ])

val not_childless_is_in_ancestors_1 = store_thm("not_childless_is_in_ancestors_1",
``!PROB vs vs'.  scc_vs(PROB, vs) /\ scc_vs(PROB, vs') /\ childless_vs(PROB, vs)
             /\ (?vs''. dep_var_set(PROB, vs', vs'')) /\ ~(vs = vs')
             /\ (!vs''. (dep_var_set (PROB,vs',vs'')) /\ scc_vs(PROB, vs'')
                         ==> vs'' IN vs INSERT (single_child_ancestors PROB vs))
             ==> vs' IN single_child_ancestors PROB vs``,
SRW_TAC[][single_child_ancestors_def]
THENL
[
   MP_TAC(SCC_DISJ_SCC_SET |> Q.SPECL [`PROB`, `vs`, `vs'`])
   THEN SRW_TAC[][]
   THEN FULL_SIMP_TAC(srw_ss())[EXTENSION, DISJOINT_DEF, INTER_DEF, GSPEC_ETA, scc_vs_def, SCC_def, SUBSET_DEF]
   THEN METIS_TAC[]
   ,
   MP_TAC(DEP_SCC_VS_EXIS_NDISJ|> Q.SPECL[`PROB`, `vs'`, `vs''`])
   THEN SRW_TAC[][]
   THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `vs'''`)
   THEN SRW_TAC[][]
   THEN METIS_TAC[TC_RULES |> Q.ISPEC `(λvs vs'. dep_var_set (PROB,vs,vs') /\ scc_vs(PROB, vs'))`]
   ,
   Q.PAT_X_ASSUM `TC R x y` (MP_TAC o SIMP_RULE(srw_ss())[Once (GSYM TC_CASES1_RW)] )
   THEN SRW_TAC[][]
   THENL
   [
      FIRST_X_ASSUM (MP_TAC o Q.SPEC `vs'''`)
      THEN SRW_TAC[][]
      THEN1 METIS_TAC[SUBSET_REFL]
      THEN1(FULL_SIMP_TAC(srw_ss())[childless_vs_def]
            THEN Q.PAT_X_ASSUM `TC R x y` (MP_TAC o SIMP_RULE(srw_ss())[Once (GSYM TC_CASES1_RW)] )
            THEN SRW_TAC[][])
      ,
      FIRST_X_ASSUM (MP_TAC o Q.SPEC `vs''''`)
      THEN SRW_TAC[][]
      THEN1(FULL_SIMP_TAC(srw_ss())[childless_vs_def]
            THEN Q.PAT_X_ASSUM `TC R x y` (MP_TAC o SIMP_RULE(srw_ss())[Once (GSYM TC_CASES1_RW)] )
            THEN SRW_TAC[][])
      THEN METIS_TAC[TC_RULES |> Q.ISPEC `(λvs vs'. dep_var_set (PROB,vs,vs') /\ scc_vs(PROB, vs'))`, SUBSET_REFL]
   ]
])

val not_childless_is_in_ancestors = store_thm("not_childless_is_in_ancestors",
``!PROB vs vs'.  scc_vs(PROB, vs) /\ scc_vs(PROB, vs') /\ childless_vs(PROB, vs)
             /\ ~childless_vs(PROB, vs') /\ ~(vs = vs')
             /\ (!vs''. (dep_var_set (PROB,vs',vs'')) /\ scc_vs(PROB, vs'')
                         ==> vs'' IN vs INSERT (single_child_ancestors PROB vs))
             ==> vs' IN single_child_ancestors PROB vs``,
SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[childless_vs_def]
THEN  MP_TAC(DEP_SCC_VS_EXIS_NDISJ
|> Q.SPECL[`PROB`,`vs'`,`vs''`] )
THEN SRW_TAC[][]
THEN MP_TAC(not_childless_is_in_ancestors_1
|> Q.SPECL[`PROB`,`vs`,`vs'`])
THEN SRW_TAC[][]
THEN METIS_TAC[childless_vs_def])

val leaves_proj_diff_subset_leaves_diff_proj = store_thm("leaves_proj_diff_subset_leaves_diff_proj",
``!PROB vs S.  scc_vs(PROB, vs) /\ childless_vs(PROB, vs) /\ scc_set PROB S
            ==> member_leaves(prob_proj(PROB,
                                         (prob_dom PROB) DIFF
                                              (vs UNION BIGUNION (single_child_ancestors PROB vs))), S)
                         SUBSET
                           (member_leaves(prob_proj(PROB, (prob_dom PROB) DIFF vs), S)
                                           DIFF ( single_child_ancestors PROB vs))``,
SRW_TAC[][member_leaves_def, INTER_DEF, SUBSET_DEF]
THEN `scc_set PROB (single_child_ancestors PROB vs)` by SRW_TAC[][scc_set_def, single_child_ancestors_def]
THEN1(FULL_SIMP_TAC(srw_ss())[prob_proj_dom_diff_eq_prob_proj_prob_proj_dom_diff]
      THEN METIS_TAC[SIMP_RULE(srw_ss())[GSPEC_ETA, problem_scc_set_def, SUBSET_DEF, IN_DEF] (proj_prob_scc_set_subset
                           |> Q.SPEC`prob_proj(PROB, (prob_dom PROB) DIFF vs)`
                           |> Q.SPEC `(single_child_ancestors PROB vs)`),
                     two_pow_n_is_a_bound_2, scc_set_proj_ancestors])
THEN1(FULL_SIMP_TAC(srw_ss())[prob_proj_dom_diff_eq_prob_proj_prob_proj_dom_diff]
      THEN MP_TAC(scc_set_proj_ancestors |> Q.SPECL [`PROB`, `vs`])
      THEN SRW_TAC[][]
      THEN MP_TAC(SIMP_RULE(srw_ss())[GSPEC_ETA, problem_scc_set_def, SUBSET_DEF, IN_DEF] (proj_prob_scc_set_subset
           |> Q.SPEC`(prob_proj (PROB,(prob_dom PROB) DIFF vs))`
           |> Q.SPEC `(single_child_ancestors PROB vs)`))
      THEN SRW_TAC[][]
      THEN `DISJOINT x (BIGUNION (single_child_ancestors PROB vs))` by METIS_TAC[SCC_SUBSET_DOM, SUBSET_PROJ_DOM_DISJ]
      THEN `x ∉ ((single_child_ancestors PROB vs))`
        by (FULL_SIMP_TAC(srw_ss())[scc_vs_def, SCC_def]
        THEN METIS_TAC[SIMP_RULE(srw_ss())[] disj_big_union_not_mem])
      THEN MP_TAC(every_child_in_sccset |> Q.SPECL [`prob_proj (PROB,(prob_dom PROB) DIFF vs)`, `x`, `(single_child_ancestors PROB vs)`])
      THEN SRW_TAC[][]
      THEN Cases_on `x = vs`
      THENL
      [
        METIS_TAC[childless_imp_childless_proj]
        ,
        `DISJOINT x vs` by METIS_TAC[neq_scc_imp_disj, scc_set_def]
        THEN `scc_vs(PROB, x)` by FULL_SIMP_TAC(srw_ss())[scc_set_def]
        THEN MP_TAC(proj_dep_scc_vs_imp_dep_scc |> Q.SPECL[`PROB`,`single_child_ancestors PROB vs`,
                                           `vs`,`x`])
        THEN SRW_TAC[][]
        THEN `childless_vs(PROB, x)` by METIS_TAC[SIMP_RULE(srw_ss())[]not_childless_is_in_ancestors
                        |> Q.SPECL [`PROB`, `x`, `vs`]]
        THEN METIS_TAC[childless_imp_childless_proj]
     ])
THEN1(FULL_SIMP_TAC(srw_ss())[prob_proj_dom_diff_eq_prob_proj_prob_proj_dom_diff]
      THEN MP_TAC(scc_set_proj_ancestors |> Q.SPECL [`PROB`, `vs`])
      THEN SRW_TAC[][]
      THEN MP_TAC(SIMP_RULE(srw_ss())[GSPEC_ETA, problem_scc_set_def, SUBSET_DEF, IN_DEF] (proj_prob_scc_set_subset
           |> Q.SPEC`(prob_proj (PROB,(prob_dom PROB) DIFF vs))`
           |> Q.SPEC `(single_child_ancestors PROB vs)`))
     THEN SRW_TAC[][]
     THEN `DISJOINT x (BIGUNION (single_child_ancestors PROB vs))` by METIS_TAC[SCC_SUBSET_DOM, SUBSET_PROJ_DOM_DISJ]
     THEN `x ∉ ((single_child_ancestors PROB vs))`
       by (FULL_SIMP_TAC(srw_ss())[scc_vs_def, SCC_def]
       THEN METIS_TAC[SIMP_RULE(srw_ss())[] disj_big_union_not_mem])
))

val scc_vs_scc_set_vs_insert_ancestors = store_thm("scc_vs_scc_set_vs_insert_ancestors",
``!PROB vs. scc_vs(PROB, vs) ==> scc_set PROB (vs INSERT (single_child_ancestors PROB vs))``,
SRW_TAC[][single_child_ancestors_def, scc_set_def]
THEN METIS_TAC[]);

val scc_set_ancestors = store_thm("scc_set_ancestors",
``!PROB vs. scc_set PROB (single_child_ancestors PROB vs)``,
FULL_SIMP_TAC(srw_ss())[scc_set_def, single_child_ancestors_def]
      THEN METIS_TAC[])

val subset_not_scc_in_proj = store_thm("subset_not_scc_in_proj",
``!PROB vs vs'. vs SUBSET vs' ==> ~scc_vs(prob_proj(PROB, (prob_dom PROB) DIFF vs'), vs)``,
SRW_TAC[][]
THEN SPOSE_NOT_THEN STRIP_ASSUME_TAC
THEN `DISJOINT vs (prob_dom(prob_proj (PROB,(prob_dom PROB) DIFF vs')))`
   by (SRW_TAC[][  PROB_DOM_PROJ_DIFF]
       THEN FULL_SIMP_TAC(srw_ss())[DISJOINT_DEF, INTER_DEF, EXTENSION, DIFF_DEF, SUBSET_DEF]
       THEN METIS_TAC[])
THEN MP_TAC(SCC_SUBSET_DOM |> Q.SPECL [`prob_proj (PROB,(prob_dom PROB) DIFF vs')`, `vs`])
THEN STRIP_TAC
THEN FULL_SIMP_TAC(srw_ss())[DISJOINT_DEF, INTER_DEF, EXTENSION, SUBSET_DEF, scc_vs_def, SCC_def]
THEN METIS_TAC[])

val vs_leaf_imp_wo_vs_leaves = store_thm("vs_leaf_imp_wo_vs_leaves",
``!S. FINITE S ==>
     (!PROB vs S'. scc_set PROB S /\ scc_vs(PROB, vs)
                   /\ ~(vs IN S') /\ (member_leaves(PROB, S) = vs INSERT S')
                   ==> (member_leaves(problem_wo_vs_ancestors(PROB, vs), S) = S'))``,
SRW_TAC[][]
THEN Q.PAT_X_ASSUM `member_leaves (PROB,S') = vs INSERT S''` (ASSUME_TAC o GSYM)
THEN FULL_SIMP_TAC(srw_ss())[DELETE_NON_ELEMENT]
THEN MP_TAC(scc_vs_imp_mem_leaves_del |> Q.SPECL [`PROB`, `S'`, `vs`])
THEN SRW_TAC[][DELETE_INSERT]
THEN FULL_SIMP_TAC(srw_ss())[problem_wo_vs_ancestors_def]
THEN MP_TAC(scc_vs_scc_set_vs_insert_ancestors |> Q.SPECL [`PROB`, `vs`])
THEN SRW_TAC[][]
THEN `childless_vs(PROB, vs)` by (METIS_TAC[insert_in, leaf_is_childless])
THEN MP_TAC(proj_mem_leaves_subset_mem_leaves_union_ancestors |> Q.SPECL [`PROB`, `S'`, `vs`])
THEN SRW_TAC[][]
THEN `~(vs IN member_leaves(prob_proj
          (PROB,
              (prob_dom PROB) DIFF
               (vs ∪ BIGUNION (single_child_ancestors PROB vs))),S'))`
   by (SRW_TAC[][member_leaves_def] THEN METIS_TAC[subset_not_scc_in_proj,SUBSET_UNION])
THEN MP_TAC(leaves_proj_diff_subset_leaves_diff_proj |> Q.SPECL[`PROB`, `vs`, `S'`])
THEN SRW_TAC[][]
THEN `member_leaves
   (prob_proj
     (PROB,
      (prob_dom PROB) DIFF
      (vs ∪ BIGUNION (single_child_ancestors PROB vs))),S')
           = member_leaves (PROB,S') DELETE vs` by METIS_TAC[UNION_COMM, scc_vs_imp_mem_leaves_del0, INSERT_DEF]
THEN FULL_SIMP_TAC(srw_ss())[DELETE_DEF, INSERT_DEF, EXTENSION, GSPEC_ETA]
THEN METIS_TAC[]);

val sccs_big_union_diff_eq_diff_big_union = store_thm("sccs_big_union_diff_eq_diff_big_union",
``!PROB S S'. scc_set PROB S /\ scc_set PROB S'
              ==> (BIGUNION (S DIFF S') = (BIGUNION S) DIFF (BIGUNION S'))``,
SRW_TAC[][]
THEN `! vs vs'. vs IN S' /\ vs' IN S'' /\ ~(vs = vs')
           ==> DISJOINT vs vs'`
      by (SRW_TAC[][]
         THEN METIS_TAC[neq_scc_imp_disj, scc_set_def])
THEN METIS_TAC[bigunion_diff_eq_diff_bigunion]);

val leaves_diff_distrib = store_thm("leaves_diff_distrib",
``!PROB vs vs'. member_leaves (PROB,vs) DIFF vs' = member_leaves (PROB,vs DIFF vs')``,
(SRW_TAC[][member_leaves_def, INTER_DEF, DIFF_DEF, GSPEC_ETA, IN_DEF]
THEN PROVE_TAC[]));

val not_in_leaves_of_ancestors = store_thm("not_in_leaves_of_ancestors",
``!PROB vs S. ~(vs IN member_leaves (problem_wo_vs_ancestors (PROB,vs),S))``,
SRW_TAC[][problem_wo_vs_ancestors_def, member_leaves_def]
THEN METIS_TAC[member_leaves_def, SUBSET_UNION, subset_not_scc_in_proj]);

val ancestors_leaves_disj = store_thm("ancestors_leaves_disj",
``!PROB vs S. DISJOINT (single_child_ancestors PROB vs) (member_leaves(problem_wo_vs_ancestors (PROB, vs), S))``,
SRW_TAC[][problem_wo_vs_ancestors_def, member_leaves_def, DISJOINT_DEF, INTER_DEF, GSPEC_ETA, EMPTY_DEF]
THEN `!x. x IN (single_child_ancestors PROB vs) ==> x SUBSET( vs UNION (BIGUNION (single_child_ancestors PROB vs)))`
       by METIS_TAC[SUBSET_BIGUNION_I, subset_imp_subset_union, UNION_COMM]
THEN METIS_TAC[member_leaves_def, SUBSET_UNION, subset_not_scc_in_proj]);

val scc_leaves_eq_leaves_diff_single_child_ancestors = store_thm("scc_leaves_eq_leaves_diff_single_child_ancestors",
``!PROB vs S.
      scc_vs (PROB,vs) /\ childless_vs (PROB,vs) /\  scc_set PROB S
      ==> (member_leaves (problem_wo_vs_ancestors (PROB,vs),S)
                   = member_leaves (problem_wo_vs_ancestors (PROB,vs),
                                    S DIFF (vs INSERT single_child_ancestors PROB vs)))``,
SRW_TAC[][SET_EQ_SUBSET]
THEN REWRITE_TAC[GSYM leaves_diff_distrib]
THENL
[
  `member_leaves (problem_wo_vs_ancestors (PROB,vs),S') DIFF
    (vs INSERT single_child_ancestors PROB vs) =
         member_leaves (problem_wo_vs_ancestors (PROB,vs),S') DIFF
             (single_child_ancestors PROB vs)`
    by METIS_TAC[DIFF_INSERT, DELETE_NON_ELEMENT, not_in_leaves_of_ancestors]
  THEN SRW_TAC[][]
  THEN METIS_TAC[ancestors_leaves_disj, DISJOINT_SYM, SUBSET_REFL, SUBSET_DIFF]
  ,
  METIS_TAC[DIFF_SUBSET]
]);

val leaves_inter_eq_inter_leaves = store_thm("leaves_inter_eq_inter_leaves",
``!PROB vs vs'.
     member_leaves (PROB,vs) INTER vs' = member_leaves (PROB,vs INTER vs')``,
SRW_TAC[][member_leaves_def, INTER_DEF, DIFF_DEF, GSPEC_ETA, IN_DEF]
THEN PROVE_TAC[]);

val scc_subset_dom = store_thm("scc_subset_dom",
``!PROB vs. scc_vs(PROB, vs) ==> vs SUBSET (prob_dom PROB)``,
SRW_TAC[][scc_vs_def, SCC_def]
THEN REWRITE_TAC[SUBSET_DEF]
THEN METIS_TAC[dep_tc_imp_in_dom, dep_tc_def])

val ndisj_scc_imp_eq = store_thm("ndisj_scc_imp_eq",
``!PROB vs vs'. ~(DISJOINT vs vs') /\ scc_vs(PROB, vs) /\ scc_vs(PROB, vs') ==> (vs = vs')``,
METIS_TAC[scc_disjoint_lemma, scc_vs_def])

val scc_vs_subset_bigunion = store_thm("scc_vs_subset_bigunion",
``!PROB S vs. (prob_dom PROB) SUBSET BIGUNION S /\ scc_set PROB S /\ scc_vs(PROB, vs)
              ==> vs IN S``,
SRW_TAC[][]
THEN `!v. v IN vs ==> vs IN S'`
   by 
     (`vs SUBSET BIGUNION S'` by METIS_TAC[scc_subset_dom, SUBSET_TRANS]
      THEN SRW_TAC[][]
      THEN `?vs'. vs' IN S' /\ v IN vs'` by METIS_TAC[IN_BIGUNION, SUBSET_DEF]
      THEN FULL_SIMP_TAC(bool_ss)[scc_set_def]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `vs' : 'a -> bool`)
      THEN SRW_TAC[][DISJOINT_DEF]
      THEN MP_TAC(scc_subset_dom |> Q.SPECL[`PROB`, `vs`])
      THEN SRW_TAC[][SUBSET_DEF]
      THEN FULL_SIMP_TAC(srw_ss())[INTER_DEF, SUBSET_DEF, GSPEC_ETA, GSYM MEMBER_NOT_EMPTY]
      THEN `scc_vs(PROB, vs')` by METIS_TAC[]
      THEN `~(DISJOINT vs vs')`
           by (SRW_TAC[][DISJOINT_DEF,INTER_DEF, GSPEC_ETA, GSYM MEMBER_NOT_EMPTY]
               THEN Q.EXISTS_TAC `v` THEN SRW_TAC[][])
      THEN METIS_TAC[ndisj_scc_imp_eq])
THEN FULL_SIMP_TAC(srw_ss())[scc_vs_def, SCC_def |> Q.SPECL [`(\v1' v2'. dep (PROB,v1',v2'))`, `vs`]]
THEN METIS_TAC[MEMBER_NOT_EMPTY]);
 
val childless_eq_leaves = store_thm("childless_eq_leaves",
``!PROB S. (prob_dom PROB) SUBSET BIGUNION S /\ scc_set PROB S
           ==> (childless_problem_scc_set(PROB) = member_leaves(PROB, S))``,
SRW_TAC[][childless_problem_scc_set_def, member_leaves_def, EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN METIS_TAC[scc_vs_subset_bigunion])

val empty_dom_no_scc = store_thm("empty_dom_no_scc",
``!PROB. ((prob_dom PROB) = EMPTY) ==> (!vs. ~scc_vs(PROB, vs))``,
SRW_TAC[][]
THEN SPOSE_NOT_THEN STRIP_ASSUME_TAC
THEN FULL_SIMP_TAC(bool_ss)[GSYM MEMBER_NOT_EMPTY, scc_vs_def, SCC_def]
THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF, EXTENSION]
THEN METIS_TAC[])

val single_child_subset_scc_set = store_thm("single_child_subset_scc_set",
``!PROB vs S. scc_set PROB S /\ scc_vs(PROB, vs) /\ (prob_dom PROB) SUBSET BIGUNION S 
              ==> single_child_ancestors PROB vs SUBSET S``,
SRW_TAC[][]
THEN SRW_TAC[][SUBSET_DEF]
THEN MP_TAC(scc_set_ancestors |> Q.SPECL[`PROB`,`vs`])
THEN SRW_TAC[][scc_set_def]
THEN METIS_TAC[scc_vs_subset_bigunion])

val _ = export_theory();
