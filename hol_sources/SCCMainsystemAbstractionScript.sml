open HolKernel Parse boolLib bossLib;
open finite_mapTheory;
open arithmeticTheory;
open pred_setTheory;
open rich_listTheory;
open listTheory;						 
open utilsTheory;
open factoredSystemTheory 
open sublistTheory;
open systemAbstractionTheory;
open relationTheory;
open SCCTheory;
open SCCsystemAbstractionTheory;
val _ = new_theory "SCCMainsystemAbstraction";

val _ = export_theory();
