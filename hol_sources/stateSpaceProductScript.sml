open HolKernel Parse boolLib bossLib finite_mapTheory pred_setTheory lcsymtacs;
(*;*)
open set_utilsTheory
open factoredSystemTheory
open systemAbstractionTheory
open utilsTheory

val _ = new_theory "stateSpaceProduct";
 
val stateSpaceProduct_def = 
      Define`stateSpaceProduct (ss1) (ss2)
                        = IMAGE (\ (s1,s2). (FUNION s1 s2)) (ss1 CROSS ss2)`;

val stateSpaceProduct_comm_thm = store_thm("stateSpaceProduct_comm_thm",
``!ss1 ss2 vs1 vs2. stateSpace ss1 vs1 /\ stateSpace ss2 vs2 /\ DISJOINT vs1 vs2
                    ==> ((stateSpaceProduct ss1 ss2) = (stateSpaceProduct ss2 ss1))``,
SRW_TAC[][stateSpaceProduct_def, EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN `DISJOINT (FDOM (FST x')) (FDOM (SND x'))` by
                (FULL_SIMP_TAC(srw_ss())[stateSpace_def]
                 THEN METIS_TAC[DISJOINT_SYM])
THEN(Q.EXISTS_TAC `(SND x', FST x')`
     THEN SRW_TAC[][]
     THEN SRW_TAC[][FUNION_COMM, DISJOINT_SYM, Once (GSYM pairTheory.PAIR)]
     THEN SRW_TAC[][pairTheory.UNCURRY]))

val stateSpaceProduct_assoc_thm = store_thm("stateSpaceProduct_assoc_thm",
``!ss1 ss2 ss3. (stateSpaceProduct ss1 (stateSpaceProduct ss2 ss3)) = (stateSpaceProduct (stateSpaceProduct ss1 ss2) ss3)``,
SRW_TAC[][stateSpaceProduct_def, EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][FUNION_ASSOC, pairTheory.UNCURRY]
THEN1(Q.EXISTS_TAC `(FUNION (FST x') (FST x''), SND x'')`
      THEN SRW_TAC[][FUNION_ASSOC]
      THEN Q.EXISTS_TAC `((FST x'), (FST x''))`
      THEN SRW_TAC[][])
THEN1(Q.EXISTS_TAC `(FST x'', FUNION (SND x'') (SND x'))`
      THEN SRW_TAC[][FUNION_ASSOC]
      THEN Q.EXISTS_TAC `((SND x''), (SND x'))`
      THEN SRW_TAC[][]))

val stateSpaceProduct_left_comm_thm = store_thm("stateSpaceProduct_left_comm_thm",
``!ss1 ss2 ss3 vs1 vs2 vs3. stateSpace ss1 vs1 /\ stateSpace ss2 vs2 /\ stateSpace ss3 vs3
                            /\ DISJOINT vs1 vs2 /\ DISJOINT vs1 vs3
                            ==> (stateSpaceProduct ss1 (stateSpaceProduct ss2 ss3) = 
                                     stateSpaceProduct ss2 (stateSpaceProduct ss1 ss3))``,
METIS_TAC[stateSpaceProduct_comm_thm, stateSpaceProduct_assoc_thm,
          LCOMM_THM_gen, DISJOINT_UNION_thm])

val invariantStateSpace_thm_7 = store_thm("invariantStateSpace_thm_7",
``!ss dom vs1 vs2. stateSpace ss dom /\ dom SUBSET (vs1 UNION vs2)
               ==> ss SUBSET stateSpaceProduct (ss_proj ss vs1) (ss_proj ss vs2)``,
SRW_TAC[][]
THEN SRW_TAC[][SUBSET_DEF, stateSpaceProduct_def]
THEN Q.EXISTS_TAC `(DRESTRICT x vs1, DRESTRICT x vs2)`
THEN SRW_TAC[][pairTheory.UNCURRY]
THEN1(FULL_SIMP_TAC(srw_ss())[fmap_EXT, DRESTRICT_DEF, FDOM_DRESTRICT, INTER_DEF, EXTENSION, SUBSET_DEF, FUNION_DEF, stateSpace_def]
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN SRW_TAC[][ss_proj_def] 
THEN Q.EXISTS_TAC `x`
THEN SRW_TAC[][])

val invariantStateSpace_thm_8 = store_thm("invariantStateSpace_thm_8",
``!ss1 ss2 ss3 ss4. ss1 SUBSET ss3 /\ ss2 SUBSET ss4 ==> stateSpaceProduct ss1 ss2 SUBSET stateSpaceProduct ss3 ss4``,
SRW_TAC[][]
THEN SRW_TAC[][SUBSET_DEF, stateSpaceProduct_def]
THEN Q.EXISTS_TAC `(FST x', SND x')`
THEN SRW_TAC[][pairTheory.UNCURRY]
THEN METIS_TAC[SUBSET_DEF])

val ss_proj_PRODf_eq_PRODf_1 = store_thm("ss_proj_PRODf_eq_PRODf_1",
``!ss1 ss2 vs. stateSpaceProduct (ss_proj ss1 vs) (ss_proj ss2 vs) = (ss_proj (stateSpaceProduct ss1 ss2) vs)``,
SRW_TAC[][stateSpaceProduct_def, EXTENSION, ss_proj_def]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1(Q.EXISTS_TAC `FUNION s s'`
      THEN SRW_TAC[][]
      THEN SRW_TAC[][pairTheory.UNCURRY, fmap_EXT, FUNION_DEF]
      THEN1(FULL_SIMP_TAC(srw_ss())[UNION_DEF, FDOM_DRESTRICT, EXTENSION, fmap_EXT] THEN METIS_TAC[])
      THEN1(FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT, EXTENSION, fmap_EXT, FUNION_DEF] 
            (*THEN EQ_TAC*)
            THEN(FULL_SIMP_TAC(srw_ss())[DRESTRICT_DEF,FUNION_DEF] THEN SRW_TAC[][] THEN METIS_TAC[]) )
      THEN1(FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT, EXTENSION, fmap_EXT, FUNION_DEF]
            (*THEN EQ_TAC*)
            THEN(FULL_SIMP_TAC(srw_ss())[DRESTRICT_DEF,FUNION_DEF] THEN SRW_TAC[][] THEN METIS_TAC[]) )      
      THEN1(Q.EXISTS_TAC `(s, s')`
            THEN SRW_TAC[][]))
THEN1(Q.EXISTS_TAC `((DRESTRICT (FST x') vs), (DRESTRICT (SND x') vs))`
      THEN SRW_TAC[][]
      THEN SRW_TAC[][pairTheory.UNCURRY, fmap_EXT, FUNION_DEF]
      THEN1(FULL_SIMP_TAC(srw_ss())[UNION_DEF, FDOM_DRESTRICT, EXTENSION, fmap_EXT] THEN METIS_TAC[])
      THEN1(FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT, EXTENSION, fmap_EXT, FUNION_DEF] 
            (*THEN EQ_TAC*)
            THEN(FULL_SIMP_TAC(srw_ss())[DRESTRICT_DEF,FUNION_DEF] THEN SRW_TAC[][] THEN METIS_TAC[]) )
      THEN1(Q.EXISTS_TAC `FST x'`
            THEN SRW_TAC[][])
      THEN1(Q.EXISTS_TAC `SND x'`
            THEN SRW_TAC[][])))

val sspaceprod_ssproj_eq_ssproj_ssprod = store_thm("sspaceprod_ssproj_eq_ssproj_ssprod",
``!ss1 ss2 vs. stateSpaceProduct (ss_proj ss1 vs) (ss_proj ss2 vs) SUBSET
                      (ss_proj (stateSpaceProduct ss1 ss2) vs)``,
SRW_TAC[][stateSpaceProduct_def, SUBSET_DEF, ss_proj_def]
THEN Q.EXISTS_TAC `FUNION s s'`
THEN SRW_TAC[][pairTheory.UNCURRY]
THEN1(FULL_SIMP_TAC(srw_ss())[fmap_EXT, DRESTRICT_DEF, FDOM_DRESTRICT, INTER_DEF, EXTENSION, SUBSET_DEF, FUNION_DEF]
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN1(Q.EXISTS_TAC `(s, s')`
      THEN SRW_TAC[][pairTheory.UNCURRY]))

val (PROD_rules,PROD_ind, PROD_cases) =  Hol_reln`
  (∀ss. PROD {} ss ss) /\
  (∀ss SS ss' ss''.
       ss ∉ SS ∧ PROD SS ss' ss'' ==>
       PROD (ss INSERT SS) ss' (stateSpaceProduct ss ss''))`

val veq = rpt BasicProvers.VAR_EQ_TAC

val PROD_empty = store_thm(
  "PROD_empty[simp]",
  ``PROD ∅ a b ⇔ (a = b)``,
  simp[Once PROD_cases] >> metis_tac[]);

val FINITE_PROD_result = store_thm(
  "FINITE_PROD_result",
  ``∀s. FINITE s ⇒ ∀a. ∃b. PROD s a b``,
  Induct_on `FINITE` >> conj_tac
  >- metis_tac[PROD_rules] >>
  metis_tac[PROD_rules, FINITE_INSERT]);

val PROD_FINITE = store_thm(
  "PROD_FINITE",
  ``∀s a b. PROD s a b ⇒ FINITE s``,
  Induct_on `PROD` >> simp[]);

val IN_INSERT_EXISTS = store_thm(
  "IN_INSERT_EXISTS",
  ``x IN s <=> ?s0. (s = x INSERT s0) /\ x NOTIN s0``,
  dsimp[EQ_IMP_THM] >> strip_tac >> qexists_tac `s DELETE x` >>
  simp[]);

val PROD_det = store_thm(
  "PROD_det",
  ``∀s a b. PROD s a b ⇒
            (∀ss1 ss2. ss1 ∈ s ∧ ss2 ∈ s ∧ ss1 ≠ ss2 ==> (? vs1 vs2. stateSpace ss1 vs1 /\ stateSpace ss2 vs2 /\ (DISJOINT vs1 vs2)))
            /\ (!ss. ss IN s ==> ?vs. stateSpace ss vs)
            ⇒
            ∀c. PROD s a c ⇒ (b = c)``,
  gen_tac >> completeInduct_on `CARD s` >>
  full_simp_tac (srw_ss() ++ boolSimps.DNF_ss) [AND_IMP_INTRO] >>
  qx_genl_tac [`s`, `a`, `b`, `c`] >> strip_tac >> veq >>
  `(CARD s = 0) ∨ ∃n. CARD s = SUC n` by (Cases_on `CARD s` >> simp[])
  >- (`FINITE s` by metis_tac [PROD_FINITE] >> fs[]) >>
  Q.UNDISCH_THEN `PROD s a b` mp_tac >> simp[Once PROD_cases] >>
  strip_tac >> fs[] >> veq >>
  Q.RENAME1_TAC `PROD (ss INSERT s) a c` >>
  Q.RENAME1_TAC `PROD s a a0` >>
  `FINITE s` by metis_tac[PROD_FINITE] >> fs[] >> veq >>
  Q.UNDISCH_THEN `PROD (ss INSERT s) a c` mp_tac >>
  simp[Once PROD_cases] >>
  disch_then (qx_choosel_then [`ss2`, `s2`, `a2`]
                              strip_assume_tac) >>
  veq >> Cases_on `ss = ss2` >> fs[]
  >- (`s = s2` suffices_by
        (strip_tac >> fs[] >>
         first_x_assum (qspecl_then [`s2`, `a`, `a0`] mp_tac) >>
         simp[] >> metis_tac[]) >>
      FULL_SIMP_TAC(srw_ss())[EXTENSION, IN_DEF] >> SRW_TAC[][] >> metis_tac[])
 >>
  `∃s0. (s = ss2 INSERT s0) ∧ (s2 = ss INSERT s0)`
    by (`ss2 ∈ ss INSERT s` by metis_tac[IN_INSERT] >>
        fs[] >> fs[] >>
        `?s0. (s = ss2 INSERT s0) /\ ss2 NOTIN s0` by metis_tac[IN_INSERT_EXISTS] >>
        rw[] >> qexists_tac `s0` >> simp[] >>
        Q.UNDISCH_THEN `ss INSERT ss2 INSERT s0 = ss2 INSERT s2` mp_tac >>
        FULL_SIMP_TAC(srw_ss())[EXTENSION, IN_DEF]>>
        SRW_TAC[][] >>
        metis_tac[]) >>
  veq >> fs[] >>
  `∃a00. PROD s0 a a00` by metis_tac[FINITE_PROD_result] >>
  `PROD (ss INSERT s0) a (stateSpaceProduct ss a00)` by metis_tac [PROD_rules] >>
  `stateSpaceProduct ss a00 = a2`
    by (first_x_assum (qspecl_then [`ss INSERT s0`, `a`, `a2`] mp_tac) >>
        SRW_TAC[][]>>   
        first_x_assum (qspec_then `stateSpaceProduct ss a00` mp_tac) >> 
        SRW_TAC[][] >>
        `(∀ss1 ss2'.
           ((ss1 = ss) ∨ ss1 ∈ s0) ∧ ((ss2' = ss) ∨ ss2' ∈ s0) ∧
           ss1 ≠ ss2' ==> ? vs1 vs2. stateSpace ss1 vs1 ∧ stateSpace ss2' vs2 /\ DISJOINT vs1 vs2)` by METIS_TAC[] >> 
        `(∀ss'. (ss' = ss) ∨ ss' ∈ s0 ⇒ ∃vs. stateSpace ss' vs) ∧
         PROD (ss INSERT s0) a (stateSpaceProduct ss a00) ⇒
         (a2 = stateSpaceProduct ss a00)` by METIS_TAC[]>>
        `(∀ss'. (ss' = ss) ∨ ss' ∈ s0 ⇒ ∃vs. stateSpace ss' vs)` by METIS_TAC[] >>
        `PROD (ss INSERT s0) a (stateSpaceProduct ss a00) ⇒
         (a2 = stateSpaceProduct ss a00)` by METIS_TAC[]>> METIS_TAC[])>>
  veq >>
  `PROD (ss2 INSERT s0) a (stateSpaceProduct ss2 a00)` by metis_tac[PROD_rules] >>
  `a0 = stateSpaceProduct ss2 a00`
    by (first_x_assum (qspecl_then [`ss2 INSERT s0`, `a`, `a0`] mp_tac) >>
        dsimp[]>> SRW_TAC[][]>>
        first_x_assum (qspecl_then [`stateSpaceProduct ss2 a00`] mp_tac) >>
        dsimp[]>> SRW_TAC[][]>>
        `(∀ss2' vs1 vs2.
          ss2' ∈ s0 ∧ ss2 ≠ ss2' ∧ stateSpace ss2 vs1 ∧
          stateSpace ss2' vs2 ⇒
          DISJOINT vs1 vs2)` by METIS_TAC[]>>
        `(∀ss1 vs1 vs2.
          ss1 ∈ s0 ∧ ss1 ≠ ss2 ∧ stateSpace ss1 vs1 ∧
          stateSpace ss2 vs2 ⇒
          DISJOINT vs1 vs2)` by METIS_TAC[]>>
        `(∀ss1 ss2' vs1 vs2.
          ss1 ∈ s0 ∧ ss2' ∈ s0 ∧ ss1 ≠ ss2' ∧ stateSpace ss1 vs1 ∧
          stateSpace ss2' vs2 ⇒
          DISJOINT vs1 vs2)` by METIS_TAC[]>>
        `(∀ss1 vs1 vs2.
           ss1 ∈ s0 ∧ ss1 ≠ ss2 ∧ stateSpace ss1 vs1 ∧
           stateSpace ss2 vs2 ⇒
           DISJOINT vs1 vs2) ∧
        (∀ss1 ss2' vs1 vs2.
           ss1 ∈ s0 ∧ ss2' ∈ s0 ∧ ss1 ≠ ss2' ∧ stateSpace ss1 vs1 ∧
           stateSpace ss2' vs2 ⇒
           DISJOINT vs1 vs2) ⇒
        (a0 = stateSpaceProduct ss2 a00)` by PROVE_TAC[]>>
        `(∀ss1 ss2' vs1 vs2.
           ss1 ∈ s0 ∧ ss2' ∈ s0 ∧ ss1 ≠ ss2' ∧ stateSpace ss1 vs1 ∧
           stateSpace ss2' vs2 ⇒
           DISJOINT vs1 vs2) ⇒
        (a0 = stateSpaceProduct ss2 a00)` by PROVE_TAC[]>>
        PROVE_TAC[]) >>
  veq >> simp[stateSpaceProduct_assoc_thm] >> 
  `∃vs vs'. stateSpace ss vs /\ stateSpace ss2 vs' /\ DISJOINT vs vs'` by METIS_TAC[] >>
  METIS_TAC[stateSpaceProduct_comm_thm]);

val PRODf_def = new_specification (
  "PRODf_def", ["PRODf"],
  SIMP_RULE (srw_ss()) [SKOLEM_THM, GSYM RIGHT_EXISTS_IMP_THM]
            FINITE_PROD_result);

val PRODf_EMPTY = store_thm(
  "PRODf_EMPTY[simp]",
  ``PRODf {} a = a``,
  `PROD {} a (PRODf {} a)` by (match_mp_tac PRODf_def >> simp[]) >>
  fs[]);

val PRODf_INSERT = store_thm(
   "PRODf_INSERT",
  ``∀e s. FINITE s ∧ e ∉ s /\ ~(e = EMPTY)
              /\ (∀ss1 ss2. ss1 ∈ s ∧ ss2 ∈ s ∧ ss1 ≠ ss2 ==> (?vs1 vs2.  stateSpace ss1 vs1 /\ stateSpace ss2 vs2 /\ DISJOINT vs1 vs2))
              /\ (!ss. ss IN s ==> ~(ss = EMPTY)) /\ (?vse. stateSpace e vse)
              /\ (∀ss. ss ∈ s ==> (?vse vs. stateSpace e vse /\ stateSpace ss vs /\ DISJOINT vse vs))
              ==> 
                 (∀a. PRODf (e INSERT s) a = stateSpaceProduct e (PRODf s a))``,
  rpt strip_tac >>
  `PROD (e INSERT s) a (PRODf (e INSERT s) a)`
    by (match_mp_tac PRODf_def >> simp[]) >>
  `PROD s a (PRODf s a)`
    by (match_mp_tac PRODf_def >> simp[]) >>
  `PROD (e INSERT s) a (stateSpaceProduct e (PRODf s a))`
    by metis_tac[PROD_rules] >>
  qspecl_then [`e INSERT s`, `a`, `PRODf (e INSERT s) a`] mp_tac PROD_det >>
  dsimp[AND_IMP_INTRO] >> 
  disch_then (qspec_then `stateSpaceProduct e (PRODf s a)` match_mp_tac) >> simp[] >>
  Q.EXISTS_TAC `vse` THEN
  metis_tac[DISJOINT_SYM, EQ_SS_DOM]);

val FINITE_stateSpaceProduct = store_thm("FINITE_stateSpaceProduct",
``!ss1 ss2. FINITE ss1 /\ FINITE ss2 ==> FINITE (stateSpaceProduct ss1 ss2)``,
SRW_TAC[][stateSpaceProduct_def])

val PRODf_FINITE = store_thm(
   "PRODf_FINITE",
``∀s.
     FINITE s ⇒
     ∀e.
       ~(e IN s) /\ e ≠ ∅ ∧ (?vse. stateSpace e vse) /\ (∀ss. ss ∈ s ⇒ ss ≠ ∅) ∧
       (∀ss1 ss2. 
          ss1 ∈ s ∧ ss2 ∈ s ∧ ss1 ≠ ss2 ==> (?vs1 vs2. stateSpace ss1 vs1 ∧ stateSpace ss2 vs2 /\ DISJOINT vs1 vs2)) /\
       (∀ss. ss ∈ s ==> (?vse vs. stateSpace e vse /\ stateSpace ss vs /\ DISJOINT vse vs)) /\
       FINITE e /\ (!ss. ss IN s ==> FINITE ss)
        ⇒
       FINITE (PRODf s e)``,
Induct_on `FINITE`
THEN SRW_TAC[][PRODf_EMPTY]
THEN MP_TAC(PRODf_INSERT |> Q.SPECL[`e`, `s`] )
THEN SRW_TAC[][]
THEN `(∀ss. ss ∈ s ==> (?vs vse. stateSpace e' vse /\ stateSpace ss vs /\ DISJOINT vse vs))` by METIS_TAC[]
THEN `(∀ss1 ss2.
          ss1 ∈ s ∧ ss2 ∈ s ∧ ss1 ≠ ss2 ==> (?vs1 vs2. stateSpace ss1 vs1 ∧ stateSpace ss2 vs2 /\ DISJOINT vs1 vs2))` by METIS_TAC[]
THEN `∀a. PRODf (e INSERT s) a = stateSpaceProduct e (PRODf s a)` by METIS_TAC[]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `e'`)
THEN SRW_TAC[][]
THEN LAST_X_ASSUM (MP_TAC o Q.SPECL[`e'`,`vse`])
THEN SRW_TAC[][]
THEN `(∀ss. ss ∈ s ==> (?vs vse. stateSpace e' vse /\ stateSpace ss vs /\ DISJOINT vse vs))` by METIS_TAC[]
THEN `(∀ss1 ss2.
          ss1 ∈ s ∧ ss2 ∈ s ∧ ss1 ≠ ss2 ==>
         (?vs1 vs2. stateSpace ss1 vs1 ∧
                   stateSpace ss2 vs2  /\ DISJOINT vs1 vs2))` by METIS_TAC[]
THEN `FINITE (PRODf s e')` by METIS_TAC[]
THEN METIS_TAC[FINITE_SS, FINITE_stateSpaceProduct])

val PRODf_FINITE' = store_thm(
   "PRODf_FINITE'",
``∀s e.
     FINITE s /\
       ~(e IN s) /\ e ≠ ∅ ∧ (?vse. stateSpace e vse) /\ (∀ss. ss ∈ s ⇒ ss ≠ ∅) ∧
       (∀ss1 ss2. 
          ss1 ∈ s ∧ ss2 ∈ s ∧ ss1 ≠ ss2 ==> (?vs1 vs2. stateSpace ss1 vs1 ∧ stateSpace ss2 vs2 /\ DISJOINT vs1 vs2)) /\
       (∀ss. ss ∈ s ==> (?vse vs. stateSpace e vse /\ stateSpace ss vs /\ DISJOINT vse vs)) /\
       FINITE e /\ (!ss. ss IN s ==> FINITE ss)
        ⇒
       FINITE (PRODf s e)``,
SRW_TAC[][PRODf_FINITE])

val stateSpaceProduct_CARD = store_thm(
   "stateSpaceProduct_CARD",
``!ss1 ss2. FINITE ss1 /\ FINITE ss2 
            ==> CARD (stateSpaceProduct ss1 ss2) <= CARD ss1 * CARD ss2``,
METIS_TAC[stateSpaceProduct_def, CARD_CROSS, CARD_IMAGE, arithmeticTheory.LESS_EQ_TRANS, FINITE_CROSS])

val PRODf_CARD = store_thm(
   "PRODf_CARD",
  ``∀s.
     FINITE s ⇒
     ∀e.
       ~(e IN s) /\ e ≠ ∅ ∧ (?vse. stateSpace e vse) /\ (∀ss. ss ∈ s ⇒ ss ≠ ∅) ∧
       (∀ss1 ss2. 
          ss1 ∈ s ∧ ss2 ∈ s ∧ ss1 ≠ ss2 ==> (?vs1 vs2. stateSpace ss1 vs1 ∧ stateSpace ss2 vs2 /\ DISJOINT vs1 vs2)) /\
       (∀ss. ss ∈ s ==> (?vse vs. stateSpace e vse /\ stateSpace ss vs /\ DISJOINT vse vs)) /\
       FINITE e /\ (!ss. ss IN s ==> FINITE ss)
        ⇒
       CARD (PRODf s e) ≤ Π CARD (e INSERT s)``,
Induct_on `FINITE`
THEN SRW_TAC[][]
THEN1 SRW_TAC[][PROD_IMAGE_DEF, ITSET_INSERT, ITSET_EMPTY]
THEN SRW_TAC[][Once INSERT_COMM]
THEN SRW_TAC[][PROD_IMAGE_THM]
THEN `(e' INSERT s) DELETE e = (e' INSERT s)` by (SRW_TAC[][Once EXTENSION] THEN METIS_TAC[])
THEN SRW_TAC[][]
THEN `?vse'. stateSpace e vse'` by METIS_TAC[]
THEN MP_TAC(PRODf_INSERT |> Q.SPECL[`e`, `s`])
THEN SRW_TAC[][]
THEN `(∀ss. ss ∈ s ==> (? vse' vs. stateSpace ss vs /\ stateSpace e vse' /\ DISJOINT vse' vs))` by METIS_TAC[]
THEN  `∀a. PRODf (e INSERT s) a = stateSpaceProduct e (PRODf s a)` by METIS_TAC[]
THEN SRW_TAC[][]
THEN LAST_ASSUM (MP_TAC o Q.SPECL[`e'`, `vse`])
THEN SRW_TAC[][]
THEN `CARD (PRODf s e') ≤ Π CARD (e' INSERT s)` by METIS_TAC[]
THEN MP_TAC(PRODf_FINITE |> Q.SPEC `s`)
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`e'`, `vse`])
THEN SRW_TAC[][]
THEN `FINITE (PRODf s e')` by METIS_TAC[]
THEN `FINITE e` by METIS_TAC[FINITE_SS]
THEN METIS_TAC[stateSpaceProduct_CARD, arithmeticTheory.MULT_COMM,
               arithmeticTheory.LESS_MONO_MULT, arithmeticTheory.LESS_EQ_TRANS]);

val _ = export_theory();
