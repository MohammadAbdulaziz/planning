(* fun cheat g = ACCEPT_TAC (mk_thm g) g *)

open HolKernel Parse boolLib bossLib;
open finite_mapTheory
open arithmeticTheory
open pred_setTheory
open rich_listTheory;
open sublistTheory;
open listTheory;
open set_utilsTheory;
open list_utilsTheory;
open HO_arith_utilsTheory;
open fmap_utilsTheory;

val _ = new_theory "factoredSystem";

val _ = type_abbrev("state", ``:'a |->bool``)
val _ = type_abbrev("action", ``:('a state# 'a state)``)
val _ = type_abbrev("problem", ``:('a state# 'a state) set``)

val action_dom_def = Define`action_dom (s1,s2) = FDOM s1 UNION FDOM s2`;

val action_dom_pair = store_thm("action_dom_pair",
``action_dom (a) = FDOM (FST a) UNION FDOM (SND a)``,
METIS_TAC[action_dom_def, pairTheory.FST, pairTheory.SND, pairTheory.PAIR]);

val prob_dom_def = Define`prob_dom PROB = BIGUNION (IMAGE action_dom PROB)`;

val valid_states_def = Define`valid_states PROB = {s | FDOM s = prob_dom PROB}`;

val valid_plans_def = Define`valid_plans PROB = {as | set as SUBSET PROB}`;

val state_succ_def
 = Define`state_succ s a = if (FST a ⊑  s) then ( FUNION (SND a) s ) else s`;

val state_succ_pair = store_thm("state_succ_pair",
``state_succ s (p,e) = if (p ⊑  s) then ( FUNION e s ) else s``,
METIS_TAC[state_succ_def, pairTheory.PAIR, pairTheory.FST, pairTheory.SND])

val exec_plan_def =
Define`(exec_plan(s, a::as) = exec_plan((state_succ s a ), as))
	 /\ (exec_plan(s, []) = s)`;


val exec_plan_Append = store_thm("exec_plan_Append",
  ``!as_a as_b s. exec_plan(s, as_a++as_b) = exec_plan(exec_plan(s,as_a),as_b)``,
	Induct_on `as_a` THEN1 FULL_SIMP_TAC (srw_ss()) [exec_plan_def]
 	THEN
 	REPEAT STRIP_TAC
 	THEN
 	FULL_SIMP_TAC (srw_ss()) [exec_plan_def]
);

val cycle_removal_lemma = store_thm("cycle_removal_lemma",
``!s as1 as2 as3. (exec_plan(s,as1++as2) = exec_plan(s,as1))
                   ==> (exec_plan(s, as1++as2++as3) = exec_plan(s, as1++as3)) ``,
 REPEAT STRIP_TAC
 THEN ASSUME_TAC exec_plan_Append
 THEN SRW_TAC [][]);

val construction_of_all_possible_states_lemma = store_thm(
"construction_of_all_possible_states_lemma",
``!e X. ( ~(e IN (X)))==>
 ({s:'a state | (FDOM s) = e INSERT X} = IMAGE (\s. s |+ (e,T)) {s:'a state | (FDOM s) = X} UNION
                                IMAGE (\s. s |+ (e,F)) {s:'a state | (FDOM s) = X})``,
     SRW_TAC [][Once pred_setTheory.EXTENSION]
     THEN SRW_TAC[][]
     THEN EQ_TAC
    	  THENL[
		SRW_TAC[][]
		THEN `FCARD (x) = CARD( e INSERT X)` by SRW_TAC[][CARD_DEF, FCARD_DEF]
		THEN CONV_TAC(OR_EXISTS_CONV)
		THEN Cases_on`x ' e`
		THENL
		[
			Q_TAC SUFF_TAC `x = ((x \\ e) |+ (e,T))`
			THENL
			[
				REPEAT STRIP_TAC
				THEN `FDOM (x \\ e) = X ` by (SRW_TAC[][]
				     THEN FULL_SIMP_TAC(srw_ss())[INSERT_DEF,DELETE_DEF, EXTENSION]
				     THEN STRIP_TAC
				     THEN EQ_TAC
				     THEN METIS_TAC[])
				THEN METIS_TAC[]
				,
			     	SRW_TAC[][fmap_EXT]
			     	THENL
				[
					EQ_TAC
			     	   	THEN1 FULL_SIMP_TAC(srw_ss()) []
				   	THEN FULL_SIMP_TAC(srw_ss()) []
					,
					EQ_TAC
					THENL
					[
						`~(x' = e) ` by (SRW_TAC[][]
						      	   THEN METIS_TAC[IN_DEF])
						THEN FULL_SIMP_TAC(srw_ss()) [FDOM_DEF, INSERT_DEF]
						THEN METIS_TAC[DOMSUB_FAPPLY_NEQ, FUPDATE_PURGE, NOT_EQ_FAPPLY]
						,
						`~(x' = e) ` by (SRW_TAC[][]
						      	   THEN METIS_TAC[IN_DEF])
						THEN FULL_SIMP_TAC(srw_ss()) [FDOM_DEF, INSERT_DEF]
						THEN METIS_TAC[DOMSUB_FAPPLY_NEQ, FUPDATE_PURGE, NOT_EQ_FAPPLY]
					]
				]
			]
			,
			Q_TAC SUFF_TAC `x = ((x \\ e) |+ (e,F))`
			THENL
			[
				REPEAT STRIP_TAC
				THEN `FDOM (x \\ e) = X ` by (SRW_TAC[][]
				     THEN FULL_SIMP_TAC(srw_ss())[INSERT_DEF,DELETE_DEF, EXTENSION]
				     THEN STRIP_TAC
				     THEN EQ_TAC
				     THEN METIS_TAC[])
				THEN METIS_TAC[]
				,
			     	SRW_TAC[][fmap_EXT]
			     	THENL
				[
					EQ_TAC
			     	   	THEN1 FULL_SIMP_TAC(srw_ss()) []
				   	THEN FULL_SIMP_TAC(srw_ss()) []
					,
					EQ_TAC
					THENL
					[
						`~(x' = e) ` by (SRW_TAC[][]
						      	   THEN METIS_TAC[IN_DEF])
						THEN FULL_SIMP_TAC(srw_ss()) [FDOM_DEF, INSERT_DEF]
						THEN METIS_TAC[DOMSUB_FAPPLY_NEQ, FUPDATE_PURGE, NOT_EQ_FAPPLY]
						,
						`~(x' = e) ` by (SRW_TAC[][]
						      	   THEN METIS_TAC[IN_DEF])
						THEN FULL_SIMP_TAC(srw_ss()) [FDOM_DEF, INSERT_DEF]
						THEN METIS_TAC[DOMSUB_FAPPLY_NEQ, FUPDATE_PURGE, NOT_EQ_FAPPLY]
					]
				]
			]
		]
	       ,
	       SIMP_TAC (srw_ss()) [GSYM EXISTS_OR_THM]
               THEN DISCH_THEN (Q.X_CHOOSE_THEN `s` ASSUME_TAC)
 	       THEN FULL_SIMP_TAC (srw_ss()) []
]);

val FINITE_states = store_thm("FINITE_states",
  ``!X. FINITE (X:'a set) ==> FINITE { s : 'a state | FDOM(s) = X}``,
  Induct_on `FINITE`  THEN SRW_TAC [][GSPEC_EQ, FDOM_EQ_EMPTY] THEN
  Q_TAC SUFF_TAC   `FINITE ((IMAGE (λ(s :α state). s |+ ((e :α),T)) {s | FDOM s = (X :α -> bool)}) UNION  (IMAGE (λ(s :α state). s |+ (e,F)) {s | FDOM s = X}))`
  THENL
  [
	REPEAT STRIP_TAC
	THEN METIS_TAC[construction_of_all_possible_states_lemma]
  ,
	 METIS_TAC[FINITELY_INJECTIVE_IMAGE_FINITE, FINITE_UNION, IMAGE_FINITE]
  ]);

val card_of_set_of_all_possible_states = store_thm("card_of_set_of_all_possible_states",
    ``! X. FINITE X ==> (CARD { s:'a state | FDOM(s) = X } = 2 EXP (CARD X))``,
     HO_MATCH_MP_TAC pred_setTheory.FINITE_INDUCT THEN REPEAT STRIP_TAC THENL
     [
	 FULL_SIMP_TAC(srw_ss())[GSPEC_EQ, FDOM_EQ_EMPTY]
         ,
     	 FULL_SIMP_TAC(srw_ss())[construction_of_all_possible_states_lemma, CARD_DEF, CARD_INSERT]
	 THEN `DISJOINT (IMAGE (λ(s :α state). s |+ ((e :α),T)) {s | FDOM s = (X :α -> bool)}) (IMAGE (λ(s :α state). s |+ (e,F)) {s | FDOM s = X})`
            by( SRW_TAC [][DISJOINT_DEF, Once EXTENSION] THEN
	        SPOSE_NOT_THEN STRIP_ASSUME_TAC THEN
		SRW_TAC[][] THEN
		METIS_TAC[SAME_KEY_UPDATES_DIFFER])
         THEN SRW_TAC[][card_union', FINITE_states]
      	 THEN Q.ISPEC_THEN `(λ(s:'a state). s |+ ((e :α),T))`
             (Q.ISPEC_THEN `{ s:'a state | FDOM(s) = X}` (MP_TAC o SIMP_RULE (srw_ss()) [FINITE_states])) CARD_INJ_IMAGE_2
         THEN REPEAT STRIP_TAC
      	 THEN `FINITE{s:'a state| FDOM(s) = X}` by METIS_TAC[FINITE_states]
      	 THEN `(!x:'a state y:'a state. (FDOM(x) = X) /\ (FDOM(y) = X) ==>
              ((x|+ (e,T) = y|+(e,T)) ⇔ (x = y)))` by METIS_TAC[FUPD11_SAME_NEW_KEY]
      	 THEN `CARD (IMAGE (λ(s:'a state). s |+ ((e :α),T)) {s:'a state | FDOM(s) = X}) = CARD ( {s:'a state | FDOM(s) = X} ) ` by
      	      FULL_SIMP_TAC(srw_ss())[ FINITE_states, GSPECIFICATION,  CARD_INJ_IMAGE]
      	 THEN Q.ISPEC_THEN `(λ(s:'a state). s |+ ((e :α),F))`
             (Q.ISPEC_THEN `{ s:'a state | FDOM(s) = X}` (MP_TAC o SIMP_RULE (srw_ss()) [FINITE_states])) CARD_INJ_IMAGE_2
         THEN REPEAT STRIP_TAC
      	 THEN `FINITE{s:'a state| FDOM(s) = X}` by METIS_TAC[FINITE_states]
      	 THEN `(!x:'a state y:'a state. (FDOM(x) = X) /\ (FDOM(y) = X) ==>
              ((x|+ (e,F) = y|+(e,F)) ⇔ (x = y)))` by METIS_TAC[FUPD11_SAME_NEW_KEY]
      	 THEN `CARD (IMAGE (λ(s:'a state). s |+ ((e :α),F)) {s:'a state | FDOM(s) = X}) = CARD ( {s:'a state | FDOM(s) = X} ) ` by
      	      FULL_SIMP_TAC(srw_ss())[ FINITE_states, GSPECIFICATION,  CARD_INJ_IMAGE]
      	 THEN ASM_SIMP_TAC (srw_ss()) []
      	 THEN `!x. 2**x + 2**x = 2**(SUC x)` by( FULL_SIMP_TAC(srw_ss())[EXP, MULT]
      	   	       	      	      	     THEN REPEAT STRIP_TAC
					     THEN Induct_on `x` THEN1 FULL_SIMP_TAC(srw_ss())[EXP, MULT, ADD]
					     THEN FULL_SIMP_TAC(srw_ss())[EXP, MULT, ADD]
					     THEN METIS_TAC[EQ_MULT_LCANCEL, ADD, LEFT_ADD_DISTRIB])
 	 THEN METIS_TAC[]
    ]
);

val state_list_def = Define`(
((state_list( s,  a::as)) = s :: (state_list( (state_succ s a), as)) )
    /\ (state_list( s, NIL) = [s]))`;

val empty_state_list_lemma = store_thm("empty_state_list_lemma",
``!as s. ~([] = state_list (s,as))``,
Induct_on`as`
THEN1 SRW_TAC[][state_list_def]
THEN SRW_TAC[][state_list_def]
);

val state_list_length_non_zero = store_thm("state_list_length_non_zero",
``!as s. ~(0 = LENGTH (state_list (s,as)) )``,
METIS_TAC[LENGTH_NIL, empty_state_list_lemma]);

val state_list_length_lemma = store_thm("state_list_length_lemma",
``!as s. LENGTH(as) = (LENGTH(state_list(s,as)) - 1)``,
Induct_on`as`
THEN SRW_TAC[][state_list_def]
THEN FULL_SIMP_TAC(srw_ss())[GSYM PRE_SUB1]
THEN METIS_TAC[PRE_SUC_EQ, NOT_ZERO_LT_ZERO, state_list_length_non_zero]);

val state_list_length_lemma_2 = store_thm("state_list_length_lemma_2",
``!as s. (LENGTH(state_list(s,as))) = (LENGTH(as)) + 1 ``,
SRW_TAC[][GSYM (SIMP_RULE(srw_ss())[GSYM PRE_SUB1, 
                          SIMP_RULE(srw_ss())[NOT_ZERO_LT_ZERO]
                             (GSYM state_list_length_non_zero),
                          PRE_SUC_EQ] state_list_length_lemma)]
THEN DECIDE_TAC);

val state_set_def = Define`
(state_set(s::ss) = [s] INSERT IMAGE (CONS s) (state_set ss)) /\
(state_set [] = {})`

val state_set_thm = store_thm(
  "state_set_thm",
  ``!s1. s1 IN state_set s2 <=> s1 <<= s2 /\ s1 <> []``,
  Induct_on `s2` THEN SRW_TAC [][state_set_def,rich_listTheory.IS_PREFIX_NIL] THEN
  Cases_on `s1` THEN SRW_TAC [][] THEN Cases_on `t = []` THEN SRW_TAC [][]);

val state_set_finite = store_thm(
  "state_set_finite",
  ``!X. FINITE (state_set X)``,
  Induct_on `X` THEN SRW_TAC [][state_set_def])
val _ = export_rewrites ["state_set_finite"]

val LENGTH_state_set = store_thm(
  "LENGTH_state_set",
  ``!X e. e IN state_set X ==> LENGTH e <= LENGTH X``,
  Induct_on `X` THEN SRW_TAC[][state_set_def] THEN SRW_TAC[][] THEN RES_TAC THEN DECIDE_TAC);

val lemma_temp = store_thm("lemma_temp",
``!x s as h. x IN state_set (state_list (s,as))
               ==> LENGTH((h::state_list (s,as))) > LENGTH(x)``,
SRW_TAC [][] THEN IMP_RES_TAC LENGTH_state_set THEN DECIDE_TAC);

val NIL_NOTIN_stateset = store_thm(
  "NIL_NOTIN_stateset",
  ``!X. [] NOTIN state_set X``,
  Induct_on `X` THEN SRW_TAC [][state_set_def]);
val _ = export_rewrites ["NIL_NOTIN_stateset"]

val state_set_card = store_thm(
  "state_set_card",
  ``!X. CARD (state_set X) = LENGTH X``,
  Induct_on `X` THEN SRW_TAC [][state_set_def] THEN
  SRW_TAC [][CARD_INJ_IMAGE]);

val _ = export_rewrites ["state_set_card"]

val FDOM_state_succ = store_thm(
  "FDOM_state_succ",
  ``FDOM (SND a) SUBSET	FDOM s ==> (FDOM (state_succ s a) = FDOM s)``,
  SRW_TAC [][state_succ_def] THEN
  SRW_TAC [][EXTENSION] THEN FULL_SIMP_TAC (srw_ss()) [SUBSET_DEF] THEN METIS_TAC[]);

val FDOM_state_succ_subset = store_thm(
  "FDOM_state_succ_subset",
  ``FDOM (state_succ s a) SUBSET ((FDOM s) UNION (FDOM (SND a)))``,
  SRW_TAC [][state_succ_def]);


		  
fun qispl_then [] ttac = ttac
  | qispl_then (q::qs) ttac = Q.ISPEC_THEN q (qispl_then qs ttac)

val FDOM_eff_subset_FDOM_valid_states = store_thm("FDOM_eff_subset_FDOM_valid_states",
``!p e s. (p,e) IN PROB /\ s IN valid_states PROB
     ==> (FDOM e) SUBSET FDOM s``,
FULL_SIMP_TAC(srw_ss())[valid_states_def, prob_dom_def]
THEN SRW_TAC[][ action_dom_def, IMAGE_DEF, BIGUNION, SUBSET_DEF, EXTENSION, UNION_DEF]
THEN Q.EXISTS_TAC `action_dom (p,e)`
THEN SRW_TAC[][]
THENL
[
  SRW_TAC[][action_dom_def]
  ,
  Q.EXISTS_TAC `(p,e)`
  THEN SRW_TAC[][]
])

val FDOM_eff_subset_FDOM_valid_states_pair = store_thm("FDOM_eff_subset_FDOM_valid_states_pair",
``!a s. a IN PROB /\ s IN valid_states PROB
     ==> (FDOM (SND a)) SUBSET FDOM s``,
METIS_TAC[FDOM_eff_subset_FDOM_valid_states, pairTheory.PAIR, pairTheory.SND])

val FDOM_pre_subset_FDOM_valid_states = store_thm("FDOM_pre_subset_FDOM_valid_states",
``!p e s. ((p,e)) IN PROB /\ (s) IN valid_states PROB
     ==> (FDOM p) SUBSET FDOM s``,
FULL_SIMP_TAC(srw_ss())[valid_states_def, prob_dom_def]
THEN SRW_TAC[][ action_dom_def, IMAGE_DEF, BIGUNION, SUBSET_DEF, EXTENSION, UNION_DEF]
THEN Q.EXISTS_TAC `action_dom (p,e)`
THEN SRW_TAC[][]
THENL
[
  SRW_TAC[][action_dom_def]
  ,
  Q.EXISTS_TAC `(p,e)`
  THEN SRW_TAC[][]
])

val FDOM_pre_subset_FDOM_valid_states_pair = store_thm("FDOM_pre_subset_FDOM_valid_states_pair",
``!a s. a IN PROB /\ s IN valid_states PROB
     ==> (FDOM (FST a)) SUBSET FDOM s``,
PROVE_TAC[FDOM_pre_subset_FDOM_valid_states, pairTheory.PAIR, pairTheory.FST])

val action_dom_subset_valid_states_FDOM = store_thm("action_dom_subset_valid_states_FDOM",
``!p e s. (p,e) IN PROB /\ s IN valid_states PROB
     ==> (action_dom (p,e)) SUBSET FDOM s``,
SRW_TAC[][action_dom_def]
THEN METIS_TAC[FDOM_pre_subset_FDOM_valid_states, FDOM_eff_subset_FDOM_valid_states])

val FDOM_eff_subset_prob_dom = store_thm("FDOM_eff_subset_prob_dom",
``!p e. ((p,e)) IN PROB
     ==> (FDOM e) SUBSET (prob_dom PROB)``,
FULL_SIMP_TAC(srw_ss())[valid_states_def, prob_dom_def]
THEN SRW_TAC[][ action_dom_def, IMAGE_DEF, BIGUNION, SUBSET_DEF, EXTENSION, UNION_DEF]
THEN Q.EXISTS_TAC `action_dom (p,e)`
THEN SRW_TAC[][]
THENL
[
  SRW_TAC[][action_dom_def]
  ,
  Q.EXISTS_TAC `(p,e)`
  THEN SRW_TAC[][]
])

val FDOM_eff_subset_prob_dom_pair = store_thm("FDOM_eff_subset_prob_dom_pair",
``!a. (a) IN PROB
     ==> (FDOM (SND a)) SUBSET (prob_dom PROB)``,
METIS_TAC[FDOM_eff_subset_prob_dom, pairTheory.PAIR, pairTheory.SND])

val FDOM_pre_subset_prob_dom = store_thm("FDOM_pre_subset_prob_dom",
``!p e. (p,e) IN PROB
     ==> (FDOM p) SUBSET (prob_dom PROB)``,
FULL_SIMP_TAC(srw_ss())[valid_states_def, prob_dom_def]
THEN SRW_TAC[][ action_dom_def, IMAGE_DEF, BIGUNION, SUBSET_DEF, EXTENSION, UNION_DEF]
THEN Q.EXISTS_TAC `action_dom (p,e)`
THEN SRW_TAC[][]
THENL
[
  SRW_TAC[][action_dom_def]
  ,
  Q.EXISTS_TAC `(p,e)`
  THEN SRW_TAC[][]
])

val FDOM_pre_subset_prob_dom_pair = store_thm("FDOM_pre_subset_prob_dom_pair",
``!a. (a) IN PROB
     ==> (FDOM (FST a)) SUBSET (prob_dom PROB)``,
METIS_TAC[FDOM_pre_subset_prob_dom, pairTheory.PAIR, pairTheory.FST])

val valid_plan_valid_head = store_thm("valid_plan_valid_head",
``(h::as IN valid_plans PROB) ==> h IN PROB``,
SRW_TAC[][valid_plans_def])

val valid_plan_valid_tail = store_thm("valid_plan_valid_tail",
``(h::as IN valid_plans PROB) ==> (as IN valid_plans PROB)``,
SRW_TAC[][valid_plans_def])

val valid_plan_pre_subset_prob_dom_pair = store_thm("valid_plan_pre_subset_prob_dom_pair",
``as IN (valid_plans PROB) ==> (!a. MEM a as ==> (FDOM (FST a)) SUBSET (prob_dom PROB))``,
Induct_on `as`
THEN SRW_TAC[][]
THEN METIS_TAC[FDOM_pre_subset_prob_dom_pair, valid_plan_valid_head, valid_plan_valid_tail])

val valid_append_valid_suff = store_thm("valid_append_valid_suff",
``(as1 ++ as2) IN (valid_plans PROB)
  ==> as2 IN (valid_plans PROB)``,
SRW_TAC[][valid_plans_def])

val valid_append_valid_pref = store_thm("valid_append_valid_pref",
``(as1 ++ as2) IN (valid_plans PROB)
  ==> as1 IN (valid_plans PROB)``,
SRW_TAC[][valid_plans_def])

val valid_pref_suff_valid_append = store_thm("valid_pref_suff_valid_append",
``as1 IN (valid_plans PROB) /\ as2 IN (valid_plans PROB) ==>
  (as1 ++ as2) IN (valid_plans PROB)``,
SRW_TAC[][valid_plans_def])

val MEM_statelist_FDOM = store_thm(
  "MEM_statelist_FDOM",
  ``!PROB h as s0. s0 IN valid_states PROB /\ as IN (valid_plans PROB) /\
                   MEM h (state_list(s0, as))
                   ==> (FDOM h = FDOM s0)``,
  STRIP_TAC
  THEN Induct_on `as` THEN SRW_TAC[][state_list_def] THEN SRW_TAC[][]
  THEN `FDOM s0 = FDOM (state_succ s0 h)` by
    (MATCH_MP_TAC(GSYM FDOM_state_succ)
     THEN `h IN PROB` by METIS_TAC[valid_plan_valid_head]
     THEN MATCH_MP_TAC(FDOM_eff_subset_FDOM_valid_states)
     THEN Q.EXISTS_TAC `FST h`
     THEN SRW_TAC[][])
  THEN `FDOM h' = FDOM (state_succ s0 h)` by 
    (FIRST_X_ASSUM(MATCH_MP_TAC)
     THEN SRW_TAC[][]
     THEN FULL_SIMP_TAC(srw_ss())[valid_states_def, valid_plans_def])
  THEN SRW_TAC[][])

val MEM_statelist_valid_state = store_thm(
  "MEM_statelist_FDOM",
  ``!PROB h as s0. s0 IN valid_states PROB /\ as IN (valid_plans PROB) /\
                   MEM h (state_list(s0, as))
                   ==> (h IN valid_states PROB)``,
  SRW_TAC[][]
  THEN MP_TAC(MEM_statelist_FDOM |> Q.SPECL[`PROB`, `h`, `as`, `s0`] )
  THEN SRW_TAC[][]
  THEN FULL_SIMP_TAC(srw_ss())[valid_states_def])

val lemma_1 = store_thm("lemma_1",
``! as PROB. s IN valid_states PROB /\ as IN valid_plans PROB ==>
     ((IMAGE LAST (state_set(state_list(s, as)))) 
                SUBSET valid_states PROB) ``,
     SIMP_TAC (srw_ss()) [SUBSET_DEF, state_set_thm] THEN
     SRW_TAC[][] THEN
     Cases_on `x'` THEN1 FULL_SIMP_TAC(srw_ss()) [] THEN
     IMP_RES_TAC MEM_LAST' THEN
     METIS_TAC[MEM_statelist_valid_state,IS_PREFIX_MEM]);

val len_in_state_set_le_max_len = store_thm("len_in_state_set_le_max_len",
``! as x PROB. s IN valid_states PROB /\ as IN valid_plans PROB /\
               ~(as = [])  ==> x IN (state_set(state_list (s,as))) ==> (LENGTH(x)<= (SUC (LENGTH as) ))``,
SRW_TAC[][]
THEN MP_TAC(LENGTH_state_set |> INST_TYPE [alpha |-> ``:'a |-> 'b``] |> Q.SPECL [`state_list(s, as)`, `x`])
THEN SRW_TAC[][]
THEN MP_TAC(state_list_length_lemma |> Q.SPECL[`as`, `s`])
THEN SRW_TAC[][]
THEN MP_TAC(state_list_length_non_zero |> Q.SPECL[`as`,`s`])
THEN SRW_TAC[][]
THEN DECIDE_TAC);

val card_state_set_cons = store_thm("card_state_set_cons",
``! as s h. (CARD (state_set (state_list (s,h::as))) 
                   = SUC(CARD (state_set (state_list (state_succ s h,as)))))``,
SRW_TAC[][state_list_length_lemma_2]
THEN DECIDE_TAC)

val card_state_set = store_thm("card_state_set",
``!as s. (SUC (LENGTH(as)) = CARD(state_set(state_list(s,as))))``,
SRW_TAC [][state_list_length_lemma_2]
THEN DECIDE_TAC)

val neq_mems_state_set_neq_len = store_thm(
"neq_mems_state_set_neq_len",
``!as x y s. x IN state_set (state_list (s,as))
                /\ y IN state_set (state_list (s,as)) /\ ~(x = y)
                ==>  ~(LENGTH(x) = LENGTH(y))``,
SRW_TAC [][state_set_thm] THEN METIS_TAC[LENGTH_INJ_PREFIXES]);

val not_eq_last_diff_paths = store_thm("not_eq_last_diff_paths",
 ``!as PROB s.
      s IN valid_states PROB /\ as IN valid_plans PROB
      ==> ~(INJ (LAST) (state_set(state_list(s, as))) (valid_states PROB))
      ==> ?slist_1 slist_2.
              slist_1 IN state_set(state_list(s, as))
              /\ slist_2 IN state_set(state_list(s, as))
              /\ ((LAST slist_1) = (LAST slist_2))
              /\ ~((LENGTH(slist_1) = LENGTH(slist_2)))``,
REWRITE_TAC[INJ_DEF]
THEN SRW_TAC[][]
THENL
[
  `!x. x IN state_set (state_list (s,as))  ==> LAST(x) IN valid_states PROB` by
   (REWRITE_TAC[]
    THEN FULL_SIMP_TAC(srw_ss())[lemma_1, GSPECIFICATION, SPECIFICATION, IMAGE_DEF]
    THEN `!as PROB s.
             s IN valid_states PROB /\ as IN valid_plans PROB
             ==> IMAGE LAST (state_set (state_list (s,as)))
                                    SUBSET (valid_states PROB)` by METIS_TAC[lemma_1]
    THEN FULL_SIMP_TAC(srw_ss())[SPECIFICATION, IMAGE_DEF, SUBSET_DEF]
    THEN METIS_TAC[])
  THEN `FDOM(LAST x) = FDOM(s)` by
       (FULL_SIMP_TAC(srw_ss())[SPECIFICATION, IMAGE_DEF, SUBSET_DEF]
        THEN METIS_TAC[])
  THEN METIS_TAC[]
  ,
  METIS_TAC[neq_mems_state_set_neq_len]
]);

val empty_list_nin_state_set = store_thm("empty_list_nin_state_set",
``!sl. [] NOTIN state_set(sl)``,
Induct_on`sl`
THEN1 SRW_TAC[][state_set_def]
THEN  SRW_TAC[][state_set_def]);

val nempty_sl_in_state_set = store_thm("nempty_sl_in_state_set",
``! sl . sl <> [] ==> sl IN state_set(sl)``,
SRW_TAC [][state_set_thm])

val empty_list_nin_state_set = prove(
``! h slist as. h::slist IN state_set (state_list (s,as)) ==> (h = s)``,
    Induct_on`as`
    THEN SRW_TAC[][state_list_def, state_set_def]);

val cons_in_state_set_2 = store_thm("cons_in_state_set_2",
``!s slist h t. slist <> [] /\ s::slist IN state_set (state_list (s,h::t))
                ==> slist IN state_set (state_list (state_succ s h , t))``,
Induct_on`slist`
THEN SRW_TAC[][state_set_def, state_list_def, NIL_NOTIN_stateset, state_succ_def]);

val valid_action_valid_succ = store_thm("valid_action_valid_succ",
``h IN PROB /\ s IN valid_states PROB
  ==> (state_succ s h) IN valid_states PROB``,
SRW_TAC[][valid_states_def]
THEN `FDOM (state_succ s h) = FDOM s` by
     (MATCH_MP_TAC(FDOM_state_succ)
      THEN MATCH_MP_TAC(FDOM_eff_subset_FDOM_valid_states)
      THEN Q.EXISTS_TAC `FST h`
      THEN SRW_TAC[][valid_states_def])
THEN SRW_TAC[][])

val in_state_set_imp_eq_exec_prefix = store_thm("in_state_set_imp_eq_exec_prefix_thm",
``!slist as PROB s. as <> [] /\ slist <> [] /\ s IN valid_states PROB
                    /\ as IN valid_plans PROB
                    ==> slist IN state_set (state_list (s,as))
                       ==> ?as'. as' <<= as /\ (exec_plan(s,as') = LAST slist)
                                 /\ (LENGTH slist = SUC (LENGTH as'))``,
Induct_on`slist`
THEN1 FULL_SIMP_TAC(srw_ss())[state_set_def, state_list_def]
THEN SRW_TAC[][]
THEN `s::slist IN state_set (state_list (s,as))` by METIS_TAC[empty_list_nin_state_set]
THEN Cases_on`as`
THEN1 SRW_TAC[][]
THEN `(state_succ s h') IN valid_states PROB /\ t IN valid_plans PROB` by
         (SRW_TAC[][]
          THEN METIS_TAC[valid_plan_valid_head, valid_plan_valid_tail, valid_action_valid_succ]) 
THEN Cases_on`slist`
THENL
[
	Q.EXISTS_TAC `[]`
	THEN SRW_TAC[][empty_list_nin_state_set, exec_plan_def, state_succ_def]
	THEN METIS_TAC[empty_list_nin_state_set]
	,
	`h''::t' IN state_set (state_list (state_succ s h', t))` by SRW_TAC[][cons_in_state_set_2]
	THEN Cases_on`t`
	THENL
	[
		FULL_SIMP_TAC(srw_ss())[exec_plan_def, state_set_def, state_list_def]
                THEN SRW_TAC[][]
                THEN Q.EXISTS_TAC `[h']`
                THEN SRW_TAC[][exec_plan_def]
		,
		Q.REFINE_EXISTS_TAC `h'::as''`
		THEN SRW_TAC[][exec_plan_def]
		THEN FIRST_X_ASSUM (Q.SPECL_THEN [`h'''::t''`, `PROB`, `state_succ s h'`] MP_TAC)
		THEN SRW_TAC[][]
        ]
])

val eq_last_state_imp_append_nempty_as = prove(
``!as PROB slist_1 slist_2. as <> [] /\ s IN valid_states PROB /\ as IN valid_plans PROB
                            /\ (slist_1 <> []) /\ (slist_2 <> [])
                            /\ (slist_1 IN state_set(state_list(s, as))
                            /\ (slist_2 IN state_set(state_list(s, as)))
                            /\ ~(LENGTH slist_1 = LENGTH slist_2)
                            /\ ((LAST slist_1) = (LAST slist_2)))
                            ==> ?as1 as2 as3. (as1++as2++as3 = as)
                                              /\ (exec_plan(s,as1++as2) = exec_plan(s,as1))
                                              /\  ~(as2=[])``,
SRW_TAC[][]
THEN `?as_1. (as_1 <<= as) /\ (exec_plan(s,as_1) = LAST slist_1)
              /\ ((LENGTH slist_1) = SUC (LENGTH as_1))`
      by METIS_TAC[ in_state_set_imp_eq_exec_prefix]
THEN `?as_2. (as_2 <<= as) /\ (exec_plan(s,as_2) = LAST slist_2)
             /\ ((LENGTH slist_2) = SUC (LENGTH as_2))`
      by METIS_TAC[ in_state_set_imp_eq_exec_prefix]
THEN `(LENGTH as_1) <> (LENGTH as_2)` by DECIDE_TAC
THEN `((LENGTH as_1) < (LENGTH as_2)) \/ ((LENGTH as_1) > (LENGTH as_2))` by DECIDE_TAC
THENL
[
  `as_1<<=as_2` by METIS_TAC[len_gt_pref_is_pref, GREATER_DEF]
  THEN `?a. as_2 = as_1 ++ a` by METIS_TAC[IS_PREFIX_APPEND]
  THEN `?b. as = as_2 ++b` by METIS_TAC[IS_PREFIX_APPEND]
  THEN Cases_on`a`
  THEN1 FULL_SIMP_TAC(srw_ss())[]
  THEN Q.EXISTS_TAC `as_1`
  THEN Q.EXISTS_TAC `h::t`
  THEN Q.EXISTS_TAC `b`
  THEN SRW_TAC[][]
  ,
  `as_2<<=as_1` by METIS_TAC[len_gt_pref_is_pref, GREATER_DEF]
  THEN `?a. as_1 = as_2 ++ a` by METIS_TAC[IS_PREFIX_APPEND]
  THEN `?b. as = as_1 ++b` by METIS_TAC[IS_PREFIX_APPEND]
  THEN Cases_on`a`
  THEN1 FULL_SIMP_TAC(srw_ss())[]
  THEN Q.EXISTS_TAC `as_2`
  THEN Q.EXISTS_TAC `h::t`
  THEN Q.EXISTS_TAC `b`
  THEN SRW_TAC[][]
]) ;

val FINITE_prob_dom = store_thm("FINITE_prob_dom",
``FINITE PROB ==> FINITE (prob_dom PROB)``,
SRW_TAC[][prob_dom_def, action_dom_def]
THEN ASSUME_TAC((GSYM pairTheory.PAIR) |> INST_TYPE [alpha |-> ``:'a |-> 'b``, beta |-> ``:'a |-> 'c``]  |> Q.SPEC `x`)
THEN FULL_SIMP_TAC(bool_ss)[]
THEN `action_dom x = FDOM (FST x) ∪ FDOM (SND x)` by METIS_TAC[action_dom_def]
THEN SRW_TAC[][])

val CARD_valid_states = store_thm("CARD_valid_states",
``FINITE (PROB: 'a problem) ==> (CARD ((valid_states PROB):'a state set) = 2 ** CARD (prob_dom PROB))``,
SRW_TAC[][valid_states_def]
THEN MATCH_MP_TAC(card_of_set_of_all_possible_states|> Q.SPEC `prob_dom PROB` |> INST_TYPE[beta |-> ``:bool``, gamma |-> ``:bool``])
THEN SRW_TAC[][FINITE_prob_dom])

val FINITE_valid_states = store_thm("FINITE_valid_states",
``FINITE (PROB:'a problem) ==> FINITE ((valid_states PROB):'a state set)``,
METIS_TAC[FINITE_states, FINITE_prob_dom, valid_states_def])

val lemma_2 = store_thm("lemma_2",
``!PROB (as:(α action) list) (s: ('a state)).
    FINITE PROB /\ s IN valid_states PROB /\ as IN valid_plans PROB
    ==> (LENGTH(as)) > ((2** (CARD (FDOM (s)))) - 1)
    ==> ?as1 as2 as3. (as1++as2++as3 = as) /\ (exec_plan(s,as1++as2) = exec_plan(s,as1)) /\  ~(as2=[])``,
SRW_TAC[][]
THEN `(CARD(state_set (state_list (s,as)))) > (2 ** CARD (FDOM s))` 
     by (SRW_TAC[][GSYM card_state_set] THEN DECIDE_TAC)
THEN `~INJ (LAST) (state_set (state_list (s: 'a state,as))) (valid_states PROB) ` by
     	   (`FDOM s = prob_dom PROB` by FULL_SIMP_TAC(srw_ss())[valid_states_def]
	   THEN `(CARD ((valid_states PROB):'a state set) = ((2 ** CARD (FDOM s)) ))` by METIS_TAC[CARD_valid_states]
	   THEN `CARD (state_set (state_list ((s: 'a state),as))) > CARD ( (valid_states PROB):'a state set)` by METIS_TAC[card_of_set_of_all_possible_states]
           THEN ASSUME_TAC(PHP |> INST_TYPE [alpha |-> ``:'a state list``, beta |-> ``:'a state``] |> Q.SPECL[`LAST`, `(state_set (state_list (s,as:(α action) list)))`, `(valid_states (PROB:'a problem)): 'a state set`])
           THEN FULL_SIMP_TAC(srw_ss())[GSYM GREATER_DEF]
	   THEN SRW_TAC[][]
	   THEN FULL_SIMP_TAC(srw_ss())[FINITE_states, FINITE_valid_states])
THEN `?slist_1 slist_2.
       slist_1 IN state_set (state_list (s,as)) /\
       slist_2 IN state_set (state_list (s,as)) /\
       (LAST slist_1 = LAST slist_2) /\ LENGTH slist_1 ≠ LENGTH slist_2` by METIS_TAC[not_eq_last_diff_paths |> INST_TYPE[beta |-> ``:bool``]]
THEN Cases_on`as`
THENL
[
	FULL_SIMP_TAC(srw_ss())[]
	THEN Cases_on`(FDOM s)`
	THEN1 FULL_SIMP_TAC(srw_ss())[]
	THEN DECIDE_TAC
	,
	`h::t <> []` by SRW_TAC[][eq_last_state_imp_append_nempty_as]
	THEN METIS_TAC[eq_last_state_imp_append_nempty_as, state_set_thm]
]);

val lemma_2_prob_dom = store_thm("lemma_2_prob_dom",
``!PROB (as:(α action) list) (s: ('a state)).
    FINITE PROB /\ s IN valid_states PROB /\ as IN valid_plans PROB
    ==> (LENGTH(as)) > ((2** (CARD (prob_dom PROB))) - 1)
    ==> ?as1 as2 as3. (as1++as2++as3 = as) /\ (exec_plan(s,as1++as2) = exec_plan(s,as1)) /\  ~(as2=[])``,
SRW_TAC[][valid_states_def]
THEN MP_TAC(lemma_2 |> Q.SPECL[`PROB`, `as`, `s`])
THEN SRW_TAC[][valid_states_def])

val lemma_3 = store_thm("lemma_3",
``!as (PROB:'a problem) s . FINITE PROB /\ s IN valid_states PROB /\ as IN valid_plans PROB
               /\ ((LENGTH(as)) > (2** (CARD (prob_dom PROB))) - 1) ==>
      ?as'. (exec_plan(s, as) = exec_plan(s, as')) /\ (LENGTH(as')<LENGTH(as)) /\ sublist as' as``,
SRW_TAC[][]
THEN `?as1 as2 as3. (as1++as2++as3 = as) /\ (exec_plan(s,as1++as2) = exec_plan(s,as1)) /\  ~(as2=[])` by METIS_TAC[lemma_2_prob_dom]
THEN ` exec_plan(s, as1 ++ as3) = exec_plan(s, as1 ++ as2 ++ as3)` by METIS_TAC[cycle_removal_lemma]
THEN Q.EXISTS_TAC `as1 ++ as3`
THEN METIS_TAC[nempty_list_append_length_add, append_sublist, sublist_refl]);

val sublist_valid_is_valid = store_thm("sublist_valid_is_valid",
``!as' as PROB. as IN valid_plans PROB /\ sublist as' as ==> as' IN valid_plans PROB``,
SRW_TAC[][valid_plans_def]
THEN METIS_TAC[sublist_subset, SUBSET_TRANS])

val main_lemma = store_thm("main_lemma",
``!(PROB:'a problem) as s. FINITE PROB /\ s IN valid_states PROB /\ as IN valid_plans PROB 
	 ==> ?as'. (exec_plan(s, as) = exec_plan(s, as'))  /\ sublist as' as  /\ (LENGTH(as') <=  (2** (CARD (prob_dom PROB))) - 1)``,
SRW_TAC[][]
THEN Cases_on`(LENGTH(as) <=  (2** (CARD (prob_dom PROB))) - 1)`
THENL
[
  Q.EXISTS_TAC `as`
  THEN METIS_TAC[sublist_refl]
  ,
  `(LENGTH as > 2 ** CARD (prob_dom PROB) - 1)` by DECIDE_TAC
  THEN ASSUME_TAC(Q.SPEC `as` (sublist_refl |> INST_TYPE [alpha |-> ``:('a state # 'a state )``]))
  THEN MP_TAC (general_theorem
               |> Q.ISPEC `(\ (as'':'a action list). (exec_plan(s, as) = exec_plan(s, as'')) /\ sublist as'' as)`
               |> Q.SPEC `LENGTH`
               |> Q.SPEC `(2** (CARD (prob_dom (PROB:'a problem)))) - 1`)
  THEN SRW_TAC[][]
  THEN Q.PAT_X_ASSUM ` a ==> b `
       (MATCH_MP_TAC o REWRITE_RULE[GSYM CONJ_ASSOC] o Q.SPEC `as` o REWRITE_RULE[AND_IMP_INTRO] o (CONV_RULE RIGHT_IMP_FORALL_CONV))
  THEN SRW_TAC[SatisfySimps.SATISFY_ss][]
  THEN PROVE_TAC[sublist_trans, lemma_3, sublist_valid_is_valid]
]);

val reachable_s_def = 
Define`reachable_s PROB s ={exec_plan(s,as) | as IN (valid_plans PROB)}`;

val valid_as_valid_exec = store_thm("valid_as_valid_exec",
``!as s PROB.
   as IN (valid_plans PROB) /\ s IN (valid_states PROB)
   ==> (exec_plan(s, as)) IN (valid_states PROB)``,
Induct_on `as`
THEN SRW_TAC[][exec_plan_def]
THEN METIS_TAC[valid_action_valid_succ, valid_plan_valid_head, valid_plan_valid_tail]);

val exec_plan_fdom_subset = store_thm("exec_plan_fdom_subset",
``! as s PROB. as IN (valid_plans PROB) ==> 
    (FDOM (exec_plan(s, as)) SUBSET ((FDOM s) UNION (prob_dom PROB))  )``,
Induct_on `as`
THEN SRW_TAC[][exec_plan_def]
THEN `FDOM (exec_plan (state_succ s h,as)) SUBSET FDOM (state_succ s h) ∪ prob_dom PROB` by
     (FIRST_X_ASSUM MATCH_MP_TAC THEN METIS_TAC[valid_plan_valid_tail])
THEN MP_TAC(FDOM_state_succ_subset |> Q.GEN `a` |> Q.SPEC `h`)
THEN SRW_TAC[][]
THEN `FDOM s ∪ FDOM (SND h) ∪ prob_dom PROB = FDOM s ∪ prob_dom PROB` by
     METIS_TAC[FDOM_eff_subset_prob_dom_pair,
               SUBSET_UNION_ABSORPTION, UNION_ASSOC,
               valid_plan_valid_head]
THEN METIS_TAC[SUBSET_TRANS, UNION_SUBSET_UNION])

val reachable_s_finite_thm_1 = store_thm("reachable_s_finite_thm_1",
``s IN (valid_states PROB) ==> (reachable_s PROB s) SUBSET (valid_states PROB)``,
SRW_TAC[][reachable_s_def]
THEN SRW_TAC[][Once SUBSET_DEF]
THEN SRW_TAC[][valid_as_valid_exec])

val reachable_s_finite_thm = store_thm("reachable_s_finite_thm",
``!s. FINITE (PROB:'a problem) /\ s IN (valid_states PROB) ==> FINITE (reachable_s PROB s)``,
METIS_TAC[FINITE_valid_states, SUBSET_FINITE, reachable_s_finite_thm_1])

val reachable_s_finite_thm = store_thm("reachable_s_finite_thm",
``!s. FINITE (PROB:'a problem) /\ s IN (valid_states PROB) ==> FINITE (reachable_s PROB s)``,
METIS_TAC[ FINITE_valid_states, SUBSET_FINITE, reachable_s_finite_thm_1])

val empty_plan_is_valid = store_thm("empty_plan_is_valid",
``[] IN (valid_plans PROB)``,
SRW_TAC[][valid_plans_def])

val valid_head_and_tail_valid_plan = store_thm("valid_head_and_tail_valid_plan",
``h IN PROB /\ as IN (valid_plans PROB)
  ==> (h::as IN (valid_plans PROB))``,
SRW_TAC[][valid_plans_def])

val lemma_1_reachability_s = store_thm("lemma_1_reachability_s",
``!PROB s as. s IN (valid_states PROB) /\ as IN (valid_plans PROB) ==>
     ( (IMAGE LAST ( state_set ( state_list ( s, as ) ) )  ) SUBSET (reachable_s PROB s)) ``,
     STRIP_TAC
     THEN Induct_on `as`
     THEN SRW_TAC[][exec_plan_def, state_set_def, state_list_def]
     THEN1(SRW_TAC[][reachable_s_def]
           THEN Q.EXISTS_TAC `[]`
           THEN SRW_TAC[][exec_plan_def])
     THEN FULL_SIMP_TAC(srw_ss())[state_set_def, state_list_def, SUBSET_DEF, reachable_s_def]
     THEN SRW_TAC[][]
     THEN1(SRW_TAC[][reachable_s_def]
           THEN Q.EXISTS_TAC `[]`
           THEN SRW_TAC[][exec_plan_def, empty_plan_is_valid])
     THEN1(FIRST_X_ASSUM (MP_TAC o Q.SPEC `state_succ s h`)
           THEN SRW_TAC[SatisfySimps.SATISFY_ss][valid_plan_valid_tail, valid_plan_valid_head, valid_action_valid_succ]
           THEN Cases_on `x''`
           THEN SRW_TAC[][]
           THEN1 FULL_SIMP_TAC(srw_ss())[state_set_def, state_list_def]
           THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `LAST (h'::t)`)
           THEN SRW_TAC[][]
           THEN `(?x'.
                    (LAST (h'::t) = LAST x') /\
                      x' IN state_set (state_list (state_succ s h,as)))` by METIS_TAC[]
           THEN `?as'.
                     (LAST (h'::t) = exec_plan (state_succ s h,as')) /\
                           (as' IN (valid_plans PROB))` by METIS_TAC[valid_plan_valid_tail, valid_plan_valid_head]
           THEN Q.EXISTS_TAC `h::as'`
           THEN SRW_TAC[][exec_plan_def]
           THEN METIS_TAC[valid_head_and_tail_valid_plan, valid_plan_valid_tail, valid_plan_valid_head]));

val not_eq_last_diff_paths_reachability_s = store_thm("not_eq_last_diff_paths_reachability_s",
    ``!as PROB s. s IN (valid_states PROB) /\ as IN (valid_plans PROB)
                 ==> ~(INJ (LAST) (state_set(state_list(s, as))) (reachable_s PROB s))
    	      	       ==> ?slist_1 slist_2.( (slist_1 IN state_set(state_list(s, as))) /\ (slist_2 IN state_set(state_list(s, as)))
		       /\ ((LAST slist_1)=(LAST slist_2)) /\ ~((LENGTH(slist_1) = LENGTH(slist_2))))``,
REWRITE_TAC[INJ_DEF]
THEN SRW_TAC[][]
THENL
[
  `!x. x IN state_set (state_list (s,as))  ==> LAST(x) IN reachable_s PROB s` by
    (SRW_TAC[][]
     THEN FULL_SIMP_TAC(srw_ss())[lemma_1_reachability_s, GSPECIFICATION, SPECIFICATION, IMAGE_DEF]
     THEN `!as PROB. s IN (valid_states PROB) /\ as IN (valid_plans PROB) ⇒ IMAGE LAST (state_set (state_list (s,as))) SUBSET reachable_s PROB s` by METIS_TAC[lemma_1_reachability_s]
     THEN FULL_SIMP_TAC(srw_ss())[GSPECIFICATION, SPECIFICATION, IMAGE_DEF, GSPEC_ETA, SUBSET_DEF]
     THEN METIS_TAC[GSPECIFICATION, SPECIFICATION, IMAGE_DEF, GSPEC_ETA, SUBSET_DEF])
  THEN `LAST x IN valid_states PROB` by(
    MP_TAC(lemma_1)
    THEN FULL_SIMP_TAC(srw_ss())[GSPECIFICATION, SPECIFICATION, IMAGE_DEF, GSPEC_ETA, SUBSET_DEF]
    THEN METIS_TAC[GSPECIFICATION, SPECIFICATION, IMAGE_DEF, GSPEC_ETA, SUBSET_DEF])
  THEN METIS_TAC[]
  ,
  METIS_TAC[neq_mems_state_set_neq_len]
]);
 
val lemma_2_reachability_s = store_thm("lemma_2_reachability_s",
``!(PROB:'a problem) as s. FINITE PROB /\ s IN (valid_states PROB) /\ as IN (valid_plans PROB)
                               ==> (LENGTH(as)) > CARD (reachable_s PROB s) - 1
                               ==> ?as1 as2 as3. (as1++as2++as3 = as) /\ (exec_plan(s,as1++as2) = exec_plan(s,as1)) /\  ~(as2=[])``,
SRW_TAC[][]
THEN `(CARD(state_set (state_list (s,as)))) > CARD (reachable_s PROB s)` by 
      (SRW_TAC[][GSYM card_state_set] THEN DECIDE_TAC)
THEN `~INJ (LAST) (state_set (state_list (s,as))) (reachable_s PROB s) ` by
     	  (MP_TAC(reachable_s_finite_thm)
           THEN SRW_TAC[][]
	   THEN MP_TAC(Q.ISPEC`(reachable_s PROB s):'a state set`
	                  (Q.ISPEC `(state_set (state_list (s,as:(α action) list)))`
			  (Q.ISPEC `LAST` PHP)))
           THEN SRW_TAC[][GSYM GREATER_DEF])
THEN `?slist_1 slist_2.
       slist_1 IN state_set (state_list (s,as)) /\
       slist_2 IN state_set (state_list (s,as)) /\
       (LAST slist_1 = LAST slist_2) /\ LENGTH slist_1 ≠ LENGTH slist_2` by METIS_TAC[not_eq_last_diff_paths_reachability_s]
THEN Cases_on`as`
THENL
[
  FULL_SIMP_TAC(srw_ss())[state_set_def, state_list_def]
  THEN DECIDE_TAC
  ,
  `h::t <> []` by SRW_TAC[][]
  THEN METIS_TAC[eq_last_state_imp_append_nempty_as, state_set_thm]
]);

val lemma_3_reachability_s = store_thm("lemma_3_reachability_s",
``!as (PROB:'a problem) s. FINITE PROB /\ s IN valid_states PROB /\ as IN valid_plans PROB
            /\ ((LENGTH(as)) > CARD (reachable_s PROB s) - 1) ==>
      ?as'. (exec_plan(s, as) = exec_plan(s, as')) /\ (LENGTH(as')<LENGTH(as)) /\ sublist as' as``,
SRW_TAC[][]
THEN `?as1 as2 as3. (as1++as2++as3 = as) /\ (exec_plan(s,as1++as2) = exec_plan(s,as1)) /\  ~(as2=[])` by METIS_TAC[lemma_2_reachability_s]
THEN ` exec_plan(s, as1 ++ as3) = exec_plan(s, as1 ++ as2 ++ as3)` by METIS_TAC[cycle_removal_lemma]
THEN Q.EXISTS_TAC `as1 ++ as3`
THEN METIS_TAC[nempty_list_append_length_add, append_sublist, sublist_refl]);

val main_lemma_reachability_s = store_thm("main_lemma_reachability_s",
``!(PROB:'a problem) as s. FINITE PROB /\ s IN valid_states PROB /\ as IN valid_plans PROB 
	 ==> ?as'. (exec_plan(s, as) = exec_plan(s, as'))  /\ sublist as' as  /\ (LENGTH(as') <=  CARD (reachable_s PROB s) - 1)``,
SRW_TAC[][]
THEN Cases_on`(LENGTH(as) <=  CARD (reachable_s PROB s) - 1)`
THENL
[
        METIS_TAC[sublist_refl]
	,
	`(LENGTH as > CARD (reachable_s PROB s) - 1)` by DECIDE_TAC
	THEN ASSUME_TAC(Q.SPEC `as` (sublist_refl |> INST_TYPE [alpha |-> ``:('a state # 'a state )``]))
	THEN MP_TAC (general_theorem |> INST_TYPE[alpha |-> ``:'a action list``]
		|> Q.SPECL [`(\as''. (exec_plan(s, as) = exec_plan(s, as'')) /\ sublist as'' as)`, `LENGTH`,`CARD ((reachable_s (PROB:'a problem) s):'a state set) - 1`])
	THEN SRW_TAC[][]
        THEN Q.PAT_X_ASSUM ` a ==> b `
          (MATCH_MP_TAC o REWRITE_RULE[GSYM CONJ_ASSOC] o Q.SPEC `as` o REWRITE_RULE[AND_IMP_INTRO] o (CONV_RULE RIGHT_IMP_FORALL_CONV))
        THEN SRW_TAC[SatisfySimps.SATISFY_ss][]
        THEN PROVE_TAC[sublist_trans, lemma_3_reachability_s, sublist_valid_is_valid]
]);

val reachable_s_non_empty = store_thm("reachable_s_non_empty",
``~(reachable_s PROB s = EMPTY)``,
SRW_TAC[][reachable_s_def, valid_states_def]
THEN SRW_TAC[][Once EXTENSION, valid_plans_def]
THEN Q.EXISTS_TAC `[]`
THEN SRW_TAC[][]);

val card_reachable_s_non_zero = store_thm("card_reachable_s_non_zero",
``!s. FINITE (PROB:'a problem) /\ s IN (valid_states PROB) ==> (0 < CARD(reachable_s PROB s))``,
SRW_TAC[][]
THEN `EMPTY PSUBSET (reachable_s PROB s)` by (SRW_TAC[][PSUBSET_MEMBER, EXTENSION] THEN SRW_TAC[][SIMP_RULE(srw_ss())[EXTENSION]reachable_s_non_empty])
THEN MP_TAC(reachable_s_finite_thm)
THEN SRW_TAC[][]
THEN MP_TAC(CARD_PSUBSET |> INST_TYPE [alpha |-> ``:'a state``] |> Q.SPEC `(reachable_s PROB s)`)
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `EMPTY`)
THEN SRW_TAC[][])

val exec_fdom_empty_prob = store_thm("exec_fdom_empty_prob",
``!s.  (prob_dom PROB = EMPTY) /\ (s IN valid_states PROB) /\ as IN valid_plans PROB
       ==> (exec_plan(s, as) = FEMPTY)``,
MP_TAC(valid_as_valid_exec)
THEN SRW_TAC[][exec_plan_def, GSYM FDOM_EQ_EMPTY, valid_states_def]
THEN METIS_TAC[FDOM_EQ_EMPTY]);

val reachable_s_empty_prob = store_thm("reachable_s_empty_prob",
``!PROB s. ((prob_dom PROB) = EMPTY) /\ s IN valid_states PROB
   ==> (reachable_s PROB s SUBSET {FEMPTY})``,
SRW_TAC[][reachable_s_def, FDOM_EQ_EMPTY, SUBSET_DEF]
THEN MP_TAC(SIMP_RULE(srw_ss())
       [SUBSET_DEF,
        FDOM_EQ_EMPTY] (exec_fdom_empty_prob |> Q.SPEC `s`))
THEN SRW_TAC[][])

val sublist_valid_plan = store_thm("sublist_valid_plan",
``as1 IN valid_plans PROB /\ sublist as2 as1 ==> as2 IN valid_plans PROB``,
SRW_TAC[][valid_plans_def]
THEN METIS_TAC[sublist_subset, SUBSET_TRANS])

val submap_imp_state_succ_submap = store_thm("submap_imp_state_succ_submap",
``!a s1 s2. FST a SUBMAP s1 /\ s1 SUBMAP s2
            ==> state_succ s1 a SUBMAP state_succ s2 a``,
SRW_TAC[][state_succ_def]
THEN1(FULL_SIMP_TAC(srw_ss())[SUBMAP_DEF]
      THEN SRW_TAC[][]
      THEN METIS_TAC[FUNION_DEF])
THEN METIS_TAC[SUBMAP_TRANS])

val pred_dom_subset_succ_submap = store_thm("pred_dom_subset_succ_submap",
``!a s1 s2. (FDOM (FST a)) SUBSET FDOM s1 /\ s1 SUBMAP s2
            ==>  state_succ s1 a SUBMAP state_succ s2 a``,
SRW_TAC[][state_succ_def]
THEN1(FULL_SIMP_TAC(srw_ss())[SUBMAP_DEF]
      THEN SRW_TAC[][]
      THEN METIS_TAC[FUNION_DEF])
THEN1 METIS_TAC[SUBMAP_TRANS]
THEN FULL_SIMP_TAC(srw_ss())[SUBMAP_DEF, SUBSET_DEF])

val valid_as_submap_init_submap_exec = store_thm("valid_as_submap_init_submap_exec",
``!s1 s2. s1 SUBMAP s2 /\ (!a. MEM a as ==> FDOM (FST a) SUBSET (FDOM s1)) ==>
  exec_plan(s1, as) SUBMAP (exec_plan(s2, as))``,
Induct_on `as`
THEN SRW_TAC[][exec_plan_def]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[action_dom_def]
THEN1 METIS_TAC[pred_dom_subset_succ_submap]
THEN SRW_TAC[][state_succ_def]
THEN METIS_TAC[SUBSET_UNION, SUBSET_TRANS])

val valid_plan_mems = store_thm("valid_plan_mems",
``as IN valid_plans PROB /\ MEM a as ==>
  a IN PROB``,
FULL_SIMP_TAC(srw_ss())[valid_plans_def]
THEN METIS_TAC[list_set_subset_mem_in]);

val valid_states_nempty = store_thm("valid_states_nempty",
``FINITE PROB ==> ?s. s IN ((valid_states (PROB:((α |-> β)#(α |-> β)) set)):(α |-> β) set)``,
SRW_TAC[][valid_states_def]
THEN METIS_TAC[finite_dom_always_has_mapping, FINITE_prob_dom])

val empty_prob_dom_single_val_state = store_thm("empty_prob_dom_single_val_state",
``((prob_dom PROB) = EMPTY)
  ==> ?s. ((valid_states PROB) = {s})``,
SRW_TAC[][valid_states_def]
THEN Q.EXISTS_TAC `FEMPTY`
THEN SRW_TAC[][Once EXTENSION, FDOM_EQ_EMPTY])

val empty_prob_dom_imp_empty_plan_always_good = store_thm("empty_prob_dom_imp_empty_plan_always_good",
``!(PROB) s.((prob_dom PROB) = EMPTY) /\ s IN (valid_states PROB) /\ as IN (valid_plans PROB)
   ==> (exec_plan(s, []) = exec_plan(s, as))``,
SRW_TAC[][]
THEN MP_TAC(empty_prob_dom_single_val_state|> INST_TYPE[gamma|-> ``:'b``, delta|->``:'b``])
THEN SRW_TAC[][]
THEN METIS_TAC[empty_plan_is_valid, valid_as_valid_exec, IN_SING])

val empty_prob_dom = store_thm("empty_prob_dom",
``((prob_dom PROB) = EMPTY) ==> ((PROB = {(FEMPTY, FEMPTY)}) \/ (PROB = EMPTY))``,
SRW_TAC[][prob_dom_def]
THEN `!x. x IN PROB ==> (FDOM (FST x)  = EMPTY)`
      by (SRW_TAC[][EXTENSION] THEN FULL_SIMP_TAC(srw_ss())[action_dom_pair, EXTENSION] 
          THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `FDOM (FST x) UNION FDOM (SND x)`)
          THEN SRW_TAC[][]
          THEN METIS_TAC[])
THEN `!x. x IN PROB ==> (FDOM(SND x) = EMPTY)`
      by (SRW_TAC[][EXTENSION] THEN FULL_SIMP_TAC(srw_ss())[action_dom_pair, EXTENSION] 
          THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `FDOM (FST x) UNION FDOM (SND x)`)
          THEN SRW_TAC[][]
          THEN METIS_TAC[])
THEN FULL_SIMP_TAC(srw_ss())[FDOM_EQ_EMPTY]
THEN `!x. x IN PROB ⇒ (x = (FEMPTY, FEMPTY))` by METIS_TAC[pairTheory.PAIR]
THEN Cases_on `PROB = EMPTY`
THEN SRW_TAC[][]
THEN `?y. y IN PROB` by METIS_TAC[MEMBER_NOT_EMPTY]
THEN METIS_TAC[UNIQUE_MEMBER_SING])

val empty_prob_dom_finite = store_thm("empty_prob_dom_finite",
``((prob_dom PROB) = EMPTY) ==> FINITE PROB``,
METIS_TAC[FINITE_EMPTY, FINITE_SING, empty_prob_dom])

val disj_imp_eq_proj_exec = store_thm("disj_imp_eq_proj_exec",
``!a vs s.
     (DISJOINT (FDOM (SND a)) vs)
     ==> ((DRESTRICT s vs) =
              (DRESTRICT (state_succ s a) vs))``,
SRW_TAC[][]
THEN SRW_TAC[][DISJOINT_DEF, DRESTRICT_DEF, INTER_DEF, EXTENSION]
THEN SRW_TAC[][state_succ_def, FUNION_DEF]
THEN METIS_TAC[disj_dom_drest_fupdate_eq]);

val no_change_vs_eff_submap = store_thm("no_change_vs_eff_submap",
``!a vs s. 
     ((DRESTRICT s vs) = (DRESTRICT (state_succ s a) vs))
     /\ FST a SUBMAP s
        ==> (DRESTRICT (SND a) vs) SUBMAP (DRESTRICT s vs)``,
SRW_TAC[][state_succ_def]
THEN METIS_TAC[SUBMAP_FUNION_ABSORPTION, DRESTRICTED_FUNION_2]);

val sat_precond_as_proj_3 = store_thm("sat_precond_as_proj_3",
``!s a vs.
     (FDOM (DRESTRICT (SND a) vs) = EMPTY)
     ==> (DRESTRICT (state_succ s a) vs = DRESTRICT s vs)``,
SRW_TAC[][]
THEN `FDOM (DRESTRICT (DRESTRICT (SND a) vs) vs) = EMPTY` by FULL_SIMP_TAC(srw_ss())[DRESTRICT_IDEMPOT]
THEN FULL_SIMP_TAC(srw_ss())[Once FDOM_DRESTRICT]
THEN `DISJOINT (FDOM (SND a)) vs` by FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT, DISJOINT_DEF]
THEN METIS_TAC[disj_imp_eq_proj_exec]);

val proj_eq_proj_exec_eq = store_thm("proj_eq_proj_exec_eq",
``!s s' vs a a'.
    (DRESTRICT s vs = DRESTRICT s' vs) /\ (FST a SUBMAP s = FST a' SUBMAP s') /\ 
    (DRESTRICT (SND a) vs = DRESTRICT (SND a') vs)
    ==> (DRESTRICT (state_succ s a) vs = DRESTRICT (state_succ s' a') vs)``,
SRW_TAC[][]
THEN `FDOM(DRESTRICT (state_succ s a) vs) = FDOM(DRESTRICT (state_succ s' a') vs)`
     	by (SRW_TAC[][FDOM_DRESTRICT, state_succ_def]
	THEN `FDOM (DRESTRICT s vs) = FDOM (DRESTRICT s' vs)` by METIS_TAC[fmap_EQ_THM]
	THEN `FDOM (DRESTRICT (SND a) vs) = FDOM (DRESTRICT (SND a') vs)` by METIS_TAC[fmap_EQ_THM]
	THEN FULL_SIMP_TAC(srw_ss())[INTER_DEF, UNION_DEF, EXTENSION, SUBSET_DEF, SPECIFICATION, SUBMAP_DEF, FDOM_DRESTRICT, EXTENSION, GSPEC_ETA]
	THEN SRW_TAC[][]
     	THEN METIS_TAC[])
THEN `!x. (DRESTRICT (state_succ s a) vs) ' x = (DRESTRICT (state_succ s' a') vs) ' x`      
     	by (SRW_TAC[][FDOM_DRESTRICT, state_succ_def]
	THEN `!x. (DRESTRICT s vs) ' x = (DRESTRICT s' vs) ' x` by METIS_TAC[fmap_EQ_THM]
	THEN `!x. (DRESTRICT (SND a) vs) ' x = (DRESTRICT (SND a') vs) ' x` by METIS_TAC[fmap_EQ_THM]
	THEN FULL_SIMP_TAC(srw_ss())[INTER_DEF, UNION_DEF, EXTENSION, SUBSET_DEF, SPECIFICATION, SUBMAP_DEF, FDOM_DRESTRICT, EXTENSION, GSPEC_ETA, FUNION_DEF, fmap_EXT, DRESTRICT_DEF]
	THEN SRW_TAC[][FUNION_DEF]
	THEN METIS_TAC[])
THEN METIS_TAC[fmap_EQ_THM]);

val empty_eff_exec_eq = store_thm("empty_eff_exec_eq",
``!s a. (FDOM (SND a) = EMPTY)
        ==> ((state_succ s a) = s)``,
SRW_TAC[][state_succ_def, FUNION_DEF, SUBMAP_DEF, fmap_EXT]);

val exec_as_proj_valid_2 = store_thm("exec_as_proj_valid_2",
``!a. a IN PROB ==> ((action_dom a) SUBSET (prob_dom PROB))``,
SRW_TAC[][action_dom_def, prob_dom_def, IMAGE_DEF, SUBSET_DEF]
THEN METIS_TAC[])

val valid_filter_valid_as = store_thm("valid_filter_valid_as",
``as IN (valid_plans PROB) ==> (FILTER P as) IN (valid_plans PROB)``,
SRW_TAC[][valid_plans_def]
THEN METIS_TAC[set_subset_filter_subset])

val sublist_valid_plan = store_thm("sublist_valid_plan",
``sublist as' as /\ as IN (valid_plans PROB)
  ==> as' IN (valid_plans PROB)``,
SRW_TAC[][valid_plans_def]
THEN METIS_TAC[sublist_subset, SUBSET_TRANS])

val prob_subset_dom_subset = store_thm("prob_subset_dom_subset",
``PROB1 SUBSET PROB2 ==> prob_dom PROB1 SUBSET prob_dom PROB2``,
SRW_TAC[][prob_dom_def, SUBSET_DEF]
THEN METIS_TAC[])

val state_succ_valid_act_disjoint = store_thm("state_succ_valid_act_disjoint",
``a IN PROB /\ DISJOINT vs (prob_dom PROB)
  ==> (DRESTRICT (state_succ s a) vs = DRESTRICT s vs)``,
SRW_TAC[][]
THEN MATCH_MP_TAC (SIMP_RULE(srw_ss())[FDOM_DRESTRICT, GSYM DISJOINT_DEF] sat_precond_as_proj_3)
THEN METIS_TAC[DISJOINT_SUBSET, FDOM_eff_subset_prob_dom_pair, DISJOINT_SYM])

val exec_valid_as_disjoint = store_thm("exec_valid_as_disjoint",
``!s. DISJOINT vs (prob_dom PROB) /\ as IN valid_plans PROB
   ==> 
  (DRESTRICT (exec_plan(s, as)) vs = DRESTRICT s vs)``,
Induct_on `as`
THEN SRW_TAC[][exec_plan_def]
THEN METIS_TAC[state_succ_valid_act_disjoint, valid_plan_valid_head, valid_plan_valid_tail])

val state_successors_def = Define`state_successors PROB s = (IMAGE (state_succ s) PROB) DIFF {s}`

(*State Spaces*)

val stateSpace_def = 
      Define`stateSpace ss vs = !s. s IN ss ==> (FDOM s = vs)`;

val EQ_SS_DOM = store_thm("EQ_SS_DOM",
``~(ss = EMPTY) /\ stateSpace ss vs1 /\ stateSpace ss vs2 ==> (vs1 = vs2)``,
SRW_TAC[][stateSpace_def] THEN 
FULL_SIMP_TAC(srw_ss())[GSYM MEMBER_NOT_EMPTY] THEN METIS_TAC[])

val FINITE_SS = store_thm("FINITE_SS",
``!ss: ('a |-> bool)->bool. ~(ss = EMPTY) /\ stateSpace ss dom ==> FINITE ss``,
SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[stateSpace_def]
THEN `FINITE dom` by (FULL_SIMP_TAC(srw_ss())[GSYM MEMBER_NOT_EMPTY]
                             THEN SRW_TAC[][] THEN METIS_TAC[FDOM_FINITE])
THEN `ss SUBSET {s | FDOM s = dom}` by SRW_TAC[][SUBSET_DEF]
THEN MP_TAC(FINITE_states |> Q.SPEC `dom`)
THEN SRW_TAC[][]
THEN Q.ABBREV_TAC `s' = { s | FDOM s = dom }`
THEN METIS_TAC[SUBSET_FINITE]);

val disjoint_effects_no_effects = store_thm("disjoint_effects_no_effects",
``!s. (!a. MEM a as ==> (FDOM (DRESTRICT (SND a) vs) = ∅))
  ==> (DRESTRICT (exec_plan(s,as)) vs = DRESTRICT s vs)``,
Induct_on `as`
THEN SRW_TAC[][exec_plan_def]
THEN METIS_TAC[sat_precond_as_proj_3])

(*Needed asses*)

val action_needed_vars_def = Define`action_needed_vars a s = {v | v IN (FDOM s) /\ v IN (FDOM (FST a)) /\ ((FST a) ' v = s ' v)}`

val action_needed_asses_def = Define`action_needed_asses a s = DRESTRICT (s) (action_needed_vars a s)`

val act_needed_asses_submap_succ_submap = store_thm("act_needed_asses_submap_succ_submap",
``!a s1 s2. (action_needed_asses a s2 SUBMAP action_needed_asses a s1) /\ s1 SUBMAP s2
            ==>  state_succ s1 a SUBMAP state_succ s2 a``,
SRW_TAC[][state_succ_def]
THEN1(FULL_SIMP_TAC(srw_ss())[SUBMAP_DEF]
      THEN SRW_TAC[][]
      THEN METIS_TAC[FUNION_DEF])
THEN1 METIS_TAC[SUBMAP_TRANS]
THEN FULL_SIMP_TAC(srw_ss())[SUBMAP_DEF, SUBSET_DEF, action_needed_asses_def, action_needed_vars_def, FUNION_DEF]
THEN FULL_SIMP_TAC(bool_ss)[DRESTRICT_DEF]
THEN FULL_SIMP_TAC(bool_ss)[IN_DEF, INTER_DEF]
THEN FULL_SIMP_TAC(bool_ss)[Ntimes GSPEC_ETA 5]
THEN METIS_TAC[GSPEC_ETA])

val as_needed_asses_submap_exec = store_thm("as_needed_asses_submap_exec",
``!s1 s2. s1 SUBMAP s2 /\ (!a. MEM a as ==> (action_needed_asses a s2 SUBMAP action_needed_asses a s1)) ==>
  exec_plan(s1, as) SUBMAP (exec_plan(s2, as))``,
Induct_on `as`
THEN SRW_TAC[][exec_plan_def]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[action_dom_def]
THEN1 METIS_TAC[act_needed_asses_submap_succ_submap]
THEN SRW_TAC[][state_succ_def, action_needed_asses_def, action_needed_vars_def, FUNION_DEF]
THEN FULL_SIMP_TAC(srw_ss())[SUBMAP_DEF, SUBSET_DEF, action_needed_asses_def, action_needed_vars_def, FUNION_DEF, DRESTRICT_FDOM]
THEN FULL_SIMP_TAC(bool_ss)[DRESTRICT_DEF]
THEN FULL_SIMP_TAC(bool_ss)[IN_DEF, INTER_DEF]
THEN FULL_SIMP_TAC(bool_ss)[Ntimes GSPEC_ETA 6]
THEN FULL_SIMP_TAC(bool_ss)[FUNION_DEF, UNION_DEF]
THEN FULL_SIMP_TAC(srw_ss())[SUBMAP_DEF, SUBSET_DEF, action_needed_asses_def, action_needed_vars_def, FUNION_DEF, DRESTRICT_FDOM]
THEN FULL_SIMP_TAC(bool_ss)[DRESTRICT_DEF]
THEN FULL_SIMP_TAC(bool_ss)[IN_DEF, INTER_DEF]
THEN FULL_SIMP_TAC(bool_ss)[Ntimes GSPEC_ETA 6]
THEN FULL_SIMP_TAC(bool_ss)[FUNION_DEF, UNION_DEF]
THEN METIS_TAC[GSPEC_ETA])

val system_needed_vars_def = Define `system_needed_vars PROB s = (BIGUNION {action_needed_vars a s | a IN PROB})`

val system_needed_asses_def = Define `system_needed_asses PROB s = DRESTRICT (s) (system_needed_vars PROB s)`

val action_needed_vars_subset_sys_needed_vars_subset = store_thm("action_needed_vars_subset_sys_needed_vars_subset",
``a IN PROB ==> (action_needed_vars a s) SUBSET (system_needed_vars PROB s)``,
SRW_TAC[][SUBSET_DEF, system_needed_vars_def] THEN METIS_TAC[])

val action_needed_asses_submap_sys_needed_asses = store_thm("action_needed_asses_submap_sys_needed_asses",
``a IN PROB ==> action_needed_asses a s SUBMAP system_needed_asses PROB s``,
SRW_TAC[][action_needed_asses_def, system_needed_asses_def, action_needed_vars_subset_sys_needed_vars_subset, DRESTRICT_SUBSET_SUBMAP])

val system_needed_asses_include_action_needed_asses_1 =store_thm("system_needed_asses_include_action_needed_asses_1",
``a IN PROB ==> 
(action_needed_vars a
     (DRESTRICT s (system_needed_vars PROB s)) = action_needed_vars a s)``,
SRW_TAC[][action_needed_vars_def, EXTENSION]
THEN EQ_TAC
THEN1(SRW_TAC[][]
      THEN1(FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT])
      THEN1 METIS_TAC[DRESTRICT_DEF])
THEN1(SRW_TAC[][]
      THEN `?s'. x IN s' /\ ?a. (s' = action_needed_vars a s) /\ a IN PROB` by 
        (Q.EXISTS_TAC `action_needed_vars a s`
         THEN SRW_TAC[][]
         THEN1 SRW_TAC[][action_needed_vars_def]
         THEN METIS_TAC[])
      THEN1(SRW_TAC[][system_needed_vars_def, FDOM_DRESTRICT]
            THEN METIS_TAC[])
      THEN1(SRW_TAC[][system_needed_vars_def, DRESTRICT_DEF]
            THEN METIS_TAC[])))

val system_needed_asses_include_action_needed_asses = store_thm("system_needed_asses_include_action_needed_asses",
``a IN PROB ==> (action_needed_asses a (system_needed_asses PROB s) = action_needed_asses a s)``,
SRW_TAC[][action_needed_asses_def, system_needed_asses_def]
THEN SRW_TAC[SatisfySimps.SATISFY_ss][system_needed_asses_include_action_needed_asses_1]
THEN METIS_TAC[action_needed_vars_subset_sys_needed_vars_subset,
               SUBSET_INTER_ABSORPTION, INTER_COMM])

val system_needed_asses_submap = store_thm("system_needed_asses_submap",
``(system_needed_asses PROB s) SUBMAP s``,
SRW_TAC[][system_needed_asses_def])

val as_works_from_system_needed_asses = store_thm("as_works_from_system_needed_asses",
``as IN valid_plans PROB ==> 
  exec_plan(system_needed_asses PROB s, as) SUBMAP exec_plan(s, as)``,
SRW_TAC[][]
THEN MATCH_MP_TAC as_needed_asses_submap_exec
THEN SRW_TAC[][]
THEN1 METIS_TAC[system_needed_asses_submap]
THEN `a IN PROB` by FULL_SIMP_TAC(srw_ss())[valid_plans_def, SUBSET_DEF]
THEN SRW_TAC[][system_needed_asses_include_action_needed_asses])

val _ = export_theory();
