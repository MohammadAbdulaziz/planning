open HolKernel Parse boolLib bossLib functionsLib;
open arithmeticTheory
open utilsTheory

val _ = new_theory "arith_utils";

val two_children_parent_bound_main_lemma_7 = store_thm("two_children_parent_bound_main_lemma_7",
``!(x:num) y z a. x <= (y*a) /\ a <= z ==> x <= (y*z)``,
Induct_on `a`
THEN SRW_TAC[][]
THEN `a <= z - 1` by DECIDE_TAC
THEN `(y * SUC a = y + y * a)` by METIS_TAC[MULT_CLAUSES]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN `x - y <= y * a` by DECIDE_TAC
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`x - y`,`y`,`z-1`])
THEN SRW_TAC[][]
THEN MP_TAC(LEFT_SUB_DISTRIB |> Q.SPECL[`z`,`1`, `y`])
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[ADD_ASSOC]
THEN `z >= 1` by DECIDE_TAC
THEN `y*z >= y` by (Cases_on `z` THEN1 DECIDE_TAC THEN `(y * SUC n = y + y * n)` by METIS_TAC[MULT_CLAUSES] THEN DECIDE_TAC)
THEN DECIDE_TAC);

val two_children_parent_bound_main_lemma_8 = store_thm("two_children_parent_bound_main_lemma_8",
``!(a:num) b c d. (a + b) * (c + d) = a*c + a*d + b*c + b*d``,
SRW_TAC[][LEFT_ADD_DISTRIB]
THEN REWRITE_TAC[Once MULT_COMM]
THEN `(a + b) * d = d * (a + b)` by METIS_TAC[MULT_COMM]
THEN SRW_TAC[][]
THEN SRW_TAC[][LEFT_ADD_DISTRIB]
THEN DECIDE_TAC);

val right_leq_mult_leq = store_thm("right_leq_mult_leq",
``! (y:num) z a. a <= z ==> (y*a) <= (y*z)``,
Induct_on `a`
THEN SRW_TAC[][]);

val mult_plus_leq = store_thm("mult_plus_leq",
``!a b c d e. b <= d /\ c <= e ==> a * b + c <= a *d + e``,
Induct_on `a`
THEN SRW_TAC[][MULT]
THEN FIRST_X_ASSUM (assume_thm_concl_after_proving_assums o SIMP_RULE(srw_ss())[] o Q.SPECL[`b`, `c+b`, `d`, `e+d`])
  THEN1 DECIDE_TAC
THEN METIS_TAC[ADD_ASSOC, ADD_COMM])

val mult_left_le = store_thm("mult_left_le",
``!a b c d. b<= c ==> a*(b + d) <= a*(c + d)``,
SRW_TAC[][])

val mult_left_distrib_one = store_thm("mult_left_distrib_one",
``!a b. (a * b + b) = b * (a + 1)``,
DECIDE_TAC)

val left_right_mult_plus_leq = store_thm("left_right_mult_plus_leq",
``!a b x y. a <= x /\ b <= y /\ c <= z ==> a*b + c <= x*y + z``,
Induct_on `a`
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[GSYM mult_left_distrib_one, MULT_COMM, ADD1]
THEN1 DECIDE_TAC
THEN FIRST_ASSUM (ASSUME_TAC o Q.SPECL[`b`, `x - 1`, `y`])
THEN `a <= x -1` by DECIDE_TAC
THEN FULL_SIMP_TAC(srw_ss())[MULT_COMM, LEFT_SUB_DISTRIB]
THEN ` a * b + c ≤ x * y − y + z` by METIS_TAC[]
THEN Cases_on `x`
THEN SRW_TAC[][]
THEN1 DECIDE_TAC
THEN FULL_SIMP_TAC(srw_ss())[ADD1, RIGHT_ADD_DISTRIB]
THEN DECIDE_TAC)

val add_less = store_thm("add_less",
``a < x /\ b <= y ==> a +b < x + y``,
DECIDE_TAC)

val add_less_2 = store_thm("add_less_2",
``a < y /\ b <= x ==> a +b < x + y``,
METIS_TAC[ADD_COMM, add_less])

val add_less_3 = store_thm("add_less_3",
``a <= x /\ b < y ==> a + b < x + y``,
METIS_TAC[ADD_COMM, add_less])

val _ = export_theory();
