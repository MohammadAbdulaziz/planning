open HolKernel Parse boolLib bossLib;
open finite_mapTheory;
open pred_setTheory;
(*;*)
open stateSpaceProductTheory;
open factoredSystemTheory
open systemAbstractionTheory;
open invariantsPlusOneTheory;
open fmap_utilsTheory
open set_utilsTheory

val _ = new_theory "invariantStateSpace";

val invariant_ss_def = Define`invariant_ss VS ss = !s. s IN ss ==> !vs. vs IN VS ==> invariant (DRESTRICT s vs)`;

val invariantStateSpace_thm_3 = store_thm("invariantStateSpace_thm_3",
``!ss vs VS dom. FINITE vs /\ stateSpace ss dom /\ invariant_ss VS ss /\ vs IN VS
          ==> ss_proj ss vs SUBSET ss_proj (invariant_states vs) dom``,
SRW_TAC[][ss_proj_def, stateSpace_def, invariant_ss_def, invariant_states_def, SUBSET_DEF]
THEN `FDOM (DRESTRICT s vs) SUBSET vs` by METIS_TAC[FDOM_DRESTRICT, INTER_SUBSET]
THEN MP_TAC(invariant_state_imp_invariant_super_state|> Q.SPEC `vs`)
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `(DRESTRICT s vs)`)
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `s''`
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[invariant_def, fmap_EXT, DRESTRICT_DEF, FDOM_DRESTRICT, INTER_DEF, EXTENSION, SUBSET_DEF, FUNION_DEF]
THEN SRW_TAC[][]
THEN METIS_TAC[])

val invariantStateSpace_thm_4 = store_thm("invariantStateSpace_thm_4",
``!ss VS1 VS2. invariant_ss VS1 ss /\ VS2 SUBSET VS1
               ==> invariant_ss VS2 ss``,
SRW_TAC[][invariant_ss_def, SUBSET_DEF])

val invariantStateSpace_thm_6 = store_thm("invariantStateSpace_thm_6",
``!ss vs VS. invariant_ss VS ss ==> invariant_ss VS (ss_proj ss vs)``,
SRW_TAC[][invariant_ss_def, ss_proj_def, invariant_def]
THEN FULL_SIMP_TAC(srw_ss())[invariant_def, fmap_EXT, DRESTRICT_DEF, FDOM_DRESTRICT, INTER_DEF, EXTENSION, SUBSET_DEF, FUNION_DEF]
THEN SRW_TAC[][]
THEN METIS_TAC[])

val invariantStateSpace_thm = store_thm("invariantStateSpace_thm",
``!VS:(('a -> bool) -> bool). FINITE VS ==> 
       ((!vs. vs IN VS ==> FINITE vs)
        /\ (!vs1 vs2. vs1 IN VS /\ vs2 IN VS /\ ~(vs1 = vs2) ==> DISJOINT vs1 vs2) ==>
       (!vs:('a->bool) dom:('a-> bool) ss:(('a |->bool)->bool).
                 FINITE vs /\ (*~(vs = EMPTY) /\*) stateSpace ss dom /\ dom SUBSET (vs UNION BIGUNION VS)
                 /\ invariant_ss (vs INSERT VS) ss /\ (!vs1. vs1 IN VS ==> DISJOINT vs vs1)
                  ==> ss SUBSET
                           ss_proj (PRODf (IMAGE invariant_states VS) (invariant_states vs)) dom )) ``,
HO_MATCH_MP_TAC FINITE_INDUCT
THEN SRW_TAC[][]
THEN1(FULL_SIMP_TAC(srw_ss())[PRODf_EMPTY]
      THEN MP_TAC((dom_subset_ssproj_eq_ss |> INST_TYPE[beta |-> ``:bool``]) |> Q.SPECL[`ss`, `dom`, `vs`])
      THEN SRW_TAC[][]
      THEN MP_TAC(invariantStateSpace_thm_3 |> Q.SPECL[`ss`, `vs`, `{vs}`, `dom`])
      THEN SRW_TAC[][])
THEN `PRODf (invariant_states e INSERT IMAGE invariant_states VS) (invariant_states vs) = 
              stateSpaceProduct (invariant_states e) (PRODf (IMAGE invariant_states VS) (invariant_states vs))` by
     (MATCH_MP_TAC (PRODf_INSERT |> INST_TYPE [beta |-> ``:bool``] |> Q.SPECL[`invariant_states e`, `IMAGE invariant_states VS`])
      THEN SRW_TAC[][]
      THEN1(Cases_on `x IN VS`
            THEN SRW_TAC[][]
            THEN METIS_TAC[invariant_prob_reachable_states_thm_2])
      THEN1 METIS_TAC[always_inavr_states]
      THEN1 METIS_TAC[invariant_states_stateSpace_thm]
      THEN1(`DISJOINT e x` by METIS_TAC[]
            THEN `stateSpace (invariant_states x) x` by METIS_TAC[invariant_states_stateSpace_thm]
            THEN METIS_TAC[always_inavr_states, EQ_SS_DOM])
      THEN1 METIS_TAC[always_inavr_states, EQ_SS_DOM, invariant_states_stateSpace_thm]
      THEN1 METIS_TAC[invariant_states_stateSpace_thm])
THEN SRW_TAC[][stateSpaceProduct_left_comm_thm]
THEN `~(invariant_states e IN (IMAGE invariant_states VS))` by
          METIS_TAC[invariant_prob_reachable_states_thm_2, (invariant_prob_reachable_states_thm_3 |> INST_TYPE[alpha |-> ``:'a -> bool``]) |> Q.SPEC `FINITE`]
THEN `(IMAGE invariant_states VS DELETE invariant_states e) = IMAGE invariant_states VS` by METIS_TAC[DELETE_NON_ELEMENT]
THEN SRW_TAC[][]
THEN `!vs. vs IN VS ==> FINITE vs` by SRW_TAC[][]
THEN UNDISCH_TAC 
``(∀vs. vs ∈ VS ⇒ FINITE vs) ∧
      (∀vs1 vs2. vs1 ∈ VS ∧ vs2 ∈ VS ∧ vs1 ≠ vs2 ⇒ DISJOINT vs1 vs2) ⇒
      ∀vs dom ss.
        FINITE vs (*∧ vs ≠ ∅ *)∧ stateSpace ss dom ∧
        dom ⊆ vs ∪ BIGUNION VS ∧ invariant_ss (vs INSERT VS) ss ∧
        (∀vs1. vs1 ∈ VS ⇒ DISJOINT vs vs1) ⇒
        ss ⊆
        ss_proj
          (PRODf (IMAGE invariant_states VS) (invariant_states vs)) dom`` 
THEN SRW_TAC[][]
THEN MP_TAC(invariantStateSpace_thm_3 |> Q.SPECL[`ss`, `e`, `vs INSERT e INSERT VS`, `dom`])
THEN SRW_TAC[][]
THEN `vs INSERT VS SUBSET (vs INSERT e INSERT VS)` by SRW_TAC[][invariant_prob_reachable_states_thm_9, SUBSET_TRANS, INSERT_DEF, SUBSET_DEF]
THEN MP_TAC(invariantStateSpace_thm_4 |> Q.SPECL[`ss`, `vs INSERT e INSERT VS`, `vs INSERT VS`])
THEN SRW_TAC[][]
THEN MP_TAC(invariantStateSpace_thm_5|> INST_TYPE [beta |-> ``:bool``] |> Q.SPECL[`ss`, `vs UNION (BIGUNION VS)`, `dom`])
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`, `dom INTER (vs UNION (BIGUNION VS))`, `ss_proj ss (vs UNION (BIGUNION VS))`])
THEN SRW_TAC[][invariantStateSpace_thm_6]
THEN `dom ⊆ e ∪ (vs ∪ BIGUNION VS)` by METIS_TAC[ UNION_ASSOC, UNION_COMM]
THEN MP_TAC(SIMP_RULE(bool_ss)[UNION_ASSOC] invariantStateSpace_thm_7 |> INST_TYPE [beta |-> ``:bool``] |> Q.SPECL[`ss`, `dom`, `e`, `(vs ∪ BIGUNION VS)`])
THEN SRW_TAC[][]
THEN MP_TAC(invariantStateSpace_thm_8 |> INST_TYPE[beta |-> ``:bool``] |> Q.SPECL[`ss_proj ss e`,
                                                   `ss_proj ss (vs ∪ BIGUNION VS)`,
                                                   `ss_proj (invariant_states e) dom`,
                                                   `ss_proj
                                                       (PRODf (IMAGE invariant_states VS)
                                                          (invariant_states vs))
                                                       (dom ∩ (vs ∪ BIGUNION VS))`])
THEN SRW_TAC[][]
THEN MP_TAC(ss_proj_PRODf_eq_PRODf |> Q.SPEC `VS`)
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL [`vs`, `EMPTY`])
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[invariantStateSpace_thm_9]
THEN MP_TAC(sspaceprod_ssproj_eq_ssproj_ssprod |> INST_TYPE[beta |-> ``:bool``] 
               |> Q.SPECL[`(invariant_states e)`,
                          `(PRODf (IMAGE invariant_states VS) (invariant_states vs))`,
                          `dom`])
THEN SRW_TAC[][]
THEN METIS_TAC[SUBSET_TRANS])

val _ = export_theory();