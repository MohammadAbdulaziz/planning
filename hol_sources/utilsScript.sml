open HolKernel Parse boolLib bossLib;

val _ = new_theory "utils";

val lambda_conj_eq_conj_lambda = store_thm("lambda_conj_eq_conj_lambda",
``!P1 P2 a. (\a. P1 a) a /\ (\a. P2 a) a
              <=> (\a. P1 a /\ P2 a) a``,
SRW_TAC[][]);


val LCOMM_THM_gen = store_thm("LCOMM_THM_gen",
``!f P. (!x y z. P x y /\ P x z ==> P x (f y z))
      /\ (!x y z. f x (f y z) = f (f x y) z)
      /\ (!x y. P x y ==> (f x y = f y x))
      ==> (!x y z. P x y /\ P x z ==> (f x (f y z) = f y (f x z)))``,
REPEAT STRIP_TAC
THEN METIS_TAC[])

val _ = export_theory();
