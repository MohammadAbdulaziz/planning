open HolKernel Parse boolLib bossLib;
open invariant_state_spaceTheory;
open ParentChildrenBoundTheory;
open SCCMainGraphPlanTheoremTheory;
open SCCGraphPlanTheoremTheory; 
open GraphPlanTheoremTheory;
open FM_planTheory;
open state_spaceTheory;
open pred_setTheory;
open planning_problem_diameter_boundTheory;
open BoundingAlgorithmsTheory;
open state_space_productTheory;
open InvariantsPlusOneTheory;
open finite_mapTheory;

val _ = new_theory "invariant_planning_problem";

val invariant_prob_def = Define`invariant_prob VS PROB = !s. s IN (valid_states PROB) ==> ((!vs. vs IN VS ==> invariant (DRESTRICT s vs)) \/ (!act. act IN PROB /\ (!vs. vs IN VS ==> invariant (DRESTRICT (state_succ s act) vs))))`

val reachable_state_space_thm = store_thm("reachable_state_space_thm",
``!PROB s.  s IN valid_states PROB ==> state_space (reachable_s PROB s) (prob_dom PROB)``,
SRW_TAC[][state_space_def, reachable_s_def]
THEN MP_TAC(graph_plan_lemma_6_1)
THEN FULL_SIMP_TAC(srw_ss())[valid_states_def])

val invariant_prob_reachable_states_thm_1_ = store_thm("invariant_prob_reachable_states_thm_1_",
``!PROB VS. invariant_prob VS PROB ==> invariant_ss VS (reachable PROB) /\ state_space (reachable PROB) (prob_dom PROB)``,
SRW_TAC[][invariant_prob_def, invariant_ss_def]
THEN SRW_TAC[][state_space_def]
THEN FULL_SIMP_TAC(srw_ss())[reachable_def]
THEN METIS_TAC[graph_plan_lemma_6_1])

val invariant_prob_reachable_states_thm = store_thm("invariant_prob_reachable_states_thm",
``!VS. FINITE VS
       ==> 
       ((!vs. vs IN VS ==> FINITE vs) 
              /\ (!vs1 vs2. vs1 IN VS /\ vs2 IN VS /\ ~(vs1 = vs2) ==> DISJOINT vs1 vs2)
              ==>
             (!vs PROB. FINITE vs /\ planning_problem PROB /\ FDOM PROB.I SUBSET (vs UNION BIGUNION (VS))
                                   /\ invariant_prob (vs INSERT VS) PROB /\ (!vs1. vs1 IN VS ==> DISJOINT vs vs1)
                        ==> (reachable PROB) SUBSET
                               ss_proj (PRODf (IMAGE invariant_states VS) (invariant_states vs)) (FDOM PROB.I)))``,
METIS_TAC[invariant_prob_reachable_states_thm_1_, invariant_state_space_thm])



val disjoint_inter = store_thm("disjoint_inter",
``!s t u. DISJOINT s t /\ ~(s INTER u = EMPTY) ==> ~(s INTER u = t INTER u)``,
SRW_TAC[][DISJOINT_DEF, INTER_DEF, EXTENSION] THEN METIS_TAC[])


val inter_disjoint = store_thm("inter_disjoint",
``!s t u. DISJOINT s t ==> (DISJOINT (s INTER u) (t INTER u))``,
SRW_TAC[][DISJOINT_DEF, INTER_DEF, EXTENSION] THEN METIS_TAC[])

val invariant_prob_diameter_loose_bound_thm = store_thm("invariant_prob_diameter_loose_bound_thm",
``!VS. FINITE VS
       ==> 
       ((!vs. vs IN VS ==> FINITE vs) 
              /\ (!vs1 vs2. vs1 IN VS /\ vs2 IN VS /\ ~(vs1 = vs2) ==> DISJOINT vs1 vs2)
              ==>
             (!vs. FINITE vs /\ (!vs1. vs1 IN VS ==> DISJOINT vs vs1) /\ ~(vs IN VS) ==> 
                   (!PROB. planning_problem PROB /\ FDOM PROB.I SUBSET (vs UNION BIGUNION (VS))
                           /\ invariant_prob (vs INSERT VS) PROB
                           ==> problem_plan_bound PROB <=
                                Π CARD
                                    (invariant_states vs INSERT
                                        IMAGE invariant_states VS))))``,
SRW_TAC[][]
THEN MP_TAC(SIMP_RULE(srw_ss())[GSYM arithmeticTheory.LE_LT1] bound_main_lemma_reachability |> Q.SPECL[`PROB`])
THEN SRW_TAC[][]
THEN `problem_plan_bound PROB <= CARD (reachable PROB)` by DECIDE_TAC
THEN MP_TAC(invariant_prob_reachable_states_thm |> Q.SPECL[`VS`])
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`, `PROB`])
THEN SRW_TAC[][]
THEN `CARD
         (PRODf
            (IMAGE invariant_states VS)
            (invariant_states vs)) 
          <= Π CARD
                 (invariant_states vs INSERT
                      IMAGE invariant_states VS)` by
     (MP_TAC(PRODf_CARD
              |> INST_TYPE [beta |-> ``:bool``] 
              |> Q.SPEC `(IMAGE invariant_states VS)`)
     THEN SRW_TAC[][]
     THEN FIRST_X_ASSUM MATCH_MP_TAC
     THEN SRW_TAC[][]
     THEN  Q.EXISTS_TAC `vs`
     THEN SRW_TAC[][]
     THEN1(Cases_on `x ∉ VS`
           THEN SRW_TAC[][]
           THEN METIS_TAC[invariant_prob_reachable_states_thm_2, FINITE_INTER, disjoint_inter])
     THEN1 METIS_TAC[invariant_state_space_thm_13, FINITE_INTER]
     THEN1 METIS_TAC[invariant_states_state_space_thm]
     THEN1(`DISJOINT vs x` by METIS_TAC[]
            THEN `state_space (invariant_states x) x` by METIS_TAC[invariant_states_state_space_thm]
            THEN METIS_TAC[invariant_state_space_thm_13, EQ_SS_DOM])
      THEN1 METIS_TAC[invariant_state_space_thm_13, EQ_SS_DOM, invariant_states_state_space_thm]
      THEN1 METIS_TAC[invariant_states_state_space_thm]
      THEN1 METIS_TAC[invariant_state_space_thm_13])
THEN `FINITE (PRODf (IMAGE invariant_states VS) (invariant_states vs))` by
      (MP_TAC (PRODf_FINITE |> INST_TYPE [beta |-> ``:bool``]
                                    |> Q.SPEC `(IMAGE invariant_states VS)`)
       THEN SRW_TAC[][]
       THEN FIRST_X_ASSUM MATCH_MP_TAC
       THEN Q.EXISTS_TAC `vs`
       THEN SRW_TAC[][]
       THEN1(Cases_on `x ∉ VS`
            THEN SRW_TAC[][]
            THEN METIS_TAC[invariant_prob_reachable_states_thm_2, FINITE_INTER, disjoint_inter])
      THEN1 METIS_TAC[invariant_state_space_thm_13, FINITE_INTER]
      THEN1 METIS_TAC[invariant_states_state_space_thm]
      THEN1(`DISJOINT vs x` by METIS_TAC[]
            THEN `state_space (invariant_states x) x` by METIS_TAC[invariant_states_state_space_thm]
            THEN METIS_TAC[invariant_state_space_thm_13, EQ_SS_DOM])
      THEN1 METIS_TAC[invariant_state_space_thm_13, EQ_SS_DOM, invariant_states_state_space_thm]
      THEN1 METIS_TAC[invariant_states_state_space_thm]
      THEN1 METIS_TAC[invariant_state_space_thm_13])
THEN FULL_SIMP_TAC(srw_ss())[ss_proj_def]
THEN METIS_TAC[SUBSET_FINITE, SUBSET_TRANS, arithmeticTheory.LESS_EQ_TRANS, CARD_SUBSET, CARD_IMAGE, IMAGE_FINITE])

val invariant_prob_diameter_bound_thm_1 = store_thm("invariant_prob_diameter_bound_thm_1",
``!VS1 VS2 PROB. (!vs1. vs1 IN VS1 ==> ?vs2. vs2 IN VS2 /\ vs1 SUBSET vs2)
                 /\ invariant_prob VS2 PROB 
                 ==> invariant_prob VS1 PROB``,
SRW_TAC[][invariant_prob_def]
THEN METIS_TAC[invariant_state_space_DRESTRICT_SUBSET_thm])

val invariant_prob_diameter_bound_thm = store_thm("invariant_prob_diameter_bound_thm",
``!VS. FINITE VS
       ==> 
       ((!vs. vs IN VS ==> FINITE vs) 
              /\ (!vs1 vs2. vs1 IN VS /\ vs2 IN VS /\ ~(vs1 = vs2) ==> DISJOINT vs1 vs2)
              ==>
             (!vs. FINITE vs /\ (!vs1. vs1 IN VS ==> DISJOINT vs vs1) /\ ~(vs IN VS) ==> 
                   (!PROB. planning_problem PROB /\ FDOM PROB.I SUBSET (vs UNION BIGUNION (VS))
                           /\ invariant_prob (vs INSERT VS) PROB
                           ==> problem_plan_bound PROB <=
                                Π CARD
                                    (invariant_states (vs ∩ FDOM PROB.I) INSERT
                                        IMAGE invariant_states ((IMAGE (λvs. vs ∩ FDOM PROB.I) VS) DIFF {EMPTY})))))``,
SRW_TAC[][]
THEN `(∀vs'.
         (∃vs. (vs' = vs ∩ FDOM PROB.I) ∧ vs ∈ VS) ∧ vs' ≠ ∅ ⇒
         FINITE vs')` by
     (SRW_TAC[][] THEN METIS_TAC[FINITE_INTER])
THEN `(∀vs1 vs2.
         ((∃vs. (vs1 = vs ∩ FDOM PROB.I) ∧ vs ∈ VS) ∧ vs1 ≠ ∅) ∧
         ((∃vs. (vs2 = vs ∩ FDOM PROB.I) ∧ vs ∈ VS) ∧ vs2 ≠ ∅) ∧
         vs1 ≠ vs2 ⇒
         DISJOINT vs1 vs2)` by
      (SRW_TAC[][] THEN FULL_SIMP_TAC(srw_ss())[INTER_DEF, DISJOINT_DEF, EXTENSION] THEN 
      SRW_TAC[][] THEN METIS_TAC[])
THEN MP_TAC(SIMP_RULE(srw_ss())[](invariant_prob_diameter_loose_bound_thm |> Q.SPEC `((IMAGE (λvs. vs ∩ FDOM PROB.I) VS) DIFF {EMPTY})`))
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]
THEN `∀vs.
         FINITE vs ∧
         (∀vs1.
            (∃vs. (vs1 = vs ∩ FDOM PROB.I) ∧ vs ∈ VS) ∧ vs1 ≠ ∅ ⇒
            DISJOINT vs vs1) ∧
         ((∀vs'. vs ≠ vs' ∩ FDOM PROB.I ∨ vs' ∉ VS) ∨ (vs = ∅)) ⇒
         ∀PROB'.
           planning_problem PROB' ∧
           FDOM PROB'.I ⊆
           vs ∪ BIGUNION (IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅}) ∧
           invariant_prob
             (vs INSERT IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅})
             PROB' ⇒
           problem_plan_bound PROB' ≤
           Π CARD
             (invariant_states vs INSERT
              IMAGE invariant_states
                (IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅}))` by (FIRST_X_ASSUM MATCH_MP_TAC THEN METIS_TAC[])
THEN WEAKEN_TAC is_imp
THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `(vs ∩ FDOM PROB.I)`)
THEN SRW_TAC[][]
THEN `(∀vs1.
          (∃vs. (vs1 = vs ∩ FDOM PROB.I) ∧ vs ∈ VS) ∧ vs1 ≠ ∅ ⇒
          DISJOINT (vs ∩ FDOM PROB.I) vs1)` by 
      (SRW_TAC[][] THEN FULL_SIMP_TAC(srw_ss())[INTER_DEF, DISJOINT_DEF, EXTENSION] THEN METIS_TAC[])
THEN `((∀vs'. vs ∩ FDOM PROB.I ≠ vs' ∩ FDOM PROB.I ∨ vs' ∉ VS) ∨
        (vs ∩ FDOM PROB.I = ∅))` by 
      (SRW_TAC[][] THEN FULL_SIMP_TAC(srw_ss())[INTER_DEF, DISJOINT_DEF, EXTENSION] THEN METIS_TAC[])
THEN `∀PROB'.
         planning_problem PROB' ∧
         FDOM PROB'.I ⊆
         vs ∩ FDOM PROB.I ∪
         BIGUNION (IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅}) ∧
         invariant_prob
           (vs ∩ FDOM PROB.I INSERT
            IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅}) PROB' ⇒
         problem_plan_bound PROB' ≤
         Π CARD
           (invariant_states (vs ∩ FDOM PROB.I) INSERT
            IMAGE invariant_states
              (IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅}))` by METIS_TAC[]
THEN WEAKEN_TAC is_imp
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN1(NTAC 7 (WEAKEN_TAC is_forall) 
      THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF, BIGUNION, INTER_DEF, EXTENSION]
      THEN SRW_TAC[][]
      THEN Cases_on `x ∈ vs`
      THEN SRW_TAC[][]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `x`)
      THEN SRW_TAC[][]
      THEN Q.EXISTS_TAC `s INTER (FDOM PROB.I)`
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN1(NTAC 7 (WEAKEN_TAC is_forall) 
      THEN MATCH_MP_TAC(invariant_prob_diameter_bound_thm_1
              |> Q.SPECL[`(vs ∩ FDOM PROB.I INSERT IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅}) `,
                          `(vs INSERT VS)`])
      THEN SRW_TAC[][]
      THEN1(Q.EXISTS_TAC `vs`
            THEN SRW_TAC[][]) 
      THEN1(Q.EXISTS_TAC `vs'`
            THEN SRW_TAC[][]))
THEN1(NTAC 6 (WEAKEN_TAC is_forall) 
      THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF, BIGUNION, INTER_DEF, EXTENSION]
      THEN SRW_TAC[][]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `x`)
      THEN SRW_TAC[][]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `x`)
      THEN SRW_TAC[][]
      THEN Q.EXISTS_TAC `s INTER (FDOM PROB.I)`
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN1(NTAC 6 (WEAKEN_TAC is_forall) 
      THEN MATCH_MP_TAC(invariant_prob_diameter_bound_thm_1
              |> Q.SPECL[`(EMPTY INSERT IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅}) `,
                          `(vs INSERT VS)`])
      THEN SRW_TAC[][]
      THEN1(Q.EXISTS_TAC `vs`
            THEN SRW_TAC[][]) 
      THEN1(Q.EXISTS_TAC `vs'`
            THEN SRW_TAC[][])))

val invariant_prob_diameter_bounding_algorithm_thm_1 = store_thm("invariant_prob_diameter_bounding_algorithm_thm_1",
``!PROB vs. FDOM (prob_proj(PROB, vs)).I SUBSET (FDOM PROB.I)``,
SRW_TAC[][prob_proj_def]
THEN SRW_TAC[][FDOM_DRESTRICT])

val invariant_prob_diameter_bounding_algorithm_thm_2 = store_thm("invariant_prob_diameter_bounding_algorithm_thm_2",
``!PROB vs1 vs2 . prob_proj(prob_proj(PROB, vs1), vs2) = prob_proj(PROB, (vs1 INTER vs2))``,
SRW_TAC[][prob_proj_def, EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1(Q.EXISTS_TAC `a'`
      THEN SRW_TAC[][])
THEN Q.EXISTS_TAC `action_proj(a,vs1)`
THEN SRW_TAC[][action_proj_def]
THEN Q.EXISTS_TAC `a`
THEN SRW_TAC[][])

val invariant_prob_diameter_bounding_algorithm_thm = store_thm("invariant_prob_diameter_bounding_algorithm_thm",
``!VS. FINITE VS
       ==> 
       ((!vs. vs IN VS ==> FINITE vs) 
              /\ (!vs1 vs2. vs1 IN VS /\ vs2 IN VS /\ ~(vs1 = vs2) ==> DISJOINT vs1 vs2)
              ==>
             (!vs. FINITE vs /\ ~(vs = EMPTY) /\ (!vs1. vs1 IN VS ==> DISJOINT vs vs1) /\ ~(vs IN VS) ==> 
                   (!lvs. ALL_DISTINCT lvs /\ disjoint_varset lvs /\ lvs ≠ [] ==>
                   (!PROB. planning_problem PROB /\ FDOM PROB.I SUBSET (vs UNION BIGUNION (VS))
                           /\ (!vs'. invariant_prob (vs INSERT VS) (prob_proj(PROB, vs')))
                           /\ (FDOM PROB.I = BIGUNION (set lvs)) /\ top_sorted (PROB,lvs)
                           /\ EVERY (λvs. (vs INTER FDOM PROB.I) ≠ ∅) lvs 
                           ==> problem_plan_bound PROB <
                                (SUM (MAP (\vs'. N_gen 
                                               (\PROB. 
                                                    Π CARD
                                                       (invariant_states (vs ∩ FDOM PROB.I) INSERT
                                                           IMAGE invariant_states ((IMAGE (λvs''. vs'' ∩ FDOM PROB.I) VS) DIFF {EMPTY})))
                                               PROB
                                               vs'
                                               lvs) lvs) + 1) ))))``,
SRW_TAC[][]
THEN `(∀PROB'.
          planning_problem PROB' ∧ FDOM PROB'.I ⊆ vs ∪ BIGUNION VS ∧
          (∀vs'.
             invariant_prob (vs INSERT VS) (prob_proj (PROB',vs'))) ⇒
          problem_plan_bound PROB' ≤
          Π CARD
            (invariant_states (vs ∩ FDOM PROB'.I) INSERT
             IMAGE invariant_states
               (IMAGE (λvs. vs ∩ FDOM PROB'.I) VS DIFF {∅})))` by 
              (SRW_TAC[][]
               THEN MP_TAC(invariant_prob_diameter_bound_thm |> Q.SPEC `VS`)
               THEN SRW_TAC[][]
               THEN METIS_TAC[scc_lemma_1_4_2_1_1_1_4])
THEN `(∀PROB' vs'.
          vs' ∩ FDOM PROB'.I ≠ ∅ ∧ planning_problem PROB' ∧
          FDOM PROB'.I ⊆ vs ∪ BIGUNION VS ∧
          (∀vs'.
             invariant_prob (vs INSERT VS) (prob_proj (PROB',vs'))) ⇒
          planning_problem (prob_proj (PROB',vs')) ∧
          FDOM (prob_proj (PROB',vs')).I ⊆ vs ∪ BIGUNION VS ∧
          ∀vs''.
            invariant_prob (vs INSERT VS)
              (prob_proj (prob_proj (PROB',vs'),vs'')))` by
        (SRW_TAC[][]
        THEN1 METIS_TAC[graph_plan_lemma_2_2]
        THEN1 METIS_TAC[invariant_prob_diameter_bounding_algorithm_thm_1, SUBSET_TRANS]
        THEN1 METIS_TAC[invariant_prob_diameter_bounding_algorithm_thm_2])
THEN MP_TAC(SIMP_RULE(srw_ss())[]
               (N_super_gen_valid_thm |> Q.SPECL[`(\PROB. Π CARD
                                       (invariant_states (vs ∩ FDOM PROB.I) INSERT
                                         IMAGE invariant_states ((IMAGE (λvs. vs ∩ FDOM PROB.I) VS) DIFF {EMPTY})))`,
                           `(\PROB. planning_problem PROB /\ FDOM PROB.I SUBSET (vs UNION BIGUNION (VS))
                                    /\ (!vs'. invariant_prob (vs INSERT VS) (prob_proj(PROB, vs')))) `]))
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`lvs`])
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`PROB`])
THEN SRW_TAC[][])


val invariant_prob_diameter_bounding_algorithm_simple_thm_1 = store_thm("invariant_prob_diameter_bounding_algorithm_simple_thm_1",
``!f. (!PROB. planning_problem PROB==>
              ?VS:(('a -> bool)-> bool) vs:('a -> bool).
                    (f(PROB) = vs INSERT VS) /\ FINITE VS /\ (!vs. vs IN VS ==> FINITE vs)
                    /\ (!vs1 vs2. vs1 IN VS /\ vs2 IN VS /\ ~(vs1 = vs2) ==> DISJOINT vs1 vs2)
                    /\ FINITE vs /\ (!vs1. vs1 IN VS ==> DISJOINT vs vs1) /\ ~(vs IN VS)
                    /\ invariant_prob (vs INSERT VS) (PROB) /\ FDOM PROB.I SUBSET (vs UNION BIGUNION (VS)))
       ==> 
           (!lvs. EVERY (λvs. vs ≠ ∅) lvs /\ ALL_DISTINCT lvs /\ disjoint_varset lvs /\ lvs ≠ [] ==>
                 (!PROB. planning_problem PROB 
                         /\ (FDOM PROB.I = BIGUNION (set lvs)) /\ top_sorted (PROB,lvs)
                         ==> problem_plan_bound PROB <
                                (SUM (MAP (\vs. N_gen 
                                               (\PROB. 
                                                    Π CARD
                                                       (invariant_states (CHOICE (f PROB)) INSERT
                                                           IMAGE invariant_states (REST (f PROB))))
                                               PROB
                                               vs
                                               lvs) lvs) + 1) ))``,
NTAC 2 STRIP_TAC
THEN MATCH_MP_TAC(
SIMP_RULE(srw_ss())[](N_gen_valid_thm |> Q.SPECL[`(\PROB. 
                                   Π CARD
                                      ((invariant_states ((CHOICE (f PROB)))) INSERT
                                            (IMAGE invariant_states (REST (f PROB))
                                                                      )))`]))
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `PROB'`)
THEN SRW_TAC[][]
THEN `(∀vs'. vs' ∈ REST (vs INSERT VS) ⇒ FINITE vs')` by 
        (SRW_TAC[SatisfySimps.SATISFY_ss][] 
         THEN MP_TAC(REST_SUBSET |> INST_TYPE [alpha |-> ``:'a -> bool``]|> Q.SPEC `(vs INSERT VS)`) THEN STRIP_TAC
         THEN FULL_SIMP_TAC(bool_ss)[SUBSET_DEF,IN_INSERT]
         THEN METIS_TAC[])
THEN `(∀vs1 vs2.
          vs1 ∈ REST (vs INSERT VS) ∧ vs2 ∈ REST (vs INSERT VS) ∧
          vs1 ≠ vs2 ⇒
          DISJOINT vs1 vs2)` by 
        (SRW_TAC[SatisfySimps.SATISFY_ss][] 
         THEN MP_TAC(REST_SUBSET |> INST_TYPE [alpha |-> ``:'a -> bool``]|> Q.SPEC `(vs INSERT VS)`) THEN STRIP_TAC
         THEN FULL_SIMP_TAC(bool_ss)[SUBSET_DEF,IN_INSERT]
         THEN METIS_TAC[DISJOINT_SYM])
THEN MP_TAC(invariant_prob_diameter_loose_bound_thm |> Q.SPEC `REST (vs INSERT VS)`)
THEN SRW_TAC[][]
THEN `FINITE (CHOICE (vs INSERT VS))` by 
        (MP_TAC(CHOICE_DEF |> INST_TYPE [alpha |-> ``:'a -> bool``]|> Q.SPEC `(vs INSERT VS)`) THEN STRIP_TAC
         THEN FULL_SIMP_TAC(bool_ss)[SUBSET_DEF,IN_INSERT]
         THEN METIS_TAC[NOT_INSERT_EMPTY,DISJOINT_SYM])
THEN `(∀vs1.
          vs1 ∈ REST (vs INSERT VS) ⇒
          DISJOINT (CHOICE (vs INSERT VS)) vs1)` by
        (SRW_TAC[][]
         THEN MP_TAC(CHOICE_DEF |> INST_TYPE [alpha |-> ``:'a -> bool``]|> Q.SPEC `(vs INSERT VS)`) THEN STRIP_TAC
         THEN MP_TAC(REST_SUBSET |> INST_TYPE [alpha |-> ``:'a -> bool``]|> Q.SPEC `(vs INSERT VS)`) THEN STRIP_TAC
         THEN FULL_SIMP_TAC(bool_ss)[SUBSET_DEF,IN_INSERT]
         THEN METIS_TAC[NOT_INSERT_EMPTY,DISJOINT_SYM, CHOICE_NOT_IN_REST])
THEN Q.PAT_ASSUM `!x. FINITE x /\ P x ==> Q x` (MP_TAC o Q.SPEC `CHOICE (vs INSERT VS)`) (* getting ass 12*)
THEN SRW_TAC[][CHOICE_NOT_IN_REST]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN METIS_TAC[NOT_INSERT_EMPTY, CHOICE_INSERT_REST, BIGUNION_INSERT])


val invariant_prob_diameter_bounding_algorithm_simple_thm = store_thm("invariant_prob_diameter_bounding_algorithm_simple_thm",
``!f. (!PROB. planning_problem PROB==>
              ?VS:(('a -> bool)-> bool) vs:('a -> bool).
                    (f(PROB) = vs INSERT VS) /\ FINITE VS /\ (!vs. vs IN VS ==> FINITE vs)
                    /\ (!vs1 vs2. vs1 IN VS /\ vs2 IN VS /\ ~(vs1 = vs2) ==> DISJOINT vs1 vs2)
                    /\ FINITE vs /\ (!vs1. vs1 IN VS ==> DISJOINT vs vs1) /\ ~(vs IN VS)
                    /\ invariant_prob (vs INSERT VS) (PROB) /\ FDOM PROB.I SUBSET (vs UNION BIGUNION (VS)))
       ==> 
           (!lvs. EVERY (λvs. vs ≠ ∅) lvs /\ ALL_DISTINCT lvs /\ disjoint_varset lvs /\ lvs ≠ [] ==>
                 (!PROB. planning_problem PROB 
                         /\ (FDOM PROB.I = BIGUNION (set lvs)) /\ top_sorted (PROB,lvs)
                         ==> problem_plan_bound PROB <
                                (SUM (MAP (\vs. N_gen 
                                               (\PROB. 
                                                    Π (\vs. CARD vs + 1)
                                                       (CHOICE (f PROB) INSERT (REST (f PROB))))
                                               PROB
                                               vs
                                               lvs) lvs) + 1) ))``,
SRW_TAC[][]
THEN
`problem_plan_bound PROB <
      SUM
        (MAP
           (λvs.
              N_gen
                (λPROB.
                   Π CARD
                     (invariant_states (CHOICE (f PROB)) INSERT
                      IMAGE invariant_states (REST (f PROB)))) PROB vs
                lvs) lvs) + 1` by
                                  (MP_TAC((SIMP_RULE(srw_ss())[AND_IMP_INTRO, PULL_FORALL]
                                              invariant_prob_diameter_bounding_algorithm_simple_thm_1) |> Q.SPECL[ `f`, `lvs`,`PROB`])
                                   THEN SRW_TAC[][invariant_state_space_CARD, FINITE_REST]
                                   THEN FIRST_X_ASSUM MATCH_MP_TAC THEN SRW_TAC[][]
                                   THEN LAST_X_ASSUM (MP_TAC o (Q.SPEC `PROB'`))
                                   THEN SRW_TAC[][] THEN Q.EXISTS_TAC `VS` THEN Q.EXISTS_TAC `vs`
                                   THEN SRW_TAC[][])
THEN FULL_SIMP_TAC(srw_ss())[]
THEN
`MAP (λvs'. N_gen
          (λPROB.
               Π CARD
                 (invariant_states (CHOICE (f PROB)) INSERT
                   IMAGE invariant_states (REST (f PROB)))) PROB vs'
            lvs) lvs =
 MAP (λvs'. N_gen
          (λPROB.
           Π (λvs. CARD vs + 1)
                        (CHOICE (f PROB) INSERT REST (f PROB))) PROB vs'
            lvs) lvs ` by 
          (REWRITE_TAC[listTheory.MAP_EQ_f] THEN NTAC 2 STRIP_TAC
           THEN SRW_TAC[][] THEN MATCH_MP_TAC (N_gen_eq) THEN SRW_TAC[][]
           THEN LAST_X_ASSUM (MP_TAC o Q.SPEC `x`)
           THEN SRW_TAC[][]
           THEN `FINITE (REST (f x))` by METIS_TAC[FINITE_REST, FINITE_INSERT]
           THEN MP_TAC(invariant_state_space_CARD |> Q.SPEC `REST (f (x:'a problem))`)
           THEN SRW_TAC[][]
           THEN `FINITE (CHOICE (vs INSERT VS))` by 
                (MP_TAC(CHOICE_DEF |> INST_TYPE [alpha |-> ``:'a -> bool``]|> Q.SPEC `(vs INSERT VS)`) THEN STRIP_TAC
                 THEN FULL_SIMP_TAC(bool_ss)[SUBSET_DEF,IN_INSERT]
                 THEN METIS_TAC[NOT_INSERT_EMPTY,DISJOINT_SYM])
           THEN `(∀vs'. vs' ∈ REST (vs INSERT VS) ⇒ FINITE vs')` by 
                (SRW_TAC[SatisfySimps.SATISFY_ss][] 
                 THEN MP_TAC(REST_SUBSET |> INST_TYPE [alpha |-> ``:'a -> bool``]|> Q.SPEC `(vs INSERT VS)`) THEN STRIP_TAC
                 THEN FULL_SIMP_TAC(bool_ss)[SUBSET_DEF,IN_INSERT]
                 THEN METIS_TAC[])
           THEN `(CHOICE (vs INSERT VS) INSERT REST (vs INSERT VS)) = vs INSERT VS` by METIS_TAC[NOT_INSERT_EMPTY, CHOICE_INSERT_REST]
           THEN Q.PAT_ASSUM `!x. FINITE x /\ P x ==> Q x` (MP_TAC o Q.SPEC `CHOICE (vs INSERT VS)`)
           THEN SRW_TAC[][CHOICE_NOT_IN_REST])
THEN METIS_TAC[])


val invariant_prob_diameter_bounding_algorithm_thm = store_thm("invariant_prob_diameter_bounding_algorithm_thm",
``!f. (!PROB. planning_problem PROB==>
              ?VS vs.
                    (f(PROB) = vs INSERT VS) /\ FINITE VS /\ (!vs. vs IN VS ==> FINITE vs)
                    /\ (!vs1 vs2. vs1 IN VS /\ vs2 IN VS /\ ~(vs1 = vs2) ==> DISJOINT vs1 vs2)
                    /\ FINITE vs /\ (!vs1. vs1 IN VS ==> DISJOINT vs vs1) /\ ~(vs IN VS)
                    /\ invariant_prob (vs INSERT VS) (PROB) /\ FDOM PROB.I SUBSET (vs UNION BIGUNION (VS)))
       ==> 
           (!lvs. EVERY (λvs. vs ≠ ∅) lvs /\ ALL_DISTINCT lvs /\ disjoint_varset lvs /\ lvs ≠ [] ==>
                 (!PROB. planning_problem PROB 
                         /\ (FDOM PROB.I = BIGUNION (set lvs)) /\ top_sorted (PROB,lvs)
                         ==> problem_plan_bound PROB <
                                (SUM (MAP (\vs'. N_gen 
                                               (\PROB. 
                                                    Π CARD
                                                       (invariant_states ((CHOICE (f PROB)) ∩ FDOM PROB.I) INSERT
                                                           IMAGE invariant_states ((IMAGE (λvs''. vs'' ∩ FDOM PROB.I) (REST (f PROB))) DIFF {EMPTY})))
                                               PROB
                                               vs'
                                               lvs) lvs) + 1) ))``,
NTAC 2 STRIP_TAC
THEN MATCH_MP_TAC(
SIMP_RULE(srw_ss())[]
(N_gen_valid_thm |> Q.SPECL[`(\PROB. 
                                   Π CARD
                                      (invariant_states ((CHOICE (f PROB)) ∩ FDOM PROB.I) INSERT
                                            IMAGE invariant_states ((IMAGE (λvs''. vs'' ∩ FDOM PROB.I)
                                                                           (REST (f PROB)))
                                                                      DIFF {EMPTY})))`]))
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `PROB'`)
THEN SRW_TAC[][]
THEN `(∀vs'. vs' ∈ REST (vs INSERT VS) ⇒ FINITE vs')` by 
        (SRW_TAC[SatisfySimps.SATISFY_ss][] 
         THEN MP_TAC(REST_SUBSET |> INST_TYPE [alpha |-> ``:'a -> bool``]|> Q.SPEC `(vs INSERT VS)`) THEN STRIP_TAC
         THEN FULL_SIMP_TAC(bool_ss)[SUBSET_DEF,IN_INSERT]
         THEN METIS_TAC[])
THEN `(∀vs1 vs2.
          vs1 ∈ REST (vs INSERT VS) ∧ vs2 ∈ REST (vs INSERT VS) ∧
          vs1 ≠ vs2 ⇒
          DISJOINT vs1 vs2)` by 
        (SRW_TAC[SatisfySimps.SATISFY_ss][] 
         THEN MP_TAC(REST_SUBSET |> INST_TYPE [alpha |-> ``:'a -> bool``]|> Q.SPEC `(vs INSERT VS)`) THEN STRIP_TAC
         THEN FULL_SIMP_TAC(bool_ss)[SUBSET_DEF,IN_INSERT]
         THEN METIS_TAC[DISJOINT_SYM])
THEN MP_TAC(invariant_prob_diameter_bound_thm |> Q.SPEC `REST (vs INSERT VS)`)
THEN SRW_TAC[][]
THEN `FINITE (CHOICE (vs INSERT VS))` by 
        (MP_TAC(CHOICE_DEF |> INST_TYPE [alpha |-> ``:'a -> bool``]|> Q.SPEC `(vs INSERT VS)`) THEN STRIP_TAC
         THEN FULL_SIMP_TAC(bool_ss)[SUBSET_DEF,IN_INSERT]
         THEN METIS_TAC[NOT_INSERT_EMPTY,DISJOINT_SYM])
THEN `(∀vs1.
          vs1 ∈ REST (vs INSERT VS) ⇒
          DISJOINT (CHOICE (vs INSERT VS)) vs1)` by
        (SRW_TAC[][]
         THEN MP_TAC(CHOICE_DEF |> INST_TYPE [alpha |-> ``:'a -> bool``]|> Q.SPEC `(vs INSERT VS)`) THEN STRIP_TAC
         THEN MP_TAC(REST_SUBSET |> INST_TYPE [alpha |-> ``:'a -> bool``]|> Q.SPEC `(vs INSERT VS)`) THEN STRIP_TAC
         THEN FULL_SIMP_TAC(bool_ss)[SUBSET_DEF,IN_INSERT]
         THEN METIS_TAC[NOT_INSERT_EMPTY,DISJOINT_SYM, CHOICE_NOT_IN_REST])
THEN Q.PAT_ASSUM `!x. FINITE x /\ P x ==> Q x` (MP_TAC o Q.SPEC `CHOICE (vs INSERT VS)`) (* getting ass 12*)
THEN SRW_TAC[][CHOICE_NOT_IN_REST]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN METIS_TAC[NOT_INSERT_EMPTY, CHOICE_INSERT_REST, BIGUNION_INSERT])


val invariant_prob_reachable_states_thm_4 = store_thm("invariant_prob_reachable_states_thm_4",
``!PROB vs. planning_problem PROB /\ FDOM PROB.I SUBSET vs ==> (PROB = prob_proj(PROB, vs))``,
METIS_TAC[scc_main_lemma_1_6_1_3, scc_lemma_1_4_2_1_1_1_4, parent_children_lemma_1_1_1_6_1])



val invariant_prob_reachable_states_thm_7 = store_thm("invariant_prob_reachable_states_thm_7",
``!PROB VS1 VS2. VS2 SUBSET VS1 /\  invariant_prob VS1 PROB ==> invariant_prob VS2 PROB``,
SRW_TAC[][invariant_prob_def, SUBSET_DEF])


val _ = export_theory();