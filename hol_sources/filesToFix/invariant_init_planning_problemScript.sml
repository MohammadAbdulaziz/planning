open HolKernel Parse boolLib bossLib;
open invariant_state_spaceTheory;
open ParentChildrenBoundTheory;
open SCCMainGraphPlanTheoremTheory;
open SCCGraphPlanTheoremTheory; 
open GraphPlanTheoremTheory;
open FM_planTheory;
open state_spaceTheory;
open pred_setTheory;
open planning_problem_diameter_boundTheory;
open BoundingAlgorithmsTheory;
open state_space_productTheory;
open InvariantsPlusOneTheory;
open finite_mapTheory;

val _ = new_theory "invariant_init_planning_problem";

val invariant_prob_I_def = Define`invariant_prob_I VS PROB = !s. s IN (reachable_s PROB PROB.I) ==> !vs. vs IN VS ==> invariant (DRESTRICT s vs)`


val reachable_I_state_space_thm = store_thm("reachable_I_state_space_thm",
``!PROB. planning_problem PROB ==> state_space (reachable_s PROB PROB.I) (FDOM PROB.I)``,
SRW_TAC[][state_space_def, reachable_s_def]
THEN METIS_TAC[graph_plan_lemma_6_1])


val invariant_prob_reachable_states_I_thm_1_ = store_thm("invariant_prob_reachable_states_I_thm_1_",
``!PROB VS. planning_problem PROB /\ invariant_prob_I VS PROB ==> invariant_ss VS (reachable_s PROB PROB.I) /\ state_space (reachable_s PROB PROB.I) (FDOM PROB.I)``,
SRW_TAC[][invariant_prob_I_def, invariant_ss_def]
THEN SRW_TAC[][state_space_def]
THEN FULL_SIMP_TAC(srw_ss())[reachable_s_def]
THEN METIS_TAC[graph_plan_lemma_6_1])

val invariant_prob_reachable_states_I_thm = store_thm("invariant_prob_reachable_states_I_thm",
``!VS. FINITE VS
       ==> 
       ((!vs. vs IN VS ==> FINITE vs) 
              /\ (!vs1 vs2. vs1 IN VS /\ vs2 IN VS /\ ~(vs1 = vs2) ==> DISJOINT vs1 vs2)
              ==>
             (!vs PROB. FINITE vs /\ planning_problem PROB /\ FDOM PROB.I SUBSET (vs UNION BIGUNION (VS))
                                   /\ invariant_prob_I (vs INSERT VS) PROB /\ (!vs1. vs1 IN VS ==> DISJOINT vs vs1)
                        ==> (reachable_s PROB PROB.I) SUBSET
                               ss_proj (PRODf (IMAGE invariant_states VS) (invariant_states vs)) (FDOM PROB.I)))``,
METIS_TAC[invariant_prob_reachable_states_I_thm_1_, invariant_state_space_thm])



val disjoint_inter = store_thm("disjoint_inter",
``!s t u. DISJOINT s t /\ ~(s INTER u = EMPTY) ==> ~(s INTER u = t INTER u)``,
SRW_TAC[][DISJOINT_DEF, INTER_DEF, EXTENSION] THEN METIS_TAC[])


val inter_disjoint = store_thm("inter_disjoint",
``!s t u. DISJOINT s t ==> (DISJOINT (s INTER u) (t INTER u))``,
SRW_TAC[][DISJOINT_DEF, INTER_DEF, EXTENSION] THEN METIS_TAC[])

val invariant_prob_diameter_loose_bound_I_thm = store_thm("invariant_prob_diameter_loose_bound_I_thm",
``!VS. FINITE VS
       ==> 
       ((!vs. vs IN VS ==> FINITE vs) 
              /\ (!vs1 vs2. vs1 IN VS /\ vs2 IN VS /\ ~(vs1 = vs2) ==> DISJOINT vs1 vs2)
              ==>
             (!vs. FINITE vs /\ (!vs1. vs1 IN VS ==> DISJOINT vs vs1) /\ ~(vs IN VS) ==> 
                   (!PROB. planning_problem PROB /\ FDOM PROB.I SUBSET (vs UNION BIGUNION (VS))
                           /\ invariant_prob_I (vs INSERT VS) PROB
                           ==> problem_plan_bound_I PROB <=
                                Π CARD
                                    (invariant_states vs INSERT
                                        IMAGE invariant_states VS))))``,
SRW_TAC[][]
THEN MP_TAC(SIMP_RULE(srw_ss())[GSYM arithmeticTheory.LE_LT1] bound_main_lemma_reachability_I |> Q.SPECL[`PROB`])
THEN SRW_TAC[][]
THEN `problem_plan_bound_I PROB <= CARD (reachable_s PROB PROB.I)` by DECIDE_TAC
THEN MP_TAC(invariant_prob_reachable_states_I_thm |> Q.SPECL[`VS`])
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`, `PROB`])
THEN SRW_TAC[][]
THEN `CARD
         (PRODf
            (IMAGE invariant_states VS)
            (invariant_states vs)) 
          <= Π CARD
                 (invariant_states vs INSERT
                      IMAGE invariant_states VS)` by
     (MP_TAC(PRODf_CARD
              |> INST_TYPE [beta |-> ``:bool``] 
              |> Q.SPEC `(IMAGE invariant_states VS)`)
     THEN SRW_TAC[][]
     THEN FIRST_X_ASSUM MATCH_MP_TAC
     THEN SRW_TAC[][]
     THEN  Q.EXISTS_TAC `vs`
     THEN SRW_TAC[][]
     THEN1(Cases_on `x ∉ VS`
           THEN SRW_TAC[][]
           THEN METIS_TAC[invariant_prob_reachable_states_thm_2, FINITE_INTER, disjoint_inter])
     THEN1 METIS_TAC[invariant_state_space_thm_13, FINITE_INTER]
     THEN1 METIS_TAC[invariant_states_state_space_thm]
     THEN1(`DISJOINT vs x` by METIS_TAC[]
            THEN `state_space (invariant_states x) x` by METIS_TAC[invariant_states_state_space_thm]
            THEN METIS_TAC[invariant_state_space_thm_13, EQ_SS_DOM])
      THEN1 METIS_TAC[invariant_state_space_thm_13, EQ_SS_DOM, invariant_states_state_space_thm]
      THEN1 METIS_TAC[invariant_states_state_space_thm]
      THEN1 METIS_TAC[invariant_state_space_thm_13])
THEN `FINITE (PRODf (IMAGE invariant_states VS) (invariant_states vs))` by
      (MP_TAC (PRODf_FINITE |> INST_TYPE [beta |-> ``:bool``]
                                    |> Q.SPEC `(IMAGE invariant_states VS)`)
       THEN SRW_TAC[][]
       THEN FIRST_X_ASSUM MATCH_MP_TAC
       THEN Q.EXISTS_TAC `vs`
       THEN SRW_TAC[][]
       THEN1(Cases_on `x ∉ VS`
            THEN SRW_TAC[][]
            THEN METIS_TAC[invariant_prob_reachable_states_thm_2, FINITE_INTER, disjoint_inter])
      THEN1 METIS_TAC[invariant_state_space_thm_13, FINITE_INTER]
      THEN1 METIS_TAC[invariant_states_state_space_thm]
      THEN1(`DISJOINT vs x` by METIS_TAC[]
            THEN `state_space (invariant_states x) x` by METIS_TAC[invariant_states_state_space_thm]
            THEN METIS_TAC[invariant_state_space_thm_13, EQ_SS_DOM])
      THEN1 METIS_TAC[invariant_state_space_thm_13, EQ_SS_DOM, invariant_states_state_space_thm]
      THEN1 METIS_TAC[invariant_states_state_space_thm]
      THEN1 METIS_TAC[invariant_state_space_thm_13])
THEN FULL_SIMP_TAC(srw_ss())[ss_proj_def]
THEN METIS_TAC[SUBSET_FINITE, SUBSET_TRANS, arithmeticTheory.LESS_EQ_TRANS, CARD_SUBSET, CARD_IMAGE, IMAGE_FINITE])

val invariant_prob_diameter_bound_I_thm_1 = store_thm("invariant_prob_diameter_bound_I_thm_1",
``!VS1 VS2 PROB. (!vs1. vs1 IN VS1 ==> ?vs2. vs2 IN VS2 /\ vs1 SUBSET vs2)
                 /\ invariant_prob_I VS2 PROB 
                 ==> invariant_prob_I VS1 PROB``,
SRW_TAC[][invariant_prob_I_def]
THEN METIS_TAC[invariant_state_space_DRESTRICT_SUBSET_thm])

val invariant_prob_diameter_bound_I_thm = store_thm("invariant_prob_diameter_bound_I_thm",
``!VS. FINITE VS
       ==> 
       ((!vs. vs IN VS ==> FINITE vs) 
              /\ (!vs1 vs2. vs1 IN VS /\ vs2 IN VS /\ ~(vs1 = vs2) ==> DISJOINT vs1 vs2)
              ==>
             (!vs. FINITE vs /\ (!vs1. vs1 IN VS ==> DISJOINT vs vs1) /\ ~(vs IN VS) ==> 
                   (!PROB. planning_problem PROB /\ FDOM PROB.I SUBSET (vs UNION BIGUNION (VS))
                           /\ invariant_prob_I (vs INSERT VS) PROB
                           ==> problem_plan_bound_I PROB <=
                                Π CARD
                                    (invariant_states (vs ∩ FDOM PROB.I) INSERT
                                        IMAGE invariant_states ((IMAGE (λvs. vs ∩ FDOM PROB.I) VS) DIFF {EMPTY})))))``,
SRW_TAC[][]
THEN `(∀vs'.
         (∃vs. (vs' = vs ∩ FDOM PROB.I) ∧ vs ∈ VS) ∧ vs' ≠ ∅ ⇒
         FINITE vs')` by
     (SRW_TAC[][] THEN METIS_TAC[FINITE_INTER])
THEN `(∀vs1 vs2.
         ((∃vs. (vs1 = vs ∩ FDOM PROB.I) ∧ vs ∈ VS) ∧ vs1 ≠ ∅) ∧
         ((∃vs. (vs2 = vs ∩ FDOM PROB.I) ∧ vs ∈ VS) ∧ vs2 ≠ ∅) ∧
         vs1 ≠ vs2 ⇒
         DISJOINT vs1 vs2)` by
      (SRW_TAC[][] THEN FULL_SIMP_TAC(srw_ss())[INTER_DEF, DISJOINT_DEF, EXTENSION] THEN 
      SRW_TAC[][] THEN METIS_TAC[])
THEN MP_TAC(SIMP_RULE(srw_ss())[](invariant_prob_diameter_loose_bound_I_thm |> Q.SPEC `((IMAGE (λvs. vs ∩ FDOM PROB.I) VS) DIFF {EMPTY})`))
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]
THEN `∀vs.
         FINITE vs ∧
         (∀vs1.
            (∃vs. (vs1 = vs ∩ FDOM PROB.I) ∧ vs ∈ VS) ∧ vs1 ≠ ∅ ⇒
            DISJOINT vs vs1) ∧
         ((∀vs'. vs ≠ vs' ∩ FDOM PROB.I ∨ vs' ∉ VS) ∨ (vs = ∅)) ⇒
         ∀PROB'.
           planning_problem PROB' ∧
           FDOM PROB'.I ⊆
           vs ∪ BIGUNION (IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅}) ∧
           invariant_prob_I
             (vs INSERT IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅})
             PROB' ⇒
           problem_plan_bound_I PROB' ≤
           Π CARD
             (invariant_states vs INSERT
              IMAGE invariant_states
                (IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅}))` by (FIRST_X_ASSUM MATCH_MP_TAC THEN METIS_TAC[])
THEN WEAKEN_TAC is_imp
THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `(vs ∩ FDOM PROB.I)`)
THEN SRW_TAC[][]
THEN `(∀vs1.
          (∃vs. (vs1 = vs ∩ FDOM PROB.I) ∧ vs ∈ VS) ∧ vs1 ≠ ∅ ⇒
          DISJOINT (vs ∩ FDOM PROB.I) vs1)` by 
      (SRW_TAC[][] THEN FULL_SIMP_TAC(srw_ss())[INTER_DEF, DISJOINT_DEF, EXTENSION] THEN METIS_TAC[])
THEN `((∀vs'. vs ∩ FDOM PROB.I ≠ vs' ∩ FDOM PROB.I ∨ vs' ∉ VS) ∨
        (vs ∩ FDOM PROB.I = ∅))` by 
      (SRW_TAC[][] THEN FULL_SIMP_TAC(srw_ss())[INTER_DEF, DISJOINT_DEF, EXTENSION] THEN METIS_TAC[])
THEN `∀PROB'.
         planning_problem PROB' ∧
         FDOM PROB'.I ⊆
         vs ∩ FDOM PROB.I ∪
         BIGUNION (IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅}) ∧
         invariant_prob_I
           (vs ∩ FDOM PROB.I INSERT
            IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅}) PROB' ⇒
         problem_plan_bound_I PROB' ≤
         Π CARD
           (invariant_states (vs ∩ FDOM PROB.I) INSERT
            IMAGE invariant_states
              (IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅}))` by METIS_TAC[]
THEN WEAKEN_TAC is_imp
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN1(NTAC 7 (WEAKEN_TAC is_forall) 
      THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF, BIGUNION, INTER_DEF, EXTENSION]
      THEN SRW_TAC[][]
      THEN Cases_on `x ∈ vs`
      THEN SRW_TAC[][]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `x`)
      THEN SRW_TAC[][]
      THEN Q.EXISTS_TAC `s INTER (FDOM PROB.I)`
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN1(NTAC 7 (WEAKEN_TAC is_forall) 
      THEN MATCH_MP_TAC(invariant_prob_diameter_bound_I_thm_1
              |> Q.SPECL[`(vs ∩ FDOM PROB.I INSERT IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅}) `,
                          `(vs INSERT VS)`])
      THEN SRW_TAC[][]
      THEN1(Q.EXISTS_TAC `vs`
            THEN SRW_TAC[][]) 
      THEN1(Q.EXISTS_TAC `vs'`
            THEN SRW_TAC[][]))
THEN1(NTAC 6 (WEAKEN_TAC is_forall) 
      THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF, BIGUNION, INTER_DEF, EXTENSION]
      THEN SRW_TAC[][]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `x`)
      THEN SRW_TAC[][]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `x`)
      THEN SRW_TAC[][]
      THEN Q.EXISTS_TAC `s INTER (FDOM PROB.I)`
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN1(NTAC 6 (WEAKEN_TAC is_forall) 
      THEN MATCH_MP_TAC(invariant_prob_diameter_bound_I_thm_1
              |> Q.SPECL[`(EMPTY INSERT IMAGE (λvs. vs ∩ FDOM PROB.I) VS DIFF {∅}) `,
                          `(vs INSERT VS)`])
      THEN SRW_TAC[][]
      THEN1(Q.EXISTS_TAC `vs`
            THEN SRW_TAC[][]) 
      THEN1(Q.EXISTS_TAC `vs'`
            THEN SRW_TAC[][])))

val invariant_prob_diameter_bounding_algorithm_I_thm_1 = store_thm("invariant_prob_diameter_bounding_algorithm_I_thm_1",
``!PROB vs. FDOM (prob_proj(PROB, vs)).I SUBSET (FDOM PROB.I)``,
SRW_TAC[][prob_proj_def]
THEN SRW_TAC[][FDOM_DRESTRICT])

val invariant_prob_diameter_bounding_algorithm_I_thm_2 = store_thm("invariant_prob_diameter_bounding_algorithm_I_thm_2",
``!PROB vs1 vs2 . prob_proj(prob_proj(PROB, vs1), vs2) = prob_proj(PROB, (vs1 INTER vs2))``,
SRW_TAC[][prob_proj_def, EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1(Q.EXISTS_TAC `a'`
      THEN SRW_TAC[][])
THEN Q.EXISTS_TAC `action_proj(a,vs1)`
THEN SRW_TAC[][action_proj_def]
THEN Q.EXISTS_TAC `a`
THEN SRW_TAC[][])



(*val invariant_prob_diameter_bounding_algorithm_I_thm = store_thm("invariant_prob_diameter_bounding_algorithm_I_thm",
``!VS. FINITE VS
       ==> 
       ((!vs. vs IN VS ==> FINITE vs) 
              /\ (!vs1 vs2. vs1 IN VS /\ vs2 IN VS /\ ~(vs1 = vs2) ==> DISJOINT vs1 vs2)
              ==>
             (!vs. FINITE vs /\ ~(vs = EMPTY) /\ (!vs1. vs1 IN VS ==> DISJOINT vs vs1) /\ ~(vs IN VS) ==> 
                   (!lvs. ALL_DISTINCT lvs /\ disjoint_varset lvs /\ lvs ≠ [] ==>
                   (!PROB. planning_problem PROB /\ FDOM PROB.I SUBSET (vs UNION BIGUNION (VS))
                           /\ (!vs'. invariant_prob_I (vs INSERT VS) (prob_proj(PROB, vs')))
                           /\ (FDOM PROB.I = BIGUNION (set lvs)) /\ top_sorted (PROB,lvs)
                           /\ EVERY (λvs. (vs INTER FDOM PROB.I) ≠ ∅) lvs 
                           ==> problem_plan_bound PROB <
                                (SUM (MAP (\vs'. N_gen 
                                               (\PROB. 
                                                    Π CARD
                                                       (invariant_states (vs ∩ FDOM PROB.I) INSERT
                                                           IMAGE invariant_states ((IMAGE (λvs''. vs'' ∩ FDOM PROB.I) VS) DIFF {EMPTY})))
                                               PROB
                                               vs'
                                               lvs) lvs) + 1) ))))``,
SRW_TAC[][]
THEN `(∀PROB'.
          planning_problem PROB' ∧ FDOM PROB'.I ⊆ vs ∪ BIGUNION VS ∧
          (∀vs'.
             invariant_prob_I (vs INSERT VS) (prob_proj (PROB',vs'))) ⇒
          problem_plan_bound PROB' ≤
          Π CARD
            (invariant_states (vs ∩ FDOM PROB'.I) INSERT
             IMAGE invariant_states
               (IMAGE (λvs. vs ∩ FDOM PROB'.I) VS DIFF {∅})))` by 
              (SRW_TAC[][]
               THEN MP_TAC(invariant_prob_diameter_bound_I_thm |> Q.SPEC `VS`)
               THEN SRW_TAC[][]
               THEN METIS_TAC[scc_lemma_1_4_2_1_1_1_4])
THEN `(∀PROB' vs'.
          vs' ∩ FDOM PROB'.I ≠ ∅ ∧ planning_problem PROB' ∧
          FDOM PROB'.I ⊆ vs ∪ BIGUNION VS ∧
          (∀vs'.
             invariant_prob_I (vs INSERT VS) (prob_proj (PROB',vs'))) ⇒
          planning_problem (prob_proj (PROB',vs')) ∧
          FDOM (prob_proj (PROB',vs')).I ⊆ vs ∪ BIGUNION VS ∧
          ∀vs''.
            invariant_prob_I (vs INSERT VS)
              (prob_proj (prob_proj (PROB',vs'),vs'')))` by
        (SRW_TAC[][]
        THEN1 METIS_TAC[graph_plan_lemma_2_2]
        THEN1 METIS_TAC[invariant_prob_diameter_bounding_algorithm_I_thm_1, SUBSET_TRANS]
        THEN1 METIS_TAC[invariant_prob_diameter_bounding_algorithm_I_thm_2])
THEN MP_TAC(SIMP_RULE(srw_ss())[]
               (N_super_gen_valid_thm |> Q.SPECL[`(\PROB. Π CARD
                                       (invariant_states (vs ∩ FDOM PROB.I) INSERT
                                         IMAGE invariant_states ((IMAGE (λvs. vs ∩ FDOM PROB.I) VS) DIFF {EMPTY})))`,
                           `(\PROB. planning_problem PROB /\ FDOM PROB.I SUBSET (vs UNION BIGUNION (VS))
                                    /\ (!vs'. invariant_prob_I (vs INSERT VS) (prob_proj(PROB, vs')))) `]))
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`lvs`])
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`PROB`])
THEN SRW_TAC[][])

*)


val invariant_prob_reachable_states_thm_4 = store_thm("invariant_prob_reachable_states_thm_4",
``!PROB vs. planning_problem PROB /\ FDOM PROB.I SUBSET vs ==> (PROB = prob_proj(PROB, vs))``,
METIS_TAC[scc_main_lemma_1_6_1_3, scc_lemma_1_4_2_1_1_1_4, parent_children_lemma_1_1_1_6_1])



val invariant_prob_reachable_states_thm_7 = store_thm("invariant_prob_reachable_states_thm_7",
``!PROB VS1 VS2. VS2 SUBSET VS1 /\  invariant_prob_I VS1 PROB ==> invariant_prob_I VS2 PROB``,
SRW_TAC[][invariant_prob_I_def, SUBSET_DEF])


val _ = export_theory();