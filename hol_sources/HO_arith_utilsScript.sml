open HolKernel Parse boolLib bossLib;

val _ = new_theory "HO_arith_utils";

val general_theorem = store_thm("general_theorem",
  ``!P f l. (!p. P p /\ f p > l:num ==> ?p'. P p' /\ f p' < f p) ==>
    !p. P p ==> ?p'. P p' /\ f p' <= l``,
  NTAC 4 STRIP_TAC THEN
  Q_TAC SUFF_TAC `!n p. (n = f p) /\ P p ==> ?p'. P p' /\ f p' <= l` THEN1 METIS_TAC[] THEN
  HO_MATCH_MP_TAC arithmeticTheory.COMPLETE_INDUCTION THEN
  REPEAT STRIP_TAC THEN
  Cases_on `f p <= l` THENL [
    METIS_TAC[],
    `f p > l` by DECIDE_TAC THEN
    `?p'. P p' /\ f p' < f p` by METIS_TAC[] THEN
    Cases_on `f p' <= l` THEN1 METIS_TAC[] THEN
    METIS_TAC [DECIDE ``~(x:num <= y:num) <=> x > y``]
  ]);

val general_theorem' = store_thm("general_theorem'",
  ``!P f l prob. (!p. P( prob, p) /\ f p > l:num ==> ?p'. P( prob, p') /\ f p' < f p) ==>
    !p. P( prob, p) ==> ?p'. P( prob, p') /\ f p' <= l``,
  NTAC 5 STRIP_TAC THEN (* GEN_TAC THEN completeInduct_on `f p` THEN *)
  Q_TAC SUFF_TAC `!n p. (n = f p) /\ P( prob, p) ==> ?p'. P( prob,  p') /\ f p' <= l` THEN1 METIS_TAC[] THEN
  (* MATCH_MP_TAC
    (arithmeticTheory.COMPLETE_INDUCTION
    |> SPEC_ALL
    |> INST [``P:num->bool`` |->
             ``\n:num. !p:'a. (n = f p) /\ P( prob, p) ==> ?p'. P( prob, p') /\ f p' <= l``]
    |> BETA_RULE) *)
  HO_MATCH_MP_TAC arithmeticTheory.COMPLETE_INDUCTION THEN
  REPEAT STRIP_TAC THEN
  Cases_on `f p <= l` THENL [
    METIS_TAC[],
    `f p > l` by DECIDE_TAC THEN
    `?p'. P( prob, p') /\ f p' < f p` by METIS_TAC[] THEN
    Cases_on `f p' <= l` THEN1 METIS_TAC[] THEN
    METIS_TAC [DECIDE ``~(x:num <= y:num) <=> x > y``]
  ]);

val _ = export_theory();
