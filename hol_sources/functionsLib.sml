
structure functionsLib :> functionsLib =
struct

open HolKernel Parse boolLib bossLib;

fun assume_conl_after_proving_concl thm = SUBGOAL_THEN (concl(UNDISCH(thm))) (STRIP_ASSUME_TAC) ;

fun assume_thm_concl_after_proving_assums thm = assume_conl_after_proving_concl thm THENL [MATCH_MP_TAC thm, REWRITE_TAC[]];

fun assume_thm_concl_after_proving_assums_2 thm = assume_conl_after_proving_concl thm THEN (MATCH_MP_TAC thm);

end;