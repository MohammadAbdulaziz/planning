open HolKernel Parse boolLib bossLib;

open finite_mapTheory
open arithmeticTheory
open pred_setTheory
open rich_listTheory
open listTheory; 
open utilsTheory;
open factoredSystemTheory 
open sublistTheory;
open systemAbstractionTheory;
open topologicalPropsTheory;
open list_utilsTheory;
open fmap_utilsTheory;
open actionSeqProcessTheory
open dependencyTheory
open fmap_utilsTheory
open set_utilsTheory
open quantHeuristicsTheory

val _ = new_theory "parentChildStructure";

val child_parent_rel_def 
   = Define `(child_parent_rel(PROB, vs)
     = ~(dep_var_set(PROB, vs,  (prob_dom PROB) DIFF vs)))`;

val vset_drest_eff_eq = store_thm("vset_drest_eff_eq",
``! a vs. varset_action (a,vs) ==>  (DRESTRICT (SND a) vs = SND a)``,
SRW_TAC[][varset_action_def]
THEN SRW_TAC[][exec_drest_5]);

val vset_eq_vset_diff = store_thm("vset_eq_vset_diff",
``! PROB a vs. (a IN PROB /\ child_parent_rel(PROB, vs) /\ (FDOM (SND a) <> EMPTY))
               ==> (varset_action(a, vs) <=>
                       ~varset_action(a, (prob_dom PROB) DIFF vs))``,
SRW_TAC[][]
THEN MP_TAC(FDOM_eff_subset_prob_dom_pair |> Q.SPEC `a`)
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[varset_action_def, dep_var_set_def, 
                        dep_def, SUBSET_DEF, DISJOINT_DEF,
                        INTER_DEF, EXTENSION, child_parent_rel_def]
THEN PROVE_TAC[]);

val ndisj_diff_disj_vs = store_thm("ndisj_diff_disj_vs",
``!PROB a vs. child_parent_rel (PROB, vs) /\ a IN PROB 
              /\ ~(DISJOINT (FDOM (SND a)) ((prob_dom PROB) DIFF vs))
              ==> (DISJOINT  (FDOM (SND a)) vs)``,
SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[dep_var_set_def, dep_def, DISJOINT_DEF, INTER_DEF, EXTENSION, child_parent_rel_def, SUBSET_DEF]
THEN PROVE_TAC[]);

val as_proj_child_def = Define `as_proj_child(as, vs) = 
    (FILTER (λa. varset_action (a,vs) /\ (FDOM (SND a)) <> EMPTY )(MAP (λa. (DRESTRICT (FST a) vs,SND a)) as))`; 

val as_proj_child_works = store_thm("as_proj_child_works",
``!PROB as vs.
     (as IN (valid_plans PROB) /\ child_parent_rel(PROB, vs))
     ==> (as_proj_child(as,vs) = as_proj(as,vs))``,
Induct_on `as`
THEN SRW_TAC[][as_proj_child_def, as_proj_def] 
THEN1(FULL_SIMP_TAC(srw_ss())[vset_drest_eff_eq, varset_action_def, action_proj_def])
THEN1(METIS_TAC[as_proj_child_def, as_proj_def, valid_plan_valid_tail])
THEN1(FULL_SIMP_TAC(srw_ss())[vset_drest_eff_eq, varset_action_def])
THEN1(FULL_SIMP_TAC(srw_ss())[]
      THEN1(`~varset_action(h,vs)` by FULL_SIMP_TAC(srw_ss())[varset_action_def]
            THEN METIS_TAC[ndisj_diff_disj_vs, not_vset_not_disj_eff_prod_dom_diff, DISJOINT_DEF, FDOM_DRESTRICT, nempty_drest_dom_imp_nempty_dom, valid_plan_valid_tail, valid_plan_valid_head])
      THEN1(METIS_TAC[ndisj_diff_disj_vs, not_vset_not_disj_eff_prod_dom_diff, DISJOINT_DEF, FDOM_DRESTRICT, nempty_drest_dom_imp_nempty_dom, valid_plan_valid_tail, valid_plan_valid_head]))
THEN1 METIS_TAC[as_proj_child_def, as_proj_def, valid_plan_valid_tail, valid_plan_valid_head]);

val graph_plan_lemma_3 = store_thm("graph_plan_lemma_3",
``!vs as. 
    no_effectless_act(as) ==>
    (LENGTH(FILTER (\a. varset_action(a, vs) ) as) 
	  = LENGTH(as_proj_child(as, vs)))``,
Induct_on`as`
THEN SRW_TAC[][FILTER, as_proj_child_def, no_effectless_act_def]
THEN FULL_SIMP_TAC(srw_ss())[LENGTH_MAP, varset_action_def]);

val as_proj_rem_effless_eq_filter_vset_map_drest_1 = store_thm("as_proj_rem_effless_eq_filter_vset_map_drest_1",
``!as vs. (as_proj_child(as, vs)) =  (FILTER (\a. varset_action(a, vs) /\ FDOM (SND a) <> EMPTY) (MAP (\a. (DRESTRICT (FST a) vs, (SND a) )) as))``,
Induct_on `as` 
THEN SRW_TAC[][as_proj_child_def, action_proj_def]);

val as_proj_rem_effless_eq_filter_vset_map_drest = store_thm("as_proj_rem_effless_eq_filter_vset_map_drest",
``!as vs. (as_proj_child(rem_effectless_act(as), vs)) = 
               FILTER (\a. varset_action(a, vs))
                      (MAP (\ a. (DRESTRICT (FST a) vs, (SND a) ))
                           (rem_effectless_act(as)))``,
Induct_on `as` 
THEN FULL_SIMP_TAC(srw_ss())[as_proj_child_def, action_proj_def, rem_effectless_act_def]
THEN SRW_TAC[][]);

val as_proj_parent_def = Define `as_proj_parent(as, vs) = 
    (FILTER (λa. ~varset_action (a,vs) /\ (FDOM (SND a)) <> EMPTY ) as)`; 

val child_parent_ndisj_pre_disj_eff = store_thm("child_parent_ndisj_pre_disj_eff",
``!PROB vs a. (child_parent_rel(PROB, vs) /\ a IN PROB
               /\ ~ (DISJOINT (FDOM(SND a)) ((prob_dom PROB) DIFF vs)))
              ==>  DISJOINT (FDOM(FST a)) vs``,
SRW_TAC[][]
THEN MP_TAC(FDOM_pre_subset_prob_dom_pair |> Q.SPEC `a`)
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[child_parent_rel_def, dep_var_set_def, dep_def, DISJOINT_DEF, DIFF_DEF, EXTENSION, SUBSET_DEF]
THEN PROVE_TAC[]);

val child_parent_dom_pre_subset_prob_dom = store_thm("child_parent_dom_pre_subset_prob_dom",
``!PROB vs a. (child_parent_rel(PROB, vs) /\ a IN PROB /\ varset_action(a, (prob_dom PROB) DIFF vs) 
               /\ (FDOM (SND a) <> EMPTY))
              ==> ((FDOM (FST a)) SUBSET (prob_dom PROB) DIFF vs)``,
SRW_TAC[][]
THEN MP_TAC(FDOM_pre_subset_prob_dom_pair |> Q.SPEC `a`)
THEN SRW_TAC[][]
THEN `~DISJOINT (FDOM (SND a)) ((prob_dom PROB) DIFF vs)` by 
     (FULL_SIMP_TAC(srw_ss())[child_parent_rel_def, dep_var_set_def, dep_def, SUBSET_DEF, DISJOINT_DEF, INTER_DEF, varset_action_def, EXTENSION]
     THEN PROVE_TAC[])
THEN `DISJOINT (FDOM (FST a)) vs` by METIS_TAC[child_parent_ndisj_pre_disj_eff]
THEN METIS_TAC[SUBSET_DIFF]);

val as_proj_parent_works = store_thm("as_proj_parent_works",
``!PROB as vs.
     as IN (valid_plans PROB) /\ child_parent_rel(PROB, vs)
     ==> (as_proj_parent(as,vs) = as_proj(as,(prob_dom PROB) DIFF vs))``,
Induct_on `as`
THEN SRW_TAC[][as_proj_parent_def, as_proj_def]
THENL
[
  `varset_action (h,(prob_dom PROB) DIFF vs)` by METIS_TAC[vset_eq_vset_diff, valid_plan_valid_head]
					      THEN ASSUME_TAC(child_parent_dom_pre_subset_prob_dom |> Q.SPECL[`PROB`, `vs`, `h`])
					      THEN SRW_TAC[][action_proj_def]
					      THEN FULL_SIMP_TAC(srw_ss())[varset_action_def]
        THEN `h IN PROB` by METIS_TAC[valid_plan_valid_head]
        THEN `FDOM (FST h) ⊆ prob_dom PROB DIFF vs` by METIS_TAC[valid_plan_valid_head]
	THEN SRW_TAC[][exec_drest_5]
	,
	  METIS_TAC[as_proj_parent_def, as_proj_def, valid_plan_valid_head, valid_plan_valid_tail]
		   ,
		     `varset_action (h,(prob_dom PROB) DIFF vs)` by METIS_TAC[vset_eq_vset_diff, valid_plan_valid_head]
								 THEN FULL_SIMP_TAC(srw_ss())[varset_action_def, FDOM_DRESTRICT, INTER_DEF, SUBSET_DEF, EXTENSION]
								 THEN METIS_TAC[valid_plan_valid_head, valid_plan_valid_tail]
								 ,
								   FULL_SIMP_TAC(srw_ss())[varset_action_def, FDOM_DRESTRICT, INTER_DEF, SUBSET_DEF, EXTENSION]
										THEN METIS_TAC[]
								 ,
        FULL_SIMP_TAC(srw_ss())[as_proj_parent_def]
        THEN METIS_TAC[valid_plan_valid_head, valid_plan_valid_tail]
]);

val child_parent_lemma_1 = store_thm("child_parent_lemma_1",
``!PROB as vs.
     (as IN (valid_plans PROB) /\ child_parent_rel(PROB, vs))
     ==>
     ((as_proj_child(as,vs) = as_proj(as,vs))
      /\ (as_proj_parent(as,vs) = as_proj(as,(prob_dom PROB) DIFF vs)))``,
METIS_TAC[as_proj_child_works, as_proj_parent_works]);

val child_parent_lemma_xx = store_thm("child_parent_lemma_xx",
``!PROB a vs. (child_parent_rel(PROB, vs) /\ (a IN PROB) /\ ~(varset_action(a, vs))) 
              ==> (DISJOINT  (FDOM (SND a)) vs)``,
SRW_TAC[][]
THEN MP_TAC(FDOM_eff_subset_prob_dom_pair |> Q.SPEC `a`)
THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF, varset_action_def,dep_var_set_def, dep_def, DISJOINT_DEF, INTER_DEF, EXTENSION, child_parent_rel_def]
THEN PROVE_TAC[]);

val as_valid_child_parent_imp_eq_exec_restrict = store_thm("as_valid_child_parent_imp_eq_exec_restrict",
``! PROB s as vs. ( (as IN (valid_plans PROB))  /\ child_parent_rel(PROB, vs)) 
                  ==>
                     ((DRESTRICT s vs) =
                       (DRESTRICT (exec_plan (s,as_proj_parent(as, vs))) vs))``,
Induct_on `as`
THEN SRW_TAC[][as_proj_parent_def, exec_plan_def]
THEN REWRITE_TAC[GSYM as_proj_parent_def]
THEN METIS_TAC[not_vset_not_disj_eff_prod_dom_diff, disj_imp_eq_proj_exec,
               ndisj_diff_disj_vs, valid_plan_valid_tail,
               valid_plan_valid_head]);

val len_filter_proj_lt_len_filter = store_thm("len_filter_proj_lt_len_filter",
``!as vs. (?a. MEM a as /\ ((\a. varset_action(a,vs)) a))
               ==> LENGTH
                      (FILTER (λa. varset_action (a,vs))
                              (as_proj_parent(as, vs)))
                   < LENGTH (FILTER (λa. varset_action (a,vs)) as)``,
SRW_TAC[][as_proj_parent_def]
THEN METIS_TAC[Q.SPEC ` as` ( Q.SPEC  `(\a. FDOM (SND a) <> EMPTY) ` (Q.ISPEC `(\a. varset_action(a, vs))` len_filter_filter_lt_len_filter))]);

val child_parent_len_in_state_set_le_max_len = store_thm("child_parent_len_in_state_set_le_max_len",
``!PROB s as vs. ((as IN (valid_plans PROB)) /\ child_parent_rel(PROB, vs)
                  /\ ((DRESTRICT s vs) = (DRESTRICT (exec_plan(s,as)) vs))
                  /\ (?a. MEM a as /\ ((\a. varset_action(a,vs)) a))) 
                  ==>
                 (((DRESTRICT (exec_plan(s,as)) vs) = (DRESTRICT (exec_plan (s,as_proj_parent(as, vs))) vs))
                  /\ (LENGTH( FILTER (\a. varset_action(a,vs)) (as_proj_parent(as, vs))) <
                             LENGTH(FILTER (\a. varset_action(a,vs)) as)) )``,
METIS_TAC[as_valid_child_parent_imp_eq_exec_restrict, len_filter_proj_lt_len_filter]);

val child_parent_neq_len_imp_neq = store_thm("child_parent_neq_len_imp_neq",
``!PROB s as vs. ((as IN (valid_plans PROB)) /\ child_parent_rel(PROB, vs) /\ sat_precond_as (s,as))
                 ==>
                 (sat_precond_as (s,as_proj_parent(as, vs)))``,
METIS_TAC[sat_precond_as_proj,  child_parent_lemma_1]);

val proj_eq_filter_vset_diff = store_thm("proj_eq_filter_vset_diff",
``!PROB vs as. (child_parent_rel(PROB, vs) /\ as IN (valid_plans PROB))
               ==>
                  (as_proj_parent (as,vs) = 
                       FILTER (λa. varset_action (a, (prob_dom PROB) DIFF vs) ∧ FDOM (SND a) <> EMPTY) as)``,
Induct_on `as`
THEN SRW_TAC[][as_proj_parent_def]
THEN SRW_TAC[][GSYM as_proj_parent_def]
THEN FULL_SIMP_TAC(srw_ss())[GSYM as_proj_parent_def, vset_eq_vset_diff]
THEN METIS_TAC[vset_eq_vset_diff, valid_plan_valid_head, valid_plan_valid_tail]);

val filter_vset_diff_eq_filter_not_vset = store_thm("filter_vset_diff_eq_filter_not_vset",
``! PROB as vs . (as IN (valid_plans PROB) /\ child_parent_rel(PROB, vs))
                 ==>
                    (FILTER (λa. varset_action (a,(prob_dom PROB) DIFF vs) /\ FDOM (SND a) <> EMPTY ) as  = 
                        FILTER (λa. ¬varset_action (a,vs) ∧ FDOM (SND a) <> EMPTY) as)``,
Induct_on `as`
THEN1 SRW_TAC[][]
THEN REPEAT GEN_TAC
THEN STRIP_TAC 
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN METIS_TAC[vset_eq_vset_diff, valid_plan_valid_tail, valid_plan_valid_head]);

fun cheat g = ACCEPT_TAC (mk_thm g) g

val replace_projected_def = 
   Define `(replace_projected(as'', a'::as', a::as, vs) = 
	    if (varset_action(a, vs)) then 
		   if (a' = (action_proj(a, vs))) then
		             replace_projected(as'' ++ [a], as', as, vs)
					         else 
						           replace_projected(as'', a'::as', as, vs)
									    else
										   replace_projected(as'' ++ [a], a'::as', as, vs))
           /\ ((replace_projected(as'', [], as, vs)) =  as'' ++ (FILTER (\a. ~varset_action(a, vs)) as))
	         /\ ((replace_projected(as'', as, [], vs)) =  as'')`;

val stitch_def = Define`
  (stitch (a'::as') vs (a::as) = if varset_action(a, vs) then 
                                   if a' = action_proj(a,vs) then 
                                     a::stitch as' vs as
                                   else stitch (a'::as') vs as
                                 else a::stitch (a'::as') vs as) /\
  (stitch [] vs as = FILTER (\a. ~varset_action (a, vs)) as) /\
  (stitch (a'::as') vs  [] = [])`;

val replace_projected_stitch = store_thm(
  "replace_projected_stitch",
  ``!as as' acc. replace_projected (acc, as', as, vs) = acc ++ stitch as' vs as``,
  Induct_on `as` THEN Cases_on `as'` THEN 
  SRW_TAC [][stitch_def, replace_projected_def]);

val two_pow_n_is_a_bound2_1 = store_thm("two_pow_n_is_a_bound2_1",
``!as as' as'' vs s. set as SUBSET s  /\ set as'' SUBSET s
					      ==> 
					       set (replace_projected(as'', as', as, vs)) SUBSET s``,
Induct_on `as`
THEN Cases_on `as'`
THEN SRW_TAC[][replace_projected_def]
THEN METIS_TAC[set_subset_filter_subset]);

val two_pow_n_is_a_bound2_1' = store_thm("two_pow_n_is_a_bound2_1'",
``!as as' as'' vs s. as IN (valid_plans s)  /\ as'' IN (valid_plans s)
					            ==> 
						     (replace_projected(as'', as', as, vs)) IN (valid_plans s)``,
Induct_on `as`
THEN Cases_on `as'`
THEN FULL_SIMP_TAC(srw_ss())[replace_projected_def, valid_plans_def]
THEN SRW_TAC[][]
THEN METIS_TAC[set_subset_filter_subset]);

val sublist_replace_proj_append = store_thm("sublist_replace_proj_append",
``! as as' as'' as''' as'''' vs.
     sublist (replace_projected (as'',as',as,vs)) as'''
     ==> sublist (replace_projected (as'''' ++ as'',as',as,vs)) (as'''' ++as''')``,
Induct_on `as`
THENL
[
  Cases_on `as'`
	   THEN SRW_TAC[][replace_projected_def, sublist_filter, sublist_cons, sublist_trans, sublist_refl]
	   THEN METIS_TAC[sublist_refl, sublist_append, GSYM APPEND_ASSOC]
	   ,
	     SRW_TAC[][]
		    THEN Cases_on `as'`
		    THEN SRW_TAC[][replace_projected_def, sublist_filter, sublist_cons, sublist_trans, sublist_def]
		    THEN FULL_SIMP_TAC(srw_ss())[replace_projected_def, sublist_filter, sublist_cons, sublist_trans, sublist_def]
		    THEN METIS_TAC[sublist_refl, sublist_append, GSYM APPEND_ASSOC]
]);

val two_pow_n_is_a_bound2_2 = store_thm("two_pow_n_is_a_bound2_2",
``!as as' vs s. sublist (replace_projected([], as', as, vs)) as``,
Induct_on `as`
THEN Cases_on `as'`
THEN SRW_TAC[][replace_projected_def, sublist_filter, sublist_cons, sublist_trans, sublist_def]
THEN MP_TAC(sublist_replace_proj_append
		|> Q.SPEC `as`
		|> Q.SPEC `t`
		|> Q.SPEC `[]`
		|> Q.SPEC `as`
		|> Q.SPEC `[h']`
		|> Q.SPEC `vs`)
THEN MP_TAC(sublist_replace_proj_append
		|> Q.SPEC `as`
		|> Q.SPEC `h::t`
		|> Q.SPEC `[]`
		|> Q.SPEC `as`
		|> Q.SPEC `[h']`
		|> Q.SPEC `vs`)
THEN SRW_TAC[][sublist_def]);

val replace_proj_append = store_thm("replace_proj_append",
``!as''' as'' as' as vs.
      replace_projected (as''' ++ as'', as', as, vs) =
            as''' ++ replace_projected (as'', as', as, vs)``,
Induct_on `as`
THENL
[
  Cases_on `as'`
	   THEN SRW_TAC[][replace_projected_def, sublist_filter, sublist_cons, sublist_trans, sublist_refl]
	   THEN METIS_TAC[sublist_refl, sublist_append, GSYM APPEND_ASSOC]
	   ,
	     SRW_TAC[][]
		    THEN Cases_on `as'`
		    THEN SRW_TAC[][replace_projected_def, sublist_filter, sublist_cons, sublist_trans, sublist_def]
]);

val two_pow_n_is_a_bound2_3 = store_thm("two_pow_n_is_a_bound2_3",
``!PROB vs as' as. no_effectless_act(as) /\  as IN (valid_plans PROB)
					        ==> no_effectless_act(replace_projected([], as', as, vs))``,
NTAC 2 STRIP_TAC
THEN Induct_on `as`
THEN Cases_on `as'`
THEN SRW_TAC[SatisfySimps.SATISFY_ss][no_effectless_act_def, replace_projected_def, rem_effectless_works_10]
THEN FULL_SIMP_TAC(bool_ss)[]
THEN MP_TAC(replace_proj_append
		    |> Q.SPEC `[h']`
		        |> Q.SPEC `[]`
			    |> Q.SPEC `t`
			        |> Q.SPEC `as`
				    |> Q.SPEC `vs`) 
THEN SRW_TAC[][]
THEN SRW_TAC[][no_effectless_act_def]
THEN MP_TAC(vset_nempty_efff_not_disj_eff_vs
		  |> Q.SPEC `PROB`
		    |> Q.SPEC `h'`
		      |> Q.SPEC `vs`)
THEN SRW_TAC[][]
THEN MP_TAC(REWRITE_RULE[FDOM_DRESTRICT, GSYM DISJOINT_DEF] (not_empty_eff_in_as_proj
								   |> INST_TYPE [alpha |-> ``:'a``] 
								     |> INST_TYPE [beta |-> ``:bool``]
								       |> INST_TYPE [gamma |-> ``:bool``]
								         |> Q.SPEC `as`
									   |> Q.SPEC `h'`
									     |> Q.SPEC `vs`))
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]
THEN FULL_SIMP_TAC(bool_ss)[sublist_cons_2]
THEN FULL_SIMP_TAC(srw_ss())[sublist_def]
THEN SRW_TAC[][]
THEN MP_TAC(replace_proj_append
		    |> Q.SPEC `[h']`
		        |> Q.SPEC `[]`
			    |> Q.SPEC `h::t`
			        |> Q.SPEC `as`
				    |> Q.SPEC `vs`) 
THEN SRW_TAC[][]
THEN SRW_TAC[][no_effectless_act_def]
THEN METIS_TAC[valid_plan_valid_tail]);

val two_pow_n_is_a_bound2_4 = store_thm("two_pow_n_is_a_bound2_4",
``!PROB vs as as'. (as) IN (valid_plans PROB) /\ child_parent_rel(PROB, vs) /\ no_effectless_act(as)
									             ==> ((replace_projected([], as', as, vs))) IN (valid_plans PROB)``,
NTAC 3 STRIP_TAC
THEN Induct_on `as`
THEN SRW_TAC[][]
THEN Cases_on `as'`
THEN SRW_TAC[SatisfySimps.SATISFY_ss][no_effectless_act_def, replace_projected_def, rem_effectless_works_10]
THEN SRW_TAC[SatisfySimps.SATISFY_ss][set_subset_filter_subset]
THEN MP_TAC(replace_proj_append
		    |> Q.SPEC `[h]`
		        |> Q.SPEC `[]`
			    |> Q.SPEC `t`
			        |> Q.SPEC `as`
				    |> Q.SPEC `vs`) 
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[no_effectless_act_def]
THEN MP_TAC(vset_nempty_efff_not_disj_eff_vs
		  |> Q.SPEC `PROB`
		    |> Q.SPEC `h`
		      |> Q.SPEC `vs`)
THEN SRW_TAC[][]
THEN MP_TAC(REWRITE_RULE[FDOM_DRESTRICT, GSYM DISJOINT_DEF] (not_empty_eff_in_as_proj
								   |> INST_TYPE [alpha |-> ``:'a``] 
								     |> INST_TYPE [beta |-> ``:bool``]
								       |> INST_TYPE [gamma |-> ``:bool``]
								         |> Q.SPEC `as`
									   |> Q.SPEC `h`
									     |> Q.SPEC `vs`))
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]
THEN FULL_SIMP_TAC(bool_ss)[sublist_cons_2]
THEN FULL_SIMP_TAC(srw_ss())[sublist_def]
THEN MP_TAC(replace_proj_append
		    |> Q.SPEC `[h]`
		        |> Q.SPEC `[]`
			    |> Q.SPEC `h'::t`
			        |> Q.SPEC `as`
				    |> Q.SPEC `vs`) 
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[no_effectless_act_def]
THEN MP_TAC(child_parent_lemma_xx
		  |> Q.SPEC `PROB`
		    |> Q.SPEC `h`
		      |> Q.SPEC `vs`)
THEN SRW_TAC[][]
THEN MP_TAC (REWRITE_RULE[FDOM_DRESTRICT, GSYM DISJOINT_DEF] (empty_eff_not_in_as_proj
								    |> INST_TYPE [alpha |-> ``:'a``] 
								      |> INST_TYPE [beta |-> ``:bool``]
								        |> INST_TYPE [gamma |-> ``:bool``]
									  |> Q.SPEC `as`
									    |> Q.SPEC `h`
									      |> Q.SPEC `vs`))
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]
THEN FULL_SIMP_TAC(bool_ss)[sublist_def]
THEN METIS_TAC[valid_head_and_tail_valid_plan, valid_plan_valid_tail, valid_plan_valid_head, valid_filter_valid_as]);

val no_effectless_works_with_proj = store_thm("no_effectless_works_with_proj",
``!PROB as vs. no_effectless_act(as) /\ child_parent_rel(PROB, vs) /\ as IN (valid_plans PROB)
               ==> ((as_proj (FILTER (λa. ¬varset_action (a,vs)) as,vs)) = []) ``,
Induct_on `as`
THEN SRW_TAC[][as_proj_def, no_effectless_act_def]
THENL[
 REWRITE_TAC[FDOM_DRESTRICT]
 THEN REWRITE_TAC[GSYM DISJOINT_DEF]
 THEN MP_TAC(child_parent_lemma_xx
   |> Q.SPEC `PROB`
   |> Q.SPEC `h`
   |> Q.SPEC `vs`)
       THEN SRW_TAC[][]
 THEN METIS_TAC[valid_plan_valid_head, valid_plan_valid_tail]
	,
 METIS_TAC[valid_plan_valid_head, valid_plan_valid_tail]
]);

val sublist_imp_replace_proj_works = store_thm("sublist_imp_replace_proj_works",
``!PROB as as' vs. no_effectless_act(as) /\ child_parent_rel(PROB, vs)
                   /\ as IN (valid_plans PROB) /\ sublist as' (as_proj(as, vs))
                   ==> (as' = (as_proj(replace_projected([], as', as, vs), vs)))``,
Induct_on `as`
THEN SRW_TAC[][as_proj_def, sublist_def, replace_projected_def]
THEN Cases_on `as'`
THEN SRW_TAC[][as_proj_def, sublist_def, replace_projected_def]
THEN FULL_SIMP_TAC(bool_ss)[no_effectless_act_def]
THENL[
      FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT]
      THEN FULL_SIMP_TAC(srw_ss())[GSYM DISJOINT_DEF]
      THEN METIS_TAC[child_parent_lemma_xx, valid_plan_valid_head, valid_plan_valid_tail]
      ,    
      METIS_TAC[no_effectless_works_with_proj, valid_plan_valid_head, valid_plan_valid_tail]
      ,
      REWRITE_TAC[REWRITE_RULE[APPEND_NIL](replace_proj_append
                                          |> Q.SPEC `[h]`
                                          |> Q.SPEC `[]`
                                          |> Q.SPEC `t`
                                          |> Q.SPEC `as`
                                          |> Q.SPEC `vs`)]
      THEN SRW_TAC[][]
      THEN MP_TAC(not_empty_eff_in_as_proj 
		  |> Q.ISPEC `(replace_projected ([],t,as,vs))`
                  |> Q.SPEC `h`
                  |> Q.SPEC `vs`)
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(bool_ss)[sublist_cons_2, no_effectless_act_def]
      THEN METIS_TAC[valid_plan_valid_head, valid_plan_valid_tail]
      ,
      FULL_SIMP_TAC(bool_ss)[sublist_def, no_effectless_act_def]
      THEN METIS_TAC[valid_plan_valid_head, valid_plan_valid_tail]
      ,
      FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT]
      THEN FULL_SIMP_TAC(srw_ss())[GSYM DISJOINT_DEF]
      THEN METIS_TAC[child_parent_lemma_xx, valid_plan_valid_head, valid_plan_valid_tail] 
      ,
      METIS_TAC[no_effectless_works_with_proj, valid_plan_valid_head, valid_plan_valid_tail] 
      ,
      METIS_TAC[no_effectless_works_with_proj, valid_plan_valid_head, valid_plan_valid_tail]
      ,
      FULL_SIMP_TAC(bool_ss)[no_effectless_act_def]
      THEN FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT]
      THEN FULL_SIMP_TAC(srw_ss())[GSYM DISJOINT_DEF]
      THEN METIS_TAC[vset_nempty_efff_not_disj_eff_vs, valid_plan_valid_head, valid_plan_valid_tail] 
      ,
      FULL_SIMP_TAC(bool_ss)[no_effectless_act_def]
      THEN FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT]
      THEN FULL_SIMP_TAC(srw_ss())[GSYM DISJOINT_DEF]
      THEN METIS_TAC[vset_nempty_efff_not_disj_eff_vs, valid_plan_valid_head, valid_plan_valid_tail]
      ,
      REWRITE_TAC[REWRITE_RULE[APPEND_NIL](replace_proj_append
                  |> Q.SPEC `[h]`
                  |> Q.SPEC `[]`
                  |> Q.SPEC `h'::t`
                  |> Q.SPEC `as`
                  |> Q.SPEC `vs`)]
      THEN SRW_TAC[][]
      THEN MP_TAC(empty_eff_not_in_as_proj
                   |> Q.ISPEC `(replace_projected ([],h'::t,as,vs))`
                   |> Q.SPEC `h`
                   |> Q.SPEC `vs`)
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(bool_ss)[sublist_cons_2, no_effectless_act_def]
      THEN METIS_TAC[valid_plan_valid_head, valid_plan_valid_tail]
]);

val len_replace_proj_works = store_thm("len_replace_proj_works",
``!PROB s as vs as'. child_parent_rel(PROB, vs) /\ (s IN (valid_states PROB))
                     /\ as IN (valid_plans PROB) /\ sublist as' (as_proj(as, vs)) /\ no_effectless_act(as)
		     ==>
                      (LENGTH as' = 
                           LENGTH (FILTER (\a. varset_action(a, vs))
                                          (replace_projected([], as', as, vs))))``,
SRW_TAC[][]
THEN MP_TAC(sublist_imp_replace_proj_works
		   |> Q.SPEC `PROB`
		      |> Q.SPEC `as`
		         |> Q.SPEC `as'`
			    |> Q.SPEC `vs`)
THEN SRW_TAC[][]
THEN MP_TAC(two_pow_n_is_a_bound2_4
		|> Q.SPEC `PROB`
		|> Q.SPEC `vs`
		|> Q.SPEC `as`
		|> Q.SPEC `as'`)
THEN SRW_TAC[][]
THEN MP_TAC(as_proj_child_works
		  |> Q.SPEC `PROB`
		    |> Q.SPEC `(replace_projected ([],as',as,vs))`
		      |> Q.SPEC `vs`)
THEN SRW_TAC[][]
THEN MP_TAC(two_pow_n_is_a_bound2_3
		|> Q.SPEC `PROB`
		|> Q.SPEC `vs`
		|> Q.SPEC `as'`
		|> Q.SPEC `as`)
THEN SRW_TAC[][]
THEN MP_TAC(graph_plan_lemma_3
		|> Q.SPEC `vs`
		|> Q.SPEC `(replace_projected ([],as',as,vs))`)
THEN SRW_TAC[][]
THEN METIS_TAC[]);

val submap_dres_eq_imp_pres_submap = store_thm("submap_dres_eq_imp_pres_submap",
``!PROB vs a s s'. child_parent_rel(PROB, vs) /\ a IN PROB /\ ~(FDOM (SND a) = EMPTY)
              /\ (s IN (valid_states PROB)) /\ (s' IN (valid_states PROB))
              /\ (DRESTRICT s ((prob_dom PROB) DIFF vs) = DRESTRICT s' ((prob_dom PROB) DIFF vs))
              /\ ~varset_action(a, vs) /\ FST a SUBMAP s
              ==>
                  FST a SUBMAP s'``,
SRW_TAC[][]
THEN MP_TAC(vset_eq_vset_diff |> Q.SPEC `PROB` |> Q.SPEC `a` |> Q.SPEC `vs`)
THEN SRW_TAC[][]
THEN MP_TAC(child_parent_dom_pre_subset_prob_dom|> Q.SPEC `PROB` |> Q.SPEC `vs` |> Q.SPEC `a`)
THEN SRW_TAC[][]
THEN METIS_TAC[exec_drest_5, EQ_SUBMAP_IMP_DRESTRICT_SUBMAP]);

val sublist_proj_imp_replace_proj_sat_precond = store_thm("sublist_proj_imp_replace_proj_sat_precond",
``!PROB vs sa sc as as_c. sublist as_c (as_proj(as, vs)) /\ (sa IN (valid_states PROB))
           /\ (sc IN (valid_states PROB)) /\ (child_parent_rel(PROB, vs))
           /\ as IN (valid_plans PROB)  /\ no_effectless_act(as) 
           /\ (DRESTRICT sa ((prob_dom PROB) DIFF vs) = DRESTRICT sc ((prob_dom PROB) DIFF vs))
           /\ sat_precond_as(sa, as)
           /\ sat_precond_as(sc, as_c)
           ==>
               (sat_precond_as(sc, (replace_projected([], as_c, as, vs))))``,
NTAC 2 STRIP_TAC
THEN Induct_on `as`
THEN Cases_on `as_c`
THEN(SRW_TAC[][replace_projected_def, sat_precond_as_def, no_effectless_act_def])
THENL
[
   MP_TAC(submap_dres_eq_imp_pres_submap
                      |> INST_TYPE[gamma |-> ``:'b``]
                      |> Q.SPEC `PROB`
                      |> Q.SPEC `vs`
                      |> Q.SPEC `h`
                      |> Q.SPEC `sa`
                      |> Q.SPEC `sc`)
   THEN SRW_TAC[][]
   THEN METIS_TAC[valid_plan_valid_head, valid_plan_valid_tail]
   ,
   ASSUME_TAC(MATCH_MP AND2_THM (MATCH_MP AND1_THM (REWRITE_RULE[CONJ_ASSOC] (replace_projected_def|>INST_TYPE [gamma |-> ``:'b``]))) |> Q.SPEC `vs` |> Q.SPEC `[]` |> Q.SPEC `as`)
   THEN FIRST_X_ASSUM (Q.SPECL_THEN [`state_succ sa h`, `state_succ sc h`, `[]`] MP_TAC)
   THEN SRW_TAC[][]
   THEN FIRST_X_ASSUM MATCH_MP_TAC
   THEN SRW_TAC[][]
   THENL
   [
      METIS_TAC[valid_action_valid_succ, valid_plan_valid_head]
      ,
      METIS_TAC[valid_action_valid_succ, valid_plan_valid_head]
      ,
      METIS_TAC[valid_plan_valid_tail]
      ,
      MATCH_MP_TAC(proj_eq_proj_exec_eq)
      THEN SRW_TAC[][]
      THEN MATCH_MP_TAC(submap_dres_eq_imp_pres_submap)
      THEN Q.EXISTS_TAC `PROB`
      THEN Q.EXISTS_TAC `vs`
      THEN Q.EXISTS_TAC `sa`
      THEN SRW_TAC[][]
      THEN METIS_TAC[valid_plan_valid_head]
      ,
      SRW_TAC[][sat_precond_as_def]
   ]
   ,
   ASSUME_TAC(MATCH_MP AND2_THM (MATCH_MP AND1_THM (REWRITE_RULE[CONJ_ASSOC] (replace_projected_def|>INST_TYPE[gamma |-> ``:'b``]))) |> Q.SPECL [`vs`, `[]`, `as`])
   THEN FIRST_X_ASSUM (Q.SPECL_THEN [`state_succ sa h`, `sc`, `[]`] MP_TAC)
   THEN SRW_TAC[][]
   THEN FIRST_X_ASSUM MATCH_MP_TAC
   THEN SRW_TAC[][]
   THENL
   [
      METIS_TAC[valid_action_valid_succ, valid_plan_valid_head]
      ,
      METIS_TAC[valid_plan_valid_tail]
      ,
      METIS_TAC[disj_imp_eq_proj_exec, vset_disj_dom_eff_diff]
      ,
      SRW_TAC[][sat_precond_as_def]
   ]
   ,
   MP_TAC(replace_proj_append
                       |> INST_TYPE[gamma |-> ``:'b``]
                       |> Q.SPEC `[h']`
                       |> Q.SPEC `[]`
                       |> Q.SPEC `t`
                       |> Q.SPEC `as`
                       |> Q.SPEC `vs`)
   THEN SRW_TAC[][sat_precond_as_def]
   THENL
   [
      MATCH_MP_TAC(drestrict_submap_and_comp_imp_submap)
      THEN Q.EXISTS_TAC `vs`
      THEN SRW_TAC[][]
      THEN1 (FULL_SIMP_TAC(srw_ss())[valid_states_def] THEN METIS_TAC[FDOM_pre_subset_prob_dom_pair, valid_plan_valid_head])
      THEN FULL_SIMP_TAC(srw_ss())[action_proj_def, valid_states_def]
      THEN MATCH_MP_TAC(EQ_SUBMAP_IMP_DRESTRICT_SUBMAP)
      THEN Q.EXISTS_TAC `sa`
      THEN SRW_TAC[][]
      THEN METIS_TAC[]
      ,
      FIRST_X_ASSUM MATCH_MP_TAC
      THEN SRW_TAC[][]
      THEN Q.EXISTS_TAC `state_succ sa h'`
      THEN SRW_TAC[][]
      THENL
      [
         FULL_SIMP_TAC(bool_ss)[varset_action_def, FDOM_DRESTRICT, as_proj_def]
         THEN MP_TAC(not_empty_eff_in_as_proj |> Q.SPEC `as` |> Q.SPEC `h'` |> Q.SPEC `vs`)
         THEN SRW_TAC[][FDOM_DRESTRICT]
         THEN MP_TAC((SUBSET_ABS |> Q.SPEC `FDOM (SND h')` |> Q.SPEC `vs`) |> INST_TYPE [gamma |-> ``:'a |-> 'b``, alpha |-> ``:'a``, beta |-> ``:'b``])
         THEN SRW_TAC[][]
         THEN METIS_TAC[sublist_cons_2]
         ,
         METIS_TAC[valid_plan_valid_head, valid_action_valid_succ]
         ,
         METIS_TAC[valid_plan_valid_head, valid_action_valid_succ]
         ,
         METIS_TAC[valid_plan_valid_tail]
         ,
         METIS_TAC[vset_disj_dom_eff_diff, disj_imp_eq_proj_exec]
         ,
         `DRESTRICT (FST h') vs ⊑ sc` by FULL_SIMP_TAC(srw_ss())[action_proj_def]
         THEN FULL_SIMP_TAC(srw_ss())[valid_states_def]
         THEN `(FST h') SUBMAP sc`
                by (MATCH_MP_TAC drestrict_submap_and_comp_imp_submap
                    THEN Q.EXISTS_TAC `vs`
                    THEN SRW_TAC[][]
                    THEN1(METIS_TAC[valid_plan_valid_head, FDOM_pre_subset_prob_dom_pair])
                    THEN METIS_TAC[EQ_SUBMAP_IMP_DRESTRICT_SUBMAP])        
         THEN `state_succ sc (action_proj (h',vs)) = state_succ sc h'`
              by(MATCH_MP_TAC state_succ_proj_eq_state_succ
                 THEN SRW_TAC[][]
                 THEN METIS_TAC[state_succ_proj_eq_state_succ, valid_plan_valid_head, FDOM_eff_subset_prob_dom_pair])
         THEN METIS_TAC[]
      ]
   ]
   ,
   FIRST_X_ASSUM MATCH_MP_TAC
   THEN Q.EXISTS_TAC `state_succ sa h'`
   THEN SRW_TAC[][]
   THENL
   [
      FULL_SIMP_TAC(bool_ss)[varset_action_def, FDOM_DRESTRICT, as_proj_def]
      THEN MP_TAC(not_empty_eff_in_as_proj |> Q.SPEC `as` |> Q.SPEC `h'` |> Q.SPEC `vs`)
      THEN SRW_TAC[][FDOM_DRESTRICT]
      THEN MP_TAC((SUBSET_ABS |> Q.SPEC `FDOM (SND h')` |> Q.SPEC `vs`) |> INST_TYPE [gamma |-> ``:'a |-> 'b``, alpha |-> ``:'a``, beta |-> ``:'b``])
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[sublist_def]
      THEN METIS_TAC[]
      ,
      METIS_TAC[valid_plan_valid_head, valid_action_valid_succ]
      ,
      METIS_TAC[valid_plan_valid_tail]
      ,
      METIS_TAC[vset_disj_dom_eff_diff, disj_imp_eq_proj_exec]
      ,
      SRW_TAC[][sat_precond_as_def]
   ]
   ,
   MP_TAC(replace_proj_append
                       |> INST_TYPE[gamma |-> ``:'b``]
                       |> Q.SPEC `[h']`
                       |> Q.SPEC `[]`
                       |> Q.SPEC `h::t`
                       |> Q.SPEC `as`
                       |> Q.SPEC `vs`)
   THEN SRW_TAC[][sat_precond_as_def]
   THEN1 METIS_TAC[submap_dres_eq_imp_pres_submap, valid_plan_valid_head, FDOM_pre_subset_prob_dom_pair]
   THEN FIRST_X_ASSUM MATCH_MP_TAC
   THEN Q.EXISTS_TAC `state_succ sa h'`
   THEN SRW_TAC[][]
   THENL
   [
      FULL_SIMP_TAC(bool_ss)[as_proj_def, FDOM_DRESTRICT]
      THEN MP_TAC(REWRITE_RULE[DISJOINT_DEF] child_parent_lemma_xx |> INST_TYPE[gamma |-> ``:'b``])
      THEN METIS_TAC[valid_plan_valid_head]
      ,
      METIS_TAC[valid_plan_valid_head, valid_action_valid_succ]
      ,
      METIS_TAC[valid_plan_valid_head, valid_action_valid_succ]
      ,
      METIS_TAC[valid_plan_valid_tail]
      ,
      MATCH_MP_TAC(proj_eq_proj_exec_eq)
      THEN SRW_TAC[][]
      THEN MATCH_MP_TAC(submap_dres_eq_imp_pres_submap)
      THEN Q.EXISTS_TAC `PROB`
      THEN Q.EXISTS_TAC `vs`
      THEN Q.EXISTS_TAC `sa`
      THEN SRW_TAC[][]
      THEN METIS_TAC[valid_plan_valid_head]
      ,
      `DRESTRICT (state_succ sc h') vs = DRESTRICT sc vs` by METIS_TAC[child_parent_lemma_xx, disj_imp_eq_proj_exec, valid_plan_valid_head]
      THEN METIS_TAC[sat_precond_drest_as_proj, sat_precond_as_proj, sublist_as_proj_eq_as, sat_precond_as_def]
   ]
]);

val replace_proj_exec_works = store_thm("replace_proj_exec_works",
``!PROB s as vs as'. child_parent_rel(PROB, vs) /\ (s IN (valid_states PROB))
                     /\ as IN (valid_plans PROB) /\ sat_precond_as(s, as) /\ sublist as' (as_proj(as, vs))
                 /\ no_effectless_act(as) /\ sat_precond_as(s, as')
                 ==>
                 (DRESTRICT (exec_plan(s, (replace_projected([], as', as, vs)))) vs
                          = exec_plan(DRESTRICT s vs , as'))``,
SRW_TAC[][]
THEN MP_TAC(sublist_imp_replace_proj_works
               |> INST_TYPE [gamma |-> ``:'b``]
               |> Q.SPEC `PROB`
               |> Q.SPEC `as`
               |> Q.SPEC `as'`
               |> Q.SPEC `vs`)
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN MP_TAC(sublist_proj_imp_replace_proj_sat_precond
               |> Q.SPEC `PROB`
               |> Q.SPEC `vs`
               |> Q.SPEC `s`
               |> Q.SPEC `s`
               |> Q.SPEC `as`
               |> Q.SPEC `as'`)
THEN SRW_TAC[][]
THEN MP_TAC(graph_plan_lemma_1
                |> Q.SPEC `s`
                |> Q.SPEC `vs`
                |> Q.ISPEC `replace_projected
                              (([] :((α |-> β) # (α |-> β)) list),
                               (as' :((α |-> β) # (α |-> β)) list),
                               (as :((α |-> β) # (α |-> β)) list),(vs :α -> bool))`)
THEN SRW_TAC[][]
THEN METIS_TAC[]);

val replace_proj_leaves_nvarset = store_thm("replace_proj_leaves_nvarset",
``!PROB vs as_c as. child_parent_rel(PROB, vs) /\ as IN (valid_plans PROB)
                    ==>
                        (FILTER (\a. ~varset_action(a, vs)) as
                                  =   FILTER (\a. ~varset_action(a, vs)) (replace_projected([], as_c, as, vs)))``,
Induct_on `as`
THEN SRW_TAC[][as_proj_def, sublist_def, replace_projected_def]
THEN Cases_on `as_c`
THEN SRW_TAC[][as_proj_def, sublist_def, replace_projected_def]
THEN FULL_SIMP_TAC(bool_ss)[no_effectless_act_def]
THEN SRW_TAC[][FILTER_IDEM]
THENL
[   
   MP_TAC(replace_proj_append |> Q.SPEC `[h]` |> Q.SPEC `[]` |> Q.SPEC `h'::t` |> Q.SPEC `as` |> Q.SPEC `vs`)
   THEN SRW_TAC[][]
   THEN FIRST_X_ASSUM MATCH_MP_TAC
   THEN Q.EXISTS_TAC `PROB`
   THEN SRW_TAC[][]
   THEN FULL_SIMP_TAC(bool_ss)[FDOM_DRESTRICT]
   THEN FULL_SIMP_TAC(bool_ss)[GSYM DISJOINT_DEF]
   THEN METIS_TAC[child_parent_lemma_xx, valid_plan_valid_tail]
   ,
   MP_TAC(replace_proj_append |> Q.SPEC `[h]` |> Q.SPEC `[]` |> Q.SPEC `t` |> Q.SPEC `as` |> Q.SPEC `vs`)
   THEN SRW_TAC[][]
   THEN METIS_TAC[sublist_cons_2, valid_plan_valid_tail]
   ,
   FIRST_X_ASSUM MATCH_MP_TAC
   THEN Q.EXISTS_TAC `PROB`
   THEN SRW_TAC[][]
   THEN FULL_SIMP_TAC(bool_ss)[sublist_def]
   THEN METIS_TAC[valid_plan_valid_tail]
]);

val filter_varset_diff_eq_filter_nvarset = store_thm("filter_varset_diff_eq_filter_nvarset",
``!PROB vs as. child_parent_rel(PROB, vs) /\ no_effectless_act(as) /\ as IN (valid_plans PROB)
                    ==>
                        (FILTER (\a. varset_action(a, (prob_dom PROB) DIFF vs)) as
                                  =   FILTER (\a. ~varset_action(a, vs)) as)``,
NTAC 2 STRIP_TAC
THEN Induct_on `as`
THEN SRW_TAC[][]
THEN1(FIRST_X_ASSUM MATCH_MP_TAC
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(bool_ss)[no_effectless_act_def]
      THEN METIS_TAC[valid_plan_valid_tail])
THEN FULL_SIMP_TAC(bool_ss)[no_effectless_act_def]
THEN METIS_TAC[vset_eq_vset_diff, valid_plan_valid_tail, valid_plan_valid_head]);

val filter_vset_eq_filter_vset_replace_proj = store_thm("filter_vset_eq_filter_vset_replace_proj",
``!PROB vs as_c as. child_parent_rel(PROB, vs) /\ no_effectless_act(as) /\ as IN (valid_plans PROB)
                    ==>
                        (FILTER (\a. varset_action(a, (prob_dom PROB) DIFF vs)) as
                                  =  FILTER (\a. varset_action(a, (prob_dom PROB) DIFF vs))
                                            (replace_projected([], as_c, as, vs)))``,
SRW_TAC[][]
THEN MP_TAC(replace_proj_leaves_nvarset
                   |> Q.SPEC `PROB`
                   |> Q.SPEC `vs`
                   |> Q.SPEC `as_c`
                   |> Q.SPEC `as`)
THEN SRW_TAC[][]
THEN MP_TAC(two_pow_n_is_a_bound2_4
           |> Q.SPEC `PROB`
           |> Q.SPEC `vs`
           |> Q.SPEC `as`
           |> Q.SPEC `as_c`)
THEN SRW_TAC[][]
THEN MP_TAC(filter_varset_diff_eq_filter_nvarset |> Q.SPEC `PROB` |> Q.SPEC `vs` |> Q.SPEC `as`)
THEN SRW_TAC[][]
THEN MP_TAC(two_pow_n_is_a_bound2_3
           |> Q.SPEC `PROB`
           |> Q.SPEC `vs`
           |> Q.SPEC `as_c`
           |> Q.SPEC `as`)
THEN SRW_TAC[][]
THEN MP_TAC(two_pow_n_is_a_bound2_4 
            |> Q.SPEC `PROB`
            |> Q.SPEC `vs`
            |> Q.SPEC `as`
            |> Q.SPEC `as_c`)
THEN SRW_TAC[][]
THEN MP_TAC(filter_varset_diff_eq_filter_nvarset |> Q.SPEC `PROB` |> Q.SPEC `vs` |> Q.SPEC `(replace_projected ([],as_c,as,vs))`)
THEN SRW_TAC[][]);

val as_proj_eq_fitler_vset_diff = store_thm("as_proj_eq_fitler_vset_diff",
``!PROB vs as. child_parent_rel(PROB, vs) /\ as IN (valid_plans PROB) /\ no_effectless_act(as)
                    ==>
                      (as_proj(as, ((prob_dom PROB) DIFF vs))
                                = FILTER (\a. varset_action(a, (prob_dom PROB) DIFF vs)) as)``,
SRW_TAC[][]
THEN MP_TAC(proj_eq_filter_vset_diff
             |> Q.SPEC `PROB`
             |> Q.SPEC `vs`
             |> Q.SPEC `as`)
THEN SRW_TAC[][]
THEN `(as_proj_parent (as,vs) = as_proj (as, (prob_dom PROB) DIFF vs))` by METIS_TAC[(as_proj_parent_works 
                         |> Q.SPEC `PROB`
                         |> Q.SPEC `as`
                         |> Q.SPEC `vs`)]
THEN FULL_SIMP_TAC(bool_ss)[GSYM FILTER_FILTER, rem_effectless_works_7]
THEN METIS_TAC[((GSYM FILTER_EQ_ID ) |> Q.ISPEC `(λa. FDOM (SND a) <> EMPTY)` |> Q.SPEC `as` |> INST_TYPE [alpha |-> ``:'a|->'b``, beta |-> ``:'a``])]);

val sat_precond_filter_vset_diff = store_thm("sat_precond_filter_vset_diff",
``!PROB vs s as. child_parent_rel(PROB, vs) /\ as IN (valid_plans PROB) /\ (s IN (valid_states PROB))
                 /\ sat_precond_as(s, as) /\ no_effectless_act(as)
                    ==>
                      sat_precond_as(s, FILTER (\a. varset_action(a, (prob_dom PROB) DIFF vs)) as)``,
SRW_TAC[][]
THEN METIS_TAC[child_parent_neq_len_imp_neq, proj_eq_filter_vset_diff, no_effectless_act_works, GSYM FILTER_FILTER]);

val replace_proj_exec_works_diff = store_thm("replace_proj_exec_works_diff",
``!PROB vs s as_c as. child_parent_rel(PROB, vs) /\ (s IN (valid_states PROB)) /\ as IN (valid_plans PROB) /\ sublist as_c (as_proj(as, vs)) /\ sat_precond_as(s, as)
                      /\ sat_precond_as(s, as_c) /\ no_effectless_act(as)
                      ==>
                     ((DRESTRICT (exec_plan(s, as)) ((prob_dom PROB) DIFF vs)  
                          = DRESTRICT (exec_plan(s, (replace_projected([], as_c, as, vs)))) 
                                      ((prob_dom PROB) DIFF vs))
                      /\ (FILTER (\a. varset_action (a,(prob_dom PROB) DIFF vs)) as =
                                    FILTER (\a. varset_action (a,(prob_dom PROB) DIFF vs))
                                                 (replace_projected ([],as_c,as,vs))))``,
SRW_TAC[][]
THEN MP_TAC(GSYM (filter_vset_eq_filter_vset_replace_proj
                  |> INST_TYPE[gamma |-> ``:'b``]
                  |> Q.SPEC `PROB`
                  |> Q.SPEC `vs`
                  |> Q.SPEC `as_c`
                  |> Q.SPEC `as`))
THEN SRW_TAC[][]
THEN MP_TAC(GSYM (as_proj_eq_fitler_vset_diff
                  |> INST_TYPE[gamma |-> ``:'b``]
                  |> Q.SPEC `PROB`
                  |> Q.SPEC `vs`
                  |> Q.SPEC `as`))
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[]
THEN MP_TAC(GSYM (sat_precond_filter_vset_diff
                  |> Q.SPEC `PROB`
                  |> Q.SPEC `vs`
                  |> Q.SPEC `s`
                  |> Q.SPEC `as`))
THEN SRW_TAC[][]
THEN MP_TAC(drest_exec_as_proj_eq_drest_exec
           |> Q.SPEC `s`
           |> Q.SPEC `as`
           |> Q.SPEC `((prob_dom (PROB:((α |-> β) # (α |-> β)) set)) DIFF vs)`)
THEN SRW_TAC[][]
THEN MP_TAC(sublist_proj_imp_replace_proj_sat_precond
                                |> Q.SPEC `PROB`
                                |> Q.SPEC `vs`
                                |> Q.SPEC `s`
                                |> Q.SPEC `s`
                                |> Q.SPEC `as`
                                |> Q.SPEC `as_c`)
THEN SRW_TAC[][]
THEN MP_TAC(drest_exec_as_proj_eq_drest_exec
                |> Q.SPEC `s`
                |> Q.ISPEC `replace_projected ([]:((α |-> β) # (α |-> β)) list,as_c,as,vs)`
                |> Q.SPEC `((prob_dom (PROB:((α |-> β) # (α |-> β)) set)) DIFF vs)`)
THEN SRW_TAC[][]
THEN MP_TAC(two_pow_n_is_a_bound2_4
           |> INST_TYPE[gamma |-> ``:'b``]
           |> Q.SPEC `PROB`
           |> Q.SPEC `vs`
           |> Q.SPEC `as`
           |> Q.SPEC `as_c`)
THEN SRW_TAC[][]
THEN MP_TAC(two_pow_n_is_a_bound2_3 
            |> INST_TYPE[gamma |-> ``:'b``]
            |> Q.SPEC `PROB`
            |> Q.SPEC `vs`
            |> Q.SPEC `as_c`
            |> Q.SPEC `as`)
THEN SRW_TAC[][]
THEN MP_TAC(GSYM(as_proj_eq_fitler_vset_diff
                  |> INST_TYPE[gamma |-> ``:'b``]
                  |> Q.SPEC `PROB`
                  |> Q.SPEC `vs`
                  |> Q.ISPEC `replace_projected ([]:((α |-> β) # (α |-> β)) list,as_c,as,vs)`))
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[]);

val bound_vset_drest_eff_eq' = store_thm("bound_vset_drest_eff_eq'",
``!(PROB:'a problem) as vs s.
      FINITE PROB /\ (s IN (valid_states PROB)) /\ as IN (valid_plans PROB)
      /\ child_parent_rel(PROB, vs) /\ sat_precond_as(s, as)
      /\ no_effectless_act(as)
      ==>  ?as'. (exec_plan(s, as') = exec_plan(s, as))
                 /\ (LENGTH (FILTER (\a. varset_action(a, vs)) as') <=
                                 problem_plan_bound(prob_proj(PROB, vs))
                 /\ ((FILTER (\a. varset_action(a, (prob_dom PROB) DIFF vs)) as')
                                = (FILTER (\a. varset_action(a, (prob_dom PROB) DIFF vs)) as)))
                 /\ (sublist as' as) /\ no_effectless_act(as')
                 /\ as' IN (valid_plans PROB) /\ sat_precond_as(s, as')``,
SRW_TAC[][]
THEN `rem_effectless_act (rem_condless_act (s,[],as)) IN valid_plans PROB`
       by METIS_TAC[rem_condless_valid_10, rem_effectless_works_4']
THEN MP_TAC (problem_plan_bound_works_proj
		 |> Q.SPEC `PROB`
		 |> Q.SPEC `s`
		 |> Q.SPEC `as`
		 |> Q.SPEC `vs`)
THEN SRW_TAC[][rem_condless_valid_4, rem_effectless_works_4, SUBSET_TRANS, rem_condless_valid_2, rem_effectless_works_2]
THEN Q.EXISTS_TAC `(replace_projected ([],as',as, vs))`
THEN SRW_TAC[][]
THENL
[
  ASSUME_TAC((MATCH_MP (rem_effectless_works_2|> Q.SPEC `rem_condless_act(s, [], as)` |> Q.SPEC `s`) (rem_condless_valid_2 |> Q.SPEC `(as)` |> Q.SPEC `s`))
                   |> INST_TYPE[beta |-> ``:bool``])
  THEN MP_TAC(replace_proj_exec_works
                   |> INST_TYPE[beta |-> ``:bool``]
                   |> Q.SPEC `PROB`
                   |> Q.SPEC `s`
                   |> Q.SPEC `as`
                   |> Q.SPEC `vs`
                   |> Q.SPEC `as'`)
  THEN SRW_TAC[][rem_condless_valid_4, rem_effectless_works_4, SUBSET_TRANS, rem_effectless_works_6]
  THEN MP_TAC(graph_plan_lemma_1 
                   |> INST_TYPE[beta |-> ``:bool``]
                   |> Q.SPEC `s` 
                   |> Q.SPEC `vs` 
                   |> Q.SPEC `as`)
  THEN SRW_TAC[][]
  THEN FULL_SIMP_TAC(bool_ss)[]
  THEN MP_TAC(replace_proj_exec_works_diff
                   |> INST_TYPE[beta |-> ``:bool``]
                   |> Q.SPEC `PROB`
                   |> Q.SPEC `vs`
                   |> Q.SPEC `s`
                   |> Q.SPEC `as'`
                   |> Q.SPEC `as`)
  THEN SRW_TAC[][rem_condless_valid_4, rem_effectless_works_4, SUBSET_TRANS, rem_effectless_works_6]
  THEN MP_TAC(valid_as_valid_exec
                   |> INST_TYPE[beta |-> ``:bool``]
                   |> Q.SPEC `as`
                   |> Q.SPEC `s`
                   |> Q.SPEC `PROB`)
  THEN SRW_TAC[][rem_condless_valid_4, rem_effectless_works_4, SUBSET_TRANS]
  THEN MP_TAC (two_pow_n_is_a_bound2_1'
                   |> Q.SPEC `as`
                   |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``]
                   |> Q.SPEC `as'`
                   |> Q.SPEC `[]`
                   |> Q.SPEC `vs`
                   |> Q.SPEC `PROB`)
  THEN SRW_TAC[][rem_condless_valid_4, rem_effectless_works_4, SUBSET_TRANS, empty_plan_is_valid]
  THEN MP_TAC (valid_as_valid_exec
                   |> INST_TYPE[beta |-> ``:bool``]
                   |> Q.SPEC `replace_projected ([],as', as, vs)`
                   |> Q.SPEC `s`
                   |> Q.SPEC `PROB`)
  THEN SRW_TAC[][rem_condless_valid_4, rem_effectless_works_4, SUBSET_TRANS]
  THEN FULL_SIMP_TAC(srw_ss())[valid_states_def]
  THEN MP_TAC(graph_plan_lemma_5
                   |> INST_TYPE[beta |-> ``:bool``]
                   |> Q.SPEC `(exec_plan (s:'a state, (replace_projected ([]:'a action list,as',as, vs))))`
                   |> Q.SPEC `(exec_plan (s, as))`
                   |> Q.SPEC `vs`)
  THEN SRW_TAC[][]
  ,
  MP_TAC(len_replace_proj_works
                   |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``, delta |-> ``:bool``]
                   |> Q.SPEC `PROB`
                   |> Q.SPEC `s`
                   |> Q.SPEC `as`
                   |> Q.SPEC `vs`
                   |> Q.SPEC `as'`)
  THEN SRW_TAC[][rem_condless_valid_4, rem_effectless_works_4, SUBSET_TRANS, rem_effectless_works_6]
  THEN FULL_SIMP_TAC(bool_ss)[]
  ,
  MP_TAC(filter_vset_eq_filter_vset_replace_proj
                   |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``]
                   |> Q.SPEC `PROB`
                   |> Q.SPEC `vs`
                   |> Q.SPEC `as'`
                   |> Q.SPEC `as`)
  THEN SRW_TAC[][rem_condless_valid_4, rem_effectless_works_4, SUBSET_TRANS, rem_effectless_works_6]
  ,
  METIS_TAC[rem_condless_valid_8, rem_effectless_works_9, two_pow_n_is_a_bound2_2, sublist_trans]
  ,
  METIS_TAC[two_pow_n_is_a_bound2_3]
  ,
  METIS_TAC[two_pow_n_is_a_bound2_4]
  ,
  METIS_TAC[sublist_proj_imp_replace_proj_sat_precond]
]);

val len_filter_replace_proj_eq_len_filter = store_thm("len_filter_replace_proj_eq_len_filter",
`` !P P2 P3 vs. (!a. P3 a /\ P2 (action_proj(a,vs)) ==> (P a <=> P (action_proj(a,vs))))
       /\ (!a. P a /\ P2 a /\ P3 a ==> varset_action(a, vs))
       /\ (!a. P2 a /\ P3 a /\ ~varset_action(a, vs) ==> (DISJOINT (FDOM (SND a)) vs))
        ==> !as' as''. sublist (as') (as_proj(as'', vs)) /\ EVERY P2 as' /\ EVERY P2 as'' /\ EVERY P3 as''
                               ==> (LENGTH (FILTER P (replace_projected([],as',as'',vs)))
                                          = LENGTH (FILTER P as'))``,
NTAC 5 STRIP_TAC
THEN Induct_on `as''`
THEN1 (Cases_on `as'`
      THEN SRW_TAC[][replace_projected_def, as_proj_def, sublist_def])
THEN1(SRW_TAC[][replace_projected_def]
      THEN Cases_on `as'`
      THEN SRW_TAC[][replace_projected_def]
      THEN1(MP_TAC(SIMP_RULE(srw_ss())[EQ_IMP_THM] (every_conj_eq_conj_every |> INST_TYPE [alpha |-> ``:('a |-> 'b) # ('a |-> 'c)``]|> Q.SPECL[`P2`,`P3`, `as''`]))
            THEN SRW_TAC[][]
            THEN METIS_TAC[(((len_filter_p_np_zero_weak
                               |> Q.SPEC `P`)
                               |> Q.SPEC `(\a. P2 a /\ P3 a)`)
                               |> Q.ISPEC `(λa. varset_action (a,vs))`)
                           , LENGTH_0])
      THEN1(MP_TAC(SIMP_RULE(srw_ss())[EQ_IMP_THM] (every_conj_eq_conj_every |> INST_TYPE [alpha |-> ``:('a |-> 'b) # ('a |-> 'c)``]|> Q.SPECL[`P2`,`P3`, `as''`]))
            THEN SRW_TAC[][]
            THEN METIS_TAC[(((len_filter_p_np_zero_weak
                               |> Q.SPEC `P`)
                               |> Q.SPEC `(\a. P2 a /\ P3 a)`)
                               |> Q.ISPEC `(λa. varset_action (a,vs))`)
                           , LENGTH_0])
      THEN1(REWRITE_TAC[REWRITE_RULE[APPEND_NIL](replace_proj_append
						     |> Q.SPEC `[h]`
						     |> Q.SPEC `[]`
						     |> Q.SPEC `t`
						     |> Q.SPEC `as''`
						     |> Q.SPEC `vs`)]
            THEN SRW_TAC[][]
            THEN SRW_TAC[][FILTER_APPEND_DISTRIB]
            THEN1(FIRST_X_ASSUM MATCH_MP_TAC
                  THEN SRW_TAC[][]
                  THEN1(FULL_SIMP_TAC(srw_ss())[as_proj_def]
                        THEN METIS_TAC[sublist_def, sublist_cons_2, sublist_cons_3])
                  THEN1 FULL_SIMP_TAC(srw_ss())[]  )
            THEN1(FULL_SIMP_TAC(srw_ss())[]
                  THEN METIS_TAC[]))
      THEN1(REWRITE_TAC[REWRITE_RULE[APPEND_NIL](replace_proj_append
						     |> Q.SPEC `[h]`
						     |> Q.SPEC `[]`
						     |> Q.SPEC `t`
						     |> Q.SPEC `as''`
						     |> Q.SPEC `vs`)]
            THEN SRW_TAC[][]
            THEN SRW_TAC[][FILTER_APPEND_DISTRIB]
            THEN1(FULL_SIMP_TAC(srw_ss())[]
                  THEN METIS_TAC[])
            THEN1(FIRST_X_ASSUM MATCH_MP_TAC
                  THEN SRW_TAC[][]
                  THEN FULL_SIMP_TAC(srw_ss())[as_proj_def]
                  THEN METIS_TAC[sublist_def, sublist_cons_2, sublist_cons_3]))
      THEN1(FIRST_X_ASSUM (MP_TAC o Q.SPECL[`h'::t`])
            THEN SRW_TAC[][] 
            THEN FIRST_X_ASSUM MATCH_MP_TAC
            THEN FULL_SIMP_TAC(srw_ss())[as_proj_def]
            THEN METIS_TAC[sublist_def, sublist_cons_2, sublist_cons_3])
      THEN1(FIRST_X_ASSUM (MP_TAC o Q.SPECL[`h'::t`])
            THEN SRW_TAC[][]
            THEN FIRST_X_ASSUM MATCH_MP_TAC
            THEN FULL_SIMP_TAC(srw_ss())[as_proj_def]
            THEN METIS_TAC[sublist_def, sublist_cons_2, sublist_cons_3])
      THEN1(REWRITE_TAC[REWRITE_RULE[APPEND_NIL](replace_proj_append
						     |> Q.SPEC `[h]`
						     |> Q.SPEC `[]`
						     |> Q.SPEC `t`
						     |> Q.SPEC `as''`
						     |> Q.SPEC `vs`)]
            THEN SRW_TAC[][]
            THEN SRW_TAC[][FILTER_APPEND_DISTRIB]
            THEN1 METIS_TAC[]
            THEN1(FIRST_X_ASSUM (MP_TAC o Q.SPECL[`h'::t`])
                  THEN SRW_TAC[][]
                  THEN FIRST_X_ASSUM MATCH_MP_TAC
                  THEN FULL_SIMP_TAC(srw_ss())[as_proj_def]
                  THEN METIS_TAC[sublist_def, sublist_cons_2, sublist_cons_3])) 
      THEN1(REWRITE_TAC[REWRITE_RULE[APPEND_NIL](replace_proj_append
						     |> Q.SPEC `[h]`
						     |> Q.SPEC `[]`
						     |> Q.SPEC `t`
						     |> Q.SPEC `as''`
						     |> Q.SPEC `vs`)]
            THEN SRW_TAC[][]
            THEN SRW_TAC[][FILTER_APPEND_DISTRIB]
            THEN1(METIS_TAC[])
            THEN1(FIRST_X_ASSUM (MP_TAC o Q.SPECL[`h'::t`])
                  THEN SRW_TAC[][]
                  THEN FIRST_X_ASSUM MATCH_MP_TAC
                  THEN FULL_SIMP_TAC(srw_ss())[as_proj_def, FDOM_DRESTRICT, DISJOINT_DEF]
                  THEN MP_TAC(sublist_every |> INST_TYPE [alpha |-> ``:'a state # 'a state``] |> Q.SPECL [`h::as''`, `as'''`, `P2`])
                  THEN SRW_TAC[][]
                  THEN METIS_TAC[sublist_def, sublist_cons_2, sublist_cons_3]))))

val bound_vset_drest_eff_eq'' = store_thm("bound_vset_drest_eff_eq''",
``!PROB as as' vs s. child_parent_rel(PROB, vs) /\ s IN valid_states PROB
                     /\ no_effectless_act(as) /\ sublist as' (as_proj(as, vs)) 
                     /\ as IN valid_plans PROB /\ (DRESTRICT (exec_plan(s,as)) vs = (exec_plan(DRESTRICT s vs,as')))
                     /\ sat_precond_as(s, as) /\ sat_precond_as(s, as')
                    ==>  (exec_plan(s, replace_projected ([],as',as,vs)) = exec_plan(s, as))``,
SRW_TAC[][]
THEN MP_TAC(replace_proj_exec_works |> Q.SPECL[`PROB`, `s`, `as`, `vs`, `as'`])
THEN SRW_TAC[][]
THEN MP_TAC(replace_proj_exec_works_diff |> Q.SPECL[`PROB`, `vs`, `s`,`as'`, `as`])
THEN SRW_TAC[][]
THEN MP_TAC(valid_as_valid_exec |> Q.SPECL[`as`, `s`, `PROB`])
THEN SRW_TAC[][]
THEN MATCH_MP_TAC(graph_plan_lemma_5)
THEN Q.EXISTS_TAC `vs`
THEN SRW_TAC[][]
THEN `(exec_plan (s,replace_projected ([],as',as,vs))) IN valid_states PROB` by METIS_TAC[two_pow_n_is_a_bound2_2, sublist_valid_plan, valid_as_valid_exec]
THEN FULL_SIMP_TAC(srw_ss())[valid_states_def])

val _ = export_theory();
