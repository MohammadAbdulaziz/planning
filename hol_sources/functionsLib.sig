signature functionsLib =
sig

val assume_conl_after_proving_concl : Abbrev.thm -> Abbrev.tactic

val assume_thm_concl_after_proving_assums : Abbrev.thm -> Abbrev.goal -> Abbrev.goal list * (Abbrev.thm list -> Abbrev.thm)

val assume_thm_concl_after_proving_assums_2 : Abbrev.thm -> Abbrev.goal -> Abbrev.goal list * (Abbrev.thm list -> Abbrev.thm)

end;