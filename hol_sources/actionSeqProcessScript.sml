open HolKernel Parse boolLib bossLib;
open finite_mapTheory;
open factoredSystemTheory;
open sublistTheory;
open listTheory;
open pred_setTheory;
open fmap_utilsTheory
open list_utilsTheory

val _ = new_theory "actionSeqProcess";

val sat_precond_as_def = 
    Define `(sat_precond_as(s, a::as) = (FST a SUBMAP s /\ sat_precond_as(state_succ s a, as))) 
    /\ (sat_precond_as(s, []) = T)`; 

val sat_precond_as_pair = store_thm("sat_precond_as_pair",
``(sat_precond_as(s, (p,e)::as) = (p SUBMAP s /\ sat_precond_as(state_succ s (p,e), as))) 
    /\ (sat_precond_as(s, []) = T)``,
METIS_TAC[sat_precond_as_def, pairTheory.PAIR, pairTheory.FST, pairTheory.SND])

val rem_effectless_act_def 
   = Define `(rem_effectless_act(a::as)
	         = if (FDOM(SND a) <> EMPTY) then a::rem_effectless_act(as)
	     	   else rem_effectless_act(as))
	   /\ (rem_effectless_act([]) = [])`;

val no_effectless_act_def
    = Define `(no_effectless_act(a::as)
		= ((FDOM (SND a) <> EMPTY) /\ no_effectless_act(as)))
	    /\ (no_effectless_act([]) = T)` ;

val graph_plan_lemma_4 = store_thm("graph_plan_lemma_4",
``!s s' as vs P. ((!a. (MEM a as /\ P a) ==> DISJOINT (FDOM (SND a)) vs)
	    /\ sat_precond_as(s, as)
	    /\ sat_precond_as(s', FILTER (\a. ~(P a)) as)
	    /\ (DRESTRICT s vs = DRESTRICT s' vs)) 
     	    ==>
            ((DRESTRICT (exec_plan(s,as)) (vs)) = (DRESTRICT (exec_plan (s',FILTER (\a. ~(P a)) as)) (vs)))``,
Induct_on`as`
THEN SRW_TAC[][exec_plan_def]
THENL
[
	FULL_SIMP_TAC(srw_ss())[sat_precond_as_def] 	
	THEN `DRESTRICT (state_succ s h) vs = DRESTRICT (state_succ s' h) vs` by SRW_TAC[][proj_eq_proj_exec_eq]
	THEN METIS_TAC[]
	,
	FULL_SIMP_TAC(srw_ss())[sat_precond_as_def] 	
	THEN `DRESTRICT (state_succ s h) (vs) = DRESTRICT s (vs)` by METIS_TAC[GSYM disj_imp_eq_proj_exec]
	THEN METIS_TAC[]
]);

val rem_condless_act_def 
   = Define `(rem_condless_act(s, pfx_a, a::as) = if ((FST a) SUBMAP exec_plan(s, pfx_a)) then rem_condless_act(s, pfx_a++[a], as)
	     else rem_condless_act(s, pfx_a, as))
	   /\ (rem_condless_act(s, pfx_a, [] ) = pfx_a)`;

val rem_condless_act_pair = store_thm("rem_condless_act_pair",
``(rem_condless_act(s, pfx_a, (p,e)::as) = if (p SUBMAP exec_plan(s, pfx_a)) then rem_condless_act(s, pfx_a++[(p,e)], as)
	     else rem_condless_act(s, pfx_a, as))
	   /\ (rem_condless_act(s, pfx_a, [] ) = pfx_a)``,
METIS_TAC[rem_condless_act_def, pairTheory.PAIR, pairTheory.FST, pairTheory.SND])

val exec_remcondless_cons = store_thm("exec_remcondless_cons",
``!s h as pfx.
     exec_plan (s,rem_condless_act (s,h:: pfx,as)) 
            = exec_plan (state_succ s h,rem_condless_act (state_succ s h ,pfx,as))``,
Induct_on`as`
THEN SRW_TAC[][rem_condless_act_def, exec_plan_def, state_succ_def]);

val rem_condless_valid_1 = store_thm("rem_condless_valid_1",
``!as s. (exec_plan(s, as) = exec_plan(s, rem_condless_act(s, [], as)))``,
Induct_on`as`
THEN SRW_TAC[][rem_condless_act_def, exec_plan_def]
THEN METIS_TAC[exec_remcondless_cons, FDOM_state_succ, state_succ_def]);

val rem_condless_act_cons = store_thm("rem_condless_act_cons",
``!h' pfx as s. rem_condless_act (s,h'::pfx,as) = h'::rem_condless_act (state_succ s h',pfx,as)``,
Induct_on`as`
THEN SRW_TAC[][rem_condless_act_def, exec_plan_def, state_succ_def]);

val rem_condless_act_cons_prefix = store_thm("rem_condless_act_cons_prefix",
``! h h' as as' s. h'::as' <<= rem_condless_act (s,[h],as) ==> (as' <<=  rem_condless_act (state_succ s h,[],as) /\ (h' = h))``,
Induct_on`as`
THEN SRW_TAC[][rem_condless_act_def, exec_plan_def]
THEN FULL_SIMP_TAC(srw_ss())[rem_condless_act_cons]);


val rem_condless_valid_2 = store_thm("rem_condless_valid_2",
``!as s.  sat_precond_as(s, rem_condless_act (s,[],as))``,
Induct_on`as`
THEN SRW_TAC[][rem_condless_act_def, exec_plan_def, sat_precond_as_def, rem_condless_act_cons]);

val rem_condless_valid_3 = store_thm("rem_condless_valid_3",
``!as s. (LENGTH (rem_condless_act(s, [], as)) <= LENGTH as)``,
Induct_on`as`
THEN SRW_TAC[][rem_condless_act_def, exec_plan_def]
THENL
[
	FULL_SIMP_TAC(srw_ss())[rem_condless_act_cons]
	THEN SRW_TAC[][rem_condless_act_cons]
	,
	`LENGTH (rem_condless_act (s,[],as)) ≤ (LENGTH as)` by METIS_TAC[]
	THEN DECIDE_TAC
]);

val rem_condless_valid_4 = store_thm("rem_condless_valid_4",
``!as A s. (set as SUBSET A) ==>
	  (set (rem_condless_act (s,[],as)) SUBSET A )``,
Induct_on`as`
THEN SRW_TAC[][rem_condless_act_def, exec_plan_def]
THEN FULL_SIMP_TAC(srw_ss())[rem_condless_act_cons]
THEN METIS_TAC[rem_condless_act_cons, FDOM_state_succ]);

val rem_condless_valid_6 = store_thm("rem_condless_valid_6",
``!as s P. (LENGTH (FILTER P (rem_condless_act (s,[],as)))  <= LENGTH (FILTER P as) )``,
Induct_on`as`
THEN SRW_TAC[][rem_condless_act_def, exec_plan_def]
THENL
[
	FULL_SIMP_TAC(srw_ss())[rem_condless_act_cons]
	THEN METIS_TAC[rem_condless_act_cons, FDOM_state_succ]
	,
	FULL_SIMP_TAC(srw_ss())[rem_condless_act_cons]
	THEN METIS_TAC[rem_condless_act_cons, FDOM_state_succ]
	,
	`LENGTH (FILTER P (rem_condless_act (s,[],as))) <= (LENGTH (FILTER P as))` by METIS_TAC[]
	THEN DECIDE_TAC
]);

val rem_condless_valid_7 = store_thm("rem_condless_valid_7",
``!s P as as2. (EVERY P as /\ EVERY P as2)==> EVERY (P) (rem_condless_act (s,as2,as))``,
Induct_on `as`
THEN SRW_TAC[][rem_condless_act_def]);

val rem_condless_valid_8 = store_thm("rem_condless_valid_8",
``!s as. sublist (rem_condless_act(s, [], as)) as``,
Induct_on `as`
THEN SRW_TAC[SatisfySimps.SATISFY_ss][rem_condless_act_def, sublist_cons_4]
THEN METIS_TAC[rem_condless_act_cons, sublist_def]);

val rem_condless_valid_10 = store_thm("rem_condless_valid_10",
``!PROB as. as IN (valid_plans PROB) ==> rem_condless_act(s, [], as) IN (valid_plans PROB)``,
SRW_TAC[][valid_plans_def]
THEN METIS_TAC[rem_condless_valid_1, rem_condless_valid_4]);

val rem_condless_valid = store_thm("rem_condless_valid",
``!as A s. (exec_plan(s, as) = exec_plan(s, rem_condless_act(s, [], as)))
	  /\ sat_precond_as(s, rem_condless_act(s, [], as))
	  /\ (LENGTH (rem_condless_act(s, [], as)) <= LENGTH as) 
	  /\ ((set as SUBSET A) ==> (set (rem_condless_act (s,[],as)) SUBSET A ))
	  /\ (!P. (LENGTH (FILTER P (rem_condless_act (s,[],as)))  <= LENGTH (FILTER P as) ))``,
METIS_TAC[rem_condless_valid_1, rem_condless_valid_2, rem_condless_valid_3, rem_condless_valid_4, rem_condless_valid_6]);

val submap_sat_precond_submap = store_thm("submap_sat_precond_submap",
``!s1 s2 as. (s1 SUBMAP s2) /\ sat_precond_as(s1, as )
             ==> sat_precond_as(s2, as)``,
Induct_on `as`
THEN SRW_TAC[][sat_precond_as_def]
THEN1 METIS_TAC[SUBMAP_TRANS]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN Q.EXISTS_TAC `state_succ s1 h`
THEN SRW_TAC[][]
THEN METIS_TAC[submap_imp_state_succ_submap])

val submap_init_submap_exec = store_thm("submap_init_submap_exec",
``!s1 s2. s1 SUBMAP s2 /\ sat_precond_as(s1, as ) ==>
  exec_plan(s1, as) SUBMAP (exec_plan(s2, as))``,
Induct_on `as`
THEN SRW_TAC[][exec_plan_def]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[sat_precond_as_def]
THEN METIS_TAC[submap_imp_state_succ_submap])

val sat_precond_drest_sat_precond = store_thm("sat_precond_drest_sat_precond",
``!vs s as. sat_precond_as(DRESTRICT s vs, as )
            ==> sat_precond_as(s, as)``,
METIS_TAC[DRESTRICT_SUBMAP, submap_sat_precond_submap])

val varset_action_def = Define ` varset_action(a, varset) <=>  ((FDOM (SND a)) SUBSET varset)`;

val varset_action_pair = store_thm("varset_action_pair",
`` varset_action((p,e), vs) <=>  ((FDOM e) SUBSET vs)``,
METIS_TAC[varset_action_def, pairTheory.PAIR, pairTheory.SND])

val eq_effect_eq_vset = store_thm("eq_effect_eq_vset",
``!x y. (SND (x) = SND y)
        ==> ((\a. varset_action(a, vs)) x = (\a. varset_action(a, vs)) y)``,
SRW_TAC[][varset_action_def]);

val rem_effectless_works_1 = store_thm("rem_effectless_works_1",
``!s as. (exec_plan (s,as) = exec_plan (s,rem_effectless_act (as)))``,
Induct_on `as` 
THEN SRW_TAC[][rem_effectless_act_def, exec_plan_def, empty_eff_exec_eq]
THEN SRW_TAC[][rem_effectless_act_def, exec_plan_def, empty_eff_exec_eq]);

val rem_effectless_works_2 = store_thm("rem_effectless_works_2",
``!as s. ( sat_precond_as (s, as) ==> sat_precond_as (s,rem_effectless_act (as)))``,
Induct_on`as`
THEN SRW_TAC[][sat_precond_as_def, rem_effectless_act_def]
THEN FULL_SIMP_TAC(srw_ss())[empty_eff_exec_eq]);

val rem_effectless_works_3 = store_thm("rem_effectless_works_3",
``!as. LENGTH (rem_effectless_act (as)) ≤ LENGTH as``,
Induct_on `as`
THEN SRW_TAC[][rem_effectless_act_def]
THEN ` LENGTH (rem_effectless_act as) ≤ (LENGTH as)` by SRW_TAC[][]
THEN DECIDE_TAC);

val rem_effectless_works_4 = store_thm("rem_effectless_works_4",
``! A as. (set as ⊆ A ⇒ set (rem_effectless_act (as)) ⊆ A)``,
Induct_on`as`
THEN SRW_TAC[][rem_effectless_act_def]);

val rem_effectless_works_4' = store_thm("rem_effectless_works_4'",
``! A as. (as IN (valid_plans A)) ⇒ (rem_effectless_act (as)) IN (valid_plans A)``,
Induct_on`as`
THEN FULL_SIMP_TAC(srw_ss())[rem_effectless_act_def, valid_plans_def]
THEN SRW_TAC[][]);

val rem_effectless_works_5 = store_thm("rem_effectless_works_5",
``∀P as. LENGTH (FILTER P (rem_effectless_act (as))) ≤
       LENGTH (FILTER P as)``,
Induct_on`as`
THEN SRW_TAC[][rem_effectless_act_def]
THEN `LENGTH (FILTER P (rem_effectless_act as)) ≤  (LENGTH (FILTER P as))` by SRW_TAC[][]
THEN DECIDE_TAC);

val rem_effectless_works_6 = store_thm("rem_effectless_works_6", 
``!as. no_effectless_act (rem_effectless_act (as))``,
Induct_on`as`
THEN SRW_TAC[][no_effectless_act_def, rem_effectless_act_def]);

val rem_effectless_works_7 = store_thm("rem_effectless_works_7",
``!as. no_effectless_act(as) =  EVERY (\a. FDOM(SND a) <> EMPTY) as ``,
Induct_on `as`
THEN SRW_TAC[][no_effectless_act_def, EVERY_DEF]);

val rem_effectless_works_8 = store_thm("rem_effectless_works_8",
``!P as. (EVERY P as)==> EVERY (P) (rem_effectless_act (as))``,
Induct_on `as`
THEN SRW_TAC[][rem_effectless_act_def]);

val rem_effectless_works_9 = store_thm("rem_effectless_works_9",
``!as. sublist (rem_effectless_act(as)) as``,
Induct_on `as`
THEN SRW_TAC[SatisfySimps.SATISFY_ss][rem_effectless_act_def, sublist_cons_4, sublist_def]);

val rem_effectless_works_10 = store_thm("rem_effectless_works_10",
``!as P. no_effectless_act(as) 
        ==> no_effectless_act(FILTER P as)``,
METIS_TAC[rem_effectless_works_7,EVERY_FILTER_IMP]);

val rem_effectless_works_11 = store_thm("rem_effectless_works_11",
``!as1 as2. sublist as1 (rem_effectless_act(as2))
       ==> sublist as1 as2 ``,
SRW_TAC[][]
THEN METIS_TAC[rem_effectless_works_9, sublist_trans]);

val rem_effectless_works_12 = store_thm("rem_effectless_works_12",
``!as1 as2. no_effectless_act(as1 ++ as2) 
       	    <=> no_effectless_act(as1) /\ no_effectless_act(as2)``,
Induct_on `as1`
THEN SRW_TAC[][no_effectless_act_def]
THEN METIS_TAC[]);


val rem_effectless_works_13 = store_thm("rem_effectless_works_13",
``!as1 as2. sublist as1 as2 /\ no_effectless_act(as2)
       ==>   no_effectless_act(as1)``,
Induct_on `as1`
THEN SRW_TAC[][no_effectless_act_def]
THEN MP_TAC(sublist_MEM |> INST_TYPE[alpha |-> ``:α # (β |-> γ)``] |> Q.SPECL[`h`,`as1`,`as2`])
THEN SRW_TAC[][]
THEN1 METIS_TAC[EVERY_MEM |> Q.ISPEC `(\a. FDOM(SND a) <> EMPTY)`, rem_effectless_works_7]
THEN1 METIS_TAC[sublist_cons_3]);

val rem_effectless_works_14 = store_thm("rem_effectless_works_14",
``!PROB as. exec_plan(s, as) = exec_plan(s, rem_effectless_act(as))``,
METIS_TAC[rem_effectless_works_1]
THEN METIS_TAC[rem_effectless_works_4]);

val rem_effectless_works = store_thm("rem_effectless_works", 
`` !s A as.   (exec_plan (s,as) = exec_plan (s,rem_effectless_act (as))) ∧
     (sat_precond_as (s, as) ==> sat_precond_as (s,rem_effectless_act (as))) ∧
     LENGTH (rem_effectless_act (as)) ≤ LENGTH as ∧
     (set as ⊆ A ⇒ set (rem_effectless_act (as)) ⊆ A) ∧
     no_effectless_act (rem_effectless_act (as)) /\
     ∀P.
       LENGTH (FILTER P (rem_effectless_act (as))) ≤
       LENGTH (FILTER P as)``,
METIS_TAC[rem_effectless_works_1, rem_effectless_works_2, rem_effectless_works_3, rem_effectless_works_4, rem_effectless_works_5, rem_effectless_works_6]);

val rem_effectless_act_set_def = Define`rem_effectless_act_set A = {a | a IN A /\ (FDOM (SND a) <> EMPTY)}`;

val rem_effectless_act_subset_rem_effectless_act_set_thm = store_thm("rem_effectless_act_subset_rem_effectless_act_set_thm",
``!as A. set as SUBSET A ==> (set (rem_effectless_act as)) SUBSET (rem_effectless_act_set A)``,
Induct_on `as`
THEN SRW_TAC[][rem_effectless_act_def]
THEN SRW_TAC[][rem_effectless_act_set_def])

val rem_effectless_act_set_no_empty_actions_thm = store_thm("rem_effectless_act_set_no_empty_actions_thm",
``!A. rem_effectless_act_set A SUBSET {a |FDOM (SND a) ≠ ∅}``,
SRW_TAC[][rem_effectless_act_set_def, SUBSET_DEF])

val rem_condless_valid_9 = store_thm("rem_condless_valid_9",
``!s as. no_effectless_act(as) ==> no_effectless_act(rem_condless_act(s, [], as))``,
Induct_on `as`
THEN SRW_TAC[SatisfySimps.SATISFY_ss][rem_condless_act_def, no_effectless_act_def]
THEN METIS_TAC[rem_condless_act_cons, no_effectless_act_def]);

val graph_plan_lemma_17 = store_thm("graph_plan_lemma_17",
``!as_1 as_2 as s. (as_1++as_2 = as) /\ sat_precond_as(s,as)
==>(sat_precond_as(s,as_1) /\ sat_precond_as( exec_plan(s, as_1),as_2))``,
Induct_on`as`
THEN SRW_TAC[][exec_plan_def]
THEN Cases_on`as_1`
THEN FULL_SIMP_TAC(srw_ss())[rich_listTheory.IS_PREFIX_NIL, isPREFIX_THM, exec_plan_def, sat_precond_as_def]
THEN SRW_TAC[][]);

val nempty_eff_every_nempty_act = store_thm("nempty_eff_every_nempty_act",
``!as. no_effectless_act as /\ (!x. ~(FDOM (SND (f x)) = EMPTY))
       ==> EVERY (λa. ~(f a = (FEMPTY,FEMPTY))) as``,
Induct_on `as` THEN SRW_TAC[][EVERY_DEF]
THEN  METIS_TAC[nempty_eff_nempty_act,
                rem_effectless_works_7,
                no_effectless_act_def])

val empty_replace_proj_dual7 = store_thm("empty_replace_proj_dual7",
``!s as as'. sat_precond_as(s, as++as') ==> sat_precond_as(exec_plan(s, as) , as')``,
Induct_on `as`
THEN SRW_TAC[][sat_precond_as_def, exec_plan_def])

val not_vset_not_disj_eff_prod_dom_diff = store_thm("not_vset_not_disj_eff_prod_dom_diff",
``!PROB a vs. a IN PROB /\ ~ varset_action (a,vs)
              ==> ~(DISJOINT (FDOM (SND a)) ((prob_dom PROB) DIFF vs))``,
SRW_TAC[][]
THEN MP_TAC(FDOM_eff_subset_prob_dom_pair |> Q.SPEC `a`)
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[DISJOINT_DEF, INTER_DEF, varset_action_def, EXTENSION,FILTER, MEM, SUBSET_DEF]
THEN SRW_TAC[][]
THEN METIS_TAC[]);

val vset_disj_dom_eff_diff = store_thm("vset_disj_dom_eff_diff",
``!PROB a vs. (varset_action(a, vs)) 
              ==> (DISJOINT  (FDOM (SND a)) ((prob_dom PROB) DIFF vs))``,
SRW_TAC[][varset_action_def, DISJOINT_DEF, INTER_DEF, EXTENSION]
THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF]
THEN METIS_TAC[]);

val vset_diff_disj_eff_vs = store_thm("vset_diff_disj_eff_vs",
``!PROB a vs. (varset_action(a, (prob_dom PROB) DIFF vs)) 
              ==> (DISJOINT  (FDOM (SND a)) vs)``,
SRW_TAC[][varset_action_def, DISJOINT_DEF, INTER_DEF, EXTENSION]
THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF]
THEN METIS_TAC[]);

val vset_nempty_efff_not_disj_eff_vs = store_thm("vset_nempty_efff_not_disj_eff_vs",
``!PROB a vs. varset_action(a, vs) /\ (FDOM (SND a) <> EMPTY)
              ==> (~ DISJOINT  (FDOM (SND a)) vs)``,
SRW_TAC[][varset_action_def, DISJOINT_DEF, INTER_DEF, EXTENSION]
THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF]
THEN METIS_TAC[]);

val vset_disj_eff_diff = store_thm("vset_disj_eff_diff",
``!s a vs. varset_action(a, vs)
           ==> (DISJOINT  (FDOM (SND a)) (s DIFF vs))``,
SRW_TAC[][varset_action_def, DISJOINT_DEF, INTER_DEF, EXTENSION]
THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF]
THEN METIS_TAC[]);

val every_vset_imp_drestrict_exec_eq = store_thm("every_vset_imp_drestrict_exec_eq",
``!PROB vs as s. (EVERY (\a. varset_action(a, (prob_dom PROB) DIFF vs)) as)
                  ==>
                   (DRESTRICT s vs = DRESTRICT (exec_plan(s, as)) vs)``,
SRW_TAC[][]
THEN `(∀a. MEM a as  ⇒ DISJOINT (FDOM (SND a)) vs)` by 
      (SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[EVERY_MEM]
      THEN METIS_TAC[vset_diff_disj_eff_vs])
THEN `EVERY (\a. DISJOINT (FDOM (SND a)) vs) as` by FULL_SIMP_TAC(srw_ss())[EVERY_MEM ]
THEN `EVERY (\a. DISJOINT (FDOM (SND a)) vs) (rem_condless_act(s, [], as))` by FULL_SIMP_TAC(srw_ss())[rem_condless_valid_7]
THEN FULL_SIMP_TAC(srw_ss())[EVERY_MEM]
THEN SRW_TAC[][]
THEN `(exec_plan(s, as) = exec_plan(s, rem_condless_act(s, [], as))) /\ sat_precond_as(s, rem_condless_act(s, [], as))` by SRW_TAC[][Once rem_condless_valid_1, rem_condless_valid_2]
THEN MP_TAC(graph_plan_lemma_4 |>INST_TYPE [beta |-> ``:'d``] |> Q.SPECL [`s`, `s`, `rem_condless_act(s, [], as)`, `vs`, `(\a. varset_action(a, (prob_dom PROB) DIFF vs))`])
THEN SRW_TAC[][]
THEN `EVERY (\a. varset_action (a, (prob_dom PROB) DIFF vs)) (rem_condless_act (s,[],as))` by FULL_SIMP_TAC(srw_ss())[EVERY_MEM, rem_condless_valid_7] 
THEN `FILTER (λa. ¬varset_action (a,(prob_dom PROB) DIFF vs)) (rem_condless_act (s,[],as)) = []` by SRW_TAC[][filter_empty_every_not]
THEN `sat_precond_as (s, FILTER (λa. ¬varset_action (a,(prob_dom PROB) DIFF vs)) (rem_condless_act (s,[],as)))` by SRW_TAC[][sat_precond_as_def]
THEN FULL_SIMP_TAC(srw_ss())[exec_plan_def, EVERY_MEM]);

val no_effectless_act_works = store_thm("no_effectless_act_works",
``!as. no_effectless_act as ==> (FILTER (\a. ~(FDOM (SND a) = EMPTY)) as = as)``,
METIS_TAC[rem_effectless_works_7,((GSYM FILTER_EQ_ID ) |> Q.ISPEC `(λa. FDOM (SND a) ≠ ∅)` |> Q.SPEC `as`)]);

val varset_act_diff_un_imp_varset_diff = store_thm("varset_act_diff_un_imp_varset_diff",
``!a vs vs' vs''. varset_action (a,vs'' DIFF (vs' ∪ vs))
                  ==> varset_action (a,vs'' DIFF vs)``,
SRW_TAC[][varset_action_def, DIFF_DEF, UNION_DEF, SUBSET_DEF])

val vset_diff_union_vset_diff = store_thm("vset_diff_union_vset_diff",
``!s vs vs' a. varset_action(a, s DIFF (vs UNION vs')) 
               ==> varset_action (a, s DIFF vs')``,
SRW_TAC[][varset_action_def,
          DISJOINT_DEF, INTER_DEF, EXTENSION,
          FDOM_DRESTRICT, SUBSET_DEF, UNION_DEF]
THEN METIS_TAC[])

val valid_filter_vset_dom_idempot = store_thm("valid_filter_vset_dom_idempot",
``!PROB as. as IN valid_plans PROB
                 ==> (FILTER (\a. varset_action(a, prob_dom PROB)) as = as)``,
STRIP_TAC
THEN Induct_on `as`
THEN SRW_TAC[][]
THEN1 METIS_TAC[valid_plan_valid_tail, valid_plan_valid_head]
THEN FULL_SIMP_TAC(srw_ss())[varset_action_def]
THEN METIS_TAC[valid_plan_valid_head, FDOM_eff_subset_prob_dom_pair])

val n_replace_proj_le_n_as_1 = store_thm("n_replace_proj_le_n_as_1",
``!a vs vs'. vs SUBSET vs' /\ varset_action(a, vs) ==> varset_action(a, vs')``,
SRW_TAC[][SUBSET_DEF, varset_action_def])

val sat_precond_as_pfx = store_thm("sat_precond_as_pfx",
``!s. sat_precond_as (s,as ++ as') ==> 
     sat_precond_as(s,as)``,
Induct_on `as`
THEN SRW_TAC[][sat_precond_as_def])

val _ = export_theory();
