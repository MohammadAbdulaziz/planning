open HolKernel Parse boolLib bossLib;
open finite_mapTheory
open pred_setTheory
open rel_utilsTheory
open relationTheory
open factoredSystemTheory
open actionSeqProcessTheory

val _ = new_theory "dependency";

val dep_def = Define`dep(PROB, v1, v2) <=>  (?a. (a IN PROB) /\ (((v1 IN (FDOM (FST a))) /\ (v2 IN (FDOM (SND a))) ) \/ ((v1 IN (FDOM (SND a))) /\ (v2 IN (FDOM (SND a))) )) ) \/ (v1 = v2) `;

val dep_var_set_def = Define`dep_var_set (PROB, vs1, vs2) <=> ? v1 v2. (v1 IN vs1) /\ (v2 IN vs2) /\ (DISJOINT vs1 vs2) /\ dep(PROB, v1, v2)`;

val dep_var_set_self_empty = store_thm("dep_var_set_self_empty",
``!PROB vs. dep_var_set(PROB, vs, vs) ==> (vs = EMPTY)``,
SRW_TAC[][dep_var_set_def, DISJOINT_DEF]);

val DEP_REFL = store_thm("DEP_REFL",
``!PROB. reflexive (\v v'. dep (PROB,v,v'))``,
SRW_TAC[][dep_def, reflexive_def])

val NEQ_DEP_IMP_IN_DOM = store_thm("NEQ_DEP_IMP_IN_DOM",
``!PROB v v'. ~(v = v') /\ dep(PROB, v, v')
              ==> v IN (prob_dom PROB) /\ v' IN (prob_dom PROB)``,
SRW_TAC[][dep_def, SUBSET_DEF]
THEN METIS_TAC[FDOM_pre_subset_prob_dom_pair, FDOM_eff_subset_prob_dom_pair, SUBSET_DEF])

val dep_sos_imp_mem_dep = store_thm("dep_sos_imp_mem_dep",
``!PROB S vs. dep_var_set(PROB, BIGUNION S, vs)
              ==> ?vs'. vs' IN S /\ dep_var_set(PROB, vs', vs)``,
SRW_TAC[][dep_var_set_def]
THEN METIS_TAC[DISJOINT_SYM])

val dep_union_imp_or_dep = store_thm("dep_union_imp_or_dep",
``!PROB vs vs' vs''. dep_var_set (PROB,vs,vs' UNION vs'')
                     ==> dep_var_set (PROB,vs,vs') \/ dep_var_set (PROB,vs,vs'')``,
SRW_TAC[][dep_var_set_def]
THEN METIS_TAC[DISJOINT_SYM])

val dep_biunion_imp_or_dep = store_thm("dep_biunion_imp_or_dep",
``!PROB vs S. dep_var_set (PROB,vs, BIGUNION S)
                     ==> ?vs'. vs' IN S /\ dep_var_set (PROB, vs, vs')``,
SRW_TAC[][dep_var_set_def]
THEN METIS_TAC[DISJOINT_SYM])

val dep_tc_def = Define `dep_tc (PROB)   =  TC (\v1' v2'. dep(PROB, v1', v2'))`;

val dep_tc_imp_in_dom = store_thm("dep_tc_imp_in_dom",
``!PROB v1 v2. ~(v1 = v2) /\ dep_tc PROB v1 v2
               ==> v1 IN (prob_dom PROB)``,
SRW_TAC[][dep_tc_def]
THEN MP_TAC(TC_CASES1_NEQ |> Q.SPECL[`(λv1' v2'. dep (PROB,v1',v2'))`, `v1`, `v2`])
THEN SRW_TAC[][]
THEN METIS_TAC[NEQ_DEP_IMP_IN_DOM])

val not_dep_disj_imp_not_dep = store_thm("not_dep_disj_imp_not_dep",
``!PROB vs_1 vs_2 vs_3.
      DISJOINT vs_1 vs_2 /\ vs_3 SUBSET vs_2 /\ ~dep_var_set(PROB,vs_1,vs_2)
      ==> ~dep_var_set(PROB,vs_1,vs_3)``,
SRW_TAC[][dep_var_set_def, SUBSET_DEF, DISJOINT_DEF, INTER_DEF, EXTENSION]
THEN Q.PAT_X_ASSUM `!x y. P` (MP_TAC o Q.SPECL[`v1`, `v2`])
THEN SRW_TAC[][]
THEN METIS_TAC[])

val dep_slist_imp_mem_dep = store_thm("dep_slist_imp_mem_dep",
``!PROB vs lvs. dep_var_set(PROB,BIGUNION (set lvs), vs) ==> ?vs'. MEM vs' lvs /\ dep_var_set(PROB, vs', vs)``,
SRW_TAC[][dep_var_set_def]
THEN SRW_TAC[][]
THEN METIS_TAC[DISJOINT_SYM])

val n_bigunion_le_sum_3 = store_thm("n_bigunion_le_sum_3",
``!PROB vs svs. (! vs'. vs' IN svs ==>  ~dep_var_set(PROB, vs', vs))
                ==> ~dep_var_set(PROB, BIGUNION svs, vs)``,
SRW_TAC[][dep_var_set_def, DISJOINT_DEF, INTER_DEF, EXTENSION]
THEN METIS_TAC[])

val disj_not_dep_vset_union_imp_or = store_thm("disj_not_dep_vset_union_imp_or",
``!PROB a vs vs'.
    a IN PROB /\ DISJOINT vs vs'
    /\ (~dep_var_set(PROB, vs', vs) \/ ~dep_var_set(PROB, vs, vs'))
    /\ varset_action(a, vs UNION vs') 
    ==> (varset_action(a, vs) \/ varset_action(a, vs'))``,
SRW_TAC[][dep_var_set_def, varset_action_def, UNION_DEF, SUBSET_DEF, dep_def]
THEN METIS_TAC[DISJOINT_SYM])

val _ = export_theory();
