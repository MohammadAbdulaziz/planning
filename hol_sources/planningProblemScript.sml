open HolKernel Parse boolLib bossLib;
open listTheory
open finite_mapTheory
open fmap_utilsTheory
open set_utilsTheory
open factoredSystemTheory 
open actionSeqProcessTheory
open pred_setTheory
open systemAbstractionTheory

val _ = new_theory "planningProblem";

val planningProblem_type_def = Hol_datatype `planningProblem = <|I:'a |-> 'b;
      		     	          PROB:(('a |-> 'b)#('a |-> 'b)) set;
				  G:'a |-> 'b |>`

val planning_prob_dom_def = Define`planning_prob_dom planningPROB = (prob_dom (planningPROB.PROB))`

val plan_def = Define`plan (planningPROB:('a, 'b) planningProblem) as = as IN (valid_plans (planningPROB.PROB)) /\ (planningPROB.G SUBMAP (exec_plan(planningPROB.I, as)))`

val needed_vars_def = Define`needed_vars planningPROB = {v | v IN (FDOM planningPROB.I) /\ ((?a. a IN planningPROB.PROB /\ v IN (FDOM (FST a)) /\ ((FST a) ' v = planningPROB.I ' v)) \/ (v IN (FDOM planningPROB.G) /\ (planningPROB.I ' v = planningPROB.G ' v)))}`

val needed_vars_pair = store_thm("needed_vars_pair",
``needed_vars planningPROB = {v | v IN (FDOM planningPROB.I) /\ ((?p e. (p,e) IN planningPROB.PROB /\ v IN (FDOM p) /\ (p ' v = planningPROB.I ' v)) \/ (v IN (FDOM planningPROB.G) /\ (planningPROB.I ' v = planningPROB.G ' v)))}``,
SRW_TAC[][needed_vars_def, EXTENSION]
THEN METIS_TAC[needed_vars_def, pairTheory.PAIR, pairTheory.FST, pairTheory.SND])

val needed_asses_def = Define`needed_asses planningPROB = DRESTRICT (planningPROB.I) (needed_vars planningPROB)`

val precede_def = 
Define`precede planningPROB1 planningPROB2 =
                       (DRESTRICT (planningPROB1.G)  (needed_vars planningPROB2) = (DRESTRICT (needed_asses planningPROB2) (planning_prob_dom planningPROB1)))
                       /\ (DRESTRICT (planningPROB1.G) (planning_prob_dom planningPROB2) = DRESTRICT (planningPROB2.G) (planning_prob_dom planningPROB1))`

val precede_prob_list_def = 
Define`(precede_prob_list (planningPROB::planningPROBList) = (!planningPROB'. MEM planningPROB' planningPROBList ==>  precede planningPROB planningPROB') /\ precede_prob_list (planningPROBList)) /\
       (precede_prob_list [] = T)`

val precede_prob_list_cons = store_thm("precede_prob_list_cons",
``precede_prob_list (planningPROB::planningPROBList) ==> precede_prob_list (planningPROBList)``,
Induct_on `planningPROBList`
THEN SRW_TAC[][precede_prob_list_def])

val planning_problem_def = Define`planning_problem planningPROB = (planningPROB.I) IN (valid_states planningPROB.PROB) /\ FDOM (planningPROB.G) SUBSET (planning_prob_dom planningPROB)`

val planning_problem_dom = store_thm("planning_problem_dom",
``planning_problem planningPROB ==> (planning_prob_dom planningPROB = FDOM planningPROB.I)``,
SRW_TAC[][planning_problem_def, planning_prob_dom_def, valid_states_def])

val precede_preserve_goals = store_thm("precede_preserve_goals",
``precede planningPROB1 planningPROB2 /\ plan planningPROB2 as /\ planningPROB2.I SUBMAP s /\
  planning_problem planningPROB2
  ==> (planningPROB1.G SUBMAP (exec_plan(s,as)) \/ ~(planningPROB1.G SUBMAP s))``,
SRW_TAC[][precede_def, plan_def]
THEN LAST_X_ASSUM (K ALL_TAC)
THEN Cases_on `(planningPROB1.G ⊑ s)`
THEN SRW_TAC[][]
THEN `DRESTRICT planningPROB1.G (planning_prob_dom planningPROB2)
     SUBMAP exec_plan (s,as)` by
     (`exec_plan (planningPROB2.I,as) SUBMAP exec_plan (s,as)` by
         (`(∀a. MEM a as ⇒ FDOM (FST a) ⊆ (FDOM planningPROB2.I))` by
               (FULL_SIMP_TAC(srw_ss())[planning_problem_def, valid_states_def]
                THEN METIS_TAC[submap_subset, planning_prob_dom_def, SUBSET_TRANS, valid_plan_pre_subset_prob_dom_pair])
          THEN METIS_TAC[valid_as_submap_init_submap_exec])
      THEN METIS_TAC[sat_precond_as_proj_4,
                     SUBMAP_TRANS])
THEN `DRESTRICT planningPROB1.G (FDOM s DIFF (planning_prob_dom planningPROB2))
      SUBMAP exec_plan (s,as)` by
     METIS_TAC[sublist_as_proj_eq_as_1, exec_valid_as_disjoint,
               submap_drest_submap, disj_diff,
               DISJOINT_SYM, planning_prob_dom_def]
THEN MATCH_MP_TAC(submap_drestrict_diff)
THEN Q.EXISTS_TAC `planning_prob_dom planningPROB2`
THEN Q.EXISTS_TAC `FDOM s UNION (planning_prob_dom planningPROB2)`
THEN SRW_TAC[][]
THEN1 METIS_TAC[submap_subset, subset_imp_subset_union]
THEN1 METIS_TAC[exec_plan_fdom_subset, planning_prob_dom_def]
THEN1 SRW_TAC[][DIFF_SAME_UNION])

fun cheat g = ACCEPT_TAC (mk_thm g) g

val needed_asses_submap_succ = store_thm("needed_asses_submap_succ",
``planning_problem planningPROB /\ a IN planningPROB.PROB ==> 
   needed_asses
           (planningPROB with I := state_succ planningPROB.I a) SUBMAP state_succ (needed_asses planningPROB) a ``,
SRW_TAC[][needed_asses_def, needed_vars_def, state_succ_def, DRESTRICT_DEF, SUBMAP_DEF]
THEN1 METIS_TAC[FUNION_DEF]
THEN1 METIS_TAC[FUNION_DEF]
THEN1 METIS_TAC[FUNION_DEF]
THEN1 METIS_TAC[FUNION_DEF]
THEN1 METIS_TAC[FUNION_DEF]
THEN1 METIS_TAC[FUNION_DEF]
THEN1 METIS_TAC[FUNION_DEF]
THEN1 METIS_TAC[FUNION_DEF]
THEN1 METIS_TAC[FUNION_DEF]
THEN1 METIS_TAC[FUNION_DEF]
THEN1 METIS_TAC[FUNION_DEF]
THEN1 METIS_TAC[FUNION_DEF]
THEN1 METIS_TAC[FUNION_DEF]
THEN1(FULL_SIMP_TAC(srw_ss())[FUNION_DEF, planning_prob_dom_def,DRESTRICT_DEF]
      THEN SRW_TAC[][]
      THEN Cases_on `x ∈ FDOM (SND a)`
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[FUNION_DEF]
      THEN METIS_TAC[]) 
THEN1(FULL_SIMP_TAC(srw_ss())[FUNION_DEF, planning_prob_dom_def,DRESTRICT_DEF]
      THEN SRW_TAC[][]
      THEN Cases_on `x ∈ FDOM (SND a)`
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[FUNION_DEF]
      THEN METIS_TAC[]) 
THEN1(FULL_SIMP_TAC(srw_ss())[FUNION_DEF, planning_prob_dom_def,DRESTRICT_DEF]
      THEN SRW_TAC[][]
      THEN Cases_on `x ∈ FDOM (SND a)`
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[FUNION_DEF]
      THEN METIS_TAC[]) 
THEN1(FULL_SIMP_TAC(srw_ss())[FUNION_DEF, planning_prob_dom_def, DRESTRICT_DEF, planning_problem_def]
      THEN METIS_TAC[FDOM_eff_subset_prob_dom_pair, SUBSET_DEF])
THEN1(FULL_SIMP_TAC(srw_ss())[FUNION_DEF, planning_prob_dom_def, DRESTRICT_DEF, planning_problem_def]
      THEN METIS_TAC[FDOM_eff_subset_prob_dom_pair, SUBSET_DEF])
THEN METIS_TAC[FUNION_DEF])

val planning_prob_with_succ_init = store_thm("planning_prob_with_succ_init",
``planning_problem planningPROB /\ h IN planningPROB.PROB ==> 
  planning_problem
        (planningPROB with I := state_succ planningPROB.I h)``,
SRW_TAC[][planning_problem_def, planning_prob_dom_def, valid_action_valid_succ])

val plan_works_from_needed_asses_1 = store_thm("plan_works_from_needed_asses",
``a IN planningPROB.PROB /\ h IN planningPROB.PROB ==> 
action_needed_asses a (state_succ (needed_asses planningPROB) h) ⊑
action_needed_asses a
  (needed_asses (planningPROB with I := state_succ planningPROB.I h))``,
MP_TAC (FDOM_eff_subset_prob_dom_pair |> Q.GEN`PROB`)
THEN SRW_TAC[][state_succ_def, action_needed_asses_def, action_needed_vars_def, needed_asses_def, needed_vars_def, FUNION_DEF, SUBSET_DEF, planning_problem_def, planning_prob_dom_def]
THEN FULL_SIMP_TAC(srw_ss())[SUBMAP_DEF, SUBSET_DEF, action_needed_asses_def, action_needed_vars_def, FUNION_DEF, DRESTRICT_FDOM]
THEN FULL_SIMP_TAC(bool_ss)[DRESTRICT_DEF]
THEN FULL_SIMP_TAC(bool_ss)[IN_DEF, INTER_DEF]
THEN FULL_SIMP_TAC(bool_ss)[Ntimes GSPEC_ETA 6]
THEN FULL_SIMP_TAC(bool_ss)[FUNION_DEF, UNION_DEF]
THEN FULL_SIMP_TAC(srw_ss())[SUBMAP_DEF, SUBSET_DEF, action_needed_asses_def, action_needed_vars_def, FUNION_DEF, DRESTRICT_FDOM]
THEN FULL_SIMP_TAC(bool_ss)[DRESTRICT_DEF]
THEN FULL_SIMP_TAC(bool_ss)[IN_DEF, INTER_DEF]
THEN FULL_SIMP_TAC(bool_ss)[Ntimes GSPEC_ETA 6]
THEN FULL_SIMP_TAC(bool_ss)[FUNION_DEF, UNION_DEF]
THEN SRW_TAC[][]
THEN TRY (METIS_TAC[GSPEC_ETA]))

val plan_works_from_needed_asses = store_thm("plan_works_from_needed_asses",
``!planningPROB. planning_problem planningPROB /\ plan planningPROB as ==> 
  planningPROB.G SUBMAP exec_plan((needed_asses planningPROB),as)``,
REWRITE_TAC[plan_def]
THEN Induct_on `as`
THEN SRW_TAC[][exec_plan_def]
THEN1(SRW_TAC[][needed_asses_def, needed_vars_def]
      THEN FULL_SIMP_TAC(srw_ss())[SUBMAP_DEF, DRESTRICT_DEF, planning_problem_def])
THEN `planningPROB.G SUBMAP (exec_plan (needed_asses (planningPROB with I:=state_succ planningPROB.I h), as))` by
       (`planningPROB.G = (planningPROB with I := state_succ planningPROB.I h).G` by 
          SRW_TAC[][]
       THEN FULL_SIMP_TAC(bool_ss)[]
       THEN FIRST_X_ASSUM MATCH_MP_TAC
       THEN FIRST_X_ASSUM (K ALL_TAC)
       THEN SRW_TAC[SatisfySimps.SATISFY_ss][valid_plan_valid_head, valid_plan_valid_tail]
       THEN1 METIS_TAC[planning_prob_with_succ_init, valid_plan_valid_head]
       THEN FULL_SIMP_TAC(srw_ss())[])
THEN MATCH_MP_TAC SUBMAP_TRANS
THEN Q.EXISTS_TAC `exec_plan
                     (needed_asses (planningPROB with I := state_succ planningPROB.I h),as)`
THEN SRW_TAC[][]
THEN MATCH_MP_TAC as_needed_asses_submap_exec
THEN SRW_TAC[][]
THEN1 METIS_TAC[valid_plan_valid_head, needed_asses_submap_succ]
THEN `a IN planningPROB.PROB` by FULL_SIMP_TAC(srw_ss())[valid_plans_def, SUBSET_DEF]
THEN METIS_TAC[plan_works_from_needed_asses_1, valid_plan_valid_head])

val precede_preserve_goals_gen = store_thm("precede_preserve_goals_gen",
``!planningPROB2 as s. precede planningPROB1 planningPROB2 /\ plan planningPROB2 as /\ (needed_asses planningPROB2 = (system_needed_asses (planningPROB2.PROB) s )) /\
  planning_problem planningPROB2 /\ (planningPROB1.G SUBMAP s)
  ==> planningPROB1.G SUBMAP (exec_plan(s,as))``,
SRW_TAC[][precede_def, plan_def]
THEN LAST_X_ASSUM (K ALL_TAC)
THEN SRW_TAC[][]
THEN `DRESTRICT planningPROB1.G (planning_prob_dom planningPROB2)
     SUBMAP exec_plan (s,as)` by
     (`exec_plan (needed_asses planningPROB2,as) SUBMAP exec_plan (s,as)` by
         (MATCH_MP_TAC as_needed_asses_submap_exec
          THEN SRW_TAC[][]
          THEN1 SRW_TAC[SatisfySimps.SATISFY_ss][system_needed_asses_def]
          THEN `a IN planningPROB2.PROB` by
               FULL_SIMP_TAC(srw_ss())[valid_plans_def, SUBSET_DEF]
          THEN METIS_TAC[system_needed_asses_include_action_needed_asses, SUBMAP_ANTISYM])
      THEN METIS_TAC[sat_precond_as_proj_4, SUBMAP_TRANS, plan_works_from_needed_asses, plan_def])
THEN `DRESTRICT planningPROB1.G (FDOM s DIFF (planning_prob_dom planningPROB2))
      SUBMAP exec_plan (s,as)` by
     METIS_TAC[sublist_as_proj_eq_as_1, exec_valid_as_disjoint,
               submap_drest_submap, disj_diff,
               DISJOINT_SYM, planning_prob_dom_def]
THEN MATCH_MP_TAC(submap_drestrict_diff)
THEN Q.EXISTS_TAC `planning_prob_dom planningPROB2`
THEN Q.EXISTS_TAC `FDOM s UNION (planning_prob_dom planningPROB2)`
THEN SRW_TAC[][]
THEN1 METIS_TAC[submap_subset, subset_imp_subset_union]
THEN1 METIS_TAC[exec_plan_fdom_subset, planning_prob_dom_def]
THEN1 SRW_TAC[][DIFF_SAME_UNION])

val precede_preserve_goals_gen' = store_thm("precede_preserve_goals_gen'",
``!planningPROB2 as s. precede planningPROB1 planningPROB2 /\ planningPROB2.G SUBMAP exec_plan(s, as) /\
  as IN valid_plans planningPROB2.PROB /\
  planning_problem planningPROB2 /\ (planningPROB1.G SUBMAP s)
  ==> planningPROB1.G SUBMAP (exec_plan(s,as))``,
SRW_TAC[][precede_def, plan_def]
THEN LAST_X_ASSUM (K ALL_TAC)
THEN SRW_TAC[][]
THEN `DRESTRICT planningPROB1.G (planning_prob_dom planningPROB2)
     SUBMAP exec_plan (s,as)` by
     METIS_TAC[sat_precond_as_proj_4, SUBMAP_ANTISYM, SUBMAP_TRANS]
THEN `DRESTRICT planningPROB1.G (FDOM s DIFF (planning_prob_dom planningPROB2))
      SUBMAP exec_plan (s,as)` by
     METIS_TAC[sublist_as_proj_eq_as_1, exec_valid_as_disjoint,
               submap_drest_submap, disj_diff,
               DISJOINT_SYM, planning_prob_dom_def]
THEN MATCH_MP_TAC(submap_drestrict_diff)
THEN Q.EXISTS_TAC `planning_prob_dom planningPROB2`
THEN Q.EXISTS_TAC `FDOM s UNION (planning_prob_dom planningPROB2)`
THEN SRW_TAC[][]
THEN1 METIS_TAC[submap_subset, subset_imp_subset_union]
THEN1 METIS_TAC[exec_plan_fdom_subset, planning_prob_dom_def]
THEN1 SRW_TAC[][DIFF_SAME_UNION])


val precede_preserve_needed_asses = store_thm("precede_preserve_needed_asses",
``!planningPROB1 as s. precede planningPROB1 planningPROB2 /\ plan planningPROB1 as /\ (needed_asses planningPROB1 = (system_needed_asses (planningPROB1.PROB) s )) /\ (needed_asses planningPROB2) SUBMAP s/\ planning_problem planningPROB1
==> (needed_asses planningPROB2) SUBMAP (exec_plan(s,as))``,
SRW_TAC[][precede_def, plan_def]
THEN SRW_TAC[][]
THEN `(DRESTRICT (needed_asses planningPROB2) ((planning_prob_dom planningPROB1)))
SUBMAP exec_plan (s,as)` by
(`exec_plan (needed_asses planningPROB1,as) SUBMAP exec_plan (s,as)` by
(MATCH_MP_TAC as_needed_asses_submap_exec
THEN SRW_TAC[][]
THEN1 SRW_TAC[SatisfySimps.SATISFY_ss][system_needed_asses_def]
THEN `a IN planningPROB1.PROB` by 
FULL_SIMP_TAC(srw_ss())[valid_plans_def, SUBSET_DEF]
THEN METIS_TAC[system_needed_asses_include_action_needed_asses, SUBMAP_ANTISYM])
THEN METIS_TAC[sat_precond_as_proj_4, SUBMAP_TRANS, plan_works_from_needed_asses, plan_def])
THEN `DRESTRICT (needed_asses planningPROB2) (FDOM s DIFF (planning_prob_dom planningPROB1))
SUBMAP exec_plan (s,as)` by
(`needed_asses planningPROB1 SUBMAP s` by METIS_TAC[SUBMAP_ANTISYM, system_needed_asses_submap, SUBMAP_TRANS]
THEN MATCH_MP_TAC sublist_as_proj_eq_as_1
THEN Q.EXISTS_TAC `(FDOM s DIFF planning_prob_dom planningPROB1)`
THEN `DRESTRICT (exec_plan (s,as))
(FDOM s DIFF planning_prob_dom planningPROB1) =
DRESTRICT s (FDOM s DIFF planning_prob_dom planningPROB1)` by 
(SRW_TAC[SatisfySimps.SATISFY_ss][planning_prob_dom_def]
THEN MATCH_MP_TAC (exec_valid_as_disjoint |> Q.GEN `PROB` |> Q.SPEC `planningPROB1.PROB`)
THEN SRW_TAC[][]
THEN METIS_TAC[disj_diff])
THEN METIS_TAC[DISJOINT_SYM, submap_drest_submap])
THEN MATCH_MP_TAC(submap_drestrict_diff)
THEN Q.EXISTS_TAC `planning_prob_dom planningPROB1`
THEN Q.EXISTS_TAC `FDOM s UNION (planning_prob_dom planningPROB1)`
THEN SRW_TAC[][]
THEN1 METIS_TAC[submap_subset, subset_imp_subset_union]
THEN1 METIS_TAC[exec_plan_fdom_subset, planning_prob_dom_def]
THEN1 SRW_TAC[][DIFF_SAME_UNION])

val precede_preserve_needed_asses' = store_thm("precede_preserve_needed_asses'",
``!planningPROB1 as s. precede planningPROB1 planningPROB2 /\ planningPROB1.G SUBMAP (exec_plan(s,as)) /\ as IN valid_plans planningPROB1.PROB /\ (needed_asses planningPROB2) SUBMAP s /\ planning_problem planningPROB1
==> (needed_asses planningPROB2) SUBMAP (exec_plan(s,as))``,
SRW_TAC[][precede_def, plan_def]
THEN SRW_TAC[][]
THEN `(DRESTRICT (needed_asses planningPROB2) ((planning_prob_dom planningPROB1)))
            SUBMAP exec_plan (s,as)` by
     (`DRESTRICT planningPROB1.G (needed_vars planningPROB2) SUBMAP exec_plan (s,as)` by
          METIS_TAC[sat_precond_as_proj_4]
       THEN METIS_TAC[SUBMAP_ANTISYM, SUBMAP_TRANS])
THEN `DRESTRICT (needed_asses planningPROB2) (FDOM s DIFF (planning_prob_dom planningPROB1))
      SUBMAP exec_plan (s,as)` by
      (MATCH_MP_TAC sublist_as_proj_eq_as_1
       THEN Q.EXISTS_TAC `(FDOM s DIFF planning_prob_dom planningPROB1)`
       THEN `DRESTRICT (exec_plan (s,as))
           (FDOM s DIFF planning_prob_dom planningPROB1) =
              DRESTRICT s (FDOM s DIFF planning_prob_dom planningPROB1)` by 
              (SRW_TAC[SatisfySimps.SATISFY_ss][planning_prob_dom_def]
               THEN MATCH_MP_TAC (exec_valid_as_disjoint |> Q.GEN `PROB` |> Q.SPEC `planningPROB1.PROB`)
               THEN SRW_TAC[][]
               THEN METIS_TAC[disj_diff])
       THEN METIS_TAC[DISJOINT_SYM, submap_drest_submap])
THEN MATCH_MP_TAC(submap_drestrict_diff)
THEN Q.EXISTS_TAC `planning_prob_dom planningPROB1`
THEN Q.EXISTS_TAC `FDOM s UNION (planning_prob_dom planningPROB1)`
THEN SRW_TAC[][]
THEN1 METIS_TAC[submap_subset, subset_imp_subset_union]
THEN1 METIS_TAC[exec_plan_fdom_subset, planning_prob_dom_def]
THEN1 SRW_TAC[][DIFF_SAME_UNION])

val planning_prob_union_def = Define`planning_prob_union planningPROB1 planningPROB2 = <|I := (FUNION (planningPROB1.I)  (planningPROB2.I)); PROB := planningPROB1.PROB UNION (planningPROB2.PROB); G := FUNION (planningPROB1.G)  (planningPROB2.G)|>`

val planningPROBEMPTY_def = Define`planningPROBEMPTY = <|I := FEMPTY; PROB := EMPTY; G := FEMPTY|>`

val prob_list_union_def = Define`prob_list_union planningPROBList = FOLDR planning_prob_union planningPROBEMPTY planningPROBList`

val prob_list_union_goal = store_thm("prob_list_union_goal",
``(prob_list_union planningPROBList).G = FOLDR FUNION FEMPTY ((MAP (\planningPROB. planningPROB.G)) planningPROBList)``,
Induct_on `planningPROBList`
THEN SRW_TAC[][prob_list_union_def, planningPROBEMPTY_def]
THEN SRW_TAC[][planning_prob_union_def, FUNION_FEMPTY_1, GSYM prob_list_union_def, GSYM planningPROBEMPTY_def])

val planning_prob_goals_submap_list_union_goal_submap = store_thm("planning_prob_goals_submap_list_union_goal_submap",
``(!planningPROB.
     MEM planningPROB planningPROBList ==>
     planningPROB.G SUBMAP s) ==>
  (prob_list_union planningPROBList).G SUBMAP s``,
SRW_TAC[][prob_list_union_goal]
THEN MATCH_MP_TAC union_of_submaps_is_submap
THEN SRW_TAC[][]
THEN METIS_TAC[MEM_MAP])

val prob_list_union_init = store_thm("prob_list_union_init",
``(prob_list_union planningPROBList).I = FOLDR FUNION FEMPTY ((MAP (\planningPROB. planningPROB.I)) planningPROBList)``,
Induct_on `planningPROBList`
THEN SRW_TAC[][prob_list_union_def, planningPROBEMPTY_def]
THEN SRW_TAC[][planning_prob_union_def, FUNION_FEMPTY_1, GSYM prob_list_union_def, GSYM planningPROBEMPTY_def])

val init_union_submap = store_thm("init_union_submap",
``planningPROB1.I SUBMAP s /\ planningPROB2.I SUBMAP s
 ==> (planning_prob_union planningPROB1 planningPROB2).I SUBMAP s``,
SRW_TAC[][needed_asses_def, needed_vars_def, planning_prob_union_def, EXTENSION, FUNION_DEF, SUBMAP_DEF, DRESTRICT_DEF, UNION_DEF, SUBSET_DEF]
THEN SRW_TAC[][]
THEN METIS_TAC[])

val prob_list_union_init = store_thm("prob_list_union_init",
``(prob_list_union planningPROBList).I = FOLDR FUNION FEMPTY ((MAP (\planningPROB. planningPROB.I)) planningPROBList)``,
Induct_on `planningPROBList`
THEN SRW_TAC[][prob_list_union_def, planningPROBEMPTY_def]
THEN SRW_TAC[][planning_prob_union_def, FUNION_FEMPTY_1, GSYM prob_list_union_def, GSYM planningPROBEMPTY_def])

val planning_prob_init_submap_list_union_goal_submap = store_thm("planning_prob_init_submap_list_union_goal_submap",
``(!planningPROB.
     MEM planningPROB planningPROBList ==>
     planningPROB.I SUBMAP s) ==>
  (prob_list_union planningPROBList).I SUBMAP s``,
SRW_TAC[][prob_list_union_init]
THEN MATCH_MP_TAC union_of_submaps_is_submap
THEN SRW_TAC[][]
THEN METIS_TAC[MEM_MAP])

val needed_asses_submap_init = store_thm("needed_asses_submap_init",
``needed_asses planningPROB SUBMAP (planningPROB.I)``,
SRW_TAC[][needed_asses_def])

val planning_prob_union_planning_prob = store_thm("planning_prob_union_planning_prob",
``planning_problem planningPROB1 /\ planning_problem planningPROB2 ==> 
  planning_problem(planning_prob_union planningPROB1 planningPROB2)``,
SRW_TAC[][planning_problem_def, planning_prob_union_def, valid_states_def, prob_dom_def, action_dom_pair, IMAGE_DEF, UNION_DEF, EXTENSION, SUBSET_DEF, planning_prob_dom_def]
THEN1(EQ_TAC
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN METIS_TAC[])

val planning_problem_list_union_is_planning_problem = store_thm("planning_problem_list_union_is_planning_problem",
``(!planningPROB. MEM planningPROB planningPROBList ==> planning_problem planningPROB)
  ==> planning_problem (prob_list_union planningPROBList)``,
Induct_on `planningPROBList`
THEN1 SRW_TAC[][planning_problem_def, prob_list_union_def, valid_states_def, planningPROBEMPTY_def, prob_dom_def]
THEN SRW_TAC[][prob_list_union_def]
THEN SRW_TAC[][GSYM prob_list_union_def]
THEN METIS_TAC[planning_prob_union_planning_prob])

val planning_prob_union_needed_vars_1 = store_thm("planning_prob_union_needed_vars_1",
``planning_problem planningPROB1 /\ planning_problem planningPROB2 /\ planningPROB1.I SUBMAP s /\ planningPROB2.I SUBMAP s
  ==> needed_vars (planning_prob_union planningPROB1 planningPROB2) SUBSET (needed_vars planningPROB1) UNION (needed_vars planningPROB2)``,
SRW_TAC[][DRESTRICT_DEF, FUNION_DEF, needed_vars_def, fmap_EXT, INTER_DEF, EXTENSION, needed_asses_def, planning_prob_dom_def, prob_dom_def, IMAGE_DEF, action_dom_pair, EXTENSION, SUBSET_DEF, planning_problem_def, valid_states_def, SUBMAP_DEF, planning_prob_union_def]
THEN1 METIS_TAC[]
THEN ASSUME_TAC (SIMP_RULE(srw_ss())[UNION_DEF, EXTENSION] action_dom_pair |> INST_TYPE [gamma |-> ``:'b``])
THEN METIS_TAC[])

val planning_prob_union_needed_vars_1' = store_thm("planning_prob_union_needed_vars_1'",
``planning_problem planningPROB1 /\ planning_problem planningPROB2 /\ agree planningPROB1.I planningPROB2.I
  ==> needed_vars (planning_prob_union planningPROB1 planningPROB2) SUBSET (needed_vars planningPROB1) UNION (needed_vars planningPROB2)``,
SRW_TAC[][agree_def, needed_asses_def, DRESTRICT_DEF, FUNION_DEF, needed_vars_def, fmap_EXT, INTER_DEF, EXTENSION, needed_asses_def, planning_prob_dom_def, prob_dom_def, IMAGE_DEF, action_dom_pair, EXTENSION, SUBSET_DEF, planning_problem_def, valid_states_def, SUBMAP_DEF, planning_prob_union_def]
THEN1 METIS_TAC[]
THEN ASSUME_TAC (SIMP_RULE(srw_ss())[UNION_DEF, EXTENSION] action_dom_pair |> INST_TYPE [gamma |-> ``:'b``])
THEN METIS_TAC[])
         
val planning_prob_union_needed_vars_2 = store_thm("planning_prob_union_needed_vars_2",
``planning_problem planningPROB1 /\ planning_problem planningPROB2 /\ planningPROB1.I SUBMAP s  /\ planningPROB2.I SUBMAP s /\
  precede planningPROB1 planningPROB2 /\ precede planningPROB2 planningPROB1
  ==> ((needed_vars planningPROB1) UNION (needed_vars planningPROB2) SUBSET needed_vars (planning_prob_union planningPROB1 planningPROB2))``,
SRW_TAC[][DRESTRICT_DEF, FUNION_DEF, needed_vars_def, fmap_EXT, INTER_DEF, EXTENSION, needed_asses_def, planning_prob_dom_def, prob_dom_def, IMAGE_DEF, action_dom_pair, EXTENSION, SUBSET_DEF, planning_problem_def, valid_states_def, SUBMAP_DEF, planning_prob_union_def, precede_def]
THEN1 METIS_TAC[]
THEN1 METIS_TAC[]
THEN1 METIS_TAC[]
THEN Cases_on `(x ∈ FDOM planningPROB1.G ∨ x ∈ FDOM planningPROB2.G) ∧
                (s ' x =
                    if x ∈ FDOM planningPROB1.G then planningPROB1.G ' x
                   else planningPROB2.G ' x)`
THEN1 METIS_TAC[]
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN METIS_TAC[])

val planning_prob_union_needed_vars_2'' = store_thm("planning_prob_union_needed_vars_2''",
``planning_problem planningPROB1 /\ planning_problem planningPROB2 /\ agree planningPROB1.I planningPROB2.I /\ agree planningPROB1.G planningPROB2.G
  ==> ((needed_vars planningPROB1) UNION (needed_vars planningPROB2) SUBSET needed_vars (planning_prob_union planningPROB1 planningPROB2))``,
SRW_TAC[][planning_prob_union_def, agree_def]
THEN FULL_SIMP_TAC(bool_ss)[DRESTRICT_DEF, FUNION_DEF, needed_vars_def, fmap_EXT, INTER_DEF, EXTENSION, planning_prob_dom_def, prob_dom_def, IMAGE_DEF, action_dom_pair, EXTENSION, SUBSET_DEF, planning_problem_def, valid_states_def, SUBMAP_DEF, precede_def]
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[needed_asses_def, DRESTRICT_DEF, FUNION_DEF, needed_vars_def, fmap_EXT, INTER_DEF, EXTENSION, planning_prob_dom_def, prob_dom_def, IMAGE_DEF, action_dom_pair, EXTENSION, SUBSET_DEF, planning_problem_def, valid_states_def, SUBMAP_DEF, precede_def]
THEN1(FULL_SIMP_TAC(srw_ss())[DRESTRICT_DEF, FUNION_DEF, needed_vars_def, fmap_EXT, INTER_DEF, EXTENSION, planning_prob_dom_def, prob_dom_def, IMAGE_DEF, action_dom_pair, EXTENSION, SUBSET_DEF, planning_problem_def, valid_states_def, SUBMAP_DEF, precede_def]
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN1(FULL_SIMP_TAC(srw_ss())[DRESTRICT_DEF, FUNION_DEF, needed_vars_def, fmap_EXT, INTER_DEF, EXTENSION, planning_prob_dom_def, prob_dom_def, IMAGE_DEF, action_dom_pair, EXTENSION, SUBSET_DEF, planning_problem_def, valid_states_def, SUBMAP_DEF, precede_def]
      THEN SRW_TAC[][]
      THEN METIS_TAC[]))

val planning_prob_union_needed_vars = store_thm("planning_prob_union_needed_vars",
``planning_problem planningPROB1 /\ planning_problem planningPROB2 /\ planningPROB1.I SUBMAP s /\ planningPROB2.I SUBMAP s /\
  precede planningPROB1 planningPROB2 /\ precede planningPROB2 planningPROB1
  ==> ((needed_vars planningPROB1) UNION (needed_vars planningPROB2) = needed_vars (planning_prob_union planningPROB1 planningPROB2))``,
METIS_TAC[planning_prob_union_needed_vars_1, planning_prob_union_needed_vars_2, SET_EQ_SUBSET])

val planning_prob_union_needed_vars' = store_thm("planning_prob_union_needed_vars'",
``planning_problem planningPROB1 /\ planning_problem planningPROB2 /\ agree planningPROB1.I planningPROB2.I /\ agree planningPROB1.G planningPROB2.G
  ==> ((needed_vars planningPROB1) UNION (needed_vars planningPROB2) = needed_vars (planning_prob_union planningPROB1 planningPROB2))``,
METIS_TAC[planning_prob_union_needed_vars_1', planning_prob_union_needed_vars_2'', SET_EQ_SUBSET])

val planning_prob_dom_union = store_thm("planning_prob_dom_union",
``(planning_prob_dom
          (planning_prob_union planningPROB1 planningPROB2)) = planning_prob_dom planningPROB1 UNION planning_prob_dom planningPROB2``,
SRW_TAC[][planning_prob_dom_def, planning_prob_union_def, prob_dom_def])

val needed_vars_subset = store_thm("needed_vars_subset",
``needed_vars planningPROB ⊆ FDOM planningPROB.I``,
SRW_TAC[][needed_vars_def, SUBSET_DEF])

val precede_planning_prob_union = store_thm("precede_planning_prob_union",
``planning_problem planningPROB /\ planning_problem planningPROB1 /\ planning_problem planningPROB2 /\ planningPROB1.I SUBMAP s  /\ planningPROB2.I SUBMAP s /\
  precede planningPROB planningPROB1 /\ precede planningPROB planningPROB2 /\
  precede planningPROB2 planningPROB1 /\ precede planningPROB1 planningPROB2 ==> 
     precede planningPROB (planning_prob_union planningPROB1 planningPROB2)``,
SRW_TAC[][]
THEN MP_TAC (GSYM planning_prob_union_needed_vars)
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[precede_def, needed_asses_def]
THEN SRW_TAC[][]
THEN1(SRW_TAC[][planning_prob_union_def]
      THEN MATCH_MP_TAC(SUBMAP_FUNION_DRESTRICT)
      THEN SRW_TAC[][]
      THEN METIS_TAC[needed_vars_subset, planning_problem_def])
THEN1(SRW_TAC[][planning_prob_dom_union, planning_prob_union_def]
      THEN MATCH_MP_TAC UNION_FUNION_DRESTRICT
      THEN SRW_TAC[][]
      THEN METIS_TAC[]))

val precede_planning_prob_union' = store_thm("precede_planning_prob_union'",
``planning_problem planningPROB /\ planning_problem planningPROB1 /\ planning_problem planningPROB2 /\ agree planningPROB1.I planningPROB2.I /\
  agree planningPROB1.G planningPROB2.G /\
  precede planningPROB planningPROB1 /\ precede planningPROB planningPROB2 ==> 
     precede planningPROB (planning_prob_union planningPROB1 planningPROB2)``,
SRW_TAC[][]
THEN MP_TAC (GSYM planning_prob_union_needed_vars')
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[precede_def, needed_asses_def]
THEN SRW_TAC[][]
THEN1(SRW_TAC[][planning_prob_union_def]
      THEN MATCH_MP_TAC(SUBMAP_FUNION_DRESTRICT')
      THEN SRW_TAC[][]
      THEN METIS_TAC[needed_vars_subset, planning_problem_def])
THEN1(SRW_TAC[][planning_prob_dom_union, planning_prob_union_def]
      THEN MATCH_MP_TAC UNION_FUNION_DRESTRICT
      THEN SRW_TAC[][]
      THEN METIS_TAC[]))

val planning_prob_union_precede = store_thm("planning_prob_union_precede",
`` precede planningPROB1 planningPROB /\ precede planningPROB2 planningPROB ==> 
     precede (planning_prob_union planningPROB1 planningPROB2) planningPROB``,
SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[precede_def, needed_asses_def]
THEN SRW_TAC[][]
THEN SRW_TAC[][planning_prob_union_def]
THEN SRW_TAC[][GSYM planning_prob_union_def]
THEN SRW_TAC[][planning_prob_dom_union, planning_prob_union_def]
THEN REWRITE_TAC[DRESTRICTED_FUNION_2, GSYM DRESTRICT_DRESTRICT]
THEN MATCH_MP_TAC FUNION_DRESTRICT_UNION
THEN SRW_TAC[][])

val inv_precede_prob_list_def = 
Define`(inv_precede_prob_list (planningPROB::planningPROBList) = (!planningPROB'. MEM planningPROB' planningPROBList ==>  precede planningPROB'  planningPROB) /\ inv_precede_prob_list (planningPROBList)) /\
       (inv_precede_prob_list [] = T)`

val inv_precede_prob_list_union = store_thm("precede_prob_list_union",
``!planningPROB. (!planningPROB'. MEM planningPROB' planningPROBList ==> precede planningPROB' planningPROB)
  ==> precede (prob_list_union planningPROBList) planningPROB``,
Induct_on `planningPROBList`
THEN1 SRW_TAC[][prob_list_union_def, precede_def, planningPROBEMPTY_def, planning_prob_dom_def, prob_dom_def, needed_vars_def, IMAGE_DEF, action_dom_pair, needed_asses_def, DRESTRICT_IS_FEMPTY]
THEN SRW_TAC[][prob_list_union_def]
THEN SRW_TAC[][GSYM prob_list_union_def]
THEN METIS_TAC[planning_prob_union_precede])

val precede_prob_list_union = store_thm("precede_prob_list_union",
``precede_prob_list planningPROBList /\ inv_precede_prob_list planningPROBList ==> !planningPROB. planning_problem planningPROB /\
 (!planningPROB'. MEM planningPROB' planningPROBList ==> planning_problem planningPROB' /\ precede planningPROB planningPROB' /\ planningPROB'.I SUBMAP s)
  ==> precede planningPROB (prob_list_union planningPROBList)``,
Induct_on `planningPROBList`
THEN1 SRW_TAC[][prob_list_union_def, precede_def, planningPROBEMPTY_def, planning_prob_dom_def, prob_dom_def, needed_vars_def, IMAGE_DEF, action_dom_pair, needed_asses_def, DRESTRICT_IS_FEMPTY]
THEN SRW_TAC[][prob_list_union_def]
THEN SRW_TAC[][GSYM prob_list_union_def]
THEN MATCH_MP_TAC precede_planning_prob_union
THEN SRW_TAC[][]
THEN1 METIS_TAC[planning_problem_list_union_is_planning_problem]
THEN1 METIS_TAC[planning_prob_init_submap_list_union_goal_submap]
THEN1 METIS_TAC[precede_prob_list_cons, inv_precede_prob_list_def]
THEN1 METIS_TAC[inv_precede_prob_list_union, precede_prob_list_cons, inv_precede_prob_list_def]
THEN1 FULL_SIMP_TAC(srw_ss())[precede_prob_list_def, inv_precede_prob_list_def])

val precede_prob_list_union' = store_thm("precede_prob_list_union'",
``!planningPROB.
     (planning_problem planningPROB /\
          (!planningPROB'. MEM planningPROB' planningPROBList ==>
                           planning_problem planningPROB' /\ 
                           precede planningPROB planningPROB') /\
          (!planningPROB' planningPROB''. 
                   MEM planningPROB' planningPROBList /\
                   MEM planningPROB'' planningPROBList ==>
                   (agree planningPROB'.I planningPROB''.I /\ agree planningPROB'.G planningPROB''.G))
       ==> precede planningPROB (prob_list_union planningPROBList))``,
Induct_on `planningPROBList`
THEN1 SRW_TAC[][prob_list_union_def, precede_def, planningPROBEMPTY_def, planning_prob_dom_def, prob_dom_def, needed_vars_def, IMAGE_DEF, action_dom_pair, needed_asses_def, DRESTRICT_IS_FEMPTY]
THEN SRW_TAC[][prob_list_union_def]
THEN SRW_TAC[][GSYM prob_list_union_def]
THEN MATCH_MP_TAC precede_planning_prob_union'
THEN SRW_TAC[][]
THEN1 METIS_TAC[planning_problem_list_union_is_planning_problem]
THEN1(SRW_TAC[][prob_list_union_init]
      THEN MATCH_MP_TAC agree_fm_list_union
      THEN SRW_TAC[][]
      THEN METIS_TAC[MEM_MAP])
THEN1(SRW_TAC[][prob_list_union_goal]
      THEN MATCH_MP_TAC agree_fm_list_union
      THEN SRW_TAC[][]
      THEN METIS_TAC[MEM_MAP]))

val state_with_needed_asses_eq_init_1 = store_thm("state_with_needed_asses_eq_init_1",
``planning_problem planningPROB /\ needed_asses planningPROB SUBMAP s /\ sat_precond_as(needed_asses planningPROB, as) ==>
   (plan planningPROB as ==> (planningPROB.G) SUBMAP (exec_plan(s,as)))``,
SRW_TAC[][]
THEN `planningPROB.G SUBMAP exec_plan (needed_asses planningPROB ,as)` by
     METIS_TAC[plan_works_from_needed_asses]
THEN `exec_plan (needed_asses planningPROB,as) SUBMAP exec_plan (s,as)` by
      METIS_TAC[submap_init_submap_exec]
THEN METIS_TAC[SUBMAP_TRANS])

val inv_precede_list_cons = store_thm("inv_precede_list_cons",
``inv_precede_prob_list (h::planningPROBList) ==> inv_precede_prob_list (planningPROBList)``,
SRW_TAC[][inv_precede_prob_list_def])

val planning_prob_union_needed_asses = store_thm("planning_prob_union_needed_vars",
``planning_problem planningPROB1 ∧ planning_problem planningPROB2 ∧
  agree planningPROB1.I planningPROB2.I /\ agree planningPROB1.G planningPROB2.G /\
  needed_asses planningPROB1 SUBMAP s /\ needed_asses planningPROB2 SUBMAP s ==> 
  needed_asses (planning_prob_union planningPROB1 planningPROB2) SUBMAP s``,
SRW_TAC[][needed_asses_def, GSYM planning_prob_union_needed_vars', planning_prob_union_def]
THEN METIS_TAC[UNION_FUNION_DRESTRICT_SUBMAP, needed_vars_subset])

val prob_needed_asses_submap_list_union_submap = store_thm("prob_needed_asses_submap_list_union_submap",
``(!planningPROB' planningPROB''.
          MEM planningPROB' planningPROBList /\
          MEM planningPROB'' planningPROBList ==> 
              (agree planningPROB'.I planningPROB''.I /\
               agree planningPROB'.G planningPROB''.G)) ==> 
  (!planningPROB.
       MEM planningPROB planningPROBList ==> 
         (needed_asses planningPROB SUBMAP s /\
          planning_problem planningPROB)) ==> 
       needed_asses (prob_list_union planningPROBList) ⊑ s``,
Induct_on `planningPROBList`
THEN1 SRW_TAC[][prob_list_union_def, planningPROBEMPTY_def, needed_asses_def, needed_vars_def, SUBMAP_FEMPTY]
THEN SRW_TAC[][prob_list_union_def]
THEN SRW_TAC[][GSYM prob_list_union_def]
THEN MATCH_MP_TAC planning_prob_union_needed_asses
THEN SRW_TAC[][planning_problem_list_union_is_planning_problem]
THEN1 METIS_TAC[agree_fm_list_union, prob_list_union_init, MEM_MAP]
THEN1 METIS_TAC[agree_fm_list_union, prob_list_union_goal, MEM_MAP])

val valid_action_valid_in_plan_list_union = store_thm("valid_plan_valid_in_plan_list_union",
``!planningPROB. MEM planningPROB planningPROBList /\ a IN planningPROB.PROB 
   ==> a IN (prob_list_union planningPROBList).PROB``,
Induct_on `planningPROBList`
THEN SRW_TAC[][prob_list_union_def]
THEN SRW_TAC[][GSYM prob_list_union_def, planning_prob_union_def]
THEN METIS_TAC[])

val mems_init_goal_agree_mems_solvable_imp_append_solves_all = store_thm("mems_init_goal_agree_mems_solvable_imp_append_solves_all",
``precede_prob_list planningPROBList /\
  (!planningPROB' planningPROB''.
          MEM planningPROB' planningPROBList /\
          MEM planningPROB'' planningPROBList ==> 
              (agree planningPROB'.I planningPROB''.I /\
               agree planningPROB'.G planningPROB''.G)) ==> 
  !s. (!planningPROB.
             MEM planningPROB planningPROBList ==>
                (plan planningPROB (f planningPROB) /\
                 planning_problem planningPROB /\
                 needed_asses planningPROB SUBMAP s /\
                 sat_precond_as(needed_asses planningPROB, f planningPROB) ))
      ==> (!planningPROB.
               MEM planningPROB planningPROBList ==> 
                  (planningPROB.G) SUBMAP (exec_plan(s, FLAT (MAP f (planningPROBList)))))``,
Induct_on `planningPROBList`
THEN SRW_TAC[][]
THEN SRW_TAC[][precede_prob_list_def, exec_plan_Append]
THEN `(!planningPROB.
             MEM planningPROB planningPROBList ==>
             needed_asses planningPROB SUBMAP exec_plan (s: ('a |-> 'b),(f: ((('a, 'b) planningProblem) -> (('a |-> 'b)#('a |-> 'b)) list)) h))` by
           (SRW_TAC[][]
            THEN FULL_SIMP_TAC(srw_ss())[precede_prob_list_def]
            THEN MATCH_MP_TAC precede_preserve_needed_asses'
            THEN Q.EXISTS_TAC `h`
            THEN SRW_TAC[][]
	    THEN1 METIS_TAC[state_with_needed_asses_eq_init_1]
            THEN1 METIS_TAC[plan_def])
THEN LAST_X_ASSUM (MP_TAC o Q.SPEC `exec_plan (s, (f :('a, 'b) planningProblem -> (('a |-> 'b) # ('a |-> 'b)) list) (h:('a, 'b) planningProblem))` o CONV_RULE(RIGHT_IMP_FORALL_CONV))
THEN SRW_TAC[SatisfySimps.SATISFY_ss][precede_prob_list_cons, inv_precede_list_cons]
THEN `(prob_list_union planningPROBList).G SUBMAP exec_plan (exec_plan (s,f h),FLAT (MAP f planningPROBList))` by 
       METIS_TAC[planning_prob_goals_submap_list_union_goal_submap]
THEN FULL_SIMP_TAC(srw_ss())[precede_prob_list_def]
THEN `needed_asses (prob_list_union planningPROBList)
      SUBMAP (exec_plan (s,f h))` by
     (MATCH_MP_TAC precede_preserve_needed_asses'
      THEN Q.EXISTS_TAC `h`
      THEN SRW_TAC[][]
      THEN1 METIS_TAC[precede_prob_list_union']
      THEN1 METIS_TAC[state_with_needed_asses_eq_init_1, plan_def]
      THEN1 METIS_TAC[plan_def]
      THEN1 METIS_TAC[prob_needed_asses_submap_list_union_submap])
THEN MATCH_MP_TAC  precede_preserve_goals_gen'
THEN Q.EXISTS_TAC `(prob_list_union planningPROBList)`
THEN SRW_TAC[][]
THEN1 METIS_TAC[precede_prob_list_union']
THEN1(SRW_TAC[][SUBSET_DEF, valid_plans_def]
      THEN MATCH_MP_TAC valid_action_valid_in_plan_list_union
      THEN FULL_SIMP_TAC(srw_ss())[MEM_FLAT, MEM_MAP]
      THEN Q.EXISTS_TAC `y`
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[plan_def, valid_plans_def, SUBSET_DEF])
THEN1 METIS_TAC[planning_problem_list_union_is_planning_problem]
THEN METIS_TAC[state_with_needed_asses_eq_init_1])

val precede_prob_list_members = store_thm("precede_prob_list_members",
``precede_prob_list planningPROBList /\ MEM planningPROB1 planningPROBList /\ MEM planningPROB2 planningPROBList /\ planningPROB1 <> planningPROB2 ==> 
(precede planningPROB1 planningPROB2 \/ precede planningPROB2 planningPROB1)``,
Induct_on `planningPROBList`
THEN SRW_TAC[][precede_prob_list_def]
THEN METIS_TAC[])

(*Removing the condition that member prob goals have to agree*)
val mems_init_agree_mems_solvable_imp_append_solves_all = store_thm("mems_init_agree_mems_solvable_imp_append_solves_all",
``precede_prob_list planningPROBList /\
  (!planningPROB' planningPROB''.
          MEM planningPROB' planningPROBList /\
          MEM planningPROB'' planningPROBList ==> 
              (agree planningPROB'.I planningPROB''.I)) ==> 
  !s. (!planningPROB.
             MEM planningPROB planningPROBList ==>
                (plan planningPROB (f planningPROB) /\
                 planning_problem planningPROB /\
                 needed_asses planningPROB SUBMAP s /\
                 sat_precond_as(needed_asses planningPROB, f planningPROB) ))
      ==> (!planningPROB.
               MEM planningPROB planningPROBList ==> 
                  (planningPROB.G) SUBMAP (exec_plan(s, FLAT (MAP f (planningPROBList)))))``,
SRW_TAC[][]
THEN MATCH_MP_TAC (SIMP_RULE(srw_ss())[AND_IMP_INTRO, PULL_FORALL] mems_init_goal_agree_mems_solvable_imp_append_solves_all)
THEN SRW_TAC[][]
THEN Cases_on `planningPROB'' = planningPROB'''`
THEN1 SRW_TAC[][agree_def]
THEN `precede planningPROB'' planningPROB''' \/ precede planningPROB''' planningPROB''` by
       METIS_TAC[precede_prob_list_members]
THEN FULL_SIMP_TAC(srw_ss())[planning_problem_def, planning_prob_dom_def, valid_states_def, precede_def]
THEN METIS_TAC[DRESTRICT_EQ_AGREE])

val IJCAI_Lemma_2 = store_thm("IJCAI_Lemma_2",
``precede_prob_list planningPROBList ==> 
  !s. (!planningPROB.
             MEM planningPROB planningPROBList ==>
                (planning_problem planningPROB /\
                 planningPROB.I SUBMAP s /\
                 plan planningPROB (f planningPROB) /\
                 sat_precond_as(needed_asses planningPROB,
                                f planningPROB)))
      ==> (!planningPROB.
               MEM planningPROB planningPROBList ==> 
                  (planningPROB.G) SUBMAP (exec_plan(s, FLAT (MAP f (planningPROBList)))))``,
SRW_TAC[][]
THEN MATCH_MP_TAC (SIMP_RULE(srw_ss())[AND_IMP_INTRO, PULL_FORALL] mems_init_agree_mems_solvable_imp_append_solves_all)
THEN SRW_TAC[][]
THEN1 METIS_TAC[SUBMAPS_AGREE]
THEN1 METIS_TAC[SUBMAP_TRANS, needed_asses_submap_init])

val IJCAI_Lemma_2_ITP = store_thm("IJCAI_Lemma_2_ITP",
``precede_prob_list planningPROBList ==> 
  (!planningPROB.
             MEM planningPROB planningPROBList ==>
                (planning_problem planningPROB /\
                 planningPROB.I SUBMAP s /\
                 plan planningPROB (f planningPROB) /\
                 sat_precond_as(needed_asses planningPROB,
                                f planningPROB))) ==> 
   (let subProbPlans = (MAP f (planningPROBList));
        concatenatedPlans = FLAT subProbPlans in
        (!planningPROB. MEM planningPROB planningPROBList ==>
                         (planningPROB.G) SUBMAP (exec_plan(s, concatenatedPlans))))``,
SRW_TAC[][LET_DEF, IJCAI_Lemma_2])
			     
(*******************************************************)

val subprob_def = Define`subprob planningPROB1 planningPROB2 = planningPROB1.I SUBMAP planningPROB2.I /\ planningPROB1.PROB SUBSET planningPROB2.PROB`

val covers_def = Define`covers planningPROBList planningPROB =
                         (!x. x IN FDOM (planningPROB.G) ==> ?planningPROB'. MEM planningPROB' planningPROBList /\ x IN FDOM planningPROB'.G /\ (planningPROB.G ' x = planningPROB'.G ' x)) /\
                         (!planningPROB'. MEM planningPROB' planningPROBList ==> subprob planningPROB' planningPROB)`


(*Proving the theorem for a general state that includes the initial state.*)

val IJCAI_Theorem_3_step_1 = store_thm("IJCAI_Theorem_3_step_1",
``covers planningPROBList planningPROB /\
  precede_prob_list planningPROBList
  ==> !s. (!planningPROB.
               MEM planningPROB planningPROBList
               ==> (planning_problem planningPROB /\
                    planningPROB.I SUBMAP s /\
                    plan planningPROB (f planningPROB) /\
                    sat_precond_as(needed_asses planningPROB,
                                   f planningPROB)))
      ==> plan planningPROB (FLAT (MAP f (planningPROBList)))``,
SRW_TAC[][plan_def]
THEN1(FULL_SIMP_TAC(srw_ss())[valid_plans_def, SUBSET_DEF, covers_def, subprob_def]
      THEN METIS_TAC[MEM_MAP, MEM_FLAT])
THEN1(MP_TAC IJCAI_Lemma_2
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[covers_def, subprob_def, SUBMAP_DEF, agree_def, plan_def]
      THEN SRW_TAC[][]
      THEN1 METIS_TAC[]
      THEN1 METIS_TAC[]))

val IJCAI_Theorem_3 = store_thm("IJCAI_Theorem_3",
``covers planningPROBList planningPROB/\
  precede_prob_list planningPROBList ==> 
  (!planningPROB.
             MEM planningPROB planningPROBList ==>
                (planning_problem planningPROB /\
                 plan planningPROB (f planningPROB) /\
                 sat_precond_as(needed_asses planningPROB,
                                f planningPROB)))
      ==> plan planningPROB (FLAT (MAP f (planningPROBList)))``,
SRW_TAC[][]
THEN MATCH_MP_TAC (SIMP_RULE(srw_ss())[AND_IMP_INTRO, PULL_FORALL] IJCAI_Theorem_3_step_1)
THEN Q.EXISTS_TAC `planningPROB.I`
THEN SRW_TAC[][]
THEN METIS_TAC[covers_def, subprob_def])

val IJCAI_Theorem_3' = store_thm("IJCAI_Theorem_3'",
``covers planningPROBList planningPROB/\
  precede_prob_list planningPROBList ==> 
  (!planningPROB.
             MEM planningPROB planningPROBList ==>
                (planning_problem planningPROB /\
                 plan planningPROB (f planningPROB)))
      ==> plan planningPROB (FLAT (MAP (\planningPROB'. rem_condless_act(needed_asses planningPROB', [], f planningPROB') ) (planningPROBList)))``,
SRW_TAC[][]
THEN MATCH_MP_TAC (SIMP_RULE(srw_ss())[AND_IMP_INTRO, PULL_FORALL] IJCAI_Theorem_3)
THEN SRW_TAC[][]
THEN1(SRW_TAC[][plan_def]
      THEN1 METIS_TAC[rem_condless_valid_10, plan_def]
      THEN1 METIS_TAC[rem_condless_valid_2, submap_init_submap_exec,
                      needed_asses_submap_init,
                      rem_condless_valid_1, plan_works_from_needed_asses,
                      SUBMAP_TRANS])
THEN1 METIS_TAC[rem_condless_valid_2])

val IJCAI_Theorem_3_ITP = store_thm("IJCAI_Theorem_3_ITP",
``covers planningPROBList planningPROB /\
  precede_prob_list planningPROBList ==> 
  (!planningPROB.
             MEM planningPROB planningPROBList ==>
                (planning_problem planningPROB /\
                 plan planningPROB (f planningPROB))) ==>
   (let inst_plans = (MAP (\planningPROB'. rem_condless_act(needed_asses planningPROB', [], f planningPROB') ) (planningPROBList));
        concatenated_plans = FLAT inst_plans in
      plan planningPROB concatenated_plans)``,
SRW_TAC[][IJCAI_Theorem_3', LET_DEF])


val _ = export_theory();
