open HolKernel Parse boolLib bossLib finite_mapTheory pred_setTheory lcsymtacs;
open systemAbstractionTheory
open set_utilsTheory
open utilsTheory
open functionsLib
open factoredSystemTheory
open set_utilsTheory
open fmap_utilsTheory
open stateSpaceProductTheory;

val _ = new_theory "invariantsPlusOne";

val invariant_def = Define `invariant(fm:('a |-> bool)) =
                                   (!x:'a. x IN (FDOM fm) /\ (fm ' x ) 
                                           ==> (!x':'a. x <> x' /\ x' IN (FDOM fm)==> ~(fm ' x')))`

val invariant_card_state_set = store_thm("invariant_card_state_set",
``!fm:('a |-> bool) fm':('a |-> bool) x:'a x':'a.
                         invariant(fm) /\ invariant(fm') /\ (FDOM fm = FDOM fm') 
                         /\ x IN FDOM fm /\ fm <> fm' /\ (fm ' x) 
                         ==> ~ (fm' ' x)``,
SRW_TAC[][invariant_def, IN_DEF, fmap_EXT]
THEN METIS_TAC[])

val invariant_not_eq_last_diff_paths = store_thm("invariant_not_eq_last_diff_paths",
``!fm:('a |-> bool) x:'a s:'a -> bool.
      invariant(fm) /\ (FDOM fm = x INSERT s) 
      /\ ~(fm ' x)
      ==> invariant(fm \\ x)``,
SRW_TAC[][fmap_domsub, DRESTRICT_DEF, invariant_def, INTER_DEF, IN_DEF, EXTENSION]
THEN METIS_TAC[])

val invariant_eq_last_state_imp_append_nempty_as = store_thm("invariant_eq_last_state_imp_append_nempty_as",
``!fm:('a |-> bool) x:'a s:'a -> bool.
                         invariant(fm) /\ (FDOM fm = s) /\ ~(x IN s)
                         ==> invariant(fm |+(x,F))``,
SRW_TAC[][invariant_def]
THEN METIS_TAC[FAPPLY_FUPDATE_THM])

val invariant_lemma_2 = store_thm("invariant_lemma_2",
``! fm'':('a |-> bool) e:'a vs:('a-> bool). 
                 (vs <> EMPTY /\ ~(e IN vs) /\ invariant(fm'') /\ (FDOM (fm'') = (e INSERT vs)) /\ fm'' ' e )==>
                 ({fm:('a |-> bool) | (FDOM fm = e INSERT vs) /\ invariant(fm)} 
                    = fm'' INSERT (IMAGE (\fm':('a |-> bool). (fm' |+ (e, F))) {fm | (FDOM fm = vs) /\ invariant(fm)}))``,
REWRITE_TAC[EXTENSION]
THEN REPEAT STRIP_TAC
THEN EQ_TAC
THEN STRIP_TAC
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[GSPEC_ETA, IN_DEF]
THEN1(Cases_on `x = fm''`
      THEN SRW_TAC[][]
      THEN Cases_on `(fm'' ' e)`
      THEN1(`~(x ' e)` by 
                 (MP_TAC(invariant_card_state_set |> Q.SPECL[`fm''`, `x`, `e`])
                  THEN SRW_TAC[][] THEN FULL_SIMP_TAC(srw_ss())[IN_DEF, EXTENSION])
            THEN Q.EXISTS_TAC `x \\ e`
            THEN SRW_TAC[][]
            THEN1(SRW_TAC[][GSYM FUPDATE_PURGE]
                  THEN MATCH_MP_TAC (GSYM FUPDATE_ELIM)
                  THEN SRW_TAC[][IN_DEF])
            THEN1(FULL_SIMP_TAC(srw_ss())[IN_DEF, EXTENSION] THEN METIS_TAC[] )
            THEN1(METIS_TAC[invariant_not_eq_last_diff_paths]))
      THEN1(FULL_SIMP_TAC(srw_ss())[] ))
THEN1(FULL_SIMP_TAC(srw_ss())[]
      THEN1(METIS_TAC[IN_DEF])
      THEN1(METIS_TAC[IN_DEF]))
THEN1(FULL_SIMP_TAC(srw_ss())[]
      THEN METIS_TAC[invariant_eq_last_state_imp_append_nempty_as, IN_DEF]))

val invariant_lemma_1 = store_thm("invariant_lemma_1",
``!vs. FINITE vs ==> ( !e. ~(e IN vs) ==> ?fm:('a|-> bool). (invariant(fm) /\ (FDOM (fm) = (e INSERT vs)) /\ fm ' e ))``,
HO_MATCH_MP_TAC FINITE_INDUCT
THEN SRW_TAC[][]
THEN1(Q.EXISTS_TAC `FEMPTY |+(e,T)`
      THEN SRW_TAC[][invariant_def])
THEN1(FIRST_X_ASSUM (MP_TAC o Q.SPEC `e'`)
      THEN SRW_TAC[][]
      THEN Q.EXISTS_TAC `fm |+(e,F)`
      THEN SRW_TAC[][]
      THEN1(MP_TAC(invariant_eq_last_state_imp_append_nempty_as|> Q.SPECL[`fm`, `e`, `e' INSERT vs`])
            THEN SRW_TAC[][])
      THEN1(SRW_TAC[][INSERT_COMM])
      THEN1(SRW_TAC[][NOT_EQ_FAPPLY])))

val invariant_lemma_3 = store_thm("invariant_lemma_3",
``?fm fm'. ({fm | (FDOM fm = {e}) ∧ invariant fm} = {fm} UNION {fm'}) /\ ~(fm = fm')``,
Q.EXISTS_TAC `FEMPTY |+(e, T)`
THEN Q.EXISTS_TAC `FEMPTY |+(e, F)`
THEN SRW_TAC[][EXTENSION, invariant_def]
THEN1(EQ_TAC
      THEN SRW_TAC[][fmap_EXT, EXTENSION]
      THEN METIS_TAC[])
THEN SRW_TAC[][fmap_EXT, EXTENSION])

val invariant_states_def = Define`invariant_states vs = {fm:('a|->bool) | (FDOM fm = vs) /\ invariant(fm) }`;

val invariant_lemma = store_thm("invariant_lemma",
``!vs. FINITE vs 
       ==> (CARD (invariant_states vs) = (CARD vs + 1))``,
HO_MATCH_MP_TAC FINITE_INDUCT
THEN SRW_TAC[][invariant_states_def]
THEN1(REWRITE_TAC[GSYM (MATCH_MP AND1_THM CARD_DEF)]
      THEN `{fm | (FDOM fm = ∅) ∧ invariant fm} = {FEMPTY}` by
           (SRW_TAC[][EXTENSION, invariant_def, fmap_EXT]
            THEN EQ_TAC
            THEN SRW_TAC[][])
      THEN SRW_TAC[][])
THEN1(Cases_on `vs = EMPTY`
      THEN1(SRW_TAC[][]
            THEN MP_TAC(invariant_lemma_3)
            THEN SRW_TAC[][]
            THEN ASM_SIMP_TAC(srw_ss())[] 
            THEN SRW_TAC[][CARD_DEF, INSERT_UNION])
      THEN MP_TAC(invariant_lemma_1 |> Q.SPEC `vs`)
      THEN SRW_TAC[][]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `e`)
      THEN SRW_TAC[][]
      THEN MP_TAC(invariant_lemma_2 |> Q.SPECL[`fm`, `e`, `vs`])
      THEN SRW_TAC[][]
      THEN assume_thm_concl_after_proving_assums((REWRITE_RULE[AND_IMP_INTRO] CARD_INJ_IMAGE_2) |> Q.ISPEC `(\fm'. fm' |+ (e,F))` |> Q.SPEC `{fm | (FDOM fm = vs) ∧ invariant fm}`)
      THEN1(SRW_TAC[][]
            THEN1(SRW_TAC[][ GSYM 
                        (SIMP_RULE(srw_ss())[](INTER_DEF 
                              |> Q.ISPECL[`(\f. FDOM f = vs)`] 
                              |> Q.ISPEC `(\f:('a|->bool). invariant(f))`))]
                  THEN METIS_TAC[SIMP_RULE(srw_ss())[GSPEC_ETA] FINITE_states, FINITE_INTER])
            THEN1(METIS_TAC[FUPD11_SAME_NEW_KEY]))
      THEN `FINITE (IMAGE (λfm'. fm' |+ (e,F)) {fm | (FDOM fm = vs) ∧ invariant fm})`
                by (SRW_TAC[][ GSYM 
                        (SIMP_RULE(srw_ss())[](INTER_DEF 
                              |> Q.ISPECL[`(\f. FDOM f = vs)`] 
                              |> Q.ISPEC `(\f:('a|->bool). invariant(f))`))]
                  THEN METIS_TAC[IMAGE_FINITE, SIMP_RULE(srw_ss())[GSPEC_ETA] FINITE_states, FINITE_INTER])
      THEN MP_TAC(CARD_INSERT |> Q.ISPECL[`(IMAGE (λfm'. fm' |+ (e,F)) {fm | (FDOM fm = vs) ∧ invariant fm})`])
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[FAPPLY_FUPDATE]
      THEN DECIDE_TAC))

val invariant_prob_reachable_states_thm_1 = store_thm("invariant_prob_reachable_states_thm_1",
``!vs. FINITE vs ==> ?s. (FDOM s = vs) /\ invariant s``,
SRW_TAC[][]
THEN MP_TAC(invariant_lemma |> Q.SPEC `vs`)
THEN SRW_TAC[][invariant_states_def, CARD_DEF, EXTENSION]
THEN `~({fm | (∀x. x ∈ FDOM fm ⇔ x ∈ vs) ∧ invariant fm} = EMPTY)` by
       (SPOSE_NOT_THEN STRIP_ASSUME_TAC
        THEN FULL_SIMP_TAC(bool_ss)[CARD_EMPTY]
        THEN DECIDE_TAC)
THEN FULL_SIMP_TAC(srw_ss())[EXTENSION]
THEN METIS_TAC[])

val invariant_prob_reachable_states_thm_2 = store_thm("invariant_prob_reachable_states_thm_2",
``!vs1 vs2. FINITE vs1 /\ FINITE vs2 /\ (invariant_states vs1 = invariant_states vs2) ==> (vs1 = vs2)``,
SRW_TAC[][invariant_states_def, EXTENSION]
THEN MP_TAC(invariant_prob_reachable_states_thm_1 |> Q.SPEC `vs1`)
THEN MP_TAC(invariant_prob_reachable_states_thm_1 |> Q.SPEC `vs2`)
THEN SRW_TAC[][]
THEN METIS_TAC[])

val invariant_imp_invariant_drest = store_thm("invariant_imp_invariant_drest",
``!s vs. invariant s ==> invariant (DRESTRICT s vs)``,
SRW_TAC[][invariant_def, DRESTRICT_DEF]
THEN METIS_TAC[])

val invariant_state_imp_invariant_super_state = store_thm("invariant_state_imp_invariant_super_state",
``!vs. FINITE vs 
       ==> !s. FDOM s SUBSET vs /\ invariant s
               ==> ?s'. (FDOM s' = vs) /\ invariant s' /\ (DRESTRICT s' (FDOM s) = s) 
                        /\ (!x. x IN (FDOM s' DIFF (FDOM s))==> ~(s' ' x))``,
HO_MATCH_MP_TAC FINITE_INDUCT
THEN SRW_TAC[][]
THEN1(Q.EXISTS_TAC `s` THEN SRW_TAC[][] THEN METIS_TAC[DRESTRICT_FDOM])
THEN `FDOM (DRESTRICT s (FDOM s DIFF {e})) ⊆ vs` by (FULL_SIMP_TAC(srw_ss())[SUBSET_DEF, FDOM_DRESTRICT] THEN METIS_TAC[])
THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `DRESTRICT s (FDOM s DIFF {e})`)
THEN SRW_TAC[][invariant_imp_invariant_drest]
THEN Q.EXISTS_TAC `FUNION s (s' |+ (e, F))`
THEN SRW_TAC[][]
THEN1(FULL_SIMP_TAC(srw_ss())[invariant_def, fmap_EXT, DRESTRICT_DEF, FDOM_DRESTRICT, INTER_DEF, EXTENSION, SUBSET_DEF, UNION_DEF]
      THEN METIS_TAC[])  
THEN1(FULL_SIMP_TAC(srw_ss())[invariant_def, fmap_EXT, DRESTRICT_DEF, FDOM_DRESTRICT, INTER_DEF, EXTENSION, SUBSET_DEF, FUNION_DEF]
      THEN SRW_TAC[][]
      THEN METIS_TAC[FAPPLY_FUPDATE_THM])
THEN1(FULL_SIMP_TAC(srw_ss())[invariant_def, fmap_EXT, DRESTRICT_DEF, FDOM_DRESTRICT, INTER_DEF, EXTENSION, SUBSET_DEF, FUNION_DEF]
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN FULL_SIMP_TAC(srw_ss())[invariant_def, fmap_EXT, DRESTRICT_DEF, FDOM_DRESTRICT, INTER_DEF, EXTENSION, SUBSET_DEF, FUNION_DEF]
THEN SRW_TAC[][FAPPLY_FUPDATE_THM])

val always_inavr_states = store_thm("always_inavr_states",
``!vs. FINITE vs ==> ~(invariant_states vs = EMPTY)``,
SRW_TAC[][invariant_prob_reachable_states_thm_1, invariant_states_def, GSYM MEMBER_NOT_EMPTY])

val invariantStateSpace_DRESTRICT_SUBSET_thm = store_thm("invariantStateSpace_DRESTRICT_SUBSET_thm",
``!s vs1 vs2. invariant (DRESTRICT s vs1) /\ vs2 SUBSET vs1  ==> invariant (DRESTRICT s vs2)``,
SRW_TAC[][invariant_def, DRESTRICT_DEF, SUBSET_DEF]
THEN METIS_TAC[])

val invariant_states_stateSpace_thm = store_thm("invariant_states_stateSpace_thm",
``!vs. stateSpace (invariant_states vs) vs``,
SRW_TAC[][stateSpace_def, invariant_states_def])

val ss_proj_union_invar_states = store_thm("ss_proj_union_invar_states",
``!vs1 vs2. ss_proj (invariant_states vs1) (vs1 UNION vs2) = invariant_states vs1``,
SRW_TAC[][ss_proj_def, invariant_states_def, EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1(SRW_TAC[][FDOM_DRESTRICT] THEN METIS_TAC[])
THEN1(FULL_SIMP_TAC(srw_ss())[invariant_def, fmap_EXT, DRESTRICT_DEF, FDOM_DRESTRICT, INTER_DEF, EXTENSION, SUBSET_DEF, FUNION_DEF]
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN1(Q.EXISTS_TAC `x` THEN SRW_TAC[][]
     THEN FULL_SIMP_TAC(srw_ss())[GSYM EXTENSION]
     THEN METIS_TAC[exec_drest_5, SUBSET_UNION, UNION_COMM]))

val ss_proj_PRODf_eq_PRODf = store_thm("ss_proj_PRODf_eq_PRODf",
``!VS. FINITE VS ==> ((!vs. vs IN VS ==>  FINITE vs)
                       /\ (!vs1 vs2. vs1 IN VS /\ vs2 IN VS /\ ~(vs1 = vs2) ==> DISJOINT vs1 vs2)
          ==> !vs vs2. FINITE vs /\ (!vs1. vs1 IN VS ==> DISJOINT vs vs1)
                   ==>
                       ((ss_proj (PRODf (IMAGE invariant_states VS) (invariant_states vs))
                             (vs2 UNION vs ∪ BIGUNION VS)) =
                               (PRODf (IMAGE invariant_states VS)
                                      (invariant_states vs))))``,
HO_MATCH_MP_TAC FINITE_INDUCT
THEN SRW_TAC[][]
THEN1(SRW_TAC[][ITSET_EMPTY, ss_proj_def, EXTENSION, invariant_states_def, DRESTRICT_FDOM, invariant_def] 
      THEN EQ_TAC
      THEN SRW_TAC[][]
      THEN1 (SRW_TAC[][FDOM_DRESTRICT, INTER_DEF] THEN METIS_TAC[])
      THEN1 (FULL_SIMP_TAC(srw_ss())[DRESTRICT_DEF] THEN METIS_TAC[])
      THEN1 (Q.EXISTS_TAC `x` THEN SRW_TAC[][]
             THEN1 (SRW_TAC[][fmap_EXT, FDOM_DRESTRICT, INTER_DEF, EXTENSION, DRESTRICT_DEF] THEN METIS_TAC[])
             THEN1 METIS_TAC[]))
THEN `~(invariant_states e IN (IMAGE invariant_states VS))` by
          METIS_TAC[invariant_prob_reachable_states_thm_2, (invariant_prob_reachable_states_thm_3 |> INST_TYPE[alpha |-> ``:'a -> bool``]) |> Q.SPEC `FINITE`]
THEN `(IMAGE invariant_states VS DELETE invariant_states e) = IMAGE invariant_states VS` by METIS_TAC[DELETE_NON_ELEMENT]
THEN SRW_TAC[][]
THEN `PRODf (invariant_states e INSERT IMAGE invariant_states VS) (invariant_states vs) = 
              stateSpaceProduct (invariant_states e) (PRODf (IMAGE invariant_states VS) (invariant_states vs))` by
     (MATCH_MP_TAC (PRODf_INSERT |> INST_TYPE [beta |-> ``:bool``] |> Q.SPECL[ `invariant_states e`, `IMAGE invariant_states VS`])
      THEN SRW_TAC[][]
      THEN1 METIS_TAC[always_inavr_states]
      THEN1 METIS_TAC[invariant_states_stateSpace_thm]
      THEN1(`DISJOINT e x` by METIS_TAC[]
            THEN `stateSpace (invariant_states x) x` by METIS_TAC[invariant_states_stateSpace_thm]
            THEN METIS_TAC[always_inavr_states, EQ_SS_DOM])
      THEN1 METIS_TAC[always_inavr_states, EQ_SS_DOM, invariant_states_stateSpace_thm]
      THEN1 METIS_TAC[invariant_states_stateSpace_thm])
THEN SRW_TAC[][GSYM ss_proj_PRODf_eq_PRODf_1]
THEN `(ss_proj (invariant_states e) (vs2 ∪ vs ∪ (e ∪ BIGUNION VS))) = invariant_states e`
          by METIS_TAC[ss_proj_union_invar_states, UNION_COMM, UNION_ASSOC]
THEN SRW_TAC[][]
THEN `(∀vs. vs ∈ VS ⇒ FINITE vs)` by METIS_TAC[]
THEN UNDISCH_TAC
``(∀vs. vs ∈ VS ⇒ FINITE vs) ∧
      (∀vs1 vs2. vs1 ∈ VS ∧ vs2 ∈ VS ∧ vs1 ≠ vs2 ⇒ DISJOINT vs1 vs2) ⇒
      ∀vs vs2.
        FINITE vs ∧ (∀vs1. vs1 ∈ VS ⇒ DISJOINT vs vs1) ⇒
        (ss_proj
           (PRODf (IMAGE invariant_states VS) (invariant_states vs))
           (vs2 ∪ vs ∪ BIGUNION VS) =
         PRODf (IMAGE invariant_states VS) (invariant_states vs))``
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (ASSUME_TAC o Q.SPECL[ `vs`, `vs2 UNION e`])
THEN SRW_TAC[][]
THEN `(ss_proj
          (PRODf (IMAGE invariant_states VS) (invariant_states vs))
          (vs2 ∪ e ∪ vs ∪ BIGUNION VS) =
        PRODf (IMAGE invariant_states VS) (invariant_states vs))` by METIS_TAC[]
THEN METIS_TAC[UNION_COMM, UNION_ASSOC])

val invariantStateSpace_CARD = store_thm("invariantStateSpace_CARD",
``!VS. FINITE VS==> !vs. FINITE vs /\ ~(vs IN VS) /\ (!vs'. vs' IN VS ==> FINITE vs') 
         ==>
         (Π CARD (invariant_states vs INSERT IMAGE invariant_states VS)
               = Π (\vs. CARD vs + 1)  (vs INSERT VS))``,
Induct_on `FINITE`
THEN SRW_TAC[][PROD_IMAGE_THM]
THEN1 METIS_TAC[invariant_lemma]
THEN SRW_TAC[][invariant_lemma]
THEN `~(invariant_states vs IN (invariant_states e INSERT IMAGE invariant_states VS))` by 
      (SRW_TAC[][IN_DEF]
      THEN1 METIS_TAC[invariant_prob_reachable_states_thm_2]
      THEN Cases_on `¬VS x`
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[IN_DEF]
      THEN METIS_TAC[invariant_prob_reachable_states_thm_2])
THEN FULL_SIMP_TAC(srw_ss())[DELETE_NON_ELEMENT]
THEN `~(vs IN (e INSERT VS))` by (SRW_TAC[][] THEN METIS_TAC[DELETE_NON_ELEMENT])
THEN FULL_SIMP_TAC(srw_ss())[DELETE_NON_ELEMENT])


val _ = export_theory();
