open HolKernel Parse boolLib bossLib;
open pred_setTheory;
val _ = new_theory "partitioning";

val partitioning_def = Define`partitioning sos X  = ((BIGUNION sos = X)  /\ !s t.s IN sos /\ t IN sos ==> ((s = t) \/ DISJOINT s t))`;

val choice_fun_def = Define`choice_fun f sos = !s. s IN sos ==> f(s) IN s`;

val transversal_def = Define`transversal tvsal sos = (INJ (tvsal) sos (BIGUNION sos)) /\ choice_fun tvsal sos`;

val quotient_def = Define`quotient sos x  = CHOICE {s | x IN s /\ s IN sos}`;

val partitioning_quoteint_sing_thm = store_thm("partitioning_quoteint_sing",
  ``!P X x. partitioning P X /\ x IN X ==> SING {p' | x IN p' /\ p' IN P} ``,
  SRW_TAC[][partitioning_def, IN_BIGUNION, SING_DEF]
  THEN MP_TAC(IN_BIGUNION |> Q.SPECL[`x`, `P`])
  THEN SRW_TAC[][]
  THEN Q.EXISTS_TAC `s`
  THEN SRW_TAC[][EXTENSION]
  THEN EQ_TAC
  THEN1(SRW_TAC[][]
        THEN FULL_SIMP_TAC(srw_ss())[DISJOINT_DEF, INTER_DEF, EXTENSION]
        THEN SRW_TAC[][]
        THEN METIS_TAC[])
  THEN SRW_TAC[][GSYM EXTENSION]);

val _ = export_theory();

