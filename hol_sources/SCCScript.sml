open HolKernel Parse boolLib bossLib;
(*open HolKernel Parse boolLib QLib tautLib mesonLib metisLib
     simpLib boolSimps BasicProvers; *)
open pred_setTheory;
open relationTheory;
open rel_utilsTheory;

val _ = new_theory "SCC";

val SCC_def = Define`SCC R vs <=> (!v v'. v IN vs /\ v' IN vs ==> R^+ v v' /\ R^+ v' v)
                                  /\ (!v v'. v IN vs /\ ~(v' IN vs) ==> ~(R^+ v v') \/ ~(R^+ v' v))
                                  (*/\ ?v v'. ~(v = v') /\ v IN vs /\ v' IN vs*)
                                  /\ ~(vs = {})`

(* A function to lift a relation ('a -> 'a -> bool) to (('a -> bool) -> ('a -> bool) -> bool), i.e
yeilds a relation between sets of objects. *)
val lift_def = Define`lift R vs vs' <=> ?v v'. v IN vs /\ v' IN vs' /\ R v v'`

val scc_disjoint_lemma = store_thm("scc_disjoint_lemma",
``!R vs vs'. SCC R vs /\ SCC R vs' /\ ~(vs = vs') ==> DISJOINT vs vs'``,
SRW_TAC[][] 
THEN SPOSE_NOT_THEN STRIP_ASSUME_TAC
THEN FULL_SIMP_TAC(srw_ss()) [DISJOINT_DEF, INTER_DEF, GSPEC_ETA, GSYM MEMBER_NOT_EMPTY]
THEN FULL_SIMP_TAC(bool_ss)[SCC_def]
THEN `?v. v IN vs /\ ~(v IN vs')` by (FULL_SIMP_TAC(bool_ss)[EXTENSION, IN_DEF] THEN METIS_TAC[])
THEN METIS_TAC[])

val scc_tc_inclusion = store_thm("scc_tc_inclusion",
``!R vs v v'. v IN vs /\ v' IN vs/\ SCC R vs /\ reflexive R
              ==> (\v v'. R v v' /\ v IN vs /\ v' IN vs)^+ v v'``,
SRW_TAC[][SCC_def]
THEN LAST_X_ASSUM (MP_TAC o Q.SPECL[`v`, `v'`])
THEN SRW_TAC[][]
THEN MP_TAC(REFL_IMP_3_CONJ  |> Q.SPEC `R`)
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[ `(\v. v IN vs)`, `v`, `v'`])
THEN SRW_TAC[][]
THEN METIS_TAC[REWRITE_RULE[transitive_def] (TC_TRANSITIVE |> Q.SPEC `R`)])

(*val SCC_loop_contradict = store_thm("SCC_loop_contradict",
``!R vs vs'. (lift R)^+ vs vs' /\ (lift R)^+ vs' vs
             ==> ~(SCC R vs) /\ ~(SCC R vs')``,
cheat)*)

val _ = export_theory();

