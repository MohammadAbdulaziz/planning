open HolKernel Parse boolLib bossLib;
val _ = new_theory "tightness";

val valid_bound_def = 
      Define`valid_bound f R g =
                   !x y. R x y ==> f x <= g y`

val tight_bound_def = 
      Define`tight_bound f R g = 
                 (valid_bound f R g /\
                  (!y. (?x. R x y) ==> (?x'. R x' y /\ (f x' = g y))))`

val tight_works = store_thm("tight_works",
  ``(tight_bound f R g /\ h y < g y /\ (?x. R x y)) ==> ~(valid_bound f R h)``,
  SRW_TAC[][tight_bound_def, valid_bound_def,arithmeticTheory.NOT_LESS_EQUAL]
  THEN METIS_TAC[]);

val valid_compose = store_thm("valid_compose",
  ``(valid_bound f R (g o h))
    <=> valid_bound f (\x y. ?z. (R x z /\ (h z = y))) g``,
  SRW_TAC[][valid_bound_def]
  THEN METIS_TAC[]);

val tight_compose_1 = store_thm("tight_compose_1",
  ``SURJ h UNIV UNIV
   ==> (tight_bound f R (g o h) ==>
           tight_bound f (\x y. ?z. (R x z /\ (h z = y))) g)``,
  SRW_TAC[][tight_bound_def, valid_compose]
  THEN FULL_SIMP_TAC(srw_ss())[pred_setTheory.SURJ_DEF]
  THEN METIS_TAC[]);

val tight_compose_2 = store_thm("tight_compose_2",
  ``INJ h UNIV UNIV
   ==> (tight_bound f (\x y. ?z. (R x z /\ (h z = y))) g ==>
             tight_bound f R (g o h))``,
  SRW_TAC[][tight_bound_def, valid_compose]
  THEN FULL_SIMP_TAC(srw_ss())[pred_setTheory.INJ_DEF, pred_setTheory.SURJ_DEF, pred_setTheory.BIJ_DEF, valid_bound_def]
  THEN SRW_TAC[][]
  THEN METIS_TAC[]);

val tight_same_dom = store_thm("tight_same_dom",
  ``tight_bound f R g
    /\ (!x y. R x y ==> (x = fst y))
    ==> (!x. R x y ==> (f x = g y))``,
  SRW_TAC[][tight_bound_def, valid_bound_def]
  THEN METIS_TAC[]);


val tight_compose_1_cor = store_thm("tight_compose_1_cor",
  ``SURJ h UNIV UNIV
   ==> (tight_bound f R (g o h) ==>
tight_bound (g o h) (\x y. h x = y) g)``,
  SRW_TAC[][tight_bound_def, valid_bound_def, valid_compose]
  THEN FULL_SIMP_TAC(srw_ss())[pred_setTheory.SURJ_DEF]
  THEN METIS_TAC[]);

val tight_compose_2_cor = store_thm("tight_compose_2_cor",
  ``INJ h UNIV UNIV
   ==> (tight_bound f (\x y. ?z. (R x z /\ (h z = y))) g ==>
             tight_bound g (\x y.  h y = x) (g o h))``,
  SRW_TAC[][tight_bound_def, valid_bound_def, valid_compose]
  THEN FULL_SIMP_TAC(srw_ss())[pred_setTheory.INJ_DEF]);


val _ = export_theory();
