open HolKernel Parse boolLib bossLib;
open partitioningTheory;
open pred_setTheory;
val _ = new_theory "CNF";

val _ = type_abbrev ( "literal", ``:('a#bool)``)

val _ = type_abbrev( "clause", ``:('a literal)->bool``)

val _ = type_abbrev( "formula", ``:('a clause)->bool``)

val model_def = Define`model(M:('a literal)->bool) = !x. ~((x,T) IN M ) \/ ~((x,F) IN M)`;

val models_clause_def = Define`models_clause (M:('a literal)->bool)  (c:'a clause) = ~(M INTER c = EMPTY)`;

val models_formula_def = Define`models_formula M phi = (!c. (c IN phi ==> models_clause M c))`;

val image_clause_def = Define` image_clause f c: 'a clause = IMAGE (\l. (f(FST l) , SND l)) c`;

val image_formula_def = Define` image_formula f  phi: 'a formula = IMAGE (image_clause f) phi`;

val consistent_tvsal_def = Define`consistent_tvsal(tvsal, phi, phi') = image_formula tvsal phi SUBSET phi'`;

val covering_tvsals_def = Define`covering_tvsals(tvsals, phi, phi') = phi' SUBSET BIGUNION {image_formula tvsal phi| tvsal IN tvsals}`;

val clause_dom_def = Define` clause_dom (c: 'a clause) = {x | (?bv. (x, bv) IN c)}`;

val formula_dom_def = Define` formula_dom (phi: 'a formula) = (BIGUNION {clause_dom (c:'a clause) | c IN phi})`;


val colouring_def = Define`colouring P (phi: 'a formula) = 
!p x y. p IN P /\ x IN p /\ y IN p ==> ((x = y) \/ (!c. c IN phi ==> ~(x IN clause_dom c) \/ ~(y IN clause_dom c)))`;


(*val formula_equisat_quotient_1_thm = store_thm("formula_equisat_quotient_1",
``!phi' phi tvsals. covering_tvsals(tvsals, phi', phi) 
                    /\ ~(tvsals = EMPTY) (*Remove this assumption*)
                    ==> ((?M'. models_formula M' phi' ) ==> (?M. models_formula M phi))``,
cheat
(*
SRW_TAC[][]
THEN Q.EXISTS_TAC `BIGUNION ({image_clause tvsal M'| tvsal IN tvsals})`
THEN FULL_SIMP_TAC(srw_ss())[models_formula_def, models_clause_def,
                             pred_setTheory.BIGUNION, pred_setTheory.INTER_DEF,
                             covering_tvsals_def, SUBSET_DEF, INTER_DEF, EXTENSION]
THEN SRW_TAC[][]
THEN Cases_on `c`
THEN1()
THEN Q.EXISTS_TAC `x'`
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `x' INSERT t`
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `x`
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[image_formula_def, image_clause_def, IMAGE_DEF, EXTENSION]
THEN SRW_TAC[][]
THEN Cases_on `x'' = x'`
THEN1(SRW_TAC[][]
      THEN LAST_ASSUM (MP_TAC o Q.SPECL[`x' INSERT t` ]   )
      THEN SRW_TAC[][] THEN METIS_TAC[])
METIS_TAC[]
*));

val formula_equisat_quotient_2_thm = store_thm("formula_equisat_quotient_2",
``!phi P tvsal. partitioning P (formula_dom phi)
                 /\ consistent_tvsal(tvsal, image_formula (quotient P) phi, phi)
                 ==> ((?M. models_formula M phi) ==>(?M'. models_formula M' (image_formula (quotient P) phi) ))``,
cheat);

val formula_equisat_quotient_thm = store_thm("formula_equisat_quotient",
``!phi P tvsals. partitioning P (formula_dom phi) /\ covering_tvsals(tvsals, image_formula (quotient P) phi, phi)
                 /\ (!tvsal. tvsal IN tvsals ==> consistent_tvsal(tvsal, image_formula (quotient P) phi, phi)) 
                 /\ ~(tvsals = EMPTY) (* This assumption is not needed, it can proven from that phi is not empty*)
                 ==> ((?M. models_formula M phi) <=>(?M'. models_formula M' (image_formula (quotient P) phi) ))``,
SRW_TAC[][]
THEN EQ_TAC 
THEN1(MATCH_MP_TAC(formula_equisat_quotient_2_thm)
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[GSYM pred_setTheory.MEMBER_NOT_EMPTY]
      THEN Q.EXISTS_TAC `x`
      THEN METIS_TAC[])
THEN MATCH_MP_TAC(formula_equisat_quotient_1_thm)
THEN METIS_TAC[]);
*)
val _ = export_theory();

