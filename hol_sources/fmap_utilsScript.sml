open HolKernel Parse boolLib bossLib;
open finite_mapTheory;
open pred_setTheory;
open pairTheory;
val _ = new_theory "fmap_utils";

val finite_dom_always_has_mapping = store_thm("finite_dom_always_has_mapping",
``!s. FINITE s ==> ?f. (FDOM f = s)``,
MATCH_MP_TAC (SIMP_RULE(srw_ss())[] (FINITE_INDUCT |> Q.SPEC `\s. ?f. (FDOM f = s)`))
THEN SRW_TAC[][FDOM_EQ_EMPTY]
THEN `?y. y IN UNIV:'b set` by METIS_TAC[UNIV_NOT_EMPTY, MEMBER_NOT_EMPTY]
THEN Q.EXISTS_TAC `f |+(e,y)`
THEN SRW_TAC[][])

val IN_FDOM_DRESTRICT_DIFF = store_thm("IN_FDOM_DRESTRICT_DIFF",
``!fdom vs v f. ~(v IN vs) /\ FDOM f SUBSET fdom /\ v IN FDOM f
                ==> v IN FDOM(DRESTRICT f (fdom DIFF vs))``,
SRW_TAC[][DIFF_DEF, INTER_DEF, EXTENSION, GSPEC_ETA, DISJOINT_DEF, SUBSET_DEF, FDOM_DRESTRICT])

val nempty_eff_nempty_act = store_thm("nempty_eff_nempty_act",
``!x. ~(FDOM (SND x) = EMPTY) ==> ((λx. ~(x = (FEMPTY,FEMPTY))) x)``,
SRW_TAC[][]
THEN METIS_TAC[PAIR_EQ, PAIR, fmap_EXT, FDOM_FEMPTY]);

val submap_drest_submap = store_thm("submap_drest_submap",
``!fm1 fm2 vs. (fm2 SUBMAP fm1) 
               ==> ((DRESTRICT fm2 vs) SUBMAP (DRESTRICT fm1 vs) )``,
SRW_TAC[][SUBSET_DEF, SUBMAP_DEF]
THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF, SUBMAP_DEF, FDOM_DRESTRICT, DRESTRICT_DEF]);

val drestrict_subset_submap = store_thm("drestrict_subset_submap",
``!fm1 fm2.  (fm2 SUBMAP fm1) /\ vs2 SUBSET vs1
       ==> ((DRESTRICT fm2 vs2) SUBMAP (DRESTRICT fm1 vs1) )``,
SRW_TAC[][SUBSET_DEF, SUBMAP_DEF]
THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF, SUBMAP_DEF, FDOM_DRESTRICT, DRESTRICT_DEF]);

val drestrict_subset_submap_2 = store_thm("drestrict_subset_submap_2",
``!fm1 fm2.  FDOM fm2 SUBSET vs /\ (fm2 SUBMAP fm1)
       ==> fm2 SUBMAP (DRESTRICT fm1 vs)``,
SRW_TAC[][SUBSET_DEF, SUBMAP_DEF]
THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF, SUBMAP_DEF, FDOM_DRESTRICT, DRESTRICT_DEF]);

val eq_map_eq_submap = store_thm("eq_map_eq_submap",
``!fm1 fm2 vs. (vs ⊆ FDOM fm1) /\ (fm2 = fm1) 
       ==> ((DRESTRICT fm2 vs) = (DRESTRICT fm1 vs) )``,
SRW_TAC[][SUBSET_DEF, SUBMAP_DEF]);

val disj_dom_drest_fupdate_eq = store_thm("disj_dom_drest_fupdate_eq",
``!x vs s. (DISJOINT (FDOM (x)) vs) ==> ((DRESTRICT s vs) = (DRESTRICT (x ⊌ s) vs))``,
SRW_TAC[][DISJOINT_DEF, DRESTRICT_DEF, INTER_DEF, EXTENSION, FUNION_DEF]
THEN `!x''. (x'' IN vs) ==> (((x ⊌ s) ' x'') = s ' x'')` by (SRW_TAC[][FUNION_DEF] THEN METIS_TAC[])
THEN `FDOM (DRESTRICT s vs) = FDOM ( DRESTRICT(x ⊌ s) vs)` by( SRW_TAC [][FDOM_DRESTRICT,EXTENSION] THEN FULL_SIMP_TAC (srw_ss()) [SUBSET_DEF] THEN METIS_TAC[])
THEN SRW_TAC[][]
THEN `!x''. (DRESTRICT s vs) ' x'' = (DRESTRICT (x ⊌ s) vs) ' x''` by (Cases_on`x'' ∉ FDOM x`
     THEN Cases_on`x'' ∉ vs`
     THEN SRW_TAC[][FDOM_DRESTRICT, INTER_DEF, DRESTRICT_DEF]
     THEN METIS_TAC[])
THEN METIS_TAC[fmap_EQ_THM]);

val DRESTRICTED_FUNION_2 = store_thm("DRESTRICTED_FUNION_2",
``DRESTRICT (f1 ⊌ f2) s =
     DRESTRICT f1 s ⊌ DRESTRICT f2 s``,
`FDOM (DRESTRICT (f1 ⊌ f2) s) =
     FDOM (DRESTRICT f1 s ⊌ DRESTRICT f2 s)` by
  (SRW_TAC[][DRESTRICT_DEF, INTER_DEF, EXTENSION, FUNION_DEF] THEN METIS_TAC[])
THEN `!x. x IN FDOM (DRESTRICT (f1 ⊌ f2) s) ==>
          ((DRESTRICT (f1 ⊌ f2) s) ' x = (DRESTRICT f1 s ⊌ DRESTRICT f2 s) ' x)` by
     (SRW_TAC[][DRESTRICT_DEF, FUNION_DEF] THEN METIS_TAC[])
THEN METIS_TAC[fmap_EQ_THM])

val drestrict_empty_iff_disj = store_thm("drestrict_empty_iff_disj",
``!x vs. (FDOM (DRESTRICT (x) vs) = EMPTY)
<=>DISJOINT (FDOM x) vs``,
SRW_TAC[][DISJOINT_DEF, FDOM_DRESTRICT]);

val graph_plan_card_state_set = store_thm("graph_plan_card_state_set",
``!PROB vs. FINITE vs ==>
(CARD( FDOM(DRESTRICT s vs)) <= CARD(vs))``,
SRW_TAC[][FDOM_DRESTRICT]
THEN METIS_TAC[CARD_INTER_LESS_EQ, INTER_COMM ,SUBSET_INTER_ABSORPTION]);

val x_in_fdom_eq_in_fdom_drestrict = store_thm("x_in_fdom_eq_in_fdom_drestrict",
``!x vs y. x IN vs ==> (x IN FDOM (DRESTRICT (y) vs) <=> (x ∈ FDOM (y)))``,
SRW_TAC[][DRESTRICT_DEF]);

val fdom_subset_imp_drestrict = store_thm("fdom_subset_imp_drestrict",
``!x y vs. (FDOM x SUBSET FDOM y ) ==> (FDOM (DRESTRICT (x) vs) ⊆ FDOM (DRESTRICT y vs))``,
SRW_TAC[][FDOM_DRESTRICT]
THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF, INTER_DEF]);

val fdom_eq_drestrict_eq = store_thm("fdom_eq_drestrict_eq",
``!s s' vs. (FDOM s = FDOM s')
	==> (FDOM (DRESTRICT s vs) = FDOM (DRESTRICT s' vs))``,
SRW_TAC[][DRESTRICT_DEF]);

val exec_drest_5= store_thm("exec_drest_5",
``!x vs. FDOM(x) SUBSET vs ==> (DRESTRICT x vs = x) ``,
SRW_TAC[][]     
THEN `FDOM x = FDOM(DRESTRICT x vs)` by METIS_TAC[FDOM_DRESTRICT, SUBSET_INTER_ABSORPTION]
THEN METIS_TAC[fmap_EQ_THM, DRESTRICT_DEF]);

val graph_plan_lemma_5 = store_thm("graph_plan_lemma_5",
``!s s' vs. ((DRESTRICT s ((FDOM s) DIFF vs)) = (DRESTRICT s' ((FDOM s') DIFF vs)))
	    /\ ((DRESTRICT s vs) = (DRESTRICT s' vs))   
     	    ==>
            (s = s')``,
SRW_TAC[][]
THEN `FDOM s = FDOM s'` by 
      (FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT, fmap_EXT, INTER_DEF] 
      THEN FULL_SIMP_TAC(srw_ss())[EXTENSION]	       	      
      THEN METIS_TAC[])
THEN `!x. s ' x = s' ' x` by
     (FULL_SIMP_TAC(srw_ss())[fmap_EXT, DRESTRICT_DEF]
     THEN Cases_on `x IN vs`
     THEN Cases_on `x ∈ FDOM s'`
     THEN FULL_SIMP_TAC(srw_ss())[]
     THEN METIS_TAC[NOT_FDOM_FAPPLY_FEMPTY])
THEN SRW_TAC[][GSYM fmap_EQ_THM]);

val eq_restricted_eq_full = store_thm("eq_restricted_eq_full",
``!s s' vs. ((DRESTRICT s vs1) = (DRESTRICT s' vs1))
	    /\ ((DRESTRICT s vs2) = (DRESTRICT s' vs2))
            /\ FDOM s SUBSET (vs1 UNION vs2)
            /\ FDOM s' SUBSET (vs1 UNION vs2)
     	    ==>
            (s = s')``,
SRW_TAC[][]
THEN `∀f g.
     (FDOM f = FDOM g) ∧ (∀x. x ∈ FDOM f ⇒ (f ' x = g ' x)) ==> (f = g)` by METIS_TAC[fmap_EQ_THM]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN1(FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT, fmap_EXT, INTER_DEF, SUBSET_DEF, UNION_DEF]
      THEN FULL_SIMP_TAC(srw_ss())[EXTENSION]	       	      
      THEN METIS_TAC[])
THEN1(FULL_SIMP_TAC(srw_ss())[fmap_EXT, DRESTRICT_DEF, SUBSET_DEF, UNION_DEF, INTER_DEF, EXTENSION]
      THEN METIS_TAC[NOT_FDOM_FAPPLY_FEMPTY]));

val drest_smap_drest_smap_drest = store_thm("drest_smap_drest_smap_drest",
``!x s vs. (DRESTRICT x vs  ⊑ s) <=> ( DRESTRICT x vs SUBMAP DRESTRICT s vs)``,
SRW_TAC[][DRESTRICT_DEF, SUBMAP_DEF]
THEN EQ_TAC
THEN METIS_TAC[]);

val sat_precond_as_proj_1 = store_thm("sat_precond_as_proj_1",
``!s s' vs x.
(DRESTRICT (s) vs = DRESTRICT (s') vs) ==>
(DRESTRICT (x) vs ⊑ s <=> DRESTRICT (x) vs ⊑ s')``,
SRW_TAC[][SUBMAP_DEF]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN `FDOM(DRESTRICT s vs) = FDOM(DRESTRICT s' vs)` by SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT, INTER_DEF, EXTENSION, fmap_EXT, DRESTRICT_DEF]
THEN METIS_TAC[FDOM_DRESTRICT]);

val sat_precond_as_proj_4 = store_thm("sat_precond_as_proj_4",
``!fm1 fm2 vs.  (fm2 SUBMAP fm1) 
       ==> ((DRESTRICT fm2 vs) SUBMAP (fm1) )``,
SRW_TAC[][SUBSET_DEF, SUBMAP_DEF]
THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF, SUBMAP_DEF, FDOM_DRESTRICT, DRESTRICT_DEF]);

val proj_pre_eff_eq = store_thm("proj_pre_eff_eq",
``!x. SND ((λa. (DRESTRICT (FST a) vs,SND a)) x) = SND x``,
SRW_TAC[][]);


val nempty_drest_dom_imp_nempty_dom = store_thm("nempty_drest_dom_imp_nempty_dom",
``!x vs. FDOM (DRESTRICT x vs) <> EMPTY  ==> (FDOM  x) <> EMPTY``,
SRW_TAC[][FDOM_DRESTRICT, INTER_DEF, EXTENSION]
THEN METIS_TAC[]);

val fdom_eq_fd_drestrict_eq = store_thm("fdom_eq_fd_drestrict_eq",
``!x y z. (FDOM x = FDOM y) ==> (FDOM (DRESTRICT x z) = FDOM (DRESTRICT y z))``,
METIS_TAC[SUBSET_DEF, FDOM_DRESTRICT]);

val sublist_as_proj_eq_as_1 = store_thm("sublist_as_proj_eq_as_1",
``!x s vs.  ( x SUBMAP DRESTRICT s vs) ==> (x ⊑ s)``,
SRW_TAC[][DRESTRICT_DEF, SUBMAP_DEF]);

val EQ_SUBMAP_IMP_DRESTRICT_SUBMAP = store_thm("EQ_SUBMAP_IMP_DRESTRICT_SUBMAP",
``!vs a s s'. (DRESTRICT s vs = DRESTRICT s' vs)
              /\ a SUBMAP s
              ==>
                 (DRESTRICT a vs) SUBMAP s'``,
METIS_TAC[sat_precond_as_proj_1, sat_precond_as_proj_4]);

val drestrict_submap_and_comp_imp_submap = store_thm("drestrict_submap_and_comp_imp_submap",
``!vs a s. FDOM a SUBSET FDOM s /\ (DRESTRICT a vs) SUBMAP s /\ (DRESTRICT a (FDOM(s) DIFF vs)) SUBMAP s
              ==>
                  a SUBMAP s``,
SRW_TAC[][DRESTRICT_DEF, SUBMAP_DEF, DIFF_DEF, SUBSET_DEF]
THEN FIRST_X_ASSUM (Q.SPECL_THEN [`x`] MP_TAC)
THEN FIRST_X_ASSUM (Q.SPECL_THEN [`x`] MP_TAC)
THEN SRW_TAC[][]
THEN METIS_TAC[]);


val submap_drestrict_diff = store_thm("submap_drestrict_diff",
``!vs a s fdom. FDOM a SUBSET fdom /\ FDOM s SUBSET fdom /\  (DRESTRICT a vs) SUBMAP s /\ (DRESTRICT a (fdom DIFF vs)) SUBMAP s
              ==>
                  a SUBMAP s``,
SRW_TAC[][DRESTRICT_DEF, SUBMAP_DEF, DIFF_DEF, SUBSET_DEF]
THEN FIRST_X_ASSUM (Q.SPECL_THEN [`x`] MP_TAC)
THEN FIRST_X_ASSUM (Q.SPECL_THEN [`x`] MP_TAC)
THEN SRW_TAC[][]
THEN METIS_TAC[]);

val FDOM_INTER_NEQ_DRESTRICT_NEQ = store_thm("FDOM_INTER_NEQ_DRESTRICT_NEQ",
``(FDOM s) INTER vs1 <> (FDOM s2) INTER vs2
    ==> DRESTRICT s vs1 <> DRESTRICT s2 vs2``,
METIS_TAC[DRESTRICT_EQ_DRESTRICT, INTER_COMM])

val limited_dom_neq_restricted_neq = store_thm("limited_dom_neq_restricted_neq",
``FDOM f1 SUBSET vs /\ FUNION f1 f2 <> f2
  ==> DRESTRICT (FUNION f1 f2) vs <> DRESTRICT f2 vs``,
SRW_TAC[][FUNION_DEF, SUBSET_DEF, DRESTRICT_DEF, fmap_EXT, UNION_DEF, INTER_DEF, EXTENSION]
THEN METIS_TAC[])

val submap_subset = store_thm("submap_subset",
``f1 SUBMAP f2 ==> FDOM f1 SUBSET FDOM f2``,
SRW_TAC[][SUBMAP_DEF,SUBSET_DEF])

val submap_drestrict_eq = store_thm("submap_drestrict_eq",
``!fm1 fm2 vs. (vs ⊆ FDOM fm1) /\ (fm1 SUBMAP fm2) 
       ==> ((DRESTRICT fm1 vs) = (DRESTRICT fm2 vs) )``,
SRW_TAC[SatisfySimps.SATISFY_ss][DRESTRICT_EQ_DRESTRICT_SAME]
THEN1 METIS_TAC[submap_subset, SUBSET_INTER_ABSORPTION, SUBSET_TRANS]
THEN METIS_TAC[SUBMAP_DEF]);

val two_submaps_are_submap = store_thm("two_submaps_are_submap",
``fm1 SUBMAP fm3 /\ fm2 SUBMAP fm3 ==> FUNION fm1 fm2 SUBMAP fm3``,
SRW_TAC[][SUBMAP_DEF, FUNION_DEF]
THEN METIS_TAC[])

val union_of_submaps_is_submap = store_thm("union_of_submaps_is_submap",
``(!fm. MEM fm fmList ==> fm SUBMAP fm')==>
    FOLDR FUNION FEMPTY fmList SUBMAP fm'``,
Induct_on `fmList`
THEN SRW_TAC[][SUBMAP_FEMPTY]
THEN METIS_TAC[two_submaps_are_submap])

val SUBMAP_FUNION_DRESTRICT = store_thm("SUBMAP_FUNION_DRESTRICT",
``fma SUBMAP s /\ fmb SUBMAP s /\ (vsa) SUBSET FDOM fma /\ (vsb) SUBSET FDOM fmb /\ 
  (DRESTRICT fm vsa = DRESTRICT fma (vsa INTER vs)) /\ (DRESTRICT fm vsb = DRESTRICT fmb (vsb INTER vs)) ==>
  (DRESTRICT fm (vsa UNION vsb) = DRESTRICT (FUNION fma fmb) ((vsa UNION vsb) INTER vs))``,
SRW_TAC[][DRESTRICT_DEF, fmap_EXT, INTER_DEF, UNION_DEF, EXTENSION, SUBMAP_DEF, SUBSET_DEF, FUNION_DEF]
THEN METIS_TAC[])

val UNION_FUNION_DRESTRICT = store_thm("UNION_FUNION_DRESTRICT",
`` (DRESTRICT fm vsa = DRESTRICT fma (vs)) /\ (DRESTRICT fm vsb = DRESTRICT fmb (vs)) ==>
  (DRESTRICT fm (vsa UNION vsb) = DRESTRICT (FUNION fma fmb) vs)``,
SRW_TAC[][DRESTRICT_DEF, fmap_EXT, INTER_DEF, UNION_DEF, EXTENSION, SUBMAP_DEF, SUBSET_DEF, FUNION_DEF]
THEN METIS_TAC[])

val FUNION_DRESTRICT_UNION = store_thm("FUNION_DRESTRICT_UNION",
``(fm1 = DRESTRICT fm vs1) /\ (fm2 = DRESTRICT fm vs2) ==>
  (FUNION fm1 fm2 = DRESTRICT fm (vs1 UNION vs2))``,
SRW_TAC[][DRESTRICT_DEF, fmap_EXT, INTER_DEF, UNION_DEF, EXTENSION, SUBMAP_DEF, SUBSET_DEF, FUNION_DEF]
THEN1 METIS_TAC[]
THEN1 METIS_TAC[]
THEN SRW_TAC[][])

val _ = export_theory();
