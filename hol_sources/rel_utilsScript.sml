open HolKernel Parse boolLib bossLib;
open relationTheory;

val _ = new_theory "rel_utils";

val NP_NR_THEN_NR'_IMP_NP = store_thm("NP_NR_THEN_NR'_IMP_NP",
``!R R' P x. (!y. P y /\ ~R x y ==> ~R' x y)
             /\ (!y. ~R x y)
             ==> (!y. R' x y ==> ~P y)``,
SRW_TAC[][]
THEN METIS_TAC[])

val TC_IN_OR_NOT_IN_SET = store_thm("TC_IN_OR_NOT_IN_SET",
``!R s x. (!x y. x IN s /\ R x y ==> y IN s)
          ==> (!y. R^+ x y ==> ((\x y. R x y /\ y IN s)^+ x y
                               \/ (?y. R x y /\ ~(y IN s)) ))``,
NTAC 4 STRIP_TAC
THEN Q.SPEC_TAC(`x`, `x`)
THEN MATCH_MP_TAC  (SIMP_RULE(srw_ss())[] (TC_INDUCT |> Q.SPECL [`R`,
                           `(\x y. (λx y. R x y ∧ y ∈ s)⁺ x y ∨ ¬∀y. R x y ⇒ y ∈ s)`]))
THEN SRW_TAC[][]
THENL
[
   Cases_on `∃y'. R x y' ∧ y' ∉ s`
   THEN SRW_TAC[][]
   THEN FULL_SIMP_TAC(srw_ss())[]
   THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `y`)
   THEN SRW_TAC[][]
   THEN METIS_TAC[TC_RULES |> Q.SPEC `(\x y. R x y /\ y IN s )` ]
   ,
   PROVE_TAC[REWRITE_RULE[transitive_def] TC_TRANSITIVE]
   ,
   FIRST_X_ASSUM (MP_TAC o MATCH_MP TC_CASES2_E)
   THEN SRW_TAC[][]
   THEN METIS_TAC[]
   ,
   METIS_TAC[]
   ,
   METIS_TAC[]
])

val TC_REL_IN_SET = store_thm("TC_REL_IN_SET",
``!R s x. (!y. R x y ==> y IN s)
          /\ (!x y. x IN s /\ R x y ==> y IN s)
          ==> (!y. R^+ x y ==> (\x y. R x y /\ y IN s)^+ x y)``,
REPEAT STRIP_TAC
THEN METIS_TAC[TC_IN_OR_NOT_IN_SET |> Q.SPEC `R`])

val TC_IMP_TC_OR_EQ = store_thm("TC_IMP_TC_OR_EQ",
``!R x y.  R^+ x y ==> (\x y. R x y /\ ~(x = y))^+ x y \/ (x = y)``,
STRIP_TAC
THEN MATCH_MP_TAC (SIMP_RULE(srw_ss())[] (TC_INDUCT |> Q.SPECL [`R`, `(\x y. (λx y. R x y ∧ x ≠ y)⁺ x y ∨ (x = y))`]))
THEN SRW_TAC[][]
THEN1(Cases_on `x = y`
      THEN SRW_TAC[][]
      THEN METIS_TAC[TC_RULES |> Q.SPEC `(\x y. R x y /\ ~(x = y))`])
THEN PROVE_TAC[REWRITE_RULE[transitive_def] TC_TRANSITIVE])

val R_OR_TCR_IMP_TCR = store_thm("R_OR_TCR_IMP_TCR",
``!R x y. (!z. ~R y z)
          ==> (!z. R^+ x z ==> (z = y) \/ ~(!a. R x a ==> (a = y))) ``,
NTAC 4 STRIP_TAC
THEN Q.SPEC_TAC(`x`, `x`)
THEN FULL_SIMP_TAC(srw_ss())[]
THEN MATCH_MP_TAC  (SIMP_RULE(srw_ss())[] (TC_INDUCT |> Q.SPECL [`R`,
                           `(\x z. (z = y) ∨ ¬∀a. R x a ⇒ (a = y))`]))
THEN SRW_TAC[][]
THEN METIS_TAC[])

val R_imp_eq_R_TC_imp_eq = store_thm("R_imp_eq_R_TC_imp_eq",
``!R x y. (!z. R x z ==> (z = y))
          /\ (!z. ~R y z)
          ==> (!z. R^+ x z ==> (z = y))``,
METIS_TAC[R_OR_TCR_IMP_TCR])

val scc_lemma_x = store_thm("scc_lemma_x",
  ``!R x z. (R x z \/ ?y:'a. R x y /\ TC R y z) ==> TC R x z ``,
SRW_TAC[][]
THEN1 METIS_TAC[MATCH_MP AND1_THM (TC_RULES |> Q.SPEC `R`)]
THEN METIS_TAC[(REWRITE_RULE[transitive_def] TC_TRANSITIVE) |> Q.SPEC `R`, 
               MATCH_MP AND1_THM (TC_RULES |> Q.SPEC `R`)]);

val TC_CASES1_RW = store_thm("TC_CASES1_RW",
``!R x z. (R x z \/ ?y:'a. R x y /\ TC R y z) <=> TC R x z ``,
mesonLib.MESON_TAC[TC_CASES1, scc_lemma_x])

val scc_lemma_xx = store_thm("scc_lemma_xx",
``!R R' P P'. (!x y. P x /\ P' y ==> (R x y ==> R' x y) /\ ((\x y. R x y /\ P x /\ P' y)^+ x y)) 
              ==> (!x y. P x /\ P' y ==> (R'^+ x y))``,
REWRITE_TAC[TC_DEF]
THEN REPEAT STRIP_TAC
THEN LAST_ASSUM (MP_TAC o Q.SPECL[`x`, `y`])
THEN REPEAT STRIP_TAC
THEN `(R x y ⇒ R' x y) ∧
      ∀P''.
        (∀x y. (λx y. R x y ∧ P x ∧ P' y) x y ⇒ P'' x y) ∧
        (∀x y z. P'' x y ∧ P'' y z ⇒ P'' x z) ⇒
        P'' x y` by METIS_TAC[]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `P''`)
THEN REPEAT STRIP_TAC
THEN METIS_TAC[])

val scc_lemma_xxx = store_thm("scc_lemma_xxx",
  ``!R x z. (R x z \/ ?y:'a. R^+ x y /\ R y z) ==> TC R x z ``,
SRW_TAC[][]
THEN1 METIS_TAC[MATCH_MP AND1_THM (TC_RULES |> Q.SPEC `R`)]
THEN METIS_TAC[(REWRITE_RULE[transitive_def] TC_TRANSITIVE) |> Q.SPEC `R`, 
               MATCH_MP AND1_THM (TC_RULES |> Q.SPEC `R`)]);

val TC_CASES2_RW = store_thm("TC_CASES2_RW",
``!R x z. (R x z \/ ?y:'a. R^+ x y /\ R y z) <=> TC R x z ``,
mesonLib.MESON_TAC[TC_CASES2, scc_lemma_xxx])

val TC_CONJ = store_thm("TC_CONJ",
``!R P x y. ~R^+ x y
            ==> ~(\x y. R x y /\ P x y) ^+ x y``,
REWRITE_TAC[TC_DEF]
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `P'`
THEN SRW_TAC[][]
THEN METIS_TAC[])

val TC_IMP_NOT_TC_CONJ = store_thm("TC_IMP_NOT_TC_CONJ",
``!R R' P x y. ((!x y. P x  y ==> R' x y ==> R x y)                  
               /\ ~R^+ x y )
              ==>  ~((\x y. R' x y /\ P x y)^+ x y)``,
SRW_TAC[][]
THEN `¬(λx y. R x y /\ P x  y)⁺ x y` 
   by METIS_TAC[ TC_CONJ |> Q.SPECL [`R`, `(\x y. P x y)`, `x`, `y`]]
THEN FULL_SIMP_TAC(srw_ss())[TC_DEF]
THEN Q.EXISTS_TAC `P'`
THEN SRW_TAC[][]
THEN METIS_TAC[])

val CONJ_THEN_R_IMP_CONJ_THEN_TC_R = store_thm("CONJ_THEN_R_IMP_CONJ_THEN_TC_R",
``!R R' P. (!x y. P x /\ P y ==> (R x y ==> R' x y) 
           /\ ((\x y. R x y /\ P x /\ P y)^+ x y)) 
              ==> (!x y. P x /\ P y ==> (R'^+ x y))``,
REWRITE_TAC[TC_DEF]
THEN REPEAT STRIP_TAC
THEN LAST_ASSUM (MP_TAC o Q.SPECL[`x`, `y`])
THEN REPEAT STRIP_TAC
THEN `(R x y ⇒ R' x y) ∧
        ∀P'.
          (∀x y. (λx y. R x y ∧ P x ∧ P y) x y ⇒ P' x y) ∧
          (∀x y z. P' x y ∧ P' y z ⇒ P' x z) ⇒
          P' x y` by METIS_TAC[]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `P'`)
THEN REPEAT STRIP_TAC
THEN METIS_TAC[])

val CONJ_TC = store_thm("CONJ_TC",
``!R P x y. (\x y. R x y /\ P x y)^+ x y
            ==> R^+ x y``,
NTAC 2 STRIP_TAC
THEN HO_MATCH_MP_TAC TC_INDUCT
THEN SRW_TAC[][]
THEN1 SRW_TAC[][TC_RULES]
THEN PROVE_TAC[TC_RULES])

val cond_reflexive_def = Define `cond_reflexive P R = (!x. P x ==> R x x )`

val REFL_IMP_3_CONJ = store_thm("REFL_IMP_3_CONJ",
``!R'. reflexive R' ==> (!P x y. (R'^+ x y) 
               ==> ( ((\x y. R' x y /\ P x /\ P y)^+ x y) \/ (?z. ~P z /\ R'^+ x z /\ R'^+ z y)))``,
NTAC 3 STRIP_TAC
THEN HO_MATCH_MP_TAC TC_INDUCT
THEN SRW_TAC[][]
THENL
[
   Cases_on `P y`
   THEN Cases_on `P x`
   THEN1 SRW_TAC[][TC_RULES]
   THEN1( `∃z. ¬P z ∧ R'⁺ x z ∧ R'⁺ z y` by
      (Q.EXISTS_TAC `x`
      THEN SRW_TAC[][]
      THEN1( FULL_SIMP_TAC(srw_ss())[reflexive_def]
      THEN SRW_TAC[][TC_RULES])
      THEN SRW_TAC[][TC_RULES])
      THEN METIS_TAC[])
   THEN(`∃z. ¬P z ∧ R'⁺ x z ∧ R'⁺ z y` by
      (Q.EXISTS_TAC `y`
      THEN SRW_TAC[][]
      THEN1 SRW_TAC[][TC_RULES]
      THEN FULL_SIMP_TAC(srw_ss())[reflexive_def]
      THEN SRW_TAC[][TC_RULES])
      THEN METIS_TAC[])
   ,
   PROVE_TAC[REWRITE_RULE[transitive_def] TC_TRANSITIVE]
   ,
   MP_TAC(CONJ_TC |> Q.SPECL[`R'`, `(\x y. P x /\ P y)`, `x`, `x'`])
   THEN SRW_TAC[][]
   THEN `R'⁺ x z` by METIS_TAC[(MATCH_MP AND2_THM (TC_RULES |> Q.SPEC `R'`) |> Q.SPECL [`x`, `y`, `z'`] )]
   THEN `∃z. ¬P z ∧ R'⁺ x z ∧ R'⁺ z z` by
      (Q.EXISTS_TAC `z`
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[reflexive_def]
      THEN SRW_TAC[][TC_RULES])
   THEN METIS_TAC[]
   ,
   MP_TAC(CONJ_TC |> Q.SPECL[`R'`, `(\x y. P x /\ P y)`, `x'`, `y`])
   THEN SRW_TAC[][]
   THEN `R'⁺ z y` by METIS_TAC[(MATCH_MP AND2_THM (TC_RULES |> Q.SPEC `R'`) |> Q.SPECL [`z`, `x'`, `y`] )]
   THEN `∃z. ¬P z ∧ R'⁺ x z ∧ R'⁺ z z` by
      (Q.EXISTS_TAC `z`
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[reflexive_def]
      THEN SRW_TAC[][TC_RULES])
   THEN METIS_TAC[]   
   ,
   `R'⁺ x z'` by METIS_TAC[(MATCH_MP AND2_THM (TC_RULES |> Q.SPEC `R'`))]
   THEN `∃z. ¬P z ∧ R'⁺ x z ∧ R'⁺ z z` by
      (Q.EXISTS_TAC `z'`
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[reflexive_def]
      THEN SRW_TAC[][TC_RULES])
   THEN METIS_TAC[]         
])

val REFL_TC_CONJ = store_thm("REFL_TC_CONJ",
``!R R' P x y. (reflexive R' 
               /\ (!x y. P x /\ P y ==> (R' x y ==> R x y))
               /\ (~R ^+ x y ))       
              ==> ( ~(R'^+ x y) \/ (?z. ~P z /\ R'^+ x z /\ R'^+ z y))``,
SRW_TAC[][]
THEN Cases_on `¬R'⁺ x y`
THEN1 METIS_TAC[]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN MP_TAC(TC_IMP_NOT_TC_CONJ |> Q.SPECL[`R`, `R'`, `(\x y. P x /\ P y)`,  `x` ,`y`] )
THEN SRW_TAC[][]
THEN MP_TAC(REFL_IMP_3_CONJ |> Q.SPEC `R'`)
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM  (MP_TAC o Q.SPECL[`P` , `x`, `y`])
THEN SRW_TAC[][])

val TC_CASES1_NEQ =
store_thm
("TC_CASES1_NEQ",
  ``!R x z. TC R x z ==> R x z \/ ?y:'a. ~(x = y) /\ ~(y = z) /\ R x y /\ TC R y z``,
GEN_TAC
 THEN HO_MATCH_MP_TAC TC_INDUCT
 THEN mesonLib.MESON_TAC [REWRITE_RULE[transitive_def] TC_TRANSITIVE, TC_SUBSET]);

val TC_CASES2_NEQ =
store_thm
("TC_CASES2_NEQ",
  ``!R x z. TC R x z ==> R x z \/ ?y:'a. ~(x = y) /\ ~(y = z) /\ TC R x y /\ R y z``,
GEN_TAC
 THEN HO_MATCH_MP_TAC TC_INDUCT
 THEN METIS_TAC [(REWRITE_RULE[transitive_def] TC_TRANSITIVE) |> Q.SPEC `R`, TC_SUBSET |> Q.SPEC `R`]);

val _ = export_theory();
