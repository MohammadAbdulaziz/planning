#!/bin/bash
rm -rf hol_sources
cp -rf ../../hol_sources .
cd hol_sources
../../../HOL/bin/Holmake cleanAll
rm -rf utils
rm -rf filesToFix
rm *\#
rm *~
rm *HOL*
cd ..
rm factoredSystems.zip
zip -r factoredSystems.zip hol_sources/

