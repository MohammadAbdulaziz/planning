#!/bin/bash
cp -rf HOL_DIR/HOL/* HOL_DIR/HOL_tmp/
cd HOL_DIR/HOL
git pull origin master
poly < tools/smart-configure.sml
bin/build cleanAll
bin/build -expk
