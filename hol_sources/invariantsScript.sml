open HolKernel Parse boolLib bossLib;

open finite_mapTheory
open pred_setTheory
open factoredSystemTheory
open set_utilsTheory
open functionsLib;

val _ = new_theory "invariants";

val invariant_def = Define `invariant(fm:('a |-> bool)) =
                                   (!x:'a. x IN (FDOM fm) /\ (fm ' x ) 
                                           ==> (!x':'a. x <> x' /\ x' IN (FDOM fm)==> ~(fm ' x')))
                                   /\ ?x:'a. x IN FDOM fm /\ fm ' x`

val invariant_card_state_set = store_thm("invariant_card_state_set",
``!fm:('a |-> bool) fm':('a |-> bool) x:'a x':'a.
                         invariant(fm) /\ invariant(fm') /\ (FDOM fm = FDOM fm') 
                         /\ x IN FDOM fm /\ fm <> fm' /\ (fm ' x) 
                         ==> ~ (fm' ' x)``,
SRW_TAC[][invariant_def, IN_DEF, fmap_EXT]
THEN METIS_TAC[])

val invariant_not_eq_last_diff_paths = store_thm("invariant_not_eq_last_diff_paths",
``!fm:('a |-> bool) x:'a s:'a -> bool.
      invariant(fm) /\ (FDOM fm = x INSERT s) 
      /\ ~(fm ' x)
      ==> invariant(fm \\ x)``,
SRW_TAC[][fmap_domsub, DRESTRICT_DEF, invariant_def, INTER_DEF, IN_DEF, EXTENSION]
THEN METIS_TAC[])

val invariant_eq_last_state_imp_append_nempty_as = store_thm("invariant_eq_last_state_imp_append_nempty_as",
``!fm:('a |-> bool) x:'a s:'a -> bool.
                         invariant(fm) /\ (FDOM fm = s) /\ ~(x IN s)
                         ==> invariant(fm |+(x,F))``,
SRW_TAC[][invariant_def]
THEN METIS_TAC[FAPPLY_FUPDATE_THM])

val invariant_lemma_2 = store_thm("invariant_lemma_2",
``! fm'':('a |-> bool) e:'a vs:('a-> bool). 
                 (vs <> EMPTY /\ ~(e IN vs) /\ invariant(fm'') /\ (FDOM (fm'') = (e INSERT vs)) /\ fm'' ' e )==>
                 ({fm:('a |-> bool) | (FDOM fm = e INSERT vs) /\ invariant(fm)} 
                    = fm'' INSERT (IMAGE (\fm':('a |-> bool). (fm' |+ (e, F))) {fm | (FDOM fm = vs) /\ invariant(fm)}))``,
REWRITE_TAC[EXTENSION]
THEN REPEAT STRIP_TAC
THEN EQ_TAC
THEN STRIP_TAC
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[GSPEC_ETA, IN_DEF]
THEN1(Cases_on `x = fm''`
      THEN SRW_TAC[][]
      THEN Cases_on `(fm'' ' e)`
      THEN1(`~(x ' e)` by 
                 (MP_TAC(invariant_card_state_set |> Q.SPECL[`fm''`, `x`, `e`])
                  THEN SRW_TAC[][] THEN FULL_SIMP_TAC(srw_ss())[IN_DEF, EXTENSION])
            THEN Q.EXISTS_TAC `x \\ e`
            THEN SRW_TAC[][]
            THEN1(SRW_TAC[][GSYM FUPDATE_PURGE]
                  THEN MATCH_MP_TAC (GSYM FUPDATE_ELIM)
                  THEN SRW_TAC[][IN_DEF])
            THEN1(FULL_SIMP_TAC(srw_ss())[IN_DEF, EXTENSION] THEN METIS_TAC[] )
            THEN1(METIS_TAC[invariant_not_eq_last_diff_paths]))
      THEN1(FULL_SIMP_TAC(srw_ss())[] ))
THEN1(FULL_SIMP_TAC(srw_ss())[]
      THEN1(METIS_TAC[IN_DEF])
      THEN1(METIS_TAC[IN_DEF]))
THEN1(FULL_SIMP_TAC(srw_ss())[]
      THEN METIS_TAC[invariant_eq_last_state_imp_append_nempty_as, IN_DEF]))

val invariant_lemma_1 = store_thm("invariant_lemma_1",
``!vs. FINITE vs ==> ( !e. ~(e IN vs) ==> ?fm:('a|-> bool). (invariant(fm) /\ (FDOM (fm) = (e INSERT vs)) /\ fm ' e ))``,
HO_MATCH_MP_TAC FINITE_INDUCT
THEN SRW_TAC[][]
THEN1(Q.EXISTS_TAC `FEMPTY |+(e,T)`
      THEN SRW_TAC[][invariant_def])
THEN1(FIRST_X_ASSUM (MP_TAC o Q.SPEC `e'`)
      THEN SRW_TAC[][]
      THEN Q.EXISTS_TAC `fm |+(e,F)`
      THEN SRW_TAC[][]
      THEN1(MP_TAC(invariant_eq_last_state_imp_append_nempty_as|> Q.SPECL[`fm`, `e`, `e' INSERT vs`])
            THEN SRW_TAC[][])
      THEN1(SRW_TAC[][INSERT_COMM])
      THEN1(SRW_TAC[][NOT_EQ_FAPPLY])))

val invariant_lemma_3 = store_thm("invariant_lemma_3",
``?fm. {fm | (FDOM fm = {e}) ∧ invariant fm} = {fm}``,
Q.EXISTS_TAC `FEMPTY |+(e, T)`
THEN SRW_TAC[][EXTENSION, invariant_def]
THEN EQ_TAC
THEN1 (SRW_TAC[][fmap_EXT, EXTENSION]
      THEN METIS_TAC[])
THEN SRW_TAC[][])

val invariant_lemma = store_thm("invariant_lemma",
``!vs. FINITE vs 
       ==> (CARD {fm:('a|->bool) | (FDOM fm = vs) /\ invariant(fm) } = CARD vs)``,
HO_MATCH_MP_TAC FINITE_INDUCT
THEN SRW_TAC[][]
THEN1(REWRITE_TAC[GSYM (MATCH_MP AND1_THM CARD_DEF)]
      THEN `{fm | (FDOM fm = ∅) ∧ invariant fm} = EMPTY` by
           (SRW_TAC[][EXTENSION, invariant_def]
            THEN METIS_TAC[])
      THEN ASM_SIMP_TAC(srw_ss())[])
THEN1(Cases_on `vs = EMPTY`
      THEN1(SRW_TAC[][]
            THEN MP_TAC(invariant_lemma_3)
            THEN SRW_TAC[][]
            THEN ASM_SIMP_TAC(srw_ss())[] )
      THEN MP_TAC(invariant_lemma_1 |> Q.SPEC `vs`)
      THEN SRW_TAC[][]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `e`)
      THEN SRW_TAC[][]
      THEN MP_TAC(invariant_lemma_2 |> Q.SPECL[`fm`, `e`, `vs`])
      THEN SRW_TAC[][]
      THEN assume_thm_concl_after_proving_assums((REWRITE_RULE[AND_IMP_INTRO] CARD_INJ_IMAGE_2) |> Q.ISPEC `(\fm'. fm' |+ (e,F))` |> Q.SPEC `{fm | (FDOM fm = vs) ∧ invariant fm}`)
      THEN1(SRW_TAC[][]
            THEN1(SRW_TAC[][ GSYM 
                        (SIMP_RULE(srw_ss())[](INTER_DEF 
                              |> Q.ISPECL[`(\f. FDOM f = vs)`] 
                              |> Q.ISPEC `(\f:('a|->bool). invariant(f))`))]
                  THEN METIS_TAC[SIMP_RULE(srw_ss())[GSPEC_ETA] FINITE_states, FINITE_INTER])
            THEN1(METIS_TAC[FUPD11_SAME_NEW_KEY]))
      THEN `FINITE (IMAGE (λfm'. fm' |+ (e,F)) {fm | (FDOM fm = vs) ∧ invariant fm})`
                by (SRW_TAC[][ GSYM 
                        (SIMP_RULE(srw_ss())[](INTER_DEF 
                              |> Q.ISPECL[`(\f. FDOM f = vs)`] 
                              |> Q.ISPEC `(\f:('a|->bool). invariant(f))`))]
                  THEN METIS_TAC[IMAGE_FINITE, SIMP_RULE(srw_ss())[GSPEC_ETA] FINITE_states, FINITE_INTER])
      THEN MP_TAC(CARD_INSERT |> Q.ISPECL[`(IMAGE (λfm'. fm' |+ (e,F)) {fm | (FDOM fm = vs) ∧ invariant fm})`])
      THEN SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[FAPPLY_FUPDATE]))

val _ = export_theory();

