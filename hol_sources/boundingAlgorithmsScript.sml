open HolKernel Parse boolLib bossLib;
open systemAbstractionTheory;
open rich_listTheory;
open listTheory;						 
open arithmeticTheory;
open dependencyTheory
open list_utilsTheory
open arith_utilsTheory
open acycDepGraphTheory;
open acycSspaceTheory
open pred_setTheory
open set_utilsTheory
open acyclicityTheory
open relationTheory
open factoredSystemTheory
open functionsLib
open pairTheory

val _ = new_theory "boundingAlgorithms";

val N_gen_def
   = Define `((N_gen b PROB vs (vs'::lvs)) =  if dep_var_set(PROB, vs, vs') then
                                                b(prob_proj(PROB,vs)) * (N_gen b PROB  vs' lvs) +
                                                           (N_gen b PROB vs lvs)
                                              else 
                                                (N_gen b PROB vs lvs)) /\ 
                ((N_gen b PROB vs []) = b (prob_proj(PROB,vs))) `

val N_gen_is_wighted_longest_path = store_thm("N_gen_is_wighted_longest_path",
``!vs. N_gen b PROB vs lvs = wlp (\vs1 vs2. dep_var_set(PROB, vs1, vs2)) (\vs. b(prob_proj(PROB,vs))) $+ $* vs (lvs)``,
Induct_on `lvs`
THEN SRW_TAC[][N_gen_def,wlp_def])

val N_gen_valid_thm_1 = store_thm("N_gen_valid_thm_1",
``!b. (!PROB'. problem_plan_bound PROB' <= b PROB')
               ==> !lvs vs. N(PROB,vs,lvs) <= (N_gen b PROB vs lvs)``,
NTAC 2 STRIP_TAC
THEN Induct_on `lvs`
THEN SRW_TAC[][N_def, N_gen_def]
THEN FIRST_ASSUM (MP_TAC o Q.SPEC `(prob_proj (PROB,vs))`)
THEN STRIP_TAC
THEN FIRST_ASSUM (MP_TAC o Q.SPEC `vs`)
THEN STRIP_TAC
THEN FIRST_ASSUM (MP_TAC o Q.SPEC `h`)
THEN STRIP_TAC
THEN METIS_TAC[two_pow_n_is_a_bound_2, (left_right_mult_plus_leq)])

val N_gen_valid_thm = store_thm("N_gen_valid_thm",
``!b. (!(PROB': 'a problem). problem_plan_bound PROB' <= b PROB')
               ==> 
               (!lvs. ALL_DISTINCT(lvs) /\ disjoint_varset(lvs)
                     ==> 
                    (!PROB. FINITE PROB
                            /\ (prob_dom PROB = BIGUNION (set lvs)) /\ top_sorted (PROB,lvs) 
                            ==> problem_plan_bound(PROB) < SUM (MAP (\vs. (N_gen b PROB vs lvs)) lvs) + 1))``,
REWRITE_TAC[GSYM arithmeticTheory.LE_LT1]
THEN NTAC 2 STRIP_TAC
THEN SRW_TAC[][]
THEN MP_TAC((REWRITE_RULE[GSYM arithmeticTheory.LE_LT1] parent_children_main_lemma) |> Q.SPEC`lvs`)
THEN SRW_TAC[][]
THEN FIRST_ASSUM (MP_TAC o Q.SPEC `PROB`)
THEN STRIP_TAC
THEN FULL_SIMP_TAC(srw_ss())[]
THEN `problem_plan_bound PROB ≤ SUM (MAP (λvs. N (PROB,vs,lvs)) lvs)` by METIS_TAC[]
THEN `SUM (MAP (λvs. N (PROB,vs,lvs)) lvs) <= SUM (MAP (λvs. N_gen b PROB vs lvs) lvs)` by
           METIS_TAC[N_gen_valid_thm_1 |> Q.SPEC `b`,
                     (N_gen_valid_thm_2 |> INST_TYPE[alpha |-> ``:'a->bool``] ) 
                                        |> Q.SPECL[`(\vs. N (PROB,vs,lvs))`, `(\vs. (N_gen b PROB vs lvs))`]]
THEN METIS_TAC[LESS_EQ_TRANS])

(*
val N_super_gen_valid_thm_1 = store_thm("N_super_gen_valid_thm_1",
``!b P. (!PROB'. P PROB' ==> problem_plan_bound PROB' <= b PROB')
        /\ (!PROB' vs. P PROB' ==> P (prob_proj(PROB', vs) ))
        ==> ! PROB lvs vs. P PROB ==> N(PROB,vs,lvs) <= (N_gen b PROB vs lvs)``,
NTAC 3 STRIP_TAC
THEN Induct_on `lvs`
THEN SRW_TAC[][N_def, N_gen_def]
THEN METIS_TAC[(left_right_mult_plus_leq)])

val N_super_ggen_valid_thm = store_thm("N_super_ggen_valid_thm",
``!b P. (!PROB'. P PROB' ==> problem_plan_bound PROB' <= b PROB')
        /\ (!PROB' vs. P PROB' ==> P (prob_proj(PROB', vs) ))
        ==> 
           (!lvs. EVERY (\vs. vs <> EMPTY) lvs
                  /\ ALL_DISTINCT(lvs) /\ disjoint_varset(lvs)
                  /\ lvs <> [] ==> 
                  (!PROB. P PROB /\ planning_problem(PROB) 
                          /\ (FDOM PROB.I = BIGUNION (set lvs)) /\ top_sorted (PROB,lvs) 
                          ==> problem_plan_bound(PROB) < SUM (MAP (\vs. (N_gen b PROB vs lvs)) lvs) + 1)) ``,
REWRITE_TAC[GSYM arithmeticTheory.LE_LT1]
THEN NTAC 3 STRIP_TAC
THEN SRW_TAC[][]
THEN MP_TAC((REWRITE_RULE[GSYM arithmeticTheory.LE_LT1] parent_children_main_lemma) |> Q.SPEC`lvs`)
THEN SRW_TAC[][]
THEN FIRST_ASSUM (MP_TAC o Q.SPEC `PROB`)
THEN STRIP_TAC
THEN FULL_SIMP_TAC(srw_ss())[]
THEN `problem_plan_bound PROB ≤ SUM (MAP (λvs. N (PROB,vs,lvs)) lvs)` by METIS_TAC[]
THEN `SUM (MAP (λvs. N (PROB,vs,lvs)) lvs) <= SUM (MAP (λvs. N_gen b PROB vs lvs) lvs)` by METIS_TAC[N_super_gen_valid_thm_1 |> Q.SPEC `b`, (N_gen_valid_thm_2 |> INST_TYPE[alpha |-> ``:'a->bool``] ) |> Q.SPECL[`(\vs. N (PROB,vs,lvs))`, `(\vs. (N_gen b PROB vs lvs))`]]
THEN METIS_TAC[LESS_EQ_TRANS]) *)

val N_super_gen_valid_thm_1 = store_thm("N_super_gen_valid_thm_1",
``!b P. (!PROB'. P PROB' ==> problem_plan_bound PROB' <= b PROB')
        /\ (!PROB' vs'. ~(vs' INTER (prob_dom (PROB': ((α |-> β) # (α |-> β)) set ))= EMPTY) /\ P PROB' 
                                        ==> P (prob_proj(PROB', vs')))
        ==> ! PROB lvs vs. P PROB /\ ~(vs INTER (prob_dom (PROB: ((α |-> β) # (α |-> β)) set))= EMPTY)
                           /\ EVERY (\vs. (vs INTER (prob_dom (PROB: ((α |-> β) # (α |-> β)) set))) <> EMPTY) lvs
                           ==> N(PROB,vs,lvs) <= (N_gen b PROB vs lvs)``,
NTAC 4 STRIP_TAC
THEN Induct_on `lvs`
THEN SRW_TAC[][N_def, N_gen_def]
THEN METIS_TAC[(left_right_mult_plus_leq)] )

val N_super_gen_valid_thm = store_thm("N_super_gen_valid_thm",
``!b P. (!PROB': 'a problem. P PROB' ==> problem_plan_bound PROB' <= b PROB')
        /\ (!PROB' vs'. ~(vs' INTER (prob_dom PROB') = EMPTY) /\ P PROB' 
                                        ==> P (prob_proj(PROB', vs')))
        ==> 
           (!lvs. ALL_DISTINCT(lvs) /\ disjoint_varset(lvs)
                  /\ lvs <> [] ==> 
                  (!PROB. P PROB /\ FINITE PROB
                          /\ ((prob_dom PROB) = BIGUNION (set lvs)) /\ top_sorted (PROB,lvs) 
                          /\ EVERY (\vs. (vs INTER (prob_dom PROB)) <> EMPTY) lvs
                          ==> problem_plan_bound(PROB) < SUM (MAP (\vs. (N_gen b PROB vs lvs)) lvs) + 1)) ``,
REWRITE_TAC[GSYM arithmeticTheory.LE_LT1]
THEN NTAC 3 STRIP_TAC
THEN SRW_TAC[][]
THEN `EVERY (λvs. vs <> EMPTY) lvs` by
      (MP_TAC (EVERY_MONOTONIC |> INST_TYPE [alpha |-> ``:'a -> bool``]
               |> Q.SPECL[`((λvs. vs ∩ (prob_dom (PROB:'a problem)) <> EMPTY))`,`(λvs. vs <> EMPTY)`] )
       THEN SRW_TAC[][] THEN METIS_TAC[])
THEN MP_TAC((REWRITE_RULE[GSYM arithmeticTheory.LE_LT1] parent_children_main_lemma) |> Q.SPEC`lvs`)
THEN SRW_TAC[][]
THEN FIRST_ASSUM (MP_TAC o Q.SPEC `PROB`)
THEN STRIP_TAC
THEN FULL_SIMP_TAC(srw_ss())[]
THEN `problem_plan_bound PROB ≤ SUM (MAP (λvs. N (PROB,vs,lvs)) lvs)` by METIS_TAC[]
THEN `SUM (MAP (λvs. N (PROB,vs,lvs)) lvs) <= SUM (MAP (λvs. N_gen b PROB vs lvs) lvs)` by
      (MP_TAC(N_super_gen_valid_thm_1 |> INST_TYPE [beta |-> ``:bool``] |> Q.SPECL[`b:'a problem -> num`, `P`])
      THEN SRW_TAC[][]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`PROB`, `(lvs:'a set list)`])
      THEN SRW_TAC[][]
      THEN `!vs:'a set. MEM (vs:'a set) (lvs:('a set) list) ==> N (PROB,vs,lvs:('a set) list) ≤ N_gen (b:'a problem->num) PROB vs (lvs:('a set) list)` by 
            (SRW_TAC[][]
            THEN MP_TAC(EVERY_MEM |> INST_TYPE [alpha |-> ``:'a -> bool``] |> Q.SPECL[`(λvs. vs ∩ (prob_dom (PROB: 'a problem)) <> EMPTY)`])
            THEN SRW_TAC[][]
            THEN METIS_TAC[])
      THEN METIS_TAC[(N_super_gen_valid_thm_2 |> INST_TYPE[alpha |-> ``:'a->bool``] ) |> Q.SPECL[`(\vs. N (PROB,vs,lvs))`, `(\vs. (N_gen b PROB vs lvs))`]])
THEN METIS_TAC[LESS_EQ_TRANS])

val N_more_gen_valid_thm_1 = store_thm("N_more_gen_valid_thm_1",
``!vs. problem_plan_bound (prob_proj(PROB, vs)) <= (b (prob_proj(PROB, vs))) /\
   (!vs'. MEM vs' lvs ==> problem_plan_bound (prob_proj(PROB, vs')) <= (b (prob_proj(PROB, vs'))))
        ==> N_curried PROB lvs vs <= (N_gen b PROB vs lvs)``,
REWRITE_TAC[N_curried_def]
THEN Induct_on `lvs`
THEN SRW_TAC[][N_def, N_gen_def]
THEN METIS_TAC[(left_right_mult_plus_leq)])

val N_more_gen_valid_thm = store_thm("N_more_gen_valid_thm",
``(!vs'. MEM vs' lvs ==> (problem_plan_bound (prob_proj(PROB, vs'))) <= (b (prob_proj(PROB, vs')))) /\
  FINITE PROB /\ dep_DAG (PROB:'a problem) lvs
  ==> problem_plan_bound(PROB) <= SUM (MAP (\vs. (N_gen b PROB vs lvs)) lvs)``,
SRW_TAC[][]
THEN HO_MATCH_MP_TAC LESS_EQ_TRANS
THEN Q.EXISTS_TAC `(SUM (MAP (N_curried PROB lvs) lvs))`
THEN SRW_TAC[][]
THEN1 METIS_TAC[SIMP_RULE(srw_ss())[GSYM arithmeticTheory.LE_LT1] parent_children_main_lemma_curried_N_thesis]
THEN MATCH_MP_TAC N_super_gen_valid_thm_2
THEN METIS_TAC[N_more_gen_valid_thm_1])

val N_gen_eq = store_thm("N_gen_eq",
``!b1  b2 PROB vs lvs. (!x. FINITE x ==> (b1 x = b2 x)) /\ FINITE PROB
                        ==> (N_gen b1 PROB vs lvs =  N_gen b2 PROB vs lvs)``,
NTAC 2 STRIP_TAC
THEN Induct_on `lvs`
THEN SRW_TAC[][N_gen_def]
THEN METIS_TAC[finite_imp_finite_prob_proj])
				       
val N_gen_empty = store_thm("N_gen_empty",
``(!PROB'. b (prob_proj (PROB',EMPTY)) = 0)
  ==> (N_gen b PROB EMPTY lvs = 0)``,
Induct_on `lvs`
THEN SRW_TAC[][N_gen_def]);
                                     
val N_gen_def_ITP2015 = store_thm("N_gen_def_ITP2015",
``(!PROB'. b (prob_proj (PROB',EMPTY)) = 0)
   ==> !PROB vs lvs.top_sorted(PROB, lvs)
           ==>
             (N_gen b PROB vs lvs
               = b (prob_proj(PROB, vs)) * (SUM (MAP (\vs'. (N_gen b PROB vs' lvs)) (children(PROB, vs, lvs))) + 1))``,
STRIP_TAC
THEN REWRITE_TAC[LEFT_ADD_DISTRIB, MULT_RIGHT_1]
THEN Induct_on `lvs`
THEN1 SRW_TAC[][N_gen_def, children_def]
THEN SRW_TAC[][N_gen_def, children_def]
THEN SRW_TAC[][GSYM children_def]
THEN `EVERY (\vs. ~dep_var_set (PROB,vs,h)) lvs` by (SRW_TAC[][listTheory.EVERY_MEM] THEN METIS_TAC[top_sorted_cons_imp_ndep_parent])
THEN `EVERY (\vs. ~dep_var_set (PROB,vs,h)) (children(PROB, vs, lvs))` by SRW_TAC[][children_def, listTheory.EVERY_FILTER_IMP]
THEN MP_TAC(N_eq_problem_plan_bound_mult_sum_2 |> INST_TYPE [alpha |-> ``:'a -> bool``, beta |-> ``:num``]
                    |> Q.SPECL[`(\vs'. b (prob_proj (PROB,vs')) * (N_gen b PROB h lvs) +
                                       (N_gen b PROB vs' lvs))`,
                               `(\vs'. (N_gen b PROB vs' lvs))`,
                               `(\vs'. dep_var_set (PROB,vs',h))`,
                               `(children (PROB,vs,lvs))`])
THEN SRW_TAC[][]
THEN1(`(N_gen b PROB h lvs) = 0` by METIS_TAC[dep_var_set_self_empty, N_gen_empty]
      THEN SRW_TAC[][MATCH_MP AND1_THM MULT, MULT_COMM]
      THEN FIRST_X_ASSUM MATCH_MP_TAC THEN SRW_TAC[][]
      THEN METIS_TAC[top_sorted_cons_2])
THEN1(SRW_TAC[][LEFT_ADD_DISTRIB]
      THEN `(!a x y. (x = y) ==> (a + x= a + y))` by DECIDE_TAC
      THEN REWRITE_TAC[GSYM ADD_ASSOC]
      THEN FIRST_X_ASSUM MATCH_MP_TAC 
      THEN FIRST_X_ASSUM MATCH_MP_TAC 
      THEN METIS_TAC[top_sorted_cons_2])
THEN1(FIRST_X_ASSUM MATCH_MP_TAC 
      THEN METIS_TAC[top_sorted_cons_2]))

val N_gen_curried_def = Define`N_gen_curried b PROB lvs vs = N_gen b PROB vs lvs`;

val N_curried_more_gen_valid_thm = store_thm("N_curried_more_gen_valid_thm",
``(!vs'. MEM vs' lvs ==> (problem_plan_bound (prob_proj(PROB, vs'))) <= (b (prob_proj(PROB, vs')))) /\
  FINITE PROB /\ dep_DAG (PROB:'a problem) lvs
  ==> problem_plan_bound(PROB) <= SUM (MAP (N_gen_curried b PROB lvs) lvs)``,
SRW_TAC[][]
THEN HO_MATCH_MP_TAC LESS_EQ_TRANS
THEN Q.EXISTS_TAC `SUM (MAP (\vs. (N_gen b PROB vs lvs)) lvs)`
THEN SRW_TAC[][]
THEN1 METIS_TAC[N_more_gen_valid_thm]
THEN MATCH_MP_TAC N_super_gen_valid_thm_2
THEN SRW_TAC[][N_gen_curried_def])

val N_gen_curried_valid_thm = store_thm("N_gen_curried_valid_thm",
``!b. (!(PROB': 'a problem). problem_plan_bound PROB' <= b PROB') /\
      FINITE PROB /\ dep_DAG PROB lvs
               ==> problem_plan_bound(PROB) <= SUM (MAP (\vs. (N_gen_curried b PROB lvs vs)) lvs)``,
SRW_TAC[][N_gen_curried_def, dep_DAG_def]
THEN METIS_TAC[N_gen_valid_thm, top_sorted_is_top_sorted_abs,LE_LT1])

val N_gen_curried_is_wighted_longest_path = store_thm("N_gen_curried_is_wighted_longest_path",
``!vs. N_gen_curried b PROB lvs vs = wlp (\vs1 vs2. dep_var_set(PROB, vs1, vs2)) (\vs. b(prob_proj(PROB,vs))) $+ $* vs (lvs)``,
SRW_TAC[][N_gen_curried_def,N_gen_is_wighted_longest_path])

val N_gen_curried_CONG = store_thm("N_gen_curried_CONG",
``!PROB1 PROB2 lvs1 lvs2 vs1 vs2 b1 b2.
  (PROB1 = PROB2)
  /\ (lvs1 = lvs2)
  /\ (vs1 = vs2)
  /\ (b1 (prob_proj(PROB1,vs1))= b2 (prob_proj(PROB2,vs2)))
  /\ (!vs. MEM vs lvs2 ==> (b1 (prob_proj(PROB1,vs))= b2 (prob_proj(PROB2,vs))))
  ==> (N_gen_curried b1 PROB1 lvs1 vs1 = N_gen_curried b2 PROB2 lvs2 vs2)``,
SRW_TAC[][N_gen_curried_is_wighted_longest_path]
THEN MATCH_MP_TAC wlp_congruence_rule
THEN SRW_TAC[][])

local open DefnBase
in
  val _ = read_congs()
  val _ = write_congs (N_gen_curried_CONG::read_congs())
end;

val _ = DefnBase.read_congs;
val _ = DefnBase.add_cong N_gen_curried_CONG

val N_gen_curried_def_ITP2015 = store_thm("N_gen_curried_def_ITP2015",
``(!PROB'. b (prob_proj (PROB',EMPTY)) = 0)
   ==> !PROB vs lvs.top_sorted_curried PROB lvs
           ==>
             (N_gen_curried b PROB lvs vs
               = b (prob_proj(PROB, vs)) * (SUM (MAP (N_gen_curried b PROB lvs) (children_curried PROB lvs vs)) + 1))``,
REWRITE_TAC[Once MAP_LAMBDA_ABSTRACTION_thm, top_sorted_curried_uncurried_equiv_thm, children_curried_uncurried_equiv_thm]
THEN REWRITE_TAC[N_gen_curried_def]
THEN METIS_TAC[N_gen_def_ITP2015])

(* val N_gen_curried_valid_thm = store_thm("N_gen_curried_valid_thm",
``!b. (!PROB':'a problem. problem_plan_bound PROB' <= b PROB')
               ==> 
               (!lvs.ALL_DISTINCT(lvs) /\ disjoint_varset(lvs)
                     ==> 
                    (!PROB. FINITE PROB /\ ((prob_dom PROB) = BIGUNION (set lvs)) /\ top_sorted_curried PROB lvs 
                            ==> problem_plan_bound(PROB) < SUM (MAP (N_gen_curried b PROB lvs) lvs) + 1))``,
REWRITE_TAC[Once MAP_LAMBDA_ABSTRACTION_thm, top_sorted_curried_uncurried_equiv_thm, children_curried_uncurried_equiv_thm]
THEN REWRITE_TAC[N_gen_curried_def]
THEN SRW_TAC[][N_gen_valid_thm])*)

val hybrid_algo_def = Define
`hybrid_algo f1 f2 PROB = if 2 <= LENGTH (f1 PROB) then 
                            SUM (MAP (N_curried PROB (f1 PROB)) (f1 PROB))
                          else if(f2 PROB <> EMPTY) then
                            let xx = CHOICE (f2 PROB)
                                 in MAX_SET {S (FST xx) (SND xx) PROB s' | s' | s' IN (valid_states (prob_proj(PROB, FST xx)))}
                          else
                            problem_plan_bound PROB`

val hybrid_algorithm_valid = store_thm("hybrid_algorithm_valid",
``FINITE (PROB:'a problem) /\ ALL_DISTINCT(f1 PROB) /\ disjoint_varset(f1 PROB) /\
 ((prob_dom PROB) = BIGUNION (set (f1 PROB))) /\ top_sorted_curried PROB (f1 PROB) /\
  (!vs lss. (vs,lss) IN (f2 PROB) ==> top_sorted_abs (state_successors (prob_proj(PROB,vs))) lss /\
                        ((set lss) = valid_states (prob_proj(PROB,vs))))
  ==> 
  problem_plan_bound PROB <= hybrid_algo f1 f2 PROB``,
SRW_TAC[][hybrid_algo_def]
THEN1 METIS_TAC[parent_children_main_lemma_curried_N, LE_LT1]
THEN1 METIS_TAC[problem_plan_bound_S_bound, CHOICE_DEF, pairTheory.PAIR])

val hybrid_algorithm_valid_thesis = store_thm("hybrid_algorithm_valid_thesis",
``FINITE (PROB:'a problem) /\ dep_DAG PROB (f1 PROB) /\
  (!vs lss. (vs,lss) IN (f2 PROB) ==> sspace_DAG (prob_proj(PROB,vs)) lss)
  ==> 
  problem_plan_bound PROB <= hybrid_algo f1 f2 PROB``,
SRW_TAC[][hybrid_algorithm_valid, dep_DAG_def, sspace_DAG_def, top_sorted_curried_uncurried_equiv_thm, GSYM top_sorted_is_top_sorted_abs])

val S_gen_def = Define`S_gen b vs lss PROB s = wlp (state_successors (prob_proj(PROB,vs)))
                                 (\s. b(snapshot PROB s ))
                                 MAX (\x y. x + y + 1) s lss`

val S_gen_ell_in_PLS_leq_2_pow_n_thesis = store_thm("S_gen_ell_in_PLS_leq_2_pow_n_thesis",
``
  FINITE (PROB:'a problem)  /\ sspace_DAG (prob_proj(PROB,vs)) lss /\
  s IN valid_states PROB /\ as IN valid_plans PROB
  ==> ?as'. (exec_plan(s,as') = exec_plan(s,as)) /\ sublist as' as /\
            LENGTH as' <= S_gen problem_plan_bound vs lss PROB (DRESTRICT s vs)``,
METIS_TAC[problem_plan_bound_S_bound_2nd_step_thesis, S_gen_def, S_def])

val S_gen_ell_bound_main_lemma = store_thm("S_gen_ell_bound_main_lemma",
``FINITE (PROB:'a problem) /\ sspace_DAG (prob_proj(PROB,vs)) lss
  ==> problem_plan_bound PROB <= MAX_SET {S_gen problem_plan_bound vs lss PROB s' | s' | s' IN (valid_states (prob_proj(PROB, vs)))}``,
SRW_TAC[][S_gen_def]
THEN METIS_TAC[SIMP_RULE(srw_ss())[S_def] problem_plan_bound_S_bound_thesis])

val S_gen_in_MPLS_leq_2_pow_n = store_thm("S_gen_in_MPLS_leq_2_pow_n",
``FINITE (PROB:'a problem) /\ (set lss = (valid_states(prob_proj (PROB,vs))))/\ 
  (!s. MEM s lss ==> problem_plan_bound  (snapshot PROB s) <= b(snapshot PROB s))
  ==> MAX_SET {S_gen problem_plan_bound vs lss PROB s' | s' | s' IN (valid_states (prob_proj(PROB, vs)))} <= MAX_SET {S_gen b vs lss PROB s' | s' | s' IN (valid_states (prob_proj(PROB, vs)))}``,
SRW_TAC[][]
THEN MATCH_MP_TAC MAX_SET_LEQ_MAX_SET
THEN SRW_TAC[][]
THEN1(SRW_TAC[][EXTENSION]
      THEN METIS_TAC[finite_imp_finite_prob_proj, valid_states_nempty])
THEN1(SRW_TAC[][GSYM IMAGE_DEF]
      THEN METIS_TAC[IMAGE_FINITE, finite_imp_finite_prob_proj, FINITE_valid_states])
THEN1(Q.EXISTS_TAC `S_gen b vs lss PROB s'`
      THEN SRW_TAC[][]
      THEN1 METIS_TAC[]
      THEN1(SRW_TAC[][S_gen_def]
            THEN MATCH_MP_TAC weight_fun_leq_imp_lp_leq
            THEN SRW_TAC[][]
            THEN1(SRW_TAC[][increasing_def] THEN DECIDE_TAC)
            THEN1 SRW_TAC[][increasing_def])))

val S_gen_bound_main_lemma = store_thm("S_gen_bound_main_lemma",
``FINITE (PROB:'a problem) /\ sspace_DAG (prob_proj (PROB,vs)) lss /\ 
  (!s. MEM s lss ==> problem_plan_bound  (snapshot PROB s) <= b(snapshot PROB s))
  ==> problem_plan_bound PROB <= MAX_SET {S_gen b vs lss PROB s' | s' | s' IN (valid_states (prob_proj(PROB, vs)))}``,
SRW_TAC[][]
THEN MATCH_MP_TAC LESS_EQ_TRANS
THEN Q.EXISTS_TAC `MAX_SET {S_gen problem_plan_bound vs lss PROB s' | s' | s' IN (valid_states (prob_proj(PROB, vs)))}`
THEN SRW_TAC[][]
THEN METIS_TAC[S_gen_ell_bound_main_lemma,
               S_gen_in_MPLS_leq_2_pow_n, sspace_DAG_def])

val parent_children_main_lemma_N_gen_curried_thesis = store_thm("parent_children_main_lemma_N_gen_curried_thesis",
``!lvs PROB:'a problem. FINITE PROB /\ dep_DAG PROB lvs
                        ==> problem_plan_bound(PROB) < (SUM ( MAP (N_gen_curried problem_plan_bound PROB lvs) lvs)) + 1``,
METIS_TAC[parent_children_main_lemma_curried_N_thesis, N_gen_curried_is_wighted_longest_path, N_curried_is_wighted_longest_path])

val termination_goal_1_1 = store_thm("termination_goal_1_1",
``
!PROB f1 x projPROB.
   (!vs. MEM vs (f1 PROB) ==> vs PSUBSET prob_dom PROB) 
   /\ MEM x (f1 PROB) /\ FINITE PROB
   /\ (?vs. MEM vs (f1 PROB) /\ (projPROB = prob_proj (PROB,vs)))
   ==> CARD (prob_dom projPROB) < CARD (prob_dom PROB)``,
SRW_TAC[][]
THEN `(prob_dom (prob_proj (PROB,vs))) PSUBSET (prob_dom PROB)`by
       (SRW_TAC[][]
        THEN METIS_TAC[SUBSET_PSUBSET_TRANS,dom_prob_proj])
THEN METIS_TAC[CARD_PSUBSET, FINITE_prob_dom])

val termination_goal_1 = store_thm("termination_goal_1",
``!f1 PROB x projPROB.
   FINITE PROB 
   /\ (!vs. MEM vs (f1 PROB) ==> vs PSUBSET prob_dom PROB)
   /\ MEM x (f1 PROB)
   /\ (?vs. MEM vs (f1 PROB) /\ (projPROB = prob_proj (PROB,vs)))
   ==> CARD projPROB + CARD (prob_dom projPROB) <
          CARD PROB + CARD (prob_dom PROB)``,
SRW_TAC[][]
THEN MATCH_MP_TAC add_less_3
THEN SRW_TAC[][]
THEN1 METIS_TAC[card_proj_leq]
THEN METIS_TAC[termination_goal_1_1])

val termination_goal_2 = store_thm("termination_goal_2",
``!f2 f1 PROB p_1 p_2 snapshotPROB.
  FINITE PROB
  /\ (?vs. MEM vs (f1 PROB) /\ ~(vs PSUBSET prob_dom PROB)) /\
  (f2 PROB <> EMPTY /\ !vs lss s. (vs,lss) IN f2 PROB
   /\ MEM s lss
   ==> snapshot PROB s PSUBSET PROB)
  /\ ((p_1,p_2) = CHOICE (f2 PROB))
  /\ (?s. MEM s p_2 /\ (snapshotPROB = snapshot PROB s))
  ==> CARD snapshotPROB + CARD (prob_dom snapshotPROB) <
         CARD PROB + CARD (prob_dom PROB)``,
SRW_TAC[][]
THEN MATCH_MP_TAC add_less
THEN SRW_TAC[][]
THEN1 METIS_TAC[CARD_PSUBSET, CHOICE_DEF]
THEN METIS_TAC[CARD_SUBSET, prob_subset_dom_subset, snapshot_subset, FINITE_prob_dom]);

val hybrid_algo_gen_def = tDefine "hybrid_algo_gen"
`hybrid_algo_gen f1 f2 PROB =
  if FINITE PROB then
    if (!vs. (MEM vs (f1 PROB) ==> vs PSUBSET (prob_dom PROB))) then 
      let lvs = f1 PROB in
      SUM (MAP (N_gen_curried 
                  (\projPROB. if (?vs. MEM vs lvs /\ (projPROB = prob_proj(PROB,vs))) then (hybrid_algo_gen f1 f2 projPROB) else 0)
                  PROB lvs) lvs)
    else if(f2 PROB <> EMPTY /\ (!vs lss s. (vs,lss) IN (f2 PROB) /\ MEM s lss ==> (snapshot PROB s) PSUBSET PROB)) then
      let (vs,lss) = CHOICE (f2 PROB)
           in MAX_SET {S_gen 
                        (\snapshotPROB.
                           if (?(s:(α |-> β)).
                                 (MEM s lss) /\
                                 (snapshotPROB = (snapshot PROB s))) then
                            (hybrid_algo_gen f1 f2 snapshotPROB)
                          else 0)
                             vs lss PROB s' | s' | s' IN (valid_states (prob_proj(PROB, vs)))}
    else
      problem_plan_bound PROB
  else
    0`
(WF_REL_TAC `measure (\(a,b,c). CARD c + CARD  (prob_dom c))`
THEN METIS_TAC[termination_goal_1, termination_goal_2]);

val FINITE_PROB_hybrid_algo_gen_def = store_thm("FINITE_PROB_hybrid_algo_gen_def",
``FINITE PROB
  ==> (hybrid_algo_gen f1 f2 PROB = 
                          if (!vs. (MEM vs (f1 PROB) ==> vs PSUBSET (prob_dom PROB)))  then 
                            let lvs = f1 PROB in
                            SUM (MAP (N_gen_curried 
                                        (\projPROB. if (?vs. MEM vs lvs /\ (projPROB = prob_proj(PROB,vs))) then (hybrid_algo_gen f1 f2 projPROB) else 0)
                                        PROB lvs) lvs)
                          else if(f2 PROB <> EMPTY /\ (!vs lss s. (vs,lss) IN (f2 PROB) /\ MEM s lss ==> (snapshot PROB s) PSUBSET PROB)) then
                            let (vs,lss)=CHOICE (f2 PROB)
                                 in MAX_SET {S_gen 
                                              (\snapshotPROB.
                                                  if (?(s:(α |-> β)).
                                                         (MEM s lss) /\
                                                         (snapshotPROB = (snapshot PROB s))) then
                                                    (hybrid_algo_gen f1 f2 snapshotPROB)
                                                  else 0)
                                             vs lss PROB s' | s' | s' IN (valid_states (prob_proj(PROB, vs)))}
                          else
                            problem_plan_bound PROB)``,
SRW_TAC[][Once hybrid_algo_gen_def])

val MAP_N_gen_remove_ite = store_thm("MAP_N_gen_remove_ite",
``!lvs. 
(MAP (N_gen_curried (\projPROB. if (?vs. MEM vs lvs /\ (projPROB = prob_proj(PROB,vs))) then (hybrid_algo_gen f1 f2 projPROB) else 0)
                                        PROB lvs) lvs)
      = (MAP (N_gen_curried (hybrid_algo_gen f1 f2) PROB lvs) lvs)``,
SRW_TAC[][]
THEN `(MAP (\vs. N_gen_curried (\projPROB. if (?vs. MEM vs lvs /\ (projPROB = prob_proj(PROB,vs))) then (hybrid_algo_gen f1 f2 projPROB) else 0)
                                        PROB lvs vs) lvs)
      = (MAP (\vs. N_gen_curried (hybrid_algo_gen f1 f2) PROB lvs vs) lvs)` by
  (SRW_TAC[][N_gen_curried_is_wighted_longest_path]
   THEN HO_MATCH_MP_TAC map_wlp_ite_weights
   THEN METIS_TAC[])
THEN METIS_TAC[])

val IMAGE_S_gen_remove_ite = store_thm("MAP_N_gen_remove_ite",
``
!vs lss.
((set lss) = valid_states (prob_proj(PROB,vs))) ==> 
(MAX_SET {S_gen
           (\snapshotPROB.
                if (?(s'':(α |-> β)).
                         (MEM s'' lss) /\
                         (snapshotPROB = (snapshot PROB s'')))
                then (hybrid_algo_gen f1 f2 snapshotPROB)
                else 0)
            vs lss PROB s' | s' | s' IN (valid_states (prob_proj(PROB, vs)))}
  = MAX_SET {S_gen
              (hybrid_algo_gen f1 f2)
              vs lss PROB s' | s' | s' IN (valid_states (prob_proj(PROB, vs)))})``,
SRW_TAC[][GSYM IMAGE_DEF]
THEN `!x1 x2. (x1 = x2) ==> (MAX_SET x1 = MAX_SET x2)` by SRW_TAC[][]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][S_gen_def]
THEN HO_MATCH_MP_TAC img_wlp_ite_weights
THEN SRW_TAC[][]
THEN METIS_TAC[])

val FINITE_no_ite_PROB_hybrid_algo_gen_def = store_thm("FINITE_no_ite_PROB_hybrid_algo_gen_def",
``FINITE PROB /\ (!vs lss. (vs,lss) IN (f2 PROB) ==> ((set lss) = valid_states (prob_proj(PROB,vs))))
              ==> (hybrid_algo_gen f1 f2 PROB = 
                          if (!vs. (MEM vs (f1 PROB) ==> vs PSUBSET (prob_dom PROB))) then 
                            let lvs = f1 PROB in
                            SUM (MAP (N_gen_curried 
                                        (hybrid_algo_gen f1 f2)
                                        PROB lvs) lvs)
                          else if(f2 PROB <> EMPTY /\ (!vs lss s. (vs,lss) IN (f2 PROB) /\ MEM s lss ==> (snapshot PROB s) PSUBSET PROB)) then
                            let (vs,lss) = CHOICE (f2 PROB)
                                 in MAX_SET {S_gen (hybrid_algo_gen f1 f2)
                                             vs lss PROB s' | s' | s' IN (valid_states (prob_proj(PROB, vs)))}
                          else
                            problem_plan_bound PROB)``,
SRW_TAC[][ FINITE_PROB_hybrid_algo_gen_def]
THEN1 METIS_TAC[MAP_N_gen_remove_ite]
THEN1 METIS_TAC[MAP_N_gen_remove_ite]
THEN MATCH_MP_TAC IMAGE_S_gen_remove_ite
THEN METIS_TAC[PAIR, CHOICE_DEF])

val FINITE_no_ite_PROB_hybrid_algo_gen_def_thesis = store_thm("FINITE_no_ite_PROB_hybrid_algo_gen_def_thesis",
``FINITE PROB /\ (!vs lss. (vs,lss) IN (f2 PROB) ==> sspace_DAG (prob_proj(PROB,vs)) lss)
              ==> (hybrid_algo_gen f1 f2 PROB = 
                          if (!vs. (MEM vs (f1 PROB) ==> vs PSUBSET (prob_dom PROB))) then 
                            let lvs = f1 PROB in
                            SUM (MAP (N_gen_curried 
                                        (hybrid_algo_gen f1 f2)
                                        PROB lvs) lvs)
                          else if(f2 PROB <> EMPTY /\ (!vs lss s. (vs,lss) IN (f2 PROB) /\ MEM s lss ==> (snapshot PROB s) PSUBSET PROB)) then
                            let (vs,lss) = CHOICE (f2 PROB)
                                 in MAX_SET {S_gen (hybrid_algo_gen f1 f2)
                                             vs lss PROB s' | s' | s' IN (valid_states (prob_proj(PROB, vs)))}
                          else
                            problem_plan_bound PROB)``,
REPEAT STRIP_TAC
THEN MATCH_MP_TAC FINITE_no_ite_PROB_hybrid_algo_gen_def
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[sspace_DAG_def])

val hybrid_algo_gen_ind_thm = fetch "-" "hybrid_algo_gen_ind"
val hybrid_algo_gen_def_thm = fetch "-" "hybrid_algo_gen_def"

val recursive_hybrid_algorithm_valid_thesis = store_thm("recursive_hybrid_algorithm_valid_thesis",
``!f1 f2 PROB. FINITE (PROB:'a problem) /\
  (!PROB'. dep_DAG PROB' (f1 PROB') /\
   (!vs lss. (vs,lss) IN (f2 PROB') ==> sspace_DAG (prob_proj(PROB',vs)) lss))
            ==> (problem_plan_bound PROB <= hybrid_algo_gen f1 f2 PROB)``,
HO_MATCH_MP_TAC hybrid_algo_gen_ind_thm
THEN SRW_TAC[][]
THEN Cases_on `(!vs. MEM vs (f1 PROB) ==> vs PSUBSET prob_dom PROB)`
THEN1(FULL_SIMP_TAC(srw_ss())[sspace_DAG_def]
      THEN SRW_TAC[][FINITE_no_ite_PROB_hybrid_algo_gen_def]
      THEN FULL_SIMP_TAC(srw_ss())[GSYM sspace_DAG_def]
      THEN SRW_TAC[][N_gen_curried_def]
      THEN MATCH_MP_TAC N_curried_more_gen_valid_thm
      THEN SRW_TAC[][]
      THEN1(FIRST_X_ASSUM (MATCH_MP_TAC o SIMP_RULE(srw_ss())[AND_IMP_INTRO])
            THEN Q.EXISTS_TAC `vs'`
            THEN SRW_TAC[][]
            THEN METIS_TAC[finite_imp_finite_prob_proj])
      THEN1 METIS_TAC[finite_imp_finite_prob_proj])      
THEN1(Cases_on `(f2 PROB <> EMPTY /\ !vs lss s. (vs,lss) IN f2 PROB /\ MEM s lss ==> snapshot PROB s PSUBSET PROB)`
      THEN1(FULL_SIMP_TAC(srw_ss())[sspace_DAG_def]
            THEN SRW_TAC[][FINITE_no_ite_PROB_hybrid_algo_gen_def]
            THEN1 METIS_TAC[]
            THEN FULL_SIMP_TAC(srw_ss())[GSYM sspace_DAG_def]
            THEN MATCH_MP_TAC S_gen_bound_main_lemma
            THEN SRW_TAC[][]
            THEN1 METIS_TAC[PAIR, CHOICE_DEF]
            THEN LAST_X_ASSUM (MATCH_MP_TAC o SIMP_RULE(srw_ss())[AND_IMP_INTRO])
            THEN SRW_TAC[][]
            THEN METIS_TAC[FINITE_snapshot]
            THEN Q.EXISTS_TAC `vs'`
            THEN SRW_TAC[][]
            THEN METIS_TAC[finite_imp_finite_prob_proj])
      THEN1(FULL_SIMP_TAC(srw_ss())[sspace_DAG_def]
            THEN SRW_TAC[][FINITE_no_ite_PROB_hybrid_algo_gen_def]
            THEN METIS_TAC[])))

val _ = export_theory();
