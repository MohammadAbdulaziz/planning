open HolKernel Parse boolLib bossLib;
open rich_listTheory;
open factoredSystemTheory;
open pred_setTheory;
open sublistTheory;
open arithmeticTheory;
open lcsymtacs
open actionSeqProcessTheory
open finite_mapTheory
open set_utilsTheory
open list_utilsTheory
open functionsLib

val _ = new_theory "topologicalProps";
 
val PLS_charles_def = Define `PLS_charles(s, as, PROB) =
   {LENGTH as' | (exec_plan (s,as') = exec_plan (s,as)) /\ as' IN valid_plans PROB}`

val MPLS_charles_def = Define `MPLS_charles(PROB)
    =   {MIN_SET (PLS_charles(s, as, PROB)) | s, as | s IN valid_states PROB /\ as IN valid_plans PROB}`;

(* val MPLS_def = Define `MPLS PROB
    =   {MIN_SET(IMAGE LENGTH PLS(PROB, s1, s2)) |  (FDOM (s1) = FDOM(PROB.I)) /\ (FDOM (s2) = FDOM(PROB.I))}`; *)

val problem_plan_bound_charles_def = Define `problem_plan_bound_charles PROB
= MAX_SET(MPLS_charles(PROB))` ;

val PLS_stage_1_def = Define `PLS_stage_1(s, as) =
    (IMAGE LENGTH {as' |(exec_plan(s, as') = exec_plan(s, as))} )`

val MPLS_stage_1_def = Define `MPLS_stage_1 PROB
    =  (IMAGE (\ (s, as). MIN_SET (PLS_stage_1(s, as)))
       	      {(s, as) |  s IN (valid_states (PROB)) /\ (as IN valid_plans(PROB))})`;

val problem_plan_bound_stage_1_def = Define `problem_plan_bound_stage_1(PROB: 'a problem)
= MAX_SET(MPLS_stage_1(PROB))` ;

val PLS_def = Define `PLS (s, as) =
    (IMAGE LENGTH {as' |(exec_plan(s, as') = exec_plan(s, as)) /\ sublist as' as} )`

val MPLS_def = Define `MPLS (PROB)
    =  (IMAGE (\ (s, as). MIN_SET (PLS(s, as)))
       	      {(s, as) |  (s IN (valid_states PROB)) /\ (as IN (valid_plans PROB))})`;

(* val MPLS_def = Define `MPLS(PROB)
    =   {MIN_SET(IMAGE LENGTH PLS(PROB, s1, s2)) |  (FDOM (s1) = FDOM(PROB.I)) /\ (FDOM (s2) = FDOM(PROB.I))}`; *)

val problem_plan_bound_def = Define `problem_plan_bound PROB
= MAX_SET(MPLS(PROB))` ;

val expanded_problem_plan_bound_thm_1 = store_thm("expanded_problem_plan_bound_thm_1",
``!PROB. problem_plan_bound PROB = MAX_SET
                         (IMAGE (λ(s,as). MIN_SET (PLS (s,as)))
                              {(s,as) | (s IN (valid_states PROB)) ∧ (as IN valid_plans PROB)})``,
SRW_TAC[][problem_plan_bound_def, MPLS_def]);

val expanded_problem_plan_bound_thm = store_thm("expanded_problem_plan_bound_thm",
``!PROB. problem_plan_bound PROB = MAX_SET{MIN_SET (PLS (s,as)) |
                               s ∈ valid_states PROB ∧ as ∈ valid_plans PROB}``,
SRW_TAC[][IMAGE_DEF, GSYM pairTheory.PAIR, pairTheory.UNCURRY, problem_plan_bound_def, MPLS_def, valid_states_def, valid_plans_def]
THEN AP_TERM_TAC
THEN SRW_TAC[boolSimps.DNF_ss][EXTENSION]);

(* ----------------------------------------------------------------------
    recurrence diameter
   ---------------------------------------------------------------------- *)

val valid_path_def = Define`
  (valid_path Pi [] ⇔ T) ∧
  (valid_path Pi [s] ⇔ (s IN valid_states (Pi))) ∧
  (valid_path Pi (s1::s2::rest) ⇔
     (s1 IN valid_states Pi) ∧
     (∃a. a ∈ Pi ∧ (exec_plan(s1,[a]) = s2)) ∧
     valid_path Pi (s2::rest))`;

val valid_path_ITP2015_def = store_thm("valid_path_ITP2015_def",
``(valid_path Pi [] ⇔ T) ∧
  (valid_path Pi [s] ⇔ ( s IN (valid_states Pi))) ∧
  (valid_path Pi (s1::s2::rest) ⇔
     (s1 IN (valid_states Pi)) ∧
     (∃a. a ∈ Pi ∧ (exec_plan(s1,[a]) = s2)) ∧
     valid_path Pi (s2::rest))``,
SRW_TAC[][valid_path_def, valid_states_def]);

val _ = export_rewrites ["valid_path_def"]

val RD_def = Define`
  RD (Pi : 'a problem) =
    MAX_SET { LENGTH p | valid_path Pi p ∧ ALL_DISTINCT p } - 1`

val RD_def = Define`
  RD (Pi : 'a problem) =
    MAX_SET { LENGTH p - 1| valid_path Pi p ∧ ALL_DISTINCT p }`

val in_PLS_leq_2_pow_n = store_thm("in_PLS_leq_2_pow_n",
``!(PROB:'a problem) s as. FINITE PROB /\ (s IN (valid_states PROB)) /\ (as IN (valid_plans PROB))
	==>
	?x. x IN PLS(s, as) /\ x <= 2 ** CARD(prob_dom PROB) - 1 ``,
SRW_TAC[][PLS_def]
THEN MP_TAC (main_lemma |> Q.SPECL [`PROB`, `as`, `s`])
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `LENGTH (as')`
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `as'`
THEN FULL_SIMP_TAC(srw_ss())[]);

val in_MPLS_leq_2_pow_n = store_thm("in_MPLS_leq_2_pow_n",
``!(PROB:'a problem) x. FINITE PROB /\ (x IN MPLS(PROB)) ==> x <= 2 ** CARD(prob_dom PROB) - 1``,
SRW_TAC[][MPLS_def]
THEN MP_TAC (in_PLS_leq_2_pow_n
		|> Q.SPEC `PROB`
		|> Q.SPEC `s`
		|> Q.SPEC `as`)
THEN SRW_TAC[][]
THEN MP_TAC(mem_le_imp_MIN_le 
			|> Q.SPEC `PLS (s:'a state, as:'a action list)`
			|> Q.SPEC `2 ** CARD (prob_dom (PROB:'a problem)) - 1`)
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]);

val FINITE_MPLS = store_thm(
  "FINITE_MPLS",
  `` FINITE (Pi:'a problem) ==> FINITE (MPLS Pi)``,
  strip_tac >> match_mp_tac mems_le_finite >>
  metis_tac[in_MPLS_leq_2_pow_n]);

val statelist'_def = Define`
  (statelist' s [] = [s]) ∧
  (statelist' s (a::as) = s :: statelist' (state_succ s a) as)`;
val _ = export_rewrites ["statelist'_def"]

val LENGTH_statelist' = store_thm(
  "LENGTH_statelist'[simp]",
  ``∀as s. LENGTH (statelist' s as) = LENGTH as + 1``,
  Induct >> simp[]);
val _ = augment_srw_ss [rewrites [valid_action_valid_succ]]

val valid_path_statelist' = store_thm(
  "valid_path_statelist'",
  ``∀as s. as IN (valid_plans Pi) ∧ s IN (valid_states Pi) ⇒
           valid_path Pi (statelist' s as)``,
  Induct >> simp[] >> Cases_on `as` >> fs[exec_plan_def] >>
  metis_tac[valid_plan_valid_head, valid_plan_valid_tail, valid_action_valid_succ]);

val statelist'_exec_plan = store_thm(
  "statelist'_exec_plan",
  ``∀as s p. (statelist' s as = p) ⇒ (exec_plan(s,as) = LAST p)``,
  Induct >> simp[exec_plan_def] >> Cases_on `as` >> fs[exec_plan_def]);

val statelist'_EQ_NIL = store_thm(
  "statelist'_EQ_NIL[simp]",
  ``(statelist' s as ≠ [])``,
  Cases_on `as` >> simp[]);

val statelist'_TAKE = store_thm(
  "statelist'_TAKE",
  ``∀as s p.
      (statelist' s as = p) ⇒
      ∀n. n ≤ LENGTH as ⇒ (exec_plan(s, TAKE n as) = EL n p)``,
  Induct >> simp[exec_plan_def] >>
  qx_genl_tac [`a`, `s`, `n`] >>
  `(n = 0) ∨ ∃m. n = SUC m` by (Cases_on `n` >> simp[]) >>
  simp[exec_plan_def]);

val RD_bounds_sublistD = store_thm(
  "RD_bounds_sublistD",
  ``∀Pi. FINITE Pi ⇒ problem_plan_bound Pi ≤ RD Pi``,
simp[RD_def, problem_plan_bound_def] >> qx_gen_tac `Pi` >> strip_tac >>
  DEEP_INTRO_TAC MAX_SET_ELIM >> simp[FINITE_MPLS] >> dsimp[MPLS_def] >>
  qx_genl_tac [`s`, `as`] >> strip_tac >>
  qabbrev_tac `limit = MIN_SET (PLS(s,as))` >>
  DEEP_INTRO_TAC MAX_SET_ELIM >> dsimp[] >> rpt conj_tac
  >- ((* FINITE { LENGTH p | ... } *)
      qmatch_abbrev_tac `FINITE ss` >>
      `ss = IMAGE (\x. LENGTH x - 1) {p | valid_path Pi p ∧ ALL_DISTINCT p}`
        by simp[Abbr`ss`, EXTENSION] >> markerLib.RM_ABBREV_TAC "ss" >>
      pop_assum SUBST1_TAC >> 
      match_mp_tac IMAGE_FINITE >>
      match_mp_tac SUBSET_FINITE_I >>
      qexists_tac `{ p | ALL_DISTINCT p ∧ set p ⊆ valid_states Pi}` >>
      conj_tac
      >- (match_mp_tac FINITE_ALL_DISTINCT_LISTS >> 
          simp[FINITE_valid_states]) >>
      simp[SUBSET_DEF] >> Induct >> simp[] >> rpt strip_tac >>
      Q.RENAME1_TAC `valid_path Pi (s1::t)` >> Cases_on `t` >> fs[])
  >- (simp[Once EXTENSION] >> disch_then (qspec_then `[]` mp_tac) >>
      simp[]) >>
  qx_gen_tac `p` >> rpt strip_tac >> qunabbrev_tac `limit` >>
  DEEP_INTRO_TAC MIN_SET_ELIM >> conj_tac
  >- (simp[PLS_def, EXTENSION] >> metis_tac[sublist_refl]) >>
  rpt strip_tac >> Q.RENAME1_TAC `n ∈ PLS (s, as)` >>
  Q.UNDISCH_THEN `n ∈ PLS (s,as)` mp_tac >>
  simp[PLS_def] >> dsimp[] >> qx_gen_tac `as'` >> rpt strip_tac >>
  rw[] >>
  qabbrev_tac `p' = statelist' s as'` >>
  `LENGTH as' = LENGTH p' - 1` by simp[Abbr`p'`] >>
  simp[] >>
  `1 + (LENGTH p − 1) = LENGTH p - 1 + 1` by DECIDE_TAC >>
  simp[] >>
  first_x_assum match_mp_tac >> conj_tac
  >- metis_tac[valid_path_statelist', sublist_valid_plan] >>
  spose_not_then assume_tac >>
  `∃rs pfx drop tail. p' = pfx ++ rs::drop ++ rs::tail`
    by metis_tac[nondistinct_repeated_segment] >>
  qunabbrev_tac `p'` >>
  qabbrev_tac `p' = pfx ++ rs::drop ++ rs::tail` >>
  qabbrev_tac `pfxn = LENGTH pfx` >>
  `EL pfxn p' = rs`
    by (simp[Abbr`p'`, Abbr`pfxn`,
             rich_listTheory.EL_APPEND2, rich_listTheory.EL_APPEND1])>>
  `LENGTH as' + 1 = LENGTH p'` by metis_tac[LENGTH_statelist'] >>
  `pfxn ≤ LENGTH as'` by simp[Abbr`p'`, Abbr`pfxn`] >>
  `exec_plan(s, TAKE pfxn as') = rs` by metis_tac[statelist'_TAKE] >>
  qabbrev_tac `prsd = LENGTH (pfx ++ rs::drop)` >>
  qabbrev_tac `ap1 = TAKE pfxn as'` >>
  `EL prsd p' = rs` by simp[Abbr`p'`, Abbr`prsd`,
                            rich_listTheory.EL_APPEND1,
                            rich_listTheory.EL_APPEND2] >>
  `prsd ≤ LENGTH as'` by simp[Abbr`p'`, Abbr`prsd`] >>
  `exec_plan(s, TAKE prsd as') = rs` by metis_tac[statelist'_TAKE] >>
  qabbrev_tac `ap2 = TAKE prsd as'` >>
  qabbrev_tac `asfx = DROP prsd as'` >>
  `as' = ap2 ++ asfx` by simp[Abbr`ap2`, Abbr`asfx`] >>
  `exec_plan (s, as') = exec_plan(exec_plan(s, ap2), asfx)`
    by metis_tac[exec_plan_Append] >>
  `exec_plan (s, as') = exec_plan(s, ap1 ++ asfx)`
    by metis_tac[exec_plan_Append] >>
  `(LENGTH ap1 = pfxn) ∧ (LENGTH ap2 = prsd)`
    by simp[Abbr`ap1`, Abbr`ap2`] >>
  `LENGTH (ap1 ++ asfx) < LENGTH (ap2 ++ asfx)`
    by simp[Abbr`pfxn`, Abbr`prsd`] >>
  `LENGTH (ap1 ++ asfx) ∈ PLS (s,as)`
    suffices_by metis_tac[DECIDE``x < y ⇔ ¬ (y ≤ x)``] >>
  simp[PLS_def] >> qexists_tac `ap1 ++ asfx` >> simp[] >>
  conj_tac >- metis_tac[] >>
  match_mp_tac sublist_trans >> qexists_tac `as'` >> simp[] >>
  match_mp_tac sublist_append >> simp[sublist_refl] >>
  simp[Abbr`ap1`, Abbr`ap2`] >> match_mp_tac isPREFIX_sublist >>
  match_mp_tac TAKE_isPREFIX >> simp[Abbr`pfxn`, Abbr`prsd`]);

val MPLS_nempty = store_thm("MPLS_nempty",
``!PROB. FINITE PROB ==> MPLS(PROB) <> EMPTY``,
SRW_TAC[][MPLS_def]
THEN SRW_TAC[][EXTENSION]
THEN METIS_TAC[empty_plan_is_valid, valid_states_nempty]);

val bound_main_lemma = store_thm("bound_main_lemma",
``!(PROB:'a problem). FINITE PROB  ==> (problem_plan_bound(PROB) <= 2**(CARD (prob_dom PROB)) - 1)``,
SRW_TAC[][problem_plan_bound_def]
THEN MP_TAC (in_MPLS_leq_2_pow_n
		|> Q.SPEC `PROB`)
THEN SRW_TAC[][]
THEN MP_TAC (bound_main_lemma_2
		|> Q.SPEC `MPLS (PROB:'a problem)`
		|> Q.SPEC `2**(CARD (prob_dom (PROB:'a problem))) - 1`)
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN MP_TAC (MPLS_nempty |> INST_TYPE [beta |-> ``:bool``] |> Q.SPEC `PROB`)
THEN SRW_TAC[][]
THEN METIS_TAC[]);

val bound_child_parent_card_state_set_cons = store_thm("bound_child_parent_card_state_set_cons",
``!P f. (!PROB as s. P PROB /\ as IN (valid_plans PROB)
                     /\ s IN (valid_states PROB)
                     ==> ?as'. (exec_plan(s, as) = 
                                   exec_plan (s, as'))
                                /\ sublist as' as
                                /\ LENGTH as' < f(PROB))
        ==> !PROB s as. P PROB /\ as IN (valid_plans PROB)
                        /\ (s IN (valid_states PROB))
                        ==>  ?x. (x IN PLS(s, as) /\ x < f(PROB)) ``,
SRW_TAC[][]
THEN `(exec_plan(s, as)) IN (valid_states PROB)` by METIS_TAC[valid_as_valid_exec]
THEN FIRST_X_ASSUM (Q.SPECL_THEN [`PROB`, `as`, `s`] MP_TAC)
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[]
THEN SRW_TAC[][]
THEN SRW_TAC[][PLS_def]
THEN Q.EXISTS_TAC `LENGTH as'`
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][]);

val bound_on_all_plans_bounds_MPLS = store_thm("bound_on_all_plans_bounds_MPLS",
``!P f. (!PROB as s. P PROB /\ s IN (valid_states PROB)
                     /\ as IN (valid_plans PROB)
                     ==> ?as'. (exec_plan(s, as) =
                                    exec_plan(s, as'))
                               /\ sublist as' as /\ LENGTH as' < f(PROB))
         ==> !PROB x. P PROB ==> (x IN MPLS(PROB)) ==> x < f(PROB) ``,
SRW_TAC[][MPLS_def]
THEN MP_TAC (bound_child_parent_card_state_set_cons
                |> Q.SPEC `P`
                |> Q.SPEC `f`)
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (Q.SPECL_THEN [`PROB`, `s`, `as`] MP_TAC)
THEN SRW_TAC[][]
THEN MP_TAC(mem_lt_imp_MIN_lt
			|> Q.SPEC `PLS (s:(α |-> β),as:((α |-> β) # (α |-> β)) list)`
			|> Q.SPEC `f(PROB:((α |-> β) # (α |-> β)) set)`)
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]);

val bound_child_parent_card_state_set_cons_finite = store_thm("bound_child_parent_card_state_set_cons_finite",
``!P f. (!PROB as s. P PROB /\ FINITE PROB
                     /\ as IN (valid_plans PROB)
                     /\ s IN (valid_states PROB)
                     ==> ?as'. (exec_plan(s, as) =
                                   exec_plan (s, as'))
                               /\ sublist as' as /\ LENGTH as' < f(PROB))
         ==> !PROB s as. P PROB /\ FINITE PROB 
                         /\ as IN (valid_plans PROB)
                         /\ (s IN (valid_states PROB))
                        ==>  ?x. (x IN PLS(s, as) /\ x < f(PROB)) ``,
SRW_TAC[][]
THEN `(exec_plan(s, as)) IN (valid_states PROB)` by METIS_TAC[valid_as_valid_exec]
THEN FIRST_X_ASSUM (Q.SPECL_THEN [`(PROB)`, `as`, `s`] MP_TAC)
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[]
THEN SRW_TAC[][]
THEN SRW_TAC[][PLS_def]
THEN Q.EXISTS_TAC `LENGTH as'`
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][]);

val bound_on_all_plans_bounds_MPLS_finite = store_thm("bound_on_all_plans_bounds_MPLS_finite",
``!P f. (!PROB as s. P PROB /\ FINITE PROB
                     /\ s IN (valid_states PROB)
                     /\ as IN (valid_plans PROB)
                     ==> ?as'. (exec_plan(s, as) =
                                   exec_plan(s, as'))
                               /\ sublist as' as /\ LENGTH as' < f(PROB))
         ==> !PROB x. P PROB /\ FINITE PROB ==>  (x IN MPLS(PROB)) ==> x < f(PROB) ``,
SRW_TAC[][MPLS_def]
THEN MP_TAC (bound_child_parent_card_state_set_cons_finite
                |> Q.SPEC `P`
                |> Q.SPEC `f`)
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (Q.SPECL_THEN [`PROB`, `s`, `as`] MP_TAC)
THEN SRW_TAC[][]
THEN MP_TAC(mem_lt_imp_MIN_lt
			|> Q.SPEC `PLS (s,as)`
			|> Q.SPEC `f(PROB:((α |-> β) # (α |-> β)) set)`)
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]);

val bound_on_all_plans_bounds_problem_plan_bound = store_thm("bound_on_all_plans_bounds_problem_plan_bound",
``!P f. (!PROB as s. P PROB /\ FINITE PROB 
                     /\ s IN (valid_states PROB)
                     /\ as IN (valid_plans PROB)
                     ==> ?as'. (exec_plan(s,as) = 
                                   exec_plan(s, as'))
                               /\ sublist as' as /\ LENGTH as' < f(PROB))
        ==> !PROB. P PROB /\ FINITE PROB ==> problem_plan_bound(PROB) < f(PROB)``,
SRW_TAC[][problem_plan_bound_def]
THEN MP_TAC (bound_on_all_plans_bounds_MPLS_finite
                |> Q.SPEC `P`
                |> Q.SPEC `f`)
THEN SRW_TAC[][]
THEN MP_TAC (bound_on_all_plans_bounds_problem_plan_bound_2
		|> Q.ISPEC `MPLS PROB`
		|> Q.SPEC `f(PROB:((α |-> β) # (α |-> β))set)`)
THEN SRW_TAC[][]
THEN FIRST_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][MPLS_nempty]);

val bound_child_parent_card_state_set_cons_thesis = store_thm("bound_child_parent_card_state_set_cons_thesis",
``FINITE PROB 
  /\ (!as s. as IN (valid_plans PROB)
             /\ s IN (valid_states PROB)
             ==> ?as'. (exec_plan(s, as) =
                           exec_plan (s, as'))
                       /\ sublist as' as /\ LENGTH as' < k)
  /\ as IN (valid_plans PROB) /\ s IN (valid_states PROB)
  ==>  ?x. (x IN PLS(s, as) /\ x < k) ``,
SRW_TAC[][]
THEN `(exec_plan(s, as)) IN (valid_states PROB)` by METIS_TAC[valid_as_valid_exec]
THEN FIRST_X_ASSUM (Q.SPECL_THEN [`as`, `s`] MP_TAC)
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[]
THEN SRW_TAC[][]
THEN SRW_TAC[][PLS_def]
THEN Q.EXISTS_TAC `LENGTH as'`
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][]);

val bound_on_all_plans_bounds_MPLS_thesis = store_thm("bound_on_all_plans_bounds_MPLS_thesis",
``FINITE PROB 
  /\ (!as s. s IN (valid_states PROB)
             /\ as IN (valid_plans PROB)
             ==> ?as'. (exec_plan(s, as) =
                            exec_plan(s, as'))
                       /\ sublist as' as /\ LENGTH as' < k)
         ==> x IN MPLS(PROB) ==> x < k``,
SRW_TAC[][MPLS_def]
THEN MP_TAC (bound_child_parent_card_state_set_cons_thesis)
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (Q.SPECL_THEN [`as`, `s`] MP_TAC)
THEN SRW_TAC[][]
THEN MP_TAC(mem_lt_imp_MIN_lt
			|> Q.SPEC `PLS (s,as)`
			|> Q.SPEC `k`)
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]);

val bound_on_all_plans_bounds_problem_plan_bound_thesis' = store_thm("bound_on_all_plans_bounds_problem_plan_bound_thesis'",
``FINITE PROB
  /\ (!as s. s IN (valid_states PROB) 
             /\ as IN (valid_plans PROB)
             ==> ?as'. (exec_plan(s,as) =
                            exec_plan(s, as'))
                       /\ sublist as' as /\ LENGTH as' < k)
  ==> problem_plan_bound(PROB) < k``,
SRW_TAC[][problem_plan_bound_def]
THEN MP_TAC (bound_on_all_plans_bounds_MPLS_thesis |> Q.GEN `x`)
THEN SRW_TAC[][]
THEN MP_TAC (bound_on_all_plans_bounds_problem_plan_bound_2
		|> Q.ISPEC `MPLS PROB`
		|> Q.SPEC `k`)
THEN SRW_TAC[][]
THEN FIRST_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][MPLS_nempty]);

val bound_on_all_plans_bounds_problem_plan_bound_thesis = store_thm("bound_on_all_plans_bounds_problem_plan_bound_thesis",
``FINITE PROB
  /\ (!as s. s IN (valid_states PROB)
             /\ as IN (valid_plans PROB)
             ==> ?as'. (exec_plan(s,as) =
                           exec_plan(s, as'))
                       /\ sublist as' as /\ LENGTH as' <= k)
  ==> problem_plan_bound(PROB) <= k``,
SRW_TAC[][problem_plan_bound_def]
THEN MP_TAC (bound_on_all_plans_bounds_MPLS_thesis |> Q.GENL [`k`, `x`] |> Q.SPEC `k+1` )
THEN SRW_TAC[][GSYM LE_LT1]
THEN MP_TAC (bound_on_all_plans_bounds_problem_plan_bound_2
		|> Q.ISPEC `MPLS PROB`
		|> Q.SPEC `k + 1`)
THEN SRW_TAC[][GSYM LE_LT1]
THEN FIRST_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][MPLS_nempty]);

val bound_on_all_plans_bounds_problem_plan_bound_ = store_thm("bound_on_all_plans_bounds_problem_plan_bound_",
``!P f (PROB). 
      (!PROB' as s. P PROB' /\ FINITE PROB'
                    /\ s IN (valid_states PROB')
                    /\ as IN (valid_plans PROB')
                    ==> ?as'. (exec_plan(s,as) =
                                   exec_plan(s, as'))
                              /\ sublist as' as /\ LENGTH as' < f(PROB'))
     	/\ P PROB /\ FINITE PROB ==> problem_plan_bound(PROB) < f(PROB)``,
SRW_TAC[][problem_plan_bound_def]
THEN MP_TAC (bound_on_all_plans_bounds_MPLS_finite
                |> Q.SPEC `P`
                |> Q.SPEC `f`)
THEN SRW_TAC[][]
THEN MP_TAC (bound_on_all_plans_bounds_problem_plan_bound_2
		|> Q.ISPEC `MPLS( PROB: ((α |-> β) # (α |-> β)) set)`
		|> Q.SPEC `f(PROB:((α |-> β) # (α |-> β)) set )`)
THEN SRW_TAC[][]
THEN FIRST_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][MPLS_nempty]);

val S_VALID_AS_VALID_IMP_MIN_IN_PLS = store_thm("S_VALID_AS_VALID_IMP_MIN_IN_PLS",
``!PROB s as. s IN (valid_states PROB) /\ as IN (valid_plans PROB)
  	     ==>  MIN_SET(PLS(s, as)) IN MPLS(PROB)``,
SRW_TAC[][MPLS_def]
THEN Q.EXISTS_TAC `(s,as)`
THEN SRW_TAC[][]);

val problem_plan_bound_ge_min_pls = store_thm("problem_plan_bound_ge_min_pls",
``!(PROB:'a problem) s as k. FINITE PROB /\ (s IN (valid_states PROB))
                             /\ (as IN (valid_plans PROB)) /\ problem_plan_bound(PROB) <= k
                             ==> MIN_SET(PLS(s, as)) <= problem_plan_bound(PROB)``,
SRW_TAC[][]
THEN MP_TAC(in_MPLS_leq_2_pow_n
	   |> Q.SPEC `PROB`)
THEN SRW_TAC[][]
THEN MP_TAC(mems_le_finite 
	   |> Q.SPEC `MPLS(PROB:'a problem)`
	   |> Q.SPEC `2 ** CARD (prob_dom (PROB:'a problem)) - 1`)
THEN SRW_TAC[][]
THEN MP_TAC(MPLS_nempty |> INST_TYPE[beta |-> ``:bool``]
		       |> Q.SPEC `PROB` )
THEN SRW_TAC[][]
THEN MP_TAC (MAX_SET_DEF
	    |> Q.SPEC `(MPLS (PROB:'a problem))`)
THEN SRW_TAC[][]
THEN METIS_TAC[S_VALID_AS_VALID_IMP_MIN_IN_PLS, problem_plan_bound_def]);

val PLS_NEMPTY = store_thm("PLS_NEMPTY",
``!s as. PLS(s, as) <> EMPTY``,
SRW_TAC[][PLS_def, EXTENSION]
THEN Q.EXISTS_TAC `as`
THEN SRW_TAC[][sublist_refl]);

val PLS_nempty_and_has_min = store_thm("PLS_nempty_and_has_min",
``!s as. ?x. x IN (PLS(s, as)) /\ (x = MIN_SET(PLS(s, as)))``,
SRW_TAC[][]
THEN MP_TAC(PLS_NEMPTY
            |> Q.SPEC `s`
            |> Q.SPEC `as`)
THEN SRW_TAC[][]
THEN MP_TAC(MIN_SET_LEM
	    |> Q.SPEC `PLS(s, as)`)
THEN SRW_TAC[][]);

val PLS_works = store_thm("PLS_works",
``!x s as. x IN PLS(s, as)
       	   ==> ?as'. (exec_plan(s, as) = exec_plan(s, as'))
                      /\ (LENGTH(as') = x) /\ sublist as' as``,
SRW_TAC[][PLS_def]
THEN Q.EXISTS_TAC `x'`
THEN SRW_TAC[][]);

val problem_plan_bound_works = store_thm("problem_plan_bound_works",
``!(PROB:'a problem) as s.
         FINITE PROB /\ s IN (valid_states PROB) /\ as IN (valid_plans PROB)
	 ==> ?as'. (exec_plan(s, as) = exec_plan(s, as'))
                   /\ sublist as' as /\ LENGTH as' <= problem_plan_bound(PROB)``,
SRW_TAC[][]
THEN MP_TAC(bound_main_lemma
		|> Q.SPEC `PROB`)
THEN SRW_TAC[][]
THEN MP_TAC(problem_plan_bound_ge_min_pls
			|> Q.SPEC `PROB`
			|> Q.SPEC `s`
			|> Q.SPEC `as`
			|> Q.SPEC `2 ** CARD (prob_dom (PROB: 'a problem)) - 1`)
THEN SRW_TAC[][]
THEN MP_TAC(PLS_nempty_and_has_min |> INST_TYPE[beta |-> ``:bool``]
			|> Q.SPEC `s:'a state`
			|> Q.SPEC `as:'a action list`)
THEN SRW_TAC[][]
THEN MP_TAC(PLS_works |> INST_TYPE[beta |-> ``:bool``]
			|> Q.SPEC `MIN_SET (PLS (s:'a state, as:'a action list))`
			|> Q.SPEC `s:'a state`
			|> Q.SPEC `as:'a action list`)
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][]
THEN METIS_TAC[sublist_subset, SUBSET_TRANS]);

val MPLS_s_def = 
Define `MPLS_s PROB s
          =  (IMAGE (\ (s, as). MIN_SET (PLS(s, as)))
                {(s, as) |  (as IN (valid_plans PROB))})`;

val bound_main_lemma_s_3 = store_thm("bound_main_lemma_s_3",
``!PROB s. MPLS_s PROB s  <> EMPTY``,
SRW_TAC[][MPLS_s_def]
THEN SRW_TAC[][EXTENSION]
THEN METIS_TAC[empty_plan_is_valid, valid_states_nempty]);

val problem_plan_bound_s_def = Define `problem_plan_bound_s PROB s = MAX_SET(MPLS_s PROB s)` ;

val bound_on_all_plans_bounds_PLS_s = store_thm("bound_on_all_plans_bounds_PLS_s",
``!P f. (!(PROB:'a problem) as s. 
                  FINITE PROB /\ P PROB /\ as IN (valid_plans PROB)
                  /\ s IN (valid_states PROB)
                  ==> ?as'. (exec_plan(s, as) =
                                exec_plan (s, as'))
                            /\ sublist as' as /\ LENGTH as' < f PROB s)
        ==> !PROB s as. FINITE PROB /\ P PROB /\ as IN (valid_plans PROB)
                        /\ (s IN (valid_states PROB))
                        ==>  ?x. (x IN PLS(s, as) /\ x < f PROB s) ``,
SRW_TAC[][]
THEN `(exec_plan(s, as)) IN (valid_states PROB)` by METIS_TAC[valid_as_valid_exec]
THEN FIRST_X_ASSUM (Q.SPECL_THEN [`(PROB):('a problem)`, `as`, `s`] MP_TAC)
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[]
THEN SRW_TAC[][]
THEN SRW_TAC[][PLS_def]
THEN Q.EXISTS_TAC `LENGTH as'`
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][]);

val bound_on_all_plans_bounds_MPLS_s = store_thm("bound_on_all_plans_bounds_MPLS_s",
``!P f. (!(PROB:'a problem) as s.
              FINITE PROB /\ P PROB /\ s IN (valid_states PROB)
              /\ as IN (valid_plans PROB)
              ==> ?as'. (exec_plan(s, as) =
                             exec_plan(s, as'))
                        /\ sublist as' as /\ LENGTH as' < f PROB s)
         ==> !PROB x s. FINITE PROB /\ P PROB
                        /\ s IN (valid_states PROB)
                        ==> (x IN MPLS_s PROB s)
                        ==> x < f PROB s``,
SRW_TAC[][MPLS_s_def]
THEN MP_TAC (bound_on_all_plans_bounds_PLS_s
                |> Q.SPEC `P`
                |> Q.SPEC `f`)
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (Q.SPECL_THEN [`PROB:('a problem)`, `s`, `as`] MP_TAC)
THEN SRW_TAC[][]
THEN MP_TAC(mem_lt_imp_MIN_lt
			|> Q.SPEC `PLS (s:'a state,as:'a action list)`
			|> Q.SPEC `f ( PROB:('a problem)) (s: 'a state)`)
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]
THEN METIS_TAC[]);

val bound_child_parent_lemma_s_2 = store_thm("bound_child_parent_lemma_s_2",
``!PROB s P f.
     (!(PROB:'a problem) as s. FINITE PROB /\ P PROB
                               /\ s IN (valid_states PROB)
                               /\ as IN (valid_plans PROB)
                               ==> ?as'. (exec_plan(s,as) =
                                              exec_plan(s, as'))
                                         /\ sublist as' as /\ LENGTH as' < f PROB s)
      ==> FINITE PROB /\ P PROB /\ s IN (valid_states PROB)
          ==> problem_plan_bound_s PROB s < f PROB s``,
SRW_TAC[][problem_plan_bound_s_def]
THEN MP_TAC (bound_on_all_plans_bounds_MPLS_s
                |> Q.SPEC `P`
                |> Q.SPEC `f`)
THEN SRW_TAC[][]
THEN MP_TAC (bound_on_all_plans_bounds_problem_plan_bound_2
		|> Q.ISPEC `MPLS_s  (PROB: ('a problem)) s`
		|> Q.SPEC `f (PROB:'a problem) (s:'a state)`)
THEN SRW_TAC[][]
THEN FIRST_ASSUM MATCH_MP_TAC
THEN METIS_TAC[bound_main_lemma_s_3]);

val bound_main_lemma_reachability_s = store_thm("bound_main_lemma_reachability_s",
``!(PROB:'a problem) s.
           FINITE PROB /\ s IN (valid_states PROB) 
           ==> problem_plan_bound_s PROB s < CARD (reachable_s PROB s)``,
MATCH_MP_TAC (SIMP_RULE(srw_ss())[] (bound_child_parent_lemma_s_2 |> Q.SPECL[`PROB`, `s`, `(\x. T)`, `(\PROB s. CARD (reachable_s PROB s))`]))
THEN SRW_TAC[][GSYM arithmeticTheory.LE_LT1]
THEN MP_TAC(main_lemma_reachability_s |> Q.SPECL[`PROB`, `as`, `s`] )
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN MP_TAC(card_reachable_s_non_zero |> Q.SPEC `s`)
THEN SRW_TAC[][valid_states_def]
THEN DECIDE_TAC);

val problem_plan_bound_s_LESS_EQ_problem_plan_bound_thm = store_thm("problem_plan_bound_s_LESS_EQ_problem_plan_bound_thm",
``!(PROB:'a problem) s. FINITE PROB /\ s IN (valid_states PROB) ==> problem_plan_bound_s PROB s < problem_plan_bound PROB + 1``,
MATCH_MP_TAC (SIMP_RULE(srw_ss())[] (bound_child_parent_lemma_s_2 |> Q.SPECL[`PROB`, `s`, `(\x. T)`, `(\PROB s. problem_plan_bound PROB + 1)`]))
THEN SRW_TAC[][GSYM arithmeticTheory.LE_LT1]
THEN METIS_TAC[problem_plan_bound_works])

val bound_main_lemma_s_1 = store_thm("bound_main_lemma_s_1",
``!(PROB:'a problem) x s.
        FINITE PROB /\ (s IN valid_states PROB)
        /\ (x IN MPLS_s PROB s)
        ==> x <= 2 ** CARD(prob_dom PROB) - 1``,
SRW_TAC[][MPLS_s_def]
THEN MP_TAC (in_PLS_leq_2_pow_n
		|> Q.SPEC `PROB`
		|> Q.SPEC `s`
		|> Q.SPEC `as`)
THEN SRW_TAC[][]
THEN MP_TAC(mem_le_imp_MIN_le
			|> Q.SPEC `PLS (s:'a state,as:'a action list)`
			|> Q.SPEC `2 ** CARD (prob_dom (PROB: 'a problem)) - 1`)
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]);

val AS_VALID_MPLS_VALID = store_thm("AS_VALID_MPLS_VALID",
``!PROB as. as IN (valid_plans PROB)
            ==>  MIN_SET(PLS(s, as)) IN MPLS_s PROB s``,
SRW_TAC[][MPLS_s_def]
THEN Q.EXISTS_TAC `(s,as)`
THEN SRW_TAC[][]);

val problem_plan_bound_s_ge_min_pls = store_thm("problem_plan_bound_s_ge_min_pls",
``!(PROB:'a problem) as k s. FINITE PROB /\ s IN (valid_states PROB) /\ as IN (valid_plans PROB) /\ problem_plan_bound_s PROB s <= k
	     	==> MIN_SET(PLS(s, as)) <= problem_plan_bound_s PROB s``,
SRW_TAC[][]
THEN MP_TAC(bound_main_lemma_s_1
	   |> Q.SPEC `PROB`)
THEN SRW_TAC[][]
THEN MP_TAC(mems_le_finite
	   |> Q.SPEC `MPLS_s (PROB:'a problem) s`
	   |> Q.SPEC `2 ** CARD (prob_dom (PROB: 'a problem)) - 1`)
THEN SRW_TAC[][]
THEN MP_TAC(bound_main_lemma_s_3 |> INST_TYPE[beta |-> ``:bool``]
		       |> Q.SPEC `PROB` )
THEN SRW_TAC[][]
THEN MP_TAC (MAX_SET_DEF
	    |> Q.SPEC `(MPLS_s (PROB:'a problem) s)`)
THEN SRW_TAC[][]
THEN METIS_TAC[AS_VALID_MPLS_VALID, problem_plan_bound_s_def]);

val bound_main_lemma_s_1 = store_thm("bound_main_lemma_s_1",
``!(PROB:'a problem) x s. FINITE PROB /\ s IN (valid_states PROB) ==>  (x IN MPLS_s PROB s) ==> x <= 2 ** CARD(prob_dom PROB) - 1``,
SRW_TAC[][MPLS_s_def]
THEN MP_TAC(in_PLS_leq_2_pow_n
		|> Q.SPEC `PROB`
		|> Q.SPEC `s`
		|> Q.SPEC `as`)
THEN SRW_TAC[][]
THEN MP_TAC(mem_le_imp_MIN_le
			|> Q.SPEC `PLS (s:'a state,as:'a action list)`
			|> Q.SPEC `2 ** CARD (prob_dom (PROB:'a problem)) - 1`)
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]);

val bound_main_lemma_s = store_thm("bound_main_lemma_s",
``!(PROB:'a problem) s. FINITE PROB /\ s IN (valid_states PROB) ==> (problem_plan_bound_s PROB s <= 2**(CARD (prob_dom PROB)) - 1)``,
SRW_TAC[][problem_plan_bound_s_def]
THEN MP_TAC (bound_main_lemma_s_1
		|> Q.SPEC `PROB`)
THEN SRW_TAC[][]
THEN MP_TAC (bound_main_lemma_2
		|> Q.SPEC `MPLS_s (PROB:'a problem) s`
		|> Q.SPEC `2**(CARD (prob_dom (PROB:'a problem))) - 1`)
THEN SRW_TAC[][]
THEN FIRST_ASSUM MATCH_MP_TAC
THEN SRW_TAC[SatisfySimps.SATISFY_ss][bound_main_lemma_s_3]);

val problem_plan_bound_s_works = store_thm("problem_plan_bound_s_works",
``!(PROB:'a problem) as s. FINITE PROB /\as IN (valid_plans PROB) /\ (s IN valid_states PROB)
	 ==> ?as'. (exec_plan(s, as) = exec_plan(s, as'))
                   /\ sublist as' as /\ LENGTH as' <= problem_plan_bound_s PROB s``,
SRW_TAC[][]
THEN MP_TAC(bound_main_lemma_s
		|> Q.SPEC `PROB`)
THEN SRW_TAC[][]
THEN MP_TAC(problem_plan_bound_s_ge_min_pls
			|> Q.SPEC `PROB`
			|> Q.SPEC `as`
			|> Q.SPEC `2 ** CARD (prob_dom (PROB: 'a problem)) - 1`)
THEN SRW_TAC[][]
THEN MP_TAC(PLS_nempty_and_has_min |> INST_TYPE[beta |-> ``:bool``]
			|> Q.SPEC `s`
			|> Q.SPEC `as`)
THEN SRW_TAC[][]
THEN MP_TAC(PLS_works |> INST_TYPE[beta |-> ``:bool``]
			|> Q.SPEC `MIN_SET (PLS (s:'a state,as:'a action list))`
			|> Q.SPEC `s:'a state`
			|> Q.SPEC `as:'a action list`)
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][]
THEN METIS_TAC[sublist_subset, SUBSET_TRANS]);

val PLS_def_ITP2015 = store_thm("PLS_def_ITP2015",
``!s as. PLS (s,as) =     
       {LENGTH as' | (exec_plan (s,as') = exec_plan (s,as)) ∧ sublist as' as}``,
SRW_TAC[][PLS_def, IMAGE_DEF]);

val PLS_def_ITP2015 = store_thm("PLS_def_ITP2015",
``!s as. PLS (s,as) =     
       {LENGTH as' | (exec_plan (s,as') = exec_plan (s,as)) ∧ sublist as' as}``,
SRW_TAC[][PLS_def, IMAGE_DEF]);

val expanded_problem_plan_bound_charles_thm = store_thm("expanded_problem_plan_bound_charles_thm",
``!PROB: 'a problem. problem_plan_bound_charles PROB = MAX_SET{MIN_SET (PLS_charles(s,as,PROB))|s,as | s:'a state ∈ valid_states (PROB:'a problem) ∧ as ∈ valid_plans PROB}``,
SRW_TAC[][ problem_plan_bound_charles_def, MPLS_charles_def,EXTENSION]
THEN METIS_TAC[]);

val bound_main_lemma_charles_3 = store_thm("bound_main_lemma_charles_3",
``!(PROB:'a problem). FINITE PROB ==> (MPLS_charles(PROB) <> EMPTY)``,
SRW_TAC[][MPLS_charles_def]
THEN  SRW_TAC[][EXTENSION]
THEN METIS_TAC[empty_plan_is_valid, valid_states_nempty]);

val in_PLS_charles_leq_2_pow_n = store_thm("in_PLS_charles_leq_2_pow_n",
``!(PROB:'a problem) s as. 
      FINITE PROB /\ s IN (valid_states PROB) /\ as IN (valid_plans PROB)
      ==> ?x. x IN PLS_charles(s, as, PROB) /\ x <= 2 ** CARD(prob_dom PROB) - 1``,
SRW_TAC[][PLS_charles_def]
THEN MP_TAC (main_lemma
		  |> Q.SPEC `PROB `
		  |> Q.SPEC `as`
                  |> Q.SPEC `s`)
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `LENGTH as'`
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `as'`
THEN FULL_SIMP_TAC(srw_ss())[valid_plans_def]
THEN METIS_TAC[SUBSET_TRANS, sublist_subset]);

val in_MPLS_charles_leq_2_pow_n = store_thm("in_MPLS_charles_leq_2_pow_n",
``!(PROB:'a problem) x. FINITE PROB ==>  (x IN MPLS_charles(PROB)) ==> x <= 2 ** CARD(prob_dom PROB) - 1``,
SRW_TAC[][MPLS_charles_def]
THEN MP_TAC (in_PLS_charles_leq_2_pow_n
		|> Q.SPEC `PROB`
		|> Q.SPEC `s`
		|> Q.SPEC `as`)
THEN SRW_TAC[][]
THEN MP_TAC(mem_le_imp_MIN_le
			|> Q.SPEC `PLS_charles (s:'a state,as:'a action list, PROB:'a problem)`
			|> Q.SPEC `2 ** CARD (prob_dom (PROB:'a problem)) - 1`)
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]);

val bound_main_lemma_charles = store_thm("bound_main_lemma_charles",
``!(PROB:'a problem). FINITE PROB ==> (problem_plan_bound_charles(PROB) <= 2**(CARD (prob_dom PROB)) - 1)``,
SRW_TAC[][problem_plan_bound_charles_def]
THEN MP_TAC (in_MPLS_charles_leq_2_pow_n
		|> Q.SPEC `PROB`)
THEN SRW_TAC[][]
THEN MP_TAC (bound_main_lemma_2
		|> Q.SPEC `MPLS_charles (PROB:'a problem)`
		|> Q.SPEC `2**(CARD (prob_dom (PROB:'a problem))) - 1`)
THEN SRW_TAC[][]
THEN FIRST_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][bound_main_lemma_charles_3]);

val bound_on_all_plans_bounds_PLS_charles = store_thm("bound_on_all_plans_bounds_PLS_charles",
``!P f. 
   (!(PROB:'a problem) as s.
          P PROB /\ FINITE PROB /\ as IN (valid_plans PROB)
          /\ s IN (valid_states PROB)
          ==> ?as'. (exec_plan(s, as) =
                        exec_plan (s, as'))
                    /\ sublist as' as /\ LENGTH as' < f(PROB))
    ==> !PROB s as. P PROB /\ FINITE PROB 
                    /\ as IN (valid_plans PROB)
                    /\ (s IN (valid_states PROB))
                    ==>  ?x. (x IN PLS_charles(s, as, PROB) /\ x < f(PROB)) ``,
SRW_TAC[][]
THEN `(exec_plan(s, as)) IN (valid_states PROB)` by METIS_TAC[valid_as_valid_exec]
THEN FIRST_X_ASSUM (Q.SPECL_THEN [`(PROB):('a problem)`, `as`, `s`] MP_TAC)
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[]
THEN SRW_TAC[][]
THEN SRW_TAC[][PLS_charles_def]
THEN Q.EXISTS_TAC `LENGTH as'`
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[valid_plans_def]
THEN METIS_TAC[SUBSET_TRANS, sublist_subset]);

val bound_on_all_plans_bounds_MPLS_charles = store_thm("bound_on_all_plans_bounds_MPLS_charles",
``!P f.
   (!(PROB:'a problem) as s.
         P PROB /\ FINITE PROB /\ s IN (valid_states PROB)
         /\ as IN (valid_plans PROB)
         ==> ?as'. (exec_plan(s, as) =
                        exec_plan(s, as'))
                   /\ sublist as' as /\ LENGTH as' < f(PROB))
         ==> !PROB x. P PROB /\ FINITE PROB ==> (x IN MPLS_charles(PROB)) ==> x < f(PROB) ``,
SRW_TAC[][MPLS_charles_def]
THEN MP_TAC (bound_on_all_plans_bounds_PLS_charles
                |> Q.SPEC `P`
                |> Q.SPEC `f`)
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (Q.SPECL_THEN [`PROB:('a problem)`, `s`, `as`] MP_TAC)
THEN SRW_TAC[][]
THEN MP_TAC(mem_lt_imp_MIN_lt
			|> Q.SPEC `PLS_charles (s:'a state,as:'a action list, PROB:'a problem)`
			|> Q.SPEC `f( PROB:('a problem))`)
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]);

val bound_on_all_plans_bounds_problem_plan_bound_charles = store_thm("bound_on_all_plans_bounds_problem_plan_bound_charles",
``!P f.
   (!(PROB:'a problem) as s.
        P PROB /\ FINITE PROB /\ s IN (valid_states PROB)
        /\ as IN (valid_plans PROB)
        ==> ?as'. (exec_plan(s,as) =
                       exec_plan(s, as'))
                  /\ sublist as' as /\ LENGTH as' < f(PROB))
     	==> !PROB. P PROB /\ FINITE PROB ==> problem_plan_bound_charles(PROB) < f(PROB)``,
SRW_TAC[][problem_plan_bound_charles_def]
THEN MP_TAC (bound_on_all_plans_bounds_MPLS_charles
                |> Q.SPEC `P`
                |> Q.SPEC `f`)
THEN SRW_TAC[][]
THEN MP_TAC (bound_on_all_plans_bounds_problem_plan_bound_2
		|> Q.ISPEC `MPLS_charles ( PROB: ('a problem))`
		|> Q.SPEC `f(PROB:'a problem)`)
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][bound_main_lemma_charles_3]);

val sublistD_bounds_D = store_thm("sublistD_bounds_D",
``!(PROB:'a problem). FINITE PROB ==> problem_plan_bound_charles PROB <= problem_plan_bound PROB``,
STRIP_TAC
THEN REWRITE_TAC[LE_LT1]
THEN MATCH_MP_TAC(SIMP_RULE(srw_ss())[] (bound_on_all_plans_bounds_problem_plan_bound_charles |> Q.SPECL[`(\x. T)`, `(\PROB. problem_plan_bound PROB + 1)`]))
THEN SRW_TAC[][]
THEN SRW_TAC[][GSYM LE_LT1]
THEN MATCH_MP_TAC(problem_plan_bound_works)
THEN SRW_TAC[][]);

val sublistD_bounds_D_and_RD_bounds_sublistD = store_thm("sublistD_bounds_D_and_RD_bounds_sublistD",
``!PROB. FINITE PROB ==> problem_plan_bound_charles PROB <= problem_plan_bound PROB /\ problem_plan_bound PROB <= RD PROB``,
METIS_TAC[sublistD_bounds_D, RD_bounds_sublistD]);

val empty_problem_bound = store_thm("empty_problem_bound",
``!PROB. ((prob_dom PROB) = EMPTY) ==> (problem_plan_bound PROB = 0)``,
SRW_TAC[][]
THEN REWRITE_TAC[GSYM LESS_EQ_0, LE_LT1]
THEN SRW_TAC[][]
THEN (HO_MATCH_MP_TAC bound_on_all_plans_bounds_problem_plan_bound_)
THEN Q.EXISTS_TAC `(\PROB'. prob_dom PROB' = ∅)`
THEN SRW_TAC[][GSYM LE_LT1, Once ONE]
THEN MP_TAC(empty_prob_dom_imp_empty_plan_always_good |> Q.SPECL[`PROB'`, `s`])
THEN SRW_TAC[][]
THEN1( `∃as'.
       (exec_plan (s,as) = exec_plan (s,as')) ∧ sublist as' as ∧
        LENGTH as' <= 0`
       by (Q.EXISTS_TAC `[]` THEN SRW_TAC[][])
     THEN Q.EXISTS_TAC `[]`
     THEN SRW_TAC[][])
THEN METIS_TAC[empty_prob_dom_finite]);

(***********)

val temp = store_thm("temp",
``(!x.?x'. (rd x = ell x') /\ (rd x = rd x'))
  ==> (!x. P x (ell x))
  ==> (?x'. (rd x = rd x') /\ P x' (rd x'))``,
SRW_TAC[][]
THEN METIS_TAC[])

val problem_plan_bound_works' = store_thm("problem_plan_bound_works'",
``!(PROB:'a problem) as s. FINITE PROB /\ s IN (valid_states PROB) /\ as IN (valid_plans PROB)
	 ==> ?as'. (exec_plan(s, as') = exec_plan(s, as)) /\ sublist as' as /\ LENGTH as' <= problem_plan_bound(PROB) /\ sat_precond_as(s, as')``,
SRW_TAC[][]
THEN MP_TAC(problem_plan_bound_works |> Q.SPECL[`PROB`, `as`, `s`])
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `rem_condless_act(s, [], as')`
THEN SRW_TAC[][]
THEN PROVE_TAC[rem_condless_valid_10,rem_condless_valid_8,
               sublist_trans, LESS_EQ_TRANS,
               rem_condless_valid_3, rem_condless_valid_2,
               rem_condless_valid_1])

val problem_plan_bound_UBound = store_thm("problem_plan_bound_UBound",
``(!as s.
        s IN valid_states PROB ∧
        as IN valid_plans PROB ⇒
        ?as'.
          (exec_plan (s,as) = exec_plan (s,as')) /\ sublist as' as /\
          LENGTH as' < f PROB) ∧ FINITE PROB ==>
     problem_plan_bound PROB < f PROB``,
SRW_TAC[][]
THEN HO_MATCH_MP_TAC bound_on_all_plans_bounds_problem_plan_bound_
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `(\Pr. PROB = Pr)`
THEN SRW_TAC[][]
THEN METIS_TAC[])

(*Traversal Diameter*)

val traversed_states_def = Define`traversed_states(s, as) = set (state_list(s,as))`

val finite_traversed_states = store_thm("finite_traversed_states",
``FINITE (traversed_states(s, as))``,
SRW_TAC[][traversed_states_def])

val traversed_states_nempty = store_thm("traversed_states_nempty",
``(traversed_states (s,as)) <> EMPTY``,
Induct_on `as`
THEN SRW_TAC[][traversed_states_def, state_list_def])

val traversed_states_geq_1 = store_thm("traversed_states_geq_1",
``!s as. 1 <= CARD (traversed_states(s,as))``,
SRW_TAC[][]
THEN `CARD (traversed_states(s,as)) <> 0` by METIS_TAC[traversed_states_nempty, finite_traversed_states,CARD_EQ_0]
THEN DECIDE_TAC)

val init_is_traversed = store_thm("init_is_traversed",
``s IN (traversed_states(s,as))``,
Induct_on `as`
THEN SRW_TAC[][traversed_states_def, state_list_def])

val td_def = Define`td(PROB) = MAX_SET {(CARD (traversed_states(s,as))) - 1 | (s IN valid_states PROB) /\ (as IN valid_plans PROB)}`

val traversed_states_rem_condless_act = store_thm("traversed_states_rem_condless_act",
``!s. traversed_states(s, rem_condless_act(s,[], as)) = (traversed_states(s,as))``,
Induct_on `as`
THEN SRW_TAC[][traversed_states_def, rem_condless_act_def, state_list_def, rem_condless_act_cons]
THEN SRW_TAC[][GSYM traversed_states_def]
THEN FULL_SIMP_TAC(srw_ss())[exec_plan_def, state_succ_def]
THEN METIS_TAC[init_is_traversed, ABSORPTION])

val td_UBound = store_thm("td_UBound",
``FINITE PROB /\ (!s as. sat_precond_as(s,as) /\ s IN valid_states PROB /\ as IN valid_plans PROB ==> CARD (traversed_states(s,as)) <= k)
  ==> td PROB <= k - 1``,
SRW_TAC[][td_def]
THEN MATCH_MP_TAC(bound_main_lemma_2)
THEN SRW_TAC[][]
THEN1(SRW_TAC[][EXTENSION]
      THEN METIS_TAC[valid_states_nempty, empty_plan_is_valid])
THEN SRW_TAC[][LE_SUB_RCANCEL]
THEN METIS_TAC[rem_condless_valid_2, traversed_states_rem_condless_act,rem_condless_valid_10])

val _ = export_theory();
