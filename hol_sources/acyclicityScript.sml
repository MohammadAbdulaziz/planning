open HolKernel Parse boolLib bossLib;
open listTheory
open relationTheory
open arithmeticTheory
open pred_setTheory

val _ = new_theory "acyclicity";

val top_sorted_abs_def = Define `(top_sorted_abs R (vtx1::G) 
<=>  EVERY (\vtx2. ~R vtx2 vtx1) G /\ top_sorted_abs R G)
/\ (top_sorted_abs R [] = T)`;

val top_sorted_abs_mem = store_thm("top_sorted_abs_mem",
``top_sorted_abs R (vtx1::G) /\ MEM vtx2 G ==> ~R vtx2 vtx1``,
SRW_TAC[][top_sorted_abs_def]
THEN METIS_TAC[EVERY_MEM |> Q.SPEC `(\a. ~(R: 'a -> 'a -> bool) a vtx1)`])

val top_sorted_cons = store_thm("top_sorted_cons",
``top_sorted_abs R (vtx::G) ==> top_sorted_abs R (G)``,
SRW_TAC[][top_sorted_abs_def])

(*Abstract function for computing weighted longest pathsin DAGs*)
val wlp_def = 
Define 
`(wlp R w g f vtx1 (vtx2::G) = if R vtx1 vtx2 then
                           g (f (w vtx1) (wlp R w g f vtx2 G))
                             (wlp R w g f vtx1 G)
                         else 
                           (wlp R w g f vtx1 G)) /\
 (wlp R w g f vtx1 [] = w vtx1)`

val geq_arg_def = Define`geq_arg f = (!x y. x <= (f x y) /\ y <= (f x y) )`

val individual_weight_less_eq_lp = store_thm("individual_weight_less_eq_lp",
``geq_arg g
  ==> (w vtx <= (wlp R w g f vtx G))``,
SRW_TAC[][geq_arg_def]
THEN Induct_on `G`
THEN SRW_TAC[][wlp_def]
THEN METIS_TAC[LESS_EQ_TRANS])

val lp_geq_lp_from_successor = store_thm("lp_geq_lp_from_successor",
``!vtx1.
   geq_arg f /\ geq_arg g /\
  (!vtx. MEM vtx G ==> ~R vtx vtx) /\ R vtx2 vtx1 /\ MEM vtx1 G /\ top_sorted_abs R G
  ==> (f (w vtx2) (wlp R w g f vtx1 G) <= (wlp R w g f vtx2 G))``,
REWRITE_TAC[geq_arg_def]
THEN Induct_on `G`
THEN SRW_TAC[][wlp_def]
THEN1 METIS_TAC[]
THEN1 METIS_TAC[top_sorted_abs_mem]
THEN1 METIS_TAC[]
THEN1 METIS_TAC[top_sorted_abs_mem]
THEN1 METIS_TAC[LESS_EQ_TRANS]
THEN1(`f (w vtx2) (wlp R w g f vtx1 G) ≤ (wlp R w g f vtx2 G)` by
      (FIRST_X_ASSUM MATCH_MP_TAC
       THEN SRW_TAC[][] THEN METIS_TAC[top_sorted_cons])
      THEN METIS_TAC[LESS_EQ_TRANS])
THEN1 METIS_TAC[LESS_EQ_TRANS, top_sorted_cons]
THEN1(FIRST_X_ASSUM MATCH_MP_TAC
      THEN SRW_TAC[][] THEN METIS_TAC[top_sorted_cons]))

val increasing_def = Define`increasing f = (!e b c d. e <= c /\ b <= d ==> f e b <= f c d)`

val weight_fun_leq_imp_lp_leq = store_thm("weight_fun_leq_imp_lp_leq",
``!vtx. 
   increasing f /\ increasing g /\  (!vtx. MEM vtx G ==> w1 vtx <= w2 vtx) /\ w1 vtx <= w2 vtx
  ==> ((wlp R w1 g f vtx G) <= (wlp R w2 g f vtx G))``,
REWRITE_TAC[increasing_def]
THEN Induct_on `G`
THEN SRW_TAC[][wlp_def])

val wlp_congruence_rule = store_thm("wlp_congruence_rule",
``! l1 l2 R1 R2 w1 w2 g1 g2 f1 f2 x1 x2.
  (l1 = l2)
  /\ (!y. MEM y l2 ==> (R1 x1 y = R2 x2 y))
  /\ (!y. MEM y l2 ==> (R1 y x1 = R2 y x2))
  /\ (w1 x1 = w2 x2 )
  /\ (!y1 y2. (y1 = y2) ==> (f1 (w1 x1) y1 = f2 (w2 x2) y2))
  /\ (!y1 y2 z1 z2. (y1 = y2) /\ (z1 = z2) ==> ((g1 (f1 (w1 x1) y1) z1) = (g2 (f2 (w2 x2) y2) z2)))
  /\ (!x y. MEM x l2 /\ MEM y l2 ==> (R1 x y = R2 x y))
  /\ (!x. MEM x l2 ==> (w1 x = w2 x))
  /\ (!x y z. MEM x l2 ==> (g1 (f1 (w1 x) y) z = g2 (f2 (w2 x) y) z))
  /\ (!x y. MEM x l2 ==> (f1 (w1 x) y = f2 (w1 x) y))
  ==> ((wlp R1 w1 g1 f1 x1 l1) = (wlp R2 w2 g2 f2 x2 l2))``,
Induct_on `l2`
THEN SRW_TAC[][wlp_def]
THEN `(wlp R1 w1 g1 f1 x1 l2) = (wlp R2 w2 g2 f2 x2 l2)` by
     (FIRST_X_ASSUM MATCH_MP_TAC
      THEN SRW_TAC[][])
THEN SRW_TAC[][]
THEN `(wlp R1 w1 g1 f1 h l2) = (wlp R2 w2 g2 f2 h l2)` by
     (FIRST_X_ASSUM MATCH_MP_TAC
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN SRW_TAC[][])

val wlp_ite_weights = store_thm("wlp_ite_weights",
``
!x.
 (!y. MEM y l1 ==> P y) /\ P x
  ==> 
   ((wlp R (\y. if P y then w1 y else w2 y) g f x l1)
      = (wlp R w1 g f x l1))``,
Induct_on `l1`
THEN SRW_TAC[][wlp_def])

val map_wlp_ite_weights = store_thm("map_wlp_ite_weights",
``(!x. MEM x l1 ==> P x) /\ 
    (!x. MEM x l2 ==> P x) ==> 
     (MAP (\x. wlp R (\y. if P y then w1 y else w2 y) g f x l1) l2 = 
          MAP (\x. wlp R w1 g f x l1) l2)``,
Induct_on `l2`
THEN SRW_TAC[][wlp_def]
THEN METIS_TAC[wlp_ite_weights])

val wlp_weight_lamda_exp = store_thm("wlp_weight_lamda_exp",
``!x. wlp R w g f x l = wlp R (\y. w y) g f x l``,
Induct_on `l`
THEN SRW_TAC[][wlp_def])

val img_wlp_ite_weights = store_thm("img_wlp_ite_weights",
``  (!x. MEM x l ==> P x) /\ 
    (!x. x IN s ==> P x) ==> 
     (IMAGE (\x. wlp R (\y. if P y then w1 y else w2 y) g f x l) s = 
          IMAGE (\x. wlp R w1 g f x l) s)``,
SRW_TAC[][IMAGE_DEF, EXTENSION]
THEN METIS_TAC[wlp_ite_weights])

val _ = export_theory();
