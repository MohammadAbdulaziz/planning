open HolKernel Parse boolLib bossLib;
open actionSeqProcessTheory
open acyclicityTheory
open arithmeticTheory
open finite_mapTheory
open factoredSystemTheory
open fmap_utilsTheory
open systemAbstractionTheory
open listTheory
open list_utilsTheory
open topologicalPropsTheory
open pred_setTheory
open sublistTheory
open functionsLib
open set_utilsTheory

val _ = new_theory "acycSspace";

val S_def = Define`S vs lss PROB s = wlp (state_successors (prob_proj(PROB,vs)))
                                 (\s. problem_plan_bound(snapshot PROB s ))
                                 MAX (\x y. x + y + 1) s lss`

val vars_change_def = 
Define`
     (vars_change (a::as) vs s = 
       if (DRESTRICT (state_succ s a) vs <> DRESTRICT s vs) then
         (state_succ s a)::(vars_change as vs (state_succ s a))
       else
         vars_change as vs (state_succ s a)) /\
     (vars_change [] vs s = [])`

val vars_change_cat = store_thm("vars_change_cat",
``!s. vars_change (as1++as2) vs s = ((vars_change as1 vs s) ++ (vars_change as2 vs (exec_plan(s,as1))))``,
Induct_on `as1`
THEN SRW_TAC[][vars_change_def, exec_plan_def])

val empty_change_no_change = store_thm("empty_change_no_change",
``!s. (vars_change as vs s = []) ==> 
  (DRESTRICT (exec_plan(s,as)) vs = DRESTRICT s vs)``,
Induct_on `as`
THEN SRW_TAC[][vars_change_def, exec_plan_def])

val zero_change_imp_all_effects_submap = store_thm("zero_change_imp_all_effects_submap",
``!s s'.
 (vars_change as vs s = []) /\ sat_precond_as (s, as) /\
  MEM a as /\ (DRESTRICT s vs = DRESTRICT s' vs)
  ==> (DRESTRICT (SND a) vs SUBMAP DRESTRICT s' vs)``,
Induct_on `as`
THEN SRW_TAC[][vars_change_def]
THEN1(FULL_SIMP_TAC(srw_ss())[sat_precond_as_def] THEN METIS_TAC[no_change_vs_eff_submap])
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[sat_precond_as_def]
THEN METIS_TAC[])

val zero_change_imp_all_preconds_submap = store_thm("zero_change_imp_all_preconds_submap",
``!s s'.
 (vars_change as vs s = []) /\ sat_precond_as (s, as) /\
  MEM a as /\ (DRESTRICT s vs = DRESTRICT s' vs)
  ==> (DRESTRICT (FST a) vs SUBMAP DRESTRICT s' vs)``,
Induct_on `as`
THEN SRW_TAC[][vars_change_def]
THEN1(FULL_SIMP_TAC(srw_ss())[sat_precond_as_def] THEN METIS_TAC[submap_drest_submap])
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[sat_precond_as_def]
THEN METIS_TAC[])

val no_vs_change_valid_in_snapshot = store_thm("no_vs_change_valid_in_snapshot",
``as IN valid_plans PROB /\ sat_precond_as(s,as) /\
 (vars_change as vs s = []) ==>
  as IN valid_plans(snapshot PROB (DRESTRICT s vs))``,
SRW_TAC[][]
THEN MATCH_MP_TAC as_mem_agree_valid_in_snapshot
THEN SRW_TAC[][]
THEN METIS_TAC[agree_imp_submap, agree_comm, restricted_agree_imp_agree,
               INTER_SUBSET, FDOM_DRESTRICT, zero_change_imp_all_preconds_submap,
               zero_change_imp_all_effects_submap])

(*Obtaining an action sequence that conforms to the snapshot bound, if no vs change happens*)
val no_vs_change_obtain_snapshot_bound_1st_step = store_thm("no_vs_change_obtain_snapshot_bound_1st_step",
``FINITE PROB /\ (vars_change as vs s = []) /\ sat_precond_as (s, as) /\
  s IN valid_states PROB /\ as IN valid_plans PROB
  ==> ?as'. (exec_plan(DRESTRICT s (prob_dom (snapshot PROB (DRESTRICT s vs))), as)
                 = exec_plan(DRESTRICT s (prob_dom (snapshot PROB (DRESTRICT s vs))), as'))
            /\ sublist as' as
            /\ LENGTH as' <= problem_plan_bound(snapshot (PROB:'a problem) (DRESTRICT s vs))``,
SRW_TAC[][]
THEN HO_MATCH_MP_TAC problem_plan_bound_works
THEN SRW_TAC[][]
THEN1 METIS_TAC[FINITE_snapshot]
THEN1 METIS_TAC[two_pow_n_is_a_bound_2, valid_states_snapshot]
THEN MATCH_MP_TAC as_mem_agree_valid_in_snapshot
THEN SRW_TAC[][]
THEN METIS_TAC[agree_imp_submap, agree_comm, restricted_agree_imp_agree,
               INTER_SUBSET, FDOM_DRESTRICT, zero_change_imp_all_preconds_submap,
               zero_change_imp_all_effects_submap])

(*Removing condless acts from last step*)
val no_vs_change_obtain_snapshot_bound_2nd_step = store_thm("no_vs_change_obtain_snapshot_bound_2nd_step",
``FINITE PROB /\ (vars_change as vs s = []) /\ sat_precond_as (s, as) /\
  s IN valid_states PROB /\ as IN valid_plans PROB
  ==> ?as'. (exec_plan(DRESTRICT s (prob_dom (snapshot PROB (DRESTRICT s vs))), as) =
                 exec_plan(DRESTRICT s (prob_dom (snapshot PROB (DRESTRICT s vs))), as')) /\
            sublist as' as /\ sat_precond_as(s,as') /\
            LENGTH as' <= problem_plan_bound(snapshot (PROB:'a problem) (DRESTRICT s vs))``,
SRW_TAC[][]
THEN MP_TAC no_vs_change_obtain_snapshot_bound_1st_step
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `rem_condless_act(DRESTRICT s (prob_dom (snapshot PROB (DRESTRICT s vs))),[],as')`
THEN SRW_TAC[][]
THEN1 METIS_TAC[rem_condless_valid_1]
THEN1 METIS_TAC[rem_condless_valid_8, sublist_trans]
THEN1 METIS_TAC[sat_precond_drest_sat_precond, rem_condless_valid_2]
THEN1 METIS_TAC[rem_condless_valid_3, LESS_EQ_TRANS])

(*Showing that projection (i.e. Drestrict) can happen after executing the concrete as*)
val no_vs_change_obtain_snapshot_bound_3rd_step = store_thm("no_vs_change_obtain_snapshot_bound_3rd_step",
``FINITE PROB /\ (vars_change as vs s = []) /\ no_effectless_act as /\ sat_precond_as (s, as) /\
  s IN valid_states PROB /\ as IN valid_plans PROB ==> 
  ?as'. (DRESTRICT (exec_plan(s, as)) (prob_dom (snapshot PROB (DRESTRICT s vs))) =
                     DRESTRICT (exec_plan(s, as')) (prob_dom (snapshot PROB (DRESTRICT s vs)))) /\
        sublist as' as /\
        LENGTH as' <= problem_plan_bound(snapshot (PROB:'a problem) (DRESTRICT s vs))``,
SRW_TAC[][]
THEN MP_TAC no_vs_change_obtain_snapshot_bound_2nd_step
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][GSYM sat_precond_exec_as_proj_eq_proj_exec]
THEN `as_proj (as,prob_dom (snapshot PROB (DRESTRICT s vs))) = 
          as` by
     (MATCH_MP_TAC (as_proj_eq_as |> Q.GEN `PROB`)
     THEN Q.EXISTS_TAC `(snapshot PROB (DRESTRICT s vs))`
     THEN SRW_TAC[][] THEN METIS_TAC[no_vs_change_valid_in_snapshot])
THEN SRW_TAC[][]
THEN `as_proj (as',prob_dom (snapshot PROB (DRESTRICT s vs))) = as'` by
     (MATCH_MP_TAC (as_proj_eq_as |> Q.GEN `PROB`)
      THEN Q.EXISTS_TAC `(snapshot PROB (DRESTRICT s vs))`
     THEN SRW_TAC[][] THEN METIS_TAC[sublist_valid_is_valid, no_vs_change_valid_in_snapshot, rem_effectless_works_13])
THEN SRW_TAC[][])

val no_vs_change_snapshot_s_vs_is_valid_bound = store_thm("no_vs_change_snapshot_s_vs_is_valid_bound",
``FINITE PROB /\ (vars_change as vs s = []) /\ no_effectless_act as /\ sat_precond_as (s, as) /\
  s IN valid_states PROB /\ as IN valid_plans PROB ==> 
  ?as'. ((exec_plan(s, as)) =
                     (exec_plan(s, as'))) /\
        sublist as' as /\
        LENGTH as' <= problem_plan_bound(snapshot (PROB:'a problem) (DRESTRICT s vs))``,
SRW_TAC[][]
THEN MP_TAC no_vs_change_obtain_snapshot_bound_3rd_step
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][]
THEN MATCH_MP_TAC graph_plan_lemma_5
THEN Q.EXISTS_TAC `(prob_dom (snapshot PROB (DRESTRICT s vs)))`
THEN SRW_TAC[][]
THEN MP_TAC (no_vs_change_valid_in_snapshot |> INST_TYPE [beta |-> ``:bool``])
THEN SRW_TAC[][]
THEN `DRESTRICT (exec_plan (s,as))
        (FDOM (exec_plan (s,as)) DIFF
         prob_dom (snapshot PROB (DRESTRICT s vs))) = 
       DRESTRICT s
        (FDOM (exec_plan (s,as)) DIFF
         prob_dom (snapshot PROB (DRESTRICT s vs)))` by
    (MATCH_MP_TAC disjoint_effects_no_effects
     THEN SRW_TAC[][FDOM_DRESTRICT]
     THEN FULL_SIMP_TAC(srw_ss())[valid_states_def]
     THEN `a IN (snapshot PROB (DRESTRICT s vs))` by
           PROVE_TAC[valid_plan_mems]
     THEN METIS_TAC[subset_inter_diff_empty, FDOM_eff_subset_prob_dom_pair])
THEN SRW_TAC[][]
THEN `as' IN valid_plans (snapshot PROB (DRESTRICT s vs))` by METIS_TAC[sublist_valid_plan]
THEN `DRESTRICT (exec_plan (s,as'))
        (FDOM (exec_plan (s,as')) DIFF
         prob_dom (snapshot PROB (DRESTRICT s vs))) = 
       DRESTRICT s
        (FDOM (exec_plan (s,as')) DIFF
         prob_dom (snapshot PROB (DRESTRICT s vs)))` by
    (MATCH_MP_TAC disjoint_effects_no_effects
     THEN SRW_TAC[][FDOM_DRESTRICT]
     THEN FULL_SIMP_TAC(srw_ss())[valid_states_def]
     THEN `a IN (snapshot PROB (DRESTRICT s vs))` by
           PROVE_TAC[valid_plan_mems]
     THEN METIS_TAC[subset_inter_diff_empty, FDOM_eff_subset_prob_dom_pair])
THEN SRW_TAC[][]
THEN `(exec_plan (s,as)) IN valid_states PROB` by METIS_TAC[valid_as_valid_exec]
THEN `(exec_plan (s,as')) IN valid_states PROB` by METIS_TAC[valid_as_valid_exec, sublist_valid_plan]
THEN FULL_SIMP_TAC(srw_ss())[valid_states_def])

val snapshot_bound_leq_S = store_thm("snapshot_bound_leq_S",
``problem_plan_bound(snapshot PROB (DRESTRICT s vs))
        <= S vs lss PROB (DRESTRICT s vs)``,
SRW_TAC[][S_def]
THEN HO_MATCH_MP_TAC (individual_weight_less_eq_lp)
THEN SRW_TAC[][geq_arg_def])

val S_geq_S_succ_plus_ell = store_thm("S_geq_S_succ_plus_ell",
`` s IN valid_states PROB /\ 
   top_sorted_abs (state_successors (prob_proj(PROB,vs))) lss /\ 
   s' IN (state_successors (prob_proj(PROB,vs)) s) /\
   ((set lss) = valid_states (prob_proj(PROB,vs))) ==> 
  (problem_plan_bound(snapshot PROB (DRESTRICT s vs))) + (S vs lss PROB (DRESTRICT s' vs)) + 1 <= S vs lss PROB (DRESTRICT s vs)``,
SRW_TAC[][S_def]
THEN HO_MATCH_MP_TAC (SIMP_RULE(srw_ss())[] (lp_geq_lp_from_successor |> Q.GEN `f` |> Q.SPEC `(\x y. x + y + 1)`))
THEN SRW_TAC[][geq_arg_def]
THEN1 DECIDE_TAC
THEN1 DECIDE_TAC
THEN1 SRW_TAC[][state_successors_def]
THEN1 METIS_TAC[state_in_successor_proj_in_state_in_successor, IN_DEF]
THEN1 METIS_TAC[proj_successors_of_valid_are_valid, SUBSET_DEF, two_pow_n_is_a_bound_2])

val vars_change_cons = store_thm("vars_change_cons",
``!s s'. 
((vars_change as vs s) = s'::ss)
  ==> (?as1 act as2. (as = (as1 ++ (act::as2))) /\ (vars_change as1 vs s = []) /\
                     ((state_succ (exec_plan(s,as1)) act) = s') /\
                     (vars_change as2 vs (state_succ (exec_plan(s,as1)) act) = ss))``,
Induct_on `as`
THEN SRW_TAC[][vars_change_def]
THEN1(Q.EXISTS_TAC `[]`
      THEN SRW_TAC[][vars_change_def, exec_plan_def])
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`state_succ s h`, `s'`])
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `h::as1`
THEN SRW_TAC[][vars_change_def, exec_plan_def])

val vars_change_cons_2 = store_thm("vars_change_cons_2",
``!s s'. 
((vars_change as vs s) = s'::ss)
  ==> (DRESTRICT s' vs <> DRESTRICT s vs)``,
Induct_on `as`
THEN SRW_TAC[][vars_change_def]
THEN METIS_TAC[])

(*Obtaining S bound: first step, assuming that the given \as has no condless or effless actions*)
val problem_plan_bound_S_bound_1st_step = store_thm("problem_plan_bound_S_bound_1st_step",
``top_sorted_abs (state_successors (prob_proj(PROB,vs))) lss /\
  FINITE (PROB:'a problem) /\ ((set lss) = valid_states (prob_proj(PROB,vs))) /\
  s IN valid_states PROB /\ as IN valid_plans PROB /\
  no_effectless_act as /\ sat_precond_as(s,as)
  ==> ?as'. (exec_plan(s,as') = exec_plan(s,as)) /\ sublist as' as /\
            LENGTH as' <= S vs lss PROB (DRESTRICT s vs)``,
Induct_on `vars_change as vs s`
THEN SRW_TAC[][]
THEN1(MP_TAC no_vs_change_snapshot_s_vs_is_valid_bound
     THEN SRW_TAC[][]
     THEN METIS_TAC[snapshot_bound_leq_S, LESS_EQ_TRANS])
THEN MP_TAC(vars_change_cons |> INST_TYPE [beta |-> ``:bool``] |> Q.GEN `ss` |> Q.SPECL[`v`, `s`, `h`])
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (assume_thm_concl_after_proving_assums o SIMP_RULE(srw_ss())[] o Q.SPECL[`as2`, `vs`, `(state_succ (exec_plan (s,as1)) act)`])
THEN SRW_TAC[][]
THEN `as1 ∈ valid_plans PROB` by METIS_TAC[valid_append_valid_pref]
THEN `act IN PROB` by FULL_SIMP_TAC(srw_ss())[valid_plans_def]
THEN1 METIS_TAC[valid_action_valid_succ, valid_as_valid_exec]
THEN1 METIS_TAC[valid_append_valid_suff]
THEN1 METIS_TAC[rem_effectless_works_12]
THEN1(`sat_precond_as (exec_plan (s,as1), [act] ++ as2)` by SRW_TAC[][empty_replace_proj_dual7]
                THEN FULL_SIMP_TAC(srw_ss())[sat_precond_as_def])
THEN1(`no_effectless_act as1` by METIS_TAC[rem_effectless_works_12]
      THEN `sat_precond_as (s,as1)` by METIS_TAC[sat_precond_as_pfx]
      THEN MP_TAC (no_vs_change_snapshot_s_vs_is_valid_bound |> Q.GEN `as` |> Q.SPEC `as1`)
      THEN SRW_TAC[][]
      THEN Q.EXISTS_TAC `as''++ [act] ++ as'`
      THEN SRW_TAC[][]
      THEN1(SRW_TAC[][exec_plan_Append, exec_plan_def]
            THEN METIS_TAC[])
      THEN1(MATCH_MP_TAC sublist_append
            THEN SRW_TAC[][]
            THEN MATCH_MP_TAC sublist_append
            THEN SRW_TAC[][sublist_def])
      THEN1(`FST act SUBMAP (exec_plan (s,as1))` by
               (`sat_precond_as (exec_plan (s,as1), [act] ++ as2)` by SRW_TAC[][empty_replace_proj_dual7]
                THEN FULL_SIMP_TAC(srw_ss())[sat_precond_as_def])
             THEN `(DRESTRICT (state_succ (exec_plan (s,as1)) act) vs)
                     = (state_succ (DRESTRICT (exec_plan (s,as'')) vs)
                                   (action_proj (act,vs)))` by FULL_SIMP_TAC(srw_ss())[GSYM (drest_succ_proj_eq_drest_succ |> Q.SPECL[`(exec_plan (s,as1))`])]
             THEN FULL_SIMP_TAC(srw_ss())[]
             THEN `(DRESTRICT (exec_plan (s,as'')) vs) = (DRESTRICT s vs)` by METIS_TAC[empty_change_no_change]
             THEN FULL_SIMP_TAC(srw_ss())[]
             THEN FULL_SIMP_TAC(srw_ss())[succ_drest_eq_drest_succ]
             THEN `(state_succ s (action_proj (act,vs))) IN 
                      (state_successors (prob_proj(PROB,vs)) s)` by
                     (SRW_TAC[][state_successors_def]
                     THEN1(Q.EXISTS_TAC `(action_proj (act,vs))`
                           THEN SRW_TAC[][]
                           THEN METIS_TAC[action_proj_in_prob_proj])
                     THEN1(METIS_TAC[vars_change_cons_2]))
             THEN MP_TAC (S_geq_S_succ_plus_ell |> INST_TYPE[beta |-> ``:bool``] |> Q.GEN `s'` |> Q.SPEC `state_succ s (action_proj (act,vs))`)
             THEN SRW_TAC[][]
             THEN DECIDE_TAC)))

(*Obtaining S bound: second step, removing condless and effless actions from the given \as*)
val problem_plan_bound_S_bound_2nd_step = store_thm("problem_plan_bound_S_bound_2nd_step",
``top_sorted_abs (state_successors (prob_proj(PROB,vs))) lss /\
  FINITE (PROB:'a problem) /\ ((set lss) = valid_states (prob_proj(PROB,vs))) /\
  s IN valid_states PROB /\ as IN valid_plans PROB
  ==> ?as'. (exec_plan(s,as') = exec_plan(s,as)) /\ sublist as' as /\
            LENGTH as' <= S vs lss PROB (DRESTRICT s vs)``,
SRW_TAC[][]
THEN assume_thm_concl_after_proving_assums (problem_plan_bound_S_bound_1st_step |> Q.GEN `as` |> Q.SPEC `rem_condless_act(s, [], rem_effectless_act as)`)
THEN SRW_TAC[][]
THEN1 METIS_TAC[rem_effectless_works_4', rem_condless_valid_10]
THEN1 METIS_TAC[rem_condless_valid_9, rem_effectless_works_6]
THEN1 METIS_TAC[rem_condless_valid_2]
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][]
THEN1 METIS_TAC[rem_condless_valid_1, rem_effectless_works_14]
THEN METIS_TAC[sublist_trans, rem_condless_valid_8, rem_effectless_works_9])

(*If the constructed \as is bounded by S of a projected state s, then it is bounded by the
  MAX of S for all valid states in the proj*)
val problem_plan_bound_S_bound_3rd_step = store_thm("problem_plan_bound_S_bound_3rd_step",
``top_sorted_abs (state_successors (prob_proj(PROB,vs))) lss /\
  FINITE (PROB:'a problem) /\ ((set lss) = valid_states (prob_proj(PROB,vs))) /\
  s IN valid_states PROB /\ as IN valid_plans PROB
  ==> ?as'. (exec_plan(s,as') = exec_plan(s,as)) /\ sublist as' as /\
            LENGTH as' <= MAX_SET {S vs lss PROB s' | s' | s' IN (valid_states (prob_proj(PROB, vs)))}``,
SRW_TAC[][]
THEN MP_TAC problem_plan_bound_S_bound_2nd_step
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][]
THEN `S vs lss PROB (DRESTRICT s vs) <= MAX_SET {S vs lss PROB s' | s' | s' ∈ valid_states (prob_proj (PROB,vs))}` by 
    (MATCH_MP_TAC (SIMP_RULE(srw_ss())[AND_IMP_INTRO] ((CONV_RULE RIGHT_IMP_FORALL_CONV) (in_max_set |> Q.SPEC `s`)))
     THEN SRW_TAC[][]
     THEN1(`{S vs lss PROB s' | s' | s' ∈ valid_states (prob_proj (PROB,vs))} = IMAGE (S vs lss PROB) (valid_states (prob_proj (PROB,vs)))` by SRW_TAC[][IMAGE_DEF, EXTENSION]
           THEN `FINITE ((valid_states (prob_proj ((PROB:'a problem),vs))):'a state set)` by METIS_TAC[finite_imp_finite_prob_proj, FINITE_valid_states]
           THEN METIS_TAC[IMAGE_FINITE])
     THEN1 METIS_TAC[two_pow_n_is_a_bound_2])
THEN DECIDE_TAC)

(*Thus, the sublist diameter has that bound.*)
val problem_plan_bound_S_bound = store_thm("problem_plan_bound_S_bound",
``top_sorted_abs (state_successors (prob_proj(PROB,vs))) lss /\
  FINITE (PROB:'a problem) /\ ((set lss) = valid_states (prob_proj(PROB,vs)))
  ==> problem_plan_bound PROB <= MAX_SET {S vs lss PROB s' | s' | s' IN (valid_states (prob_proj(PROB, vs)))}``,
SRW_TAC[][LE_LT1]
THEN HO_MATCH_MP_TAC problem_plan_bound_UBound
THEN SRW_TAC[][GSYM LE_LT1]
THEN METIS_TAC[problem_plan_bound_S_bound_3rd_step])

val sspace_DAG_def = Define`sspace_DAG PROB lss = ((set lss) = valid_states (PROB)) /\ top_sorted_abs (state_successors PROB) lss`

val problem_plan_bound_S_bound_2nd_step_thesis = store_thm("problem_plan_bound_S_bound_2nd_step_thesis",
``
  FINITE (PROB:'a problem)  /\ sspace_DAG (prob_proj(PROB,vs)) lss /\
  s IN valid_states PROB /\ as IN valid_plans PROB
  ==> ?as'. (exec_plan(s,as') = exec_plan(s,as)) /\ sublist as' as /\
            LENGTH as' <= S vs lss PROB (DRESTRICT s vs)``,
SRW_TAC[][problem_plan_bound_S_bound_2nd_step, sspace_DAG_def])

val problem_plan_bound_S_bound_thesis = store_thm("problem_plan_bound_S_bound_thesis",
``FINITE (PROB:'a problem) /\ sspace_DAG (prob_proj(PROB,vs)) lss
  ==> problem_plan_bound PROB <= MAX_SET {S vs lss PROB s' | s' | s' IN (valid_states (prob_proj(PROB, vs)))}``,
SRW_TAC[][problem_plan_bound_S_bound, sspace_DAG_def])

val _ = export_theory();
