open HolKernel Parse boolLib bossLib;

open listTheory;
open arithmeticTheory;
open list_utilsTheory;
open rich_listTheory;
open lcsymtacs

val _ = new_theory "sublist"
val sublist_def = Define`
    		  (sublist [] l1 = T) /\
  (sublist (h::t) [] = F) /\
  (sublist (x::l1) (y::l2) = (x = y) /\ sublist l1 l2 \/ sublist (x::l1) l2)`;

val sublist_EQNS = store_thm(
  "sublist_EQNS",
  ``(sublist [] l = T) /\ (sublist (h::t) [] = F)``,
  SRW_TAC[][sublist_def]);
val _ = export_rewrites ["sublist_EQNS"]

(* val _  = overload_on ("<=", ``sublist``) *)

(* This is to make the sublist theory usable by SRW_TAC *)
val _ = export_rewrites ["sublist_def"];
val sublist_refl = store_thm(
  "sublist_refl",
  ``!l. sublist l l``,
  Induct_on `l` THEN SRW_TAC[][sublist_def]);
val _ = export_rewrites ["sublist_refl"]

val sublist_cons = store_thm("sublist_cons",
``!l1 l2 h. sublist l1 l2 ==> sublist l1 (h::l2)``,
  Induct_on `l1` THEN SRW_TAC [][] THEN Cases_on `l2` THEN
  SRW_TAC [][sublist_def]);

val sublist_NIL = store_thm(
  "sublist_NIL",
  ``sublist (l1:'a list) [] <=> (l1 = [])``,
  Cases_on `l1` THEN SRW_TAC [][sublist_def]);
val _ = export_rewrites ["sublist_NIL"]

val sublist_trans = store_thm(
  "sublist_trans",
  ``!l1 l2 l3:'a list. sublist l1  l2 /\ sublist l2 l3 ==> sublist l1 l3``,
  Induct_on `l3` THEN SIMP_TAC (srw_ss()) [] THEN
  Cases_on `l1` THEN SRW_TAC [][] THEN
  Cases_on `l2` THEN FULL_SIMP_TAC (srw_ss()) [] THEN
  METIS_TAC [sublist_def]);

val sublist_length = store_thm("sublist_length",
``!l l'. sublist l l' ==> LENGTH l <= LENGTH l'``,
HO_MATCH_MP_TAC (theorem "sublist_ind") THEN SRW_TAC [][sublist_def] THEN
RES_TAC THEN DECIDE_TAC);

val sublist_CONS1_E = store_thm(
  "sublist_CONS1_E",
  ``!l1 l2. sublist (h::l1) l2 ==> sublist l1 l2``,
  HO_MATCH_MP_TAC (theorem "sublist_ind") THEN SRW_TAC [][sublist_def] THEN
  FULL_SIMP_TAC (srw_ss()) []);

val sublist_equal_lengths = store_thm(
  "sublist_equal_lengths",
  ``!l1 l2. sublist l1 l2 /\ (LENGTH l1 = LENGTH l2) ==> (l1 = l2)``,
  HO_MATCH_MP_TAC (theorem "sublist_ind") THEN SRW_TAC [][sublist_def] THENL [
    METIS_TAC [LENGTH_NIL],
    IMP_RES_TAC sublist_length THEN FULL_SIMP_TAC (srw_ss() ++ ARITH_ss) [],
    METIS_TAC[sublist_CONS1_E]
  ]);

val sublist_antisym = store_thm(
  "sublist_antisym",
  ``sublist l1 l2 /\ sublist l2 l1 ==> (l1 = l2)``,
  METIS_TAC [arithmeticTheory.LESS_EQUAL_ANTISYM, sublist_equal_lengths, sublist_length]);

val sublist_append_back = store_thm(
  "sublist_append_I",
  ``!l1 l2. sublist l1 (l2 ++ l1)``,
  Induct_on `l2` THEN SRW_TAC [][sublist_def] THEN
  METIS_TAC [sublist_cons]);

val sublist_snoc = store_thm(
  "sublist_snoc",
  ``!l1 l2 h. sublist l1 l2 ==> sublist l1 (l2 ++ [h])``,
  HO_MATCH_MP_TAC (theorem "sublist_ind") THEN SRW_TAC [][sublist_def] THEN
  METIS_TAC []);

val sublist_append_front = store_thm(
  "sublist_append_front",
  ``!l1 l2. sublist l1 (l1 ++ l2)``,
  Induct_on `l1` THEN SRW_TAC [][sublist_def]);

val sublist_append1_E = store_thm(
  "sublist_append1_E",
  ``sublist (l1 ++ l2) l ==> sublist l1 l /\ sublist l2 l``,
  METIS_TAC [sublist_trans, sublist_append_front, sublist_append_back]);

val append_sublist_1 = save_thm("append_sublist_1", sublist_append1_E);


val sublist_cons_exists = store_thm(
  "sublist_cons_exists",
  ``!h l1 l2.
      sublist (h::l1) l2 <=>
      ?l2a l2b. (l2 = l2a ++ [h] ++ l2b) /\
                ~MEM h l2a /\
                sublist l1 l2b``,
  Induct_on `l2` THEN SRW_TAC [][sublist_def] THEN
  EQ_TAC THEN REPEAT STRIP_TAC THENL [
    MAP_EVERY Q.EXISTS_TAC [`[]`, `l2`] THEN SRW_TAC [][],
    Cases_on `h = h'` THEN SRW_TAC [][] THENL [
      Q.EXISTS_TAC `[]` THEN SRW_TAC[][] THEN
      METIS_TAC[sublist_append_back, sublist_trans],
      Q.EXISTS_TAC `h::l2a` THEN SRW_TAC [][]
    ],
    Cases_on `h = h'` THEN SRW_TAC [][] THEN
    Cases_on `l2a` THEN FULL_SIMP_TAC (srw_ss()) [] THEN
    METIS_TAC[]
  ]);

val sublist_append_exists = store_thm(
  "sublist_append_exists",
  ``!l1 l2 l3.
      sublist (l1 ++ l2) l3 <=>
      ?l3a l3b. (l3 = l3a ++ l3b) /\ sublist l1 l3a /\ sublist l2 l3b``,
  Induct THEN SRW_TAC [][sublist_def, EQ_IMP_THM, sublist_cons_exists] THENL [
    METIS_TAC [APPEND],
    METIS_TAC [sublist_trans, sublist_append_back],
    SRW_TAC [boolSimps.DNF_ss][] THEN METIS_TAC[],
    SRW_TAC [boolSimps.DNF_ss][] THEN METIS_TAC[]
  ]);


val sublist_append_both_I = store_thm(
  "sublist_append_both_I",
  ``sublist a b /\ sublist c d ==> sublist (a ++ c) (b ++ d)``,
  METIS_TAC [sublist_append_exists]);



val sublist_append = store_thm("sublist_append",
``!l1 l1' l2 l2'. sublist l1 l1' /\ sublist l2 l2'
                        ==> sublist (l1 ++ l2) (l1' ++ l2')``,
SRW_TAC[][sublist_append_both_I]);


val sublist_append_2 = store_thm("sublist_append_2",
``!l1 l2 l3. sublist l1 l2
                        ==> sublist l1 (l2 ++ l3)``,
SRW_TAC[][]
THEN `sublist [] l3` by SRW_TAC[][sublist_def]
THEN MP_TAC(sublist_append |> Q.SPECL [`l1`, `l2`, `[]`, `l3`] )
THEN SRW_TAC[][]);


val append_sublist = store_thm("append_sublist",
``!l1 l2 l3 l. sublist (l1 ++ l2 ++ l3) l
      	       ==> sublist (l1 ++ l3) l``,
Induct_on `l`
THEN SRW_TAC[][sublist_def]
THEN Cases_on `l1`
THEN FULL_SIMP_TAC(srw_ss())[sublist_def]
THEN1 METIS_TAC[append_sublist_1]
THEN1 METIS_TAC[]
THEN REWRITE_TAC[ Once (GSYM (MATCH_MP AND2_THM APPEND))]
THEN Q.PAT_X_ASSUM `sublist (h'::(t ++ l2 ++ l3)) l` (ASSUME_TAC o REWRITE_RULE[(GSYM (MATCH_MP AND2_THM APPEND))])
THEN METIS_TAC[]);


val sublist_subset = store_thm("sublist_subset",
  ``!l1 l2. sublist l1 l2 ==> set l1 SUBSET set l2``,
  SIMP_TAC (srw_ss()) [pred_setTheory.SUBSET_DEF] THEN
  HO_MATCH_MP_TAC (theorem "sublist_ind") THEN SRW_TAC [][sublist_def] THEN
  METIS_TAC[]);


val sublist_filter = store_thm("sublist_filter",
  ``!l P. sublist (FILTER P l) l``,
  Induct THEN SRW_TAC [][sublist_def, sublist_cons]);


val sublist_cons_3 = save_thm("sublist_cons_3", sublist_CONS1_E);


val sublist_cons_2 = store_thm("sublist_cons_2",
``!l1 l2 h. sublist (h::l1) (h::l2)
                        <=> sublist l1 l2``,
SRW_TAC[][]
THEN EQ_TAC
THEN1( SRW_TAC[][sublist_def]
THEN METIS_TAC[sublist_cons_3])
THEN1( MP_TAC(sublist_append |> Q.SPECL [`[h]`, `[h]`, `l1`, `l2`] )
THEN SRW_TAC[][sublist_def]));

val sublist_every = store_thm("sublist_every",
``!l1 l2 P. sublist l1 l2 /\ EVERY P l2 ==> EVERY P l1``,
METIS_TAC[EVERY_MEM, sublist_subset, pred_setTheory.SUBSET_DEF]);

val sublist_SING_MEM = store_thm(
  "sublist_SING_MEM",
  ``sublist [h] l <=> MEM h l``,
  Induct_on `l` THEN SRW_TAC [][sublist_def]);
val _ = export_rewrites ["sublist_SING_MEM"]


val sublist_every = store_thm("sublist_every",
``!l1 l2 P. sublist (l1) (l2) /\ EVERY P l2
                        ==> EVERY P l1 ``,
Induct_on `l2`
THEN SRW_TAC[][sublist_def]
THEN Cases_on `l1`
THEN SRW_TAC[][sublist_def]
THEN FULL_SIMP_TAC(bool_ss)[sublist_def]
THEN FIRST_X_ASSUM (Q.SPECL_THEN [`h'::t`, `P`] MP_TAC)
THEN SRW_TAC[][]
);

val sublist_append_exists = store_thm("sublist_append_exists",
``!l1 l2 h. sublist (h::l1) (l2)
                        ==> ?l3 l4. (l2 = l3 ++ [h] ++ l4 ) /\ sublist l1 l4``,
METIS_TAC[sublist_cons_exists]);

val sublist_append_4 = store_thm("sublist_append_4",
``!l l1 l2 h. sublist (h::l) (l1 ++ [h] ++ l2) /\ EVERY (\x. ~(h = x)) l1
                        ==> sublist l l2``,
Induct_on `l1`
THEN SRW_TAC[][sublist_def]
THEN METIS_TAC[sublist_cons_3]);

val sublist_append_5 = store_thm("sublist_append_5",
``!l l1 l2 h. sublist (h::l) (l1 ++ l2) /\ EVERY (\x. ~(h = x)) l1
                        ==> sublist (h::l) l2``,
Induct_on `l1`
THEN SRW_TAC[][sublist_def]
THEN METIS_TAC[]);
val sublist_append_6 = store_thm("sublist_append_6",
``!l l1 l2 h. sublist (h::l) (l1++l2) /\ ~(MEM h l1)
              ==> sublist (h::l) l2``,
Induct_on `l1`
THEN SRW_TAC[][sublist_def]
THEN METIS_TAC[])

val sublist_MEM = store_thm("sublist_MEM",
``!h l1 l2. sublist (h::l1) (l2)
              ==> MEM h l2``,
Induct_on `l2`
THEN SRW_TAC[][]
THEN METIS_TAC[])

val sublist_cons_4 = store_thm("sublist_cons_4",
``!l h l'. sublist l l' ==> sublist l (h::l')``,
Induct_on `l`
THEN SRW_TAC[][sublist_def]);

val sublist_imp_len_filter_le = store_thm("sublist_imp_len_filter_le",
``!P l l'. sublist l' l ==> LENGTH (FILTER P l') <= LENGTH (FILTER P l)``,
STRIP_TAC
THEN Induct_on `l`
THEN Induct_on `l'`
THEN SRW_TAC[][sublist_def]
THEN METIS_TAC[sublist_cons_3, LENGTH, FILTER, LESS_EQ_TRANS, LESS_EQ_SUC_REFL])

val list_with_three_types_shorten_type2 = store_thm("list_with_three_types_shorten_type2",
``!P1 P2 P3 k1 f PProbs PProbl s l.
    PProbs(s) /\ PProbl(l)
    /\ (!l s. PProbs(s) /\  PProbl(l) /\ EVERY P1 l
              ==> ?l'. (f(s, l') = f(s, l)) 
                       /\ (LENGTH (FILTER P2 l') <= k1)
                       /\ (LENGTH (FILTER P3 l') <= LENGTH (FILTER P3 l))
                       /\ (EVERY P1 l') /\ sublist l' l)
    /\ (!s l1 l2. (f(f(s, l1), l2) = f(s, l1 ++ l2)))
    /\ (!s l. PProbs(s) /\ PProbl(l) ==> PProbs(f(s, l)))
    /\ (!l1 l2. sublist l1 l2 /\ PProbl(l2) ==> PProbl(l1))
    /\ (!l1 l2. PProbl(l1 ++ l2) <=> (PProbl(l1) /\ PProbl(l2)))
    ==> ?l'. (f(s, l') = f(s, l)) 
             /\ (LENGTH (FILTER P3 l') <= LENGTH (FILTER P3 l))
             /\ (!l''. list_frag (l', l'') /\ EVERY P1 l''
                       ==> LENGTH (FILTER P2 l'') <= k1)
             /\ sublist l' l``,
NTAC 7 STRIP_TAC
THEN Induct_on`FILTER (\x. ~P1 x) l`
THEN SRW_TAC[][]
THENL
[
	FULL_SIMP_TAC(srw_ss())[FILTER_EQ_NIL]
        THEN `EVERY P1 l` by METIS_TAC[]
        THEN LAST_X_ASSUM (MP_TAC o Q.SPECL[`l`, `s`])
        THEN SRW_TAC[][]
        THEN Q.EXISTS_TAC `l'`
        THEN SRW_TAC[][]
        THEN METIS_TAC[frag_len_filter_le, LESS_EQ_TRANS]
	,
	FULL_SIMP_TAC(srw_ss())[FILTER_EQ_CONS]
        THEN FIRST_X_ASSUM (Q.SPECL_THEN  [`P1`, `l2`] MP_TAC)
	THEN SRW_TAC[][]
        THEN `PProbl l2` by METIS_TAC[APPEND_ASSOC]
        THEN FIRST_X_ASSUM ( MP_TAC o Q.SPEC `f (f (s,l1),[h])`)
	THEN SRW_TAC[][]
        THEN `∃l'.
                 (f (f (f (s,l1),[h]),l') = f (f (f (s,l1),[h]),l2)) ∧
                  LENGTH (FILTER P3 l') ≤ LENGTH (FILTER P3 l2) ∧
                  (∀l''.
                     list_frag (l',l'') ∧ EVERY P1 l'' ⇒
                     LENGTH (FILTER P2 l'') ≤ k1) ∧ sublist l' l2` by METIS_TAC[]
        THEN FULL_SIMP_TAC(bool_ss)[filter_empty_every_not]
        THEN `EVERY P1 l1` by METIS_TAC[]
        THEN LAST_X_ASSUM (MP_TAC o Q.SPECL[`l1`,`s`])
        THEN SRW_TAC[][]
        THEN `PProbs (f (s,l1 ++ [h]))` by METIS_TAC[]
        THEN `∃l'.
               (f (s,l1 ++ [h] ++ l') = f (s,l1 ++ [h] ++ l2)) ∧
                  LENGTH (FILTER P3 l') ≤ LENGTH (FILTER P3 l2) ∧
                     (∀l''.
                       list_frag (l',l'') ∧ EVERY P1 l'' ⇒
                       LENGTH (FILTER P2 l'') ≤ k1) ∧ sublist l' l2` by METIS_TAC[]
        THEN Q.EXISTS_TAC `l'' ++ [h] ++ l'''`
        THEN SRW_TAC[][]
	THENL
	[
		Q.PAT_X_ASSUM `∀s l1 l2. f (f (s,l1),l2) = f (s,l1 ++ l2)` (MP_TAC o GSYM) THEN STRIP_TAC
		THEN REWRITE_TAC[GSYM APPEND_ASSOC]
		THEN ASM_SIMP_TAC(bool_ss)[] 
		THEN FULL_SIMP_TAC(srw_ss())[]
		,
                REWRITE_TAC[FILTER_APPEND]
		THEN REWRITE_TAC[LENGTH_APPEND]
		THEN SRW_TAC[][]
                THEN DECIDE_TAC
		,	
                `EVERY P1 l1` by METIS_TAC[]
                THEN MP_TAC(LIST_FRAG_DICHOTOMY_2|> Q.SPECL[`l''''`,`l''`,`h`,`l'''`,`P1`])
		THEN SRW_TAC[][]	 
		THENL
		[
                        MP_TAC(frag_len_filter_le |> Q.SPECL[`P2`,`l''''`,`l''`])
			THEN SRW_TAC[][]
			THEN DECIDE_TAC     
			,
                        METIS_TAC[]
		]																	 
		,
		FULL_SIMP_TAC(srw_ss())[sublist_append]		
	]
])

val isPREFIX_sublist = store_thm(
  "isPREFIX_sublist",
  ``∀x y. x <<= y ⇒ sublist x y``,
  Induct >> simp[] >> qx_genl_tac [`h`, `y`] >> Cases_on `y` >>
  simp[]);

val _ = export_theory()
