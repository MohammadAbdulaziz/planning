#!/bin/bash
while IFS='' read -r line || [[ -n "$line" ]]; do
 old=`echo $line | cut -d " " -f 1`;
 new=`echo $line | cut -d " " -f 2`;
 # echo $line
 # echo ${old}_${new}
 sed -i 's/'"$old"'/'"$new"'/g' *Script.sml
done < usedNames

