open HolKernel Parse boolLib bossLib;

open finite_mapTheory
open arithmeticTheory
open pred_setTheory
open rich_listTheory
open listTheory;							 
open utilsTheory;
open factoredSystemTheory 
open sublistTheory;
open parentChildStructureTheory;
open systemAbstractionTheory;
(* open depGraphVtxCutTheory; *)
open factoredSystemTheory;
open topologicalPropsTheory;
open prim_recTheory;
open dependencyTheory
open set_utilsTheory

val _ = new_theory "parentTwoChildrenStructure";

val two_children_parent_rel_def = 
      Define`two_children_parent_rel(PROB, vs_1, vs_2)
                <=> ~(dep_var_set(PROB, vs_1, (prob_dom PROB) DIFF vs_1)) /\ ~(dep_var_set(PROB, vs_2, (prob_dom PROB) DIFF vs_2))`;
val two_children_parent_in_MPLS_leq_2_pow_n = store_thm("two_children_parent_in_MPLS_leq_2_pow_n",
``!PROB vs_1 vs_2. two_children_parent_rel(PROB, vs_1, vs_2)
                   ==> child_parent_rel(PROB, vs_1 UNION vs_2)``,
SRW_TAC[][two_children_parent_rel_def, child_parent_rel_def, dep_var_set_def, DISJOINT_DEF, UNION_DEF,
          INTER_DEF, DIFF_DEF, GSPEC_ETA, EMPTY_DEF]
THEN METIS_TAC[])

val indep_rel_def = 
      Define`indep_rel(PROB, vs_1)
                <=> ~(dep_var_set(PROB, vs_1, (prob_dom PROB) DIFF vs_1)) /\ ~(dep_var_set(PROB, (prob_dom PROB) DIFF vs_1, vs_1))`;

val two_children_parent_bound_main_lemma_2 = store_thm("two_children_parent_bound_main_lemma_2",
``!PROB vs_1 vs_2. vs_1 SUBSET (prob_dom PROB) /\ vs_2 SUBSET (prob_dom PROB) /\ DISJOINT vs_1 vs_2
                   /\ two_children_parent_rel(PROB, vs_1, vs_2)
                   ==> indep_rel(prob_proj(PROB, vs_1 UNION vs_2), vs_1)``,
SRW_TAC[][two_children_parent_rel_def, indep_rel_def]
THEN `(vs_1 UNION vs_2) SUBSET (prob_dom PROB)` by SRW_TAC[][]
THEN MP_TAC(two_children_parent_mems_le_finite 
               |> Q.SPECL[`PROB`, `vs_1 UNION vs_2`])
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN1( `(vs_1 UNION vs_2) DIFF vs_1 = vs_2` 
           by (SRW_TAC[][UNION_DEF, DIFF_DEF, EXTENSION, IN_DEF] 
               THEN FULL_SIMP_TAC(srw_ss())[DISJOINT_DEF, INTER_DEF, EXTENSION, IN_DEF] 
               THEN METIS_TAC[])
       THEN SRW_TAC[][]
       THEN `vs_2 SUBSET ((prob_dom PROB) DIFF vs_1)` by 
             (FULL_SIMP_TAC(srw_ss())[SUBSET_DEF, DIFF_DEF, DISJOINT_DEF, EXTENSION]
              THEN METIS_TAC[])
       THEN METIS_TAC[not_dep_disj_imp_not_dep, NDEP_PROJ_NDEP,
                      disj_diff, DISJOINT_SYM])
THEN1( `(vs_1 UNION vs_2) DIFF vs_1 = vs_2` 
           by (SRW_TAC[][UNION_DEF, DIFF_DEF, EXTENSION, IN_DEF] 
               THEN FULL_SIMP_TAC(srw_ss())[DISJOINT_DEF, INTER_DEF, EXTENSION, IN_DEF] 
               THEN METIS_TAC[])
       THEN SRW_TAC[][]
       THEN `vs_1 SUBSET ((prob_dom PROB) DIFF vs_2)` by 
             (FULL_SIMP_TAC(srw_ss())[SUBSET_DEF, DIFF_DEF, DISJOINT_DEF, EXTENSION]
              THEN METIS_TAC[])
       THEN METIS_TAC[not_dep_disj_imp_not_dep, NDEP_PROJ_NDEP,
                      disj_diff, DISJOINT_SYM]))

val indep_rel_imp_child_par_child_par_diff = store_thm("indep_rel_imp_child_par_child_par_diff",
``!PROB vs. vs SUBSET (prob_dom PROB) /\ indep_rel(PROB, vs)
            ==> child_parent_rel(PROB, vs) /\ child_parent_rel(PROB, (prob_dom PROB) DIFF vs)``,
SRW_TAC[][indep_rel_def, child_parent_rel_def]
THEN METIS_TAC[subset_diff_diff_eq])

val _ = export_theory();
