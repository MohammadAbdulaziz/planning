open HolKernel Parse boolLib bossLib;
open factoredSystemTheory
open systemAbstractionTheory
open pred_setTheory

open stateSpaceProductTheory
open set_utilsTheory
open actionSeqProcessTheory
open fmap_utilsTheory
open functionsLib
open arithmeticTheory
open topologicalPropsTheory

val _ = new_theory "prodValidtd";

val state_list_proj_eq_state_proj_as_proj = store_thm("state_list_proj_eq_state_proj_as_proj",
``!s. sat_precond_as(s, as)
       ==> (ss_proj (traversed_states(s,as)) vs = (traversed_states(DRESTRICT s vs, as_proj(as, vs))))``,
REWRITE_TAC[ss_proj_def]
THEN Induct_on `as`
THEN1 SRW_TAC[][sat_precond_as_def, traversed_states_def, as_proj_def, state_list_def]
THEN SRW_TAC[][traversed_states_def, state_list_def, as_proj_def]
THEN SRW_TAC[][GSYM traversed_states_def]
THEN FULL_SIMP_TAC(srw_ss())[sat_precond_as_def]
THEN1 METIS_TAC[drest_succ_proj_eq_drest_succ]
THEN SRW_TAC[][sat_precond_as_proj_3]
THEN METIS_TAC[init_is_traversed, ABSORPTION])

val traversed_states_is_stateSpace = store_thm("traversed_states_is_stateSpace",
``!s. s IN valid_states PROB /\ as IN valid_plans PROB ==>
      stateSpace (traversed_states(s,as)) (prob_dom PROB)``,
Induct_on `as`
THEN1 FULL_SIMP_TAC(srw_ss())[stateSpace_def, traversed_states_def, state_list_def, valid_states_def]
THEN SRW_TAC[][stateSpace_def, traversed_states_def, state_list_def]
THEN1 FULL_SIMP_TAC(srw_ss())[valid_states_def]
THEN `s' IN valid_states PROB` by
       METIS_TAC[MEM_statelist_FDOM, valid_action_valid_succ, valid_plan_valid_head, valid_plan_valid_tail]
THEN FULL_SIMP_TAC(srw_ss())[valid_states_def])


(*We need to have projections because:
  1) proving catd_traversed_states_leq_prod_td_proj needs a projection with bounded traversals
  2) having a concrete sys with bounded traversals doesn't guaratee that the same holds for a projection e.g.:
     concrete sys {a_1,a_2,a_3....} each action requires some value of var, and the value is less than or equal 4. Each action increments v. For this system the largets number of traversed states is 4.
     An abstraction that removes v can traverse as many states...
  *)
val bounded_traversal_system_def = Define`bounded_traversal_system PROB = (!vs. ?k.!s as. s IN valid_states (prob_proj(PROB,vs)) /\ as IN valid_plans (prob_proj(PROB,vs)) ==>  CARD(traversed_states(s,as)) <= k)`

val traversed_states_subset_valid_states = store_thm("traversed_states_subset_valid_states",
``s IN valid_states PROB /\ as IN valid_plans PROB
  ==> (traversed_states (s,as)) SUBSET (valid_states PROB)``,
SRW_TAC[][traversed_states_def, SUBSET_DEF]
THEN METIS_TAC[MEM_statelist_FDOM])

val traversed_states_leq_td = store_thm("traversed_states_leq_td",
``s IN valid_states PROB /\ as IN valid_plans PROB /\ bounded_traversal_system PROB
  ==> 
  CARD (traversed_states(s,as)) - 1  <= td(PROB)``,
REWRITE_TAC[td_def]
THEN REPEAT STRIP_TAC
THEN `CARD (traversed_states (s,as)) − 1 IN
  {CARD (traversed_states (s,as)) − 1 |
   s ∈ valid_states PROB ∧ as ∈ valid_plans PROB}`
     by (SRW_TAC[][] THEN METIS_TAC[])
THEN assume_thm_concl_after_proving_assums(((CONV_RULE RIGHT_IMP_FORALL_CONV) (in_max_set |> Q.SPEC `{CARD (traversed_states (s,as)) − 1 |
   s ∈ valid_states PROB ∧ as ∈ valid_plans PROB}`)) |> Q.SPEC `CARD (traversed_states (s,as)) − 1`)
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[bounded_traversal_system_def]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPEC `prob_dom (PROB: (α |-> β) # (α |-> β) -> bool)`)
THEN SRW_TAC[][]
THEN MATCH_MP_TAC(bound_child_parent_neq_mems_state_set_neq_len)
THEN Q.EXISTS_TAC `k`
THEN SRW_TAC[][]
THEN ASSUME_TAC(traversed_states_geq_1 |> Q.SPECL[`s''`, `as''`])
THEN FULL_SIMP_TAC(srw_ss())[]
THEN `CARD (traversed_states (s'',as'')) ≤ k` by METIS_TAC[PROJ_DOM_IDEMPOT]
THEN DECIDE_TAC)

val propositional_sys_bounded_traversal = store_thm("propositional_sys_bounded_traversal",
``FINITE PROB ==> bounded_traversal_system (PROB:'a problem)``,
SRW_TAC[][bounded_traversal_system_def]
THEN Q.EXISTS_TAC `2 ** CARD (prob_dom PROB)`
THEN SRW_TAC[][]
THEN `CARD (traversed_states (s,as)) <= 2 ** CARD (prob_dom (prob_proj(PROB,vs)))` by 
      PROVE_TAC[FINITE_valid_states, traversed_states_subset_valid_states,
                CARD_SUBSET, finite_imp_finite_prob_proj, CARD_valid_states, LESS_EQ_TRANS, LESS_EQ_REFL]
THEN FULL_SIMP_TAC(srw_ss())[graph_plan_neq_mems_state_set_neq_len]
THEN `0 < 2` by SRW_TAC[][]
THEN METIS_TAC[CARD_INTER_LESS_EQ, FINITE_prob_dom,
               EXP_BASE_LEQ_MONO_IMP, LESS_EQ_TRANS])

val bool_traversed_states_leq_td = store_thm("bool_traversed_states_leq_td",
``FINITE PROB/\ s IN valid_states PROB /\ as IN valid_plans (PROB :'a problem)
  ==> 
  CARD (traversed_states(s,as)) - 1  <= td(PROB)``,
METIS_TAC[propositional_sys_bounded_traversal, traversed_states_leq_td])

val bounded_traversal_system_proj = store_thm("bounded_traversal_system_proj",
``bounded_traversal_system PROB
  ==> bounded_traversal_system (prob_proj (PROB,vs))``,
SRW_TAC[][bounded_traversal_system_def]
THEN METIS_TAC[prob_proj_inter])

val traversed_states_subset_proj_prodf = store_thm("traversed_states_subset_proj_prodf",
``!VS. 
  FINITE VS ==> 
  (!PROB s as.
   (((!vs'. vs' IN VS ==> DISJOINT vs vs') /\ (!vs vs'. vs IN VS /\ vs' IN VS /\ vs <> vs' ==> DISJOINT vs vs') /\
     (BIGUNION (vs INSERT VS) = (prob_dom PROB)) /\
     (s IN valid_states PROB) /\ (as IN valid_plans PROB) /\
     (sat_precond_as(s,as))) ==>
     (traversed_states(s,as) SUBSET PRODf (IMAGE (\vs. ss_proj (traversed_states(s,as)) vs) VS) (ss_proj (traversed_states(s,as)) vs))))``,
  Induct_on `FINITE`
  THEN SRW_TAC[][]
  THEN1(METIS_TAC[traversed_states_is_stateSpace, exec_drest_5, dom_subset_ssproj_eq_ss, SET_EQ_SUBSET])
  THEN `e ∪ (vs ∪ BIGUNION VS) = prob_dom PROB` by
              (`vs UNION e = e UNION vs` by SRW_TAC[][UNION_COMM]
               THEN FULL_SIMP_TAC(srw_ss())[UNION_ASSOC])
        THEN
        `traversed_states (s,as) SUBSET
         stateSpaceProduct (ss_proj (traversed_states (s,as)) e)
                             (PRODf (IMAGE (\vs. ss_proj (traversed_states (s,as)) vs) VS)
                                    (ss_proj (traversed_states (s,as)) vs))` by
          (`ss_proj (traversed_states (s,as)) (vs UNION BIGUNION VS) SUBSET
            PRODf
              (IMAGE (\vs. ss_proj (traversed_states (s,as)) vs) VS)
                (ss_proj (traversed_states (s,as)) vs)` by
            (`sat_precond_as(DRESTRICT s (vs UNION BIGUNION VS), as_proj(as, vs UNION BIGUNION VS))` by METIS_TAC[sat_precond_drest_as_proj]
            THEN FIRST_ASSUM(assume_thm_concl_after_proving_assums o Q.SPECL[`prob_proj(PROB, vs UNION BIGUNION VS)`, `DRESTRICT s (vs UNION BIGUNION VS)`, `as_proj(as, vs UNION BIGUNION VS)`])
            THEN1(SRW_TAC[][]
                  THEN1 METIS_TAC[graph_plan_neq_mems_state_set_neq_len, INTER_UNION]
                  THEN1 SRW_TAC[][two_pow_n_is_a_bound_2]
                  THEN1 SRW_TAC[][valid_as_valid_as_proj])
            THEN `(traversed_states
                       (DRESTRICT s (vs UNION BIGUNION VS),
                          as_proj (as,vs UNION BIGUNION VS))) =
                    ss_proj (traversed_states(s,as)) (vs UNION BIGUNION VS)` by 
                   FULL_SIMP_TAC(srw_ss())[state_list_proj_eq_state_proj_as_proj]
            THEN FULL_SIMP_TAC(srw_ss())[]
            THEN `(IMAGE
                    (\vs'.
                       ss_proj
                         (ss_proj (traversed_states (s,as))
                            (vs UNION BIGUNION VS)) vs') VS) =
                    (IMAGE
                      (\vs'.
                         ss_proj (traversed_states (s,as)) vs') VS)` by
                 (MATCH_MP_TAC(eq_funs_eq_images)
                  THEN SRW_TAC[][GSYM invariantStateSpace_thm_9]
                  THEN METIS_TAC[UNION_COMM, SUBSET_INTER_ABSORPTION, subset_imp_subset_union, SUBSET_BIGUNION_I])
            THEN FULL_SIMP_TAC(srw_ss())[]
            THEN `(ss_proj (ss_proj (traversed_states (s,as)) (vs ∪ BIGUNION VS)) vs) =
                      (ss_proj (traversed_states (s,as)) vs)` by
                   SRW_TAC[][GSYM invariantStateSpace_thm_9, INTER_UNION]
            THEN FULL_SIMP_TAC(srw_ss())[])
          THEN `stateSpaceProduct (ss_proj (traversed_states (s,as)) e)
                                    (ss_proj (traversed_states (s,as)) (vs UNION BIGUNION VS))
                SUBSET
                stateSpaceProduct (ss_proj (traversed_states (s,as)) e)
                                    (PRODf (IMAGE (\vs. ss_proj (traversed_states (s,as)) vs) VS) (ss_proj (traversed_states (s,as)) vs))` by
            METIS_TAC[invariantStateSpace_thm_8, state_list_proj_eq_state_proj_as_proj, eq_imp_subset]
          THEN `traversed_states (s,as) SUBSET
                stateSpaceProduct (ss_proj (traversed_states (s,as)) e)
                                    (ss_proj (traversed_states (s,as)) (vs UNION BIGUNION VS))` by
            (MATCH_MP_TAC(invariantStateSpace_thm_7)
            THEN Q.EXISTS_TAC `e ∪ (vs ∪ BIGUNION VS)`
            THEN SRW_TAC[][traversed_states_is_stateSpace, stateSpaceProduct_comm_thm]
            THEN METIS_TAC[SUBSET_UNION, UNION_ASSOC, UNION_COMM])
          THEN METIS_TAC[SUBSET_TRANS])
        THEN assume_thm_concl_after_proving_assums (Q.SPECL[`(ss_proj (traversed_states (s,as)) e)`, `(IMAGE (λvs. ss_proj (traversed_states (s,as)) vs) VS)`] PRODf_INSERT)
             THEN1(SRW_TAC[][]
                   THEN1(Cases_on `~(vs' IN VS)`
                         THEN SRW_TAC[][]
                         THEN MATCH_MP_TAC(neq_vs_neq_ss_proj)
                         THEN Q.EXISTS_TAC `e ∪ (vs ∪ BIGUNION VS)`
                         THEN SRW_TAC[][traversed_states_nempty, traversed_states_is_stateSpace]
                         THEN1 PROVE_TAC[subset_imp_subset_union, UNION_ASSOC, UNION_COMM, SUBSET_BIGUNION_I]
                         THEN1 METIS_TAC[in_not_in_neq])
                   THEN1 METIS_TAC[nempty_stateSpace_nempty_ss_proj, traversed_states_nempty]
                   THEN1(Q.EXISTS_TAC `vs'`
                         THEN Q.EXISTS_TAC `vs''`
                         THEN SRW_TAC[][]
                         THEN1(MATCH_MP_TAC(subset_dom_stateSpace_ss_proj)
                               THEN Q.EXISTS_TAC `prob_dom PROB`
                               THEN SRW_TAC[][]
                               THEN1 PROVE_TAC[subset_imp_subset_union, UNION_ASSOC, UNION_COMM, SUBSET_BIGUNION_I] 
                               THEN1 METIS_TAC[traversed_states_is_stateSpace])
                         THEN1(MATCH_MP_TAC(subset_dom_stateSpace_ss_proj)
                               THEN Q.EXISTS_TAC `prob_dom PROB`
                               THEN SRW_TAC[][]
                               THEN1 PROVE_TAC[subset_imp_subset_union, UNION_ASSOC, UNION_COMM, SUBSET_BIGUNION_I]
                               THEN1 METIS_TAC[traversed_states_is_stateSpace])
                         THEN1 METIS_TAC[DISJOINT_SYM])
                   THEN1 METIS_TAC[nempty_stateSpace_nempty_ss_proj, traversed_states_nempty]
                   THEN1(Q.EXISTS_TAC `e`
                         THEN MATCH_MP_TAC(subset_dom_stateSpace_ss_proj)
                         THEN Q.EXISTS_TAC `prob_dom PROB`
                         THEN SRW_TAC[][]
                         THEN1 PROVE_TAC[subset_imp_subset_union, SUBSET_UNION,UNION_COMM]
                         THEN1 METIS_TAC[traversed_states_is_stateSpace])
                   THEN1(Q.EXISTS_TAC `e`
                         THEN Q.EXISTS_TAC `vs'`
                         THEN SRW_TAC[][]
                         THEN1(MATCH_MP_TAC(subset_dom_stateSpace_ss_proj)
                               THEN Q.EXISTS_TAC `prob_dom PROB`
                               THEN SRW_TAC[][]
                               THEN1 PROVE_TAC[subset_imp_subset_union, SUBSET_UNION,UNION_COMM]
                               THEN1 METIS_TAC[traversed_states_is_stateSpace])
                         THEN1(MATCH_MP_TAC(subset_dom_stateSpace_ss_proj)
                               THEN Q.EXISTS_TAC `prob_dom PROB`
                               THEN SRW_TAC[][]
                               THEN1 PROVE_TAC[subset_imp_subset_union, UNION_ASSOC, UNION_COMM, SUBSET_BIGUNION_I]
                               THEN1 METIS_TAC[traversed_states_is_stateSpace])
                         THEN1 METIS_TAC[DISJOINT_SYM]))
  THEN METIS_TAC[])

val traversed_states_subset_proj_prodf' = store_thm("traversed_states_subset_proj_prodf'",
``FINITE VS /\
   ((!vs'. vs' IN VS ==> DISJOINT vs vs') /\ (!vs vs'. vs IN VS /\ vs' IN VS /\ vs <> vs' ==> DISJOINT vs vs') /\
     (BIGUNION (vs INSERT VS) = (prob_dom PROB)) /\
     (s IN valid_states PROB) /\ (as IN valid_plans PROB) /\
     (sat_precond_as(s,as))) ==>
     (traversed_states(s,as) SUBSET PRODf (IMAGE (\vs. ss_proj (traversed_states(s,as)) vs) VS) (ss_proj (traversed_states(s,as)) vs))``,
METIS_TAC[traversed_states_subset_proj_prodf])

val finite_ss_proj_prod = store_thm("finite_ss_proj_prod",
``!VS.
  FINITE VS /\ ~(vs IN VS) /\ (!vs'. vs' IN VS ==> DISJOINT vs vs') /\ (!vs vs'. vs IN VS /\ vs' IN VS /\ vs <> vs' ==> DISJOINT vs vs') /\
     (BIGUNION (vs INSERT VS) = (prob_dom PROB)) /\
     (s IN valid_states PROB) /\ (as IN valid_plans PROB) /\
     (sat_precond_as(s,as)) ==>
     (FINITE (PRODf (IMAGE (\vs. ss_proj (traversed_states(s,as)) vs) VS) (ss_proj (traversed_states(s,as)) vs)))``,
SRW_TAC[][]
THEN MATCH_MP_TAC(PRODf_FINITE')
THEN SRW_TAC[][]
THEN1(Cases_on `~(vs' IN VS)`
      THEN SRW_TAC[][]
      THEN MATCH_MP_TAC(neq_vs_neq_ss_proj)
      THEN Q.EXISTS_TAC `(vs ∪ BIGUNION VS)`
      THEN SRW_TAC[][traversed_states_nempty, traversed_states_is_stateSpace]
      THEN1 PROVE_TAC[subset_imp_subset_union, UNION_ASSOC, UNION_COMM, SUBSET_BIGUNION_I]
      THEN1 METIS_TAC[in_not_in_neq])
THEN1 METIS_TAC[nempty_stateSpace_nempty_ss_proj, traversed_states_nempty]
THEN1(Q.EXISTS_TAC `vs`
      THEN MATCH_MP_TAC(subset_dom_stateSpace_ss_proj)
      THEN Q.EXISTS_TAC `prob_dom PROB`
      THEN SRW_TAC[][]
      THEN1 PROVE_TAC[subset_imp_subset_union, UNION_ASSOC, UNION_COMM, UNION_SUBSET, SUBSET_REFL] 
      THEN1 METIS_TAC[traversed_states_is_stateSpace])
THEN1 METIS_TAC[nempty_stateSpace_nempty_ss_proj, traversed_states_nempty]
THEN1(Q.EXISTS_TAC `vs'`
      THEN Q.EXISTS_TAC `vs''`
      THEN SRW_TAC[][]
      THEN1(MATCH_MP_TAC(subset_dom_stateSpace_ss_proj)
            THEN Q.EXISTS_TAC `prob_dom PROB`
            THEN SRW_TAC[][]
            THEN1 PROVE_TAC[subset_imp_subset_union, UNION_ASSOC, UNION_COMM, SUBSET_BIGUNION_I] 
            THEN1 METIS_TAC[traversed_states_is_stateSpace])
      THEN1(MATCH_MP_TAC(subset_dom_stateSpace_ss_proj)
            THEN Q.EXISTS_TAC `prob_dom PROB`
            THEN SRW_TAC[][]
            THEN1 PROVE_TAC[subset_imp_subset_union, UNION_ASSOC, UNION_COMM, SUBSET_BIGUNION_I]
            THEN1 METIS_TAC[traversed_states_is_stateSpace])
      THEN1 METIS_TAC[DISJOINT_SYM])
THEN1(Q.EXISTS_TAC `vs`
      THEN Q.EXISTS_TAC `vs'`
      THEN SRW_TAC[][]
      THEN1(MATCH_MP_TAC(subset_dom_stateSpace_ss_proj)
            THEN Q.EXISTS_TAC `prob_dom PROB`
            THEN SRW_TAC[][]
            THEN1 PROVE_TAC[subset_imp_subset_union, SUBSET_UNION,UNION_COMM]
            THEN1 METIS_TAC[traversed_states_is_stateSpace])
      THEN1(MATCH_MP_TAC(subset_dom_stateSpace_ss_proj)
            THEN Q.EXISTS_TAC `prob_dom PROB`
            THEN SRW_TAC[][]
            THEN1 PROVE_TAC[subset_imp_subset_union, UNION_ASSOC, UNION_COMM, SUBSET_BIGUNION_I]
            THEN1 METIS_TAC[traversed_states_is_stateSpace]))
THEN SRW_TAC[][finite_traversed_states, state_list_proj_eq_state_proj_as_proj])

val prod_traversed_states_proj_catd = store_thm("prod_traversed_states_proj_catd",
``!VS. 
  FINITE VS ==> 
  (!PROB s as.
   (((!vs'. vs' IN VS ==> DISJOINT vs vs') /\ (!vs vs'. vs IN VS /\ vs' IN VS /\ vs <> vs' ==> DISJOINT vs vs') /\
     (BIGUNION (vs INSERT VS) = (prob_dom PROB)) /\
     (s IN valid_states PROB) /\ (as IN valid_plans PROB) /\
     (sat_precond_as(s,as)) /\ ~(vs IN VS)) ==>
     CARD (PRODf (IMAGE (\vs. ss_proj (traversed_states(s,as)) vs) VS) (ss_proj (traversed_states(s,as)) vs)) <=
          Π CARD ((ss_proj (traversed_states(s,as)) vs) INSERT (IMAGE (\vs. ss_proj (traversed_states(s,as)) vs) VS))))``,
SRW_TAC[][]
THEN assume_thm_concl_after_proving_assums(PRODf_CARD |> Q.SPECL[`(IMAGE (\vs. ss_proj (traversed_states(s,as)) vs) VS)`])
THEN1 METIS_TAC[IMAGE_FINITE]
THEN FIRST_X_ASSUM (MATCH_MP_TAC o Q.SPEC `(ss_proj (traversed_states(s,as)) vs)`)
THEN SRW_TAC[][]
THEN1(Cases_on `~(vs' IN VS)`
      THEN SRW_TAC[][]
      THEN MATCH_MP_TAC(neq_vs_neq_ss_proj)
      THEN Q.EXISTS_TAC `(vs ∪ BIGUNION VS)`
      THEN SRW_TAC[][traversed_states_nempty, traversed_states_is_stateSpace]
      THEN1 PROVE_TAC[subset_imp_subset_union, UNION_ASSOC, UNION_COMM, SUBSET_BIGUNION_I]
      THEN1 METIS_TAC[in_not_in_neq])
THEN1 METIS_TAC[nempty_stateSpace_nempty_ss_proj, traversed_states_nempty]
THEN1(Q.EXISTS_TAC `vs`
      THEN MATCH_MP_TAC(subset_dom_stateSpace_ss_proj)
      THEN Q.EXISTS_TAC `prob_dom PROB`
      THEN SRW_TAC[][]
      THEN1 PROVE_TAC[subset_imp_subset_union, SUBSET_UNION,UNION_COMM]
      THEN1 METIS_TAC[traversed_states_is_stateSpace])
THEN1 METIS_TAC[nempty_stateSpace_nempty_ss_proj, traversed_states_nempty]
THEN1(Q.EXISTS_TAC `vs'`
      THEN Q.EXISTS_TAC `vs''`
      THEN SRW_TAC[][]
      THEN1(MATCH_MP_TAC(subset_dom_stateSpace_ss_proj)
            THEN Q.EXISTS_TAC `prob_dom PROB`
            THEN SRW_TAC[][]
            THEN1 PROVE_TAC[subset_imp_subset_union, UNION_ASSOC, UNION_COMM, SUBSET_BIGUNION_I] 
            THEN1 METIS_TAC[traversed_states_is_stateSpace])
      THEN1(MATCH_MP_TAC(subset_dom_stateSpace_ss_proj)
            THEN Q.EXISTS_TAC `prob_dom PROB`
            THEN SRW_TAC[][]
            THEN1 PROVE_TAC[subset_imp_subset_union, UNION_ASSOC, UNION_COMM, SUBSET_BIGUNION_I]
            THEN1 METIS_TAC[traversed_states_is_stateSpace])
      THEN1 METIS_TAC[DISJOINT_SYM])
THEN1(Q.EXISTS_TAC `vs`
      THEN Q.EXISTS_TAC `vs'`
      THEN SRW_TAC[][]
      THEN1(MATCH_MP_TAC(subset_dom_stateSpace_ss_proj)
            THEN Q.EXISTS_TAC `prob_dom PROB`
            THEN SRW_TAC[][]
            THEN1 PROVE_TAC[subset_imp_subset_union, UNION_ASSOC, UNION_COMM, SUBSET_UNION]
            THEN1 METIS_TAC[traversed_states_is_stateSpace])
      THEN1(MATCH_MP_TAC(subset_dom_stateSpace_ss_proj)
            THEN Q.EXISTS_TAC `prob_dom PROB`
            THEN SRW_TAC[][]
            THEN1 PROVE_TAC[subset_imp_subset_union, UNION_ASSOC, UNION_COMM, SUBSET_BIGUNION_I]
            THEN1 METIS_TAC[traversed_states_is_stateSpace]))
THEN METIS_TAC[FINITE_ss_proj, finite_traversed_states])

val prod_traversed_states_proj_catd' = store_thm("prod_traversed_states_proj_catd'",
``(FINITE VS /\ (!vs'. vs' IN VS ==> DISJOINT vs vs') /\ (!vs vs'. vs IN VS /\ vs' IN VS /\ vs <> vs' ==> DISJOINT vs vs') /\
     (BIGUNION (vs INSERT VS) = (prob_dom PROB)) /\
     (s IN valid_states PROB) /\ (as IN valid_plans PROB) /\
     (sat_precond_as(s,as)) /\ ~(vs IN VS) ==>
     CARD (PRODf (IMAGE (\vs. ss_proj (traversed_states(s,as)) vs) VS) (ss_proj (traversed_states(s,as)) vs)) <=
          Π CARD ((ss_proj (traversed_states(s,as)) vs) INSERT (IMAGE (\vs. ss_proj (traversed_states(s,as)) vs) VS)))``,
METIS_TAC[prod_traversed_states_proj_catd])

val catd_traversed_states_leq_catd_comp_ss_proj = store_thm("catd_traversed_states_leq_catd_comp_ss_proj",
``!VS. 
  FINITE VS ==> 
  (!PROB s as.
   (((!vs'. vs' IN VS ==> DISJOINT vs vs') /\ (!vs vs'. vs IN VS /\ vs' IN VS /\ vs <> vs' ==> DISJOINT vs vs') /\
     (BIGUNION (vs INSERT VS) = (prob_dom PROB)) /\
     (s IN valid_states PROB) /\ (as IN valid_plans PROB) /\
     (sat_precond_as(s,as)) /\ ~(vs IN VS)) ==>
      Π CARD ((ss_proj (traversed_states(s,as)) vs) INSERT (IMAGE (\vs. ss_proj (traversed_states(s,as)) vs) VS)) <=
         Π (CARD o (\vs. (ss_proj (traversed_states(s,as)) vs))) (vs INSERT VS)))``,
SRW_TAC[][]
THEN `Π CARD (IMAGE (\vs. ss_proj (traversed_states(s,as)) vs) (vs INSERT VS)) <=
         Π (CARD o (\vs. (ss_proj (traversed_states(s,as)) vs))) (vs INSERT VS)` by
     (MATCH_MP_TAC(PROD_IMAGE_COMPOSE')
      THEN SRW_TAC[][]
      THEN METIS_TAC[CARD_EQ_0, traversed_states_nempty, finite_traversed_states, ss_proj_def, IMAGE_EQ_EMPTY, NOT_ZERO_LT_ZERO, GREATER_DEF, CARD_EMPTY, IMAGE_FINITE])
THEN FULL_SIMP_TAC(srw_ss())[])

val catd_traversed_states_leq_catd_comp_ss_proj' = store_thm("catd_traversed_states_leq_catd_comp_ss_proj'",
``(FINITE VS /\ (!vs'. vs' IN VS ==> DISJOINT vs vs') /\ (!vs vs'. vs IN VS /\ vs' IN VS /\ vs <> vs' ==> DISJOINT vs vs') /\
     (BIGUNION (vs INSERT VS) = (prob_dom PROB)) /\
     (s IN valid_states PROB) /\ (as IN valid_plans PROB) /\
     (sat_precond_as(s,as)) /\ ~(vs IN VS)) ==>
      Π CARD ((ss_proj (traversed_states(s,as)) vs) INSERT (IMAGE (\vs. ss_proj (traversed_states(s,as)) vs) VS)) <=
         Π (CARD o (\vs. (ss_proj (traversed_states(s,as)) vs))) (vs INSERT VS)``,
METIS_TAC[catd_traversed_states_leq_catd_comp_ss_proj])

val catd_traversed_states_leq_prod_td_proj = store_thm("catd_traversed_states_leq_prod_td_proj",
``!VS. 
  FINITE VS ==> 
  (!PROB s as.
   ((bounded_traversal_system PROB /\ (!vs'. vs' IN VS ==> DISJOINT vs vs') /\ (!vs vs'. vs IN VS /\ vs' IN VS /\ vs <> vs' ==> DISJOINT vs vs') /\
     (BIGUNION (vs INSERT VS) = (prob_dom PROB)) /\
     (s IN valid_states PROB) /\ (as IN valid_plans PROB) /\
     (sat_precond_as(s,as)) /\ ~(vs IN VS)) ==>
      Π (CARD o (\vs. (ss_proj (traversed_states(s,as)) vs))) (vs INSERT VS) <=
         Π (\vs. td(prob_proj(PROB,vs)) + 1) (vs INSERT VS)))``,
SRW_TAC[][]
THEN MATCH_MP_TAC(PROD_IMAGE_MONO_LESS_EQ')
THEN SRW_TAC[][Once ADD_COMM, state_list_proj_eq_state_proj_as_proj]
THEN REWRITE_TAC[GSYM SUB_RIGHT_LESS_EQ]
THEN MATCH_MP_TAC(traversed_states_leq_td)
THEN SRW_TAC[][two_pow_n_is_a_bound_2, valid_as_valid_as_proj, bounded_traversal_system_proj])

val catd_traversed_states_leq_prod_td_proj' = store_thm("catd_traversed_states_leq_prod_td_proj'",
``(FINITE VS /\ (bounded_traversal_system PROB /\ (!vs'. vs' IN VS ==> DISJOINT vs vs') /\ (!vs vs'. vs IN VS /\ vs' IN VS /\ vs <> vs' ==> DISJOINT vs vs') /\
     (BIGUNION (vs INSERT VS) = (prob_dom PROB)) /\
     (s IN valid_states PROB) /\ (as IN valid_plans PROB) /\
     (sat_precond_as(s,as)) /\ ~(vs IN VS)) ==>
      Π (CARD o (\vs. (ss_proj (traversed_states(s,as)) vs))) (vs INSERT VS) <=
         Π (\vs. td(prob_proj(PROB,vs)) + 1) (vs INSERT VS))``,
METIS_TAC[catd_traversed_states_leq_prod_td_proj])

val prodValidtd = store_thm("prodValidtdS",
``!VS. 
  FINITE VS /\
   FINITE PROB /\ ~(vs IN VS) /\ bounded_traversal_system PROB /\ (!vs'. vs' IN VS ==> DISJOINT vs vs')
    /\ (!vs vs'. vs IN VS /\ vs' IN VS /\ vs <> vs' ==> DISJOINT vs vs') /\
     (BIGUNION (vs INSERT VS) = (prob_dom PROB)) /\ ~(vs IN VS) ==>
      td PROB <=
         Π (\vs. td(prob_proj(PROB,vs)) + 1) (vs INSERT VS) - 1``,
SRW_TAC[][]
THEN MATCH_MP_TAC(td_UBound)
THEN SRW_TAC[][]
THEN `CARD (traversed_states (s,as)) <=
      CARD (PRODf (IMAGE (λvs. ss_proj (traversed_states (s,as)) vs) VS)
            (ss_proj (traversed_states (s,as)) vs))` by
     (MATCH_MP_TAC(CARD_SUBSET')
      THEN SRW_TAC[][]
      THEN1(MATCH_MP_TAC(finite_ss_proj_prod)
            THEN SRW_TAC[][])
      THEN1(MATCH_MP_TAC(traversed_states_subset_proj_prodf')
            THEN SRW_TAC[][]))
THEN `CARD (PRODf (IMAGE (λvs. ss_proj (traversed_states (s,as)) vs) VS)
            (ss_proj (traversed_states (s,as)) vs)) <=
        Π CARD
           (ss_proj (traversed_states (s,as)) vs INSERT
              IMAGE (λvs. ss_proj (traversed_states (s,as)) vs) VS)` by
      (MATCH_MP_TAC(prod_traversed_states_proj_catd')
       THEN SRW_TAC[][finite_traversed_states])
THEN `Π CARD
           (ss_proj (traversed_states (s,as)) vs INSERT
              IMAGE (λvs. ss_proj (traversed_states (s,as)) vs) VS) <=
        Π (CARD o (λvs. ss_proj (traversed_states (s,as)) vs))
              (vs INSERT VS)` by 
       (MATCH_MP_TAC(catd_traversed_states_leq_catd_comp_ss_proj')
        THEN SRW_TAC[][finite_traversed_states])
THEN `Π (CARD o (λvs. ss_proj (traversed_states (s,as)) vs))
         (vs INSERT VS) <=
         Π (λvs. td (prob_proj (PROB,vs)) + 1) (vs INSERT VS)` by
     (MATCH_MP_TAC(catd_traversed_states_leq_prod_td_proj')
      THEN SRW_TAC[][finite_traversed_states])
THEN METIS_TAC[LESS_EQ_TRANS])

val prodValidtd_propositional_sys = store_thm("prodValidtd_propositional_sys",
``!VS. 
  FINITE VS /\
   FINITE PROB /\ ~(vs IN VS) /\ (!vs'. vs' IN VS ==> DISJOINT vs vs')
    /\ (!vs vs'. vs IN VS /\ vs' IN VS /\ vs <> vs' ==> DISJOINT vs vs') /\
     (BIGUNION (vs INSERT VS) = (prob_dom (PROB:'a problem))) /\ ~(vs IN VS) ==>
      td PROB <=
         Π (\vs. td(prob_proj(PROB,vs)) + 1) (vs INSERT VS) - 1``,
METIS_TAC[prodValidtd, propositional_sys_bounded_traversal])

val _ = export_theory();