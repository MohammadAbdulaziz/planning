open HolKernel Parse boolLib bossLib;
open finite_mapTheory
open arithmeticTheory
open pred_setTheory
open rich_listTheory
open listTheory;
open factoredSystemTheory 
open sublistTheory;
open relationTheory;
open set_utilsTheory;
open fmap_utilsTheory;
open rel_utilsTheory;
open list_utilsTheory;
open actionSeqProcessTheory;
open topologicalPropsTheory;
open dependencyTheory;

val _ = new_theory "systemAbstraction";

val action_proj_def = Define `action_proj(a, vs) = (DRESTRICT (FST a) vs,DRESTRICT (SND a) vs)` ;

val action_proj_pair = store_thm("action_proj_pair",
``action_proj((p,e), vs) = (DRESTRICT p vs,DRESTRICT e vs)``,
METIS_TAC[action_proj_def, pairTheory.PAIR, pairTheory.FST, pairTheory.SND]);

val prob_proj_def = Define`prob_proj(PROB, vs) = IMAGE (\a. action_proj(a,vs)) PROB`;

val as_proj_def = Define `(as_proj(a::as, vs) =  if (FDOM (DRESTRICT (SND a) vs) <> EMPTY) then
						     action_proj(a,vs) ::as_proj(as, vs)
else
    (as_proj(as, vs)))
/\ (as_proj([], vs) = [])`;

val as_proj_pair = store_thm("as_proj_pair",
``(as_proj((p,e)::as, vs) =  if (FDOM (DRESTRICT e vs) <> EMPTY) then
				 action_proj((p,e),vs) ::as_proj(as, vs)
else
    (as_proj(as, vs)))
/\ (as_proj([], vs) = [])``,
METIS_TAC[as_proj_def, pairTheory.PAIR, pairTheory.FST, pairTheory.SND]);

val proj_state_succ = store_thm ("proj_state_succ",
``!s a vs.
    ((FST a) SUBMAP s)
    ==> (state_succ (DRESTRICT s vs) (action_proj(a,vs)) = DRESTRICT (state_succ s a) vs)``,
SRW_TAC[][state_succ_def, action_proj_def]
THEN `FDOM(FUNION (DRESTRICT (SND a) vs)  (DRESTRICT s vs)) = FDOM(DRESTRICT (FUNION (SND a) s) vs)`
     by (SRW_TAC[][FDOM_DRESTRICT]
     THEN FULL_SIMP_TAC(srw_ss())[INTER_DEF, UNION_DEF, EXTENSION, SUBSET_DEF, SPECIFICATION, SUBMAP_DEF]
     THEN METIS_TAC[])
THEN `!x. FUNION (DRESTRICT (SND a) vs) (DRESTRICT s vs) ' x = (DRESTRICT (FUNION (SND a) s) vs) ' x`      
     by (SRW_TAC[][]
     THEN REWRITE_TAC[DRESTRICT_DEF]
     THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF,FUNION_DEF, UNION_DEF, EXTENSION, DRESTRICT_DEF]
     THEN SRW_TAC[][]
     THEN METIS_TAC[SPECIFICATION])
THEN METIS_TAC[fmap_EQ_THM, submap_drest_submap]);

val graph_plan_lemma_1 = store_thm("graph_plan_lemma_1",
``! s vs as. sat_precond_as(s, as)
             ==> (exec_plan((DRESTRICT s vs), as_proj(as, vs))
                     = ((DRESTRICT (exec_plan(s,as)) vs) ))``,
Induct_on`as`
THEN SRW_TAC[][exec_plan_def, sat_precond_as_def, as_proj_def]
THENL
[
  `state_succ (DRESTRICT s vs) (action_proj (h,vs)) =
 DRESTRICT (state_succ s h) vs` 
         by METIS_TAC[proj_state_succ]
	 THEN METIS_TAC[]
	 ,
	   FULL_SIMP_TAC(srw_ss())[drestrict_empty_iff_disj]
			THEN `DRESTRICT s vs = DRESTRICT (state_succ s h) vs` by METIS_TAC[disj_imp_eq_proj_exec]
			THEN SRW_TAC[][]
			THEN METIS_TAC[FDOM_state_succ]
]);

val proj_action_dom_eq_inter = store_thm("proj_action_dom_eq_inter",
``action_dom(action_proj(a,vs)) = (action_dom a) INTER vs``,
SRW_TAC[][action_dom_def, action_proj_def, INTER_DEF, UNION_DEF, EXTENSION]
THEN `x ∈ FDOM (DRESTRICT (FST a) vs) ∨ x ∈ FDOM (DRESTRICT (SND a) vs) ⇔
      x ∈ action_dom (FST a, SND a) /\ x ∈ vs` by
     (SRW_TAC[][action_dom_def, FDOM_DRESTRICT]
      THEN METIS_TAC[])
THEN SRW_TAC[][pairTheory.PAIR])

val graph_plan_neq_mems_state_set_neq_len = store_thm("graph_plan_neq_mems_state_set_neq_len",
``(prob_dom(prob_proj(PROB, vs))) = (prob_dom(PROB) INTER vs)``,
SRW_TAC[][prob_proj_def, prob_dom_def,
          action_dom_def, IMAGE_DEF, EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[action_dom_def, proj_action_dom_eq_inter]
THEN1(Q.EXISTS_TAC `s UNION (action_dom a)`
      THEN SRW_TAC[][]
      THEN Q.EXISTS_TAC `a`
      THEN SRW_TAC[][]
      THEN METIS_TAC[])
THEN1(Q.EXISTS_TAC `vs INTER (action_dom x')`
      THEN SRW_TAC[][INTER_DEF]
      THEN Q.EXISTS_TAC `action_proj(x',vs)`
      THEN SRW_TAC[][proj_action_dom_eq_inter]
      THEN METIS_TAC[]))
      
val two_pow_n_is_a_bound_2 = store_thm("two_pow_n_is_a_bound_2",
``!PROB vs. (s IN valid_states PROB) 
				    ==> ((DRESTRICT s vs) IN valid_states (prob_proj(PROB,vs)))``,
SRW_TAC[][graph_plan_neq_mems_state_set_neq_len, valid_states_def, FDOM_DRESTRICT])

val dom_eff_subset_imp_dom_succ_eq_proj = store_thm("dom_eff_subset_imp_dom_succ_eq_proj",
``!h s vs. (FDOM(SND h) SUBSET FDOM s) 
           ==> (FDOM(state_succ s (action_proj(h,vs))) =
                      FDOM (state_succ s h))``,
SRW_TAC[][action_proj_def, state_succ_def, FUNION_DEF, FDOM_DRESTRICT, INTER_DEF, SUBSET_DEF, UNION_DEF, EXTENSION, SUBMAP_DEF]
THEN METIS_TAC[]);

val drest_proj_succ_eq_drest_succ = store_thm("drest_proj_succ_eq_drest_succ",
``!h s vs. ((FST h) SUBMAP s) /\ (FDOM(SND h) SUBSET FDOM s) 
           ==> (DRESTRICT (state_succ s (action_proj(h,vs))) vs =
                    DRESTRICT (state_succ s h) vs)``,
SRW_TAC[][]
THEN `FDOM(DRESTRICT (state_succ s (action_proj(h,vs))) vs) = FDOM (DRESTRICT (state_succ s h) vs)` by
     SRW_TAC[][dom_eff_subset_imp_dom_succ_eq_proj, FDOM_DRESTRICT]
THEN `!x. (DRESTRICT (state_succ s (action_proj(h,vs))) vs) ' x = (DRESTRICT (state_succ s h) vs) ' x` by
     (FULL_SIMP_TAC(srw_ss())[action_proj_def, state_succ_def, FUNION_DEF, DRESTRICT_DEF, SUBSET_DEF, INTER_DEF, EXTENSION]
     THEN SRW_TAC[][]
     THENL
     [
       FULL_SIMP_TAC(srw_ss())[FUNION_DEF]
       THEN SRW_TAC[][]
       THEN1 FULL_SIMP_TAC(srw_ss())[DRESTRICT_DEF]
       THEN METIS_TAC[x_in_fdom_eq_in_fdom_drestrict]
       ,
       FULL_SIMP_TAC(srw_ss())[FUNION_DEF]
       THEN SRW_TAC[][]
       THEN1 FULL_SIMP_TAC(srw_ss())[DRESTRICT_DEF]
       THEN METIS_TAC[x_in_fdom_eq_in_fdom_drestrict]
       ,
       FULL_SIMP_TAC(srw_ss())[FUNION_DEF]
       THEN SRW_TAC[][]
       THEN1 FULL_SIMP_TAC(srw_ss())[DRESTRICT_DEF]
       THEN METIS_TAC[x_in_fdom_eq_in_fdom_drestrict]
       ,
       FULL_SIMP_TAC(srw_ss())[FUNION_DEF, SUBMAP_DEF, FDOM_DRESTRICT, INTER_DEF, DRESTRICT_DEF]
       THEN METIS_TAC[x_in_fdom_eq_in_fdom_drestrict]
       ,
       FULL_SIMP_TAC(srw_ss())[FUNION_DEF, SUBMAP_DEF, FDOM_DRESTRICT, INTER_DEF, DRESTRICT_DEF]
       THEN METIS_TAC[x_in_fdom_eq_in_fdom_drestrict]
     ])
THEN METIS_TAC[fmap_EQ_THM]);

val drest_succ_proj_eq_drest_succ = store_thm("drest_succ_proj_eq_drest_succ",
``!s vs a . FST a SUBMAP s ==>
            (state_succ (DRESTRICT s vs) (action_proj(a, vs))=
                DRESTRICT (state_succ s a) vs)``,
SRW_TAC[][state_succ_def,action_proj_def]
THEN `FDOM (FUNION (DRESTRICT (SND a) vs) (DRESTRICT s vs)) = FDOM (DRESTRICT (FUNION (SND a) s) vs)` by
     (SRW_TAC[][FDOM_DRESTRICT]
     THEN FULL_SIMP_TAC(srw_ss())[INTER_DEF, UNION_DEF, EXTENSION, SUBSET_DEF, SPECIFICATION]
     THEN METIS_TAC[])
THEN `!x. FUNION (DRESTRICT (SND a) vs) (DRESTRICT s vs) ' x = (DRESTRICT (FUNION (SND a) s) vs) ' x` by (SRW_TAC[][]
     THEN REWRITE_TAC[DRESTRICT_DEF]
     THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF,FUNION_DEF, UNION_DEF, EXTENSION, DRESTRICT_DEF]
     THEN SRW_TAC[][]
     THEN METIS_TAC[SPECIFICATION])
THEN METIS_TAC[fmap_EQ_THM, FDOM_FUNION, submap_drest_submap]);

val exec_drest_cons_proj_eq_succ = store_thm("exec_drest_cons_proj_eq_succ",
``! as PROB vs a. FST a ⊑ s 
                  ==>(exec_plan (DRESTRICT s vs, action_proj(a,vs)::as) 
                          = exec_plan(DRESTRICT (state_succ s a) vs ,as))``,
Induct_on`as`
THEN SRW_TAC[][exec_plan_def, prob_proj_def] 
THEN METIS_TAC[drest_succ_proj_eq_drest_succ]);

val exec_drest = store_thm("exec_drest",
``!as a vs. ((FST a) SUBMAP s) ==>
                 (exec_plan(DRESTRICT (state_succ s a) vs, as) = 
                         exec_plan(DRESTRICT s vs,action_proj(a, vs)::as))``,
SRW_TAC[][proj_state_succ, exec_plan_def])

val not_empty_eff_in_as_proj = store_thm("not_empty_eff_in_as_proj",
``!as a vs. FDOM (DRESTRICT (SND a) vs) <> ∅ ==>
     ((as_proj (a::as,vs)) =  (action_proj(a, vs)::as_proj(as,vs))) ``,
SRW_TAC[][action_proj_def, as_proj_def]);

val empty_eff_not_in_as_proj = store_thm("empty_eff_not_in_as_proj",
``!as a vs. (FDOM (DRESTRICT (SND a) vs) = ∅) ==>
      (as_proj (a::as,vs) = as_proj(as,vs))``,
SRW_TAC[][action_proj_def, as_proj_def]);

val empty_eff_drest_no_eff = store_thm("empty_eff_drest_no_eff",
``!s a vs. (FDOM (DRESTRICT (SND a) vs) = ∅) ==>
       (DRESTRICT (state_succ s (action_proj (a,vs))) vs = DRESTRICT s vs)``,
SRW_TAC[][action_proj_def]
THEN `FDOM (DRESTRICT (DRESTRICT (SND a) vs) vs) = EMPTY` by FULL_SIMP_TAC(srw_ss())[DRESTRICT_IDEMPOT]
THEN FULL_SIMP_TAC(srw_ss())[Once FDOM_DRESTRICT]
THEN `DISJOINT (FDOM (DRESTRICT (SND (DRESTRICT (FST a) vs,DRESTRICT (SND a) vs)) vs)) vs` by SRW_TAC[][DISJOINT_DEF]
THEN FULL_SIMP_TAC(srw_ss())[disj_imp_eq_proj_exec]); 

val sat_precond_exec_as_proj_eq_proj_exec = store_thm("sat_precond_exec_as_proj_eq_proj_exec",
``!as vs s. (sat_precond_as(s, as))
                ==> (exec_plan(DRESTRICT s vs, as_proj(as, vs))
                       = DRESTRICT (exec_plan(s, as)) vs)``,
Induct_on`as`
THEN1 SRW_TAC[][as_proj_def, exec_plan_def] 
THEN SRW_TAC[][as_proj_def, exec_plan_def, sat_precond_as_def, drest_succ_proj_eq_drest_succ]
THEN METIS_TAC[sat_precond_as_proj_3, drest_succ_proj_eq_drest_succ, empty_eff_drest_no_eff])

val action_proj_in_prob_proj = store_thm("action_proj_in_prob_proj",
``a IN PROB ==> (action_proj(a,vs) IN (prob_proj(PROB, vs)))``,
SRW_TAC[][action_proj_def, prob_proj_def]
THEN METIS_TAC[])

val valid_as_valid_as_proj = store_thm("valid_as_valid_as_proj",
``!PROB vs. as IN valid_plans PROB
            ==> (as_proj(as,vs)) IN valid_plans (prob_proj(PROB,vs))``,
Induct_on `as`
THEN SRW_TAC[][as_proj_def, valid_plans_def,  action_proj_in_prob_proj]
THEN FULL_SIMP_TAC(srw_ss())[valid_plans_def])

val finite_imp_finite_prob_proj = store_thm("finite_imp_finite_prob_proj",
``!PROB. FINITE PROB  ==> (FINITE (prob_proj(PROB,vs)))``,
SRW_TAC[][prob_proj_def])

val two_pow_n_is_a_bound = store_thm("two_pow_n_is_a_bound",
``!PROB vs as (s:'a state). 
      FINITE PROB /\ s IN (valid_states PROB) /\ as IN (valid_plans PROB)
      /\ FINITE vs /\ LENGTH(as_proj(as, vs)) > (2**(CARD(vs))) - 1 /\ sat_precond_as(s, as)
      ==> (?as1 as2 as3. (as1 ++ as2 ++ as3 = as_proj(as, vs))
                         /\ ((exec_plan(DRESTRICT s vs, as1 ++ as2) =
                                   exec_plan (DRESTRICT s vs, as1))
                         /\ (as2 <> [])))``,
SRW_TAC[][]
THEN MP_TAC(graph_plan_card_state_set |> INST_TYPE[alpha |-> ``:'a problem``, beta |-> ``:'a``, gamma |-> ``:bool``] |> Q.SPECL[`PROB`, `vs`])
THEN SRW_TAC[][]
THEN `LENGTH (as_proj(as, (vs:'a set))) > 2 ** CARD (FDOM (DRESTRICT (s:'a state) (vs:'a set))) - 1` by 
     (SRW_TAC[][]
     THEN `2 ** (CARD (FDOM (DRESTRICT (s:'a state) (vs:'a set)))) ≤ 2 ** (CARD vs)` by SRW_TAC[][]
     THEN DECIDE_TAC)
THEN ASSUME_TAC(two_pow_n_is_a_bound_2 |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``, delta |-> ``:bool``] |> Q.SPEC `PROB:'a problem`)
THEN ASSUME_TAC(sat_precond_exec_as_proj_eq_proj_exec)
THEN ASSUME_TAC(valid_as_valid_as_proj |> INST_TYPE[beta |-> ``:bool``, gamma |-> ``:bool``] |> Q.SPECL[`PROB`, `vs`])
THEN ASSUME_TAC(finite_imp_finite_prob_proj |> INST_TYPE[beta |-> ``:bool``, gamma |-> ``:bool``] |> Q.SPEC `PROB`)
THEN MP_TAC(lemma_2 |> Q.SPECL[`prob_proj(PROB, vs)`, `as_proj (as,vs)`, `DRESTRICT s vs`])
THEN SRW_TAC[][]);

val as_proj_eq_filter_action_proj = store_thm("as_proj_eq_filter_action_proj",
``!as vs. (as_proj(as, vs)) =  (FILTER (\a. FDOM (SND a) <> EMPTY) (MAP (\ a. action_proj(a, vs)) as))``,
Induct_on `as` 
THEN SRW_TAC[][as_proj_def, action_proj_def]);

val append_eq_as_proj = store_thm("append_eq_as_proj",
``!as1 as2 as3 p vs.
      (as1++as2++as3 = as_proj(p, vs))
       ==> ?p_1 p_2 p_3. (p_1 ++ p_2 ++ p_3 = p) /\ (as2 = as_proj(p_2, vs))
                         /\ (as1 = as_proj(p_1, vs))``,
METIS_TAC[append_eq_as_proj_1, as_proj_eq_filter_action_proj]);

val succ_drest_eq_drest_succ = store_thm("succ_drest_eq_drest_succ",
``!a s vs. (state_succ (DRESTRICT s vs) (action_proj (a,vs)) =
                  DRESTRICT (state_succ s (action_proj (a,vs))) vs)``,
SRW_TAC[][state_succ_def, action_proj_def]
THEN1 (`FDOM (FUNION (DRESTRICT (SND a) vs) (DRESTRICT s vs)) = FDOM (DRESTRICT (FUNION (DRESTRICT (SND a) vs) s) vs)`
     by (SRW_TAC[][FDOM_DRESTRICT]
     THEN FULL_SIMP_TAC(srw_ss())[INTER_DEF, UNION_DEF, EXTENSION, SUBSET_DEF, SPECIFICATION, SUBMAP_DEF]
     THEN METIS_TAC[])
THEN `!x. FUNION (DRESTRICT (SND a) vs) (DRESTRICT s vs) ' x = (DRESTRICT (FUNION (DRESTRICT (SND a) vs) s) vs) ' x`      
     by (SRW_TAC[][]
     THEN REWRITE_TAC[DRESTRICT_DEF]
     THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF,FUNION_DEF, UNION_DEF, EXTENSION, DRESTRICT_DEF]
     THEN SRW_TAC[][]
     THEN METIS_TAC[SPECIFICATION])
THEN METIS_TAC[fmap_EQ_THM, submap_drest_submap])
THEN METIS_TAC[drest_smap_drest_smap_drest]);

val proj_exec_proj_eq_exec_proj = store_thm("proj_exec_proj_eq_exec_proj",
``!s as vs.(DRESTRICT (exec_plan(DRESTRICT s vs, as_proj(as, vs))) vs 
		         = exec_plan(DRESTRICT s vs, as_proj(as, vs)))``,
Induct_on `as`
THEN SRW_TAC[][exec_plan_def, as_proj_def]
THEN METIS_TAC[succ_drest_eq_drest_succ]);

val proj_exec_proj_eq_exec_proj' = store_thm("proj_exec_proj_eq_exec_proj'",
``!s as vs. (DRESTRICT (exec_plan(DRESTRICT s vs, as_proj(as, vs))) vs 
		          = DRESTRICT (exec_plan(s, as_proj(as, vs))) vs)``,
Induct_on `as`
THEN SRW_TAC[][exec_plan_def, as_proj_def]
THEN SRW_TAC[][succ_drest_eq_drest_succ]);

val graph_plan_lemma_9 = store_thm("graph_plan_lemma_9",
``!s as vs. (DRESTRICT (exec_plan( s, as_proj(as, vs))) vs = exec_plan(DRESTRICT s vs,as_proj(as, vs)))``,
SRW_TAC[][]
THEN METIS_TAC[proj_exec_proj_eq_exec_proj', proj_exec_proj_eq_exec_proj]);

val act_dom_proj_eff_subset_act_dom_eff = store_thm("act_dom_proj_eff_subset_act_dom_eff",
``!a vs. FDOM (SND(action_proj (a,vs))) SUBSET FDOM (SND a)``,
SRW_TAC[][action_proj_def, FDOM_DRESTRICT]);

val exec_as_proj_valid = store_thm("exec_as_proj_valid",
``!as s PROB vs.  s IN (valid_states PROB) /\ as IN (valid_plans PROB)  
                  ==> (exec_plan (s,as_proj(as, vs)) IN (valid_states PROB))``,
Induct_on `as`
THEN SRW_TAC[][exec_plan_def, as_proj_def, act_dom_proj_eff_subset_act_dom_eff]
THEN `(state_succ s (action_proj(h, vs))) IN (valid_states PROB)` by
     (FULL_SIMP_TAC(srw_ss())[FDOM_state_succ, SUBSET_TRANS, act_dom_proj_eff_subset_act_dom_eff, valid_states_def]
      THEN MP_TAC(act_dom_proj_eff_subset_act_dom_eff |> INST_TYPE [gamma |-> ``:'b``] |> Q.SPECL[`h:(α |-> β) # (α |-> β)`,`vs`])
      THEN SRW_TAC[][]
      THEN `h IN PROB` by
           METIS_TAC[valid_plan_valid_head]      
      THEN MP_TAC(exec_as_proj_valid_2 |> INST_TYPE [gamma |-> ``:'b``] |> Q.SPEC `h`)
      THEN SRW_TAC[][]
      THEN `FDOM (SND h) SUBSET prob_dom PROB` by
           METIS_TAC[action_dom_def, pairTheory.PAIR, UNION_SUBSET]     
      THEN METIS_TAC[SUBSET_TRANS, FDOM_state_succ])
THEN METIS_TAC[SUBSET_TRANS, FDOM_state_succ, act_dom_proj_eff_subset_act_dom_eff, valid_plan_valid_tail]);

val drest_exec_as_proj_eq_drest_exec = store_thm("drest_exec_as_proj_eq_drest_exec",
``!s as vs. sat_precond_as(s, as) ==>
        (DRESTRICT (exec_plan(s, as_proj(as, vs))) vs = DRESTRICT (exec_plan(s,  as)) vs)``,
SRW_TAC[][]
THEN `(DRESTRICT (exec_plan (s,as_proj(as,vs))) vs =
        exec_plan (DRESTRICT s vs, as_proj(as, vs)))` by METIS_TAC[graph_plan_lemma_9]
THEN `?s'. (exec_plan (DRESTRICT s vs, as_proj(as, vs)) = DRESTRICT s' vs)` by 
   (SRW_TAC[][]
	      THEN Q.EXISTS_TAC `(exec_plan(s, as_proj(as, vs)))`
	         THEN SRW_TAC[][])
THEN `DRESTRICT s' vs = DRESTRICT (exec_plan(s,as)) vs` by METIS_TAC[graph_plan_lemma_1]
THEN METIS_TAC[]);

val action_proj_idempot = store_thm("action_proj_idempot",
``!a vs. (action_proj(action_proj(a, vs), vs)) = (action_proj(a, vs))``,
SRW_TAC[][action_proj_def]);

val action_proj_idempot' = store_thm("action_proj_idempot'",
``!a vs. action_dom(a) SUBSET vs ==> ((action_proj(a, vs)) = a)``,
SRW_TAC[][action_proj_def, action_dom_pair, pairTheory.PAIR, pairTheory.FST, pairTheory.SND, exec_drest_5]);

val action_proj_idempot'' = store_thm("action_proj_idempot''",
``!P vs. prob_dom(P) SUBSET vs ==> ((prob_proj(P, vs)) = P)``,
SRW_TAC[][prob_proj_def, IMAGE_DEF, EXTENSION]
THEN METIS_TAC[action_proj_idempot', exec_as_proj_valid_2, SUBSET_TRANS]);

val sat_precond_as_proj = store_thm("sat_precond_as_proj",
``! as s s' vs.
sat_precond_as(s, as)  /\ (DRESTRICT s vs = DRESTRICT s' vs) 
==>sat_precond_as(s', as_proj(as, vs))``,
Induct_on`as` 
THEN SRW_TAC[][exec_plan_def, as_proj_def, sat_precond_as_def]
THEN `DRESTRICT (FST h) vs ⊑ s` by SRW_TAC[][sat_precond_as_proj_4]
THENL
[
  SRW_TAC[][action_proj_def]
	 THEN METIS_TAC[sat_precond_as_proj_1]
  ,
  `FST (action_proj (h,vs))  ⊑ s'` by 
      (SRW_TAC[][action_proj_def]
      THEN  METIS_TAC[sat_precond_as_proj_1])
  THEN `DRESTRICT (SND h) vs  = DRESTRICT (SND (action_proj(h, vs))) vs` by SRW_TAC[][action_proj_def]
  THEN `FST (action_proj(h, vs)) SUBMAP s` by SRW_TAC[][Once action_proj_def]
  THEN `DRESTRICT (state_succ s h) vs = DRESTRICT (state_succ s' (action_proj (h,vs))) vs` by METIS_TAC[proj_eq_proj_exec_eq]
  THEN METIS_TAC[]
  ,
  METIS_TAC[sat_precond_as_proj_3]
]);

val sat_precond_drest_as_proj = store_thm("sat_precond_drest_as_proj",
``!as s s' vs . sat_precond_as (s,as) /\ (DRESTRICT s vs = DRESTRICT s' vs)
                ==> sat_precond_as (DRESTRICT s' vs,as_proj (as,vs))``,
Induct_on `as`
THEN SRW_TAC[][as_proj_def, sat_precond_as_def]
THEN `DRESTRICT (FST h) vs ⊑ DRESTRICT s vs` by SRW_TAC[][submap_drest_submap]
THENL
[
  SRW_TAC[][action_proj_def]
  THEN METIS_TAC[]
  ,
  `FST (action_proj (h,vs))  ⊑ DRESTRICT s' vs` by 
      (SRW_TAC[][action_proj_def]
       THEN  METIS_TAC[sat_precond_as_proj_1])
  THEN `DRESTRICT (SND h) vs  = DRESTRICT (SND (action_proj(h, vs))) vs` by SRW_TAC[][action_proj_def]
  THEN `FST (action_proj(h, vs)) SUBMAP s` by SRW_TAC[][Once action_proj_def, sat_precond_as_proj_4]
  THEN `DRESTRICT (state_succ s h) vs = DRESTRICT (state_succ (DRESTRICT s' vs) (action_proj (h,vs))) vs` by 
  METIS_TAC[proj_eq_proj_exec_eq, DRESTRICT_IDEMPOT]
  THEN METIS_TAC[DRESTRICT_IDEMPOT, succ_drest_eq_drest_succ]
  ,
  METIS_TAC[sat_precond_as_proj_3]
]);

val as_proj_eq_as = store_thm("as_proj_eq_as",
``no_effectless_act as /\ as IN valid_plans PROB
  /\ prob_dom PROB SUBSET vs
  ==> (as_proj(as, vs) = as)``,
Induct_on `as`
THEN SRW_TAC[][as_proj_def]
THEN FULL_SIMP_TAC(srw_ss())[valid_plans_def, no_effectless_act_def]
THEN1 METIS_TAC[exec_as_proj_valid_2, action_proj_idempot', SUBSET_TRANS]
THEN `action_proj(h,vs) = h` by METIS_TAC[exec_as_proj_valid_2, action_proj_idempot', SUBSET_TRANS]
THEN FULL_SIMP_TAC(srw_ss())[action_proj_def]
THEN METIS_TAC[pairTheory.PAIR, pairTheory.CLOSED_PAIR_EQ]);

val exec_rem_effless_as_proj_eq_exec_as_proj = store_thm("exec_rem_effless_as_proj_eq_exec_as_proj",
``!s. exec_plan(s, as_proj (rem_effectless_act as, vs)) = exec_plan(s, as_proj (as, vs))``,
Induct_on `as`
THEN SRW_TAC[][exec_plan_def, as_proj_def, rem_effectless_act_def]
THEN FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT])

val exec_as_proj_eq_exec_as = store_thm("exec_as_proj_eq_exec_as",
``!PROB as vs s. as IN valid_plans PROB /\ prob_dom PROB SUBSET vs
                ==> (exec_plan(s, as_proj(as, vs)) = exec_plan(s, as))``,
METIS_TAC[as_proj_eq_as, exec_rem_effless_as_proj_eq_exec_as_proj, rem_effectless_works_1, rem_effectless_works_6, rem_effectless_works_9, sublist_valid_plan])

val dom_prob_proj = store_thm("dom_prob_proj",
``prob_dom (prob_proj(PROB, vs)) SUBSET vs``,
SRW_TAC[][graph_plan_neq_mems_state_set_neq_len]);

val subset_proj_absorb_1 = store_thm("subset_proj_absorb_1",
``vs1 SUBSET vs2
  ==> (action_proj (action_proj (a, vs2),vs1) = action_proj (a,vs1))``,
SRW_TAC[][action_proj_def, action_dom_pair]
THEN FULL_SIMP_TAC(srw_ss())[SUBSET_INTER_ABSORPTION, INTER_COMM])

val subset_proj_absorb = store_thm("subset_proj_absorb",
``!PROB vs1 vs2. vs1 SUBSET vs2
               ==> (prob_proj(prob_proj(PROB, vs2), vs1) = prob_proj(PROB, vs1))``,
SRW_TAC[][prob_proj_def]
THEN SRW_TAC[][EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1(Q.EXISTS_TAC `a'`
     THEN SRW_TAC[][]
     THEN METIS_TAC[subset_proj_absorb_1, exec_as_proj_valid_2, DISJOINT_SYM])
THEN1(Q.EXISTS_TAC `action_proj (a,vs2)`
     THEN SRW_TAC[][]
     THEN1 METIS_TAC[subset_proj_absorb_1, exec_as_proj_valid_2, DISJOINT_SYM]
     THEN METIS_TAC[]))

val union_proj_absorb = store_thm("union_proj_absorb",
``!PROB vs vs'. prob_proj(prob_proj(PROB, vs UNION vs'), vs) = prob_proj(PROB, vs)``,
SRW_TAC[][]
THEN `vs SUBSET (vs UNION vs')` by SRW_TAC[][]
THEN METIS_TAC[subset_proj_absorb])

val NOT_VS_IN_DOM_PROJ_PRE_EFF = store_thm("NOT_VS_IN_DOM_PROJ_PRE_EFF",
``!PROB vs v a. ~(v IN vs) /\ a IN PROB
                ==> ((v IN FDOM(FST a) ==> v IN FDOM(FST (action_proj(a, (prob_dom PROB) DIFF vs))))
                    /\ (v IN FDOM(SND a) ==> v IN FDOM(SND (action_proj(a, (prob_dom PROB) DIFF vs)))))``,
SRW_TAC[][action_proj_def]
THEN METIS_TAC[IN_FDOM_DRESTRICT_DIFF, FDOM_pre_subset_prob_dom_pair, FDOM_eff_subset_prob_dom_pair])

val IN_DISJ_DEP_IMP_DEP_DIFF = store_thm("IN_DISJ_DEP_IMP_DEP_DIFF",
``!PROB  vs vs' v v'. v  IN vs' /\ v' IN vs'  /\ DISJOINT vs vs'
              ==> (dep(PROB, v, v') ==> dep(prob_proj(PROB, (prob_dom PROB) DIFF vs), v, v'))``,
SRW_TAC[][dep_def, prob_proj_def, DIFF_DEF, INTER_DEF, EXTENSION, GSPEC_ETA, DISJOINT_DEF]
THEN Cases_on `v = v'`
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `action_proj(a, (prob_dom PROB) DIFF vs)`
THEN SRW_TAC[][]
THENL
[
   Q.EXISTS_TAC `a`
   THEN SRW_TAC[][action_proj_def, DIFF_DEF, EXTENSION, GSPEC_ETA]
   ,
   METIS_TAC[NOT_VS_IN_DOM_PROJ_PRE_EFF]
   ,
   Q.EXISTS_TAC `a`
   THEN SRW_TAC[][action_proj_def, DIFF_DEF, EXTENSION, GSPEC_ETA]
   ,
   METIS_TAC[NOT_VS_IN_DOM_PROJ_PRE_EFF]
])

val PROB_DOM_PROJ_DIFF = store_thm("PROB_DOM_PROJ_DIFF",
``!PROB vs. prob_dom (prob_proj(PROB, (prob_dom PROB) DIFF vs)) = (prob_dom PROB) DIFF vs``,
SRW_TAC[][graph_plan_neq_mems_state_set_neq_len, inter_diff_contains]);

val two_children_parent_mems_le_finite = store_thm("two_children_parent_mems_le_finite",
``!PROB vs. vs SUBSET (prob_dom PROB)
                   ==> (prob_dom (prob_proj(PROB, vs)) = vs)``,
METIS_TAC[graph_plan_neq_mems_state_set_neq_len, SUBSET_INTER_ABSORPTION, INTER_COMM])

val PROJ_DOM_PRE_EFF_SUBSET_DOM = store_thm("PROJ_DOM_PRE_EFF_SUBSET_DOM",
``!a vs. (FDOM(FST (action_proj(a, vs))) SUBSET FDOM (FST a))
         /\ (FDOM(SND (action_proj(a, vs))) SUBSET FDOM (SND a))``,
SRW_TAC[][action_proj_def, SUBSET_DEF, FDOM_DRESTRICT])

val NOT_IN_PRE_EFF_NOT_IN_PRE_EFF_PROJ = store_thm("NOT_IN_PRE_EFF_NOT_IN_PRE_EFF_PROJ",
``!a v vs. (~(v IN FDOM(FST a)) ==> ~(v IN FDOM(FST (action_proj(a, vs)))))
           /\ (~(v IN FDOM(SND a)) ==> ~(v IN FDOM(SND (action_proj(a, vs)))))``,
SRW_TAC[][]
THEN MP_TAC(REWRITE_RULE[SUBSET_DEF] PROJ_DOM_PRE_EFF_SUBSET_DOM)
THEN SRW_TAC[][]
THEN METIS_TAC[])

val NDEP_PROJ_NDEP = store_thm("NDEP_PROJ_NDEP",
``!PROB vs vs' vs''. ~dep_var_set(PROB, vs, vs')
                     ==> ~dep_var_set(prob_proj(PROB, vs''), vs, vs')``,
SRW_TAC[][dep_var_set_def, prob_proj_def, dep_def, GSYM action_proj_def]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL [`v1`, `v2`])
THEN METIS_TAC[NOT_IN_PRE_EFF_NOT_IN_PRE_EFF_PROJ])

val SUBSET_PROJ_DOM_DISJ = store_thm("SUBSET_PROJ_DOM_DISJ",
``!PROB vs vs'. vs SUBSET (prob_dom (prob_proj(PROB, (prob_dom PROB) DIFF vs')))
                ==> DISJOINT vs vs'``,
SRW_TAC[][PROB_DOM_PROJ_DIFF, SUBSET_DIFF])

val NOT_VS_DEP_IMP_DEP_PROJ = store_thm("NOT_VS_DEP_IMP_DEP_PROJ",
``!PROB vs v v'.  (~(v IN vs) /\ ~(v' IN vs)) /\
                     dep (PROB,v,v') ==>  dep (prob_proj (PROB, (prob_dom PROB) DIFF vs),v,v')``,
SRW_TAC[][dep_def, prob_proj_def]
THEN Cases_on `v = v' `
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `action_proj(a,(prob_dom PROB) DIFF vs)`
THEN SRW_TAC[][]
THENL
[
   Q.EXISTS_TAC `a`
   THEN SRW_TAC[][]
   ,
   SRW_TAC[][action_proj_def, graph_plan_neq_mems_state_set_neq_len]
   THEN MP_TAC(FDOM_pre_subset_prob_dom_pair)
   THEN MP_TAC(FDOM_eff_subset_prob_dom_pair)   
   THEN SRW_TAC[][]
   THEN FULL_SIMP_TAC(bool_ss)[pairTheory.FST, pairTheory.SND, FDOM_DRESTRICT, action_proj_def, DIFF_DEF, GSPEC_ETA, INTER_DEF, IN_DEF, SUBSET_DEF]
   THEN FULL_SIMP_TAC(srw_ss())[GSYM IN_DEF]
   THEN METIS_TAC[]
   ,
   Q.EXISTS_TAC `a`
   THEN SRW_TAC[][]
   ,
   SRW_TAC[][action_proj_def, graph_plan_neq_mems_state_set_neq_len]
   THEN MP_TAC(FDOM_pre_subset_prob_dom_pair)
   THEN MP_TAC(FDOM_eff_subset_prob_dom_pair)   
   THEN SRW_TAC[][]
   THEN FULL_SIMP_TAC(bool_ss)[pairTheory.FST, pairTheory.SND, FDOM_DRESTRICT, action_proj_def, DIFF_DEF, GSPEC_ETA, INTER_DEF, IN_DEF, SUBSET_DEF]
   THEN FULL_SIMP_TAC(srw_ss())[GSYM IN_DEF]
   THEN METIS_TAC[]
])

val DISJ_PROJ_NDEP_IMP_NDEP = store_thm("DISJ_PROJ_NDEP_IMP_NDEP",
``!PROB vs vs' vs'' . DISJOINT vs vs'' /\ DISJOINT vs vs'
                      /\ ~ dep_var_set(prob_proj(PROB, (prob_dom PROB) DIFF vs), vs', vs'')
                      ==> ~ dep_var_set(PROB, vs', vs'')``,
SRW_TAC[][dep_var_set_def]
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`v1`, `v2`])
THEN SRW_TAC[][]
THEN1 METIS_TAC[]
THEN1 METIS_TAC[]
THEN1 METIS_TAC[]
THEN Cases_on `~(v1 IN vs')`
THEN1 METIS_TAC[]
THEN Cases_on `~(v2 IN vs'')`
THEN1 METIS_TAC[]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN `DISJOINT vs (vs' UNION vs'')` by (SRW_TAC[][] THEN METIS_TAC[DISJOINT_SYM])
THEN METIS_TAC[IN_DISJ_DEP_IMP_DEP_DIFF, IN_UNION])

val PROJ_DOM_IDEMPOT = store_thm("PROJ_DOM_IDEMPOT",
``!PROB. (prob_proj(PROB, (prob_dom PROB)) = PROB)``,
SRW_TAC[][action_proj_idempot''])

val prob_proj_idempot = store_thm("prob_proj_idempot",
``!PROB vs vs'. vs SUBSET vs'
                ==> (prob_proj(PROB, vs) = prob_proj(prob_proj(PROB,vs'), vs))``,
SRW_TAC[][subset_proj_absorb])

val prob_proj_dom_diff_eq_prob_proj_prob_proj_dom_diff = store_thm("prob_proj_dom_diff_eq_prob_proj_prob_proj_dom_diff",
``!PROB vs vs'. prob_proj(PROB, (prob_dom PROB) DIFF (vs UNION vs')) =
       prob_proj(prob_proj(PROB, (prob_dom PROB) DIFF vs), prob_dom(prob_proj(PROB,(prob_dom PROB) DIFF vs)) DIFF vs')``,
SRW_TAC[][PROB_DOM_PROJ_DIFF]
THEN METIS_TAC[DIFF_UNION, DIFF_SUBSET, subset_proj_absorb])

val PROJ_DEP_IMP_DEP = store_thm("PROJ_DEP_IMP_DEP",
``!PROB  vs v v'. (dep(prob_proj(PROB, (prob_dom PROB) DIFF vs), v, v') ==> dep(PROB, v, v'))``,
SRW_TAC[][dep_def, prob_proj_def]
THEN Cases_on `v = v' `
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `a'`
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[pairTheory.FST, pairTheory.SND, FDOM_DRESTRICT, action_proj_def, DIFF_DEF, GSPEC_ETA]
THEN METIS_TAC[INTER_SUBSET, SUBSET_DEF, DIFF_DEF])

val PROJ_NDEP_TC_IMP_NDEP_TC_OR = store_thm("PROJ_NDEP_TC_IMP_NDEP_TC_OR",
``!PROB vs v v'. ~(λv1' v2'. dep(prob_proj (PROB, (prob_dom PROB) DIFF vs),v1',v2'))⁺ v v'
                 ==> ((¬(λv1' v2'. dep (PROB,v1',v2'))⁺ v v')
                      \/ (?v''. v'' IN vs /\ (λv1' v2'. dep (PROB,v1',v2'))⁺ v v''
                                /\ (λv1' v2'. dep (PROB,v1',v2'))⁺ v'' v'))``,
SRW_TAC[][]
THEN MP_TAC(REWRITE_RULE[Once (GSYM AND_IMP_INTRO)] NOT_VS_DEP_IMP_DEP_PROJ |> Q.SPECL [`PROB`, `vs`])
THEN SRW_TAC[][]
THEN MP_TAC(REWRITE_RULE[Once (GSYM AND_IMP_INTRO)]  DEP_REFL|> Q.SPECL [`PROB`])
THEN SRW_TAC[][]
THEN MP_TAC(REFL_TC_CONJ |> Q.SPECL[`(\v v'. dep (prob_proj (PROB, (prob_dom PROB) DIFF vs),v,v'))`,
                                          `(\v v'. dep (PROB,v,v'))`,
                                          `(\v. ~(v IN vs))`,
                                          `v`, `v'`])
THEN SRW_TAC[][])

val every_action_proj_eq_as_proj = store_thm("every_action_proj_eq_as_proj",
``!as vs. EVERY (\a. action_proj(a, vs) = a) (as_proj(as, vs))``,
Induct_on `as`
THEN SRW_TAC[][as_proj_def]
THEN METIS_TAC[action_proj_idempot]);

val empty_eff_not_in_as_proj_2 = store_thm("empty_eff_not_in_as_proj_2",
``!a as vs. (FDOM (SND(action_proj(a,vs))) = EMPTY)
				      ==> (as_proj(as, vs) = as_proj(a::as, vs))``,
SRW_TAC[][as_proj_def, action_proj_def]
);

val sublist_as_proj_eq_as = store_thm("sublist_as_proj_eq_as",
``!as' as vs. sublist as' (as_proj(as, vs))
				         ==> (as_proj(as', vs) = as')``,
Induct_on `as`
THEN Cases_on `as'`
THEN SRW_TAC[][sublist_def, as_proj_def]
THEN FULL_SIMP_TAC(srw_ss())[as_proj_def, sublist_def]
THEN1 METIS_TAC[action_proj_idempot] 
THEN1 (MP_TAC(every_action_proj_eq_as_proj
		  |> Q.SPEC `as`
		  |> Q.SPEC `vs`)
       THEN SRW_TAC[][]
       THEN MP_TAC(sublist_every |> Q.SPEC `h::t` |> Q.ISPEC `as_proj(as,vs)` |> Q.ISPEC `(\a. action_proj(a, vs) = a)`)
       THEN SRW_TAC[][])
THEN1 METIS_TAC[sublist_cons_3]
THEN1 FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT, action_proj_def, INTER_IDEMPOT, GSYM INTER_ASSOC]
THEN1 METIS_TAC[(REWRITE_RULE[action_proj_def] empty_eff_not_in_as_proj_2)]
THEN1 (MP_TAC(every_action_proj_eq_as_proj
		  |> Q.SPEC `as`
		  |> Q.SPEC `vs`)
       THEN SRW_TAC[][]
       THEN MP_TAC(sublist_every |> Q.SPEC `h::t` |> Q.ISPEC `as_proj(as,vs)` |> Q.ISPEC `(\a. action_proj(a, vs) = a)`)
       THEN SRW_TAC[][])
THEN1 (FULL_SIMP_TAC(srw_ss())[GSYM FDOM_DRESTRICT]
      THEN FIRST_X_ASSUM ( Q.SPECL_THEN [`h::t`, `vs` ] MP_TAC)
      THEN SRW_TAC[][as_proj_def, CONS_11])
THEN1 METIS_TAC[(REWRITE_RULE[action_proj_def] empty_eff_not_in_as_proj_2)]);

val DISJ_EFF_DISJ_PROJ_EFF = store_thm("DISJ_EFF_DISJ_PROJ_EFF",
``!a s vs. DISJOINT (FDOM(SND a)) s ==> DISJOINT (FDOM(SND (action_proj (a, vs)))) s``,
METIS_TAC[act_dom_proj_eff_subset_act_dom_eff, DISJOINT_SUBSET, DISJOINT_SYM]);
val state_succ_proj_eq_state_succ = store_thm("state_succ_proj_eq_state_succ",
``!a s vs. varset_action(a, vs) /\ FST(a) SUBMAP s /\ FDOM(SND(a)) SUBSET FDOM s
  ==>  (state_succ s (action_proj (a, vs)) = state_succ s a)``,
SRW_TAC[][]
THEN MP_TAC(drest_proj_succ_eq_drest_succ |> Q.SPEC `a` |> Q.SPEC `s` |> Q.SPEC `vs`)
THEN SRW_TAC[][]
THEN MP_TAC(vset_disj_eff_diff |> INST_TYPE [beta |-> ``:('a|->'b)``, gamma |-> ``:'b``] |> Q.SPEC `FDOM s` |>Q.SPEC `a` |> Q.SPEC `vs` )
THEN SRW_TAC[SatisfySimps.SATISFY_ss][]
THEN MP_TAC(disj_imp_eq_proj_exec |> Q.SPEC `a` |> Q.SPEC `FDOM(s) DIFF vs` |> Q.SPEC `s`)
THEN SRW_TAC[][]
THEN MP_TAC(DISJ_EFF_DISJ_PROJ_EFF |> INST_TYPE [gamma |-> ``:'b``] |> Q.SPEC `a` |>Q.SPEC `(FDOM s DIFF vs)` |> Q.SPEC `vs`)
THEN SRW_TAC[][]
THEN MP_TAC(disj_imp_eq_proj_exec |> Q.SPEC `(action_proj (a,vs))` |> Q.SPEC `FDOM(s) DIFF vs` |> Q.SPEC `s`)
THEN SRW_TAC[][]
THEN MP_TAC(graph_plan_lemma_5 |> Q.ISPEC `state_succ s (action_proj (a,vs))` |> Q.ISPEC `state_succ s a` |> Q.SPEC `vs`)
THEN SRW_TAC[][]
THEN MP_TAC(dom_eff_subset_imp_dom_succ_eq_proj |> Q.SPEC `a` |> Q.SPEC `s` |> Q.SPEC `vs`)
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[]
THEN MP_TAC((Q.GEN `s`(Q.GEN `a` FDOM_state_succ)) |> Q.SPEC `s` |> Q.SPEC `a`)
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[]
THEN METIS_TAC[]);

val no_effectless_proj = store_thm("no_effectless_proj",
``!vs as. no_effectless_act (as_proj (as,vs))``,
STRIP_TAC
THEN Induct_on `as`
THEN SRW_TAC[][no_effectless_act_def, as_proj_def]
THEN SRW_TAC[][action_proj_def])

val as_proj_valid_in_prob_proj = store_thm("as_proj_valid_in_prob_proj",
``!PROB vs as. as IN (valid_plans PROB)
                ==> (as_proj(as, vs)) IN (valid_plans (prob_proj(PROB, vs)))``,
Induct_on `as`
THEN SRW_TAC[][as_proj_def, empty_plan_is_valid] 
THEN METIS_TAC[valid_plan_valid_tail, valid_plan_valid_head, action_proj_in_prob_proj, valid_head_and_tail_valid_plan])

val prob_proj_comm = store_thm("prob_proj_comm",
``!PROB vs vs'. prob_proj(prob_proj(PROB, vs), vs') = prob_proj(prob_proj(PROB, vs'), vs)``,
SRW_TAC[][prob_proj_def]
THEN SRW_TAC[][EXTENSION]
THEN EQ_TAC
THEN1(SRW_TAC[][]
      THEN Q.EXISTS_TAC `action_proj(a', vs')`
      THEN SRW_TAC[][action_proj_def] 
      THEN METIS_TAC[INTER_COMM])
THEN1(SRW_TAC[][]
      THEN Q.EXISTS_TAC `action_proj(a', vs)`
      THEN SRW_TAC[][action_proj_def] 
      THEN METIS_TAC[INTER_COMM]))

val vset_proj_imp_vset = store_thm("vset_proj_imp_vset",
``!vs vs' a. varset_action (a,vs') /\ varset_action (action_proj(a, vs'),vs)
             ==> varset_action (a,vs) ``,
SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[varset_action_def,
                        SUBSET_DEF,
                        DISJOINT_DEF,
                        INTER_DEF,
                        EXTENSION,
                        SPECIFICATION,
                        DIFF_DEF,
                        action_proj_def,
                        FDOM_DRESTRICT, GSPEC_ETA]
THEN METIS_TAC[])

val vset_imp_vset_act_proj_diff = store_thm("vset_imp_vset_act_proj_diff",
``!PROB vs vs' a. varset_action (a,vs)
                  ==> varset_action (action_proj (a,(prob_dom PROB) DIFF vs'),vs)``,
SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[varset_action_def,
                        dep_var_set_def,
                        dep_def,
                        SUBSET_DEF,
                        DISJOINT_DEF,
                        INTER_DEF,
                        EXTENSION,
                        SPECIFICATION,
                        DIFF_DEF,
                        action_proj_def,
                        FDOM_DRESTRICT, GSPEC_ETA]
THEN METIS_TAC[])

val action_proj_disj_diff = store_thm("action_proj_disj_diff",
``((action_dom a) SUBSET vs1) /\ DISJOINT vs2 vs3 
  ==> (action_proj (action_proj (a, vs1 DIFF vs2),vs3) =
              action_proj (a,vs3))``,
SRW_TAC[][action_proj_def, action_dom_pair]
THEN1(SRW_TAC[][fmap_EXT]
      THEN1(SRW_TAC[][FDOM_DRESTRICT]
            THEN FULL_SIMP_TAC(srw_ss())[DISJOINT_DEF, INTER_DEF, EXTENSION, DIFF_DEF, SUBSET_DEF]
            THEN METIS_TAC[])
      THEN1(SRW_TAC[][disj_imp_diff_inter_eq_inter]
            THEN Q.UNDISCH_TAC `x ∈ FDOM (DRESTRICT (FST a) ((vs1 DIFF vs2) ∩ vs3))`
            THEN SRW_TAC[][]
            THEN REWRITE_TAC[Once INTER_COMM]
            THEN REWRITE_TAC[GSYM DRESTRICT_DRESTRICT]
            THEN FULL_SIMP_TAC(bool_ss)[FDOM_DRESTRICT, Once INTER_COMM]
            THEN `x ∈ (FDOM (FST a) ∩ vs3) ∩ (vs1 DIFF vs2)` by FULL_SIMP_TAC(srw_ss())[]
            THEN `x IN (FDOM (DRESTRICT (FST a) vs3)) INTER (vs1 DIFF vs2)` by FULL_SIMP_TAC(bool_ss)[FDOM_DRESTRICT]
            THEN METIS_TAC[DRESTRICT_DEF]))
THEN1(SRW_TAC[][fmap_EXT]
      THEN1(SRW_TAC[][FDOM_DRESTRICT]
            THEN FULL_SIMP_TAC(srw_ss())[DISJOINT_DEF, INTER_DEF, EXTENSION, DIFF_DEF, SUBSET_DEF]
            THEN METIS_TAC[])
      THEN1(SRW_TAC[][disj_imp_diff_inter_eq_inter]
            THEN Q.UNDISCH_TAC `x ∈ FDOM (DRESTRICT (SND a) ((vs1 DIFF vs2) ∩ vs3))`
            THEN SRW_TAC[][]
            THEN REWRITE_TAC[Once INTER_COMM]
            THEN REWRITE_TAC[GSYM DRESTRICT_DRESTRICT]
            THEN FULL_SIMP_TAC(bool_ss)[FDOM_DRESTRICT, Once INTER_COMM]
            THEN `x ∈ (FDOM (SND a) ∩ vs3) ∩ (vs1 DIFF vs2)` by FULL_SIMP_TAC(srw_ss())[]
            THEN `x IN (FDOM (DRESTRICT (SND a) vs3)) INTER (vs1 DIFF vs2)` by FULL_SIMP_TAC(bool_ss)[FDOM_DRESTRICT]
            THEN METIS_TAC[DRESTRICT_DEF])))

val disj_proj_proj_eq_proj = store_thm("disj_proj_proj_eq_proj",
``!PROB vs vs'. DISJOINT vs vs'
               ==> (prob_proj(prob_proj(PROB, (prob_dom PROB) DIFF vs'), vs)
                      = prob_proj(PROB, vs))``,
SRW_TAC[][prob_proj_def]
THEN SRW_TAC[][EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1(Q.EXISTS_TAC `a'`
     THEN SRW_TAC[][]
     THEN METIS_TAC[action_proj_disj_diff, exec_as_proj_valid_2, DISJOINT_SYM])
THEN1(Q.EXISTS_TAC `action_proj (a,prob_dom PROB DIFF vs')`
     THEN SRW_TAC[][]
     THEN1 METIS_TAC[action_proj_disj_diff, exec_as_proj_valid_2, DISJOINT_SYM]
     THEN METIS_TAC[]))

val n_replace_proj_le_n_as_2 = store_thm("n_replace_proj_le_n_as_2",
``!a vs vs'. vs SUBSET vs' /\ varset_action(a, vs') ==> (varset_action(action_proj(a,vs'), vs) <=> varset_action(a, vs))``,
SRW_TAC[][SUBSET_DEF, varset_action_def, FDOM_DRESTRICT, INTER_DEF, action_proj_def]
THEN EQ_TAC
THEN METIS_TAC[]
THEN SRW_TAC[][])

val empty_problem_proj_bound = store_thm("empty_problem_proj_bound",
``!PROB. (problem_plan_bound (prob_proj(PROB,EMPTY)) = 0)``,
SRW_TAC[][]
THEN MP_TAC(dom_prob_proj |> INST_TYPE [gamma |-> ``:'b``]|> Q.GEN `vs` |> Q.SPEC `EMPTY`)
THEN SRW_TAC[][]
THEN METIS_TAC[empty_problem_bound, finite_imp_finite_prob_proj]);

val problem_plan_bound_works_proj = store_thm("problem_plan_bound_works_proj",
``!(PROB:'a problem) s as vs. FINITE PROB /\ s IN (valid_states PROB)
                              /\ as IN (valid_plans PROB) /\ sat_precond_as(s, as)
                              ==>
                                ?as'. (exec_plan(DRESTRICT s vs, as') = exec_plan(DRESTRICT s vs, as_proj(as, vs)))
                                       /\ LENGTH(as') <= problem_plan_bound(prob_proj(PROB, vs)) 
                                       /\ (sublist as' (as_proj(as, vs))) /\ sat_precond_as(s, as')
                                       /\ no_effectless_act as'``,
SRW_TAC[][]
THEN MP_TAC((REWRITE_RULE[prob_proj_def] sat_precond_exec_as_proj_eq_proj_exec |> INST_TYPE [beta |-> ``:bool``])
		|> Q.SPEC `as`
		|> Q.SPEC `vs`
		|> Q.SPEC `s`)
THEN SRW_TAC[][]
THEN MP_TAC(two_pow_n_is_a_bound_2  |> INST_TYPE [beta |-> ``:bool``,gamma |-> ``:bool``,delta |-> ``:bool``] |> Q.SPECL[`PROB`, `vs`])
THEN SRW_TAC[][]
THEN MP_TAC(valid_as_valid_as_proj |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``]|> Q.SPECL[`PROB`, `vs`])
THEN SRW_TAC[][]
THEN MP_TAC(problem_plan_bound_works |> Q.SPECL[`prob_proj(PROB, vs)`, `as_proj(as, vs)`, `DRESTRICT s vs`])
THEN SRW_TAC[][finite_imp_finite_prob_proj]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN ASSUME_TAC(rem_condless_valid_1 |> INST_TYPE [beta |-> ``:bool``]
				     |> Q.SPEC `as'`
				     |> Q.SPEC `DRESTRICT s vs`)
THEN ASSUME_TAC((rem_condless_valid_8 |> INST_TYPE [beta |-> ``:bool``]
				             |> Q.SPEC `DRESTRICT s vs`
					            |> Q.SPEC `as'`))
THEN ASSUME_TAC(rem_condless_valid_3 |> INST_TYPE [beta |-> ``:bool``] |> Q.SPEC `as'` |> Q.SPEC `DRESTRICT s vs`)
THEN ASSUME_TAC(rem_condless_valid_2 |> INST_TYPE [beta |-> ``:bool``] |> Q.SPEC `as'` |> Q.SPEC `DRESTRICT s vs`)
THEN ASSUME_TAC(rem_effectless_works_1 |> INST_TYPE [beta |-> ``:bool``] |> Q.SPEC `DRESTRICT s vs` |> Q.SPEC `(rem_condless_act (DRESTRICT s vs,[],as'))`)
THEN ASSUME_TAC(rem_effectless_works_9  |> Q.ISPEC `(rem_condless_act (DRESTRICT s vs,[], as':'a action list))`)
THEN ASSUME_TAC(rem_effectless_works_3 |> Q.ISPEC `(rem_condless_act (DRESTRICT s vs,[], as':'a action list))`)
THEN MP_TAC(rem_effectless_works_2 |> INST_TYPE [beta |-> ``:bool``] |> Q.SPEC `(rem_condless_act (DRESTRICT s vs,[],as'))` |> Q.SPEC `DRESTRICT s vs`)
THEN SRW_TAC[][]
THEN ASSUME_TAC(rem_effectless_works_6 |> Q.ISPEC `(rem_condless_act (DRESTRICT s vs,[],as':'a action list))`)
THEN Q.EXISTS_TAC `(rem_effectless_act(rem_condless_act(DRESTRICT (s:'a state) vs, [], as')))`
THEN SRW_TAC[][]
THEN1 DECIDE_TAC
THEN1 METIS_TAC[sublist_trans]
THEN1 PROVE_TAC[sublist_trans, sublist_as_proj_eq_as, sat_precond_as_proj, DRESTRICT_IDEMPOT]);

val action_proj_inter = store_thm("action_proj_inter",
``action_proj(action_proj(a,vs1),vs2) = action_proj(a, vs1 INTER vs2)``,
SRW_TAC[][action_proj_def])

val prob_proj_inter = store_thm("prob_proj_inter",
``prob_proj(prob_proj(PROB,vs1),vs2) = prob_proj(PROB,vs1 INTER vs2)``,
SRW_TAC[][prob_proj_def, EXTENSION]
THEN METIS_TAC[action_proj_inter])

(*Snapshotting *)

val agree_def = Define`agree s1 s2 = !v. (v IN FDOM s1 /\ v IN FDOM s2) ==> (s1 ' v = s2 ' v)`

val agree_state_succ_idempot = store_thm("agree_state_succ_idempot",
``a IN PROB /\ s IN valid_states PROB /\ agree (SND a) s
  ==> (state_succ s a = s)``,
SRW_TAC[][state_succ_def]
THEN `!f g. (FDOM f = FDOM g) /\ (!x. x ∈ FDOM f ⇒ (f ' x = g ' x)) ==> (f = g)` by METIS_TAC[fmap_EQ_THM]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN1(FULL_SIMP_TAC(srw_ss())[valid_states_def]
      THEN METIS_TAC[SUBSET_UNION_ABSORPTION, FDOM_eff_subset_prob_dom_pair])
THEN1(FULL_SIMP_TAC(srw_ss())[valid_states_def, agree_def]
      THEN SRW_TAC[][FUNION_DEF]
      THEN MP_TAC (FDOM_eff_subset_prob_dom_pair |> INST_TYPE [gamma |-> ``:'b``])
      THEN SRW_TAC[][SUBSET_DEF]
      THEN METIS_TAC[])
THEN1(FULL_SIMP_TAC(srw_ss())[valid_states_def, agree_def]
      THEN SRW_TAC[][FUNION_DEF]))

val agree_restrict_state_succ_idempot = store_thm("agree_restrict_state_succ_idempot",
``a IN PROB /\ s IN valid_states PROB /\ agree (DRESTRICT (SND a) vs) (DRESTRICT s vs)
  ==> (DRESTRICT (state_succ s a) vs = DRESTRICT s vs)``,
SRW_TAC[][state_succ_def]
THEN `!f g. (FDOM f = FDOM g) /\ (!x. x ∈ FDOM f ⇒ (f ' x = g ' x)) ==> (f = g)` by METIS_TAC[fmap_EQ_THM]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN MP_TAC (FDOM_eff_subset_prob_dom_pair |> INST_TYPE [gamma |-> ``:'b``])
THEN SRW_TAC[][SUBSET_DEF]
THEN1(FULL_SIMP_TAC(srw_ss())[valid_states_def, EXTENSION]
      THEN SRW_TAC[][FDOM_DRESTRICT, INTER_DEF,EXTENSION]
      THEN METIS_TAC[])
THEN1(FULL_SIMP_TAC(srw_ss())[valid_states_def, agree_def, DRESTRICT_DEF]
      THEN SRW_TAC[][FUNION_DEF]
      THEN METIS_TAC[]))

val agree_exec_idempot = store_thm("agree_exec_idempot",
``as IN valid_plans PROB /\ s IN valid_states PROB /\
 (!a. MEM a as ==> agree (SND a) s)
  ==> (exec_plan(s, as) = s)``,
Induct_on `as`
THEN SRW_TAC[][exec_plan_def]
THEN METIS_TAC[agree_state_succ_idempot, valid_plan_valid_head, valid_plan_valid_tail])

val agree_restrict_exec_idempot = store_thm("agree_restrict_exec_idempot",
``!s s'. as IN valid_plans PROB /\ s' IN valid_states PROB /\ s IN valid_states PROB /\
 (!a. MEM a as ==> agree (DRESTRICT (SND a) vs) (DRESTRICT s vs)) /\
  (DRESTRICT s' vs = DRESTRICT s vs)
  ==> (DRESTRICT (exec_plan(s', as)) vs = DRESTRICT s vs)``,
Induct_on `as`
THEN SRW_TAC[][exec_plan_def]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN METIS_TAC[agree_restrict_state_succ_idempot, valid_plan_valid_head, valid_plan_valid_tail, valid_action_valid_succ])

val agree_restrict_exec_idempot_pair = store_thm("agree_restrict_exec_idempot_pair",
``!s s'. as IN valid_plans PROB /\ s' IN valid_states PROB /\ s IN valid_states PROB /\
 (!p e. MEM (p,e) as ==> agree (DRESTRICT e vs) (DRESTRICT s vs)) /\
  (DRESTRICT s' vs = DRESTRICT s vs)
  ==> (DRESTRICT (exec_plan(s', as)) vs = DRESTRICT s vs)``,
METIS_TAC[agree_restrict_exec_idempot, pairTheory.PAIR, pairTheory.FST, pairTheory.SND])

val agree_comm = store_thm("agree_comm",
``agree x x' = agree x' x``,
SRW_TAC[][agree_def]
THEN METIS_TAC[])

val restricted_agree_imp_agree = store_thm("restricted_agree_imp_agree",
``FDOM s2 SUBSET vs /\ agree (DRESTRICT s1 vs) s2 
  ==> agree s1 s2``,
SRW_TAC[][agree_def, SUBSET_DEF, FDOM_DRESTRICT, INTER_DEF]
THEN MP_TAC DRESTRICT_DEF
THEN SRW_TAC[][INTER_DEF]
THEN METIS_TAC[])

val agree_imp_submap = store_thm("agree_imp_submap",
``f1 SUBMAP f2 ==> agree f1 f2``,
SRW_TAC[][agree_def, SUBMAP_DEF])

val agree_FUNION = store_thm("agree_FUNION",
``agree fm fm1 /\ agree fm fm2
  ==> agree fm (FUNION fm1 fm2)``,
SRW_TAC[][agree_def, FUNION_DEF]
THEN METIS_TAC[])

val agree_fm_list_union = store_thm("agree_fm_list_union",
``!fm. (!fm'. MEM fm' fmList ==> agree fm fm') ==>
  agree fm (FOLDR FUNION FEMPTY fmList)``,
Induct_on `fmList`
THEN1 SRW_TAC[][agree_def]
THEN SRW_TAC[][]
THEN METIS_TAC[agree_FUNION])

val DRESTRICT_EQ_AGREE = store_thm("DRESTRICT_EQ_AGREE",
``FDOM s2 SUBSET vs2 /\ FDOM s1 SUBSET vs1 ==>
  ((DRESTRICT s1 vs2 = DRESTRICT s2 vs1) ==> agree s1 s2)``,
SRW_TAC[][agree_def, DRESTRICT_DEF, fmap_EXT, INTER_DEF, EXTENSION, SUBSET_DEF]
THEN METIS_TAC[])

val SUBMAPS_AGREE = store_thm("SUBMAPS_AGREE",
``s1 SUBMAP s /\ s2 SUBMAP s ==> agree s1 s2``,
SRW_TAC[][agree_def, SUBMAP_DEF]
THEN METIS_TAC[])

val snapshot_def = Define`snapshot PROB s = {a | a IN PROB /\ agree (FST a) s /\ agree (SND a) s}`

val snapshot_pair = store_thm("snapshot_pair",
``snapshot PROB s = {(p,e) | (p,e) IN PROB /\ agree p s /\ agree e s}``,
SRW_TAC[][EXTENSION, snapshot_def]
THEN METIS_TAC[pairTheory.PAIR, pairTheory.FST, pairTheory.SND]);

val action_agree_valid_in_snapshot = store_thm("action_agree_valid_in_snapshot",
``a IN PROB /\ agree (FST a) s /\ agree (SND a) s
  ==> a IN snapshot PROB s``,
SRW_TAC[][snapshot_def])

val as_mem_agree_valid_in_snapshot = store_thm("as_mem_agree_valid_in_snapshot",
``(!a. MEM a as ==> agree (FST a) s /\ agree (SND a) s) /\ as IN valid_plans PROB
  ==> as IN valid_plans (snapshot PROB s)``,
Induct_on `as`
THEN SRW_TAC[][snapshot_def,valid_plans_def]
THEN FULL_SIMP_TAC(srw_ss())[snapshot_def,valid_plans_def])

val SUBMAP_FUNION_DRESTRICT' = store_thm("SUBMAP_FUNION_DRESTRICT'",
``agree fma fmb /\ (vsa) SUBSET FDOM fma /\ (vsb) SUBSET FDOM fmb /\ 
  (DRESTRICT fm vsa = DRESTRICT fma (vsa INTER vs)) /\ (DRESTRICT fm vsb = DRESTRICT fmb (vsb INTER vs)) ==>
  (DRESTRICT fm (vsa UNION vsb) = DRESTRICT (FUNION fma fmb) ((vsa UNION vsb) INTER vs))``,
SRW_TAC[][agree_def, DRESTRICT_DEF, fmap_EXT, INTER_DEF, UNION_DEF, EXTENSION, SUBMAP_DEF, SUBSET_DEF, FUNION_DEF]
THEN METIS_TAC[])

val UNION_FUNION_DRESTRICT_SUBMAP = store_thm("UNION_FUNION_DRESTRICT_SUBMAP",
``(vs1) SUBSET FDOM fma /\ (vs2) SUBSET FDOM fmb /\ agree fma fmb /\
  DRESTRICT fma (vs1) SUBMAP s /\ DRESTRICT fmb (vs2) SUBMAP s ==>
  DRESTRICT (FUNION fma fmb) (vs1 UNION vs2) SUBMAP s``,
SRW_TAC[][DRESTRICT_DEF, fmap_EXT, INTER_DEF, UNION_DEF, EXTENSION, SUBMAP_DEF, SUBSET_DEF, FUNION_DEF, agree_def]
THEN METIS_TAC[])

val agree_DRESTRICT = store_thm("agree_DRESTRICT",
``agree s1 s2 ==> agree (DRESTRICT s1 vs) (DRESTRICT s2 vs)``,
SRW_TAC[][agree_def, DRESTRICT_DEF, FDOM_DRESTRICT])

val agree_DRESTRICT_2 = store_thm("agree_DRESTRICT_2",
``FDOM s1 SUBSET vs1 /\ FDOM s2 SUBSET vs2 /\
  agree s1 s2 ==> agree (DRESTRICT s1 vs2) (DRESTRICT s2 vs1)``,
SRW_TAC[][agree_def, DRESTRICT_DEF, FDOM_DRESTRICT])

val FINITE_snapshot = store_thm("FINITE_snapshot",
``FINITE PROB ==> FINITE (snapshot PROB s)``,
`?PROB'. (snapshot PROB s) = PROB INTER PROB'` by
  (SRW_TAC[][snapshot_def] THEN Q.EXISTS_TAC `{a | agree (FST a) s /\ agree (SND a) s}`
  THEN SRW_TAC[][EXTENSION])
THEN METIS_TAC[FINITE_INTER])

val snapshot_subset = store_thm("snapshot_subset",
``snapshot PROB s SUBSET PROB``,
SRW_TAC[][SUBSET_DEF, snapshot_def])

val dom_proj_snapshot = store_thm("dom_proj_snapshot",
``prob_dom (prob_proj(PROB, prob_dom (snapshot PROB s))) = prob_dom (snapshot PROB s)``,
METIS_TAC[snapshot_subset, two_children_parent_mems_le_finite, prob_subset_dom_subset])

val valid_states_snapshot = store_thm("valid_states_snapshot",
``valid_states (prob_proj(PROB, prob_dom (snapshot PROB s))) = valid_states (snapshot PROB s)``,
METIS_TAC[dom_proj_snapshot, valid_states_def])


(*``valid_states PROB = valid_states (snapshote PROB s)``
SRW_TAC
``problem_plan_bound {a | a IN PROB /\ agree (FST a) s /\ agree (SND a) s} < problem_plan_bound (snapshot PROB s) + 1``
HO_MATCH_MP_TAC problem_plan_bound_UBound
THEN SRW_TAC[][]*)

val valid_proj_neq_succ_restricted_neq_succ = store_thm("valid_proj_neq_succ_restricted_neq_succ",
``x' IN prob_proj (PROB,vs) /\ state_succ s x' <> s
  ==> DRESTRICT (state_succ s x') vs ≠ DRESTRICT s vs``,
SRW_TAC[][state_succ_def]
THEN METIS_TAC[FDOM_eff_subset_prob_dom_pair, dom_prob_proj, SUBSET_TRANS,
               limited_dom_neq_restricted_neq])

val proj_successors = store_thm("proj_successors",
``IMAGE (\s. DRESTRICT s vs) (state_successors (prob_proj (PROB,vs)) s)
    SUBSET state_successors (prob_proj (PROB,vs)) (DRESTRICT s vs)``,
SRW_TAC[][state_successors_def, SUBSET_DEF]
THEN1(Q.EXISTS_TAC `x'`
      THEN SRW_TAC[][]
      THEN SRW_TAC[][state_succ_def, DRESTRICTED_FUNION_2]
      THEN1(`FDOM (SND x') SUBSET vs` by 
              METIS_TAC[FDOM_eff_subset_prob_dom_pair, dom_prob_proj, SUBSET_TRANS]
            THEN METIS_TAC[exec_drest_5])
      THEN1(`FDOM (FST x') SUBSET vs` by 
              METIS_TAC[FDOM_pre_subset_prob_dom_pair, dom_prob_proj, SUBSET_TRANS]
            THEN `DRESTRICT (FST x') vs =
                   FST x'` by METIS_TAC[exec_drest_5]
            THEN METIS_TAC[submap_drest_submap])
      THEN1(METIS_TAC[sublist_as_proj_eq_as_1]))
THEN1 METIS_TAC[valid_proj_neq_succ_restricted_neq_succ])

val state_in_successor_proj_in_state_in_successor = store_thm("state_in_successor_proj_in_state_in_successor",
``s' ∈ state_successors (prob_proj (PROB,vs)) s ==>
  (DRESTRICT s' vs) IN state_successors (prob_proj (PROB,vs)) (DRESTRICT s vs)``,
MP_TAC proj_successors
THEN SRW_TAC[][IMAGE_DEF, SUBSET_DEF]
THEN METIS_TAC[])

val proj_FDOM_eff_subset_FDOM_valid_states = store_thm("proj_FDOM_eff_subset_FDOM_valid_states",
``!p e s. (p,e) IN (prob_proj(PROB, vs)) /\ s IN valid_states PROB
     ==> (FDOM e) SUBSET FDOM s``,
SRW_TAC[][valid_states_def]
THEN METIS_TAC[FDOM_eff_subset_prob_dom, graph_plan_neq_mems_state_set_neq_len,
               INTER_SUBSET, SUBSET_TRANS])

val valid_proj_action_valid_succ = store_thm("valid_proj_action_valid_succ",
``h IN (prob_proj(PROB,vs)) /\ s IN valid_states PROB
  ==> (state_succ s h) IN valid_states PROB``,
SRW_TAC[][valid_states_def]
THEN `FDOM (state_succ s h) = FDOM s` by
     (MATCH_MP_TAC(FDOM_state_succ)
      THEN MATCH_MP_TAC(proj_FDOM_eff_subset_FDOM_valid_states)
      THEN Q.EXISTS_TAC `FST h`
      THEN SRW_TAC[][valid_states_def])
THEN SRW_TAC[][])

val proj_successors_of_valid_are_valid = store_thm("proj_successors_of_valid_are_valid",
``s IN valid_states PROB ==> 
  state_successors (prob_proj (PROB,vs)) s SUBSET (valid_states PROB)``,
SRW_TAC[][state_successors_def, IMAGE_DEF, SUBSET_DEF]
THEN METIS_TAC[valid_proj_action_valid_succ])

(*State space projection*)

val ss_proj_def = Define`ss_proj ss vs = IMAGE (\s. DRESTRICT s vs) ss`;

val invariantStateSpace_thm_9 = store_thm("invariantStateSpace_thm_9",
``!ss vs1 vs2. ss_proj ss (vs1 INTER vs2) = ss_proj (ss_proj ss vs2) vs1``,
SRW_TAC[][ss_proj_def, EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN1(Q.EXISTS_TAC `DRESTRICT s vs2`
      THEN SRW_TAC[][Once INTER_COMM]
      THEN METIS_TAC[])
THEN1(Q.EXISTS_TAC `s'`
      THEN SRW_TAC[][Once INTER_COMM]))

val FINITE_ss_proj = store_thm("FINITE_ss_proj",
``!ss vs. FINITE ss ==> FINITE (ss_proj ss vs)``,
SRW_TAC[][ss_proj_def])

val nempty_stateSpace_nempty_ss_proj = store_thm("nempty_stateSpace_nempty_ss_proj",
``ss <> EMPTY ==> ss_proj ss vs <> EMPTY``,
SRW_TAC[][ss_proj_def])

val invariantStateSpace_thm_5 = store_thm("invariantStateSpace_thm_5",
``!ss vs dom. stateSpace ss dom ==> stateSpace (ss_proj ss vs) (dom INTER vs)``,
SRW_TAC[][stateSpace_def, ss_proj_def]
THEN METIS_TAC[FDOM_DRESTRICT])

val dom_subset_ssproj_eq_ss = store_thm("dom_subset_ssproj_eq_ss",
``!ss dom vs. stateSpace ss dom /\ dom SUBSET vs ==> (ss_proj ss vs = ss)``,
SRW_TAC[][ss_proj_def, stateSpace_def, EXTENSION]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN METIS_TAC[exec_drest_5, EXTENSION])

val neq_vs_neq_ss_proj = store_thm("neq_vs_neq_ss_proj",
``!vs. ss <> EMPTY /\ stateSpace ss vs /\ vs1 SUBSET vs /\ vs2 SUBSET vs /\ vs1 <> vs2  
  ==> ss_proj ss vs1 <> ss_proj ss vs2``,
SRW_TAC[][ss_proj_def]
THEN MATCH_MP_TAC (neq_funs_neq_images)
THEN SRW_TAC[][]
THEN1(FULL_SIMP_TAC(srw_ss())[stateSpace_def]
      THEN MATCH_MP_TAC(FDOM_INTER_NEQ_DRESTRICT_NEQ)
      THEN METIS_TAC[SUBSET_INTER_ABSORPTION, INTER_COMM])
THEN FULL_SIMP_TAC(srw_ss())[EMPTY_DEF, EXTENSION]
THEN METIS_TAC[])

val subset_dom_stateSpace_ss_proj = store_thm("subset_dom_stateSpace_ss_proj",
``!vs1 vs2. vs1 SUBSET vs2 /\ stateSpace ss vs2 ==> stateSpace (ss_proj ss vs1) vs1``,
METIS_TAC[INTER_SUBSET_EQN, invariantStateSpace_thm_5])

val card_proj_leq = store_thm("card_proj_leq",
``FINITE PROB ==> CARD (prob_proj(PROB,vs)) <= CARD PROB``,
SRW_TAC[][prob_proj_def]
THEN METIS_TAC[CARD_IMAGE])

val _ = export_theory();
