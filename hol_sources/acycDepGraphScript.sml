open HolKernel Parse boolLib bossLib;
open arithmeticTheory;
open pred_setTheory;
open rich_listTheory;
open listTheory;
open relationTheory;
open finite_mapTheory;
open utilsTheory;
open factoredSystemTheory 
open sublistTheory;
open systemAbstractionTheory;
open topologicalPropsTheory;
open parentChildStructureTheory;
open set_utilsTheory;
open fmap_utilsTheory;
open list_utilsTheory;
open actionSeqProcessTheory
open dependencyTheory
open arith_utilsTheory
open functionsLib
open acyclicityTheory

val _ = new_theory "acycDepGraph";

val n_def = Define `n(vs, as) = LENGTH (FILTER (\a. varset_action(a, vs)) as)`

(*All dependencies going outside of vs only can go to vs*)
val gen_parent_child_rel_def = 
  Define`gen_parent_child_rel(PROB, p, c)
      <=>  DISJOINT p c 
                   /\ ~dep_var_set(PROB, c, p)
                   /\ ~dep_var_set(PROB, p, (prob_dom PROB) DIFF (p UNION c))
                   /\ (!v1 v2. v1 IN c /\ v2 IN (prob_dom PROB) DIFF (p UNION c) 
                              ==> (~dep(PROB, v1,v2) \/ ~dep(PROB, v2,v1)))`

val gen_par_chil_imp_proj_gen_par_chil = store_thm("gen_par_chil_imp_proj_gen_par_chil",
``!PROB vs vs'. gen_parent_child_rel(PROB, vs, vs') 
                ==> child_parent_rel(prob_proj(PROB, (prob_dom PROB) DIFF vs'), vs)``,
SRW_TAC[][gen_parent_child_rel_def, child_parent_rel_def]
THEN FULL_SIMP_TAC(bool_ss)[PROB_DOM_PROJ_DIFF]
THEN METIS_TAC[NDEP_PROJ_NDEP, DIFF_UNION, UNION_COMM])

val varset_act_iff_not_varset_act_diff = store_thm("varset_act_iff_not_varset_act_diff",
``∀PROB a vs vs'.
     a ∈ PROB ∧ gen_parent_child_rel (PROB,vs,vs') ∧ FDOM (SND a) ≠ ∅ ⇒
     (varset_action (a,vs') ⇔ ¬varset_action (a, (prob_dom PROB) DIFF vs'))``,
SRW_TAC[][]
THEN ASSUME_TAC(exec_as_proj_valid_2)
THEN FULL_SIMP_TAC(srw_ss())[varset_action_def,
                        gen_parent_child_rel_def,
                        dep_var_set_def,
                        dep_def,
                        SUBSET_DEF,
                        DISJOINT_DEF,
                        INTER_DEF,
                        EXTENSION,
                        SPECIFICATION,
                        gen_parent_child_rel_def,
                        DIFF_DEF,
                        action_dom_pair]
THEN METIS_TAC[])

val every_nvset_imp_every_vset_diff = store_thm("every_nvset_imp_every_vset_diff",
``!PROB vs vs' s as. gen_parent_child_rel (PROB,vs, vs') ∧ as IN valid_plans PROB /\
               no_effectless_act(as) /\ EVERY (\a. ~varset_action (a, vs')) as
               ==> EVERY (\a. varset_action (a, (prob_dom PROB) DIFF vs')) as ``,
NTAC 4 STRIP_TAC
THEN Induct_on `as`
THEN SRW_TAC[][no_effectless_act_def]
THEN1 METIS_TAC[varset_act_iff_not_varset_act_diff, valid_plan_valid_head]
THEN METIS_TAC[varset_act_iff_not_varset_act_diff, valid_plan_valid_tail])

val varset_act_dichotomy = store_thm("varset_act_dichotomy",
``!PROB a vs vs' vs''. a IN PROB /\ gen_parent_child_rel (PROB,vs,vs')
                       /\ varset_action (a, (prob_dom PROB) DIFF vs)
                       ==> (varset_action (a, vs')
                            \/ varset_action (a, (prob_dom PROB) DIFF (vs' UNION vs)))``,
SRW_TAC[][varset_action_def, DIFF_DEF, UNION_DEF, SUBSET_DEF,
          SPECIFICATION, DISJOINT_DEF,
          INTER_DEF, EXTENSION, gen_parent_child_rel_def,
          dep_var_set_def, dep_def]
THEN Cases_on `(∀x. FDOM (SND a) x ⇒ vs' x)`
THEN1 METIS_TAC[]
THEN SRW_TAC[][]
THEN METIS_TAC[])

val filter_vset_diff_eq_union = store_thm("filter_vset_diff_eq_union",
``!PROB vs vs' as. gen_parent_child_rel (PROB,vs,vs') /\ as IN valid_plans PROB
                   /\ EVERY (\a. ~varset_action (a,vs')) as /\ no_effectless_act(as)
                   ==> (FILTER (\a. varset_action (a, (prob_dom PROB) DIFF (vs' UNION vs))) as
                         = FILTER (\a. varset_action (a, (prob_dom PROB) DIFF vs )) as)``,
NTAC 3 STRIP_TAC
THEN Induct_on `as`
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(bool_ss)[no_effectless_act_def]
THEN1 METIS_TAC[valid_plan_valid_tail]
THEN1 METIS_TAC[varset_act_diff_un_imp_varset_diff]
THEN1 METIS_TAC[varset_act_dichotomy, valid_plan_valid_head]
THEN1 METIS_TAC[valid_plan_valid_tail])

val every_nvarset_act_imp_empty_filter = store_thm("every_nvarset_act_imp_empty_filter",
``!PROB vs vs' as. gen_parent_child_rel (PROB,vs,vs') /\ EVERY (λa. ¬varset_action (a,vs')) as
                   /\ as IN (valid_plans PROB) /\ no_effectless_act(as)
                   ==> (FILTER (λa. ¬varset_action (a, (prob_dom PROB) DIFF vs')) as = [])``,
NTAC 3 STRIP_TAC
THEN Induct_on `as`
THEN1 SRW_TAC[][]
THEN REPEAT STRIP_TAC
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN FULL_SIMP_TAC(srw_ss())[no_effectless_act_def]
THEN1 METIS_TAC[valid_plan_valid_head, varset_act_iff_not_varset_act_diff]
THEN METIS_TAC[valid_plan_valid_tail])

val sublist_proj_imp_replace_proj_sat_precond_diff = store_thm("sublist_proj_imp_replace_proj_sat_precond_diff",
``!PROB vs vs' s1 s2 as' as. (s1 IN valid_states PROB) /\ (s2 IN valid_states PROB)
                 /\ as IN valid_plans PROB 
	      	 /\ EVERY (\a. ~varset_action (a,vs')) as /\ gen_parent_child_rel(PROB, vs, vs')
                 /\ sublist as' (as_proj(as, (prob_dom PROB) DIFF vs')) /\ sat_precond_as(s1, as')
                 /\ sat_precond_as(s2, as) /\ no_effectless_act(as) /\ (DRESTRICT s1 vs' = DRESTRICT s2 vs')
                  ==> sat_precond_as(s1, replace_projected ([],as',as, (prob_dom PROB) DIFF vs'))``,
NTAC 3 STRIP_TAC
THEN Induct_on `as`
THEN1(SRW_TAC[][replace_projected_def, sat_precond_as_def]
      THEN Cases_on `as'`
      THEN SRW_TAC[][replace_projected_def, sat_precond_as_def])
THEN1(SRW_TAC[][replace_projected_def, sat_precond_as_def]
      THEN Cases_on `as'`
      THEN SRW_TAC[][replace_projected_def, sat_precond_as_def]
      THEN1(FULL_SIMP_TAC(srw_ss())[no_effectless_act_def]
            THEN METIS_TAC[varset_act_iff_not_varset_act_diff, valid_plan_valid_head])
      THEN1(FULL_SIMP_TAC(srw_ss())[no_effectless_act_def]
            THEN METIS_TAC[varset_act_iff_not_varset_act_diff, valid_plan_valid_head])
      THEN1(MP_TAC(every_nvarset_act_imp_empty_filter |> INST_TYPE [gamma |-> ``:'b``] |> Q.SPECL[`PROB`, `vs`, `vs'`, `as`])
            THEN FULL_SIMP_TAC(srw_ss())[no_effectless_act_def]
            THEN SRW_TAC[][replace_projected_def, sat_precond_as_def]
            THEN METIS_TAC[valid_plan_valid_tail])
      THEN1(REWRITE_TAC[SIMP_RULE(srw_ss())[] (replace_proj_append |> Q.SPECL[`[h]`, `[]`])]
            THEN SRW_TAC[][sat_precond_as_def]
            THEN `FST h ⊑ s1` by
                  (FULL_SIMP_TAC(srw_ss())[sat_precond_as_def, action_proj_def]
                  THEN `DRESTRICT (FST h) ((prob_dom PROB) DIFF vs') ⊑ 
                          DRESTRICT s1 ((prob_dom PROB) DIFF vs')` by METIS_TAC[drest_smap_drest_smap_drest]
                  THEN MP_TAC((EQ_SUBMAP_IMP_DRESTRICT_SUBMAP)|> Q.SPECL[`vs'`, `FST (h: (α |-> β) # (α |-> β))`, `s2`, `s1`])
                  THEN SRW_TAC[][]
                  THEN FULL_SIMP_TAC(srw_ss())[valid_states_def]
                  THEN METIS_TAC[drestrict_submap_and_comp_imp_submap, drest_smap_drest_smap_drest,
                                 valid_plan_valid_head, exec_as_proj_valid_2, FDOM_pre_subset_prob_dom_pair])
            THEN FIRST_X_ASSUM MATCH_MP_TAC
            THEN Q.EXISTS_TAC `state_succ s2 h`
            THEN SRW_TAC[][]
            THEN1 METIS_TAC[valid_plan_valid_head, valid_action_valid_succ]
            THEN1 METIS_TAC[valid_plan_valid_head, valid_action_valid_succ]
            THEN1 METIS_TAC[valid_plan_valid_tail]
            THEN1(FULL_SIMP_TAC(srw_ss())[no_effectless_act_def]
                  THEN FULL_SIMP_TAC(bool_ss)[varset_action_def, FDOM_DRESTRICT, as_proj_def]
                  THEN MP_TAC(not_empty_eff_in_as_proj |> INST_TYPE [gamma |-> ``:'b``] |> Q.SPEC `as` |> Q.SPEC `h` |> Q.SPEC `(prob_dom (PROB:((α |-> β) # (α |-> β)) set)) DIFF vs'`)
                  THEN SRW_TAC[][FDOM_DRESTRICT]
                  THEN MP_TAC(SUBSET_ABS |> Q.SPECL[`FDOM (SND (h:(α |-> β)#(α |-> β)))`, `(prob_dom (PROB: ((α |-> β) # (α |-> β)) set)) DIFF vs'`])
                  THEN SRW_TAC[][]
                  THEN METIS_TAC[sublist_cons_2])
            THEN1(MP_TAC(state_succ_proj_eq_state_succ |> Q.SPECL[`h`, `s1`, `(prob_dom (PROB: ((α |-> β) # (α |-> β)) set)) DIFF vs'`])
                  THEN SRW_TAC[][]
                  THEN FULL_SIMP_TAC(srw_ss())[sat_precond_as_def,valid_states_def]
                  THEN METIS_TAC[valid_plan_valid_head, FDOM_eff_subset_prob_dom_pair])
            THEN1(FULL_SIMP_TAC(srw_ss())[no_effectless_act_def])
            THEN1(METIS_TAC[vset_diff_disj_eff_vs, disj_imp_eq_proj_exec]))
      THEN1(FIRST_X_ASSUM MATCH_MP_TAC
            THEN SRW_TAC[][]
            THEN Q.EXISTS_TAC `state_succ s2 h`
            THEN SRW_TAC[][]
            THEN1(METIS_TAC[valid_action_valid_succ, valid_plan_valid_head])
            THEN1(METIS_TAC[valid_plan_valid_tail])
            THEN1(FULL_SIMP_TAC(srw_ss())[no_effectless_act_def]
                  THEN FULL_SIMP_TAC(bool_ss)[varset_action_def, FDOM_DRESTRICT, as_proj_def]
                  THEN MP_TAC(not_empty_eff_in_as_proj |> Q.SPEC `as` |> Q.SPEC `h` |> Q.SPEC `(prob_dom PROB) DIFF vs'`)
                  THEN SRW_TAC[][FDOM_DRESTRICT]
                  THEN MP_TAC(SUBSET_ABS |> Q.SPECL[`FDOM (SND (h:(α |-> β)#(α |-> β)))`, `(prob_dom (PROB: ((α |-> β) # (α |-> β)) set)) DIFF vs'`])
                  THEN SRW_TAC[][]
                  THEN FULL_SIMP_TAC(srw_ss())[])
            THEN1(FULL_SIMP_TAC(srw_ss())[no_effectless_act_def])
            THEN1(METIS_TAC[vset_diff_disj_eff_vs, disj_imp_eq_proj_exec]))
     THEN1(FULL_SIMP_TAC(srw_ss())[no_effectless_act_def]
           THEN METIS_TAC[varset_act_iff_not_varset_act_diff, valid_plan_valid_tail, valid_plan_valid_head, valid_action_valid_succ])))

val nvarset_act_disj_eff = store_thm("nvarset_act_disj_eff",
`` ∀PROB a vs vs'.
     gen_parent_child_rel (PROB,vs, vs') ∧ a ∈ PROB ∧
     ¬varset_action (a, (prob_dom PROB) DIFF vs') ⇒
     DISJOINT (FDOM (SND a)) ((prob_dom PROB) DIFF vs')``,
SRW_TAC[][varset_action_def,dep_var_set_def, dep_def, DISJOINT_DEF, INTER_DEF, EXTENSION, gen_parent_child_rel_def]
THEN ASSUME_TAC(FDOM_eff_subset_prob_dom_pair)
THEN ASSUME_TAC(FDOM_pre_subset_prob_dom_pair)
THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF]
THEN METIS_TAC[])

val proj_filter_nvarset_empty = store_thm("proj_filter_nvarset_empty",
``∀PROB as vs vs'.
     no_effectless_act as ∧
     gen_parent_child_rel (PROB,vs,vs') ∧ as IN valid_plans PROB ⇒
     (as_proj (FILTER (λa. ¬varset_action (a, (prob_dom PROB) DIFF vs')) as, (prob_dom PROB) DIFF vs') = [])``,
Induct_on `as`
THEN SRW_TAC[][as_proj_def, no_effectless_act_def]
THENL[
	REWRITE_TAC[FDOM_DRESTRICT]
	THEN REWRITE_TAC[GSYM DISJOINT_DEF]
	THEN MP_TAC(nvarset_act_disj_eff
		  |> Q.SPEC `PROB`
		  |> Q.SPEC `h`
		  |> Q.SPEC `vs`
                  |> Q.SPEC `vs'`)
        THEN SRW_TAC[][]
	THEN1 METIS_TAC[valid_plan_valid_head]
        THEN METIS_TAC[valid_plan_valid_tail]
	,
	METIS_TAC[valid_plan_valid_tail]
])

val sublist_imp_replace_proj_works_diff = store_thm("sublist_imp_replace_proj_works_diff",
``!PROB vs vs' as' as. gen_parent_child_rel(PROB, vs, vs') /\ (as IN valid_plans PROB
                       /\ no_effectless_act(as) /\ sublist as' (as_proj(as, (prob_dom PROB) DIFF vs')))
                       ==> (as' = as_proj(replace_projected ([],as',as, (prob_dom PROB) DIFF vs'), 
                                          (prob_dom PROB) DIFF vs'))``,
Induct_on `as`
THEN SRW_TAC[][as_proj_def, sublist_def, replace_projected_def]
THEN Cases_on `as'`
THEN SRW_TAC[][as_proj_def, sublist_def, replace_projected_def]
THEN FULL_SIMP_TAC(bool_ss)[no_effectless_act_def]
THENL[    FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT]
      	  THEN FULL_SIMP_TAC(srw_ss())[GSYM DISJOINT_DEF]
      	  THEN METIS_TAC[nvarset_act_disj_eff, valid_plan_valid_head]
	  ,  	  
          SRW_TAC[SatisfySimps.SATISFY_ss][valid_plan_valid_tail, proj_filter_nvarset_empty]
  	  ,
	  REWRITE_TAC[REWRITE_RULE[APPEND_NIL](replace_proj_append
			|> Q.SPEC `[h]`
			|> Q.SPEC `[]`
			|> Q.SPEC `t`
			|> Q.SPEC `as`
			|> Q.SPEC `((prob_dom (PROB: ((α |-> β) # (α |-> γ)) set)) DIFF vs')`)]
	  THEN SRW_TAC[][]
	  THEN MP_TAC(not_empty_eff_in_as_proj 
			  |> Q.ISPEC `(replace_projected ([]:((α |-> β) # (α |-> γ)) list,t,as,((prob_dom (PROB: ((α |-> β) # (α |-> γ)) set)) DIFF vs')))`
			  |> Q.SPEC `h`
			  |> Q.SPEC `((prob_dom (PROB: ((α |-> β) # (α |-> γ)) set)) DIFF vs')`)
	  THEN SRW_TAC[][]
	  THEN FULL_SIMP_TAC(bool_ss)[sublist_cons_2, no_effectless_act_def]
	  THEN METIS_TAC[valid_plan_valid_tail]
	  ,
	  FULL_SIMP_TAC(bool_ss)[sublist_def, no_effectless_act_def]
	  THEN METIS_TAC[valid_plan_valid_tail]
	  ,
          FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT]
      	  THEN FULL_SIMP_TAC(srw_ss())[GSYM DISJOINT_DEF]
      	  THEN METIS_TAC[nvarset_act_disj_eff, valid_plan_valid_head] 
	  ,
	  SRW_TAC[SatisfySimps.SATISFY_ss][proj_filter_nvarset_empty, valid_plan_valid_tail]
	  ,
	  SRW_TAC[SatisfySimps.SATISFY_ss][proj_filter_nvarset_empty, valid_plan_valid_tail]
	  ,
	  FULL_SIMP_TAC(bool_ss)[no_effectless_act_def]
          THEN FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT]
      	  THEN FULL_SIMP_TAC(srw_ss())[GSYM DISJOINT_DEF]
      	  THEN METIS_TAC[vset_nempty_efff_not_disj_eff_vs] 
	  ,
	  FULL_SIMP_TAC(bool_ss)[no_effectless_act_def]
          THEN FULL_SIMP_TAC(srw_ss())[FDOM_DRESTRICT]
      	  THEN FULL_SIMP_TAC(srw_ss())[GSYM DISJOINT_DEF]
      	  THEN METIS_TAC[vset_nempty_efff_not_disj_eff_vs] 
	  ,
	  REWRITE_TAC[REWRITE_RULE[APPEND_NIL](replace_proj_append
			|> Q.SPEC `[h]`
			|> Q.SPEC `[]`
			|> Q.SPEC `h'::t`
			|> Q.SPEC `as`
			|> Q.SPEC `((prob_dom (PROB: ((α |-> β) # (α |-> γ)) set)) DIFF vs')`)]
	  THEN SRW_TAC[][]
	  THEN MP_TAC(empty_eff_not_in_as_proj
			  |> Q.ISPEC `(replace_projected ([]:((α |-> β) # (α |-> γ)) list, h'::t,as,((prob_dom (PROB: ((α |-> β) # (α |-> γ)) set)) DIFF vs')))`
			  |> Q.SPEC `h`
			  |> Q.SPEC `((prob_dom (PROB: ((α |-> β) # (α |-> γ)) set)) DIFF vs')`)
	  THEN SRW_TAC[][]
	  THEN FULL_SIMP_TAC(bool_ss)[sublist_cons_2, no_effectless_act_def]
	  THEN METIS_TAC[valid_plan_valid_tail]
])

val nvset_diff_imp_nvset_nvset_diff_union = store_thm("nvset_diff_imp_nvset_nvset_diff_union",
``∀PROB vs vs' a.
    gen_parent_child_rel (PROB,vs,vs') ∧
    a IN PROB ∧ ~(FDOM (SND a) = EMPTY) /\ ~varset_action (a,(prob_dom PROB) DIFF vs')
    ==>  (~varset_action (a,vs) /\ ~varset_action (a,(prob_dom PROB) DIFF (vs UNION vs')))``,
SRW_TAC[][]
THEN ASSUME_TAC(exec_as_proj_valid_2)
THEN FULL_SIMP_TAC(srw_ss())[varset_action_def,
                        gen_parent_child_rel_def,
                        dep_var_set_def,
                        dep_def,
                        SUBSET_DEF,
                        DISJOINT_DEF,
                        INTER_DEF,
                        EXTENSION,
                        SPECIFICATION,
                        gen_parent_child_rel_def,
                        DIFF_DEF,
                        action_dom_pair]
THEN METIS_TAC[])

val vset_diff_ndisj_diff_eff = store_thm("vset_diff_ndisj_diff_eff",
``!PROB a vs vs'.
     gen_parent_child_rel (PROB,vs, vs') ∧ a IN PROB ∧ ~(FDOM (SND a) = EMPTY)
     /\ varset_action (a, (prob_dom PROB) DIFF vs')
     ==> ~ DISJOINT (FDOM (SND a)) ((prob_dom PROB) DIFF vs')``,
SRW_TAC[][varset_action_def,dep_var_set_def, dep_def, DISJOINT_DEF, INTER_DEF, EXTENSION, gen_parent_child_rel_def]
THEN FULL_SIMP_TAC(srw_ss())[SUBSET_DEF]
THEN METIS_TAC[])

val nempty_proj_eff_imp_vset_eq_vset_diff = store_thm("nempty_proj_eff_imp_vset_eq_vset_diff",
``(!PROB vs vs' a. gen_parent_child_rel (PROB,vs,vs')
                   /\ a IN PROB /\ FDOM (SND (action_proj (a,(prob_dom PROB) DIFF vs'))) <> EMPTY
                   ==> (varset_action (a,vs) <=>
                            varset_action (action_proj (a,(prob_dom PROB) DIFF vs'),vs)))``,
NTAC 4 STRIP_TAC
THEN MP_TAC(FDOM_eff_subset_prob_dom_pair)
THEN SRW_TAC[][varset_action_def,
          gen_parent_child_rel_def,
          dep_var_set_def, dep_def, 
          DISJOINT_DEF, INTER_DEF, EXTENSION,
          action_proj_def, FDOM_DRESTRICT, SUBSET_DEF]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN METIS_TAC[])

val empty_eff_vset_vs_vset_diff = store_thm("empty_eff_vset_vs_vset_diff",
``!PROB vs vs' a. gen_parent_child_rel (PROB,vs,vs')
                  /\ a IN PROB /\ FDOM (SND (a)) <> EMPTY  /\ varset_action(a,vs) 
                  ==> varset_action (a, (prob_dom PROB) DIFF vs')``,
NTAC 4 STRIP_TAC
THEN MP_TAC(FDOM_eff_subset_prob_dom_pair)
THEN SRW_TAC[][varset_action_def,
          gen_parent_child_rel_def,
          dep_var_set_def, dep_def, 
          DISJOINT_DEF, INTER_DEF, EXTENSION,
          action_proj_def, FDOM_DRESTRICT, SUBSET_DEF]
THEN METIS_TAC[])

val len_filter_vset_eq_len_replace_proj = store_thm("len_filter_vset_eq_len_replace_proj",
``∀PROB vs vs' as  as'.
     gen_parent_child_rel (PROB,vs,vs') ∧ as IN valid_plans PROB ∧
     sublist as' (as_proj (as, (prob_dom PROB) DIFF vs')) ∧ no_effectless_act as ⇒
     (LENGTH (FILTER (λa. varset_action (a,vs)) as') =
           LENGTH (FILTER (λa. varset_action (a,vs))
                          (replace_projected ([],as',as, (prob_dom PROB) DIFF vs'))))``,
NTAC 5 STRIP_TAC
THEN MP_TAC((nvarset_act_disj_eff |> Q.SPECL[`PROB`,`a`,`vs`,`vs'`] ) |> Q.GEN `a`)
THEN SRW_TAC[][]
THEN MP_TAC(nempty_proj_eff_imp_vset_eq_vset_diff
           |> Q.SPECL  [`PROB`, `vs`, `vs'`])
THEN SRW_TAC[][]
THEN MP_TAC(empty_eff_vset_vs_vset_diff
           |> Q.SPECL  [`PROB`, `vs`, `vs'`])
THEN SRW_TAC[][]
THEN MP_TAC(len_filter_replace_proj_eq_len_filter 
           |> Q.SPECL [`(λa. varset_action (a,vs))`,
                       `(\a. ~(FDOM (SND a)= EMPTY))`,
                       `(\a. a IN PROB)`,
                       `(prob_dom PROB) DIFF vs'`])
THEN SRW_TAC[][]
THEN REWRITE_TAC[Once EQ_SYM_EQ]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN1 METIS_TAC[sublist_every, no_effectless_proj, rem_effectless_works_7]
THEN1 METIS_TAC[rem_effectless_works_7]
THEN1 (FULL_SIMP_TAC(srw_ss())[valid_plans_def] THEN METIS_TAC[subset_every_in])) 

val vset_diff_eq_vset_proj_diff = store_thm("vset_diff_eq_vset_proj_diff",
``!PROB vs vs' a. gen_parent_child_rel (PROB,vs,vs')
                  /\ a IN PROB /\ FDOM (SND (action_proj (a,(prob_dom PROB) DIFF vs'))) <> EMPTY 
                  ==> (varset_action (a,(prob_dom PROB) DIFF (vs UNION vs')) <=>
                          varset_action (action_proj (a,(prob_dom PROB) DIFF vs'),
                                         (prob_dom PROB) DIFF (vs UNION vs')))``,
NTAC 4 STRIP_TAC
THEN MP_TAC(FDOM_eff_subset_prob_dom_pair)
THEN SRW_TAC[][varset_action_def,
          gen_parent_child_rel_def,
          dep_var_set_def, dep_def, 
          DISJOINT_DEF, INTER_DEF, EXTENSION,
          action_proj_def, FDOM_DRESTRICT, SUBSET_DEF, UNION_DEF]
THEN EQ_TAC
THEN SRW_TAC[][]
THEN METIS_TAC[])

val len_filter_vset_diff_eq_len_replace_proj_diff = store_thm("len_filter_vset_diff_eq_len_replace_proj_diff",
``!PROB vs vs' as  as'.
     gen_parent_child_rel (PROB,vs,vs') /\
     as IN valid_plans PROB /\
     sublist as' (as_proj (as, (prob_dom PROB) DIFF vs')) /\ no_effectless_act as
     ==> (LENGTH (FILTER (\a. varset_action (a, (prob_dom PROB) DIFF (vs UNION vs'))) as') =
            LENGTH (FILTER (\a. varset_action (a, (prob_dom PROB) DIFF (vs UNION vs')))
                    (replace_projected ([],as',as, (prob_dom PROB) DIFF vs'))))``,
NTAC 5 STRIP_TAC
THEN SRW_TAC[][]
THEN MP_TAC((nvarset_act_disj_eff |> Q.SPECL[`PROB`,`a`,`vs`,`vs'`] ) |> Q.GEN `a`)
THEN SRW_TAC[][]
THEN MP_TAC(vset_diff_eq_vset_proj_diff
           |> Q.SPECL  [`PROB`, `vs`, `vs'`])
THEN SRW_TAC[][]
THEN MP_TAC(vset_diff_union_vset_diff |> INST_TYPE[beta |-> ``:(α |-> β)``]
           |> Q.SPECL  [`prob_dom (PROB:((α |-> β) # (α |-> γ)) set)`, `vs`, `vs'`])
THEN SRW_TAC[][]
THEN MP_TAC(len_filter_replace_proj_eq_len_filter 
           |> Q.SPECL [`(λa. varset_action (a,(prob_dom (PROB:((α |-> β) # (α |-> γ)) set)) DIFF (vs UNION vs')))`,
                       `(\a. ~(FDOM (SND a)= EMPTY))`,
                       `(\a. a IN PROB)`,
                       `(prob_dom (PROB:((α |-> β) # (α |-> γ)) set)) DIFF vs'`])
THEN SRW_TAC[][]
THEN REWRITE_TAC[Once EQ_SYM_EQ]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN1(METIS_TAC[sublist_every, no_effectless_proj, rem_effectless_works_7])
THEN1(METIS_TAC[rem_effectless_works_7])
THEN1(FULL_SIMP_TAC(srw_ss())[valid_plans_def] THEN METIS_TAC[subset_every_in]))

val vset_diff_eq_vset_proj_diff' = store_thm("vset_diff_eq_vset_proj_diff'",
``!PROB vs vs' a. gen_parent_child_rel (PROB,vs,vs')
                  /\ a IN PROB /\ FDOM (SND (action_proj (a, (prob_dom PROB) DIFF vs'))) <> EMPTY
                  ==> (varset_action (a,(prob_dom PROB) DIFF vs') <=>
                          varset_action (action_proj (a,(prob_dom PROB) DIFF vs'),
                                         (prob_dom PROB) DIFF vs'))``,
NTAC 4 STRIP_TAC
THEN MP_TAC(FDOM_eff_subset_prob_dom_pair)
THEN SRW_TAC[][varset_action_def,
          gen_parent_child_rel_def,
          dep_var_set_def, dep_def, 
          DISJOINT_DEF, INTER_DEF, EXTENSION,
          action_proj_def, FDOM_DRESTRICT, SUBSET_DEF, UNION_DEF]
THEN METIS_TAC[])

val vset_diff_union_vset_diff' = store_thm("vset_diff_union_vset_diff'",
``(∀PROB vs vs' a. gen_parent_child_rel (PROB,vs,vs')
                   /\ a ∈ PROB ∧ FDOM (SND (a)) ≠ ∅  /\ varset_action(a, (prob_dom PROB) DIFF (vs')) 
                   ⇒  varset_action (a, (prob_dom PROB) DIFF vs'))``,
NTAC 4 STRIP_TAC
THEN MP_TAC(FDOM_eff_subset_prob_dom_pair)
THEN SRW_TAC[][varset_action_def,
          gen_parent_child_rel_def,
          dep_var_set_def, dep_def, 
          DISJOINT_DEF, INTER_DEF, EXTENSION,
          action_proj_def, FDOM_DRESTRICT, SUBSET_DEF, UNION_DEF]
THEN METIS_TAC[])

val len_filter_vset_diff_eq_len_replace_proj_diff' = store_thm("len_filter_vset_diff_eq_len_replace_proj_diff'",
``∀PROB vs vs' as  as'.
     gen_parent_child_rel (PROB,vs,vs') ∧
     as IN valid_plans PROB ∧
     sublist as' (as_proj (as, (prob_dom PROB) DIFF vs')) ∧ no_effectless_act as ⇒
     (LENGTH (FILTER (λa. varset_action (a, (prob_dom PROB) DIFF (vs'))) as') =
      LENGTH
        (FILTER (λa. varset_action (a, (prob_dom PROB) DIFF (vs')))
           (replace_projected ([],as',as, (prob_dom PROB) DIFF vs'))))``,
NTAC 5 STRIP_TAC
THEN SRW_TAC[][]
THEN MP_TAC((nvarset_act_disj_eff |> Q.SPECL[`PROB`,`a`,`vs`,`vs'`] ) |> Q.GEN `a`)
THEN SRW_TAC[][]
THEN MP_TAC(vset_diff_eq_vset_proj_diff'
           |> Q.SPECL  [`PROB`, `vs`, `vs'`])
THEN SRW_TAC[][]
THEN MP_TAC(vset_diff_union_vset_diff'
           |> Q.SPECL  [`PROB`, `vs`, `vs'`])
THEN SRW_TAC[][]
THEN MP_TAC(len_filter_replace_proj_eq_len_filter 
           |> Q.SPECL [`(λa. varset_action (a, (prob_dom (PROB:((α |-> β) # (α |-> γ)) set)) DIFF (vs')))`,
                       `(\a. ~(FDOM (SND a)= EMPTY))`,
                       `(\a. a IN PROB)`,
                       `(prob_dom PROB) DIFF vs'`])
THEN SRW_TAC[][]
THEN REWRITE_TAC[Once EQ_SYM_EQ]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN1(METIS_TAC[sublist_every, no_effectless_proj, rem_effectless_works_7])
THEN1(METIS_TAC[rem_effectless_works_7])
THEN1(FULL_SIMP_TAC(srw_ss())[valid_plans_def] THEN METIS_TAC[subset_every_in]))

val nvset_eq_nvset_proj = store_thm("nvset_eq_nvset_proj",
``(∀PROB vs vs' a. gen_parent_child_rel (PROB,vs,vs') 
                   /\ a ∈ PROB ∧ FDOM (SND (action_proj (a, (prob_dom PROB) DIFF vs'))) ≠ ∅ ⇒
                   (¬varset_action (a, (prob_dom PROB) DIFF (vs' ∪ vs)) <=>
                      ~varset_action (action_proj (a, (prob_dom PROB) DIFF vs'),
                                      (prob_dom PROB) DIFF (vs' UNION vs))))``,
NTAC 4 STRIP_TAC
THEN MP_TAC(FDOM_eff_subset_prob_dom_pair)
THEN SRW_TAC[][varset_action_def,
          gen_parent_child_rel_def,
          dep_var_set_def, dep_def, 
          DISJOINT_DEF, INTER_DEF, EXTENSION,
          action_proj_def, FDOM_DRESTRICT, SUBSET_DEF, UNION_DEF]
THEN METIS_TAC[])

val vset_imp_nempty_proj_eff = store_thm("vset_imp_nempty_proj_eff",
``(∀PROB vs vs' a. gen_parent_child_rel (PROB,vs,vs')
                   /\ a ∈ PROB ∧ FDOM (SND a) ≠ ∅  /\ varset_action (a, (prob_dom PROB) DIFF (vs' ∪ vs))
                   ==> ~(FDOM (DRESTRICT (SND a) ((prob_dom PROB) DIFF vs')) = ∅))``,
NTAC 4 STRIP_TAC
THEN MP_TAC(FDOM_eff_subset_prob_dom_pair)
THEN SRW_TAC[][varset_action_def,
          gen_parent_child_rel_def,
          dep_var_set_def, dep_def, 
          DISJOINT_DEF, INTER_DEF, EXTENSION,
          action_proj_def, FDOM_DRESTRICT, SUBSET_DEF, UNION_DEF]
THEN METIS_TAC[])

val every_vset_len_filter_eq_len_filter_proj = store_thm("every_vset_len_filter_eq_len_filter_proj",
``!PROB vs vs' as. as IN valid_plans PROB /\ gen_parent_child_rel(PROB, vs, vs')
                   /\ EVERY (\a. ~varset_action(a, vs')) as /\ no_effectless_act(as)
                   ==> (LENGTH (FILTER (λa. varset_action (a, (prob_dom PROB) DIFF (vs' UNION vs)))
                                       (as_proj (as, (prob_dom PROB) DIFF vs'))) = 
                             LENGTH (FILTER (λa. varset_action (a, (prob_dom PROB) DIFF (vs' UNION vs))) as))``,
NTAC 3 STRIP_TAC
THEN Induct_on `as`
THEN SRW_TAC[][as_proj_def, no_effectless_act_def]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN1(METIS_TAC[valid_plan_valid_tail, vset_imp_vset_act_proj_diff, LENGTH])
THEN1(MP_TAC(SIMP_RULE(srw_ss())[action_proj_def] (nvset_eq_nvset_proj |> Q.SPECL[`PROB`, `vs`, `vs'`, `h`]))
      THEN STRIP_TAC
      THEN `(varset_action (h,(prob_dom PROB) DIFF (vs' ∪ vs)) ⇔
        varset_action
          ((DRESTRICT (FST h) ((prob_dom PROB) DIFF vs'),
            DRESTRICT (SND h) ((prob_dom PROB) DIFF vs')),
           (prob_dom PROB) DIFF (vs' ∪ vs)))` by METIS_TAC[action_proj_def, valid_plan_valid_head]
      THEN METIS_TAC[action_proj_def, valid_plan_valid_head, valid_plan_valid_tail])
THEN1(METIS_TAC[vset_imp_nempty_proj_eff, valid_plan_valid_head])
THEN1(METIS_TAC[vset_imp_nempty_proj_eff, valid_plan_valid_tail]))

(*Removig the vs-actions from a pure actions sequence, i.e. w/o vs'-actions.*)
val n_construction_1st_step = store_thm("n_construction_1st_step",
``!(PROB:'a problem) as vs vs' s. FINITE PROB /\ (s IN valid_states PROB) /\ as IN valid_plans PROB 
	      	 /\ EVERY (\a. ~varset_action (a,vs')) as /\ sat_precond_as(s, as) /\ no_effectless_act(as)
	      	 /\ gen_parent_child_rel(PROB, vs, vs')
		 ==> ?as'. (exec_plan (s,as') = exec_plan (s,as)) /\
       		     	   n(vs, as') ≤ problem_plan_bound(prob_proj(PROB, vs)) /\
                           (n((prob_dom PROB) DIFF vs, as') = n((prob_dom PROB) DIFF vs, as)) /\
       			   EVERY (\a. ~varset_action (a,vs')) as' /\
       			   sublist as' as /\
                           no_effectless_act(as') /\ sat_precond_as(s, as')``,
SRW_TAC[][n_def]
THEN MP_TAC(two_pow_n_is_a_bound_2 |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``, delta |-> ``:bool``] |> Q.SPECL[`PROB`, `(prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs'`])
THEN SRW_TAC[][]
THEN `(FDOM (DRESTRICT s ((prob_dom PROB) DIFF vs')) =
       prob_dom (prob_proj (PROB,(prob_dom PROB) DIFF vs')))` by
      (SRW_TAC[][FDOM_DRESTRICT, graph_plan_neq_mems_state_set_neq_len] THEN FULL_SIMP_TAC(srw_ss())[valid_states_def])
THEN MP_TAC(as_proj_valid_in_prob_proj |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``] |> Q.SPECL[`PROB`, `(prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs'`, `as`] )
THEN SRW_TAC[][]
THEN MP_TAC(gen_par_chil_imp_proj_gen_par_chil |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``] |> Q.SPECL[`PROB`, `vs`, `vs'`] )
THEN SRW_TAC[][]
THEN MP_TAC(sat_precond_drest_as_proj |> INST_TYPE [beta |-> ``:bool``] |> Q.SPECL[`as`, `s`, `s`, `(prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs'`])
THEN SRW_TAC[][]
THEN MP_TAC((no_effectless_proj |> INST_TYPE [beta |->``:bool``, gamma |->``:bool``] )|> Q.SPECL[`(prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs'`,`as`])
THEN SRW_TAC[][]
THEN MP_TAC((finite_imp_finite_prob_proj |> INST_TYPE [beta |->``:bool``, gamma |->``:bool``] |> Q.SPEC `(PROB:((α |-> bool) # (α |-> bool)) set)`) |> Q.GEN `vs` |> Q.SPEC `(prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs'`)
THEN SRW_TAC[][]
THEN MP_TAC(bound_vset_drest_eff_eq' |> Q.SPECL[`prob_proj(PROB, (prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs')`,
                                                `as_proj(as, (prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs')`,
                                                `vs`, `DRESTRICT s ((prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs')`])
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `replace_projected ([],as',as,((prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs'))`
THEN SRW_TAC[][]
THEN1(`as_proj(as', (prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs') = as'` by METIS_TAC[sublist_as_proj_eq_as]
      THEN MP_TAC(sat_precond_drest_sat_precond |> INST_TYPE [beta |-> ``:bool``] |> Q.SPECL[`((prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs')`, `s`, `as'`])
      THEN SRW_TAC[][]
      THEN MP_TAC( sublist_proj_imp_replace_proj_sat_precond_diff |> INST_TYPE [beta |-> ``:bool``] |> Q.SPECL[`PROB`,`vs`,`vs'`,`s`,`s`,`as'`,`as`])
      THEN SRW_TAC[][]
      THEN MP_TAC(sublist_imp_replace_proj_works_diff |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``] |> Q.SPECL[`PROB`, `vs`,`vs'`,`as'`,`as`])
      THEN SRW_TAC[][]       
      THEN `DRESTRICT (exec_plan ( s ,replace_projected ([],as',as,(prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs'))) ((prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs') =
        DRESTRICT (exec_plan ( s ,as)) ((prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs')` by METIS_TAC[graph_plan_lemma_9, graph_plan_lemma_1]
      THEN MP_TAC(every_nvset_imp_every_vset_diff |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``] |> Q.SPECL[`PROB`, `vs`, `vs'`, `s`, `as`])
      THEN SRW_TAC[][]
      THEN MP_TAC(every_vset_imp_drestrict_exec_eq |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``, delta |-> ``:bool``] |> Q.SPECL[`PROB`, `vs'`, `as`, `s`])
      THEN SRW_TAC[][]
      THEN `DRESTRICT s vs' = DRESTRICT (exec_plan (s,replace_projected ([],as',as, (prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs'))) vs'` by 
               METIS_TAC[two_pow_n_is_a_bound2_2, sublist_every, every_nvset_imp_every_vset_diff,
                         every_vset_imp_drestrict_exec_eq, sublist_subset, SUBSET_TRANS, two_pow_n_is_a_bound2_3]
      THEN MP_TAC(valid_as_valid_exec |> INST_TYPE [beta |-> ``:bool``] |> Q.SPECL[`as`,`s`,`PROB`])
      THEN SRW_TAC[][]
      THEN `(exec_plan (s,replace_projected ([]:'a action list, as', as, (prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs'))) IN (valid_states (PROB:((α |-> bool) # (α |-> bool)) set))` by
            METIS_TAC[two_pow_n_is_a_bound2_2, valid_as_valid_exec, sublist_valid_plan]
      THEN FULL_SIMP_TAC(srw_ss()) [valid_states_def]
      THEN METIS_TAC[graph_plan_lemma_5])
THEN1 (MP_TAC(len_filter_vset_eq_len_replace_proj |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``] |> Q.SPECL[`PROB`, `vs`, `vs'`, `as`, `as'`])
      THEN SRW_TAC[][]
      THEN METIS_TAC[disj_proj_proj_eq_proj, gen_parent_child_rel_def])
THEN1(FULL_SIMP_TAC(bool_ss) [PROB_DOM_PROJ_DIFF]
      THEN FULL_SIMP_TAC(bool_ss) [GSYM DIFF_UNION] 
      THEN `((replace_projected ([],as',as,(prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs'))) IN  (valid_plans PROB)` by METIS_TAC[two_pow_n_is_a_bound2_2, sublist_valid_plan]
      THEN FULL_SIMP_TAC(srw_ss())[UNION_COMM]
      THEN MP_TAC(len_filter_vset_diff_eq_len_replace_proj_diff |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``] |> Q.SPECL[`PROB`,`vs`,`vs'`,`as`,`as'`])
      THEN SRW_TAC[][]
      THEN MP_TAC(SIMP_RULE(srw_ss())[UNION_COMM] (every_vset_len_filter_eq_len_filter_proj |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``] |> Q.SPECL[`PROB`,`vs`,`vs'`,`as`]))
      THEN SRW_TAC[][]
      THEN MP_TAC(SIMP_RULE(srw_ss())[UNION_COMM] (filter_vset_diff_eq_union |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``] |> Q.SPECL[`PROB`, `vs`, `vs'`,
                                                   `as`]))
      THEN SRW_TAC[][]
      THEN MP_TAC(SIMP_RULE(srw_ss())[UNION_COMM] (filter_vset_diff_eq_union |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``] |> Q.SPECL[`PROB`, `vs`, `vs'`,
                                                   `(replace_projected ([],as',as,(prob_dom (PROB:((α |-> bool) # (α |-> bool)) set)) DIFF vs'))`]))
      THEN SRW_TAC[][]
      THEN METIS_TAC[sublist_every, two_pow_n_is_a_bound2_2, two_pow_n_is_a_bound2_3])
THEN1 METIS_TAC[ (sublist_every|> Q.SPECL[`as'`, `as`]) |> Q.ISPEC `(λa. ¬varset_action (a,vs'))`, two_pow_n_is_a_bound2_2, two_pow_n_is_a_bound2_3]
THEN1 METIS_TAC[ (sublist_every|> Q.SPECL[`as'`, `as`]) |> Q.ISPEC `(λa. ¬varset_action (a,vs'))`, two_pow_n_is_a_bound2_2, two_pow_n_is_a_bound2_3]
THEN1 METIS_TAC[ (sublist_every|> Q.SPECL[`as'`, `as`]) |> Q.ISPEC `(λa. ¬varset_action (a,vs'))`, two_pow_n_is_a_bound2_2, two_pow_n_is_a_bound2_3]
THEN1 (MATCH_MP_TAC sublist_proj_imp_replace_proj_sat_precond_diff
       THEN Q.EXISTS_TAC `vs`
       THEN Q.EXISTS_TAC `s`
       THEN SRW_TAC[][]
       THEN METIS_TAC[sat_precond_drest_sat_precond, sublist_proj_imp_replace_proj_sat_precond_diff]))

(*Removing condless and effectless actions from what was obtained in the last lemma*)
val n_construction_1st_step' = store_thm("n_construction_1st_step'",
``!(PROB:'a problem) as vs vs' s. FINITE PROB /\ s IN valid_states PROB /\ as IN valid_plans PROB 
	      	 /\ EVERY (\a. ~varset_action (a,vs')) as 
	      	 /\ gen_parent_child_rel(PROB, vs, vs')
		 ==> ?as'. (exec_plan (s,as') = exec_plan (s,as)) /\
       		     	   n(vs, as') ≤ problem_plan_bound(prob_proj(PROB, vs)) /\
(*There is <= instead of = because rem_effectless_act and rem_condless_act remove actions which might be 
(\a. varset_action(a, FDOM PROB.I DIFF vs))*)
                           (n((prob_dom PROB) DIFF vs, as') <= n((prob_dom PROB) DIFF vs, as)) /\
       			   EVERY (\a. ~varset_action (a,vs')) as' /\
       			   sublist as' as /\
                           no_effectless_act(as') /\ sat_precond_as(s, as')``,
SRW_TAC[][]
THEN `(rem_condless_act (s,[],rem_effectless_act as)) IN valid_plans PROB` 
     by METIS_TAC[sublist_trans, rem_effectless_works_9, rem_condless_valid_8,
                  sublist_valid_plan]
THEN `EVERY (λa. ¬varset_action (a,vs'))
        (rem_condless_act (s,[],rem_effectless_act as))` by METIS_TAC[sublist_trans, sublist_every, rem_condless_valid_8, rem_effectless_works_9]
THEN `sat_precond_as
             (s,rem_condless_act (s,[],rem_effectless_act as))` by METIS_TAC[rem_condless_valid_2]
THEN `no_effectless_act
        (rem_condless_act (s,[],rem_effectless_act as))` by METIS_TAC[rem_condless_valid_9, rem_effectless_works_6]
THEN MP_TAC(((n_construction_1st_step |> Q.SPEC `PROB`)|>Q.SPEC `rem_condless_act(s,[],rem_effectless_act(as))`) |> Q.SPECL[ `vs`, `vs'`, `s`])
THEN SRW_TAC[][]
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][]
THEN1(METIS_TAC[rem_effectless_works_1, rem_condless_valid_1])
THEN1(METIS_TAC[rem_effectless_works_5,rem_condless_valid_6,LESS_EQ_TRANS, LESS_OR_EQ, n_def])
THEN1(METIS_TAC[sublist_trans, rem_condless_valid_8, rem_effectless_works_9]))

(*Applying the last lemma to every fragment of the givena ction sequence that has no vs'-actions*)
val n_construction_3rd_step = store_thm("n_construction_3rd_step",
``!(PROB:'a problem) as vs vs' s. FINITE PROB /\ s IN valid_states PROB /\ as IN valid_plans PROB /\ gen_parent_child_rel(PROB, vs, vs')
                   ==> ?as'.  (!as''. list_frag(as', as'') 
                                      /\ EVERY (\a. ~varset_action(a, vs')) as''
                                      ==> n(vs, as'')
                                              <= problem_plan_bound 
                                                   (prob_proj(PROB, vs)))
                            /\ sublist as' as
                            /\ (n((prob_dom PROB) DIFF vs, as') <= n((prob_dom PROB) DIFF vs,as))
                            /\ (exec_plan(s, as') = exec_plan(s, as))``,
SRW_TAC[][]
THEN MP_TAC(exec_plan_Append |> INST_TYPE[beta |-> ``:bool``])
THEN SRW_TAC[][]
THEN MP_TAC (Q.GENL [`as`, `s`](valid_as_valid_exec |> INST_TYPE[beta |-> ``:bool``] |> Q.SPECL[`as`, `s`, `PROB`]) )
THEN SRW_TAC[][]
THEN `!l1 l2. sublist l1 l2 /\ l2 IN (valid_plans PROB) ==> l1 IN valid_plans PROB` by METIS_TAC[sublist_valid_plan]
THEN MP_TAC((n_construction_1st_step' |> Q.SPECL[`PROB`, `as`, `vs`, `vs'`])|>Q.GEN `as`)
THEN SRW_TAC[][]
THEN MP_TAC ((list_with_three_types_shorten_type2 |> INST_TYPE [alpha |->  ``:'a state # 'a state``]
                                 |> INST_TYPE [beta |-> ``:'a state``])
|> Q.SPECL[`(λa. ¬varset_action (a,vs'))`,
           `(λa. varset_action (a,vs))`, 
           `(λa. varset_action (a,(prob_dom (PROB: ('a state # 'a state) set)) DIFF vs))`, 
           `problem_plan_bound (prob_proj (PROB: ('a state # 'a state) set,vs))`,
           `exec_plan`,
           `(\s:'a state. s IN valid_states (PROB: ('a state # 'a state) set))`,
           `(\as'''. as''' IN valid_plans PROB)`,
           `s`,`as`])
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[GSYM n_def]
THEN `∃l'.
        (exec_plan (s,l') = exec_plan(s, as)) ∧
        n ((prob_dom (PROB: ('a state # 'a state) set)) DIFF vs,l') ≤ n ((prob_dom (PROB: ('a state # 'a state) set)) DIFF vs,as) ∧
        (∀l''.
           list_frag (l',l'') ∧ EVERY (λa. ¬varset_action (a,vs')) l'' ⇒
           n (vs,l'') ≤ problem_plan_bound (prob_proj (PROB,vs))) ∧
        sublist l' as` by 
        (FIRST_X_ASSUM MATCH_MP_TAC
        THEN SRW_TAC[][]
        THEN1 METIS_TAC[]
        THEN1 METIS_TAC[]
        THEN1 METIS_TAC[valid_pref_suff_valid_append, valid_append_valid_pref, valid_append_valid_suff])
THEN Q.EXISTS_TAC `l'`
THEN SRW_TAC[][]
THEN METIS_TAC[])

val n_construction_4th_step_2 = store_thm("n_construction_4th_step_2",
``(∀PROB vs vs' a. gen_parent_child_rel (PROB,vs,vs')
                   /\ a ∈ PROB ∧ FDOM (SND (a)) ≠ ∅  /\ varset_action(a, vs') 
                   ⇒  ~varset_action (a,vs))``,
SRW_TAC[][varset_action_def,
          gen_parent_child_rel_def,
          dep_var_set_def, dep_def, 
          DISJOINT_DEF, INTER_DEF, EXTENSION,
          action_proj_def, FDOM_DRESTRICT, SUBSET_DEF, UNION_DEF]
THEN METIS_TAC[])

(*Removing the vs-actions in the mixed sequence, after removing the vs'-actions in the 3rd-step*)
val n_construction_4th_step = store_thm("n_construction_4th_step",
``!(PROB:'a problem) vs vs' as s. FINITE PROB /\ (s IN valid_states PROB) /\ as IN (valid_plans PROB)
                     /\ gen_parent_child_rel(PROB, vs, vs')
                   ==> ?as'.n(vs, as')
                             <= problem_plan_bound (prob_proj(PROB, vs)) * (n(vs', as') + 1)
                         /\ sublist as' as
                         /\ (n((prob_dom PROB) DIFF vs, as') <= n((prob_dom PROB) DIFF vs,as))
                         /\ (exec_plan (s,as') = exec_plan (s,as))``,
REWRITE_TAC[Once MULT_COMM]
THEN SRW_TAC[][]
THEN `(exec_plan (s,rem_effectless_act as) = exec_plan(s,as)) /\ ((rem_effectless_act as) IN valid_plans PROB)` by
     (SRW_TAC[][]
     THEN FULL_SIMP_TAC(srw_ss())[]
     THEN1 METIS_TAC[rem_effectless_works_1]
     THEN1 METIS_TAC[rem_effectless_works_9,sublist_valid_plan])
THEN MP_TAC(n_construction_3rd_step |> Q.SPECL[`PROB`, `rem_effectless_act(as)`, `vs`, `vs'`, `s`])
THEN SRW_TAC[][]
THEN `no_effectless_act(as')` by METIS_TAC[rem_effectless_works_13, rem_effectless_works_6]
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][]
THEN1(MP_TAC ((REWRITE_RULE[GSYM n_def] (threesorted_list_length |> INST_TYPE [alpha |-> ``:'a action``]
                 |> Q.SPECL[`(λa:'a action. varset_action (a:'a action,vs':'a set))`, `(λa:'a action. varset_action (a,vs:'a set))`]))
                 |> Q.SPECL[`as':'a action list`, `n (vs':'a set,as':'a action list)`, `problem_plan_bound (prob_proj ((PROB:'a problem),vs))`])
      THEN SRW_TAC[][]
      THEN FIRST_X_ASSUM MATCH_MP_TAC
      THEN SRW_TAC[][] 
      THEN FULL_SIMP_TAC(srw_ss())[]
      THEN MATCH_MP_TAC (n_construction_4th_step_2 |> Q.SPECL[`PROB`, `vs`,`vs'`])
      THEN SRW_TAC[][]
      THEN1 METIS_TAC[valid_plan_mems, rem_effectless_works_9, sublist_trans, sublist_valid_plan]
      THEN1 METIS_TAC[rem_effectless_works_7,EVERY_MEM |> Q.ISPEC `(\a. FDOM (SND a) <>  EMPTY)`])
THEN1(METIS_TAC[rem_effectless_works_9, sublist_trans])
THEN1(FULL_SIMP_TAC(srw_ss())[n_def] 
      THEN METIS_TAC[rem_effectless_works_5|> Q.ISPEC `(λa. varset_action (a, (prob_dom PROB) DIFF vs))`, 
                     LESS_EQ_TRANS]))

val n_construction_4th_step_ITP2015 = n_construction_4th_step;

val N_def
      = Define `(N(PROB, vs, vs'::lvs) =  if dep_var_set(PROB, vs, vs') then
                                                problem_plan_bound(prob_proj(PROB,vs))*N(PROB, vs', lvs) +
                                                           N(PROB,vs,lvs)
                                          else 
                                                N(PROB,vs,lvs)) /\ 
                (N(PROB,vs,[]) = problem_plan_bound(prob_proj(PROB,vs)))`

val top_sorted_def 
     = Define `(top_sorted(PROB, vs::lvs) 
                  =  (FOLDL (\acc vs'. acc /\ ~dep_var_set(PROB, vs',vs)) T lvs) /\ top_sorted(PROB, lvs))
               /\ (top_sorted(PROB, []) = T)`

val top_sorted_is_top_sorted_abs = store_thm("top_sorted_is_top_sorted_abs",
``top_sorted(PROB, lvs) = top_sorted_abs (\vs1 vs2. dep_var_set(PROB, vs1, vs2)) (lvs)``,
Induct_on `lvs`
THEN SRW_TAC[][top_sorted_def,top_sorted_abs_def]
THEN METIS_TAC[FOLDL_TRUE_eq_EVERY_TRUE])

val disjoint_varset_def = 
   Define`disjoint_varset(lvs) <=> (!vs vs'. (MEM vs lvs /\ MEM vs' lvs /\ vs' <> vs) ==> (DISJOINT vs vs') )`;

val children_def
      = Define `(children(PROB, vs, lvs) =  FILTER (\vs'. dep_var_set(PROB, vs, vs')) lvs)`

val disjvarset_cons_1 = store_thm("disjvarset_cons_1",
``!h lvs. ALL_DISTINCT (h::lvs) /\ disjoint_varset(h::lvs) ==> DISJOINT h (BIGUNION (set lvs))``,
SRW_TAC[][disjoint_varset_def]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN SRW_TAC[][]
THEN METIS_TAC[set_list_works])

val disjvarset_cons_2 = store_thm("disjvarset_cons_2",
``!h lvs. disjoint_varset(h::lvs) ==> disjoint_varset(lvs)``,
SRW_TAC[][disjoint_varset_def])

val top_sorted_cons_2 = store_thm("top_sorted_cons_2",
``!PROB h lvs. top_sorted (PROB,h::lvs) ==> top_sorted (PROB,lvs)``,
SRW_TAC[][top_sorted_def])

val top_sorted_imp_top_sorted_proj = store_thm("top_sorted_imp_top_sorted_proj",
``!PROB vs lvs. top_sorted (PROB, lvs) ==> top_sorted (prob_proj(PROB, vs), lvs)``,
Induct_on `lvs`
THEN SRW_TAC[][top_sorted_def]
THEN FULL_SIMP_TAC(srw_ss())[top_sorted_def]
THEN MP_TAC( SIMP_RULE(srw_ss())[] foldl_conj_with_acc_eq_T_cong 
            |> Q.ISPECL[`(\vs'. ~dep_var_set(PROB, vs', h))`,`(\vs'. ~dep_var_set(prob_proj(PROB, vs), vs', h))`]
            |> Q.SPEC `lvs`)
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM MATCH_MP_TAC
THEN METIS_TAC[NDEP_PROJ_NDEP])

val top_sorted_cons = store_thm("top_sorted_cons",
``!PROB vs vs' lvs. top_sorted(PROB, vs::vs'::lvs) ==> top_sorted(PROB, vs::lvs)``,
SRW_TAC[][top_sorted_def]
THEN MP_TAC(foldl_conj_with_acc
            |> Q.ISPEC `(\vs'. ~dep_var_set(PROB, vs', vs))`
            |> Q.SPECL[`(¬dep_var_set (PROB,vs',vs))`,`T`, `lvs`]    )
THEN SRW_TAC[][])

val top_sorted_cons_imp_ndep_parent = store_thm("top_sorted_cons_imp_ndep_parent",
``!PROB vs lvs vs'. top_sorted (PROB,vs::lvs) /\ MEM vs' lvs ==> ~dep_var_set(PROB,vs',vs)``,
NTAC 2 STRIP_TAC
THEN Induct_on `lvs`
THEN SRW_TAC[][]
THEN1(FULL_SIMP_TAC(srw_ss())[top_sorted_def]
      THEN MP_TAC(foldl_with_acc |> Q.ISPEC `(\vs'. ~dep_var_set(PROB, vs', vs))`
            |> Q.SPECL [`(¬dep_var_set (PROB,h,vs))`, `lvs`])
      THEN SRW_TAC[][] )
THEN1(METIS_TAC[top_sorted_cons]))

val top_sorted_child_parent_rest = store_thm("top_sorted_child_parent_rest",
``!PROB vs lvs. (prob_dom PROB = BIGUNION (set (vs::lvs))) /\ ALL_DISTINCT (vs::lvs) /\
                 disjoint_varset(vs::lvs) /\ top_sorted (PROB, vs::lvs)
                 ==> child_parent_rel(PROB, BIGUNION (set lvs))``,
REPEAT STRIP_TAC
THEN MP_TAC(disjvarset_cons_1 |> Q.SPECL[`vs`,`lvs`])
THEN FULL_SIMP_TAC(bool_ss)[]
THEN STRIP_TAC
THEN SRW_TAC[][top_sorted_def, child_parent_rel_def]
THEN SRW_TAC[][DIFF_SAME_UNION, disj_diff_eq]
THEN METIS_TAC[top_sorted_cons_imp_ndep_parent, dep_slist_imp_mem_dep])

val children_sublist = store_thm("children_sublist",
``!vs lvs. sublist (children(PROB, vs,lvs)) lvs ``,
SRW_TAC[][children_def]
THEN PROVE_TAC[sublist_filter])

val disjvset_imp_neq_mem_then_disj = store_thm("disjvset_imp_neq_mem_then_disj",
``!lvs lvs'. sublist lvs' lvs /\ disjoint_varset lvs
                  ==> ∀vs vs'. MEM vs lvs ∧ MEM vs' lvs' ∧ vs' ≠ vs ⇒ DISJOINT vs vs'``,
METIS_TAC[children_sublist, sublist_subset,
          SUBSET_DEF, disjoint_varset_def, set_list_works])

val dep_imp_mem_children = store_thm("dep_imp_mem_children",
``!PROB vs vs' lvs. dep_var_set(PROB, vs, vs') /\ MEM vs' lvs
                    ==> MEM vs' (children(PROB,vs,lvs))``,
NTAC 3 STRIP_TAC
THEN Induct_on `lvs`
THEN SRW_TAC[][children_def]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN SRW_TAC[][GSYM children_def])

val top_sorted_pfx_imp_ndep = store_thm("top_sorted_pfx_imp_ndep",
``!PROB vs vs' pfx mid sfx. top_sorted(PROB, pfx ++ [vs] ++ mid ++ [vs'] ++ sfx)
           ==> ~dep_var_set(PROB, vs', vs)``,
STRIP_TAC
THEN Induct_on `pfx`
THEN SRW_TAC[][]
THEN1(Induct_on `mid`
      THEN SRW_TAC[][]
      THEN1(FULL_SIMP_TAC(srw_ss())[top_sorted_def]
            THEN MP_TAC(foldl_with_acc|> Q.ISPEC `(\vs'. ~dep_var_set(PROB, vs', vs))`
                         |> Q.SPECL [`~dep_var_set(PROB, vs', vs)`, `sfx`])
            THEN SRW_TAC[][])
      THEN1(METIS_TAC[top_sorted_cons]))
THEN1(METIS_TAC[top_sorted_cons_2]))

val top_sorted_gen_child_parent_rest = store_thm("top_sorted_gen_child_parent_rest",
``!PROB vs lvs. (prob_dom PROB = BIGUNION (set (vs::lvs))) /\ ALL_DISTINCT(vs::lvs) /\
                 disjoint_varset(vs::lvs) /\ top_sorted (PROB, vs::lvs)
                 ==> gen_parent_child_rel(PROB,vs,
                                          BIGUNION (set (children (PROB,vs,lvs))))``,
SRW_TAC[][gen_parent_child_rel_def]
THEN1(`s' IN set  lvs` by
        METIS_TAC[sublist_subset, children_sublist, set_list_works, SUBSET_DEF]
      THEN MP_TAC(disjvarset_cons_1 |> Q.SPECL [`vs`, `lvs`])
      THEN SRW_TAC[][])
THEN1(SPOSE_NOT_THEN STRIP_ASSUME_TAC
      THEN assume_thm_concl_after_proving_assums(dep_sos_imp_mem_dep |> Q.SPECL[`PROB`,`(set (children (PROB,vs,lvs)))`, `vs`])
           THEN1 SRW_TAC[][]
      THEN `vs' IN set  lvs` by
           METIS_TAC[sublist_subset, children_sublist, set_list_works, SUBSET_DEF]
      THEN METIS_TAC[top_sorted_cons_imp_ndep_parent])
THEN1(SRW_TAC[][disj_imp_union_diff_union_eq_diff, disjvarset_cons_1]
      THEN assume_thm_concl_after_proving_assums(GSYM (bigunion_diff_eq_diff_bigunion |> Q.SPECL [`set lvs`, `(set (children (PROB,vs,lvs)))`]))
          THEN1 METIS_TAC[children_sublist,
                          disjvset_imp_neq_mem_then_disj,
                          disjvarset_cons_2]
      THEN SRW_TAC[][]
      THEN SPOSE_NOT_THEN STRIP_ASSUME_TAC
      THEN assume_thm_concl_after_proving_assums(dep_biunion_imp_or_dep |> Q.SPECL[`PROB`, `vs`, `(set lvs DIFF set (children (PROB,vs,lvs)))`])
           THEN1 SRW_TAC[][]
      THEN FULL_SIMP_TAC(srw_ss())[DIFF_DEF, EXTENSION]
      THEN METIS_TAC[dep_imp_mem_children,
                     set_list_works])
THEN1(FIRST_X_ASSUM (MP_TAC o Q.SPEC `s'`)
      THEN SRW_TAC[][]
      THEN `MEM s lvs` by
           METIS_TAC[children_sublist, sublist_subset, SUBSET_DEF]
      THEN `s <> s'` by METIS_TAC[set_list_works]
      THEN MP_TAC(neq_mem_dichotomy |> INST_TYPE[alpha |-> ``:'a -> bool``] |> Q.SPECL[`s`, `s'`, `lvs`])
      THEN METIS_TAC[top_sorted_pfx_imp_ndep, top_sorted_cons_2, 
                     disjoint_varset_def, disjvarset_cons_2,
                     dep_var_set_def]))

val n_replace_proj_le_n_as = store_thm("n_replace_proj_le_n_as",
``!PROB vs lvs as as'. MEM vs lvs
                       ==>n(vs, replace_projected([], as', as, BIGUNION (set lvs))) <= n(vs,as')``,
NTAC 3 STRIP_TAC
THEN Induct_on `as`
THEN1(SRW_TAC[][]
      THEN Cases_on `as'`
      THEN SRW_TAC[][replace_projected_def, n_def])
THEN1(SRW_TAC[][]
      THEN Cases_on `as'`
      THEN1(SRW_TAC[][replace_projected_def, n_def]
            THEN1(Cases_on `varset_action (h,vs)`
                  THEN SRW_TAC[][]
                  THEN1(METIS_TAC[SUBSET_BIGUNION_I, set_list_works,
                        n_replace_proj_le_n_as_1])
                  THEN1(FIRST_X_ASSUM (ASSUME_TAC o SIMP_RULE(srw_ss())[n_def, replace_projected_def] o Q.SPEC `[]`)
                        THEN METIS_TAC[]))
            THEN1(FIRST_X_ASSUM (ASSUME_TAC o SIMP_RULE(srw_ss())[n_def, replace_projected_def] o Q.SPEC `[]`)
                        THEN METIS_TAC[]))
      THEN1(SRW_TAC[][replace_projected_def]
            THEN1(SRW_TAC[][n_def]
                  THEN1(`varset_action(h,vs)` by
                            METIS_TAC[SUBSET_BIGUNION_I, set_list_works,
                            n_replace_proj_le_n_as_2]
                        THEN SRW_TAC[][REWRITE_RULE[APPEND_NIL] (replace_proj_append
                                         |> Q.SPECL[`[h]`, `[]`, `t`, `as`, `BIGUNION (set lvs)`] )]
                        THEN METIS_TAC[n_def])
                  THEN1(`~varset_action(h,vs)` by
                            METIS_TAC[SUBSET_BIGUNION_I, set_list_works,
                            n_replace_proj_le_n_as_2]
                        THEN SRW_TAC[][REWRITE_RULE[APPEND_NIL] (replace_proj_append
                                         |> Q.SPECL[`[h]`, `[]`, `t`, `as`, `BIGUNION (set lvs)`] )]
                        THEN METIS_TAC[n_def]))
            THEN1(SRW_TAC[][n_def]
                  THEN `~varset_action(h,vs)` by
                            METIS_TAC[SUBSET_BIGUNION_I, set_list_works,
                            n_replace_proj_le_n_as_1]
                  THEN SRW_TAC[][REWRITE_RULE[APPEND_NIL] (replace_proj_append
                                      |> Q.SPECL[`[h]`, `[]`, `h'::t`, `as`, `BIGUNION (set lvs)`] )]
                  THEN FIRST_X_ASSUM (MP_TAC o SIMP_RULE(srw_ss())[n_def] o Q.SPEC `h'::t`)
                  THEN SRW_TAC[][]))))

val N_cons_mem = store_thm("N_cons_mem",
``!PROB vs vs' lvs.
  top_sorted(PROB,vs::lvs) /\ MEM vs' lvs 
  ==> (N (PROB,vs',lvs) = N (PROB,vs',vs::lvs)) ``,
SRW_TAC[][N_def]
THEN METIS_TAC[top_sorted_cons_imp_ndep_parent])

val prob_proj_idempot_symm = GSYM prob_proj_idempot;

val N_proj_le_N = store_thm("N_proj_le_N",
``!PROB vs' vs lvs. vs SUBSET vs' /\ BIGUNION (set lvs) SUBSET vs'
                ==>  N (prob_proj (PROB,vs'),vs,lvs) <= N (PROB,vs,lvs)``,
STRIP_TAC
THEN Induct_on `lvs`
THEN1 SRW_TAC[][N_def, prob_proj_idempot_symm]
THEN1(SRW_TAC[][N_def]
      THEN1(SRW_TAC[][prob_proj_idempot_symm]
            THEN FIRST_ASSUM (assume_thm_concl_after_proving_assums o Q.SPECL[`vs'`,`h`])
                 THEN1 SRW_TAC[][]
            THEN FIRST_ASSUM (assume_thm_concl_after_proving_assums o Q.SPECL[`vs'`,`vs`])
                 THEN1 SRW_TAC[][]
            THEN METIS_TAC[mult_plus_leq])
      THEN1(METIS_TAC[NDEP_PROJ_NDEP])
      THEN1(METIS_TAC[mult_plus_leq, LESS_EQ_ADD, LESS_EQ_TRANS, ADD_COMM])))

val N_proj_le_N' = store_thm("N_proj_le_N'",
``!PROB lvs vs. MEM vs lvs 
                ==>  N (prob_proj (PROB,BIGUNION (set lvs)),vs,lvs) <= N (PROB,vs,lvs)``,
SRW_TAC[][]
THEN MATCH_MP_TAC N_proj_le_N
THEN SRW_TAC[][] THEN METIS_TAC[SUBSET_BIGUNION_I])

val n_bigunion_le_sum_1 = store_thm("n_bigunion_le_sum_1",
``!PROB vs lvs. top_sorted(PROB, vs::lvs)
                ==> ~dep_var_set(PROB, BIGUNION (set lvs), vs)``,
METIS_TAC[top_sorted_cons_imp_ndep_parent, dep_slist_imp_mem_dep])

val n_bigunion_le_sum_4 = store_thm("n_bigunion_le_sum_4",
``!PROB as vs vs'. as IN valid_plans PROB /\ DISJOINT vs vs' /\ 
                   (~dep_var_set(PROB, vs', vs) \/ ~dep_var_set(PROB, vs, vs'))
                   ==> n(vs UNION vs', as) <= n(vs, as) + n(vs',as)``,
STRIP_TAC
THEN Induct_on `as`
THEN SRW_TAC[][n_def]
THEN `as IN valid_plans PROB` by METIS_TAC[valid_plan_valid_tail]
THEN `h IN PROB` by METIS_TAC[valid_plan_valid_head]
THEN1(SRW_TAC[][GSYM n_def]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`,`vs'`])
      THEN SRW_TAC[][]
      THEN DECIDE_TAC)
THEN1(SRW_TAC[][GSYM n_def]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`,`vs'`])
      THEN SRW_TAC[][]
      THEN DECIDE_TAC)
THEN1(SRW_TAC[][GSYM n_def]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`,`vs'`])
      THEN SRW_TAC[][]
      THEN DECIDE_TAC)
THEN1(SRW_TAC[][GSYM n_def]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`,`vs'`])
      THEN SRW_TAC[][]
      THEN DECIDE_TAC)
THEN1(SRW_TAC[][GSYM n_def]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`,`vs'`])
      THEN SRW_TAC[][]
      THEN DECIDE_TAC)
THEN1(SRW_TAC[][GSYM n_def]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`,`vs'`])
      THEN SRW_TAC[][]
      THEN DECIDE_TAC)
THEN1(METIS_TAC[disj_not_dep_vset_union_imp_or])
THEN1(METIS_TAC[disj_not_dep_vset_union_imp_or])
THEN1(SRW_TAC[][GSYM n_def]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`,`vs'`])
      THEN SRW_TAC[][]
      THEN DECIDE_TAC)
THEN1(SRW_TAC[][GSYM n_def]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`,`vs'`])
      THEN SRW_TAC[][]
      THEN DECIDE_TAC)
THEN1(SRW_TAC[][GSYM n_def]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`,`vs'`])
      THEN SRW_TAC[][]
      THEN DECIDE_TAC)
THEN1(SRW_TAC[][GSYM n_def]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`,`vs'`])
      THEN SRW_TAC[][]
      THEN DECIDE_TAC)
THEN1(SRW_TAC[][GSYM n_def]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`,`vs'`])
      THEN SRW_TAC[][]
      THEN DECIDE_TAC)
THEN1(SRW_TAC[][GSYM n_def]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`,`vs'`])
      THEN SRW_TAC[][]
      THEN DECIDE_TAC)
THEN1(SRW_TAC[][GSYM n_def]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`,`vs'`])
      THEN SRW_TAC[][]
      THEN DECIDE_TAC)
THEN1(SRW_TAC[][GSYM n_def]
      THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`vs`,`vs'`])
      THEN SRW_TAC[][]
      THEN DECIDE_TAC))

val n_bigunion_le_sum = store_thm("n_bigunion_le_sum",
``!P f PROB as lvs. as IN valid_plans PROB /\ no_effectless_act(as)
                    /\ top_sorted(PROB, lvs) /\ disjoint_varset(lvs) /\ ALL_DISTINCT(lvs)
                    /\ EVERY (\vs. n(vs, as) <= f vs) lvs
                    ==>  n( BIGUNION (set (FILTER P lvs)), as) <= SUM (MAP f (FILTER P lvs))``,
NTAC 3 STRIP_TAC
THEN Induct_on `lvs`
THEN1 SRW_TAC[][n_def, rem_effectless_works_7, varset_action_def, GSYM filter_empty_every_not, disjoint_varset_def]
THEN1(SRW_TAC[][]
      THEN1(`!a b c d. a <= c /\ b <= d ==> (a+b) <= (c+d)` by DECIDE_TAC
            THEN MP_TAC(n_bigunion_le_sum_1 |> Q.SPECL[`PROB`, `h`,`lvs`])
            THEN SRW_TAC[][]
            THEN MP_TAC(set_subset_filter_subset |> INST_TYPE [alpha |-> ``:'a -> bool``] |> Q.SPECL[`P`, `lvs`, `set lvs`])
            THEN SRW_TAC[][]
            THEN MP_TAC(n_bigunion_le_sum_2 |> Q.SPECL[`set (FILTER P lvs)`, `set lvs`])
            THEN SRW_TAC[][]
            THEN assume_thm_concl_after_proving_assums(n_bigunion_le_sum_3 |> Q.SPECL[`PROB`, `h`, `(set (FILTER P lvs))`])
                 THEN1(SRW_TAC[][MEM_FILTER]
                       THEN METIS_TAC[top_sorted_cons_imp_ndep_parent])
            THEN assume_thm_concl_after_proving_assums(n_bigunion_le_sum_4 |> Q.SPECL[`PROB`, `as`, `h`, `BIGUNION (set (FILTER P lvs))`])
                 THEN1 METIS_TAC[disjvarset_cons_1, DISJOINT_SUBSET, DISJOINT_SYM, ALL_DISTINCT]
            THEN METIS_TAC[top_sorted_cons_2, LESS_EQ_TRANS, ALL_DISTINCT, disjvarset_cons_2])
      THEN1 METIS_TAC[top_sorted_cons_2, LESS_EQ_TRANS, ALL_DISTINCT, disjvarset_cons_2]))

val N_eq_problem_plan_bound_mult_sum_1_non_empty_prob = store_thm("N_eq_problem_plan_bound_mult_sum_1_non_empty_prob",
``!vs lvs PROB. vs <> EMPTY ==> (N(PROB, vs, vs::lvs) = N(PROB, vs, lvs))``,
SRW_TAC[][N_def]
THEN FULL_SIMP_TAC(srw_ss())[dep_var_set_def])

val N_eq_problem_plan_bound_mult_sum_1_empty_prob = store_thm("N_eq_problem_plan_bound_mult_sum_1_empty_prob",
``!vs lvs PROB. (vs = EMPTY) ==> (N(PROB, vs, vs::lvs) = N(PROB, vs, lvs))``,
SRW_TAC[][N_def]
THEN METIS_TAC[empty_problem_proj_bound])

val N_eq_problem_plan_bound_mult_sum_1 = store_thm("N_eq_problem_plan_bound_mult_sum_1",
``!vs lvs PROB. (N(PROB, vs, vs::lvs) = N(PROB, vs, lvs))``,
 METIS_TAC[N_eq_problem_plan_bound_mult_sum_1_empty_prob, N_eq_problem_plan_bound_mult_sum_1_non_empty_prob])

val N_eq_problem_plan_bound_mult_sum_non_empty_prob = store_thm("N_eq_problem_plan_bound_mult_sum_non_empty_prob",
``!PROB vs lvs.EVERY (\vs. vs <> EMPTY) (vs::lvs) /\ top_sorted(PROB, vs::lvs)
                ==> (N(PROB, vs, vs::lvs) = 
                      problem_plan_bound (prob_proj (PROB,vs)) *
                                  (SUM (MAP (\vs'. N(PROB, vs', lvs)) (children(PROB,vs,lvs))) + 1))``,
STRIP_TAC
THEN Induct_on `lvs`
THEN1(SRW_TAC[][N_def, children_def] THEN FULL_SIMP_TAC(srw_ss())[dep_var_set_def])
THEN SRW_TAC[][N_eq_problem_plan_bound_mult_sum_1_non_empty_prob, children_def, N_def] 
THEN SRW_TAC[][dep_var_set_def]
THEN SRW_TAC[][GSYM dep_var_set_def, GSYM children_def]
THEN `EVERY (\vs'. ~dep_var_set((PROB: ((α |-> β) # (α |-> β)) set), vs', h)) (children ((PROB: ((α |-> β) # (α |-> β)) set),vs,lvs))` by
               (SRW_TAC[][children_def]
                THEN METIS_TAC[top_sorted_cons_imp_ndep_parent |> INST_TYPE[gamma |-> ``:'b``] |> Q.SPECL[`(PROB: ((α |-> β) # (α |-> β)) set)`, `h`,`lvs`,`vs'`] |> Q.GEN `vs'`,
                                top_sorted_cons_2, EVERY_MEM |> Q.ISPEC `(\vs'. ~dep_var_set((PROB: ((α |-> β) # (α |-> β)) set), vs', vs))`,
                                EVERY_FILTER_IMP |> Q.ISPECL[`(λvs'. ¬dep_var_set ((PROB: ((α |-> β) # (α |-> β)) set),vs',h))`, `(λvs'. dep_var_set ((PROB: ((α |-> β) # (α |-> β)) set),vs,vs'))`]])
THEN MP_TAC(N_eq_problem_plan_bound_mult_sum_2
                              |> Q.ISPECL[`(\vs'. problem_plan_bound (prob_proj ((PROB: ((α |-> β) # (α |-> β)) set),vs')) *
                                                    N ((PROB: ((α |-> β) # (α |-> β)) set),h,lvs) + N ((PROB: ((α |-> β) # (α |-> β)) set),vs',lvs))`,
                                          `(\vs'. N ((PROB: ((α |-> β) # (α |-> β)) set),vs',lvs))`, 
                                          `(\vs'. dep_var_set ((PROB: ((α |-> β) # (α |-> β)) set),vs',h))`,
                                          `(children ((PROB: ((α |-> β) # (α |-> β)) set),vs,lvs))`] )
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (assume_thm_concl_after_proving_assums o REWRITE_RULE[LEFT_ADD_DISTRIB] o Q.SPEC `vs`)
     THEN1(SRW_TAC[][] THEN METIS_TAC[top_sorted_cons])
THEN `!a b c. (a = b) ==> (c + a = c + b)` by DECIDE_TAC
THEN SRW_TAC[][LEFT_ADD_DISTRIB]
THEN `N ((PROB: ((α |-> β) # (α |-> β)) set),vs,vs::lvs) = N ((PROB: ((α |-> β) # (α |-> β)) set),vs,lvs)` by METIS_TAC[N_eq_problem_plan_bound_mult_sum_1_non_empty_prob]
THEN1 DECIDE_TAC
THEN1 METIS_TAC[top_sorted_cons]
THEN1 DECIDE_TAC)

val N_eq_problem_plan_bound_mult_sum = store_thm("N_eq_problem_plan_bound_mult_sum",
``!PROB vs lvs. top_sorted(PROB, vs::lvs)
                ==> (N(PROB, vs, vs::lvs) = 
                      problem_plan_bound (prob_proj (PROB,vs)) *
                                  (SUM (MAP (\vs'. N(PROB, vs', lvs))
                                            (children(PROB,vs,lvs)))
                                    + 1))``,
Induct_on `lvs`
THEN1(SRW_TAC[][N_def, children_def] THEN FULL_SIMP_TAC(srw_ss())[dep_var_set_def]
      THEN SRW_TAC[][empty_problem_proj_bound])
THEN SRW_TAC[][N_eq_problem_plan_bound_mult_sum_1, children_def, N_def] 
THEN SRW_TAC[][dep_var_set_def]
THEN SRW_TAC[][GSYM dep_var_set_def, GSYM children_def]
THEN1 FULL_SIMP_TAC(srw_ss())[]
THEN `EVERY (\vs'. ~dep_var_set((PROB: ((α |-> β) # (α |-> β)) set), vs', h)) (children ((PROB: ((α |-> β) # (α |-> β)) set),vs,lvs))` by
               (SRW_TAC[][children_def]
                THEN METIS_TAC[top_sorted_cons_imp_ndep_parent |> INST_TYPE [gamma |-> ``:'b``] |> Q.SPECL[`(PROB: ((α |-> β) # (α |-> β)) set)`, `h`,`lvs`,`vs'`] |> Q.GEN `vs'`,
                               top_sorted_cons_2, EVERY_MEM |> Q.ISPEC `(\vs'. ~dep_var_set((PROB: ((α |-> β) # (α |-> β)) set), vs', vs))`,
                               EVERY_FILTER_IMP|> Q.ISPECL[`(λvs'. ¬dep_var_set ((PROB: ((α |-> β) # (α |-> β)) set),vs',h))`, `(λvs'. dep_var_set ((PROB: ((α |-> β) # (α |-> β)) set),vs,vs'))`]])
THEN MP_TAC(N_eq_problem_plan_bound_mult_sum_2
                              |> Q.ISPECL[`(\vs'. problem_plan_bound (prob_proj ((PROB: ((α |-> β) # (α |-> β)) set),vs')) *
                                                    N ((PROB: ((α |-> β) # (α |-> β)) set),h,lvs) + N ((PROB: ((α |-> β) # (α |-> β)) set),vs',lvs))`,
                                          `(\vs'. N ((PROB: ((α |-> β) # (α |-> β)) set),vs',lvs))`,
                                          `(\vs'. dep_var_set ((PROB: ((α |-> β) # (α |-> β)) set),vs',h))`,
                                          `(children ((PROB: ((α |-> β) # (α |-> β)) set),vs,lvs))`] )
THEN SRW_TAC[][]
THEN FIRST_X_ASSUM (assume_thm_concl_after_proving_assums o REWRITE_RULE[LEFT_ADD_DISTRIB] o Q.SPEC `vs` o Q.SPEC `(PROB: ((α |-> β) # (α |-> β)) set)` )
     THEN1(SRW_TAC[][] THEN METIS_TAC[top_sorted_cons])
THEN `!a b c. (a = b) ==> (c + a = c + b)` by DECIDE_TAC
THEN SRW_TAC[][LEFT_ADD_DISTRIB]
THEN `N ((PROB: ((α |-> β) # (α |-> β)) set),vs,vs::lvs) = N ((PROB: ((α |-> β) # (α |-> β)) set),vs,lvs)` by METIS_TAC[N_eq_problem_plan_bound_mult_sum_1]
THEN1 DECIDE_TAC
THEN1 METIS_TAC[top_sorted_cons]
THEN1 FULL_SIMP_TAC(srw_ss())[GSYM N_def])

(*We first show that there is a sublist of the given action sequence, 
  where for every vs in the dependency graph, the number of vs actions
  in the constructed sublist will be less than N(vs).*)

val problem_plan_bound_N_bound_1st_step = store_thm("problem_plan_bound_N_bound_1st_step",
``!(PROB:'a problem) as lvs s.
       FINITE PROB /\ s IN valid_states PROB /\ as IN valid_plans PROB
       /\ ALL_DISTINCT(lvs) /\ disjoint_varset(lvs)
       /\ (prob_dom PROB = BIGUNION (set lvs)) /\ top_sorted (PROB,lvs)
       /\ no_effectless_act(as) /\ sat_precond_as(s, as) /\ lvs <> []
       ==> ?as'.
            (exec_plan(s, as) = exec_plan(s, as')) /\ sublist as' as
            /\ EVERY (\vs. n(vs, as') <= N (PROB, vs, lvs)) lvs
            /\ sat_precond_as(s, as')``,
Induct_on `lvs`
THEN SRW_TAC[][]
THEN Cases_on `lvs = []`
THEN1(FULL_SIMP_TAC(srw_ss())[]
      THEN SRW_TAC[][N_def, n_def]
      THEN1(FULL_SIMP_TAC(srw_ss())[DISJOINT_EMPTY_REFL_RWT, dep_var_set_def, sat_precond_as_def]
            THEN SRW_TAC[][empty_problem_proj_bound] THEN Q.EXISTS_TAC `[]`
            THEN SRW_TAC[][sublist_def, sat_precond_as_def, exec_plan_def]
            THEN FULL_SIMP_TAC(srw_ss())[])
      THEN1(METIS_TAC[valid_filter_vset_dom_idempot,
                      problem_plan_bound_works',
                      PROJ_DOM_IDEMPOT, LE_LT1, sublist_valid_plan]))
THEN1(Q.PAT_X_ASSUM `x = y UNION z` (ASSUME_TAC o GSYM)
      THEN MP_TAC(REWRITE_RULE[Once DISJOINT_SYM] disjvarset_cons_1 |> Q.SPECL[`h`,`lvs`])
      THEN ASM_SIMP_TAC(bool_ss)[]
      THEN STRIP_TAC
      THEN MP_TAC (MATCH_MP  AND1_THM   (DIFF_SAME_UNION |> Q.SPECL[`h`,`BIGUNION (set lvs)`]))
      THEN MP_TAC(disj_diff_eq |> Q.SPECL[`BIGUNION (set lvs)`, `h`])
      THEN SRW_TAC[][]
      THEN MP_TAC(disjvarset_cons_2 |> Q.SPECL[`h`,`lvs`])
      THEN SRW_TAC[][]
      THEN MP_TAC(top_sorted_cons_2 |> INST_TYPE [beta |-> ``:bool``, gamma|-> ``:bool``]|> Q.SPECL[`PROB`, `h`,`lvs`])
      THEN SRW_TAC[][]      
      THEN MP_TAC(top_sorted_imp_top_sorted_proj |> INST_TYPE [beta |-> ``:bool``, gamma|-> ``:bool``] |> Q.SPECL[`PROB`, `BIGUNION (set lvs)`,`lvs`])
      THEN SRW_TAC[][]
      THEN MP_TAC(sat_precond_drest_as_proj |> INST_TYPE [beta |-> ``:bool``, gamma|-> ``:bool``] |> Q.SPECL[`as`, `s`, `s`, `BIGUNION (set lvs)`])
      THEN SRW_TAC[][]
      THEN MP_TAC(sat_precond_exec_as_proj_eq_proj_exec  |> INST_TYPE [beta |-> ``:bool``, gamma|-> ``:bool``] |> Q.SPECL[`as`, `(BIGUNION (set lvs))`, `s`])
      THEN SRW_TAC[][]
      THEN MP_TAC((no_effectless_proj |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``]) |> Q.SPECL[`BIGUNION (set lvs)`, `as`])
      THEN SRW_TAC[][]
      THEN `FINITE (prob_proj (PROB,BIGUNION (set lvs)))` by METIS_TAC[finite_imp_finite_prob_proj]
      THEN FIRST_X_ASSUM (MP_TAC o
                          Q.SPECL[`prob_proj(PROB, (prob_dom (PROB:'a problem)) DIFF h)`, `as_proj(as, (prob_dom (PROB:'a problem)) DIFF h)`, `DRESTRICT s ((prob_dom (PROB:'a problem)) DIFF h)`])
      THEN SRW_TAC[][PROB_DOM_PROJ_DIFF, two_pow_n_is_a_bound_2, as_proj_valid_in_prob_proj]
      THEN MP_TAC(top_sorted_child_parent_rest |> INST_TYPE [beta |-> ``:bool``, gamma|-> ``:bool``] |> Q.SPECL[`PROB`, `h`, `lvs`])
      THEN SRW_TAC[][]
      THEN assume_thm_concl_after_proving_assums(bound_vset_drest_eff_eq'' |> INST_TYPE [beta |-> ``:bool``, gamma|-> ``:bool``]
                   |> Q.SPECL[`PROB`, `as`, `as'`, `BIGUNION (set lvs)`, `s`])
           THEN1(METIS_TAC[sat_precond_drest_sat_precond])
      THEN MP_TAC(top_sorted_gen_child_parent_rest |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``] |> Q.SPECL[`PROB`, `h`, `lvs`])
      THEN SRW_TAC[][]
      THEN `replace_projected ([],as',as,BIGUNION (set lvs)) IN valid_plans PROB` by METIS_TAC[two_pow_n_is_a_bound2_2, sublist_valid_plan]
      THEN MP_TAC(n_construction_4th_step |> Q.SPECL[`PROB`, `h`,
                                              `BIGUNION (set (children(PROB: 'a problem, h, lvs)))`,
                                              `replace_projected([], as', as, BIGUNION (set lvs))`, `s`])
      THEN SRW_TAC[][]
      THEN Q.EXISTS_TAC `rem_condless_act(s, [], as'')`
      THEN SRW_TAC[][]
      THEN1 METIS_TAC[rem_condless_valid_1]
      THEN1 METIS_TAC[sublist_trans, two_pow_n_is_a_bound2_2, rem_condless_valid_8]
      THEN `EVERY (λvs. n (vs,as'') ≤ N (PROB,vs,lvs)) lvs` by
            (MP_TAC(N_proj_le_N' |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``] |> Q.SPECL[`PROB`, `lvs`])
             THEN SRW_TAC[][]
             THEN assume_thm_concl_after_proving_assums (SIMP_RULE(srw_ss())[]
                  ((every_cong
                        |> Q.ISPECL[`(\vs'. n(vs', as':'a action list) <= N (prob_proj (PROB:'a problem,BIGUNION (set lvs)),vs',lvs))`,
                                       `(\vs'. n(vs', as':'a action list) <= N (PROB:'a problem,vs',lvs))`])|>Q.SPEC `lvs`))
                 THEN1 METIS_TAC[LESS_EQ_TRANS]
             THEN assume_thm_concl_after_proving_assums (SIMP_RULE(srw_ss())[](every_cong 
                         |> Q.ISPECL[`(\vs'. (n (vs',as':'a action list) ≤ N (PROB:'a problem,vs',lvs)))`,
                                     `(\vs'. (n (vs',replace_projected ([],as':'a action list,as,BIGUNION (set lvs))) ≤ N (PROB:'a problem,vs',lvs)))`])|> Q.SPEC `lvs`)
                  THEN1 METIS_TAC[LESS_EQ_TRANS, n_def, n_replace_proj_le_n_as]
             THEN assume_thm_concl_after_proving_assums_2 (SIMP_RULE(srw_ss())[](every_cong 
                         |> Q.ISPECL[`(\vs'. (n (vs',replace_projected ([],as':'a action list,as,BIGUNION (set lvs))) ≤ N (PROB:'a problem,vs',lvs)))`,
                                     `(\vs'. (n (vs',as'':'a action list) ≤ N (PROB:'a problem,vs',lvs)))`])|> Q.SPEC `lvs`)
                 THEN1(SRW_TAC[][]
                       THEN METIS_TAC[LESS_EQ_TRANS, n_def, sublist_imp_len_filter_le]))
      THEN `EVERY (λvs. n (vs,as'':'a action list) ≤ N (PROB:'a problem,vs,h::lvs)) lvs` by
            (assume_thm_concl_after_proving_assums ((every_cong 
                         |> Q.ISPECL[`(\vs'. (n (vs',as'':'a action list) ≤ N (PROB:'a problem,vs',lvs)))`,
                                     `(\vs'. (n (vs',as'':'a action list) ≤ N (PROB:'a problem,vs',h::lvs)))`])|> Q.SPEC `lvs`)
                 THEN1(SRW_TAC[][]
                      THEN METIS_TAC[LESS_EQ_TRANS, n_def, N_cons_mem])
            THEN METIS_TAC[])
      THEN1(`n(h,as'':'a action list) <= N (PROB:'a problem,h,h::lvs)` by
               (assume_thm_concl_after_proving_assums(SIMP_RULE(srw_ss())[GSYM children_def] (n_bigunion_le_sum  |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``]
                       |> Q.SPECL[`(λvs'. dep_var_set (PROB:'a problem,h,vs'))`,
                                  `(\vs'. N(PROB:'a problem, vs',lvs))`,
                                  `PROB:'a problem`, `as'':'a action list`,`lvs`]))
                    THEN1(SRW_TAC[][] 
                          THEN1 METIS_TAC[sublist_trans, sublist_valid_plan, two_pow_n_is_a_bound2_2]
                          THEN1 METIS_TAC[rem_effectless_works_13, two_pow_n_is_a_bound2_2,
                                          sublist_trans])
                THEN SRW_TAC[][N_eq_problem_plan_bound_mult_sum] 
                THEN METIS_TAC[mult_left_le, LESS_EQ_TRANS])
            THEN METIS_TAC[rem_condless_valid_6 |> Q.SPECL[`as''`, `s`, `(\a. varset_action(a, h))`],
                        n_def, LESS_EQ_TRANS])
      THEN1(assume_thm_concl_after_proving_assums((every_cong 
                         |> Q.ISPECL[`(\vs'. (n (vs',as'':'a action list) ≤ N (PROB:'a problem,vs',h::lvs)))`,
                                     `(\vs'. (n (vs',rem_condless_act(s, [], as'':'a action list)) 
                                                ≤ N (PROB:'a problem ,vs',h::lvs)))`])|> Q.SPEC `lvs`)
                 THEN1(SRW_TAC[][]
                      THEN METIS_TAC[rem_condless_valid_6 |> Q.SPECL[`as''`, `s`, `(\a. varset_action(a, h))`],
                           n_def, LESS_EQ_TRANS])
            THEN METIS_TAC[])
      THEN1 METIS_TAC[rem_condless_valid_2]))

val every_vset_n_eq_length = store_thm("every_vset_n_eq_length",
``!k as vs. EVERY (\a. varset_action(a, vs)) as /\ n(vs, as) <= k
            ==> LENGTH as <= k``,
Induct_on `as`
THEN SRW_TAC[][]
THEN FULL_SIMP_TAC(srw_ss())[n_def]
THEN `LENGTH (h::FILTER (\a. varset_action (a,vs)) as) <= k` by METIS_TAC[]
THEN FULL_SIMP_TAC(srw_ss())[GSYM n_def]
THEN `(n (vs,as)) <= k - 1 ` by DECIDE_TAC
THEN FIRST_X_ASSUM (MP_TAC o Q.SPECL[`k - 1`, `vs`])
THEN SRW_TAC[][]
THEN DECIDE_TAC)

(*Thus, the length of the contructed sublist will be less than the sum of N(vs)
  for all vs in the dep graph.*)

val problem_plan_bound_N_bound_2nd_step = store_thm("problem_plan_bound_N_bound_2nd_step",
``!(PROB:'a problem) as lvs s.
       FINITE PROB /\ ALL_DISTINCT(lvs) /\ disjoint_varset(lvs)
       /\ s IN valid_states PROB /\ as IN valid_plans PROB
       /\ (prob_dom PROB = BIGUNION (set lvs))
       /\ top_sorted (PROB,lvs) /\ no_effectless_act(as)
       /\ sat_precond_as(s, as) /\ lvs <> []
       ==> ?as'.
            (exec_plan(s,as) = exec_plan(s,as')) /\ sublist as' as /\
            LENGTH as' < SUM (MAP (\vs. N (PROB,vs,lvs)) lvs) + 1``,
SRW_TAC[][]
THEN MP_TAC(problem_plan_bound_N_bound_1st_step |> Q.SPECL[`PROB`, `as`, `lvs`, `s`])
THEN SRW_TAC[][]
THEN  Q.EXISTS_TAC `rem_effectless_act(as')`
THEN SRW_TAC[][GSYM LE_LT1]
THEN1 METIS_TAC[rem_effectless_works_14]
THEN1 METIS_TAC[rem_effectless_works_9, sublist_trans]
THEN FULL_SIMP_TAC(srw_ss())[]
THEN MP_TAC(rem_effectless_works_6 |> INST_TYPE [beta |-> ``:'a``, gamma |-> ``:bool``] |> Q.SPEC `as`)
THEN SRW_TAC[][]
THEN assume_thm_concl_after_proving_assums(SIMP_RULE(srw_ss())[] (n_bigunion_le_sum |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``] |> Q.SPECL[`(\a. T)`,`(\vs. N(PROB:'a problem, vs, lvs))`, `PROB`, `rem_effectless_act(as')`, `lvs`]))
     THEN1(SRW_TAC[][]
           THEN1 METIS_TAC[rem_effectless_works_9, sublist_valid_plan, sublist_trans]
           THEN1 SRW_TAC[][rem_effectless_works_6]
           THEN1(assume_thm_concl_after_proving_assums((every_cong
                         |> Q.ISPECL[`(\vs'. (n (vs',as':'a action list) ≤ N (PROB:'a problem,vs',lvs)))`,
                                     `(\vs'. (n (vs',rem_effectless_act (as':'a action list)) 
                                                ≤ N (PROB:'a problem,vs',lvs)))`])|> Q.SPEC `lvs`)
                 THEN1(SRW_TAC[][]
                       THEN METIS_TAC[LESS_EQ_TRANS, n_def,
                                      rem_effectless_works_5 |> Q.ISPEC `(\a. varset_action(a, vs))`])
                 THEN METIS_TAC[]))
THEN FULL_SIMP_TAC(srw_ss())[filter_true]
THEN MATCH_MP_TAC every_vset_n_eq_length
THEN Q.EXISTS_TAC `BIGUNION (set lvs)`
THEN SRW_TAC[][]
THEN assume_thm_concl_after_proving_assums(valid_filter_vset_dom_idempot |> INST_TYPE [beta |-> ``:bool``, gamma |-> ``:bool``] |> Q.SPECL[`PROB`, `rem_effectless_act as'`])
     THEN1 METIS_TAC[rem_effectless_works_9, sublist_valid_plan, sublist_trans]
THEN METIS_TAC[FILTER_EQ_ID])

(*We now show that we can remove the effectless actions from the constructed sublist.*)

val problem_plan_bound_N_bound_3rd_step = store_thm("problem_plan_bound_N_bound_3rd_step",
``!(PROB:'a problem) as lvs s.
       FINITE PROB /\ ALL_DISTINCT(lvs) /\ disjoint_varset(lvs)
       /\ s IN valid_states PROB /\ as IN valid_plans PROB
       /\ (prob_dom PROB = BIGUNION (set lvs)) /\ top_sorted (PROB,lvs)
       /\ sat_precond_as(s, as) /\ lvs <> []
       ==> ?as'.
              (exec_plan (s,as) = exec_plan (s,as')) /\ sublist as' as
              /\ LENGTH as' < SUM (MAP (\vs. N (PROB,vs,lvs)) lvs) + 1``,
SRW_TAC[][]
THEN assume_thm_concl_after_proving_assums(problem_plan_bound_N_bound_2nd_step |> Q.SPECL[`PROB`, `rem_effectless_act(as)`, `lvs`, `s`])
     THEN1(SRW_TAC[][]
           THEN1 METIS_TAC[sublist_valid_plan, rem_effectless_works_9]
           THEN1 METIS_TAC[rem_effectless_works_6]
           THEN1 METIS_TAC[rem_effectless_works_2])
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][]
THEN1 METIS_TAC[rem_effectless_works_14]
THEN METIS_TAC[rem_effectless_works_9, sublist_trans])

(*Simialrly, we remove the condless actions from the constructed sublist.*)

val problem_plan_bound_N_bound_4th_step = store_thm("problem_plan_bound_N_bound_4th_step",
``!(PROB:'a problem) as lvs s.
       FINITE PROB /\ ALL_DISTINCT(lvs) /\ disjoint_varset(lvs)
       /\ s IN valid_states PROB /\ as IN valid_plans PROB
       /\ (prob_dom PROB = BIGUNION (set lvs))
       /\ top_sorted (PROB,lvs) /\ lvs <> []
       ==> ?as'.
             (exec_plan (s,as) = exec_plan (s,as')) /\ sublist as' as
             /\ LENGTH as' < SUM (MAP (\vs. N (PROB,vs,lvs)) lvs) + 1``,
SRW_TAC[][]
THEN assume_thm_concl_after_proving_assums(problem_plan_bound_N_bound_3rd_step |> Q.SPECL[`PROB`, `rem_condless_act(s,[],as)`, `lvs`, `s`])
     THEN1(SRW_TAC[][]
           THEN1 METIS_TAC[rem_condless_valid_10]
           THEN1 METIS_TAC[rem_condless_valid_2])
THEN Q.EXISTS_TAC `as'`
THEN SRW_TAC[][]
THEN1 METIS_TAC[rem_condless_valid_1]
THEN METIS_TAC[rem_condless_valid_8, sublist_trans])

val parent_children_main_lemma_non_empty_graph = store_thm("parent_children_main_lemma_non_empty_graph",
``!lvs. ALL_DISTINCT(lvs) /\ disjoint_varset(lvs)
        /\ lvs <> [] ==> 
        (!(PROB:'a problem). 
             ((prob_dom PROB = BIGUNION (set lvs)) /\ top_sorted (PROB,lvs)) 
              /\ FINITE PROB
             ==> problem_plan_bound(PROB) < SUM (MAP (\vs. N(PROB,vs,lvs)) lvs) + 1)``,
NTAC 2 STRIP_TAC
THEN MATCH_MP_TAC( SIMP_RULE(srw_ss())[] (bound_on_all_plans_bounds_problem_plan_bound |> INST_TYPE [beta |-> ``:bool``]
                  |> Q.SPECL[`(\PROB. (prob_dom PROB = BIGUNION (set lvs)) /\ top_sorted (PROB,lvs))`,
                              `(\PROB. SUM (MAP (\vs. N(PROB,vs,lvs)) lvs) + 1)`]))
THEN SRW_TAC[][]
THEN METIS_TAC[problem_plan_bound_N_bound_4th_step])

val parent_children_main_lemma_empty_graph = store_thm("parent_children_main_lemma_empty_graph",
``!lvs. ALL_DISTINCT(lvs) /\ disjoint_varset(lvs)
        /\ (lvs = []) ==> 
        (!(PROB:'a problem). ((prob_dom PROB = BIGUNION (set lvs)) /\ top_sorted (PROB,lvs))
             ==> problem_plan_bound(PROB) < SUM (MAP (\vs. N(PROB,vs,lvs)) lvs) + 1)``,
SRW_TAC[][]
THEN MP_TAC(empty_problem_bound |> INST_TYPE [beta |-> ``:bool``] |> Q.SPEC `PROB:'a problem`)
THEN SRW_TAC[][])

val parent_children_main_lemma = store_thm("parent_children_main_lemma",
``!lvs. ALL_DISTINCT(lvs) /\ disjoint_varset(lvs)
        ==> 
        (!(PROB:'a problem). ((prob_dom PROB = BIGUNION (set lvs)) /\ top_sorted (PROB,lvs)) 
             /\ FINITE PROB
             ==> problem_plan_bound(PROB) < SUM (MAP (\vs. N(PROB,vs,lvs)) lvs) + 1)``,
METIS_TAC[parent_children_main_lemma_non_empty_graph, parent_children_main_lemma_empty_graph])

val N_empty = store_thm("N_empty",
``(N(PROB, EMPTY, lvs) = 0)``,
Induct_on `lvs`
THEN SRW_TAC[][N_def, empty_problem_proj_bound]);

val N_def_ITP2015 = store_thm("N_def_ITP2015",
``!(PROB :((α |-> β) # (α |-> β)) set) vs lvs. top_sorted((PROB :((α |-> β) # (α |-> β)) set), lvs)
           ==>
             (N((PROB :((α |-> β) # (α |-> β)) set), vs, lvs)
               = problem_plan_bound (prob_proj((PROB :((α |-> β) # (α |-> β)) set), vs)) * (SUM (MAP (\vs'. N((PROB :((α |-> β) # (α |-> β)) set), vs', lvs)) (children((PROB :((α |-> β) # (α |-> β)) set), vs, lvs))) + 1))``,
REWRITE_TAC[LEFT_ADD_DISTRIB, MULT_RIGHT_1]
THEN Induct_on `lvs`
THEN1 SRW_TAC[][N_def, children_def]
THEN SRW_TAC[][N_def, children_def]
THEN SRW_TAC[][GSYM children_def]
THEN `EVERY (\vs. ~dep_var_set ((PROB :((α |-> β) # (α |-> β)) set),vs,h)) lvs` by (SRW_TAC[][listTheory.EVERY_MEM] THEN METIS_TAC[top_sorted_cons_imp_ndep_parent])
THEN `EVERY (\vs. ~dep_var_set ((PROB :((α |-> β) # (α |-> β)) set),vs,h)) (children((PROB :((α |-> β) # (α |-> β)) set), vs, lvs))` by SRW_TAC[][children_def, listTheory.EVERY_FILTER_IMP]
THEN MP_TAC(N_eq_problem_plan_bound_mult_sum_2 |> INST_TYPE [alpha |-> ``:'a -> bool``, beta |-> ``:num``]
                    |> Q.SPECL[`(\vs'. problem_plan_bound (prob_proj ((PROB :((α |-> β) # (α |-> β)) set),vs')) * N ((PROB :((α |-> β) # (α |-> β)) set),h,lvs) +
                                       N ((PROB :((α |-> β) # (α |-> β)) set),vs',lvs))`,
                               `(\vs'. N ((PROB :((α |-> β) # (α |-> β)) set),vs',lvs))`,
                               `(\vs'. dep_var_set ((PROB :((α |-> β) # (α |-> β)) set),vs',h))`,
                               `(children ((PROB :((α |-> β) # (α |-> β)) set),vs,lvs))`])
THEN SRW_TAC[][]
THEN1(`N ((PROB :((α |-> β) # (α |-> β)) set),h,lvs) = 0` by METIS_TAC[dep_var_set_self_empty,N_empty]
      THEN SRW_TAC[][MATCH_MP AND1_THM MULT, MULT_COMM]
      THEN FIRST_X_ASSUM MATCH_MP_TAC THEN SRW_TAC[][]
      THEN METIS_TAC[top_sorted_cons_2])
THEN1(SRW_TAC[][LEFT_ADD_DISTRIB]
      THEN `(!a x y. (x = y) ==> (a + x= a + y))` by DECIDE_TAC
      THEN REWRITE_TAC[GSYM ADD_ASSOC]
      THEN FIRST_X_ASSUM MATCH_MP_TAC 
      THEN FIRST_X_ASSUM MATCH_MP_TAC 
      THEN METIS_TAC[top_sorted_cons_2])
THEN1(FIRST_X_ASSUM MATCH_MP_TAC 
      THEN METIS_TAC[top_sorted_cons_2]))

val N_curried_def = Define`N_curried PROB lvs vs = N(PROB, vs, lvs)`;

val N_is_wighted_longest_path = store_thm("N_is_wighted_longest_path",
``!vs. N(PROB,vs,lvs) = wlp (\vs1 vs2. dep_var_set(PROB, vs1, vs2)) (\vs. problem_plan_bound(prob_proj(PROB,vs))) $+ $* vs (lvs)``,
Induct_on `lvs`
THEN SRW_TAC[][N_def,wlp_def])

val N_curried_is_wighted_longest_path = store_thm("N_curried_is_wighted_longest_path",
``!vs. N_curried PROB lvs vs = wlp (\vs1 vs2. dep_var_set(PROB, vs1, vs2)) (\vs. problem_plan_bound(prob_proj(PROB,vs))) $+ $* vs (lvs)``,
SRW_TAC[][N_curried_def, N_is_wighted_longest_path])

val MAP_LAMBDA_ABSTRACTION_thm = store_thm("MAP_LAMBDA_ABSTRACTION_thm",
``MAP f l = MAP (\x. f x) l``,
Induct_on `l`
THEN SRW_TAC[][])

val top_sorted_curried_def 
     = Define `(top_sorted_curried PROB  (vs::lvs)
                  =  (FOLDL (\acc vs'. acc /\ ~dep_var_set(PROB, vs',vs)) T lvs) /\ top_sorted_curried PROB  lvs)
               /\ (top_sorted_curried PROB [] = T)`

val top_sorted_curried_uncurried_equiv_thm = store_thm("top_sorted_curried_uncurried_equiv_thm",
``top_sorted_curried PROB lvs = top_sorted(PROB,lvs)``,
Induct_on `lvs`
THEN SRW_TAC[][top_sorted_curried_def, top_sorted_def])

val children_curried_def
      = Define `children_curried PROB lvs vs =  FILTER (\vs'. dep_var_set(PROB, vs, vs')) lvs`

val children_curried_uncurried_equiv_thm = store_thm("children_curried_uncurried_equiv_thm",
``children_curried PROB lvs vs = children(PROB, vs, lvs)``,
SRW_TAC[][children_curried_def, children_def])

val parent_children_main_lemma_curried_N = store_thm("parent_children_main_lemma_curried_N",
``!lvs. ALL_DISTINCT(lvs) /\ disjoint_varset(lvs)
        ==> 
        (!PROB:'a problem. (prob_dom PROB = BIGUNION (set lvs)) /\ top_sorted_curried PROB lvs /\ FINITE PROB
             ==> problem_plan_bound(PROB) < (SUM ( MAP (N_curried PROB lvs) lvs)) + 1)``,
REWRITE_TAC[Once MAP_LAMBDA_ABSTRACTION_thm]
THEN REWRITE_TAC[N_curried_def, top_sorted_curried_uncurried_equiv_thm]
THEN SRW_TAC[][parent_children_main_lemma])

val N_curried_def_ITP2015 = store_thm("N_curried_def_ITP2015",
``!PROB vs lvs. top_sorted_curried PROB lvs
           ==>
             ((N_curried PROB lvs vs)
               = problem_plan_bound (prob_proj(PROB, vs)) * (SUM (MAP (N_curried PROB lvs) (children_curried PROB lvs vs)) + 1))``,
SRW_TAC[][MAP_LAMBDA_ABSTRACTION_thm, top_sorted_curried_uncurried_equiv_thm, children_curried_uncurried_equiv_thm]
THEN SRW_TAC[][N_curried_def]
THEN MATCH_MP_TAC (N_def_ITP2015)
THEN SRW_TAC[][])

val dep_DAG_def = Define `dep_DAG PROB lvs = (prob_dom PROB = BIGUNION (set lvs)) /\ ALL_DISTINCT(lvs) /\ disjoint_varset(lvs) /\ top_sorted_abs (\vs1 vs2. dep_var_set(PROB, vs1, vs2)) (lvs)`


val parent_children_main_lemma_curried_N_thesis = store_thm("parent_children_main_lemma_curried_N_thesis",
``!lvs PROB:'a problem. FINITE PROB /\ dep_DAG PROB lvs
                        ==> problem_plan_bound(PROB) < (SUM ( MAP (N_curried PROB lvs) lvs)) + 1``,
SRW_TAC[][dep_DAG_def, GSYM top_sorted_is_top_sorted_abs]
THEN METIS_TAC[parent_children_main_lemma_curried_N, top_sorted_curried_uncurried_equiv_thm])


val _ = export_theory();
