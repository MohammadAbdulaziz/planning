\vspace{-1ex}
\section{Conclusions}

The recurrence diameter was identified by many earlier authors as an appealing upper bound on transition sequence lengths, both in the area of verification~\cite{baumgartner2002property,KroeningS03,kroening2011linear} and AI~planning~\cite{abdulaziz2015verified,icaps2017,abdulaziz:2019}.
However, previous authors noted that computing the recurrence diameter is not practically useful, since, in the worst case, it can take exponentially longer than solving the underlying planning or model-checking problem.
Nonetheless, we show that, indeed, computing the recurrence diameter can be practically very useful when used for compositional bounding.
We do so by providing a careful SMT encoding that exploits the factored state space representation, and by cleverly combining the recurrence diameter with other easier to compute topological properties, like the traversal diameter.

%% The encoding by Kroening.
