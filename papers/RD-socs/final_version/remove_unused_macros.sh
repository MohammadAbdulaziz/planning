#!/bin/bash

macro_names=`cat macro_names.txt`

cp paper.tex old_paper.tex

remove(){
    for macro in $macro_names; do
	echo -n $macro
	uses=`grep $macro paper.tex | wc -l`
	if [ $uses -gt 1 ]; then
	    echo " used macro $uses";
	else
	    echo " unused macro $uses"
	    cat paper.tex | grep -v $macro > paper_tmp
            mv paper_tmp paper.tex;
	fi
    done
}

remove
while [ `diff old_paper.tex paper.tex | wc -l` -gt 0 ]; do
   cp paper.tex old_paper.tex
   remove
done

rm old_paper.tex
