\def\year{2020}\relax
\documentclass[letterpaper]{article}
\usepackage{aaai20}
\usepackage{times}
\usepackage{helvet}
\usepackage{courier}
\usepackage{url}
\usepackage{graphicx}
\frenchspacing  
\setlength{\pdfpagewidth}{8.5in}  
\setlength{\pdfpageheight}{11in}  


\usepackage[T1]{fontenc}

\usepackage{multirow}
\usepackage[ruled,noline,noalgohanging]{algorithm2e}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows,positioning,decorations.markings}
\tikzset{
  varnode/.style={rectangle,outer sep=0mm},
  varnodenoperi/.style={rectangle,outer sep=-1mm},
  ourarrow/.style={>=stealth}, 
  ourarc/.style={>=stealth,thick,arc}}
\usepackage{amsthm}
\usepackage{amssymb}
\newtheorem{mycase}{Case}
\newtheorem{mydef}{Definition}
\newtheorem{myenc}{Encoding}
\newtheorem{mythm}{Theorem}
\newtheorem{myprop}{Proposition}
\newtheorem{myeg}{Example}
\newtheorem{myconj}{Conjecture}


\usepackage{multicol}
\usepackage{latexsym}
\usepackage[inline]{enumitem}
\setlist[enumerate]{label=(\roman*)}
\usepackage[Euler]{upgreek}
\usepackage{tabularx}
\usepackage{mathtools}
\usepackage{mathtools}
\usepackage{subcaption}

\pdfinfo{
/Keywords (State Space Diameter, Causal Graph, Plan Length Bounds)}

\title{Computing Plan-Length Bounds Using Lengths of Longest Paths (Extended Abstract)}
\author{Mohammad Abdulaziz and Dominik Berger\\
Technical University of Munich, Munich, Germany
}


\begin{document}

\maketitle

\providecommand{\insts}{}
\renewcommand{\insts}{\ensuremath{\Delta}}
\providecommand{\inst}{\ensuremath{\tvsal}}
\newcommand{\act}{\ensuremath{\pi}}
\newcommand{\asarrow}[1]{\vec{#1}}
\renewcommand{\vec}[1]{\overset{\rightarrow}{#1}}
\newcommand{\as}{\ensuremath{\vec{{\act}}}}

\newcommand{\etc}{\textit{etc.}}
\newcommand{\versus}{\textit{vs.}}
\newcommand{\ie}{i.e.}
\newcommand{\Ie}{I.e.}
\newcommand{\eg}{e.g.}
\newcommand{\michael}[1]{\textcolor{blue}{M: #1}}
\newcommand{\abziz}[1]{\textcolor{brown}{#1}}
\newcommand{\sublist}[2]{ \ensuremath{#1} \preceq\!\!\!\raisebox{.4mm}{\ensuremath{\cdot}}\; \ensuremath{#2}}
\newcommand{\subscriptsublist}[2]{\ensuremath{#1}\preceq\!\raisebox{.05mm}{\ensuremath{\cdot}}\ensuremath{#2}}
\newcommand{\PLS}{\Pi^\preceq\!\raisebox{1mm}{\ensuremath{\cdot}}}
\newcommand{\PLScharles}{\Pi^d}
\newcommand{\execname}{\mathsf{ex}}
\newcommand{\IndHyp}{\mathsf{IH}}
\newcommand{\exec}[2]{#2(#1)}

\newcommand{\ancestorssymbol}{\textsf{\upshape ancestors}}
\newcommand{\ancestors}{\ancestorssymbol}
\newcommand{\satpreas}[2]{\ensuremath{sat_precond_as(s, \as)}}
\newcommand{\proj}[2]{\ensuremath{#1{\downharpoonright}_{#2}}}
\newcommand{\dep}[3]{\ensuremath{#2 {\rightarrow} #3}}
\newcommand{\deptc}[3]{\ensuremath{#2 {\rightarrow^+} #3}}
\newcommand{\negdep}[3]{\ensuremath{#2 \not\rightarrow #3}}
\newcommand{\leavessymbol}{\textsf{\upshape leaves}}
\newcommand{\leaves}{\leavessymbol}

\newcommand{\childrensymbol}{\textsf{\upshape children}}
\newcommand{\children}[2]{\mathcal{\childrensymbol}_{#2}(#1)}
\newcommand{\succsymbol}{\textsf{\upshape succ}}
\newcommand{\succstates}[2]{\succsymbol(#1, #2)}
\newcommand{\concat}{\#}
\newcommand{\RG}{\cite{Rintanen:Gretton:2013}\ }
\newcommand{\cupdot}{\charfusion[\mathbin]{\cup}{\cdot}}
\newcommand{\cuparrow}{\charfusion[\mathbin]{\cup}{{\raisebox{.5ex} {\smathcalebox{.4}{\ensuremath{\leftarrow}}}}}}
\newcommand{\bigcuparrow}{\charfusion[\mathop]{\bigcup}{\leftarrow}}
\newcommand{\finiteunion}{\cuparrow}
\newcommand{\finitemap}{\ensuremath{\sqsubseteq}}
\newcommand{\dgraph}{dependency graph}
\newcommand{\domain}[1]{{\sc #1}}
\newcommand{\solver}[1]{{\sc #1}}
\providecommand{\problem}[1]{\domain{#1}}
\renewcommand{\v}{\ensuremath{\mathit{v}}}
\providecommand{\vs}[1]{\domain{#1}}
\renewcommand{\vs}{\ensuremath{\mathit{vs}}}
\newcommand{\VS}{\ensuremath{\mathit{VS}}}
\newcommand{\Aut}{\ensuremath{\mathit{Aut}}}
\newcommand{\Inst}[2]{\ensuremath{\mathit{#2 \rightarrow_{#1} #1}}}
\newcommand{\Image}{\ensuremath{\mathit{Im}}}
\newcommand{\Img}[2]{\protect{#1 \llparenthesis #2 \rrparenthesis}}
\newcommand{\SND}{\ensuremath{\mathit{\pi_2}}}
\newcommand{\FST}{\ensuremath{\mathit{\pi_1}}}
\newcommand{\tvsal}{{\pitchfork}}
\newcommand{\nauty}{CGIP}

\newcommand{\pwinter}{\ensuremath{\mathit{\bigcap_{pw}}}}

\newcommand{\dom}{\ensuremath{\mathit{\mathcal{D}}}}
\newcommand{\codom}{\ensuremath{\mathcal{R}}}

\newcommand{\map}{\ensuremath{\mathit{map}}}
\newcommand{\BIJEC}{\ensuremath{\mathit{bij}}}
\newcommand{\INJ}{\ensuremath{\mathit{inj}}}
\newcommand{\funion}{\ensuremath{\overset{\leftarrow}{\cup}}}

\newcommand{\ifnew}{\mbox{\upshape \textsf{if}}}
\newcommand{\thennew}{\mbox{\upshape \textsf{then}}}
\newcommand{\elsenew}{\mbox{\upshape \textsf{else}}}
\newcommand{\choice}{\mbox{\upshape \textsf{ch}}}
\newcommand{\arbchoice}{\mbox{\upshape \textsf{arb}}}
\newcommand{\acycchoice}{\mbox{\upshape \textsf{ac}}}
\newcommand{\cycchoice}{\mbox{\upshape \textsf{cyc}}}
\newcommand{\filter}{\ensuremath{\mathit{FIL}}}
\newcommand{\probset}{\ensuremath{\boldsymbol \Pi}}
\newcommand{\probleq}{\ensuremath{\leq_\Pi}}
\newcommand{\CommVar}{\ensuremath{\bigcap_\v} }
\newcommand{\quotfun}{\ensuremath{ \mathcal{Q}}}

\newcommand{\apre}{\mbox{\upshape \textsf{pre}}}
\newcommand{\aeff}{\mbox{\upshape \textsf{eff}}}
\newcommand{\problist}{\ensuremath \probset}
\newcommand{\cat}{{\frown}}
\newcommand{\probproj}[2]{{#1}{\downharpoonright}^{#2}}
\newcommand{\preced}{\mathbin{\rotatebox[origin=c]{180}{\ensuremath{\rhd}}}}
\newcommand{\perm}{\ensuremath{\sigma}}
\newcommand{\invariant}[2]{\ensuremath{\mathit{inv({#1},{#2})}}}
\newcommand{\invstates}[1]{\ensuremath{\mathit{inv({#1})}}}
\newcommand{\probss}[1]{{\mathcal S}(#1)}
\newcommand{\parChildRel}[3]{\ensuremath{\negdep{#1}{#2}{#3}}}
\newcommand{\asessymbol}{\ensuremath{\mathbb{A}}}
\newcommand{\ases}[1]{{#1}^*}
\newcommand{\uniStates}{\ensuremath{\mathbb{U}}}
\newcommand{\recurrenceDiam}{\ensuremath{\mathit{rd}}}
\newcommand{\recurrenceAcycDiamfun}{\ensuremath{\mathit{{\mathfrak A}}}}
\newcommand{\recurrenceDiamfun}{\ensuremath{\mathit{\mathfrak R}}}
\newcommand{\traversalDiam}{\ensuremath{\mathit{td}}}
\newcommand{\traversalDiamfun}{\ensuremath{\mathit{\mathfrak T}}}
\newcommand{\isPrefix}[2]{\ensuremath{#1 \preceq #2}}
\providecommand{\path}{\ensuremath{\gamma}}
\newcommand{\aspath}{\ensuremath{\vec{\path}}}
\renewcommand{\path}{\ensuremath{\gamma}}
\newcommand{\n}{\textsf{\upshape n}}
\providecommand{\graph}{}
\providecommand{\cal}{}
\renewcommand{\cal}{}
\renewcommand{\graph}{{\cal G}}
\newcommand{\undirgraph}{{\cal G}}
\newcommand{\sset}{\ensuremath{\mbox{\upshape \textsf{ss}}}}
\renewcommand{\ss}{\ensuremath{\state s}}
\newcommand{\slist}{\ensuremath{\vec{\mbox{\upshape \textsf{ss}}}}}
\newcommand{\sll}{\ensuremath{\vec{\state}}}
\newcommand{\listset}{\mbox{\upshape \textsf{set}}}
\newcommand{\asset}{\ensuremath{\mathit{K}}}
\newcommand{\aslist}{\ensuremath{\mathit{\overset{\rightarrow}{\gamma}}}}
\newcommand{\head}{\mbox{\upshape \textsf{hd}}}
\renewcommand{\max}{\textsf{\upshape max}}
\newcommand{\argmax}{\textsf{\upshape argmax}}
\renewcommand{\min}{\textsf{\upshape min}}
\newcommand{\bool}{\mbox{\upshape \textsf{bool}}}
\newcommand{\last}{\mbox{\upshape \textsf{last}}}
\newcommand{\front}{\mbox{\upshape \textsf{front}}}
\newcommand{\rot}{\mbox{\upshape \textsf{rot}}}
\newcommand{\stuff}{\mbox{\upshape \textsf{intlv}}}
\newcommand{\tail}{\mbox{\upshape \textsf{tail}}}
\newcommand{\ngrtoas}{\ensuremath{\mathit{\as_{\graph_\mathbb{N}}}}}
\newcommand{\vsfun}{\mbox{\upshape \textsf{vs}}}
\newcommand{\inits}{\mbox{\upshape \textsf{init}}}
\newcommand{\satprecondas}{\mbox{\upshape \textsf{sat-pre}}}
\newcommand{\remcondlessact}{\mbox{\upshape \textsf{rem-condless}}}
\providecommand{\state}{}
\renewcommand{\state}{x}
\newcommand{\statea}{\ensuremath{x_1}}
\newcommand{\stateb}{\ensuremath{x_2}}
\newcommand{\statec}{\ensuremath{x_3}}
\newcommand{\fals}{\mbox{\upshape \textsf{F}}}
\newcommand{\indices}{\ensuremath{V}}
\newcommand{\edges}{\ensuremath{E}}
\newcommand{\vertices}{\ensuremath{V}}
\newcommand{\listtype}{\mbox{\upshape \textsf{list}}}
\newcommand{\settype}{\mbox{\upshape \textsf{set}}}
\newcommand{\acttype}{\mbox{\upshape \textsf{action}}}
\newcommand{\graphtype}{\mbox{\upshape \textsf{graph}}}
\newcommand{\projfun}[2]{\ensuremath{\Delta_{#1}^{#2}}}
\newcommand{\snapfun}[2]{\ensuremath{\Sigma_{#1}^{#2}}}
\newcommand{\RDfun}[1]{\ensuremath{{\mathcal R}_{#1}}}
\newcommand{\elldbound}[1]{\ensuremath{{\mathcal LS}_{#1}}}
\newcommand{\distinct}{\textsf{\upshape distinct}}
\newcommand{\ddistinct}{\mbox{\upshape \textsf{ddistinct}}}
\newcommand{\simple}{\mbox{\upshape \textsf{simple}}}


\newcommand{\reachable}[3]{\ensuremath{{#1}\rightsquigarrow{#3}}}


\newcommand{\Omit}[1]{}

\newcommand{\charles}[1]{\textcolor{red}{#1}}

\newcommand{\negreachable}[3]{\ensuremath{{#2}\not\rightsquigarrow{#3}}}
\newcommand{\wdiam}[2]{{#1}^{#2}}
\newcommand{\dsnapshot}[2]{\Delta_{#1}}
\newcommand{\ellsnapshot}[2]{{\mathcal L}_{#1}}

\newcommand{\snapshotsymbol}{|\kern-.7ex\raise.08ex\hbox{\scalebox{0.7}{$\bullet$}}}
\newcommand{\snapshot}[2]{\ensuremath{\mathrel{#1\snapshotsymbol_{#2}}}}
\newcommand{\vstype}{\texttt{\upshape VS}}
\newcommand{\vtype}{{\scriptsize \ensuremath{\dom(\delta)}}}
\newcommand{\Balgo}{{\mbox{\textsc{Hyb}}}}
\newcommand{\ssgraph}[1]{\graph_\ss}
\newcommand{\agree}{\textsf{\upshape agree}}
\newcommand{\ck}{\ensuremath{\texttt{ck}}}
\newcommand{\lk}{\ensuremath{\texttt{lk}}}
\newcommand{\gr}{\ensuremath{\texttt{gr}}}
\newcommand{\gk}{\ensuremath{\texttt{gk}}}
\newcommand{\CK}{\ensuremath{\texttt{CK}}}
\newcommand{\LK}{\ensuremath{\texttt{LK}}}
\newcommand{\GR}{\ensuremath{\texttt{GR}}}
\newcommand{\GK}{\ensuremath{\texttt{GK}}}
\newcommand{\safe}{\ensuremath{\texttt{s}}}

\newcommand{\derivname}{\ensuremath{\partial}}
\newcommand{\deriv}[3]{\ensuremath{\derivname(#1,#2,#3)}}
\newcommand{\derivabbrev}[3]{\ensuremath{{\partial(#1,#2)}}}
\newcommand{\subsetoracle}{\ensuremath{ \Omega}}
\newcommand{\Aalgo}{{\mbox{\textsc{Pur}}}}
\newcommand{\Sname}{\textsf{\upshape S}}
\newcommand{\Sbrace}[1]{\Sname\langle#1\rangle}
\newcommand{\SalgoName}{\Sname_{\textsf{\upshape max}}}
\newcommand{\Salgo}[1]{\SalgoName\langle#1\rangle}

\newcommand{\WLPname}{{\mbox{\textsc{wlp}}}}
\newcommand{\WLPbrace}[1]{\WLPname\langle#1\rangle}
\newcommand{\WLPalgoName}{\WLPname_{\textsf{\upshape max}}}
\newcommand{\WLP}[1]{\WLPalgoName\langle#1\rangle}

\newcommand{\Nname}{\ensuremath{\textsf{\upshape N}}}
\newcommand{\Nbrace}[1]{\Nname\langle#1\rangle}
\newcommand{\NalgoName}{\Nname{_{\textsf{\upshape sum}}}}
\newcommand{\Nalgobrace}[1]{\NalgoName\langle#1\rangle}

\newcommand{\acycNname}{\widehat{\textsf{\upshape N}}}
\newcommand{\acycNbrace}[1]{\acycNname\langle#1\rangle}
\newcommand{\acycNalgoName}{\acycNname{_{\textsf{\upshape sum}}}}
\newcommand{\acycNalgobrace}[1]{\acycNalgoName\langle#1\rangle}


\newcommand{\Mname}{\ensuremath{\textsf{\upshape M}}}
\newcommand{\Mbrace}[1]{\Mname\langle#1\rangle}
\newcommand{\MalgoName}{\Mname{_{\textsf{\upshape sum}}}}
\newcommand{\Malgobrace}[1]{\MalgoName\langle#1\rangle}
\newcommand{\cardinality}[1]{{\ensuremath{|#1|}}}
\newcommand{\length}[1]{\cardinality{#1}}
\newcommand{\basecasefun}{\ensuremath{b}}
\newcommand{\Basecasefun}{\ensuremath{\mathcal B}}



\newcommand{\vertexgen}{\ensuremath{u}}
\newcommand{\vertexa}{{\ensuremath{\vertexgen_1}}}
\newcommand{\vertexb}{{\ensuremath{\vertexgen_2}}}
\newcommand{\vertexc}{{\ensuremath{\vertexgen_3}}}
\newcommand{\vertexd}{{\ensuremath{\vertexgen_4}}}
\newcommand{\vertexsetgen}{\ensuremath{\mathit{us}}}
\newcommand{\vertexseta}{\vertexsetgen_1}
\newcommand{\vertexsetb}{\vertexsetgen_2}
\newcommand{\labelsymbol}{\ensuremath{l}}
\newcommand{\labelfun}{\ensuremath{\mathcal{L}}}
\newcommand{\DAG}{\ensuremath{A}}
\newcommand{\NalgoNameN}{{\ensuremath{\NalgoName_{\mathbb{N}}}}}
\newcommand{\NnameN}{\ensuremath{\Nname_\mathbb{N}}}
\newcommand{\replaceprojsinglename}{\raisebox{-0.3mm} {\scalebox{0.7}{\textpmhg{H}}}}
\newcommand{\replaceprojsingle}[3] {{ #2} \underset {#1} {\raisebox{-0.3mm} {\scalebox{0.7}{\textpmhg{H}}}}  #3}
\newcommand{\HOLreplaceprojsingle}[1]{\underset {#1} {\raisebox{-0.3mm} {\scalebox{0.7}{\textpmhg{H}}}}}

\newcommand{\lotus}{{\scalebox{0.6}{\includegraphics{lotus.pdf}}}}
\newcommand{\invlotus}{\mathbin{\rotatebox[origin=c]{180}{$\lotus$}}}
\newcommand{\clique}{\ensuremath{K}}
\newcommand{\partition}{\ensuremath{\vs_{1..n}}}
\newcommand{\partitiontype}{\ensuremath{\vstype_{1..n}}}
\newcommand{\vtxpartition}{\ensuremath{P}}

\newcommand{\traversalDiamAlgo}{{\mbox{\textsc{TravDiam}}}}
\newcommand{\prefix}{\textsf{\upshape pfx}}
\newcommand{\powerset}{\mathbb{P}}
\newcommand{\postfix}{\textsf{\upshape sfx}}
\newcommand{\dfunproj}{\ensuremath{{\mathfrak D}}}
\newcommand{\dfunsnap}{\ensuremath{{\textgoth D}}}
\newcommand{\ellfunproj}{\ensuremath{\mathfrak L}}
\newcommand{\ellfunsnap}{\ensuremath{\textgoth L}}
\newcommand{\cycle}{\ensuremath{C}}
\newcommand{\petal}{\ensuremath{\eta}}
\renewcommand{\prod}{\ensuremath{{{{{\mathlarger{\mathlarger {{\mathlarger {\Pi}}}}}}}}}}
\newcommand{\sccset}{{\ensuremath{SCC}}}
\newcommand{\scc}{{\ensuremath{scc}}}
\newcommand{\negate}[1]{\overline{#1}}
\newcommand{\setofsets}{\ensuremath{S}}
\newcommand{\group}{\ensuremath{\cal \Gamma}}
\newcommand{\neededvars}{{\cal N}}
\newcommand{\sspace}{\mbox{\upshape \textsf{sspc}}}
\newcommand{\tip}{\ensuremath{t}}
\newcommand{\vara}{\ensuremath{\v_1}}
\newcommand{\varb}{\ensuremath{\v_2}}
\newcommand{\varc}{\ensuremath{\v_3}}
\newcommand{\vard}{\ensuremath{\v_4}}
\newcommand{\vare}{\ensuremath{\v_5}}
\newcommand{\varf}{\ensuremath{\v_6}}
\newcommand{\varg}{\ensuremath{\v_7}}
\newcommand{\varh}{\ensuremath{\v_8}}
\newcommand{\vari}{\ensuremath{\v_9}}
\newcommand{\acta}{\ensuremath{\act_1}}
\newcommand{\actb}{\ensuremath{\act_2}}
\newcommand{\actc}{\ensuremath{\act_3}}
\newcommand{\actd}{\ensuremath{\act_4}}
\newcommand{\acte}{\ensuremath{\act_5}}
\newcommand{\actf}{\ensuremath{\act_6}}
\newcommand{\actg}{\ensuremath{\act_7}}
\newcommand{\acth}{\ensuremath{\act_8}}
\newcommand{\acti}{\ensuremath{\act_9}}




\tikzset{dots/.style args={#1per #2}{line cap=round,dash pattern=on 0 off #2/#1}}
\providecommand{\moham}[1]{\fbox{{\bf \@Mohammad: }#1}}
\newcommand{\TDbound}{{\mbox{\textsc{Arb}}}}
\newcommand{\expbound}{{\mbox{\textsc{Exp}}}}
\newcommand{\sasdom}{\expbound}
\newcommand{\cardfun}{\ensuremath{\mathbb{C}}}
\newcommand{\AGNa}{AGN1}
\newcommand{\AGNb}{AGN2}
\newcommand{\reset}{{\ensuremath{reset}}}
 \renewcommand{\childrensymbol}{\textsf{\upshape child}}
\renewcommand{\traversalDiamAlgo}{{\mbox{\textsc{TravD}}}}

\newcommand{\biere}{BiereCCZ99}

Many techniques for solving problems defined on transition systems, like SAT-based planning~\cite{kautz:selman:92} and bounded model checking~\cite{BiereCCZ99}, benefit from knowledge of {\em upper bounds} on the lengths of solution transition sequences, aka \emph{completeness thresholds}.
If $N$ is such a bound, and if a solution exists, then that solution need not comprise more than $N$ transitions.
In AI planning, upper bounds on plan lengths can be used as a completeness threshold, i.e. to prove a planning problem has no solution, and also it can be used to improve the ability of a SAT-based planner to find a solution.

\citeauthor{BiereCCZ99}~(\citeyear{BiereCCZ99}) identified the {\em diameter} ($d$) and the \emph{recurrence diameter} ($\recurrenceDiam$), which are topological properties of the state space, as completeness thresholds for bounded model-checking of safety and liveness properties, respectively.
$d$ is the longest shortest path between any two states.
$\recurrenceDiam$ is the length of the longest simple path in the state space, i.e. the length of the longest path that does not traverse any state more than once.
Both, $d$ and $\recurrenceDiam$, are upper bounds on the shortest plan's length, i.e. they are completeness thresholds for SAT-based planning.
In this work we devise new methods to compute $\recurrenceDiam$.






 
\section{Background}
\label{sec:defs}


A maplet, $\v \mapsto b$, maps a variable $\v$---i.e. a state-characterising proposition---to a Boolean $b$.
A state, $\state$, is a finite set of maplets.
We write $\dom(\state)$ to denote $\{\v \mid (\v \mapsto b) \in \state\}$, the domain of $\state$.
For states $\state_1$ and $\state_2$, the union, $\state_1 \uplus \state_2$, is defined as $\{\v \mapsto b \mid $ $\v \in \dom(\state_1) \cup \dom(\state_2) \wedge \ifnew\; \v \in \dom(\state_1) \;\thennew\; b = \state_1(\v) \;\elsenew\; b = \state_2(\v)\}$.
Note that the state $\state_1$ takes precedence.
An action is a pair of states, $(p,e)$, where $p$ represents the \emph{preconditions} and $e$ represents the \emph{effects}.
For action $\act=(p,e)$, the domain of that action is $\dom(\act)\equiv\dom(p) \cup \dom(e)$.
 When an action \act\ $(=(p,e))$ is executed at state $\state$, it produces a successor state $\exec{\state}{\act}$, formally defined as
$\exec{\state}{\act} = \ifnew\; p \subseteq \state\; \thennew\; e \uplus \state \;\elsenew\;\allowbreak \state$.
We lift execution to lists of actions $\as$, so $\exec{\state}{\as}$ denotes the state resulting from successively applying each action from $\as$ in turn, starting at $\state$, which corresponds to a path in the state space.
 A set of actions $\delta$ constitutes a factored transition system.
$\dom(\delta)$ denotes the domain of $\delta$, which is the union of the domains of all the actions it contains.
Let $\listset(\as)$ be the set of elements from $\as$.
The set of valid action sequences, $\ases{\delta}$, is $\{\as \mid\; \listset(\as) \subseteq \delta\}$.
The set of valid states, $\uniStates(\delta)$,  is $\{\state \mid \dom(\state) = \dom (\delta)\}$.
$\graph(\delta)$ denotes the state space of $\delta$, which is the set of pairs 
$\{(\state, \exec{\state}{\act})\mid \state \in \uniStates(\delta), \act \in \delta\}$, corresponding to different transitions in the state space of $\delta$.
 
\begin{mydef}
\label{def:diam}
The diameter, written $d(\delta)$, is the length of the longest shortest action sequence, formally
$\max \{\min\{\cardinality{\as'}\mid\exec{\state}{\as} = \exec{\state}{\as'}\wedge \as' \in \ases{\delta}\} \mid \state \in \uniStates(\delta)\wedge \as \in \ases{\delta}\}$
\end{mydef}
 Note that if there is a valid action sequence between any two states, then there is a valid action sequence between them which is not longer than $d$.
\begin{mydef}
\label{def:diamrd}
Let $\distinct(\state,\as)$ denote that all states traversed by executing $\as$ at $\state$ are distinct states.
The recurrence diameter is the length of the longest simple path in the state space, formally
$\max\{\cardinality{\as}\mid \state \in \uniStates(\delta) \wedge \as \in \ases{\delta} \wedge \distinct(\state,\as)\}$.
\end{mydef}
 Note that in general $\recurrenceDiam$ is an upper bound on $d$, and that it can be exponentially larger than $d$.

Currently, the compositional bounding method by~\citeauthor{icaps2017}~\citeyear{icaps2017} is the most successful in decomposing a given system into the smallest abstractions.
It decompose a given factored system using two kinds of abstraction: \emph{projection} and \emph{snapshotting}.
\newcommand{\Balgobrace}[1]{\ensuremath{\Balgo\langle{#1}\rangle}}
After the system is decomposed into abstractions, which we call \emph{base case systems}, a topological property, which we call the \emph{base case function}, of the state space of each of the base case systems is computed, and then its values for base case systems are composed to bound $d$ of the concrete system.
\citeauthor{abdulaziz:2019}~\citeyear{abdulaziz:2019} use the \emph{traversal diameter} as a base case function.
The traversal diameter is one less than the largest number of states that could be traversed by any path.
\begin{mydef}
\label{def:td}
Let $\sset(\state,\as)$ be the set of states traversed by executing $\as$ at $\state$.
Traversal diameter, written as $\traversalDiam(\delta)$, is
$\max \{\cardinality{\sset(\state,\as)} - 1\mid \state \in \uniStates(\delta) \wedge \as \in \ases{\delta}\}$.
\end{mydef}
  \section{Contributions}

Our first contribution is showing that $\recurrenceDiam$ can be exponentially smaller than $\traversalDiam$.
This gives rise to the possibility of substantial improvements to the bounds computed if we use $\recurrenceDiam$ as a base case function for compositional bounding, instead of $\traversalDiam$.



\citeauthor{BiereCCZ99}~\citeyear{BiereCCZ99} suggested the only method to compute $\recurrenceDiam$ of which we are aware.
They encode the question of whether a given number $k$ is $\recurrenceDiam$ of a given transition system as a SAT formula.
$\recurrenceDiam$ is found by querying a SAT-solver for different values of $k$, until the SAT-solver answers positively for one $k$.
The method terminates since $\recurrenceDiam$ cannot be larger than one less the number of states in the given transition system.
The size of their encoding grows linearly in $k^2$.
\newcommand{\encodinga}{\ensuremath{\phi_1}}
In our experiments we use an SMT solver to reason about encoding of $\recurrenceDiam$.
Firstly, let for a set $S$ of $n$-tuples and a predicate $P$ of arity $n$, $\bigwedge S. \; P(a_1,a_2\dots,a_n)$ denote the conjunction of $P(a_1,a_2\dots,a_n)$, for all $(a_1,a_2\dots,a_n)\in S$.
Note: $\bigwedge S. \; Q(a_1,a_2\dots,a_n)$ is only well defined if $S$ is finite.
Analogously, let $\bigvee$ denote a finite disjunction.
We reformulate the encoding of \citeauthor{BiereCCZ99} as follows.
\newcommand{\pathstate}{\ensuremath{y}}
{
\newcommand{\inspace}{\frak{G}}
\renewcommand{\underset}[2]{#2#1.\; }
\begin{myenc}
\label{enc:rdBiere}
Let for $\delta$, $\inspace(\state_1,\state_2)$ denote $(\state_1,\state_2)\in\graph(\delta)$.
For  $\delta$ and $0\leq k$, let $\encodinga'(\delta,k)$ denote the conjunction of the formulae
\begin{enumerate*}
  \item $\underset{\{(\statea,\stateb)\mid \inspace(\statea,\stateb)\}}{\bigwedge}\graph(\statea,\stateb)$, 
  \item $\underset{\{(\statea,\stateb)\mid\statea,\stateb\in\uniStates(\delta) \wedge \neg\inspace(\statea,\stateb)\}}{\bigwedge}\neg\graph(\statea,\stateb)$, and
  \item ${\underset{\{i\mid 1\leq i\leq k\}}{\bigwedge}} (\graph(\pathstate_i,\pathstate_{i+1}) \wedge \underset{\{j\mid i<j\leq k\}}{\bigwedge}$\\ $ \pathstate_i\neq\pathstate_j)$.
\end{enumerate*}
\end{myenc}
}

To use the above encoding to compute $\recurrenceDiam$ of a given system $\delta$, we iteratively query an SMT solver to check for the satisfiability of $\encodinga'(\delta,k)$ for different values of $k$, starting at 1, until the we have an unsatisfiable formula.
The smallest $k$ for which the formula is unsatisfiable is $\recurrenceDiam(\delta)$.

Observe that, to use Encoding~\ref{enc:rdBiere}, one has to build the entire state space as a part of building the encoding, i.e. one has to build the graph $\graph(\delta)$ and include it in the encoding.
This means that the worst-case complexity of computing $\recurrenceDiam$ using either one of those encodings is doubly-exponential.


To experimentally test this encoding, we use it as a base case function for the compositional algorithm by~\citeauthor{icaps2017}~\citeyear{icaps2017} instead of $\traversalDiam$.
We use Yices 2.6.1~\cite{DBLP:conf/cav/Dutertre14} as the SMT solver to prove the satisfiability or unsatisfiability of the resulting SMT formulae.
Our experiments show that Encoding~\ref{enc:rdBiere} is not practical when used as a base case function: bounds are only computed within the timeout for less than 0.1\% of our set of benchmarks.

Our second contribution is devising an encoding that performs better than Encoding~\ref{enc:rdBiere}.
We observe that the encodings by~\citeauthor{BiereCCZ99} does not exploit the compactness of factored representations of transition systems, and instead assume explicitly represented transition systems.
We devise a new encoding which exploits the factored representation in a way that is reminiscent to encodings used for SAT-based planning~\cite{kautz:selman:92}.
This avoids constructing the state space in an explicit form, whenever possible.
We devise a new encoding that avoids building the state space as a part of the encoding and, effectively, we let the SMT solver build as much of it during its search as needed.
\newcommand{\encodingb}{\ensuremath{\phi_2}}
{
\renewcommand{\underset}[2]{#2#1.\; }
\begin{myenc}
\label{enc:rdnew}
For a state $\state$, let $\state_i$ denote the formula $(\underset{\{\v\mid \v\in\state\}}{\bigwedge}\v_j)\wedge(\underset{\{\v\mid \neg\v\in\state\}}{\bigwedge}\neg\v_j)$.
For $\delta$ and $0\leq k$, let $\encodingb(\delta,k)$ denote the conjunction of the formulae
\begin{enumerate}
  \item $\underset{\{i\mid 1\leq i \leq k\}}{\bigwedge}\act_i\rightarrow\apre(\act)_i\wedge\aeff(\act)_{i+1}\wedge(\underset{\{\v\mid \v\in\dom(\delta)\setminus\dom(\act)\}} {\bigwedge}\v_i\leftrightarrow\v_{i+1})$, 
  \item ${\underset{\{i\mid 1\leq i \leq k\}}{\bigwedge}}\underset{\{\act\mid \act\in\delta\}}{\bigvee}\act_i$, 
  \item ${\underset{\{i\mid 1\leq i \leq k\}}{\bigwedge}}\underset{\{(\act,\act')\mid \act,\act'\in\delta\}}{\bigwedge}\neg\act_i \vee \neg\act'_i$
  \item $\underset{\{(i, j) \mid 1\leq i < j \leq k\}}{\bigwedge}\underset{\{\v\mid v\in\dom(\delta)\}}{\bigvee}\v_i\neq\v_j$
\end{enumerate}
\end{myenc}
}



We experimentally test the new encoding as a base case function for the algorithm.
We note two observations.
Encoding~\ref{enc:rdnew} performs much better than Encoding~\ref{enc:rdBiere} in practice since our new encoding is represented in terms of the factored representation of the system, while Encoding~\ref{enc:rdBiere} represents the system as an explicitly represented state space.
This leads to exponentially smaller formulae: Encoding~\ref{enc:rdnew} grows quadratically with the size of the given factored system, while Encoding~\ref{enc:rdBiere} grows quadratically in the size of the state space, which can be exponentially larger than the given factored system.
However, both encodings have the same worst-case doubly exponential running time.

Secondly, when $\recurrenceDiam$ is the base case function the bounds computed are much tighter than those computed when $\traversalDiam$ as a base case function.
This agrees with the theoretical prediction.
In particular, in the domains TPP, ParcPrinter, NoMystery, Logistics, OpenStacks, Woodworking, Satellites, Scanalyzer, Hyp and NewOpen (a compiled Qualitative Preference rovers domain), we have between two orders of magnitude and 50\% smaller bounds when $\recurrenceDiam$ is used as a base case function compared to $\traversalDiam$.
Also, the domain Visitall has twice as many problems whose bounds are less than $10^9$ when $\recurrenceDiam$ is used instead of $\traversalDiam$.
In contrast, equal bounds are found using $\recurrenceDiam$ and $\traversalDiam$ as base case functions in Zeno.

\paragraph{Using the bounds for SAT-based planning}
The coverage of {\sc Mp} increases if we use as horizons the bounds computed when $\recurrenceDiam$ is the base case, compared to when using $\traversalDiam$ as a base case function.
Compared to $\traversalDiam$, $\recurrenceDiam$ increases the coverage by 187 solvable problems, overall.
Those problems come from the domains: \domain{newopen} (104 problems), NoMystery (23 problems), Floortile (25 problems), rover (15 problems), satellite (10 problems), TPP (8 problems), BlocksWorld (1 problem), and ParcPrinter (1 problem).

  
\bibliography{paper}
\bibliographystyle{aaai}

\end{document}
