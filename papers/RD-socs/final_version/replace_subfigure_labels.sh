#!/bin/bash

num_asses=`wc -l fig_asses.txt | awk '{print $1}'`
label_num_map=`cat fig_asses.txt`
for i in `seq 0 $num_asses`;
do
  label=`cut -d' ' -f$(($((2 * $i)) + 1)) <<<$label_num_map`
  replacement=`cut -d' ' -f$(($((2 * $i)) + 2)) <<<$label_num_map`
  echo $i $label ---- $replacement
  sed -i 's/\\ref{'"$label"'}/'"$replacement"'/g' paper.tex
done  
