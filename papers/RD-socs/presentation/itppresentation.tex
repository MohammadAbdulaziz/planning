% Copyright 2007 by Till Tantau
%
% This file may be distributed and/or modified
%
% 1. under the LaTeX Project Public License and/or
% 2. under the GNU Public License.
%
% See the file doc/licenses/LICENSE for more details.



\documentclass[t]{beamer}

%
% DO NOT USE THIS FILE AS A TEMPLATE FOR YOUR OWN TALKS¡!!
%
% Use a file in the directory solutions instead.
% They are much better suited.
%


% Setup appearance:
\useoutertheme{infolines}

%\usetheme{Darmstadt}
%\usetheme[sidebar=false]{NICTA}
\setbeamertemplate{navigation symbols}{}
%% \usefonttheme[onlylarge]{structurebold}
%% \setbeamerfont*{frametitle}{size=\normalsize,series=\bfseries}
%% \setbeamertemplate{navigation symbols}{}


% Standard packages
\usepackage[ruled,noline,noalgohanging]{algorithm2e}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}

\usepackage[makeroom]{cancel}

\usepackage{tikz}
\usetikzlibrary{arrows,positioning,decorations.markings}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{tabularx}
\captionsetup{compatibility=false}

\newtheorem{mylem}{Lemma}
\newtheorem{mythm}{HOL4 Theorem}
\newtheorem{mydef}{HOL4 Definition}
\newtheorem{myeg}{Example}

%\usepackage{holtexbasic}

%% \usepackage{alltt}
%% \usepackage{enumitem}
\usepackage[Euler]{upgreek}
\usepackage{xcolor,colortbl}
\renewcommand{\Pi}{\Uppi}

\newcommand{\vs}{\ensuremath{\mathit{vs}}}
%\newcommand{\subscriptsublist}{\preceq}

\newcommand{\pp}[1]{\text{\em #1}}
\newcommand{\lopt}{\textcolor[gray]{0.2}{[}}
\newcommand{\ropt}{\textcolor[gray]{0.2}{]}}
\usepackage[llparenthesis,rrparenthesis]{stmaryrd}
\newcolumntype{r}{>{\columncolor{red}}c}
\newcolumntype{g}{>{\columncolor{green}}c}
\input{../../latexIncludes/macros.tex}
%% \renewcommand{\HOLConst}[1]{\textsf{#1}}
%% \renewcommand{\HOLTyOp}[1]{\mbox{\fontencoding{T1}\fontfamily{bch}\fontseries{m}\fontshape{it}\fontsize{9.5}{10}\selectfont #1}}
%% \newcommand{\etc}{\textit{etc.}}
%% \newcommand{\ie}{\textit{i.e.}}

%% \usepackage{color}
%% \newcommand{\charles}[1]{\textcolor{red}{Charles: #1}}
%% \newcommand{\michael}[1]{\textcolor{blue}{Michael: #1}}
%% \newcommand{\calA}{\ensuremath{\mathbf{\cal A}}}

% Author, Title, etc.
\title[Computing Plan-Length Bounds Using Lengths of Longest Paths]
% (optional, use only with long paper titles)
{Computing Plan-Length Bounds Using Lengths of Longest Paths}
\author[Abdulaziz and Berger]{Mohammad Abdulaziz and Dominik Berger
\\ Technical University of Munich}

\tikzset{
  textnode/.style={},
  varnode2/.style={draw},
  varnode2green/.style={draw,fill=green},
  varnode2red/.style={draw,fill=red},
  varnode2blue/.style={draw,fill=blue},
  varnode/.style={rectangle,draw},
  varnodegreen/.style={rectangle,draw,fill=green},
  varnodered/.style={rectangle,draw,fill=red},
  varnodeblue/.style={rectangle,draw,fill=blue},
  varnodepurple/.style={rectangle,draw,fill=purple},
  varnodeyellow/.style={rectangle,draw,fill=yellow},
  varnodeempty/.style={inner sep=0pt,fill},
  line/.style={=stealth,thick,fill=red},
  ourarrow/.style={>=stealth,thick,fill=red}
}


\setbeamertemplate{itemize/enumerate body begin}{\large}
\setbeamertemplate{itemize/enumerate subbody begin}{\large}
\setbeamertemplate{itemize/enumerate subsubbody begin}{\large}
\setbeamertemplate{itemize/enumerate subsubsubbody begin}{\large}
% The main document

\date{}
\begin{document}


\begin{frame}[t]
  \titlepage
\end{frame}


\begin{frame}[t]{Recurrence Diameter}
\begin{itemize}
  \item Given a digraph
  \begin{itemize}
    \item what is the length of the longest path that doesn't traverse the same state twice?
  \end{itemize}
  \item For an explicitly represented digraph, this problem is NP-Hard [Pardalos and Migdalas 2004]
  \item We explore how to compute that for planning problem state spaces
  \item Why:
  \begin{itemize}
    \item to compute upper-bounds on plan-length
    \item important for SAT-based planning
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[t]{Our Contribution}
\begin{itemize}
  \item Challenge:
  \begin{itemize}
    \item NEXP-Hard for succinct digraphs like STRIPS state spaces [Papadimitriou and Yannakakis 1985]
    \item i.e. doubly-exponential worst case running time
  \end{itemize}
  \item Solutions:
  \begin{itemize}
    \item better encoding into SMT
    \begin{itemize}
      \item previous encoding by Biere et al. [1999] builds the entire state space
      \item our encoding directly uses the factored representation of the state space
      \item potentially exponentially smaller formulae
    \end{itemize}
    \item only computing it for small abstractions of state spaces
    \begin{itemize}
      \item effective combination of recurrence diameter and other bounds
    \end{itemize}
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Experimental Evaluation}

\begin{figure}[h]
\centering
\begin{minipage}[b]{0.49\textwidth}
\centering
        \includegraphics[width=1\textwidth,height=0.6\textwidth]{log-labelled-Balgo-RD-TDgt2-vs-Balgo-TD_ARB-bounds.png}
\end{minipage}
\begin{minipage}[b]{0.49\textwidth}
\centering
        \includegraphics[width=1\textwidth,height=0.6\textwidth]{log-labelled-Mp_noshort-0-plan-withbound-Balgo-RD-TDgt2-vs-Mp_noshort-0-plan-withbound-Balgo-TD_ARB-total-runtimes.png}
\end{minipage}
\end{figure}
\begin{itemize}
\item Compared to bounds computed by Abdulaziz [2019]
\begin{itemize}
  \item bounds are much tighter, but take longer to be computed
  \item overall the tightness of the bounds pays off in terms of SAT-based planning coverage
\end{itemize}
\end{itemize}
\end{frame}

\end{document}
