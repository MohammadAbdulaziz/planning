;; rover::roverprob2312, problem open #1
;; selected constraints: {e4, o5}
;; value = 19.01828, penalty = 182.93034
(define (domain rover)
  (:types block)
 (:predicates (at_rover0_waypoint0) (at_rover0_waypoint1) (at_rover0_waypoint2) (at_rover0_waypoint3) (at_rover0_waypoint5) (at_rover0_waypoint4) (at_rover1_waypoint0) (at_rover1_waypoint1) (at_rover1_waypoint4) (at_rover1_waypoint2) (at_rover1_waypoint5) (at_rover1_waypoint3) (empty_rover1store) (full_rover1store) (at_soil_sample_waypoint1) (have_soil_analysis_rover1_waypoint1) (at_soil_sample_waypoint2) (have_soil_analysis_rover1_waypoint2) (at_soil_sample_waypoint3) (have_soil_analysis_rover1_waypoint3) (at_soil_sample_waypoint4) (have_soil_analysis_rover1_waypoint4) (at_soil_sample_waypoint5) (have_soil_analysis_rover1_waypoint5) (at_rock_sample_waypoint0) (empty_rover0store) (full_rover0store) (have_rock_analysis_rover0_waypoint0) (at_rock_sample_waypoint2) (have_rock_analysis_rover0_waypoint2) (at_rock_sample_waypoint3) (have_rock_analysis_rover0_waypoint3) (at_rock_sample_waypoint5) (have_rock_analysis_rover0_waypoint5) (calibrated_camera0_rover0) (calibrated_camera1_rover0) (calibrated_camera2_rover1) (have_image_rover0_objective0_colour) (have_image_rover0_objective0_low_res) (have_image_rover0_objective1_colour) (have_image_rover0_objective1_low_res) (have_image_rover1_objective0_colour) (have_image_rover1_objective0_high_res) (have_image_rover1_objective1_colour) (have_image_rover1_objective1_high_res) (communicated_soil_data_waypoint1) (communicated_soil_data_waypoint2) (communicated_soil_data_waypoint3) (communicated_soil_data_waypoint4) (communicated_soil_data_waypoint5) (communicated_rock_data_waypoint0) (communicated_rock_data_waypoint2) (communicated_rock_data_waypoint3) (communicated_rock_data_waypoint5) (communicated_image_data_objective0_colour) (communicated_image_data_objective0_high_res) (communicated_image_data_objective0_low_res) (communicated_image_data_objective1_colour) (communicated_image_data_objective1_high_res) (communicated_image_data_objective1_low_res) (ok_e4) (first_o5))
 (:action navigate_rover0_waypoint0_waypoint1
  :parameters () :precondition (at_rover0_waypoint0)
  :effect (and (at_rover0_waypoint1) (not (at_rover0_waypoint0))))
 (:action navigate_rover0_waypoint0_waypoint2
  :parameters () :precondition (at_rover0_waypoint0)
  :effect (and (at_rover0_waypoint2) (not (at_rover0_waypoint0))))
 (:action navigate_rover0_waypoint0_waypoint3
  :parameters () :precondition (at_rover0_waypoint0)
  :effect (and (at_rover0_waypoint3) (not (at_rover0_waypoint0))))
 (:action navigate_rover0_waypoint0_waypoint5
  :parameters () :precondition (at_rover0_waypoint0)
  :effect (and (at_rover0_waypoint5) (not (at_rover0_waypoint0))))
 (:action navigate_rover0_waypoint1_waypoint0
  :parameters () :precondition (at_rover0_waypoint1)
  :effect (and (at_rover0_waypoint0) (not (at_rover0_waypoint1))))
 (:action navigate_rover0_waypoint1_waypoint4
  :parameters () :precondition (at_rover0_waypoint1)
  :effect (and (at_rover0_waypoint4) (not (at_rover0_waypoint1))))
 (:action navigate_rover0_waypoint2_waypoint0
  :parameters () :precondition (at_rover0_waypoint2)
  :effect (and (at_rover0_waypoint0) (not (at_rover0_waypoint2))))
 (:action navigate_rover0_waypoint3_waypoint0
  :parameters () :precondition (at_rover0_waypoint3)
  :effect (and (at_rover0_waypoint0) (not (at_rover0_waypoint3))))
 (:action navigate_rover0_waypoint4_waypoint1
  :parameters () :precondition (at_rover0_waypoint4)
  :effect (and (at_rover0_waypoint1) (not (at_rover0_waypoint4))))
 (:action navigate_rover0_waypoint5_waypoint0
  :parameters () :precondition (at_rover0_waypoint5)
  :effect (and (at_rover0_waypoint0) (not (at_rover0_waypoint5))))
 (:action navigate_rover1_waypoint0_waypoint1
  :parameters () :precondition (at_rover1_waypoint0)
  :effect (and (at_rover1_waypoint1) (not (at_rover1_waypoint0))))
 (:action navigate_rover1_waypoint1_waypoint0
  :parameters () :precondition (at_rover1_waypoint1)
  :effect (and (at_rover1_waypoint0) (not (at_rover1_waypoint1))))
 (:action navigate_rover1_waypoint1_waypoint4
  :parameters () :precondition (at_rover1_waypoint1)
  :effect (and (at_rover1_waypoint4) (not (at_rover1_waypoint1))))
 (:action navigate_rover1_waypoint2_waypoint5
  :parameters () :precondition (at_rover1_waypoint2)
  :effect (and (at_rover1_waypoint5) (not (at_rover1_waypoint2))))
 (:action navigate_rover1_waypoint3_waypoint4
  :parameters () :precondition (at_rover1_waypoint3)
  :effect (and (at_rover1_waypoint4) (not (at_rover1_waypoint3))))
 (:action navigate_rover1_waypoint4_waypoint1
  :parameters () :precondition (at_rover1_waypoint4)
  :effect (and (at_rover1_waypoint1) (not (at_rover1_waypoint4))))
 (:action navigate_rover1_waypoint4_waypoint3
  :parameters () :precondition (at_rover1_waypoint4)
  :effect (and (at_rover1_waypoint3) (not (at_rover1_waypoint4))))
 (:action navigate_rover1_waypoint4_waypoint5
  :parameters () :precondition (at_rover1_waypoint4)
  :effect (and (at_rover1_waypoint5) (not (at_rover1_waypoint4))))
 (:action navigate_rover1_waypoint5_waypoint2
  :parameters () :precondition (at_rover1_waypoint5)
  :effect (and (at_rover1_waypoint2) (not (at_rover1_waypoint5))))
 (:action navigate_rover1_waypoint5_waypoint4
  :parameters () :precondition (at_rover1_waypoint5)
  :effect (and (at_rover1_waypoint4) (not (at_rover1_waypoint5))))
 (:action sample_soil_rover1_rover1store_waypoint1
  :parameters () :precondition (and (at_rover1_waypoint1) (empty_rover1store) (at_soil_sample_waypoint1))
  :effect (and (full_rover1store) (have_soil_analysis_rover1_waypoint1) (not (empty_rover1store)) (not (at_soil_sample_waypoint1))))
 (:action sample_soil_rover1_rover1store_waypoint2
  :parameters () :precondition (and (at_rover1_waypoint2) (empty_rover1store) (at_soil_sample_waypoint2))
  :effect (and (full_rover1store) (have_soil_analysis_rover1_waypoint2) (not (empty_rover1store)) (not (at_soil_sample_waypoint2))))
 (:action sample_soil_rover1_rover1store_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint3) (empty_rover1store) (at_soil_sample_waypoint3))
  :effect (and (full_rover1store) (have_soil_analysis_rover1_waypoint3) (not (empty_rover1store)) (not (at_soil_sample_waypoint3))))
 (:action sample_soil_rover1_rover1store_waypoint4
  :parameters () :precondition (and (at_rover1_waypoint4) (empty_rover1store) (at_soil_sample_waypoint4))
  :effect (and (full_rover1store) (have_soil_analysis_rover1_waypoint4) (not (empty_rover1store)) (not (at_soil_sample_waypoint4))))
 (:action sample_soil_rover1_rover1store_waypoint5
  :parameters () :precondition (and (at_rover1_waypoint5) (empty_rover1store) (at_soil_sample_waypoint5))
  :effect (and (full_rover1store) (have_soil_analysis_rover1_waypoint5) (not (empty_rover1store)) (not (at_soil_sample_waypoint5))))
 (:action sample_rock_rover0_rover0store_waypoint0
  :parameters () :precondition (and (at_rover0_waypoint0) (at_rock_sample_waypoint0) (empty_rover0store))
  :effect (and (full_rover0store) (have_rock_analysis_rover0_waypoint0) (not (at_rock_sample_waypoint0)) (not (empty_rover0store))))
 (:action sample_rock_rover0_rover0store_waypoint2
  :parameters () :precondition (and (at_rover0_waypoint2) (empty_rover0store) (at_rock_sample_waypoint2))
  :effect (and (full_rover0store) (have_rock_analysis_rover0_waypoint2) (not (empty_rover0store)) (not (at_rock_sample_waypoint2))))
 (:action sample_rock_rover0_rover0store_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint3) (empty_rover0store) (at_rock_sample_waypoint3))
  :effect (and (full_rover0store) (have_rock_analysis_rover0_waypoint3) (not (empty_rover0store)) (not (at_rock_sample_waypoint3))))
 (:action sample_rock_rover0_rover0store_waypoint5
  :parameters () :precondition (and (at_rover0_waypoint5) (empty_rover0store) (at_rock_sample_waypoint5))
  :effect (and (full_rover0store) (have_rock_analysis_rover0_waypoint5) (not (empty_rover0store)) (not (at_rock_sample_waypoint5))))
 (:action drop_rover0_rover0store
  :parameters () :precondition (full_rover0store)
  :effect (and (empty_rover0store) (not (full_rover0store))))
 (:action drop_rover1_rover1store
  :parameters () :precondition (full_rover1store)
  :effect (and (empty_rover1store) (not (full_rover1store))))
 (:action calibrate_rover0_camera0_objective0_waypoint0
  :parameters () :precondition (and (at_rover0_waypoint0) (first_o5))
  :effect (calibrated_camera0_rover0))
 (:action calibrate_rover0_camera0_objective0_waypoint1
  :parameters () :precondition (and (at_rover0_waypoint1) (first_o5))
  :effect (calibrated_camera0_rover0))
 (:action calibrate_rover0_camera0_objective0_waypoint2
  :parameters () :precondition (and (at_rover0_waypoint2) (first_o5))
  :effect (calibrated_camera0_rover0))
 (:action calibrate_rover0_camera0_objective0_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint3) (first_o5))
  :effect (calibrated_camera0_rover0))
 (:action calibrate_rover0_camera0_objective0_waypoint4
  :parameters () :precondition (and (at_rover0_waypoint4) (first_o5))
  :effect (calibrated_camera0_rover0))
 (:action calibrate_rover0_camera0_objective0_waypoint5
  :parameters () :precondition (and (at_rover0_waypoint5) (first_o5))
  :effect (calibrated_camera0_rover0))
 (:action calibrate_rover0_camera1_objective1_waypoint0
  :parameters () :precondition (at_rover0_waypoint0)
  :effect (calibrated_camera1_rover0))
 (:action calibrate_rover0_camera1_objective1_waypoint1
  :parameters () :precondition (at_rover0_waypoint1)
  :effect (calibrated_camera1_rover0))
 (:action calibrate_rover1_camera2_objective0_waypoint0
  :parameters () :precondition (at_rover1_waypoint0)
  :effect (calibrated_camera2_rover1))
 (:action calibrate_rover1_camera2_objective0_waypoint1
  :parameters () :precondition (at_rover1_waypoint1)
  :effect (calibrated_camera2_rover1))
 (:action calibrate_rover1_camera2_objective0_waypoint2
  :parameters () :precondition (at_rover1_waypoint2)
  :effect (calibrated_camera2_rover1))
 (:action calibrate_rover1_camera2_objective0_waypoint3
  :parameters () :precondition (at_rover1_waypoint3)
  :effect (calibrated_camera2_rover1))
 (:action calibrate_rover1_camera2_objective0_waypoint4
  :parameters () :precondition (at_rover1_waypoint4)
  :effect (calibrated_camera2_rover1))
 (:action calibrate_rover1_camera2_objective0_waypoint5
  :parameters () :precondition (at_rover1_waypoint5)
  :effect (calibrated_camera2_rover1))
 (:action take_image_rover0_waypoint0_objective0_camera0_colour
  :parameters () :precondition (and (at_rover0_waypoint0) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective0_colour) (ok_e4) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint0_objective0_camera0_low_res
  :parameters () :precondition (and (at_rover0_waypoint0) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective0_low_res) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint0_objective0_camera1_colour
  :parameters () :precondition (and (at_rover0_waypoint0) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective0_colour) (ok_e4) (not (calibrated_camera1_rover0))))
 (:action take_image_rover0_waypoint0_objective0_camera1_low_res
  :parameters () :precondition (and (at_rover0_waypoint0) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective0_low_res) (not (calibrated_camera1_rover0))))
 (:action take_image_rover0_waypoint0_objective1_camera0_colour
  :parameters () :precondition (and (at_rover0_waypoint0) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective1_colour) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint0_objective1_camera0_low_res
  :parameters () :precondition (and (at_rover0_waypoint0) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective1_low_res) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint0_objective1_camera1_colour
  :parameters () :precondition (and (at_rover0_waypoint0) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective1_colour) (not (calibrated_camera1_rover0))))
 (:action take_image_rover0_waypoint0_objective1_camera1_low_res
  :parameters () :precondition (and (at_rover0_waypoint0) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective1_low_res) (not (calibrated_camera1_rover0))))
 (:action take_image_rover0_waypoint1_objective0_camera0_colour
  :parameters () :precondition (and (at_rover0_waypoint1) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective0_colour) (ok_e4) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint1_objective0_camera0_low_res
  :parameters () :precondition (and (at_rover0_waypoint1) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective0_low_res) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint1_objective0_camera1_colour
  :parameters () :precondition (and (at_rover0_waypoint1) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective0_colour) (ok_e4) (not (calibrated_camera1_rover0))))
 (:action take_image_rover0_waypoint1_objective0_camera1_low_res
  :parameters () :precondition (and (at_rover0_waypoint1) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective0_low_res) (not (calibrated_camera1_rover0))))
 (:action take_image_rover0_waypoint1_objective1_camera0_colour
  :parameters () :precondition (and (at_rover0_waypoint1) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective1_colour) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint1_objective1_camera0_low_res
  :parameters () :precondition (and (at_rover0_waypoint1) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective1_low_res) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint1_objective1_camera1_colour
  :parameters () :precondition (and (at_rover0_waypoint1) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective1_colour) (not (calibrated_camera1_rover0))))
 (:action take_image_rover0_waypoint1_objective1_camera1_low_res
  :parameters () :precondition (and (at_rover0_waypoint1) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective1_low_res) (not (calibrated_camera1_rover0))))
 (:action take_image_rover0_waypoint2_objective0_camera0_colour
  :parameters () :precondition (and (at_rover0_waypoint2) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective0_colour) (ok_e4) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint2_objective0_camera0_low_res
  :parameters () :precondition (and (at_rover0_waypoint2) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective0_low_res) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint2_objective0_camera1_colour
  :parameters () :precondition (and (at_rover0_waypoint2) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective0_colour) (ok_e4) (not (calibrated_camera1_rover0))))
 (:action take_image_rover0_waypoint2_objective0_camera1_low_res
  :parameters () :precondition (and (at_rover0_waypoint2) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective0_low_res) (not (calibrated_camera1_rover0))))
 (:action take_image_rover0_waypoint3_objective0_camera0_colour
  :parameters () :precondition (and (at_rover0_waypoint3) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective0_colour) (ok_e4) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint3_objective0_camera0_low_res
  :parameters () :precondition (and (at_rover0_waypoint3) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective0_low_res) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint3_objective0_camera1_colour
  :parameters () :precondition (and (at_rover0_waypoint3) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective0_colour) (ok_e4) (not (calibrated_camera1_rover0))))
 (:action take_image_rover0_waypoint3_objective0_camera1_low_res
  :parameters () :precondition (and (at_rover0_waypoint3) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective0_low_res) (not (calibrated_camera1_rover0))))
 (:action take_image_rover0_waypoint4_objective0_camera0_colour
  :parameters () :precondition (and (at_rover0_waypoint4) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective0_colour) (ok_e4) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint4_objective0_camera0_low_res
  :parameters () :precondition (and (at_rover0_waypoint4) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective0_low_res) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint4_objective0_camera1_colour
  :parameters () :precondition (and (at_rover0_waypoint4) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective0_colour) (ok_e4) (not (calibrated_camera1_rover0))))
 (:action take_image_rover0_waypoint4_objective0_camera1_low_res
  :parameters () :precondition (and (at_rover0_waypoint4) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective0_low_res) (not (calibrated_camera1_rover0))))
 (:action take_image_rover0_waypoint5_objective0_camera0_colour
  :parameters () :precondition (and (at_rover0_waypoint5) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective0_colour) (ok_e4) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint5_objective0_camera0_low_res
  :parameters () :precondition (and (at_rover0_waypoint5) (calibrated_camera0_rover0))
  :effect (and (have_image_rover0_objective0_low_res) (not (calibrated_camera0_rover0)) (not (first_o5))))
 (:action take_image_rover0_waypoint5_objective0_camera1_colour
  :parameters () :precondition (and (at_rover0_waypoint5) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective0_colour) (ok_e4) (not (calibrated_camera1_rover0))))
 (:action take_image_rover0_waypoint5_objective0_camera1_low_res
  :parameters () :precondition (and (at_rover0_waypoint5) (calibrated_camera1_rover0))
  :effect (and (have_image_rover0_objective0_low_res) (not (calibrated_camera1_rover0))))
 (:action take_image_rover1_waypoint0_objective0_camera2_colour
  :parameters () :precondition (and (at_rover1_waypoint0) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective0_colour) (not (calibrated_camera2_rover1))))
 (:action take_image_rover1_waypoint0_objective0_camera2_high_res
  :parameters () :precondition (and (at_rover1_waypoint0) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective0_high_res) (not (calibrated_camera2_rover1))))
 (:action take_image_rover1_waypoint0_objective1_camera2_colour
  :parameters () :precondition (and (at_rover1_waypoint0) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective1_colour) (not (calibrated_camera2_rover1))))
 (:action take_image_rover1_waypoint0_objective1_camera2_high_res
  :parameters () :precondition (and (at_rover1_waypoint0) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective1_high_res) (not (calibrated_camera2_rover1))))
 (:action take_image_rover1_waypoint1_objective0_camera2_colour
  :parameters () :precondition (and (at_rover1_waypoint1) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective0_colour) (not (calibrated_camera2_rover1))))
 (:action take_image_rover1_waypoint1_objective0_camera2_high_res
  :parameters () :precondition (and (at_rover1_waypoint1) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective0_high_res) (not (calibrated_camera2_rover1))))
 (:action take_image_rover1_waypoint1_objective1_camera2_colour
  :parameters () :precondition (and (at_rover1_waypoint1) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective1_colour) (not (calibrated_camera2_rover1))))
 (:action take_image_rover1_waypoint1_objective1_camera2_high_res
  :parameters () :precondition (and (at_rover1_waypoint1) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective1_high_res) (not (calibrated_camera2_rover1))))
 (:action take_image_rover1_waypoint2_objective0_camera2_colour
  :parameters () :precondition (and (at_rover1_waypoint2) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective0_colour) (not (calibrated_camera2_rover1))))
 (:action take_image_rover1_waypoint2_objective0_camera2_high_res
  :parameters () :precondition (and (at_rover1_waypoint2) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective0_high_res) (not (calibrated_camera2_rover1))))
 (:action take_image_rover1_waypoint3_objective0_camera2_colour
  :parameters () :precondition (and (at_rover1_waypoint3) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective0_colour) (not (calibrated_camera2_rover1))))
 (:action take_image_rover1_waypoint3_objective0_camera2_high_res
  :parameters () :precondition (and (at_rover1_waypoint3) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective0_high_res) (not (calibrated_camera2_rover1))))
 (:action take_image_rover1_waypoint4_objective0_camera2_colour
  :parameters () :precondition (and (at_rover1_waypoint4) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective0_colour) (not (calibrated_camera2_rover1))))
 (:action take_image_rover1_waypoint4_objective0_camera2_high_res
  :parameters () :precondition (and (at_rover1_waypoint4) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective0_high_res) (not (calibrated_camera2_rover1))))
 (:action take_image_rover1_waypoint5_objective0_camera2_colour
  :parameters () :precondition (and (at_rover1_waypoint5) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective0_colour) (not (calibrated_camera2_rover1))))
 (:action take_image_rover1_waypoint5_objective0_camera2_high_res
  :parameters () :precondition (and (at_rover1_waypoint5) (calibrated_camera2_rover1))
  :effect (and (have_image_rover1_objective0_high_res) (not (calibrated_camera2_rover1))))
 (:action communicate_soil_data_rover1_general_waypoint1_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint0) (have_soil_analysis_rover1_waypoint1))
  :effect (communicated_soil_data_waypoint1))
 (:action communicate_soil_data_rover1_general_waypoint1_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint1) (have_soil_analysis_rover1_waypoint1))
  :effect (communicated_soil_data_waypoint1))
 (:action communicate_soil_data_rover1_general_waypoint1_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint2) (have_soil_analysis_rover1_waypoint1))
  :effect (communicated_soil_data_waypoint1))
 (:action communicate_soil_data_rover1_general_waypoint1_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint4) (have_soil_analysis_rover1_waypoint1))
  :effect (communicated_soil_data_waypoint1))
 (:action communicate_soil_data_rover1_general_waypoint1_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint5) (have_soil_analysis_rover1_waypoint1))
  :effect (communicated_soil_data_waypoint1))
 (:action communicate_soil_data_rover1_general_waypoint2_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint0) (have_soil_analysis_rover1_waypoint2))
  :effect (communicated_soil_data_waypoint2))
 (:action communicate_soil_data_rover1_general_waypoint2_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint1) (have_soil_analysis_rover1_waypoint2))
  :effect (communicated_soil_data_waypoint2))
 (:action communicate_soil_data_rover1_general_waypoint2_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint2) (have_soil_analysis_rover1_waypoint2))
  :effect (communicated_soil_data_waypoint2))
 (:action communicate_soil_data_rover1_general_waypoint2_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint4) (have_soil_analysis_rover1_waypoint2))
  :effect (communicated_soil_data_waypoint2))
 (:action communicate_soil_data_rover1_general_waypoint2_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint5) (have_soil_analysis_rover1_waypoint2))
  :effect (communicated_soil_data_waypoint2))
 (:action communicate_soil_data_rover1_general_waypoint3_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint0) (have_soil_analysis_rover1_waypoint3))
  :effect (communicated_soil_data_waypoint3))
 (:action communicate_soil_data_rover1_general_waypoint3_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint1) (have_soil_analysis_rover1_waypoint3))
  :effect (communicated_soil_data_waypoint3))
 (:action communicate_soil_data_rover1_general_waypoint3_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint2) (have_soil_analysis_rover1_waypoint3))
  :effect (communicated_soil_data_waypoint3))
 (:action communicate_soil_data_rover1_general_waypoint3_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint4) (have_soil_analysis_rover1_waypoint3))
  :effect (communicated_soil_data_waypoint3))
 (:action communicate_soil_data_rover1_general_waypoint3_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint5) (have_soil_analysis_rover1_waypoint3))
  :effect (communicated_soil_data_waypoint3))
 (:action communicate_soil_data_rover1_general_waypoint4_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint0) (have_soil_analysis_rover1_waypoint4))
  :effect (communicated_soil_data_waypoint4))
 (:action communicate_soil_data_rover1_general_waypoint4_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint1) (have_soil_analysis_rover1_waypoint4))
  :effect (communicated_soil_data_waypoint4))
 (:action communicate_soil_data_rover1_general_waypoint4_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint2) (have_soil_analysis_rover1_waypoint4))
  :effect (communicated_soil_data_waypoint4))
 (:action communicate_soil_data_rover1_general_waypoint4_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint4) (have_soil_analysis_rover1_waypoint4))
  :effect (communicated_soil_data_waypoint4))
 (:action communicate_soil_data_rover1_general_waypoint4_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint5) (have_soil_analysis_rover1_waypoint4))
  :effect (communicated_soil_data_waypoint4))
 (:action communicate_soil_data_rover1_general_waypoint5_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint0) (have_soil_analysis_rover1_waypoint5))
  :effect (communicated_soil_data_waypoint5))
 (:action communicate_soil_data_rover1_general_waypoint5_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint1) (have_soil_analysis_rover1_waypoint5))
  :effect (communicated_soil_data_waypoint5))
 (:action communicate_soil_data_rover1_general_waypoint5_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint2) (have_soil_analysis_rover1_waypoint5))
  :effect (communicated_soil_data_waypoint5))
 (:action communicate_soil_data_rover1_general_waypoint5_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint4) (have_soil_analysis_rover1_waypoint5))
  :effect (communicated_soil_data_waypoint5))
 (:action communicate_soil_data_rover1_general_waypoint5_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint5) (have_soil_analysis_rover1_waypoint5))
  :effect (communicated_soil_data_waypoint5))
 (:action communicate_rock_data_rover0_general_waypoint0_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint0) (have_rock_analysis_rover0_waypoint0))
  :effect (communicated_rock_data_waypoint0))
 (:action communicate_rock_data_rover0_general_waypoint0_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint1) (have_rock_analysis_rover0_waypoint0))
  :effect (communicated_rock_data_waypoint0))
 (:action communicate_rock_data_rover0_general_waypoint0_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint2) (have_rock_analysis_rover0_waypoint0))
  :effect (communicated_rock_data_waypoint0))
 (:action communicate_rock_data_rover0_general_waypoint0_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint4) (have_rock_analysis_rover0_waypoint0))
  :effect (communicated_rock_data_waypoint0))
 (:action communicate_rock_data_rover0_general_waypoint0_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint5) (have_rock_analysis_rover0_waypoint0))
  :effect (communicated_rock_data_waypoint0))
 (:action communicate_rock_data_rover0_general_waypoint2_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint0) (have_rock_analysis_rover0_waypoint2))
  :effect (communicated_rock_data_waypoint2))
 (:action communicate_rock_data_rover0_general_waypoint2_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint1) (have_rock_analysis_rover0_waypoint2))
  :effect (communicated_rock_data_waypoint2))
 (:action communicate_rock_data_rover0_general_waypoint2_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint2) (have_rock_analysis_rover0_waypoint2))
  :effect (communicated_rock_data_waypoint2))
 (:action communicate_rock_data_rover0_general_waypoint2_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint4) (have_rock_analysis_rover0_waypoint2))
  :effect (communicated_rock_data_waypoint2))
 (:action communicate_rock_data_rover0_general_waypoint2_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint5) (have_rock_analysis_rover0_waypoint2))
  :effect (communicated_rock_data_waypoint2))
 (:action communicate_rock_data_rover0_general_waypoint3_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint0) (have_rock_analysis_rover0_waypoint3))
  :effect (communicated_rock_data_waypoint3))
 (:action communicate_rock_data_rover0_general_waypoint3_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint1) (have_rock_analysis_rover0_waypoint3))
  :effect (communicated_rock_data_waypoint3))
 (:action communicate_rock_data_rover0_general_waypoint3_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint2) (have_rock_analysis_rover0_waypoint3))
  :effect (communicated_rock_data_waypoint3))
 (:action communicate_rock_data_rover0_general_waypoint3_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint4) (have_rock_analysis_rover0_waypoint3))
  :effect (communicated_rock_data_waypoint3))
 (:action communicate_rock_data_rover0_general_waypoint3_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint5) (have_rock_analysis_rover0_waypoint3))
  :effect (communicated_rock_data_waypoint3))
 (:action communicate_rock_data_rover0_general_waypoint5_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint0) (have_rock_analysis_rover0_waypoint5))
  :effect (communicated_rock_data_waypoint5))
 (:action communicate_rock_data_rover0_general_waypoint5_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint1) (have_rock_analysis_rover0_waypoint5))
  :effect (communicated_rock_data_waypoint5))
 (:action communicate_rock_data_rover0_general_waypoint5_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint2) (have_rock_analysis_rover0_waypoint5))
  :effect (communicated_rock_data_waypoint5))
 (:action communicate_rock_data_rover0_general_waypoint5_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint4) (have_rock_analysis_rover0_waypoint5))
  :effect (communicated_rock_data_waypoint5))
 (:action communicate_rock_data_rover0_general_waypoint5_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint5) (have_rock_analysis_rover0_waypoint5))
  :effect (communicated_rock_data_waypoint5))
 (:action communicate_image_data_rover0_general_objective0_colour_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint0) (have_image_rover0_objective0_colour))
  :effect (communicated_image_data_objective0_colour))
 (:action communicate_image_data_rover0_general_objective0_colour_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint1) (have_image_rover0_objective0_colour))
  :effect (communicated_image_data_objective0_colour))
 (:action communicate_image_data_rover0_general_objective0_colour_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint2) (have_image_rover0_objective0_colour))
  :effect (communicated_image_data_objective0_colour))
 (:action communicate_image_data_rover0_general_objective0_colour_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint4) (have_image_rover0_objective0_colour))
  :effect (communicated_image_data_objective0_colour))
 (:action communicate_image_data_rover0_general_objective0_colour_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint5) (have_image_rover0_objective0_colour))
  :effect (communicated_image_data_objective0_colour))
 (:action communicate_image_data_rover0_general_objective0_low_res_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint0) (have_image_rover0_objective0_low_res))
  :effect (communicated_image_data_objective0_low_res))
 (:action communicate_image_data_rover0_general_objective0_low_res_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint1) (have_image_rover0_objective0_low_res))
  :effect (communicated_image_data_objective0_low_res))
 (:action communicate_image_data_rover0_general_objective0_low_res_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint2) (have_image_rover0_objective0_low_res))
  :effect (communicated_image_data_objective0_low_res))
 (:action communicate_image_data_rover0_general_objective0_low_res_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint4) (have_image_rover0_objective0_low_res))
  :effect (communicated_image_data_objective0_low_res))
 (:action communicate_image_data_rover0_general_objective0_low_res_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint5) (have_image_rover0_objective0_low_res))
  :effect (communicated_image_data_objective0_low_res))
 (:action communicate_image_data_rover0_general_objective1_colour_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint0) (have_image_rover0_objective1_colour))
  :effect (communicated_image_data_objective1_colour))
 (:action communicate_image_data_rover0_general_objective1_colour_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint1) (have_image_rover0_objective1_colour))
  :effect (communicated_image_data_objective1_colour))
 (:action communicate_image_data_rover0_general_objective1_colour_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint2) (have_image_rover0_objective1_colour))
  :effect (communicated_image_data_objective1_colour))
 (:action communicate_image_data_rover0_general_objective1_colour_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint4) (have_image_rover0_objective1_colour))
  :effect (communicated_image_data_objective1_colour))
 (:action communicate_image_data_rover0_general_objective1_colour_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint5) (have_image_rover0_objective1_colour))
  :effect (communicated_image_data_objective1_colour))
 (:action communicate_image_data_rover0_general_objective1_low_res_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint0) (have_image_rover0_objective1_low_res))
  :effect (communicated_image_data_objective1_low_res))
 (:action communicate_image_data_rover0_general_objective1_low_res_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint1) (have_image_rover0_objective1_low_res))
  :effect (communicated_image_data_objective1_low_res))
 (:action communicate_image_data_rover0_general_objective1_low_res_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint2) (have_image_rover0_objective1_low_res))
  :effect (communicated_image_data_objective1_low_res))
 (:action communicate_image_data_rover0_general_objective1_low_res_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint4) (have_image_rover0_objective1_low_res))
  :effect (communicated_image_data_objective1_low_res))
 (:action communicate_image_data_rover0_general_objective1_low_res_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover0_waypoint5) (have_image_rover0_objective1_low_res))
  :effect (communicated_image_data_objective1_low_res))
 (:action communicate_image_data_rover1_general_objective0_colour_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint0) (have_image_rover1_objective0_colour))
  :effect (communicated_image_data_objective0_colour))
 (:action communicate_image_data_rover1_general_objective0_colour_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint1) (have_image_rover1_objective0_colour))
  :effect (communicated_image_data_objective0_colour))
 (:action communicate_image_data_rover1_general_objective0_colour_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint2) (have_image_rover1_objective0_colour))
  :effect (communicated_image_data_objective0_colour))
 (:action communicate_image_data_rover1_general_objective0_colour_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint4) (have_image_rover1_objective0_colour))
  :effect (communicated_image_data_objective0_colour))
 (:action communicate_image_data_rover1_general_objective0_colour_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint5) (have_image_rover1_objective0_colour))
  :effect (communicated_image_data_objective0_colour))
 (:action communicate_image_data_rover1_general_objective0_high_res_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint0) (have_image_rover1_objective0_high_res))
  :effect (communicated_image_data_objective0_high_res))
 (:action communicate_image_data_rover1_general_objective0_high_res_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint1) (have_image_rover1_objective0_high_res))
  :effect (communicated_image_data_objective0_high_res))
 (:action communicate_image_data_rover1_general_objective0_high_res_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint2) (have_image_rover1_objective0_high_res))
  :effect (communicated_image_data_objective0_high_res))
 (:action communicate_image_data_rover1_general_objective0_high_res_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint4) (have_image_rover1_objective0_high_res))
  :effect (communicated_image_data_objective0_high_res))
 (:action communicate_image_data_rover1_general_objective0_high_res_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint5) (have_image_rover1_objective0_high_res))
  :effect (communicated_image_data_objective0_high_res))
 (:action communicate_image_data_rover1_general_objective1_colour_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint0) (have_image_rover1_objective1_colour))
  :effect (communicated_image_data_objective1_colour))
 (:action communicate_image_data_rover1_general_objective1_colour_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint1) (have_image_rover1_objective1_colour))
  :effect (communicated_image_data_objective1_colour))
 (:action communicate_image_data_rover1_general_objective1_colour_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint2) (have_image_rover1_objective1_colour))
  :effect (communicated_image_data_objective1_colour))
 (:action communicate_image_data_rover1_general_objective1_colour_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint4) (have_image_rover1_objective1_colour))
  :effect (communicated_image_data_objective1_colour))
 (:action communicate_image_data_rover1_general_objective1_colour_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint5) (have_image_rover1_objective1_colour))
  :effect (communicated_image_data_objective1_colour))
 (:action communicate_image_data_rover1_general_objective1_high_res_waypoint0_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint0) (have_image_rover1_objective1_high_res))
  :effect (communicated_image_data_objective1_high_res))
 (:action communicate_image_data_rover1_general_objective1_high_res_waypoint1_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint1) (have_image_rover1_objective1_high_res))
  :effect (communicated_image_data_objective1_high_res))
 (:action communicate_image_data_rover1_general_objective1_high_res_waypoint2_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint2) (have_image_rover1_objective1_high_res))
  :effect (communicated_image_data_objective1_high_res))
 (:action communicate_image_data_rover1_general_objective1_high_res_waypoint4_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint4) (have_image_rover1_objective1_high_res))
  :effect (communicated_image_data_objective1_high_res))
 (:action communicate_image_data_rover1_general_objective1_high_res_waypoint5_waypoint3
  :parameters () :precondition (and (at_rover1_waypoint5) (have_image_rover1_objective1_high_res))
  :effect (communicated_image_data_objective1_high_res))
;; action partitions
)