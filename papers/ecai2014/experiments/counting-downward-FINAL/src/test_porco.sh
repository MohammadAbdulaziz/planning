#!/bin/bash

prefix="/home/cgretton/Work2012/Jussi/Porco/np2pddl-read-only/problems/"
satproblems=`echo $prefix/sat/satisfiable/91-20/`
unsatproblems=`echo $prefix/sat/unsatisfiable/218-50/`
threeclique=`echo $prefix/clique/10size/5prob/3clique/`

coloring10x10=`echo $prefix/coloring/10size/10prob/`
coloring10x25=`echo $prefix/coloring/10size/25prob/`
coloring10x50=`echo $prefix/coloring/10size/50prob/`

coloring15x10=`echo $prefix/coloring/15size/10prob/`
coloring15x25=`echo $prefix/coloring/15size/25prob/`
coloring15x50=`echo $prefix/coloring/15size/50prob/`

# echo "Clique problems"

# for f in `ls $threeclique/*.pddl` ; do  ./plan-ipc mutex-model-count $prefix/clique/clique.pddl $f output  | grep "Maximum serial plan length is" ; done


echo "Coloring problems"

for f in `ls $coloring15x10/*.pddl` ; do  ./plan-ipc mutex-model-count $prefix/coloring/coloring.pddl $f output 2> /dev/null | grep "Maximum serial plan length is" ; done

for f in `ls $coloring15x25/*.pddl` ; do  ./plan-ipc mutex-model-count $prefix/coloring/coloring.pddl $f output 2> /dev/null | grep "Maximum serial plan length is" ; done

for f in `ls $coloring15x50/*.pddl` ; do  ./plan-ipc mutex-model-count $prefix/coloring/coloring.pddl $f output 2> /dev/null | grep "Maximum serial plan length is" ; done


for f in `ls $coloring10x10/*.pddl` ; do  ./plan-ipc mutex-model-count $prefix/coloring/coloring.pddl $f output 2> /dev/null | grep "Maximum serial plan length is" ; done

for f in `ls $coloring10x25/*.pddl` ; do  ./plan-ipc mutex-model-count $prefix/coloring/coloring.pddl $f output 2> /dev/null | grep "Maximum serial plan length is" ; done

for f in `ls $coloring10x50/*.pddl` ; do  ./plan-ipc mutex-model-count $prefix/coloring/coloring.pddl $f output 2> /dev/null | grep "Maximum serial plan length is" ; done


# echo "SAT problems"

# for f in `ls $unsatproblems/*.pddl` ; do  ./plan-ipc mutex-model-count $prefix/sat/sat.pddl $f output 2> /dev/null | grep "Maximum serial plan length is" ; done

# for f in `ls $satproblems/*.pddl` ; do  ./plan-ipc mutex-model-count $prefix/sat/sat.pddl $f output 2> /dev/null | grep "Maximum serial plan length is" ; done
