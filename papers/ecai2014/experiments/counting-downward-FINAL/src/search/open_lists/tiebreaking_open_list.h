#ifndef OPEN_LISTS_TIEBREAKING_OPEN_LIST_H
#define OPEN_LISTS_TIEBREAKING_OPEN_LIST_H

#include "open_list.h"
#include "../evaluator.h"

#include <deque>
#include <map>
#include <vector>
#include <utility>

class ScalarEvaluator;

class _TieBreakingOpenList : public OpenList<unsigned char*> {
    typedef std::deque<unsigned char*> Bucket;

    std::map<const std::vector<int>, Bucket> buckets;
    int size;

    std::vector<ScalarEvaluator *> evaluators;
    std::vector<int> last_evaluated_value;
    bool last_preferred;
    bool dead_end;
    bool first_is_dead_end;
    bool dead_end_reliable;
    bool allow_unsafe_pruning; // don't insert if main evaluator
    // says dead end, even if not reliably

    const std::vector<int> &get_value() {
        return last_evaluated_value;
    }


    int dimension() const {
        return evaluators.size();
    }
    // const std::vector<int> &get_value(); // currently not used
    // int dimension() const;
protected:
    Evaluator *get_evaluator() {return this; }

public:



    OpenList<unsigned char*> *create(
        const std::vector<string> &config, int start, int &end, bool dry_run) {
        std::vector<ScalarEvaluator *> evaluators;
        NamedOptionParser option_parser;
        bool only_pref_ = false;
        bool allow_unsafe_ = true;
        option_parser.add_bool_option("pref_only", &only_pref_,
                                      "insert only preferred operators");
        option_parser.add_bool_option("unsafe_pruning", &allow_unsafe_,
                                      "allow unsafe pruning when the main evaluator regards a state a dead end");
        OptionParser *parser = OptionParser::instance();
        parser->parse_evals_and_options(config, start, end, evaluators,
                                        option_parser, false, dry_run);

        if (dry_run)
            return 0;
        else
            return new _TieBreakingOpenList(evaluators, only_pref_,
                                            allow_unsafe_);
    }


    _TieBreakingOpenList(
        const std::vector<ScalarEvaluator *> &evals,
        bool preferred_only, bool unsafe_pruning)
        : OpenList<unsigned char*>(preferred_only), size(0), evaluators(evals),
          allow_unsafe_pruning(unsafe_pruning) {
        last_evaluated_value.resize(evaluators.size());
    }


    ~_TieBreakingOpenList() {
    }


    typedef unsigned char* Thingi;
    int insert(const Thingi& entry) ;

    unsigned char* remove_min(vector<int> *key) {
        assert(size > 0);
        typename std::map<const std::vector<int>, Bucket>::iterator it;
        it = buckets.begin();
        assert(it != buckets.end());
        assert(!it->second.empty());
        size--;
        if (key) {
            assert(key->empty());
            *key = it->first;
        }
        const unsigned char* result = it->second.front();
        it->second.pop_front();
        if (it->second.empty())
            buckets.erase(it);
        return const_cast<unsigned char*>(result);
    }


    bool empty() const {
        return size == 0;
    }


    void clear() {
        buckets.clear();
        size = 0;
    }


    void evaluate(int g, bool preferred) {
        dead_end = false;
        dead_end_reliable = false;

        for (unsigned int i = 0; i < evaluators.size(); i++) {
            evaluators[i]->evaluate(g, preferred);

            // check for dead end
            if (evaluators[i]->is_dead_end()) {
                last_evaluated_value[i] = std::numeric_limits<int>::max();
                dead_end = true;
                if (evaluators[i]->dead_end_is_reliable()) {
                    dead_end_reliable = true;
                }
            } else { // add value if no dead end
                last_evaluated_value[i] = evaluators[i]->get_value();
            }
        }
        first_is_dead_end = evaluators[0]->is_dead_end();
        last_preferred = preferred;
    }


    bool is_dead_end() const {
        return dead_end;
    }


    bool dead_end_is_reliable() const {
        return dead_end_reliable;
    }




    void get_involved_heuristics(std::set<Heuristic *> &hset) {
        for (unsigned int i = 0; i < evaluators.size(); i++) {
            evaluators[i]->get_involved_heuristics(hset);
        }
    }




    // _TieBreakingOpenList(const std::vector<ScalarEvaluator *> &evals,
    //                     bool preferred_only, bool unsafe_pruning);
    // ~_TieBreakingOpenList();
    
    // typedef unsigned char* Thingi;
    // int insert(const Thingi &){return 9;}
    // unsigned char* remove_min(std::vector<int> *key = 0);
    // bool empty() const;
    // void clear();

    // // tuple evaluator interface
    // void evaluate(int g, bool preferred);
    // bool is_dead_end() const;
    // bool dead_end_is_reliable() const;
    // void get_involved_heuristics(std::set<Heuristic *> &hset);

    // static OpenList<unsigned char*> *create(const std::vector<std::string> &config,
    //                                int start, int &end, bool dry_run = false);
};

template<class Entry>
class TieBreakingOpenList : public OpenList<Entry> {
    typedef std::deque<Entry> Bucket;

    std::map<const std::vector<int>, Bucket> buckets;
    int size;

    std::vector<ScalarEvaluator *> evaluators;
    std::vector<int> last_evaluated_value;
    bool last_preferred;
    bool dead_end;
    bool first_is_dead_end;
    bool dead_end_reliable;
    bool allow_unsafe_pruning; // don't insert if main evaluator
    // says dead end, even if not reliably

    const std::vector<int> &get_value(); // currently not used
    int dimension() const;
protected:
    Evaluator *get_evaluator() {return this; }

public:
    TieBreakingOpenList(const std::vector<ScalarEvaluator *> &evals,
                        bool preferred_only, bool unsafe_pruning);
    ~TieBreakingOpenList();


    int insert(const Entry &entry);
    Entry remove_min(std::vector<int> *key = 0);
    bool empty() const;
    void clear();

    // tuple evaluator interface
    void evaluate(int g, bool preferred);
    bool is_dead_end() const;
    bool dead_end_is_reliable() const;
    void get_involved_heuristics(std::set<Heuristic *> &hset);

    static OpenList<Entry> *create(const std::vector<std::string> &config,
                                   int start, int &end, bool dry_run = false);
};

#include "tiebreaking_open_list.cc"

// HACK! Need a better strategy of dealing with templates, also in the Makefile.

#endif
