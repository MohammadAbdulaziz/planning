
#include <algorithm>

namespace Planning
{

    class SCCisolation : public SCChashsat {
    public:
        
        SCCisolation(vector<Operator>& problem, string name):SCChashsat(problem, name){};
        
        // int  worstCaseModelCount(int component, 
        //                          int predecessor, 
        //                          int vars, 
        //                          int mutexes);


        void action_setHasHaveInverse(int actionIndex);
        void action_inverses();
        bool action_getHasInverse(int actionIndex);
        /* expensive calculation of whether two argument actions are inverses.*/
        bool action_isInverse(int actionIndexI, int actionIndexJ);
    protected:
        int action_inverse_count;
        vector<int> action_inverse;
        vector<bool> action_hasInverse;
    };

    // int SCCisolation::worstCaseModelCount(int component, 
    //                                       int predecessor, 
    //                                       int vars, 
    //                                       int mutexes){
        
    // }
    
    // /*Indirect.*/
    // bool SCCisolation::indirect_action_isInverse(int actionIndexI, int actionIndexJ)
    // {
    // }



    


    bool SCCisolation::action_isInverse(int actionIndexI, int actionIndexJ)
    {
        
        set<Proposition<> > addI = set<Proposition<> >(actions[actionIndexI]._add.begin(), actions[actionIndexI]._add.end());
        set<Proposition<> > delJ = set<Proposition<> >(actions[actionIndexJ]._del.begin(), actions[actionIndexJ]._del.end());
        
        
        if(addI == delJ){
            cerr<<"Add list is delete list test passed :: "<<actionIndexJ<<" "<<actionIndexI<<endl;
            
            
            set<Proposition<> > addJ = set<Proposition<> >(actions[actionIndexJ]._add.begin(), actions[actionIndexJ]._add.end());
            set<Proposition<> > delI = set<Proposition<> >(actions[actionIndexI]._del.begin(), actions[actionIndexI]._del.end());
            

            for(set<Proposition<> >::iterator i_prec = addJ.begin(); i_prec != addJ.end(); i_prec++){
                cerr<<i_prec->id<<"["<<assignments[i_prec->id].first<<"->"<<assignments[i_prec->id].second<<"], ";
            }cerr<<endl;
            
            for(set<Proposition<> >::iterator i_prec = delI.begin(); i_prec != delI.end(); i_prec++){
                cerr<<i_prec->id<<"["<<assignments[i_prec->id].first<<"->"<<assignments[i_prec->id].second<<"], ";
            }cerr<<endl;
            
            
                
            
            /* If we have an instance inverse...*/
            if(addJ == delI){
                
                
                cerr<<"Add list is delete list test passed :: "<<actionIndexI<<" "<<actionIndexJ<<endl;
                
                set<Proposition<> > precI = set<Proposition<> >(actions[actionIndexI]._prec.begin(), actions[actionIndexI]._prec.end());
                set<Proposition<> > precJ = set<Proposition<> >(actions[actionIndexJ]._prec.begin(), actions[actionIndexJ]._prec.end());
                
                vector<Proposition<> > intersection(std::min(precI.size(), precJ.size()));
                
                vector<Proposition<> >::iterator end_of_intersection = 
                    set_intersection(precI.begin(), precI.end(), precJ.begin(), precJ.end(), intersection.begin());

                for(vector<Proposition<> >::iterator intersect = intersection.begin()
                        ; intersect == end_of_intersection
                        ; intersect++){
                    precI.erase(*intersect);
                    precJ.erase(*intersect);
                }

                for(set<Proposition<> >::iterator i_prec = precI.begin(); i_prec != precI.end(); i_prec++){
                    if(addJ.find(*i_prec) == addJ.end()){
                        return false;
                    }
                }
                
                for(set<Proposition<> >::iterator j_prec = precJ.begin(); j_prec != precJ.end(); j_prec++){
                    if(addI.find(*j_prec) == addI.end()){
                        return false;
                    }
                }

                return true;
            } 
        }
        
        return false;
    }
    
    bool SCCisolation::action_getHasInverse(int actionIndex)
    {
        return action_hasInverse[actionIndex];
    }
    
    void SCCisolation::action_setHasHaveInverse(int actionIndex)
    {
        action_hasInverse[actionIndex] = true;
    }
    
    void SCCisolation::action_inverses()
    {
        int action_id = 0;
        action_inverse_count = 0;
        action_inverse = vector<int>(actions.size());
        action_hasInverse = vector<bool>(actions.size());
        for(int i = 0; i < actions.size(); i++)
        {
            action_hasInverse[i] =  false;
            action_inverse[i] = -1;
        }

        for (typename std::vector<GroundActionType>::iterator action =  actions.begin()
                 ; action != actions.end()
                 ; action++, action_id++){ 
            
            cerr<<"Trying inverses for action :: "<<action_id<<endl;

            if(!action_getHasInverse(action_id)){
                cerr<<"No inverse found yet for action :: "<<action_id<<endl;
                typename std::vector<GroundActionType>::iterator action2 = action;
                int action2_id = action_id;
                for (action2++, action2_id++
                         ; action2 != actions.end()
                         ; action2++, action2_id++){ 
                    
                    cerr<<"Trying action :: "<<action2_id<<" as an inverse"<<endl;
                    if(action_isInverse(action_id, action2_id)){
                        
                        cerr<<"Got inverse :: "<<action_id<<" "<<action2_id<<std::endl;

                        action_inverse_count++;
                        action_setHasHaveInverse(action_id);
                        action_setHasHaveInverse(action2_id);
                        action_inverse[action_id] = action2_id;
                        action_inverse[action2_id] = action_id;
                        break;
                    }
                    
                }
            }
        }
        cerr<<"Action count is :: "<<actions.size()<<" and we got :: "<<action_inverse_count<<" inverses.\n";
    } 

}
