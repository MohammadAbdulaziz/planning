/* =========FOR INTERNAL USE ONLY. NO DISTRIBUTION PLEASE ========== */

/*********************************************************************
 Copyright 2000-2001, Princeton University.  All rights reserved. 
 By using this software the USER indicates that he or she has read, 
 understood and will comply with the following:

 --- Princeton University hereby grants USER nonexclusive permission 
 to use, copy and/or modify this software for internal, noncommercial,
 research purposes only. Any distribution, including commercial sale 
 or license, of this software, copies of the software, its associated 
 documentation and/or modifications of either is strictly prohibited 
 without the prior consent of Princeton University.  Title to copyright
 to this software and its associated documentation shall at all times 
 remain with Princeton University.  Appropriate copyright notice shall 
 be placed on all software copies, and a complete copy of this notice 
 shall be included in all copies of the associated documentation.  
 No right is  granted to use in advertising, publicity or otherwise 
 any trademark,  service mark, or the name of Princeton University. 


 --- This software and any associated documentation is provided "as is" 

 PRINCETON UNIVERSITY MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS 
 OR IMPLIED, INCLUDING THOSE OF MERCHANTABILITY OR FITNESS FOR A 
 PARTICULAR PURPOSE, OR THAT  USE OF THE SOFTWARE, MODIFICATIONS, OR 
 ASSOCIATED DOCUMENTATION WILL NOT INFRINGE ANY PATENTS, COPYRIGHTS, 
 TRADEMARKS OR OTHER INTELLECTUAL PROPERTY RIGHTS OF A THIRD PARTY.  

 Princeton University shall not be liable under any circumstances for 
 any direct, indirect, special, incidental, or consequential damages 
 with respect to any claim by USER or any third party on account of 
 or arising from the use, or inability to use, this software or its 
 associated documentation, even if Princeton University has been advised
 of the possibility of those damages.
*********************************************************************/
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <set>
#include <vector>
#include "SAT.h"#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <cassert>
#include <cctype>


using namespace std;

#define MAX_LINE_LENGTH    65536
#define MAX_WORD_LENGTH    64

int fake_counter = 1;
int fake_SAT_AddVariable(SAT_Manager& mng){
    return fake_counter++;
}

//This cnf parser function is based on the GRASP code by Joao Marques Silva
void read_cnf(SAT_Manager mng, char * filename )
{
    char line_buffer[MAX_LINE_LENGTH];
    char word_buffer[MAX_WORD_LENGTH];
    set<int> clause_vars;
    set<int> clause_lits;
    int line_num = 0;

    ifstream inp (filename, ios::in);
    if (!inp) {
	cerr << "Can't open input file" << endl;
	exit(1);
    }
    while (inp.getline(line_buffer, MAX_LINE_LENGTH)) {
	++ line_num;
	if (line_buffer[0] == 'c') { 
	    continue; 
	}
	else if (line_buffer[0] == 'p') {
	    int var_num;
	    int cl_num;

	    int arg = sscanf (line_buffer, "p cnf %d %d", &var_num, &cl_num);
	    if( arg < 2 ) {
		cerr << "Unable to read number of variables and clauses"
		     << "at line " << line_num << endl;
		exit(3);
	    }
	    SAT_SetNumVariables(mng, var_num); //first element not used.
	}
	else if (line_buffer[0] == 'w')		// added by sang
	{
		;	// skip lines starting with w
	}
	else {                             // Clause definition or continuation
	    char *lp = line_buffer;
	    do {
		char *wp = word_buffer;
		while (*lp && ((*lp == ' ') || (*lp == '\t'))) {
		    lp++;
		}
		while (*lp && (*lp != ' ') && (*lp != '\t') && (*lp != '\n')) {
		    *(wp++) = *(lp++);
		}
		*wp = '\0';                                 // terminate string

		if (strlen(word_buffer) != 0) {     // check if number is there
		    int var_idx = atoi (word_buffer);
		    int sign = 0;

		    if( var_idx != 0) {
			if( var_idx < 0)  { var_idx = -var_idx; sign = 1; }
			clause_vars.insert(var_idx);
			clause_lits.insert( (var_idx << 1) + sign);
		    } 	
		    else {
			//add this clause
			if (clause_vars.size() != 0 && (clause_vars.size() == clause_lits.size())) { //yeah, can add this clause
			    vector <int> temp;
			    for (set<int>::iterator itr = clause_lits.begin();
				 itr != clause_lits.end(); ++itr)
				{
					temp.push_back (*itr);
				}			
			    SAT_AddClause(mng, & temp.begin()[0], temp.size() );
			}
			else {} //it contain var of both polarity, so is automatically satisfied, just skip it
			clause_lits.clear();
			clause_vars.clear();
		    }
		}
	    }
	    while (*lp);
	}
    }
    if (!inp.eof()) {
	cerr << "Input line " << line_num <<  " too long. Unable to continue..." << endl;
	exit(2);
    }
//    assert (clause_vars.size() == 0); 	//some benchmark has no 0 in the last clause
    if (clause_lits.size() && clause_vars.size()==clause_lits.size() ) {
	vector <int> temp;
	for (set<int>::iterator itr = clause_lits.begin();
	     itr != clause_lits.end(); ++itr)
	    temp.push_back (*itr);
	SAT_AddClause(mng, & temp.begin()[0], temp.size() );
    }
    clause_lits.clear();
    clause_vars.clear();
}


void handle_result(SAT_Manager mng, int outcome, char * filename, bool quiet)
{
    char * result = "UNKNOWN";
    switch (outcome) {
    case SATISFIABLE:
	//cout << "Instance satisfiable" << endl;
//following lines will print out a solution if a solution exist
	/* for (int i=1, sz = SAT_NumVariables(mng); i<= sz; ++i) {
	    switch(SAT_GetVarAsgnment(mng, i)) {
	    case -1:	
		cout <<"("<< i<<")"; break;
	    case 0:
		cout << "-" << i; break;
	    case 1:
		cout << i ; break;
	    default:
		cerr << "Unknown variable value state"<< endl;
		exit(4);
	    }
	    cout << " ";
	} */
	result  = "SAT";
	cout << endl;
	break;
    case UNSATISFIABLE:
	result  = "UNSAT";
	cout << endl;
	//cout << "Instance unsatisfiable" << endl << endl;
	break;
    case TIME_OUT:
	result  = "ABORT : TIME OUT"; 
	cout << "Time out, unable to determing the satisfiablility of the instance";
	cout << endl;
	break;
    case MEM_OUT:
	result  = "ABORT : MEM OUT"; 
	cout << "Memory out, unable to determing the satisfiablility of the instance";
	cout << endl;
	break;
    default:
	cerr << "Unknown outcome" << endl;
	exit (5);
    }	
    if (!quiet)
	{
    cout << "Number of Decisions\t\t\t" << SAT_NumDecisions(mng)<< endl;	// order changed
	cout << "Max Decision Level\t\t\t" << SAT_MaxDLevel(mng) << endl;	// order changed
    cout << "Number of Variables\t\t\t" << SAT_NumVariables(mng) << endl;
    cout << "Original Num Clauses\t\t\t" << SAT_InitNumClauses(mng) << endl;
    cout << "Original Num Literals\t\t\t" << SAT_InitNumLiterals(mng) << endl;
    cout << "Added Conflict Clauses\t\t\t" << SAT_NumAddedClauses(mng)- SAT_InitNumClauses(mng)<< endl;
    cout << "Added Conflict Literals\t\t\t" << SAT_NumAddedLiterals(mng) - SAT_InitNumLiterals(mng) << endl;
    cout << "Deleted Unrelevant clauses\t\t" << SAT_NumDeletedClauses(mng) <<endl;
    cout << "Deleted Unrelevant literals\t\t" << SAT_NumDeletedLiterals(mng) <<endl;
    cout << "Number of Implications\t\t\t" << SAT_NumImplications(mng)<< endl;
    //other statistics comes here
    cout << "Total Run Time\t\t\t\t" << SAT_GetCPUTime(mng) << endl << endl;
	}
//    cout << "RESULT:\t" << filename << " " << result << " RunTime: " << SAT_GetCPUTime(mng)<< endl;
	//if (result  == "UNSAT")
	//	cout  << "UNSAT" << endl;
	//else 
	if (SAT_GMP(mng))
		{
			long double solutions = SAT_NumSolutions(mng);
			if (solutions > 1000000 && !quiet)
				cout << "In scientific number form\t\t" << solutions << endl;
		}
	else 
	{
		if (!quiet)
			cout << "Satisfying probability\t\t\t" << SAT_SatProb(mng) << endl;
		cout << "Number of solutions\t\t\t" << SAT_NumSolutions(mng) << endl;
	}
	cout << endl << endl;
}
void output_status(SAT_Manager mng)
{
    cout << "Dec: " << SAT_NumDecisions(mng)<< "\t ";
    cout << "AddCl: " << SAT_NumAddedClauses(mng) <<"\t";
    cout << "AddLit: " << SAT_NumAddedLiterals(mng)<<"\t";
    cout << "DelCl: " << SAT_NumDeletedClauses(mng) <<"\t";
    cout << "DelLit: " << SAT_NumDeletedLiterals(mng)<<"\t";
    cout << "NumImp: " << SAT_NumImplications(mng) <<"\t";
    cout << "AveBubbleMove: " << SAT_AverageBubbleMove(mng) <<"\t";
    //other statistics comes here
    cout << "RunTime:" << SAT_GetElapsedCPUTime(mng) << endl;
}

void verify_solution(SAT_Manager mng)
{
    int num_verified = 0;
    for ( int cl_idx = SAT_GetFirstClause (mng); cl_idx >= 0; 
	  cl_idx = SAT_GetNextClause(mng, cl_idx)) {
	int len = SAT_GetClauseNumLits(mng, cl_idx);
	int * lits = new int[len+1];
	SAT_GetClauseLits( mng, cl_idx, lits);
	int i;
	for (i=0; i< len; ++i) {
	    int v_idx = lits[i] >> 1;
	    int sign = lits[i] & 0x1;
	    int var_value = SAT_GetVarAsgnment( mng, v_idx);
	    if( (var_value == 1 && sign == 0) ||
		(var_value == 0 && sign == 1) ) break;
	}
	if (i >= len) {
	    cerr << "Verify Satisfiable solution failed, please file a bug report, thanks. " << endl;
	    exit(6);
	}
	delete [] lits;
	++ num_verified;
    }
    cout << num_verified << " Clauses are true, Verify Solution successful. ";
}

#include<vector>
#include<set>
#include<map>
#include<cassert>

int current_group_id;

typedef set<int> Clause;
typedef set<Clause> CNF;


void read_cnf (SAT_Manager& mng, CNF& cnf )
{//, std::set<std::pair<int, int> >& positive){

    cerr<<"Reading CNF\n";
    std::map<int, int> ayplan_vars;

    for(CNF::iterator p = cnf.begin()
            ; p != cnf.end()
            ; p++){
        const Clause& literals = *p;;
        for(Clause::iterator literal = literals.begin()
                ; literal != literals.end()
                ; literal++){
            int variable = std::abs(*literal);;
            if(ayplan_vars.find(variable) == ayplan_vars.end()){
                ayplan_vars[variable] =  SAT_AddVariable(mng);
            }
        }

    }
    
    set<int> clause_vars;
    set<int> clause_lits;
    int var_idx;
    int sign;
    for(CNF::iterator p = cnf.begin()
            ; p != cnf.end()
            ; p++){
        const Clause& literals = *p;;
        for(Clause::iterator literal = literals.begin()
                ; literal != literals.end()
                ; literal++){
            //1//cerr<<*literal<<" ";
            int variable = std::abs(*literal);;
            int my_sign = (*literal < 0)?-1:1;
            assert(ayplan_vars.find(variable) != ayplan_vars.end());
            var_idx = my_sign *  ayplan_vars[variable];
            sign = 0;
            if( var_idx < 0)  { var_idx = -var_idx; sign = 1; }
            clause_vars.insert(var_idx);
            clause_lits.insert( (var_idx << 1) + sign);
        }
        //1//cerr<<" 0"<<endl;
        
        vector <int> temp;
        for (set<int>::iterator itr = clause_lits.begin();
             itr != clause_lits.end(); ++itr)
        {
            temp.push_back (*itr);
        }

        SAT_AddClause(mng, & temp.begin()[0], temp.size(), current_group_id );
        
        clause_lits.clear();
        clause_vars.clear();
    }
}



int write_cnf (SAT_Manager& mng,  
               std::set<int > freeVariables, 
               std::set<std::pair<int, int> >& negative, 
               std::set<std::pair<int, int> >& equivalent,
               string name
)
{//, std::set<std::pair<int, int> >& positive){
    
    ofstream outfile; 

    system("pwd");
    string filename_output = "./search/Cachet/example.cnf."+name;
    cerr<<filename_output<<std::endl;
    outfile.open (filename_output.c_str()  , ios::out);
    

    //write stuff to that.


    std::map<int, int> FAKE_ayplan_vars;

    
    for(std::set<int >::iterator p = freeVariables.begin()
            ; p != freeVariables.end()
            ; p++){
        if(FAKE_ayplan_vars.find(*p) == FAKE_ayplan_vars.end()){
            FAKE_ayplan_vars[*p] =  fake_SAT_AddVariable(mng);//counter++;
            //1//cerr<<*p<<" is "<< FAKE_ayplan_vars[*p] <<std::endl;
        }

    }
    for(std::set<std::pair<int, int> >::iterator p = negative.begin()
            ; p != negative.end()
            ; p++){
        if(FAKE_ayplan_vars.find(p->first) == FAKE_ayplan_vars.end()){
            FAKE_ayplan_vars[p->first] =  fake_SAT_AddVariable(mng);//counter++;
            //1//cerr<<p->first<<" is "<< FAKE_ayplan_vars[p->first] <<std::endl;
        }

        if(FAKE_ayplan_vars.find(p->second) == FAKE_ayplan_vars.end()){
            FAKE_ayplan_vars[p->second] = fake_SAT_AddVariable(mng);
            //1//cerr<<p->second<<" is "<< FAKE_ayplan_vars[p->first] <<std::endl;
        }
    }
    for(std::set<std::pair<int, int> >::iterator p = equivalent.begin()
            ; p != equivalent.end()
            ; p++){
        if(FAKE_ayplan_vars.find(p->first) == FAKE_ayplan_vars.end()){
            FAKE_ayplan_vars[p->first] =  fake_SAT_AddVariable(mng);//counter++;
            //1//cerr<<p->first<<" is "<< FAKE_ayplan_vars[p->first] <<std::endl;
        }

        if(FAKE_ayplan_vars.find(p->second) == FAKE_ayplan_vars.end()){
            FAKE_ayplan_vars[p->second] = fake_SAT_AddVariable(mng);
            //1//cerr<<p->second<<" is "<< FAKE_ayplan_vars[p->first] <<std::endl;
        }
    }
    
    
    std::map<int, int> ayplan_vars = FAKE_ayplan_vars;


    outfile<<"p cnf "<<ayplan_vars.size()<<" 1\n";

    set<int> clause_vars;
    set<int> clause_lits;
    int var_idx;
    int sign;
    for(std::set<int >::iterator p = freeVariables.begin()
            ; p != freeVariables.end()
            ; p++){
        assert(ayplan_vars.find(*p) != ayplan_vars.end());

        
        outfile<<-ayplan_vars[*p] <<" "<<ayplan_vars[*p]<<" 0"<<std::endl;
    }

    for(std::set<std::pair<int, int> >::iterator p = negative.begin()
            ; p != negative.end()
            ; p++){
        assert(ayplan_vars.find(p->first) != ayplan_vars.end());
        assert(ayplan_vars.find(p->second) != ayplan_vars.end());
        
        outfile<<-ayplan_vars[p->first] <<" "<<-ayplan_vars[p->second]<<" 0"<<std::endl;
    }

    
    for(std::set<std::pair<int, int> >::iterator p = equivalent.begin()
            ; p != equivalent.end()
            ; p++){
        assert(ayplan_vars.find(p->first) != ayplan_vars.end());
        assert(ayplan_vars.find(p->second) != ayplan_vars.end());
        
        outfile<< -ayplan_vars[p->first] <<" "<< ayplan_vars[p->second]<<" 0"<<std::endl;
    }

    for(std::set<std::pair<int, int> >::iterator p = equivalent.begin()
            ; p != equivalent.end()
            ; p++){

        cerr<< ayplan_vars[p->first] <<" "<<-ayplan_vars[p->second]<<" 0"<<std::endl;

        
        assert(ayplan_vars.find(p->first) != ayplan_vars.end());
        assert(ayplan_vars.find(p->second) != ayplan_vars.end());
        
        outfile<< ayplan_vars[p->first] <<" "<<-ayplan_vars[p->second]<<" 0"<<std::endl;
    }
    
    fake_counter = 1;

    outfile.close();

    string run = "~/bin/cachet ./search/Cachet/example.cnf."+name+" > output.cachet."+name;
    cerr<<run<<std::endl;
    system(run.c_str());
    
    ifstream infile;
    string in_file_name ="./output.cachet."+name ;
    infile.open (in_file_name.c_str()  , ios::in);
    
    if(infile.eof())return -1;
    string tabname = "";
    string tmp;
    
    do{
        // cerr<<"Getting line...\n";
        getline(infile, tmp);//>>tmp;
        //infile.getline(tmp, MAX_LINE_LENGTH)
        if(!tmp.size())continue;

        istringstream iss(tmp);

        string first;
        iss>>first;

        cerr<<first<<std::endl;

        string second;
        iss>>second;

        cerr<<second<<std::endl;
        string third;
        iss>>third;

        cerr<<third<<std::endl;

        double forth;
        iss>>forth;

        cerr<<forth<<std::endl;

        //Number of solutions
        if(first == "Number" && second == "of" && third == "solutions"){
            if(forth < 1000000){
                return forth;
            }else {
                return -1;
            }
        }
    }while(!infile.eof());
        
    exit(-1);
    return -1;
}

void read_cnf (SAT_Manager& mng,  
               std::set<int > freeVariables, 
               std::set<std::pair<int, int> >& negative, 
                std::set<std::pair<int, int> >& equivalent)
{//, std::set<std::pair<int, int> >& positive){

    //1//cerr<<"Reading CNF\n";
    std::map<int, int> FAKE_ayplan_vars;

    
    for(std::set<int >::iterator p = freeVariables.begin()
            ; p != freeVariables.end()
            ; p++){
        if(FAKE_ayplan_vars.find(*p) == FAKE_ayplan_vars.end()){
            FAKE_ayplan_vars[*p] =  fake_SAT_AddVariable(mng);//counter++;
            //1//cerr<<*p<<" is "<< FAKE_ayplan_vars[*p] <<std::endl;
        }

    }
    for(std::set<std::pair<int, int> >::iterator p = negative.begin()
            ; p != negative.end()
            ; p++){
        if(FAKE_ayplan_vars.find(p->first) == FAKE_ayplan_vars.end()){
            FAKE_ayplan_vars[p->first] =  fake_SAT_AddVariable(mng);//counter++;
            //1//cerr<<p->first<<" is "<< FAKE_ayplan_vars[p->first] <<std::endl;
        }

        if(FAKE_ayplan_vars.find(p->second) == FAKE_ayplan_vars.end()){
            FAKE_ayplan_vars[p->second] = fake_SAT_AddVariable(mng);
            //1//cerr<<p->second<<" is "<< FAKE_ayplan_vars[p->first] <<std::endl;
        }
    }
    for(std::set<std::pair<int, int> >::iterator p = equivalent.begin()
            ; p != equivalent.end()
            ; p++){
        if(FAKE_ayplan_vars.find(p->first) == FAKE_ayplan_vars.end()){
            FAKE_ayplan_vars[p->first] =  fake_SAT_AddVariable(mng);//counter++;
            //1//cerr<<p->first<<" is "<< FAKE_ayplan_vars[p->first] <<std::endl;
        }

        if(FAKE_ayplan_vars.find(p->second) == FAKE_ayplan_vars.end()){
            FAKE_ayplan_vars[p->second] = fake_SAT_AddVariable(mng);
            //1//cerr<<p->second<<" is "<< FAKE_ayplan_vars[p->first] <<std::endl;
        }
    }




    std::map<int, int> ayplan_vars = FAKE_ayplan_vars;

    SAT_SetNumVariables(mng, ayplan_vars.size());



    // for(std::set<int >::iterator p = freeVariables.begin()
    //         ; p != freeVariables.end()
    //         ; p++){
    //     if(ayplan_vars.find(*p) == ayplan_vars.end()){
    //         ayplan_vars[*p] =  SAT_AddVariable(mng);//counter++;
    //         //1//cerr<<*p<<" is "<< ayplan_vars[*p] <<std::endl;
    //     }

    // }
    // for(std::set<std::pair<int, int> >::iterator p = negative.begin()
    //         ; p != negative.end()
    //         ; p++){
    //     if(ayplan_vars.find(p->first) == ayplan_vars.end()){
    //         ayplan_vars[p->first] =  SAT_AddVariable(mng);//counter++;
    //         //1//cerr<<p->first<<" is "<< ayplan_vars[p->first] <<std::endl;
    //     }

    //     if(ayplan_vars.find(p->second) == ayplan_vars.end()){
    //         ayplan_vars[p->second] = SAT_AddVariable(mng);
    //         //1//cerr<<p->second<<" is "<< ayplan_vars[p->first] <<std::endl;
    //     }
    // }
    // for(std::set<std::pair<int, int> >::iterator p = equivalent.begin()
    //         ; p != equivalent.end()
    //         ; p++){
    //     if(ayplan_vars.find(p->first) == ayplan_vars.end()){
    //         ayplan_vars[p->first] =  SAT_AddVariable(mng);//counter++;
    //         //1//cerr<<p->first<<" is "<< ayplan_vars[p->first] <<std::endl;
    //     }

    //     if(ayplan_vars.find(p->second) == ayplan_vars.end()){
    //         ayplan_vars[p->second] = SAT_AddVariable(mng);
    //         //1//cerr<<p->second<<" is "<< ayplan_vars[p->first] <<std::endl;
    //     }
    // }
    // for(std::set<std::pair<int, int> >::iterator p = positive.begin()
    //         ; p != positive.end()
    //         ; p++){
    //     if(ayplan_vars.find(p->first) == ayplan_vars.end()){
    //         ayplan_vars[p->first] =  SAT_AddVariable(mng);//counter++;
    //         cerr<<p->first<<" is "<< ayplan_vars[p->first] <<std::endl;
    //     }

    //     if(ayplan_vars.find(p->second) == ayplan_vars.end()){
    //         ayplan_vars[p->second] = SAT_AddVariable(mng);
    //         cerr<<p->second<<" is "<< ayplan_vars[p->first] <<std::endl;
    //     }
    // }
    set<int> clause_vars;
    set<int> clause_lits;
    int var_idx;
    int sign;
    for(std::set<int >::iterator p = freeVariables.begin()
            ; p != freeVariables.end()
            ; p++){
        assert(ayplan_vars.find(*p) != ayplan_vars.end());

        var_idx = - ayplan_vars[*p];
        sign = 0;
        if( var_idx < 0)  { var_idx = -var_idx; sign = 1; }
        clause_vars.insert(var_idx);
        clause_lits.insert( (var_idx << 1) + sign);
        var_idx = ayplan_vars[*p];
        sign = 0;
        if( var_idx < 0)  { var_idx = -var_idx; sign = 1; }
        clause_vars.insert(var_idx);
        clause_lits.insert( (var_idx << 1) + sign);
        
        //1//cerr<<" Adding stupid clause :: "<<-ayplan_vars[*p] <<" or "<<ayplan_vars[*p]<<std::endl;
        cerr<<-ayplan_vars[*p] <<" "<<ayplan_vars[*p]<<" 0"<<std::endl;
        vector <int> temp;
        for (set<int>::iterator itr = clause_lits.begin();
             itr != clause_lits.end(); ++itr)
        {
            temp.push_back (*itr);
        }
        SAT_AddClause(mng, & temp.begin()[0], temp.size());//, current_group_id );
        
        clause_lits.clear();
        clause_vars.clear();
    }

    for(std::set<std::pair<int, int> >::iterator p = negative.begin()
            ; p != negative.end()
            ; p++){
        assert(ayplan_vars.find(p->first) != ayplan_vars.end());
        assert(ayplan_vars.find(p->second) != ayplan_vars.end());
        

        var_idx = - ayplan_vars[p->first];
        sign = 0;
        if( var_idx < 0)  { var_idx = -var_idx; sign = 1; }
        clause_vars.insert(var_idx);
        clause_lits.insert( (var_idx << 1) + sign);
        var_idx = - ayplan_vars[p->second];
        sign = 0;
        if( var_idx < 0)  { var_idx = -var_idx; sign = 1; }
        clause_vars.insert(var_idx);
        clause_lits.insert( (var_idx << 1) + sign);
        
        //1//cerr<<" Adding clause :: "<<-ayplan_vars[p->first] <<" or "<<-ayplan_vars[p->second]<<std::endl;
        cerr<<-ayplan_vars[p->first] <<" "<<-ayplan_vars[p->second]<<" 0"<<std::endl;
        vector <int> temp;
        for (set<int>::iterator itr = clause_lits.begin();
             itr != clause_lits.end(); ++itr)
        {
            temp.push_back (*itr);
        }
        SAT_AddClause(mng, & temp.begin()[0], temp.size());//, current_group_id );
        
        clause_lits.clear();
        clause_vars.clear();
    }

    
    for(std::set<std::pair<int, int> >::iterator p = equivalent.begin()
            ; p != equivalent.end()
            ; p++){
        assert(ayplan_vars.find(p->first) != ayplan_vars.end());
        assert(ayplan_vars.find(p->second) != ayplan_vars.end());
        

        var_idx = - ayplan_vars[p->first];
        sign = 0;
        if( var_idx < 0)  { var_idx = -var_idx; sign = 1; }
        clause_vars.insert(var_idx);
        clause_lits.insert( (var_idx << 1) + sign);
        var_idx =  ayplan_vars[p->second];
        sign = 0;
        if( var_idx < 0)  { var_idx = -var_idx; sign = 1; }
        clause_vars.insert(var_idx);
        clause_lits.insert( (var_idx << 1) + sign);
        
        //1//cerr<<" Adding clause :: "<< -ayplan_vars[p->first] <<" or "<< ayplan_vars[p->second]<<std::endl;
        vector <int> temp;
        for (set<int>::iterator itr = clause_lits.begin();
             itr != clause_lits.end(); ++itr)
        {
            temp.push_back (*itr);
        }
        SAT_AddClause(mng, & temp.begin()[0], temp.size());//, current_group_id );
        
        clause_lits.clear();
        clause_vars.clear();
    }

    for(std::set<std::pair<int, int> >::iterator p = equivalent.begin()
            ; p != equivalent.end()
            ; p++){
        assert(ayplan_vars.find(p->first) != ayplan_vars.end());
        assert(ayplan_vars.find(p->second) != ayplan_vars.end());
        

        var_idx = ayplan_vars[p->first];
        sign = 0;
        if( var_idx < 0)  { var_idx = -var_idx; sign = 1; }
        clause_vars.insert(var_idx);
        clause_lits.insert( (var_idx << 1) + sign);
        var_idx =  - ayplan_vars[p->second];
        sign = 0;
        if( var_idx < 0)  { var_idx = -var_idx; sign = 1; }
        clause_vars.insert(var_idx);
        clause_lits.insert( (var_idx << 1) + sign);
        
        //1//cerr<<" Adding clause :: "<< ayplan_vars[p->first] <<" or "<<-ayplan_vars[p->second]<<std::endl;
        vector <int> temp;
        for (set<int>::iterator itr = clause_lits.begin();
             itr != clause_lits.end(); ++itr)
        {
            temp.push_back (*itr);
        }
        SAT_AddClause(mng, & temp.begin()[0], temp.size());//, current_group_id );
        
        clause_lits.clear();
        clause_vars.clear();
    }
    
    fake_counter = 1;

    // for(std::set<std::pair<int, int> >::iterator p = positive.begin()
    //         ; p != positive.end()
    //         ; p++){
    //     assert(ayplan_vars.find(p->first) != ayplan_vars.end());
    //     assert(ayplan_vars.find(p->second) != ayplan_vars.end());
        

    //     var_idx = ayplan_vars[p->first];
    //     sign = 0;
    //     if( var_idx < 0)  { var_idx = -var_idx; sign = 1; }
    //     clause_vars.insert(var_idx);
    //     clause_lits.insert( (var_idx << 1) + sign);
    //     var_idx = ayplan_vars[p->second];
    //     sign = 0;
    //     if( var_idx < 0)  { var_idx = -var_idx; sign = 1; }
    //     clause_vars.insert(var_idx);
    //     clause_lits.insert( (var_idx << 1) + sign);
        
    //     cerr<<" Adding clause :: "<<ayplan_vars[p->first] <<" or "<<ayplan_vars[p->second]<<std::endl;
    //     vector <int> temp;
    //     for (set<int>::iterator itr = clause_lits.begin();
    //          itr != clause_lits.end(); ++itr)
    //     {
    //         temp.push_back (*itr);
    //     }
    //     SAT_AddClause(mng, & temp.begin()[0], temp.size(), current_group_id );
        
    //     clause_lits.clear();
    //     clause_vars.clear();
    // }
    //exit(0);
}

bool initialised_manager = false;
SAT_Manager* __mng;
SAT_Manager mng;

#include <cmath>

// int ayPlan_main_NSAT(CNF& completed_CNF){

//     if(!initialised_manager){
//         mng = SAT_InitManager();
//         // cerr<<"Initialising manager.\n";
//         // __mng = new SAT_Manager();
//         // __mng = SAT_InitManager();
//         // initialised_manager = true;
//     }
    
//     //current_group_id = SAT_AllocClauseGroupID(mng);
    
//     //1//cerr<<"Group ID for this problem is :: "<<current_group_id<<std::endl;


//     read_cnf (mng, completed_CNF );//, positive);

    
//     int result = SAT_Solve(mng);

//     long double solutions = SAT_NumSolutions(mng);
    
//     int solution_count = ceil(solutions);
//     //1//cerr<<"Number of solutions is :: "<<solution_count<<" -- "<<solutions<<std::endl;
//     //exit(0);
//     //SAT_DeleteClauseGroup(mng, current_group_id);
//     SAT_Reset(mng);
    
//     return ceil(solutions);
// }


int ayPlan_main_2SAT(string name, 
                     std::set<int > freeVariables,
                std::set<std::pair<int, int> >& negative, 
                std::set<std::pair<int, int> >& equivalent){//, std::set<std::pair<int, int> >& positive){


    int answer = write_cnf(mng,  freeVariables, negative, equivalent, name);
    return answer;
    
    // if(!initialised_manager){
    //     mng = SAT_InitManager();
    //     // cerr<<"Initialising manager.\n";
    //     // __mng = new SAT_Manager();
    //     // __mng = SAT_InitManager();
    //     initialised_manager = true;
    // }
    // // SAT_Manager& mng = *__mng;
    // //SAT_Manager mng = SAT_InitManager();
    // //current_group_id = SAT_AllocClauseGroupID(mng);
    
    // //1//cerr<<"Group ID for this problem is :: "<<current_group_id<<std::endl;


    // read_cnf (mng,  freeVariables, negative, equivalent);//, positive);

    
    // int result = SAT_Solve(mng);

    // long double solutions = SAT_NumSolutions(mng);
    // //cerr << "Number of solutions: " << _stats.num_solutions << endl;
    // cerr<<"Number of solutions is :: "<<solutions<<std::endl;
    // //exit(0);
    // //SAT_DeleteClauseGroup(mng, current_group_id);
    // SAT_Reset(mng);
    
    // return solutions;
	// cout << "Number of solutions: " << _stats.num_solutions << endl;
}

// int main(int argc, char ** argv)
// {
//     SAT_Manager mng = SAT_InitManager();
// 	bool quiet = false;	// for output control
// 	bool static_heuristic = false;
//     if (argc < 2) {
// 	cout << "cachet version 1.2, December 2005" << endl
// 		 << "copyright 2005, University of Washington" << endl
// 		 << "incorporating code from zchaff, copyright 2004, Princeton University" << endl
// 		 << "Usage: "<< argv[0] << " cnf_file [-t time_limit]" << endl
// 		 << " [-c cache_size]" 
// 		 << " [-o oldest_cache_entry]" 
// 		 << " [-l cache_clean_limit]" 
// 		 << " [-h heuristic_selection]"
// 		 << " [-b backtrack_factor]"
// 		 << " [-f far_backtrack_enabled]"
// 		 << " [-r cross_implication_off] "
// 		 << " [-a adjusting_component_ordering_off] "
// 		 << " [-n max_num_learned_clause] " 
// 		 << " [-q quiet] " << endl;
// 	return 2;
//     }
// 	cout << "cachet version 1.2, December 2005" << endl
// 		 << "copyright 2005, University of Washington" << endl
// 		 << "incorporating code from zchaff, copyright 2004, Princeton University" << endl;
//     cout <<"Solving " << argv[1] << " ......" << endl;
//     //if (argc == 2) {
// 	//read_cnf (mng, argv[1] );
//     //}
//     //else {
// 	//read_cnf (mng, argv[1] );
// 	//SAT_SetTimeLimit(mng, atoi(argv[2]));
//     //}

// 	read_cnf (mng, argv[1] );
// 	int current = 1, option;
// 	while (++current < argc)
// 	{
// 		switch (argv[current][1])
// 		{
// 			case 't':	SAT_SetTimeLimit(mng, atoi(argv[++current]));
// 						break;
// 			case 'c':	SAT_SetCacheSize(mng, atoi(argv[++current]));
// 						break;
// 			//case 'e':	SAT_SetMaxEntry(mng, atoi(argv[++current]));
// 			//			break;
// 			//case 'm':	SAT_SetMaxDistance(mng, atoi(argv[++current]));
// 			//			break;
// 			//case 'd':	SAT_SetDynamicHeuristic(mng, true);
// 			//			break;
// 			case 's':	SAT_SetStaticHeuristic(mng, true);	// false -> true
// 						static_heuristic = true;
// 						break;
// 			case 'h':	option = atoi(argv[++current]);
// 						if (option < 0 || option > 7)
// 						{
// 							cout << "invalid heuristic selection, must be between 0 and 7" << endl;
// 							exit(0);
// 						}
// 						SAT_SetDynamicHeuristic(mng, option);
// 						break;
// 			case 'n':	SAT_SetMaxNumLearnedClause(mng, atoi(argv[++current]));
// 						break;
// 			case 'o':	SAT_SetOldestEntry(mng, atoi(argv[++current]));
// 						break;
// 			case 'l':	SAT_SetCleanLimit(mng, atoi(argv[++current]));
// 						break;
// 			case 'r':	SAT_SetCrossFlag(mng, false);	// true -> false
// 						break;
// 			case 'a':	SAT_SetAdjustFlag(mng, false);	// true -> false
// 						break;
// 			case 'b':	SAT_SetBacktrackFactor(mng, atof(argv[++current]));
// 						break;
// 			case 'f':	SAT_SetFarBacktrackFlag(mng, true);
// 						break;
// 			case 'q':	SAT_SetQuietFlag(mng, true);	// true -> false
// 						quiet = true;
// 						break;
// 			default:	cout << "unkonwn option! " << argv[current] << endl;
// 						exit(0);
// 		}	// end switch
// 	}
//     int result = SAT_Solve(mng);
// 	// cout << "Number of solutions: " << _stats.num_solutions << endl;
// #ifndef KEEP_GOING
// //    if (result == SATISFIABLE)
// //		verify_solution(mng);
// #endif
//     handle_result (mng, result,  argv[1], quiet);
//     return 0;
// }
