#include "globals.h"

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <limits>
#include <string>
#include <vector>
#include <sstream>
#include <sys/wait.h>
using namespace std;

#include "axioms.h"
#include "causal_graph.h"
#include "domain_transition_graph.h"
#include "operator.h"
#include "state.h"
#include "successor_generator.h"
#include "timer.h"
#include "heuristic.h"

bool test_goal(const State &state) {
    for (int i = 0; i < g_goal.size(); i++) {
        if (state[g_goal[i].first] != g_goal[i].second) {
            return false;
        }
    }
    return true;
}

int calculate_plan_cost(const vector<const Operator *> &plan) {
    // TODO: Refactor: this is only used by save_plan (see below)
    //       and the SearchEngine classes and hence should maybe
    //       be moved into the SearchEngine (along with save_plan).
    int plan_cost = 0;
    for (int i = 0; i < plan.size(); i++) {
        plan_cost += plan[i]->get_cost();
    }
    return plan_cost;
}

void save_plan(const vector<const Operator *> &plan, int iter) {
    // TODO: Refactor: this is only used by the SearchEngine classes
    //       and hence should maybe be moved into the SearchEngine.
    ofstream outfile;
    if (iter == 0) {
        outfile.open(g_plan_filename.c_str(), ios::out);
    } else {
        ostringstream out;
        out << g_plan_filename << "." << iter;
        outfile.open(out.str().c_str(), ios::out);
    }
    for (int i = 0; i < plan.size(); i++) {
        cout << plan[i]->get_name() << " (" << plan[i]->get_cost() << ")" << endl;
        outfile << "(" << plan[i]->get_name() << ")" << endl;
    }
    outfile.close();
    int plan_cost = calculate_plan_cost(plan);
    ofstream statusfile;
    statusfile.open("plan_numbers_and_cost", ios::out|ios::app);
    statusfile << iter << " " << plan_cost << endl;
    statusfile.close();
    cout << "Plan length: " << plan.size() << " step(s)." << endl;
    cout << "Plan cost: " << plan_cost << endl;
}

bool peek_magic(istream &in, string magic) {
    string word;
    in >> word;
    bool result = (word == magic);
    for (int i = word.size() - 1; i >= 0; i--)
        in.putback(word[i]);
    return result;
}

void check_magic(istream &in, string magic) {
    string word;
    in >> word;
    if (word != magic) {
        cerr << "Failed to match magic word '" << magic << "'." << endl;

        cerr << "Got '" << word << "'." << endl;
        while(true){
            cerr<<word<<std::endl;
            in >> word;
        }

        exit(1);
    }
}

void read_metric(istream &in) {
    check_magic(in, "begin_metric");
    in >> g_use_metric;
    check_magic(in, "end_metric");
}

void read_variables(istream &in) {
    cerr<<"Checking magic in read_variables.\n";
    check_magic(in, "begin_variables");
    int count;
    in >> count;
    for (int i = 0; i < count; i++) {
        string name;
        in >> name;
        g_variable_name.push_back(name);
        int range;
        in >> range;
        g_variable_domain.push_back(range);
        if (range > numeric_limits<state_var_t>::max()) {
            cout << "Problem with the range of a variable!" << endl;
            //exit(1);
        }
        int layer;
        in >> layer;
        g_axiom_layers.push_back(layer);
    }
    check_magic(in, "end_variables");
}

void read_goal(istream &in) {
    check_magic(in, "begin_goal");
    int count;
    in >> count;
    for (int i = 0; i < count; i++) {
        int var, val;
        in >> var >> val;
        g_goal.push_back(make_pair(var, val));
    }
    check_magic(in, "end_goal");
}

void dump_goal() {
    cout << "Goal Conditions:" << endl;
    for (int i = 0; i < g_goal.size(); i++)
        cout << "  " << g_variable_name[g_goal[i].first] << ": "
             << g_goal[i].second << endl;
}

#include "jussi_counting.hh"

void read_operators__CNF_counting()
{
    using namespace  Planning;
    SCCisolation discovery(g_operators, g_plan_filename);
    // SCChashsat discovery(g_operators);
    
    discovery.discoverVariables();
    cerr<<"Made variables discovery.\n";
    //discovery.action_inverses();
    cerr<<"Calculated action inverses.\n";

    // exit(0);

    discovery.initialiseFreeVariables();
    cerr<<"Initialising free variables.\n";
    
    discovery.initialise__occurs_positively_as_effect_without();
    discovery.initialise__occurs_negatively_as_effect_without();
    cerr<<"Initialised variable's co-occurrence.\n";
    discovery.discoverGraph();
    cerr<<"Discovered.\n";
    int i = discovery.connectedComponents();
    cerr<<"Components = "<<i<<"\n";
    discovery.sasplus_mutex();
    //discovery.processAllMutex();
    cerr<<"Processed mutex.\n";
    discovery.componentsGraph();
    cerr<<"Components graph.\n";

    discovery.computeComponentAllMutexes();
    cerr<<"Component mutex count available.\n";
    discovery.computeComponentEquivalence();
    cerr<<"Components variables that are logically equivalent.\n";
    discovery.freeVariablesByComponent();
    cerr<<"Calculated the free variables for each component.\n";

    
    discovery.orderComponents();
    cerr<<"Ordered components.\n";
    discovery.writeComponentOrder();
    cerr<<"Written components order.\n";

    discovery.resetLength();
    // // cout<<"Maximum parallel plan length is "<<discovery.factoredMaximumPlanLength(true)<<"\n";
    // // cout<<"Reported maximum parallel plan length\n";
    // // discovery.resetLength();
    // // cout<<"Maximum serial plan length is "<<discovery.factoredMaximumPlanLength(false)<<"\n";
    cout<<"Reported maximum serial plan length\n";
    printf("%d\r\n\r\n\r\n", discovery.connectedCliques());
    discovery.writeToGraphviz();
    cerr<<"Written.\n";

    cout<<"All finished.\n";
    /*Jussi counting -- Breakout.*/
    exit(0);
}

void read_operators__simple_counting()
{
    using namespace  Planning;
    SCCDiscovery discovery(g_operators, g_plan_filename);
    
    discovery.discoverVariables();
    cerr<<"Made variables discovery.\n";

    discovery.initialiseFreeVariables();
    cerr<<"Initialising free variables.\n";
    
    discovery.initialise__occurs_positively_as_effect_without();
    discovery.initialise__occurs_negatively_as_effect_without();
    cerr<<"Initialised variable's co-occurrence.\n";
    discovery.discoverGraph();
    cerr<<"Discovered.\n";
    int i = discovery.connectedComponents();
    cerr<<"Components = "<<i<<"\n";
    discovery.processAllMutex();
    cerr<<"Processed mutex.\n";
    discovery.componentsGraph();
    cerr<<"Components graph.\n";

    discovery.computeComponentAllMutexes();
    cerr<<"Component mutex count available.\n";
    discovery.computeComponentEquivalence();
    cerr<<"Components variables that are logically equivalent.\n";
    discovery.freeVariablesByComponent();
    cerr<<"Calculated the free variables for each component.\n";

    
    discovery.orderComponents();
    cerr<<"Ordered components.\n";
    discovery.writeComponentOrder();
    cerr<<"Written components order.\n";

    cout<<"Maximum serial plan length is "<<discovery.maximumPlanLength(false)<<"\n";
    discovery.resetLength();
    cout<<"Maximum parallel plan length is "<<discovery.maximumPlanLength(true)<<"\n";
    discovery.writeToGraphviz();
    cerr<<"Written.\n";
    
    /*Jussi counting -- Breakout.*/
    exit(0);
}


void read_operators(istream &in) {
    int count;
    in >> count;
    for (int i = 0; i < count; i++){
        g_operators.push_back(Operator(in, false));
    }


    
    cerr<<"Got "<<count<<" operators.\n";
    // if(mutex_only){
    //     read_operators__simple_counting();
    // } else {
    read_operators__CNF_counting();
    // }
}

void read_axioms(istream &in) {
    int count;
    in >> count;
    for (int i = 0; i < count; i++)
        g_axioms.push_back(Operator(in, true));

    g_axiom_evaluator = new AxiomEvaluator;
    g_axiom_evaluator->evaluate(*g_initial_state);
}

void read_everything(istream &in) {

    if (peek_magic(in, "begin_metric")) {
        read_metric(in);
        g_legacy_file_format = false;
    } else {
        g_use_metric = false;
        g_legacy_file_format = true;
    }
    cerr<<"Read the metric.\n";

    read_variables(in);
    g_initial_state = new State(in);
    read_goal(in);
    read_operators(in);
    read_axioms(in);
    check_magic(in, "begin_SG");
    g_successor_generator = read_successor_generator(in);
    check_magic(in, "end_SG");
    DomainTransitionGraph::read_all(in);
    g_causal_graph = new CausalGraph(in);
}

void dump_everything() {
    cout << "Use metric? " << g_use_metric << endl;
    cout << "Min Action Cost: " << g_min_action_cost << endl;
    cout << "Max Action Cost: " << g_max_action_cost << endl;
    cout << "Variables (" << g_variable_name.size() << "):" << endl;
    for (int i = 0; i < g_variable_name.size(); i++)
        cout << "  " << g_variable_name[i]
             << " (range " << g_variable_domain[i] << ")" << endl;
    cout << "Initial State:" << endl;
    g_initial_state->dump();
    dump_goal();
    /*
    cout << "Successor Generator:" << endl;
    g_successor_generator->dump();
    for(int i = 0; i < g_variable_domain.size(); i++)
      g_transition_graphs[i]->dump();
    */
}

bool g_legacy_file_format = false; // TODO: Can rip this out after migration.
bool g_use_metric;
int g_min_action_cost = numeric_limits<int>::max();
int g_max_action_cost = 0;
vector<string> g_variable_name;
vector<int> g_variable_domain;
vector<int> g_axiom_layers;
vector<int> g_default_axiom_values;
State *g_initial_state;
vector<pair<int, int> > g_goal;
vector<Operator> g_operators;
vector<Operator> g_axioms;
AxiomEvaluator *g_axiom_evaluator;
SuccessorGenerator *g_successor_generator;
vector<DomainTransitionGraph *> g_transition_graphs;
CausalGraph *g_causal_graph;
HeuristicOptions g_default_heuristic_options;

Timer g_timer;
string g_plan_filename = "sas_plan";
