#include "best_first_search.h"
#include "enforced_hill_climbing_search.h"
#include "g_evaluator.h"
#include "general_eager_best_first_search.h"
#include "general_lazy_best_first_search.h"
#include "globals.h"
#include "iterated_search.h"
#include "operator.h"
#include "option_parser.h"
#include "pref_evaluator.h"
#include "sum_evaluator.h"
#include "timer.h"
#include "utilities.h"
#include "weighted_evaluator.h"

#include <iostream>
#include <sstream>

using namespace std;


typedef std::vector<int> Hash_Array;
Hash_Array hash_Array;
#include<tr1/unordered_map>
#include<tr1/unordered_set>
std::tr1::unordered_map<state_var_t *, int> in_use;

//WITH BITSTATE HASHING 
int hash_array_size;
int maximum_g_value;
int frontier;
int use_hash_array;

void register_parsers();

int using_cachet = 0;
int mutex_only = 1;
int cut_limit = 0;

int main(int argc, const char **argv) {
    register_event_handlers();
    string usage =
        "usage: \n" +
        string(argv[0]) + " [OPTIONS] --search SEARCH < OUTPUT\n\n"
        "* SEARCH (SearchEngine): configuration of the search algorithm\n"
        "* OUTPUT (filename): preprocessor output\n\n"
        "Options:\n"
        "--landmarks LANDMARKS_PREDEFINITION\n"
        "    Predefines a set of landmarks that can afterwards be referenced\n"
        "    by the name that is specified in the definition.\n"
        "--heuristic HEURISTIC_PREDEFINITION\n"
        "    Predefines a heuristic that can afterwards be referenced\n"
        "    by the name that is specified in the definition.\n"
        "--random-seed SEED\n"
        "    Use random seed SEED\n\n"
        "--plan-file FILENAME\n"
        "    Plan will be output to a file called FILENAME\n\n"
        "See http://www.fast-downward.org/ for details.";

    if (argc < 2) {
        cout << usage << endl;
        exit(1);
    }

    // read prepropressor output first because we need to know the initial
    // state when we create a general lazy search engine
    bool poly_time_method = false;

    istream &in = cin;

    in >> poly_time_method;
    if (poly_time_method) {
        cout << "Poly-time method not implemented in this branch." << endl;
        cout << "Starting normal solver." << endl;
    }


    for (int i = 4; i < argc; ++i) {
        string arg = string(argv[i]);
        if (arg.compare("--cachet") == 0) {
            ++i;
            using_cachet = atoi(argv[i]);


            cerr<<"Using Cachet is :: "<<using_cachet<<endl;
        } 
        else if (arg.compare("--mutex-only") == 0) {
            ++i;
            mutex_only = atoi(argv[i]);

            cerr<<"Mutex only is :: "<<mutex_only<<endl;
        } else if (arg.compare("--plan-file") == 0) {
            ++i;
            g_plan_filename = argv[i];
            cerr<<g_plan_filename<<std::endl;
        } else if (arg.compare("--cut-limit") == 0) {
            ++i;
	    cut_limit = atoi(argv[i]);
            cout << "Cut limit " << cut_limit << endl;
        }
    }
    cerr<<"About to read everything!\n";
    read_everything(in);


    
    {//WITH BITSTATE HASHING
        hash_array_size = 3;
        maximum_g_value = 1000000000;
        frontier = 0;

        {
            
            std::cerr<<"Here"<<std::endl;
            istringstream iss(argv[1]);
            std::cerr<<"Here"<<std::endl;
            iss>>hash_array_size;
            std::cerr<<"Here array size :: "<<hash_array_size<<std::endl;
        }
        {
            
            std::cerr<<"Here"<<std::endl;
            istringstream iss(argv[2]);
            std::cerr<<"Here"<<std::endl;
            iss>>maximum_g_value;
            std::cerr<<"Here max Gs :: "<< maximum_g_value<<std::endl; 
        }
        {
            
            std::cerr<<"Here"<<std::endl;
            istringstream iss(argv[3]);
            std::cerr<<"Here"<<std::endl;
            iss>>frontier;
            std::cerr<<"Here max Gs :: "<<frontier<<std::endl; 
        }
        
    }
    
    register_parsers();
    SearchEngine *engine = 0;

    for (int i = 4; i < argc; ++i) {
        string arg = string(argv[i]);
        if (arg.compare("--cachet") == 0) {
            ++i;
            using_cachet = atoi(argv[i]);

            exit(-1);

            cerr<<"Using Cachet is :: "<<using_cachet<<endl;sleep(1);
        } 
        else if (arg.compare("--mutex-only") == 0) {
            ++i;
            mutex_only = atoi(argv[i]);

            exit(-1);
            cerr<<"Mutex only is :: "<<mutex_only<<endl;sleep(1);
        } 
        else if (arg.compare("--heuristic") == 0) {
            ++i;
            OptionParser::instance()->predefine_heuristic(argv[i]);
        } else if (arg.compare("--landmarks") == 0) {
            ++i;
            OptionParser::instance()->predefine_lm_graph(argv[i]);
        } else if (arg.compare("--search") == 0) {
            ++i;
            engine = OptionParser::instance()->parse_search_engine(argv[i]);
        } else if (arg.compare("--random-seed") == 0) {
            ++i;
            srand(atoi(argv[i]));
            cout << "random seed " << argv[i] << endl;
        } else if (arg.compare("--plan-file") == 0) {
            ++i;
            g_plan_filename = argv[i];
            cerr<< g_plan_filename<<std::endl;
            exit(-1);
        } else {
            cerr << "unknown option " << arg << endl << endl;
            cout << usage << endl;
            exit(1);
        }
    }

    Timer search_timer;
    engine->search();
    search_timer.stop();
    g_timer.stop();

    engine->save_plan_if_necessary();
    engine->statistics();
    engine->heuristic_statistics();
    cout << "Search time: " << search_timer << endl;
    cout << "Total time: " << g_timer << endl;

    return engine->found_solution() ? 0 : 1;
}

void register_parsers() {
    // Register search engines
    OptionParser::instance()->register_search_engine("eager",
                                                     GeneralEagerBestFirstSearch::create);
    OptionParser::instance()->register_search_engine("astar",
                                                     GeneralEagerBestFirstSearch::create_astar);
    OptionParser::instance()->register_search_engine("eager_greedy",
                                                     GeneralEagerBestFirstSearch::create_greedy);
    OptionParser::instance()->register_search_engine("lazy",
                                                     GeneralLazyBestFirstSearch::create);

    //WITH BITSTATE HASHING -- This is the one we care about.
    OptionParser::instance()->register_search_engine("lazy_greedy",
                                                     GeneralLazyBestFirstSearch::create_greedy);

    
    OptionParser::instance()->register_search_engine("lazy_wastar",
                                                     GeneralLazyBestFirstSearch::create_weighted_astar);
    OptionParser::instance()->register_search_engine("ehc",
                                                     EnforcedHillClimbingSearch::create);
    OptionParser::instance()->register_search_engine("old_greedy",
                                                     BestFirstSearchEngine::create);
    OptionParser::instance()->register_search_engine("iterated",
                                                     IteratedSearch::create);

    // register combinations and g evaluator
    // (Note: some additional ones are already registered as plugins.)
    OptionParser::instance()->register_scalar_evaluator("weight",
                                                        WeightedEvaluator::create);
    OptionParser::instance()->register_scalar_evaluator("g",
                                                        GEvaluator::create);
    OptionParser::instance()->register_scalar_evaluator("pref",
                                                        PrefEvaluator::create);

    // Note:
    // open lists are registered in the constructor of OpenListParser.
    // This is necessary because the open list entries are specified
    // as template parameter and we would have template parameters everywhere
    // otherwise
}


#include "./open_lists/alternation_open_list.h"


extern bool GLOBAL__reopen;

template<>
int AlternationOpenList<OpenListEntryLazy>::insert(const OpenListEntryLazy &entry) {
  //cerr<<"do-3";
  if(hash_array_size > 0){
    //WITH BITSTATE HASHING -- Initialisation of bitstate.
    if(hash_Array.size() == 0){
        cerr<<"**";
        std::cerr<</*GLOBAL*/hash_array_size<<std::endl;
        hash_Array = vector<int>(/*GLOBAL*/hash_array_size);
        for(int i = 0; i < hash_Array.size(); i++){
            hash_Array[i] = 0;
        }
    }
    
    //WITH BITSTATE HASHING 
    if(!GLOBAL__reopen){
        size_t number = entry.hash_value();
        number = number % hash_Array.size();   
        if(hash_Array[number]){
            // cerr<<".";
            //cerr<<"do-4";
            return 0;
        } 
    }
    // else {
    //     cerr<<"Bollocks!\n";
    //     exit(0);
    // }
    // cerr<<"&";
    //hash_Array[number] = 1;
  }

    
    
    
    int new_entries = 0;
    for (size_t i = 0; i < open_lists.size(); i++)
        if (!open_lists[i]->is_dead_end())
            new_entries += open_lists[i]->insert(entry);
    size += new_entries;


    //cerr<<"do-4";
    return new_entries;
}


#include "./open_lists/standard_scalar_open_list.h"


typedef unsigned char* Thingi2;
int _TieBreakingOpenList::insert(const Thingi2& entry) {

    if (OpenList<unsigned char*>::only_preferred && !last_preferred)
        return 0;
    if (first_is_dead_end && allow_unsafe_pruning) {
        return 0;
    }

    if(hash_array_size > 0){
        //WITH BITSTATE HASHING -- Initialisation of bitstate.
        if(hash_Array.size() == 0){
            cerr<<"&&";
            std::cerr<</*GLOBAL*/hash_array_size<<std::endl;
            hash_Array = vector<int>(/*GLOBAL*/hash_array_size);
            for(int i = 0; i < hash_Array.size(); i++){
                hash_Array[i] = 0;
            }
        }

        // if(false){//!GLOBAL__reopen){
        //     size_t hash = 0; 
        //     // const unsigned char* _entry = entry;
        //     for (uint i =0; i < g_variable_domain.size(); i++){
        //         boost::hash_combine(hash, boost::hash_value(entry[i]));
        //     }
            
        //     // while(_entry != 0){boost::hash_combine(hash, boost::hash_value(*_entry++));};
            
        //     //WITH BITSTATE HASHING 
        //     hash = hash % hash_Array.size();   
        //     if(hash_Array[hash]){
        //         // cerr<<"o";
        //         //cerr<<"do-2";
        //         return 1;//0;
        //     }
        //     hash_Array[hash] = 1;
        // }
    }
        
    const std::vector<int> &key = last_evaluated_value;
    buckets[key].push_back(entry);
    size++;
    return 1;
}

template<>
int StandardScalarOpenList<OpenListEntryLazy>::insert(const OpenListEntryLazy &entry) {

  //cerr<<"do-1";

  if(hash_array_size > 0){

    //WITH BITSTATE HASHING -- Initialisation of bitstate.
    if(hash_Array.size() == 0){
        cerr<<"&&";
        std::cerr<</*GLOBAL*/hash_array_size<<std::endl;
        hash_Array = vector<int>(/*GLOBAL*/hash_array_size);
        for(int i = 0; i < hash_Array.size(); i++){
            hash_Array[i] = 0;
        }
    }
    
    //WITH BITSTATE HASHING 
    size_t number = entry.hash_value();
    number = number % hash_Array.size();   
    if(hash_Array[number]){
        // cerr<<"o";
      //cerr<<"do-2";
        return 0;
    }
    // cerr<<"#";
    // exit(0);
    hash_Array[number] = 1;
  }
    

    if (OpenList<OpenListEntryLazy>::only_preferred && !last_preferred){
      //cerr<<"do-2";
      return 0;}
    if (dead_end) {
      //cerr<<"do-2";
        return 0;
    }
    int key = last_evaluated_value;
    if (key < lowest_bucket)
        lowest_bucket = key;
    buckets[key].push_back(entry);
    size++;

    //cerr<<"do-2";
  
    return 1;
}
