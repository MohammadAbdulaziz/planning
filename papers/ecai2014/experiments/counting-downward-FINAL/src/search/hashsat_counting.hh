
extern int ayPlan_main_NSAT(std::set<std::set<int> >& completed_CNF);

namespace Planning
{

    class SCChashsat : public SCCDiscovery {
    public:
        typedef set<int> Clause;
        typedef set<Clause> CNF;

        typedef set<int> MultiComponent;
        typedef set<MultiComponent> SetOfMultiComponents;
        typedef map<int,  MultiComponent> ComponentToMultiComponent;
        typedef map<MultiComponent,  SetOfMultiComponents> MultiComponentGraph;
        struct MultiComponentAnalysis {
            /*each of these should be the predecessor of a component being analysed.*/
            MultiComponentGraph graph;
            ComponentToMultiComponent joins;
            SetOfMultiComponents keys;
            bool completed;

            bool operator!=(const MultiComponentAnalysis& in) const {
                return in.graph !=  graph ||
                    in.joins != joins ||
                    in.keys != keys ||
                    in.completed != completed;
                    
            }
            bool operator==(const MultiComponentAnalysis& in) const {
                return in.graph ==  graph &&
                    in.joins == joins &&
                    in.keys == keys &&
                    in.completed == completed;
                    
            }
        };
    
        class MultiComponentTree{
        public:
            MultiComponentTree(){}
            MultiComponentTree(const MultiComponent& in ):node_contents(in)
            {}
        
            void push_child(MultiComponentTree& in){
                children.push_back(in);
            }

            MultiComponent node_contents;
            vector<MultiComponentTree> children;
        };

        MultiComponentAnalysis allIntersections(map<int, set<int> >& per_component_candidate_cut_points);
        set<int> getCandidateCutPoints(int pred_component, int component_id);
        MultiComponentTree getPredecessorOrders(set<int>& components);
        bool traverseCutGraph(MultiComponent& key, 
                              SetOfMultiComponents& visited, 
                              MultiComponentTree& answer, 
                              MultiComponentGraph& per_component_component_intersections);
        int factoredClusteredMaximumPlanLength__allMutex(set<int>& components,  int component_id, bool parallel = true);
        int descend_tree(MultiComponentTree& mct, int component_id);




        SCChashsat(vector<Operator>& problem, string name):SCCDiscovery(problem, name){};

        
        void discoverGraph();
        int  worstCaseModelCount(int component, 
                                         int predecessor, 
                                         int vars, 
                                         int mutexes);
        int  worstCaseModelCount(int component, 
                                 set<int>& predecessors, 
                                         int vars, 
                                         int mutexes);
        

        int factored_plan_length_with_non_interfering_predecessors(int component_id, set<int>& predecessor_components, bool parallel = true);
        int factored_plan_length_with_interfering_predecessors(int component_id, set<int>& predecessor_components, bool parallel = true);
        int factoredMaximumPlanLength(set<int>& layer, bool parallel);
        int simpleFactoredMaximumPlanLength(set<int>& layer, bool parallel);
        int factoredMaximumPlanLength(bool parallel);
        int factoredMaximumPlanLength(int i, bool parallel);
        int factoredClusteredMaximumPlanLength(set<int>& layer, bool parallel);
        void report_to_cluster_of_components(int comp, map<set<int>, set<int> >& clusters_of_components);
        
        int setup_ayplan_action_vars(int component);
        int report_edge(int action_id, int a, int b, bool is_a_delete_edge);
        int report_add_edge(int action_id, int a, int b);
        int report_delete_edge(int action_id, int a, int b);
        int setup_ayplan_vars(int component, 
                              std::set<int> freeVariables, 
                              std::set<std::pair<int, int> >& mutex, 
                              std::set<std::pair<int, int> >& equiv);
        /* CNF building :: Returns the size of the goal clause for the component.*/
        int calculate_goal_implications_in_last_layer();
        int setup_ayplan_actions_vars(int component);
        /* Arguments are ayPlan indices. */
        int sat_action_implies_edge(int action_id, int edge_id);
        int sat_edge_implies_action(int edge_id, int action_id);
        int sat_variable_implies_edge(int variable, int edge_id, int sign = 1);
        int sat_edge_implies_variable(int edge_id, int variable, int sign = 1);
        int complete_variable_to_implied_edges();
        int complete_edge_to_implied_actions();
        int exactly_one_action();
        int  complete_cnf();
        int  print_CNF(CNF& );
        int SAT_AddVariable();
        /*****/int compileSATforComponent(int component, 
                                          std::set<int> freeVariables, 
                                          std::set<std::pair<int, int> >& mutex, 
                                          std::set<std::pair<int, int> >& equiv,
                                          int predecessor = -1);/*****/
        
        /* DEBUG */
        //void allvarspos();
        void actions_noop_processing();
        
        /* Experimental unit prop processing for component actions.*/
        int count_all_mutex_with_predecessor(int component, int predecessor);
        int count_all_mutex_with_predecessor(int component, set<int>& predecessor);
        void  actions_prev_component_processing(int component_id, int predecessor = -1);
        bool  is_foreign_action(int action_id, int component_id, int predecessor = -1);
        bool is_foreign_fluent(int fluent_id, int component_id, int predecessor = -1);
        void  fluents_prev_component_processing(int component_id, int predecessor = -1);


        bool non_interfering_components(int c1, int c2);
        bool non_interfering_predecessors(int component);
        
        map<pair<int, int>, bool > cached__non_interfering_components;
      
        /* A noop is a special action associated with each fluent. It
         * gives the fluent an excuse to take on any value it
         * likes. */
        vector< set<int> > action_to_noops;
        vector< set<int> > noops_to_action;
        map<int, int> sat_noop;
        map<int, int> neg_sat_noop;/*fluent to SAT noop action*/
        map<int, int> sat_fluent_to_noop;/*fluent to SAT noop action*/
      
        std::map<int, int> ayplan_noop_vars;
        std::map<int, int> ayplan_vars;/*Fluents*/
        std::map<int, int> ayplan_action_vars;
        std::map<int, int> ayplan_edge_vars;
 
        /* Used to keep track of the largest variable index in a SAT instance.*/
        int variable_id;// = 0;

        /* Largest index associated with an action. */
        int largest_action_id;


        /* First int is an edge, rest are variable.*/
        CNF free_variables;/*2SAT*/
        CNF mutex_variables;/*2SAT*/
        CNF equiv_variables;/*2SAT*/
        CNF noop_clauses;/* 2SAT*/

        CNF only_one_action;/*2SAT*/
        CNF edge_implies_variable__clauses;/*2SAT*/
        CNF action_implies_edge__clauses;/*2SAT*/
        map<int, Clause> edge_to_implied_actions;
        map<int, Clause> variable_to_implied_edges;
        CNF edge_to_implied_actions__clauses;/*NSAT*/
        CNF variable_to_implied_edges__clauses;/*NSAT*/
        CNF goals_in_last_layer;/*NSAT*/

        /* #SAT CNF representation of an SCC state count problem.*/
        set<int> all_CNF_variables;
        CNF completed_CNF;
        
        typedef std::pair<int, int> SAT_edge;
        std::set<SAT_edge> _sat_edges;
        std::set<SAT_edge> _sat_neg_edges;
        std::vector<SAT_edge> sat_edges;
        std::map<SAT_edge, int> sat_edge_id;
        std::map<SAT_edge, int> sat_neg_edge_id;

        map<int, set<int> > action_to_edges;
        map<int, set<int> > edge_to_actions;
        
        /* Components traversed by last call to \member{factoredMaximumPlanLength}*/
        set<int> traversed;

        
        /*Very-Parallel Plan length.*/
        map<int,int > clustered_component_cost;
    };



    int SCChashsat::report_edge(int action_id, int a, int b, bool del_edge){
        SAT_edge edge(a, b);
        int old_size = sat_edges.size();

        
        if(action_to_edges.find(action_id) == action_to_edges.end()){
            action_to_edges[action_id] =set<int>();
        }

        if(!del_edge){
            
            if(_sat_edges.find(edge) == _sat_edges.end()){
                _sat_edges.insert(edge);
                sat_edge_id[edge] = old_size;
                sat_edges.push_back(edge);
            }

            if(edge_to_actions.find(sat_edge_id[edge]) == edge_to_actions.end()){
                edge_to_actions[sat_edge_id[edge]] =set<int>();
            }
            action_to_edges[action_id].insert(sat_edge_id[edge]);
            edge_to_actions[sat_edge_id[edge]].insert(action_id);
            
            return  sat_edge_id[edge];
        } else if (del_edge){
            if(_sat_neg_edges.find(edge) == _sat_neg_edges.end()){
                _sat_neg_edges.insert(edge);
                sat_neg_edge_id[edge] = old_size;
                sat_edges.push_back(edge);
            }

            if(edge_to_actions.find(sat_neg_edge_id[edge]) == edge_to_actions.end()){
                edge_to_actions[sat_neg_edge_id[edge]] =set<int>();
            }
            action_to_edges[action_id].insert(sat_neg_edge_id[edge]);
            edge_to_actions[sat_neg_edge_id[edge]].insert(action_id);
            
            return  sat_neg_edge_id[edge];
        }
        assert(0);

        return 0;
        
    }
    int SCChashsat::report_add_edge(int action_id, int a, int b){
        return report_edge(action_id, a, b, false);
    }

    int SCChashsat::report_delete_edge(int action_id, int a, int b){
        return report_edge(action_id, a, b, true);
    }
    

    void SCChashsat::report_to_cluster_of_components(int comp, map<set<int>, set<int> >& clusters_of_components)
    {
        if(predecessor_components.find(comp) == predecessor_components.end())return;
        set<int>& predecessors = predecessor_components[comp];

        if(clusters_of_components.find(predecessors) == clusters_of_components.end()){
            clusters_of_components[predecessors] = set<int>();
        }
        
        clusters_of_components[predecessors].insert(comp);
    }
    

    /*REVIEW FROM HERE.*/

     SCChashsat::MultiComponentAnalysis SCChashsat::allIntersections(map<int, set<int> >& per_component_candidate_cut_points)
     {
         MultiComponentAnalysis answer;
         answer.completed = false;
         MultiComponentGraph& graph = answer.graph;
         ComponentToMultiComponent& joins = answer.joins;
         set<MultiComponent >& keys = answer.keys;
        
        for(map<int, set<int> >::iterator loop1 = per_component_candidate_cut_points.begin()
                ; loop1 != per_component_candidate_cut_points.end()
                ; loop1++){
            if(joins.find(loop1->first) == joins.end()){
                joins[loop1->first] = set<int>();
            }
            joins[loop1->first].insert(loop1->first);

            map<int, set<int> >::iterator loop2 = loop1;
            for(loop2++
                    ; loop2 != per_component_candidate_cut_points.end()
                    ; loop2++){
                if(joins.find(loop2->first) == joins.end()){
                    joins[loop2->first] = set<int>();
                }
                joins[loop2->first].insert(loop2->first);

                vector<int> insersection(std::min(loop2->second.size(), loop1->second.size()));
                vector<int>::iterator points_to = set_intersection(loop1->second.begin(), 
                                                                   loop1->second.end()
                                                                   , loop2->second.begin(), 
                                                                   loop2->second.end()
                                                                   , insersection.begin());

                if(points_to == insersection.begin()) continue;
                vector<int>::iterator second = insersection.begin();
                second++;
                if(points_to == second) continue;
                
                
                joins[loop1->first].insert(loop2->first);
                joins[loop2->first].insert(loop1->first);
            }
        }


        /*the joins must be transitively closed for my analysis to work.*/
        bool transitive_closure_test_passed = true;
        for( map<int, set<int> >::iterator join =  joins.begin()
                 ; join != joins.end()
                 ; join++){
            set<int>& co_joins = join->second;
            keys.insert(co_joins);
            for(set<int>::iterator co_join = co_joins.begin()
                    ; co_join != co_joins.end()
                    ; co_join++){
                if(joins[*co_join] != joins[join->first]){
                    transitive_closure_test_passed = false;
                    break;
                }
            }
            if(!transitive_closure_test_passed)break;
        }

        if(!transitive_closure_test_passed){   
            return answer;
        }

        
        bool all_cojoins_intersect_canonically = true;
        for( set< set<int> >::iterator key1 =  keys.begin()
                 ; key1 != keys.end()
                 ; key1++){

            set< set<int> >::iterator key2 = key1;
            for( key2++
                     ; key2 != keys.end()
                     ; key2++){
                
                bool non_canonical_communications = false;
                bool communicating = true;
                for(set<int>::iterator comp1 = key1->begin()
                        ; comp1 != key1->end()
                        ; comp1++){
                    for(set<int>::iterator comp2 = key2->begin()
                            ; comp2 != key2->end()
                            ; comp2++){
                        assert( per_component_candidate_cut_points.find(*comp1) !=  per_component_candidate_cut_points.end());
                        assert( per_component_candidate_cut_points.find(*comp2) !=  per_component_candidate_cut_points.end());
                        set<int>& cut_points1 = per_component_candidate_cut_points[*comp1];
                        set<int>& cut_points2 = per_component_candidate_cut_points[*comp2];

                        vector<int> insersection(std::min(cut_points1.size(), cut_points2.size()));
                        vector<int>::iterator points_to = set_intersection(cut_points1.begin(), 
                                                                           cut_points1.end()
                                                                           , cut_points2.begin(), 
                                                                           cut_points2.end()
                                                                           , insersection.begin());

                        vector<int>::iterator second = insersection.begin();
                        second++;
                        
                        
                        /*First time in 2D loop.*/
                        if(comp1 == key1->begin() && comp2 == key2->begin()){
                            if(points_to == second){
                                if(graph.find(*key1) == graph.end()){
                                    graph[*key1] = set<MultiComponent>();
                                }
                                if(graph.find(*key2) == graph.end()){
                                    graph[*key2] = set<MultiComponent>();
                                }
                                graph[*key1].insert(*key2);
                                graph[*key2].insert(*key1);
                                communicating = true;
                            } else if (points_to == insersection.begin()){
                                communicating = false;
                            } else {
                                non_canonical_communications = true;
                                break;
                            }
                        } else {/*Nth time in 2D loop.*/
                            if(points_to == second && communicating == false){
                                non_canonical_communications = true;
                                break;
                            } else if (points_to == insersection.begin() && communicating == true){
                                non_canonical_communications = true;
                                break;
                            } else {
                                non_canonical_communications = true;
                                break;
                            }
                        }
                        
                    }
                    if(non_canonical_communications){
                        all_cojoins_intersect_canonically = false;
                        break;
                    }
                }
                if(!all_cojoins_intersect_canonically){
                    break;
                }
            }
            if(!all_cojoins_intersect_canonically){
                break;
            }
        }

        if(!all_cojoins_intersect_canonically){  
            return answer;
        }
            

        answer.completed = true;
        //assert(graph.size());
        return answer;

    }

    set<int> SCChashsat::getCandidateCutPoints(int pred_component, int component_id){
        assert(component_contents.find(pred_component) != component_contents.end());
        assert(component_contents.find(component_id) != component_contents.end());
        set<int> answer;
        
        set<int>& contents = component_contents[pred_component];
        for(set<int>::iterator variable = contents.begin()
                ; variable != contents.end()
                ; variable++){
            assert(vertex_edges.find(*variable) != vertex_edges.end());
            set<int>& successors = vertex_edges[*variable];

            for(set<int>::iterator successor = successors.begin()
                    ; successor != successors.end()
                    ; successor++){
                if(component_contents[component_id].find(*successor) != component_contents[component_id].end()){
                    answer.insert(*successor);
                }
            }
        }
        
        return answer;
    }

    SCChashsat::MultiComponentTree SCChashsat::getPredecessorOrders(set<int>& components){
        
        MultiComponentAnalysis per_component_component_intersections;
        
        for(set<int>::iterator component =  components.begin()
                ; component !=  components.end()
                ; component ++){
            int component_id = *component;
            assert(predecessor_components.find(component_id) != predecessor_components.end());
            set<int>& predecessors_for_component = predecessor_components[component_id];
            
            assert(predecessors_for_component.size() != 0);

            
            map<int, set<int> > per_component_candidate_cut_points;
            for(set<int>::iterator pred  = predecessors_for_component.begin()
                    ; pred != predecessors_for_component.end()
                    ; pred++){
                set<int> cut_points = getCandidateCutPoints(*pred, component_id);

                per_component_candidate_cut_points[*pred] = cut_points;
            }

            MultiComponentAnalysis  __per_component_component_intersections = allIntersections(per_component_candidate_cut_points);

            if(!__per_component_component_intersections.graph.size()) return MultiComponentTree();

            if(!per_component_component_intersections.graph.size()){
                per_component_component_intersections = __per_component_component_intersections;
                
            } else if(__per_component_component_intersections != per_component_component_intersections) {
                cout<<"Non-Canonical predecessors.\n";
                return MultiComponentTree();//vector<set<int> >();
            }
        }
        
        MultiComponent key_with_one;// = -1;
        for(MultiComponentGraph::iterator p =  per_component_component_intersections.graph.begin()
                ; p !=  per_component_component_intersections.graph.end()
                ; p++){
            if(p->second.size() == 1){
                key_with_one = p->first;
            }
        }
        if(!key_with_one.size()){
            cout<<"No start to the tree.\n";
            return MultiComponentTree();//vector<set<int> >();
        }
        
        SetOfMultiComponents visited;
        MultiComponentTree tree(key_with_one);
        bool useful_answer = traverseCutGraph(key_with_one, 
                                              visited, 
                                              tree, 
                                              per_component_component_intersections.graph);


        if(!useful_answer){
            cout<<"Non-useful answer -- traverseCutGraph.\n";
            return MultiComponentTree();
        } else {
            return tree;
        }
    }

    
    bool SCChashsat::traverseCutGraph(MultiComponent& key, 
                                      SetOfMultiComponents& visited, 
                                      MultiComponentTree& answer, 
                                      MultiComponentGraph& graph){
        
        assert(graph.find(key) != graph.end());
        SetOfMultiComponents successors = graph[key];
        
        assert(successors.size());
 

        bool exactly_one_erasure = false;
        if(visited.size()){
            for(SetOfMultiComponents::iterator visit = visited.begin()
                    ; visit != visited.end()
                    ; visit++){
                
                if(successors.find(*visit) != successors.end()){
                    /*No self loops...*/
                    assert(*visit != key);
                    
                    if(!exactly_one_erasure){
                        exactly_one_erasure = true;
                    } else {
                        exactly_one_erasure = false;
                        break;
                    }
                    successors.erase(*visit);
                }
            }
        } else {
            exactly_one_erasure = true;
        }

        visited.insert(key); 
        
        if(!exactly_one_erasure) {
            cout<<"Erasing too many elements.\n";
            return false;
        }
      

        if(!successors.size())return true;
        
        for(SetOfMultiComponents::iterator successor = successors.begin()
                ; successor != successors.end()
                ; successor++){
            MultiComponentTree mct(*successor);
            answer.push_child(mct);
            visited.insert(*successor); 
        }
        
        SetOfMultiComponents::iterator successor = successors.begin();
        for(int i =0; i < answer.children.size(); i++, successor++){
            assert(successor != successors.end());
            MultiComponent _successor = *successor;
            MultiComponentTree& _child = answer.children[i];
            
            bool useful = traverseCutGraph(_successor, visited, _child, graph);
            if(!useful)return false;
        }

        return true;
    }

    int  SCChashsat::factoredClusteredMaximumPlanLength__allMutex(set<int>& components,  int component_id, bool parallel){
        assert(parallel);
        SCChashsat::MultiComponentTree tree = getPredecessorOrders(components);

        if(tree.node_contents.size() == 0){
            cout<<"Got an empty tree.\n";
            return -1;
        }

        return descend_tree(tree, component_id);
    }

    int SCChashsat::descend_tree(MultiComponentTree& mct, int component_id){
        
        int factor = worstCaseModelCount(component_id, 
                                         mct.node_contents,
                                         component_contents[component_id].size(),
                                         component_mutex_count[component_id]);            

        int max_contribution_in_phase = 0;
        for(set<int>::iterator comp = mct.node_contents.begin()
                ; comp != mct.node_contents.end()
                ; comp++) { 
            
            int predecessor_count = factoredMaximumPlanLength(*comp, true);
            if(max_contribution_in_phase < predecessor_count){
                max_contribution_in_phase = predecessor_count;
            }
        }

        int predecessor_contribution_interfering_score = 0;
        if(ancestors_dont_mention_goals(component_id, mct.node_contents)){
            predecessor_contribution_interfering_score += 2 * max_contribution_in_phase * factor + 1;
        } else {
            predecessor_contribution_interfering_score += 2 * max_contribution_in_phase * factor + max_contribution_in_phase + 1;
        }

        cout<<"Descending tree :: got a contribution :: "<<predecessor_contribution_interfering_score<<std::endl;
        
        int max_child = 0;
        for(int i=0; i < mct.children.size(); i++){
            int contribution = descend_tree(mct.children[i], component_id);
            if(contribution > max_child){
                max_child = contribution;
            }
        }
        predecessor_contribution_interfering_score += max_child;// can be a zero contribution at leaf nodes.

        return predecessor_contribution_interfering_score;
        
    }

    int SCChashsat::factoredClusteredMaximumPlanLength(set<int>& components, bool parallel)
    {
        /* Suppose each component only mentions a single SAS+ function
         * symbol. Moreover, suppose the predecessors (all equal and
         * non-interfering) of the SCCs "cut" each SCC at different
         * elements. One cuts at vertex A&B, another at B&C, then
         * another at C&D.  Any any longest abstract plan, B has to
         * happen before C or vise versa. C has to happen before D or
         * vise versa. So we have a total order. Any any longest
         * abstract plan, either ABCD, or DCBA.  Here, we only have to
         * move "left", or otherwise only have to move right in the
         * order. All left movement can be done in parallel, and so
         * can all right movement. Also, there is no interference in
         * the D->C and A->B direction. Those can be done in
         * parallel. So this gives us a massive steps savings. */
        
        /* See if all \argument{components} a linear, in the sense
         * they have N possible assignments for N variables.  */
        cerr<<"Got a cluster with "<<components.size()<<" components.\n";

        int signature_component_id = *components.begin();
        if(clustered_component_cost.find(signature_component_id) != clustered_component_cost.end()) {
            assert(clustered_component_cost[signature_component_id] > 0);
            return clustered_component_cost[signature_component_id];
        }
        
        for(std::set<int >::iterator comp = components.begin()
                ; comp != components.end()
                ; comp++){
            assert(non_interfering_predecessors(*comp));
            traversed.insert(*comp);
        }

        
        bool all_mutex = true;
        for(set<int>::iterator component =  components.begin()
                ; component !=  components.end()
                ; component ++){
            int n = component_contents[*component].size();
            int max_mutex = floor(((n - 1)*(n-1) + n ) / 2);    
            int mutex_count = component_mutex_count[*component];
            if(mutex_count != max_mutex){
                all_mutex = false;
                break;
            }
        }



        int predecessor_contribution = 0;
        int predecessor_contribution_interfering_score = 0;
        if(predecessor_components.find(signature_component_id) != predecessor_components.end()){ 
            cout<<"Got mutex.\n";
            if(all_mutex){
                cout<<"Got mutex component = YES.\n";
                int answer =  factoredClusteredMaximumPlanLength__allMutex(components, parallel);
                
                if(answer > 0)return answer;
            }

            //exit(-1);
        }
        
        
        if(predecessor_components.find(signature_component_id) != predecessor_components.end()){ 
            set<int>& predecessors_for_component = predecessor_components[signature_component_id];
            
            if(predecessors_for_component.size() != 0){
                for(set<int>::iterator comp = predecessors_for_component.begin()
                        ; comp != predecessors_for_component.end()
                        ; comp++){
                    int predecessor_count = factoredMaximumPlanLength(*comp, parallel);
                    

                    int max_factor = 0;
                    for(set<int>::iterator test_component = components.begin()
                            ; test_component != components.end()
                            ; test_component++){
                        assert(predecessor_components[*test_component].find(*comp) 
                               != predecessor_components[*test_component].end());
                        
                        int factor = worstCaseModelCount(*test_component, 
                                                         *comp, /*Predecessor*/
                                                         component_contents[*test_component].size(), 
                                                         component_mutex_count[*test_component]);

                        if(factor > max_factor){
                            max_factor = factor;
                        }

                                                       
                    }
                    
                    if(parallel && ancestors_dont_mention_goals(signature_component_id, predecessors_for_component)){
                        predecessor_contribution_interfering_score += 2 * predecessor_count * max_factor + 1;
                    } else {
                        predecessor_contribution_interfering_score += 2 * predecessor_count * max_factor + predecessor_count + 1;
                    }
                }

                cerr<<"Got a score of "<<predecessor_contribution_interfering_score<<endl
                    <<"To be multiplied by : "<<components.size()<<endl;

                predecessor_contribution_interfering_score *= components.size();
            }
        }
        predecessor_contribution = predecessor_contribution_interfering_score;


        if(predecessor_contribution == 0){
            clustered_component_cost[signature_component_id] = 0;
            
            for(set<int>::iterator component = components.begin()
                    ; component != components.end()
                    ; component++){
                
                    int contrib = SCCDiscovery::worstCaseModelCount(*component, 
                                                                    component_contents[*component].size(), 
                                                                    component_mutex_count[*component]);

                    if(contrib > clustered_component_cost[signature_component_id]){
                        clustered_component_cost[signature_component_id] = contrib;
                    }
            }
            
            cerr<<"Got layer 0 component "<<signature_component_id<<" cost is :: "<<clustered_component_cost[signature_component_id]<<endl;

            assert(clustered_component_cost[signature_component_id] > 0);

            if(clustered_component_cost[signature_component_id] == 0) {cerr<<"Got a zero cost component!\n";exit(-1);}

            return clustered_component_cost[signature_component_id];
        }

        int answer = predecessor_contribution;

        clustered_component_cost[signature_component_id] = predecessor_contribution;
        
        assert(answer > 0);

        return answer;        
    }

    int SCChashsat::simpleFactoredMaximumPlanLength(set<int>& layer, bool parallel)
    {
        
        assert(cached__ancestors_dont_mention_goals.size() > 0);
        int contributions =0;

        
        set<int> missed;
        set<int> not_useful;
        
        /* For each component in the final layer. */
        for(set<int>::iterator comp =  layer.begin()
                ; comp != layer.end()
                ; comp++){


            assert(component_contents.find(*comp) != component_contents.end());
            bool useful = false;
            /* For each problem goal.*/
            for(std::set< Proposition<int> >::iterator goal =  goals.begin()
                    ; goal != goals.end()
                    ; goal++){
                assert(goal->id >= 0);
                if(component_contents[*comp].find(goal->id) != component_contents[*comp].end()){
                    useful = true;
                    break;
                }
            }

            /* This component is not immediately useful. */
            if(!useful) {
                not_useful.insert(*comp);
                if(predecessor_components.find(*comp) == predecessor_components.end()){
                    cerr<<"Skipping :: "<<*comp<<" which is not needed for the goal.\n";
                    continue;
                }
                
                /* We probably have to descend on the predecessors of the non-useful component. */
                assert(predecessor_components.find(*comp) != predecessor_components.end());
                for(set<int>::iterator pred = predecessor_components[*comp].begin()
                        ; pred != predecessor_components[*comp].end()
                        ; pred++){
                    missed.insert(*pred);
                }
                
                continue;
            }

            contributions += factoredMaximumPlanLength(*comp, parallel); 
        }


        /* Just in case we have already traversed the ancestor of a missed component. */
        for(set<int>::iterator component = traversed.begin()
                ; component != traversed.end()
                ; component++){
            missed.erase(*component);
        }

        if(missed.size()){
            int missed_element_contribution = simpleFactoredMaximumPlanLength(missed, parallel);

            assert(missed_element_contribution > 0);

            contributions += missed_element_contribution;
        }
        
        return contributions;
        
    }
    
    int SCChashsat::factoredMaximumPlanLength(set<int>& layer, bool parallel)
    {
        assert(cached__ancestors_dont_mention_goals.size() > 0);
        int contributions =0;

        map<set<int>, set<int> > clusters_of_components;

        set<int> missed;
        set<int> not_useful;
        for(set<int>::iterator comp =  layer.begin()
                ; comp != layer.end()
                ; comp++){


            assert(component_contents.find(*comp) != component_contents.end());
            bool useful = false;
            for(std::set< Proposition<int> >::iterator goal =  goals.begin()
                    ; goal != goals.end()
                    ; goal++){
                assert(goal->id >= 0);
                if(component_contents[*comp].find(goal->id) != component_contents[*comp].end()){
                    useful = true;
                    break;
                }
            }
            if(!useful) {
                not_useful.insert(*comp);
                if(predecessor_components.find(*comp) == predecessor_components.end()){
                    cerr<<"Skipping :: "<<*comp<<" which is not needed for the goal.\n";
                    continue;
                }

                assert(predecessor_components.find(*comp) != predecessor_components.end());
                for(set<int>::iterator pred = predecessor_components[*comp].begin()
                        ; pred != predecessor_components[*comp].end()
                        ; pred++){
                    missed.insert(*pred);
                }
                
                continue;
            }


            report_to_cluster_of_components(*comp, clusters_of_components);

        }


        set<set<int> > clusters_already_dealt_with;
        
        for(set<int>::iterator comp =  layer.begin()
                ; comp != layer.end()
                ; comp++){

            if(not_useful.find(*comp) != not_useful.end())continue;
            

#define CLASSICAL_COUNTING  else {              \
                                                \
                if(parallel){                                           \
                    cerr<<"Usefully descending on :: "<<__PRETTY_FUNCTION__<<" "<<*comp<<endl; \
                    contributions += factoredMaximumPlanLength(*comp, parallel); \
                } else {                                                \
                    cerr<<"MISTAKEN --  descending on :: "<<__PRETTY_FUNCTION__<<" "<<*comp<<endl; \
                    contributions += factoredMaximumPlanLength(*comp, parallel); \
                }                                                       \
                assert(contributions>0);                                \
            }                                                           \
            
            if(predecessor_components.find(*comp) != predecessor_components.end()){         
                set<int>& predecessors = predecessor_components[*comp];
                if(predecessors.size()){
                    assert(clusters_of_components.find(predecessors) != clusters_of_components.end());
                    
                    if(parallel && clusters_of_components[predecessors].size() > 1 && non_interfering_predecessors(*comp)) 
                    {
                        if(clusters_already_dealt_with.find(predecessors) == clusters_already_dealt_with.end()){
                            clusters_already_dealt_with.insert(predecessors);
                            contributions += factoredClusteredMaximumPlanLength(clusters_of_components[predecessors], parallel);
                        }
                    }CLASSICAL_COUNTING
                } CLASSICAL_COUNTING   
            } CLASSICAL_COUNTING
        }
        

        for(set<int>::iterator component = traversed.begin()
                ; component != traversed.end()
                ; component++){
            missed.erase(*component);
        }

        if(missed.size()){
            int missed_element_contribution = factoredMaximumPlanLength(missed, parallel);

            assert(missed_element_contribution > 0);

            contributions += missed_element_contribution;
        }
        
        return contributions;
    }
    
    int SCChashsat::factoredMaximumPlanLength(bool parallel)
    {
        assert(component_contents.size() > 0 );

        /* Erase cached component costs.*/
        component_cost = map<int, int>();
        clustered_component_cost = map<int, int>() ;
        traversed = set<int>();
        cached__ancestors_dont_mention_goals = vector<int>(component_contents.size());
        for(int i =0 ;  i  < component_contents.size(); i++){cached__ancestors_dont_mention_goals[i] =-1;}

        cerr<<__PRETTY_FUNCTION__<<" "<<cached__ancestors_dont_mention_goals.size()<<" "<<component_contents.size()<<endl;

        assert(cached__ancestors_dont_mention_goals.size() > 0);

        // set<int>& last_layer = component_layers.back();
        set<int>& last_layer = regression_counting_from;

        assert(last_layer.size());
        int answer1 =  factoredMaximumPlanLength(last_layer, parallel);
        component_cost = map<int, int>();
        clustered_component_cost = map<int, int>() ;
        traversed = set<int>();
        cached__ancestors_dont_mention_goals = vector<int>(component_contents.size());
        for(int i =0 ;  i  < component_contents.size(); i++){cached__ancestors_dont_mention_goals[i] =-1;}
        int answer2 =  simpleFactoredMaximumPlanLength(last_layer, parallel);
        return std::min(answer1, answer2);
        return answer2;

        // cout<<"Answer 1 is :: "<<answer1<<" and Answer 2 is :: "<<answer2<<endl;

        // return std::min(answer1, answer2);
    }
    
    
    int SCChashsat::factored_plan_length_with_non_interfering_predecessors
    (int component_id
     , set<int>& predecessors_for_component, bool parallel){

        assert(cached__ancestors_dont_mention_goals.size() > 0);

        int predecessor_contribution_non_interfering_score = 0;
        int maximum = 0;
        int factor = worstCaseModelCount(component_id,
                                         -1/*Ignoring predecessor dependence*/, 
                                         component_contents[component_id].size(), 
                                         component_mutex_count[component_id]);
        
        for(set<int>::iterator comp = predecessors_for_component.begin()
                ; comp != predecessors_for_component.end()
                ; comp++){
            int predecessor_count = factoredMaximumPlanLength(*comp, parallel);
            if(predecessor_count > maximum){
                maximum = predecessor_count;
            }
        }
        assert(maximum > 0);

        if(parallel && ancestors_dont_mention_goals(component_id, predecessors_for_component)){
            predecessor_contribution_non_interfering_score =   maximum * factor  + factor;
            cerr<<"Ancestors did not mention the goal.\n";
        } else {
            cerr<<"Ancestors mentioned the goal.\n";
            predecessor_contribution_non_interfering_score = maximum * factor + maximum + factor;
        }

        cerr<<"One contribution is :: "<<__PRETTY_FUNCTION__<<" "<<component_id<<" == "<<predecessor_contribution_non_interfering_score<<endl;
        assert(predecessor_contribution_non_interfering_score > 0);
        if(predecessor_contribution_non_interfering_score <= 0) {
            cerr<<"Error, negative state count. Too many states probably. "<<maximum<<" "<<factor<<"\n";
            exit(-1);
        }

        return predecessor_contribution_non_interfering_score;
    }
    
    
    int SCChashsat::factored_plan_length_with_interfering_predecessors
    (int component_id
     , set<int>& predecessors_for_component, bool parallel){


        assert(cached__ancestors_dont_mention_goals.size() > 0);

        int predecessor_contribution_interfering_score = 0;



        int maximum_length_of_prefix = 0;
        for(set<int>::iterator comp = predecessors_for_component.begin()
                ; comp != predecessors_for_component.end()
                ; comp++){
                
            int predecessor_count = factoredMaximumPlanLength(*comp, parallel);

            if(maximum_length_of_prefix < predecessor_count){
                maximum_length_of_prefix = predecessor_count;
            }

            int factor = worstCaseModelCount(component_id, 
                                             *comp, /*Predecessor*/
                                             component_contents[component_id].size(), 
                                             component_mutex_count[component_id]);
                
            assert(predecessor_count > 0);
            assert(factor > 0);
            //cerr<<"Predecessor count :: "<<predecessor_count<<" with current factor ::  "<<factor<<std::endl
            //   <<predecessor_count * factor<<endl;
            if(predecessor_count == 0) exit(-1);
            if(factor == 0) exit(-1);
                
            if(predecessor_count < 0) exit(-1);
            if(factor <  0) exit(-1);
                
            assert(predecessor_count * factor > 0);

            if(ancestors_dont_mention_goals(component_id, predecessors_for_component)){
                if(parallel && non_interfering_predecessors(component_id)){
                    predecessor_contribution_interfering_score += predecessor_count * (factor - 1)  + factor;
                } else {
                    predecessor_contribution_interfering_score += predecessor_count * factor  + factor;
                }
                cerr<<"Ancestors did not mention the goal "<<predecessor_contribution_interfering_score<<" .\n";
            } else {
                cerr<<"Ancestors mentioned the goal.\n";
                if(parallel && non_interfering_predecessors(component_id)){
                    predecessor_contribution_interfering_score += predecessor_count * (factor - 1) + predecessor_count + factor;
                } else {
                    predecessor_contribution_interfering_score += predecessor_count * factor + predecessor_count + factor;
                }
            }
            

            if(predecessor_contribution_interfering_score <= 0) {
                cerr<<"Error, negative state count. Too many states probably.\n";
                exit(-1);//predecessor_contribution_interfering_score = 0;
            }
            //cerr<<"Cumulative models in layer :: 0 < "<<predecessor_contribution<<std::endl;
        }
            
        if(parallel && non_interfering_predecessors(component_id)){
            predecessor_contribution_interfering_score += maximum_length_of_prefix;
        }

        if(predecessor_contribution_interfering_score <= 0) {
            cerr<<"(1) Error, negative state count. Too many states probably.\n";
            exit(-1);
        } else {
            cerr<<"(1) positive non-zero value of "<<predecessor_contribution_interfering_score<<".\n";
        }
          

        return  predecessor_contribution_interfering_score;
    }

    

    int SCChashsat::factoredMaximumPlanLength(int component_id, bool parallel)
    {

        assert(cached__ancestors_dont_mention_goals.size() > 0);

        cerr<<"Descending on :: "<<__PRETTY_FUNCTION__<<" "<<component_id<<endl;

        // if(parallel != true) {
        //     cerr<<" I no longer support serial plan length estimation.\n";
        //     exit(-1);
        // }

        /* If we have already done the counting use the cache. */
        if(component_cost.find(component_id) != component_cost.end()) {
            assert(component_cost[component_id] > 0);
            return component_cost[component_id];
        }
        
        traversed.insert(component_id);
        int predecessor_contribution = 0;

        int predecessor_contribution_interfering_score = 0;
        int predecessor_contribution_non_interfering_score = 0;

        /* If we do not have a leaf component.*/
        if(predecessor_components.find(component_id) != predecessor_components.end()){
            
            cerr<<"We have predecessors :: "<<__PRETTY_FUNCTION__<<" "<<component_id<<endl;

            set<int>& predecessor_component = predecessor_components[component_id];
            

            if(predecessor_component.size() != 0){
                if(parallel && non_interfering_predecessors(component_id)){
                    predecessor_contribution_non_interfering_score
                        = factored_plan_length_with_non_interfering_predecessors(component_id, predecessor_component);
                } 
                
                
                predecessor_contribution_interfering_score 
                    = factored_plan_length_with_interfering_predecessors(component_id, predecessor_component, parallel);
                
                
                predecessor_contribution = (predecessor_contribution_non_interfering_score > 0)?
                    std::min(predecessor_contribution_interfering_score, predecessor_contribution_non_interfering_score):
                    predecessor_contribution_interfering_score;
                
                cerr<<"Chosen contribution is :: "<<__PRETTY_FUNCTION__<<" "<<component_id<<" == "<<predecessor_contribution<<endl
                    <<"Choice 1 was :: "<<predecessor_contribution_non_interfering_score<<endl
                    <<"Choice 2 was :: "<<predecessor_contribution_interfering_score<<endl;
                
                if(predecessor_contribution <= 0) {
                    cerr<<"Error, negative state count. Too many states probably.\n";
                    exit(-1);
                }
                
                assert(predecessor_contribution>0);
            }
        }

        if(predecessor_contribution == 0){
            //component_cost[i] = worstCaseModelCount(i, -1, component_contents[i].size(), component_mutex_count[i]);
            component_cost[component_id] = SCCDiscovery::worstCaseModelCount(component_id, 
                                                                            component_contents[component_id].size(), 
                                                                            component_mutex_count[component_id]);
            
            cerr<<"Got layer 0 component "<<component_id<<" cost is :: "<<component_cost[component_id]<<endl;

            assert(component_cost[component_id] > 0);

            if(component_cost[component_id] == 0) {cerr<<"Got a zero cost component!\n";exit(-1);}

            return component_cost[component_id];
        }

        assert(predecessor_contribution > 0);

        int answer = predecessor_contribution;
        

        component_cost[component_id] = predecessor_contribution;
        
        assert(answer > 0);

        return answer;
    }


    
    
    int  SCChashsat::SAT_AddVariable(){

        return variable_id++;
    }
    
    
    int  SCChashsat::
    setup_ayplan_vars(int, 
                      std::set<int> freeVariables, 
                      std::set<std::pair<int, int> >& mutex, 
                      std::set<std::pair<int, int> >& equiv)
    {
        for(std::set<int >::iterator p = freeVariables.begin()
                ; p != freeVariables.end()
                ; p++){
            assert(*p>=0);

            if(ayplan_vars.find(*p) == ayplan_vars.end()){
                ayplan_vars[*p] =  SAT_AddVariable();//counter++;
                assert(ayplan_vars[*p] > 0);
                //1//cerr<<*p<<" is "<< ayplan_vars[*p] <<std::endl;
            }

        }
        for(std::set<std::pair<int, int> >::iterator p = mutex.begin()
                ; p != mutex.end()
                ; p++){
            assert(p->first >= 0);
            assert(p->second >= 0);
            if(ayplan_vars.find(p->first) == ayplan_vars.end()){
                ayplan_vars[p->first] =  SAT_AddVariable();//counter++;
                //1//cerr<<p->first<<" is "<< ayplan_vars[p->first] <<std::endl;
            }

            if(ayplan_vars.find(p->second) == ayplan_vars.end()){
                ayplan_vars[p->second] = SAT_AddVariable();
                //1//cerr<<p->second<<" is "<< ayplan_vars[p->second] <<std::endl;
            }
            assert(ayplan_vars[p->first] > 0);
            assert(ayplan_vars[p->second] > 0);
        }
        for(std::set<std::pair<int, int> >::iterator p = equiv.begin()
                ; p != equiv.end()
                ; p++){
            assert(p->first >= 0);
            assert(p->second >= 0);

            if(ayplan_vars.find(p->first) == ayplan_vars.end()){
                ayplan_vars[p->first] =  SAT_AddVariable();//counter++;
                //1//cerr<<p->first<<" is "<< ayplan_vars[p->first] <<std::endl;
            }

            if(ayplan_vars.find(p->second) == ayplan_vars.end()){
                ayplan_vars[p->second] = SAT_AddVariable();
                //1//cerr<<p->second<<" is "<< ayplan_vars[p->second] <<std::endl;
            }

            assert(ayplan_vars[p->first] > 0);
            assert(ayplan_vars[p->second] > 0);
            
        }

        return ayplan_vars.size();
    }
    
    
    void SCChashsat::actions_noop_processing(){
        
        
        for(int fluent = 0; fluent < noops_to_action.size()/*A count of fluents*/; fluent++){
            /*Only continue if in component.*/
            if(ayplan_vars.find(fluent) == ayplan_vars.end())continue;
            Clause clause;
            assert(sat_noop.find(fluent) != sat_noop.end());
            assert(neg_sat_noop.find(fluent) != neg_sat_noop.end());
            clause.insert(- sat_noop[fluent]);
            assert(sat_noop[fluent] != neg_sat_noop[fluent]);
            clause.insert(- neg_sat_noop[fluent]);
            noop_clauses.insert(clause);// A noop chooses for true or false, but not both.

            clause = Clause();
            clause.insert(- sat_noop[fluent]);
            clause.insert(ayplan_vars[fluent]);
            noop_clauses.insert(clause);//sat noop implies var is true.
            clause = Clause();
            clause.insert(- neg_sat_noop[fluent]);
            clause.insert(- ayplan_vars[fluent]);
            noop_clauses.insert(clause);//neg_sat noop implies var is true.
        }

        for(std::map<int, int>::iterator action = ayplan_action_vars.begin()
                ; action != ayplan_action_vars.end()
                ; action++){
            
            assert(action_to_noops.size() > action->first);

            set<int>& noop_fluents = action_to_noops[action->first];
            assert(noop_fluents.size());/*FIX :: Doesn't have to be true.*/
            for(int fluent = 0; fluent < noops_to_action.size()/*A count of fluents*/; fluent++){
                /*Only continue if in component.*/
                if(ayplan_vars.find(fluent) == ayplan_vars.end())continue;
                
                Clause clause;
                if(noop_fluents.find(fluent) != noop_fluents.end()){
                    assert(sat_noop.find(fluent)!= sat_noop.end());
                    assert(neg_sat_noop.find(fluent)!= neg_sat_noop.end());
                    assert(neg_sat_noop[fluent] != sat_noop[fluent]);
                    assert(- action->second != neg_sat_noop[fluent]);
                    assert(- action->second != sat_noop[fluent]);
                    clause.insert(- action->second);//(- *action, sat_noop[fluent]);/* noop implied by action */
                    clause.insert(sat_noop[fluent]);
                    clause.insert(neg_sat_noop[fluent]);
                    noop_clauses.insert(clause);/*If action does not disturb the truth value.*/
                } else {
                    assert(sat_noop.find(fluent)!= sat_noop.end());
                    assert(neg_sat_noop.find(fluent)!= neg_sat_noop.end());
                    clause.insert(- action->second);
                    clause.insert(- sat_noop[fluent]);
                    noop_clauses.insert(clause);/*If action does disturb the truth value (positive).*/
                    clause = Clause();
                    clause.insert(- action->second);
                    clause.insert(- neg_sat_noop[fluent]);
                    noop_clauses.insert(clause);/*If action does disturb the truth value (negative).*/
                }
            }
        }
    }

    
    int SCChashsat::exactly_one_action(){
        //1//cerr<<"ACTION MUTEX: ";
        for(std::map<int, int>::iterator action = ayplan_action_vars.begin()
                ; action != ayplan_action_vars.end()
                ; action++){
            std::map<int, int>::iterator _action = action;
            for( _action++
                     ; _action != ayplan_action_vars.end()
                     ; _action++){
                
                //1//cerr<<", "<<- action->second<<" "<<- _action->second;

                /*Two actions cannot execute for counting purposes.*/
                Clause clause;//(- action->second, - _action->second);
                clause.insert(- action->second);
                clause.insert(- _action->second);
                only_one_action.insert(clause);
            }
            //1//cerr<<std::endl;
        }

        Clause clause;
        /*And there must be one action.*/        
        for(std::map<int, int>::iterator action = ayplan_action_vars.begin()
                ; action != ayplan_action_vars.end()
                ; action++){
            clause.insert(action->second);
        }
        only_one_action.insert(clause);

        return only_one_action.size(); 
    }
     
    
    int SCChashsat::complete_edge_to_implied_actions()
    {
        for(map<int, Clause>::iterator p = edge_to_implied_actions.begin()
                ; p != edge_to_implied_actions.end()
                ; p++){
            Clause clause= p->second;
            clause.insert(- p->first);
            edge_to_implied_actions__clauses.insert(clause);
        }


        return edge_to_implied_actions__clauses.size();
    }
     
    
    int  SCChashsat::complete_variable_to_implied_edges()
    {
        for(map<int, Clause>::iterator p = variable_to_implied_edges.begin()
                ; p != variable_to_implied_edges.end()
                ; p++){

            Clause clause = p->second;
            assert(clause.size());
            clause.insert(- p->first);
            
            assert(sat_fluent_to_noop.find(p->first) != sat_fluent_to_noop.end());
            clause.insert(sat_fluent_to_noop[p->first]);/* OR NOOP excuse*/
            variable_to_implied_edges__clauses.insert(clause);
        }

        return variable_to_implied_edges__clauses.size();
    }

    /* If edge, then vars (2SAT) */
    
    int  SCChashsat::sat_edge_implies_variable(int edge_id, int variable, int sign){
        assert(ayplan_vars.find(std::abs(variable)) != ayplan_vars.end());
        assert(ayplan_edge_vars.find(edge_id) != ayplan_edge_vars.end());

        Clause clause;
        //int sign = ((variable >= 0)?1:-1);
        clause.insert(- ayplan_edge_vars[edge_id]);
        clause.insert(sign * ayplan_vars[std::abs(variable)]);
        
        edge_implies_variable__clauses.insert(clause);

        return edge_implies_variable__clauses.size();
    }
    
    /*If var, then one of edge (NSAT).*/
    
    int  SCChashsat::sat_variable_implies_edge(int variable, int edge_id, int sign){
        assert(ayplan_vars.find(std::abs(variable)) != ayplan_vars.end());
        assert(ayplan_edge_vars.find(edge_id) != ayplan_edge_vars.end());
        assert(ayplan_vars[std::abs(variable)] > 0);

        
        //int sign = ((variable >= 0)?1:-1);


        if(variable_to_implied_edges.find(sign * ayplan_vars[std::abs(variable)]) == variable_to_implied_edges.end()){
            variable_to_implied_edges[sign *  ayplan_vars[std::abs(variable)]] =  Clause(); 
        }
        variable_to_implied_edges[sign *  ayplan_vars[std::abs(variable)]].insert(ayplan_edge_vars[edge_id]);
        

        
        return variable_to_implied_edges.size();
    }
                   
    /* If action, then edge (2SAT) */
    
    int  SCChashsat::sat_action_implies_edge(int action_id, int edge_id){
        Clause clause;
        clause.insert(- ayplan_action_vars[action_id]);
        clause.insert(ayplan_edge_vars[edge_id]);
        
        action_implies_edge__clauses.insert(clause);

        return action_implies_edge__clauses.size();
    }     
    // /* If action, then edge (2SAT) */
    // 
    // int  SCChashsat::sat_action_implies_edge(int action_id, int edge_id){
    //     Clause clause;
    //     clause.insert(- ayplan_action_vars[action_id]);
    //     clause.insert(ayplan_edge_vars[edge_id]);
        
    //     action_implies_edge__clauses.insert(clause);

    //     return action_implies_edge__clauses.size();
    // }
    
    /*If edge, then one of action (NSAT).*/
    
    int  SCChashsat::sat_edge_implies_action(int edge_id, int action_id){
        if(edge_to_implied_actions.find(ayplan_edge_vars[edge_id]) == edge_to_implied_actions.end()){
            edge_to_implied_actions[ayplan_edge_vars[edge_id]] = Clause();   
        }
        edge_to_implied_actions[ayplan_edge_vars[edge_id]].insert(ayplan_action_vars[action_id]);

        return edge_to_implied_actions.size();
    }
    

        
    
    int SCChashsat::setup_ayplan_action_vars(int ){
                

        /* For each component variable.*/
        for(std::map<int, int>::const_iterator variable = ayplan_vars.begin()
                ; variable != ayplan_vars.end()
                ; variable++) {
            if(truthifying_edges__reversed.find(variable->first) != truthifying_edges__reversed.end()){
                
                set<int>& seconds = truthifying_edges__reversed[variable->first];
      
                for(set<int>::iterator second =  seconds.begin()
                        ; second != seconds.end()
                        ; second++){
                    SAT_edge edge(*second, variable->first);
                        
                    assert(sat_edge_id.find(edge) != sat_edge_id.end());
                    int edge_id = sat_edge_id[edge];
                    assert(edge_to_actions.find(edge_id) != edge_to_actions.end());
                   
                    set<int>& edge_actions = edge_to_actions[edge_id];
                        
                    assert(edge_actions.size());

                    for(set<int>::iterator _action_id = edge_actions.begin()
                            ; _action_id != edge_actions.end()
                            ; _action_id++){
                        int action_id = *_action_id;
                        if(ayplan_action_vars.find(action_id) == ayplan_action_vars.end()){
                            ayplan_action_vars[action_id]  = SAT_AddVariable();;
                        }
                    }

                    if(ayplan_edge_vars.find(edge_id) == ayplan_edge_vars.end()){
                        ayplan_edge_vars[edge_id] = SAT_AddVariable();;
                    }
                     
                    
                    /* If edge, then vars (2SAT) */
                    sat_edge_implies_variable(edge_id, variable->first);
                    /*If var, then one of edge (NSAT).*/
                    sat_variable_implies_edge(variable->first, edge_id);
                        
                    for(set<int>::iterator _action_id = edge_actions.begin()
                            ; _action_id != edge_actions.end()
                            ; _action_id++){
                        int action_id = *_action_id;
                        /* If action, then edge (2SAT) */
                        sat_action_implies_edge(action_id, edge_id);
                        /*If edge, then one of action (NSAT).*/
                        sat_edge_implies_action(edge_id, action_id);
                    }
                }
            }

            
            if(falsifying_edges__reversed.find(variable->first) != falsifying_edges__reversed.end()){
                set<int>& seconds = falsifying_edges__reversed[variable->first];

                

                assert(seconds.size());
                for(set<int>::iterator second =  seconds.begin()
                        ; second != seconds.end()
                        ; second++){
                    SAT_edge edge(*second, variable->first);
                        
                    assert(sat_neg_edge_id.find(edge) != sat_neg_edge_id.end());
                    int edge_id = sat_neg_edge_id[edge];
                    assert(edge_to_actions.find(edge_id) != edge_to_actions.end());

                    set<int>& edge_actions = edge_to_actions[edge_id];
                    assert(edge_actions.size());
                        
                    for(set<int>::iterator _action_id = edge_actions.begin()
                            ; _action_id != edge_actions.end()
                            ; _action_id++){
                        int action_id = *_action_id;
                        if(ayplan_action_vars.find(action_id) == ayplan_action_vars.end()){
                            ayplan_action_vars[action_id]  = SAT_AddVariable();;
                        }
                    }

                    if(ayplan_edge_vars.find(edge_id) == ayplan_edge_vars.end()){
                        ayplan_edge_vars[edge_id] = SAT_AddVariable();
                    }
                        
                    

                    /* If edge, then vars (2SAT) */
                    sat_edge_implies_variable(edge_id, variable->first, -1);
            
                    
                    /*If var, then one of edge (NSAT).*/
                    sat_variable_implies_edge(variable->first, edge_id, -1);
            
                        
                    for(set<int>::iterator _action_id = edge_actions.begin()
                            ; _action_id != edge_actions.end()
                            ; _action_id++){
                        int action_id = *_action_id;
                        /* If action, then edge (2SAT) */
                        sat_action_implies_edge(action_id, edge_id);
            
                        /*If edge, then one of action (NSAT).*/
                        sat_edge_implies_action(edge_id, action_id);
            
                    }
                }
            }
        }

        int fluent_id = 0;
        for(int action_id = action_to_noops.size(); action_id < action_to_noops.size() + noops_to_action.size() ; action_id++, fluent_id++){
            assert(ayplan_noop_vars.find(action_id) == ayplan_noop_vars.end());

            /* fluent is not mentioned in component begin analysed.*/
            if(ayplan_vars.find(action_id - action_to_noops.size()) == ayplan_vars.end())continue;
            
            ayplan_noop_vars[action_id]  = SAT_AddVariable();
            assert(fluent_id == action_id - action_to_noops.size());
            assert(ayplan_vars.find(fluent_id) != ayplan_vars.end());
            assert(sat_noop.find(fluent_id) == sat_noop.end());
            assert(neg_sat_noop.find(fluent_id) == neg_sat_noop.end());
            assert(sat_fluent_to_noop.find(ayplan_vars[fluent_id]) == sat_fluent_to_noop.end());
            assert(sat_fluent_to_noop.find(-ayplan_vars[fluent_id]) == sat_fluent_to_noop.end());
            
            sat_noop[fluent_id] = ayplan_noop_vars[action_id];
            assert(neg_sat_noop.find(fluent_id) == neg_sat_noop.end());
            neg_sat_noop[fluent_id] = SAT_AddVariable();//ayplan_noop_vars[action_id];
            assert(sat_noop[fluent_id] != neg_sat_noop[fluent_id]);

            sat_fluent_to_noop[ayplan_vars[fluent_id]] = ayplan_noop_vars[action_id];
            sat_fluent_to_noop[-ayplan_vars[fluent_id]] = neg_sat_noop[fluent_id];
        }

        complete_edge_to_implied_actions();
        complete_variable_to_implied_edges();
            
        

        return edge_implies_variable__clauses.size() + 
            action_implies_edge__clauses.size() + 
            edge_to_implied_actions__clauses.size() + 
            variable_to_implied_edges__clauses.size();
    }


    
    int  SCChashsat::
    compileSATforComponent(int component, 
                           std::set<int> freeVariables, 
                           std::set<std::pair<int, int> >& mutex, 
                           std::set<std::pair<int, int> >& equiv,
                           int predecessor)
    {
        variable_id = 1;
        all_CNF_variables = set<int> ();
        completed_CNF = CNF();
        free_variables = CNF();/*2SAT*/
        mutex_variables = CNF();/*2SAT*/
        equiv_variables = CNF();/*2SAT*/
        only_one_action = CNF();/*2SAT*/
        edge_implies_variable__clauses = CNF();/*2SAT*/
        action_implies_edge__clauses = CNF();/*2SAT*/
        edge_to_implied_actions = map<int, Clause>();
        variable_to_implied_edges = map<int, Clause>();
        edge_to_implied_actions__clauses = CNF();/*NSAT*/
        variable_to_implied_edges__clauses = CNF();/*NSAT*/
        goals_in_last_layer = CNF();/*NSAT*/
        noop_clauses = CNF();

        ayplan_noop_vars = std::map<int, int>();
        sat_noop = map<int, int>();
        neg_sat_noop = map<int, int>();
        sat_fluent_to_noop = map<int, int>();

        ayplan_vars = std::map<int, int>() ;
        ayplan_action_vars = std::map<int, int>();
        ayplan_edge_vars = std::map<int, int>();
        setup_ayplan_vars(component, freeVariables, mutex, equiv);
        setup_ayplan_action_vars(component);
        
        /* Makes a CNF that ensure that only one action can execute. */
        exactly_one_action();
        
        for(std::set<int >::iterator p = freeVariables.begin()
                ; p != freeVariables.end()
                ; p++){
            Clause clause;//e(ayplan_vars[*p], - ayplan_vars[*p]);
            clause.insert(ayplan_vars[*p]);
            clause.insert(- ayplan_vars[*p]);
            free_variables.insert(clause);
        }

        for(std::set<std::pair<int, int> >::iterator p = mutex.begin()
                ; p != mutex.end()
                ; p++){
            Clause clause;//(- ayplan_vars[p->first], - ayplan_vars[p->second]);
            clause.insert(- ayplan_vars[p->first]);
            clause.insert(- ayplan_vars[p->second]);
            mutex_variables.insert(clause);
        }
        
    
        for(std::set<std::pair<int, int> >::iterator p = equiv.begin()
                ; p != equiv.end()
                ; p++)
        {
            Clause clause;//(- ayplan_vars[p->first],  ayplan_vars[p->second]);
            clause.insert(- ayplan_vars[p->first]);
            clause.insert(ayplan_vars[p->second]);
            equiv_variables.insert(clause);
            clause = Clause();// ayplan_vars[p->first],  - ayplan_vars[p->second]);
            clause.insert(ayplan_vars[p->first]);
            clause.insert(- ayplan_vars[p->second]);
            equiv_variables.insert(clause);
        }

        /* Add all the noop clauses.*/
        actions_noop_processing();

        /* Non-component actions must be false. */
        if(predecessor != -1){
            actions_prev_component_processing(component, predecessor);
            fluents_prev_component_processing(component, predecessor);
        }


        /* Forbidding goal achievement in last layer, so need a +1
         * for the final returned count for each component in the last layer...*/
        if(component_layers.back().find(component) != component_layers.back().end()){
            int size = calculate_goal_implications_in_last_layer();
            //{char ch; cin>>ch;}
            if(size == 0) return 0;
        }

        return complete_cnf();
    }
    
    
    
    
    bool  SCChashsat::is_foreign_action(int action_id, int component_id, int predecessor){
        
   

        set<int>& edges = action_to_edges[action_id];

        bool self_parent = false;
        bool predecessor_parent = false;
        set<int> components;
        for(set<int>::iterator edge = edges.begin()
                ; edge != edges.end()
                ; edge++){
            assert(sat_edges.size() > *edge);
            SAT_edge& _edge = sat_edges[*edge];
            int origin = _edge.first;
            components.insert(vertex_to_component[origin]);

            if(component_id == vertex_to_component[origin]) self_parent = true;
            if(predecessor == vertex_to_component[origin]) predecessor_parent = true;
        }
        if(components.size() > 2) return false;/*If the action is from more than two components, we will just assume non-foreign.*/
        


        if(!predecessor_parent && self_parent){//no_matching_count == components.size()){
            //exit(-1);
            return true;
        } else {
            return false;
        }


        // bool foreign= false;
        // /*If one of the edges starts in another component, then we forbid the action.*/
        // for(set<int>::iterator edge = edges.begin()
        //         ; edge != edges.end()
        //         ; edge++){
        //     assert(sat_edges.size() > *edge);
        //     SAT_edge& _edge = sat_edges[*edge];
        //     int origin = _edge.first;

        //     assert(origin < vertex_to_component.size());
        //     if(vertex_to_component[origin] != component_id && 
        //        ( predecessor == -1 || vertex_to_component[origin] != predecessor ) ){
                
        //         foreign = true; break;
        //     }
        // }

        // return foreign;
    }

    
    void  SCChashsat::actions_prev_component_processing(int component_id, int predecessor){
        for(map<int, int>::iterator action = ayplan_action_vars.begin()
                ; action != ayplan_action_vars.end()
                ; action++){
            int action_id = action->first;
            int literal = action->second;

            bool foreign = is_foreign_action(action_id, component_id, predecessor);
            
            if(foreign){
                Clause clause;
                clause.insert(-literal);
                only_one_action.insert(clause);
            }
        }
    }
    
    
    bool SCChashsat::is_foreign_fluent(int fluent_id, int component_id, int predecessor){
        
        assert(truthifying_edges__reversed.find(fluent_id) != truthifying_edges__reversed.end());
        set<int>& predecessor_fluents = truthifying_edges__reversed[fluent_id];

        assert(predecessor_fluents.size());
        for(set<int>::iterator predflu = predecessor_fluents.begin()
                ; predflu != predecessor_fluents.end()
                ; predflu++){

            SAT_edge sedge(*predflu, fluent_id);
            assert(sat_edge_id.find(sedge) != sat_edge_id.end());
            int edge_id = sat_edge_id[sedge];
            assert(edge_to_actions.find(edge_id) != edge_to_actions.end());
            set<int>& actions = edge_to_actions[edge_id];  
            assert(actions.size());
            for(set<int>::iterator action = actions.begin()
                    ; action != actions.end()
                    ; action++){
                if(!is_foreign_action(*action, component_id, predecessor)){
                    return false;
                }
            }
        }
        
        return true;
    }

    
    void  SCChashsat::fluents_prev_component_processing(int component_id, int predecessor){
        for(map<int, int>::iterator fluent = ayplan_vars.begin()
                ; fluent != ayplan_vars.end()
                ; fluent++){
            int fluent_id = fluent->first;
            int literal = fluent->second;

            bool foreign = is_foreign_fluent(fluent_id, component_id, predecessor);
            
            if(foreign){
                Clause clause;
                clause.insert(-literal);
                free_variables.insert(clause);
            }
        }
    }

    
    int  SCChashsat::calculate_goal_implications_in_last_layer()
    {
        assert(goals.size());
        //allvarspos();
        Clause goal_clause;
        for(std::set< Proposition<int> >::iterator goal =  goals.begin()
                ; goal != goals.end()
                ; goal++){
            assert(goal->id >= 0);
            if(ayplan_vars.find(goal->id) != ayplan_vars.end()){
                assert(ayplan_vars[goal->id] >= 0);
                goal_clause.insert(- ayplan_vars[goal->id]);
            }
        }
        if(goal_clause.size() == 0) return 0;

        /* Not strictly true. If N-layer components do not contain
         * goal propositions, then this is false. In that case, we can
         * apply this to N-1, and we can actually completely remove
         * the Nth layer from the problem. If this is triggered, we
         * will look into that.*/
        assert(goal_clause.size());
        
        goals_in_last_layer.insert(goal_clause);

        return goal_clause.size();
    }
        
    
    int  SCChashsat::print_CNF(CNF& in){
        for(CNF::iterator p = in.begin()
                ; p != in.end()
                ; p++){
            for(Clause::iterator q = p->begin()
                    ; q != p->end()
                    ; q++){
                //1//cerr<<*q<<", ";
            }
            //1//cerr<<"\n";
        }

        return 0;
    }
        
    
    int  SCChashsat::complete_cnf(){     

        for(std::map<int, int>::iterator v = ayplan_vars.begin()
                ; v != ayplan_vars.end()
                ; v++){
            assert(v->first >= 0);
            assert(v->second >= 0);
            assert(all_CNF_variables.find(v->second) == all_CNF_variables.end());
            all_CNF_variables.insert(v->second);


            //1//cerr<<"Noop variables are :: "<<sat_fluent_to_noop[v->second]
            //1//     <<" and "<<sat_fluent_to_noop[-v->second]<<endl;
            all_CNF_variables.insert(sat_fluent_to_noop[v->second]);
            all_CNF_variables.insert(sat_fluent_to_noop[-v->second]);
        }
        for(std::map<int, int>::iterator v = ayplan_action_vars.begin()
                ; v != ayplan_action_vars.end()
                ; v++){
            //1//cerr<<"Action variable is :: "<<v->second<<std::endl;
            assert(all_CNF_variables.find(v->second) == all_CNF_variables.end());
            all_CNF_variables.insert(v->second);
        }

        for(std::map<int, int>::iterator v = ayplan_edge_vars.begin()
                ; v != ayplan_edge_vars.end()
                ; v++){
            //1//cerr<<"Edge variable is :: "<<v->second<<std::endl;
            assert(all_CNF_variables.find(v->second) == all_CNF_variables.end());
            all_CNF_variables.insert(v->second);
        }

        // for(std::map<int, int>::iterator v = ayplan_noop_vars.begin()
        //         ; v != ayplan_noop_vars.end()
        //         ; v++){
        //     cerr<<"Noop variable is :: "<<v->second<<std::endl;
        //     assert(all_CNF_variables.find(v->second) == all_CNF_variables.end());
        //     all_CNF_variables.insert(v->second);
        // }

        //1//cerr<<"Actions Noops\n";
        print_CNF(noop_clauses);assert(noop_clauses.size());
        //1//cerr<<"Mutex\n";
        print_CNF(mutex_variables);//assert(mutex_variables.size());
        //1//cerr<<"Free variables\n";
        print_CNF(free_variables);//assert(free_variables.size());
        //1//cerr<<"Equiv\n";
        print_CNF(equiv_variables);//assert(equiv_variables.size());
        //1//cerr<<"Only one action\n";
        print_CNF(only_one_action);assert(only_one_action.size());
        //1//cerr<<"Edge implies var\n";
        print_CNF(edge_implies_variable__clauses);assert(edge_implies_variable__clauses.size());
        //1//cerr<<"act implies edge\n";
        print_CNF(action_implies_edge__clauses);assert(action_implies_edge__clauses.size());
        //1//cerr<<"edge implies act\n";
        print_CNF(edge_to_implied_actions__clauses);assert(edge_to_implied_actions__clauses.size());
        //1//cerr<<"var implies edge\n";
        print_CNF(variable_to_implied_edges__clauses);assert(variable_to_implied_edges__clauses.size());
        //cerr<<"goals must be false in last layer\n";
        //print_CNF(goals_in_last_layer);

        completed_CNF.insert(noop_clauses.begin(), noop_clauses.end());
        completed_CNF.insert(free_variables.begin(), free_variables.end());
        completed_CNF.insert(mutex_variables.begin(), mutex_variables.end());
        completed_CNF.insert(equiv_variables.begin(), equiv_variables.end());
        completed_CNF.insert(only_one_action.begin(), only_one_action.end());
        completed_CNF.insert(edge_implies_variable__clauses.begin(), edge_implies_variable__clauses.end());
        completed_CNF.insert(action_implies_edge__clauses.begin(), action_implies_edge__clauses.end());
        completed_CNF.insert(edge_to_implied_actions__clauses.begin(), edge_to_implied_actions__clauses.end());
        completed_CNF.insert(variable_to_implied_edges__clauses.begin(), variable_to_implied_edges__clauses.end());
        //completed_CNF.insert(goals_in_last_layer.begin(), goals_in_last_layer.end());

        return completed_CNF.size();
    }

    
    int SCChashsat::count_all_mutex_with_predecessor(int component, set<int>& predecessors){
        
        assert(component_contents.find(component) != component_contents.end());
        if(!predecessors.size()){// == -1){
            return component_contents[component].size();
        }

        set<int>& contents = component_contents[component];
        int count = contents.size();
        for(set<int>::iterator content =  contents.begin()
                ; content != contents.end()
                ; content++){
            bool foreign = true;

            for(set<int>::iterator p = predecessors.begin()
                    ; p != predecessors.end()
                    ; p++){
                if(!is_foreign_fluent(*content, component, *p)){
                    foreign = false;
                    break;
                }
            }
            

            if(foreign){
                count--;
            }
        }
        return count;
    }
    int SCChashsat::count_all_mutex_with_predecessor(int component, int predecessor){
        
        assert(component_contents.find(component) != component_contents.end());
        if(predecessor == -1){
            return component_contents[component].size();
        }

        set<int>& contents = component_contents[component];
        int count = contents.size();
        for(set<int>::iterator content =  contents.begin()
                ; content != contents.end()
                ; content++){
            bool foreign = is_foreign_fluent(*content, component, predecessor);
            if(foreign){
                cerr<<*content<<" in "<<component<<" from "<<predecessor<<endl;
                count--;
            }
        }
        return count;
    }

    bool SCChashsat::non_interfering_components(int c1, int c2){
        
        if(c1 == c2) return false;
        
        /* Both in layer 0 */
        if(predecessor_components.find(c1) == predecessor_components.end() &&
           predecessor_components.find(c2) == predecessor_components.end()){
            return  true;
        }

        /* Both in layer 0 */
        if(!predecessor_components[c1].size() && !predecessor_components[c2].size()){
            return true;
        }

        
        /* one is the predecessor of the other */
        if( predecessor_components[c1].find(c2) != predecessor_components[c1].end() || 
            predecessor_components[c2].find(c1) != predecessor_components[c2].end() ){
            return false;
        }
        
        pair<int, int> cache_lookup(c1, c2);
        if(cached__non_interfering_components.find(cache_lookup) != cached__non_interfering_components.end()){
            return cached__non_interfering_components[cache_lookup];
        }

        for(set<int>::iterator pred = predecessor_components[c1].begin()
                ; pred != predecessor_components[c1].end()
                ; pred++){
            for(set<int>::iterator pred2 = predecessor_components[c2].begin()
                    ; pred2 != predecessor_components[c2].end()
                    ; pred2++){
                if(!non_interfering_components(*pred, *pred2)){
                    cached__non_interfering_components[cache_lookup] =  false;
                    
                    return false;
                }
            }
        }
        
        cached__non_interfering_components[cache_lookup] =  true;
        return true;
    }
    
    bool SCChashsat::non_interfering_predecessors(int component){
        assert(predecessor_components.find(component) != predecessor_components.end());

        for(set<int>::iterator pred = predecessor_components[component].begin()
                ; pred != predecessor_components[component].end()
                ; pred++){
            set<int>::iterator pred2 = pred;
            pred2++;
            
            for(; pred2  != predecessor_components[component].end(); pred2++){
                if(!non_interfering_components(*pred, *pred2)){
                    return false;
                }
            }
        }
        
        return true;
    }


    int  SCChashsat::worstCaseModelCount(int component, set<int>& predecessors, int vars, int mutexes)
    {
        int n = component_contents[component].size();
        int max_mutex = floor(((n - 1)*(n-1) + n ) / 2);    
        int mutex_count = component_mutex_count[component];
        
        /* If ony one of the components variables can be true in a state.*/
        if(mutex_count != 0 && max_mutex != 0 && mutex_count == max_mutex){
            /* If none of the components predecessors interfere.*/
            if(non_interfering_predecessors(component)){
                int model_elems = count_all_mutex_with_predecessor( component,  predecessors);
                assert(model_elems <=vars );

                //if(model_elems != vars)exit(-1);

                if(model_elems == 0){cerr<<"Error, vacuous component.\n";exit(-1);return 0;}//exit(-1);}
                
                assert(model_elems > 0);

                return (model_elems - 1);
            }
        }

        return worstCaseModelCount(component, -1, vars, mutexes);
    }
    
    int  SCChashsat::worstCaseModelCount(int component, int predecessor, int vars, int mutexes)
    {

        int n = component_contents[component].size();
        int max_mutex = floor(((n - 1)*(n-1) + n ) / 2);    
        int mutex_count = component_mutex_count[component];
        
        /* If ony one of the components variables can be true in a state.*/
        if(mutex_count != 0 && max_mutex != 0 && mutex_count == max_mutex){
            cerr<<"Meassive mutex :: "<<vars<<endl;
            char ch;
            cin>>ch;

            // /* If none of the components predecessors interfere.*/
            // if(non_interfering_predecessors(component)){

            //     /* Calculate the longest plan that does not use any
            //      * actions from predecessor components not equal to
            //      * \argument{predecessor}. */
            //     int model_elems = count_all_mutex_with_predecessor( component,  predecessor);
                
            //     assert(model_elems <=vars );

            //     //if(model_elems != vars)exit(-1);

            //     if(model_elems == 0){cerr<<"Error, vacuous component.\n";exit(-1);return 0;}//exit(-1);}
                
            //     assert(model_elems > 0);

            //     return (model_elems - 1);
            // } else 
                /* Two or more of the components ancestors interfere. */
            {
                return (vars - 1);
            }
        } else if (!using_cachet){
            
            return pow(2, vars) - 1;
        } else if (mutex_only) {
            cerr<<"Got something called mutex only...\n";
            int number =  SCCDiscovery::worstCaseModelCount( component,  vars,  mutexes);

            cerr<<"Number is :: "<<number<<std::endl;
            return  number;
        } 
        
        if(!non_interfering_predecessors(component)){
            return pow(2, vars) - 1;
        }
        return pow(2, vars) - 1;
        
        exit(-1);
        
        int answer  = 0;
        int size_of_problem = compileSATforComponent(component, 
                                                     component_freeVariables[component], 
                                                     component_mutexes[component], 
                                                     component_equivalence[component], 
                                                     predecessor);
        
        if (0 == size_of_problem) return 0;

        answer = ayPlan_main_NSAT(completed_CNF);


        if(answer == 1) {
            cerr<<"Something went wrong with Cachet-based state counting.\n";
            exit(-1);
        }

        //cerr<<"Number of models in layer i > 1,  component = "<<component<<", predecessor = "<<predecessor<<" is :: "<<answer<<std::endl;
        //exit(0);
        
        return answer - 1;
    }

    void SCChashsat::discoverGraph()
    {

        int vertex_count = assignments.size();
        noops_to_action = vector< set<int> >(vertex_count);

        action_to_noops = vector< set<int> >(actions.size());

        set<int> allvariables;
        for(int i = 0; i < assignments.size(); i++){
            allvariables.insert(i);
            for(int j =0 ; j < actions.size(); j++){
                noops_to_action[i].insert(j);
            }
        }
        for(int i =0 ; i < actions.size(); i++){
            action_to_noops[i] = allvariables;

        }


        int action_id = 0;

        set<int> preconditions_for_eearc_test;
        for (typename std::vector<GroundActionType>::iterator action =  actions.begin()
                 ; action != actions.end()
                 ; action++){ 
            for(vector<Proposition<> >::iterator pre = action->_prec.begin()
                    ; pre != action->_prec.end()
                    ; pre++){
                cerr<<pre->id<<" is Precondition "<<std::endl;   
                cerr<<assignments[pre->id].first<<"|->"<<assignments[pre->id].second<<std::endl;
                preconditions_for_eearc_test.insert(pre->id);
            }
        }

        set<int> occurs_as_an_add_effect;
        set<int> occurs_as_an_delete_effect;
        for (typename std::vector<GroundActionType>::iterator action =  actions.begin()
                 ; action != actions.end()
                 ; action++){ 
                
            for(vector<Proposition<> >::iterator add = action->_add.begin(); add != action->_add.end(); add++){
                occurs_as_an_add_effect.insert(add->id);
            }
            for(vector<Proposition<> >::iterator del = action->_del.begin(); del != action->_del.end(); del++){
                occurs_as_an_delete_effect.insert(del->id);
            }
        }
        
        std::set<int> AD_set_difference__adds_and_deletes;
        std::set_difference(occurs_as_an_add_effect.begin(), occurs_as_an_add_effect.end(), 
                            occurs_as_an_delete_effect.begin(), occurs_as_an_delete_effect.end(),
                            std::inserter(AD_set_difference__adds_and_deletes, AD_set_difference__adds_and_deletes.end()));
        
        std::set<int> DA_set_difference__adds_and_deletes;
        std::set_difference(occurs_as_an_delete_effect.begin(), occurs_as_an_delete_effect.end(),
                            occurs_as_an_add_effect.begin(), occurs_as_an_add_effect.end(), 
                            std::inserter(DA_set_difference__adds_and_deletes, DA_set_difference__adds_and_deletes.end()));
        

        std::set<int> set_difference__adds_and_deletes;
        set_difference__adds_and_deletes.insert(AD_set_difference__adds_and_deletes.begin(), AD_set_difference__adds_and_deletes.end());
        set_difference__adds_and_deletes.insert(DA_set_difference__adds_and_deletes.begin(), DA_set_difference__adds_and_deletes.end());

#define NEW_EDGE(X, Y)                                                  \
            if(edges.find(Edge(*X, *Y)) == edges.end() ){               \
                cerr<<"NEW EDGE :: "<<X->id<<" TO "<<Y->id<<std::endl;  \
                cerr<<assignments[X->id].first<<"|->"<<assignments[X->id].second<<std::endl; \
                cerr<<assignments[Y->id].first<<"|->"<<assignments[Y->id].second<<std::endl; \
            }                                                           \
            edges.insert(Edge(*X, *Y));                                 \
                                                                        \
            if(vertex_edges.find(X->id) == vertex_edges.end())          \
                vertex_edges[X->id] = set<int>();                       \
            vertex_edges[X->id].insert(Y->id);                          \
            if(vertex_edges_outgoing.find(X->id) == vertex_edges_outgoing.end()) \
                vertex_edges_outgoing[X->id] = set<int>();              \
            vertex_edges_outgoing[X->id].insert(Y->id);                 \
            if(vertex_edges_incoming.find(Y->id) == vertex_edges_incoming.end()) \
                vertex_edges_incoming[Y->id] = set<int>();              \
            vertex_edges_incoming[Y->id].insert(X->id);                 \

#define IGNORE_PURE_VARIABLE1(X) if ( preconditions_for_eearc_test.find(X->id) == preconditions_for_eearc_test.end() && \
                                      set_difference__adds_and_deletes.find(X->id) != set_difference__adds_and_deletes.end()) { ;} \
            
            
#define IGNORE_PURE_VARIABLE2(X, Y)                                     \
        if ( preconditions_for_eearc_test.find(X->id) == preconditions_for_eearc_test.end() && \
             set_difference__adds_and_deletes.find(X->id) != set_difference__adds_and_deletes.end()) { \
            IGNORE_PURE_VARIABLE1(Y)                                   \
            else { ; }                                    \
        }                                                               \

#define IGNORE_VARIABLE(X)  if ( goals.find(*X) == goals.end()          \
                                 && preconditions_for_eearc_test.find(X->id) == preconditions_for_eearc_test.end() ) { \
            ;                                                           \
        }                                                               \
            

#define XXXYYY(X) if (set_difference__adds_and_deletes.find(X->id) != set_difference__adds_and_deletes.end()) 

#define IGNORE_PAIR(X, Y) if(assignments[X->id].first == assignments[Y->id].first) {;}
        
#define REPORT_BIDIRECTED_ARCS(X, Y)                \
        IGNORE_VARIABLE(X)                          \
        else IGNORE_VARIABLE(Y)                     \
        else XXXYYY(X) { XXXYYY(Y) {; } else NEW_EDGE(Y, X); }   \
        else XXXYYY(Y) { NEW_EDGE(X, Y); }                           \
        else                                            \
        {                                               \
            NEW_EDGE(X, Y);                             \
            NEW_EDGE(Y, X);                             \
        }                                               \


// #define REPORT_BIDIRECTED_ARCS(X, Y)                \
//         XXXYYY(Y)                                   \
//         else XXXYYY(X)                              \
//             else IGNORE_VARIABLE(X)                 \
//             else IGNORE_VARIABLE(Y)                 \
//             else { NEW_EDGE(X, Y);                  \
//                 NEW_EDGE(Y, X);}                    \
        
// #define REPORT_BIDIRECTED_ARCS(X, Y)             \
//         IGNORE_PAIR(X, Y)                        \
//         else IGNORE_VARIABLE(X)                  \
//         else IGNORE_VARIABLE(Y)              \
//         else  IGNORE_PURE_VARIABLE2(X, Y)       \
//         else  IGNORE_PURE_VARIABLE2(Y, X)       \
//         else { NEW_EDGE(X, Y);              \
//                NEW_EDGE(Y, X); }               \
        

                  
        for (typename std::vector<GroundActionType>::iterator action =  actions.begin()
                 ; action != actions.end()
                 ; action++, action_id++){ 
            
            
            if(action->_prec.size() == 0 ) {cerr<<"Error, got an empty precondition.\n";exit(-1);}
            for(vector<Proposition<> >::iterator pre = action->_prec.begin(); pre != action->_prec.end(); pre++){
                
                
                for(vector<Proposition<> >::iterator add = action->_add.begin(); add != action->_add.end(); add++){
                    

                    edges.insert(Edge(*pre,*add));
                    report_add_edge(action_id, pre->id, add->id);
                    action_to_noops[action_id].erase(add->id);
                    noops_to_action[add->id].erase(action_id);

                    for(vector<Proposition<> >::iterator add2 = add; add2 != action->_add.end(); add2++){
                        Proposition<>& p1 = *add;
                        Proposition<>& p2 = *add2;
                        testAndAddNonMutexPair(p1, p2);
                    }
                    
                    set<int> deletes;
                    for(vector<Proposition<> >::iterator del = action->_del.begin(); del != action->_del.end(); del++){
                        Proposition<>& p1 = *add;
                        Proposition<>& p2 = *del;
                        testAndAddMutexPair(p1, p2);
                        
                        //assert(fluentPropositionIndex.find(*del) != fluentPropositionIndex.end());
                        deletes.insert(del->id);
                    }
                    //assert(fluentPropositionIndex.find(*add) != fluentPropositionIndex.end());
                    update__occurs_positively_as_effect_without(add->id, deletes);
                    
                    if(vertex_edges.find(pre->id) == vertex_edges.end())
                        vertex_edges[pre->id] = set<int>();
                    vertex_edges[pre->id].insert(add->id);
                    
                    if(vertex_edges_outgoing.find(pre->id) == vertex_edges_outgoing.end())
                        vertex_edges_outgoing[pre->id] = set<int>();
                    vertex_edges_outgoing[pre->id].insert(add->id);
                    if(vertex_edges_incoming.find(add->id) == vertex_edges_incoming.end())
                        vertex_edges_incoming[add->id] = set<int>();
                    vertex_edges_incoming[add->id].insert(pre->id);

                    
                    if(truthifying_edges.find(pre->id) == truthifying_edges.end())
                        truthifying_edges[pre->id] = set<int>();
                    truthifying_edges[pre->id].insert(add->id);
                    if(truthifying_edges__reversed.find(add->id) == truthifying_edges__reversed.end())
                        truthifying_edges__reversed[add->id] = set<int>();
                    truthifying_edges__reversed[add->id].insert(pre->id);
                }
                
                for(vector<Proposition<> >::iterator del = action->_del.begin(); del != action->_del.end(); del++){
                    edges.insert(Edge(*pre,*del));
                    report_delete_edge(action_id, pre->id, del->id);
                    action_to_noops[action_id].erase(del->id);
                    noops_to_action[del->id].erase(action_id);
                    

                    for(vector<Proposition<> >::iterator del2 = del; del2 != action->_del.end(); del2++){
                        Proposition<>& p1 = *del;
                        Proposition<>& p2 = *del2;
                        testAndAddNonPosMutexPair(p1, p2);
                    } 
                    
                    set<int> adds;
                    for(vector<Proposition<> >::iterator add = action->_add.begin(); add != action->_add.end(); add++){
                        Proposition<>& p1 = *del;
                        Proposition<>& p2 = *add;
                        testAndAddPosMutexPair(p1, p2);
                        
                        adds.insert(add->id);
                    }
                    update__occurs_negatively_as_effect_without(del->id, adds);


                    if(vertex_edges.find(pre->id) == vertex_edges.end())
                        vertex_edges[pre->id] = set<int>();
                    vertex_edges[pre->id].insert(del->id);

                    if(vertex_edges_outgoing.find(pre->id) == vertex_edges_outgoing.end())
                        vertex_edges_outgoing[pre->id] = set<int>();
                    vertex_edges_outgoing[pre->id].insert(del->id);
                    if(vertex_edges_incoming.find(del->id) == vertex_edges_incoming.end())
                        vertex_edges_incoming[del->id] = set<int>();
                    vertex_edges_incoming[del->id].insert(pre->id);


                    
                    if(falsifying_edges.find(pre->id) == falsifying_edges.end())
                        falsifying_edges[pre->id] = set<int>();
                    falsifying_edges[pre->id].insert(del->id);
                    if(falsifying_edges__reversed.find(del->id) == falsifying_edges__reversed.end())
                        falsifying_edges__reversed[del->id] = set<int>();
                    falsifying_edges__reversed[del->id].insert(pre->id);
                }
            }
        }
        

        for (typename std::vector<GroundActionType>::iterator action =  actions.begin()
                 ; action != actions.end()
                 ; action++){ 
            for(vector<Proposition<> >::iterator add = action->_add.begin(); add != action->_add.end(); add++){
                for(vector<Proposition<> >::iterator del = action->_del.begin(); del != action->_del.end(); del++){
                    REPORT_BIDIRECTED_ARCS(add, del);
                }
            }
            
            for(vector<Proposition<> >::iterator add = action->_add.begin(); add != action->_add.end(); add++){
                vector<Proposition<> >::iterator add2 = add;
                for(add2++; add2 != action->_add.end(); add2++){
                    assert(add2 != add);
                    REPORT_BIDIRECTED_ARCS(add, add2);
                }
            }
            for(vector<Proposition<> >::iterator del = action->_del.begin(); del != action->_del.end(); del++){
                vector<Proposition<> >::iterator del2 = del;
                for(del2++; del2 != action->_del.end(); del2++){
                    REPORT_BIDIRECTED_ARCS(del, del2);
                }
            }
        }


	// printf("%d\r\n\r\n\r\n",vertices(graph).first);
	// printf("%d\r\n\r\n\r\n",vertices(graph).second);
        // sleep(15);

        // for(boost::range_detail::integer_iterator<long unsigned int> vertix = vertices.begin(); vertix != vertices(graph).second; vertix++){
	//   ugraph2.add_vertex(*vertix);
	// }
        graph = Graph(assignments.size());//edges.size());
	ugraph = UGraph(assignments.size());
        biggest = 0;
	std::set<int> vertices_local;
	std::set< pair<int, int> > edges_local;
        for(typename Edges::iterator edge = edges.begin(); edge != edges.end(); edge++){
            typename boost::graph_traits<Graph>::vertex_descriptor u, v;
            u = vertex(edge->first.i.id, graph);
            v = vertex(edge->second.i.id, graph);
            
            if(edge->first.i.id > biggest) biggest = edge->first.i.id;
            if(edge->second.i.id > biggest) biggest = edge->second.i.id;

            // cerr<<edge->first.i.id<<"->"<<edge->second.i.id<<" ; "<<std::endl;
            // cerr<<edge->first.i<<"->"<<edge->second.i<<" ; "<<std::endl;

            add_edge(u, v, graph);
            
            if (vertices_local.find(edge->first.i.id) == (vertices_local.end()))
              {
                vertices_local.insert(edge->first.i.id);
		//                ugraph.add_vertex(edge->first.i.id);
              }
            if (vertices_local.find(edge->second.i.id) == (vertices_local.end()))
              {
                vertices_local.insert(edge->second.i.id);
		//                ugraph.add_vertex(edge->second.i.id);
              }
            pair <int, int> temp_edge;
            temp_edge.first = edge->first.i.id;
            temp_edge.second = edge->second.i.id;
            pair <int, int> temp2;
            temp2.first = edge->second.i.id;
            temp2.second = edge->first.i.id;

            if (edges_local.find(temp_edge) == (edges_local.end()) &&
                edges_local.find(temp2) == (edges_local.end()))
              {
                edges_local.insert(temp_edge);
                typename boost::graph_traits<UGraph>::vertex_descriptor u_, v_;
                u_ = vertex(edge->first.i.id, ugraph.impl());
                v_ = vertex(edge->second.i.id, ugraph.impl());
                add_edge(u_, v_, ugraph);                
              }
        }

        largest_action_id = action_id;
    }

}

#include "isolation_counting.hh"

