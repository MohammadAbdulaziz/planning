
#include<map>
#include<vector>
#include <boost/config.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/graph/bron_kerbosch_all_cliques.hpp>
#include <boost/property_map/property_map_iterator.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/iteration_macros.hpp>

#include <sstream>
#include <iostream>

#include "state_var_t.h"
#include "globals.h"
#include "operator_cost.h"
#include "operator.h"
#include "search_space.h"
#include "search_progress.h"


extern int using_cachet;
extern int mutex_only;

#include <boost/graph/undirected_graph.hpp>




namespace Planning
{
    using namespace boost;
    using namespace std;
    typedef undirected_graph<int,int,int> UGraph;
    typedef typename graph_traits<UGraph>::vertex_descriptor MyVtx;
  //    typedef std::deque<MyVtx> Clique;
    typedef std::deque<MyVtx> cliqueVertices;
    typedef std::vector<int> Clique;
    typedef property_map<UGraph, vertex_index_t>::type vertexIndexMap;
    typedef property_map<UGraph, vertex_index_t>::const_type constVertexIndexMap;
    typedef boost::detail::lvalue_pmap_iter<std::vector<Planning::MyVtx>::iterator, vertexIndexMap> vertexIndexMapIterator;
    typedef std::vector<pair<int, int> > Assignments;
  //typedef property_map<UGraph, vertex_index_t>::random_access_iterator IndexMapIter;
    /* A \class{Planner} does a forward search for the goal (see
     * \member{mustBeTrue} and \member{mustBeFalse}) in the
     * state-space \member{states}.
     *
     * NOTE: I assume that there is one \class{Planner} per process.*/
    class SCCDiscovery 
    {
    public:
        string g_plan_filename;

        template<typename T = int>
        class Proposition {
        public:
            bool operator==(const Proposition<T>& rhs) const {return id == rhs.id;}
            bool operator<(const Proposition<T>& rhs) const {return id < rhs.id;}

            Proposition<T>& operator=(const Proposition<T>& in){this->id = in.id; return *this;}


            Proposition(T id = 0):id(id){}
            Proposition(const Proposition&in):id(in.id){}
            T id;
        };
        

        /*Goal facts. */
        std::set< Proposition<int> > goals;
        
        class GroundActionType{
        public:
            vector<Proposition<> > _prec;
            vector<Proposition<> > _add;
            vector<Proposition<> > _del;
            

        };

	SCCDiscovery(vector<Operator>&, string name);
        void discoverGraph();
        void discoverVariables();
        void writeToGraphviz();
        int connectedComponents();
        int connectedCliques();
 
       class Vertex {
        public:
            Vertex():i(){};Vertex(Proposition<>& i):i(i){};Proposition<> i;
            
            bool operator<(const Vertex& v) const {return i < v.i;}
            bool operator==(const Vertex& v) const {return i == v.i;}
        };
        
        typedef pair<Vertex, Vertex> Edge;
        typedef std::set<Edge> Edges;
        Edges edges;

        class local_label_writer {
        public:
            local_label_writer(vector<int>&  vertex_to_component)
                :vertex_to_component(vertex_to_component)
            {
            }

            vector<int>&  vertex_to_component; 
            
            template <class VertexOrEdge>
            void operator()(std::ostream& out, const VertexOrEdge& v) const {
                

                
                int i = static_cast<int>(v);

                out << "[label=\"" << i << " :: " << vertex_to_component[i] << "\"]";
            }
        };

        bool ancestors_dont_mention_goals(int component_id, set<int>& predecessor_components);

        void initialise__occurs_positively_as_effect_without();
        void update__occurs_positively_as_effect_without(int variable, set<int>& variables);

        void initialise__occurs_negatively_as_effect_without();
        void update__occurs_negatively_as_effect_without(int variable, set<int>& variables);

        void orderComponents();
        void orderComponents(map<int, set<int> >& predecessor_comp, map<int, set<int> >& component_cont);
        void componentsGraph();
        void writeComponentOrder();
        int maximumPlanLength(bool parallel);
        int maximumPlanLength(int i, bool parallel);
        void resetLength();
        

        int worstCaseModelCount(int component, int vars, int mutexes);
        bool isMutex(int i, int j);
        bool isPosMutex(int i, int j);

        void testAndAddNonMutexPair(Proposition<>& p1, Proposition<>& p2);
        void testAndAddMutexPair(Proposition<>& p1, Proposition<>& p2);

        void testAndAddNonPosMutexPair(Proposition<>& p1, Proposition<>& p2);
        void testAndAddPosMutexPair(Proposition<>& p1, Proposition<>& p2);

        void computeComponentMutexes();
        void computeComponentAllMutexes();
        void computeComponentPosMutexes();

        void processMutex();
        void processAllMutex();
        void processPosMutex();

        void sasplus_mutex(int component_id);
        void sasplus_mutex();
        
        void finalMutexTransitiveClosure();
        void finalMutexTransitiveClosure(int component_id, set<int> vertices);
        bool disconnectedBAWithout(int item, int a, int b, int component);
        bool disconnectedABWithout(int item, int a, int b, int component);
        bool disconnectedWithout(int item, int a, int b, int component);

        void computeComponentEquivalence();
        void processFluentEquivalence();
        
        void initialiseFreeVariables();
        void freeVariablesByComponent();

        void compute__unit_clause_seeds();
        
        void regression_counting();

    protected:

        /* Maximum range of variable in LHS*/
        map<int, int> maxrange;
        void report_sasvariable_value(int, int );

        /*Cached result.*/
        std::map<int, int> cached__worstCaseModelCount;
        
        vector<Operator>& problem;
        vector<GroundActionType> actions;
        typedef pair<int, int> Assignment;
        typedef std::set<pair<int, int> > AssignmentSet;
        Assignments assignments;
        AssignmentSet assignmentSet;
        typedef std::map<Assignment, int> AssignmentToFluent;
        AssignmentToFluent assignment_to_fluent;
        
        /* Keep track of what's missing from Freiburg's action model.*/
        struct CompleteDelete{
            CompleteDelete(int action, Assignment& assignment):action(action),assignment(assignment) {}

            int action;
            Assignment assignment;
        };
        
        typedef vector<CompleteDelete> CompleteDeletes;
        CompleteDeletes complete_deletes;
        void complete_action_delete_effect(CompleteDelete& completeDelete);
        void complete_action_delete_effects();
        

        
        /* Variables for which all incoming arcs are the same sign. */
        set<int> unit_clause_seed__false;
        set<int> unit_clause_seed__true;

        /* They may not actually be "free" logically, but that are
         * mentioned in the problem. These are variables that occur in
         * a component, but are not mentioned in any of the
         * components constraints. */
        set<int> freeVariables;
        vector<set<int >  > component_freeVariables;

        /* Equivalent. */
        set<pair<int, int> > always_same_truth_value;
        vector<set<pair<int, int> >  > component_equivalence;
        
        /* Elements in this set cannot both be true a the same
         * time. If one is added in an action, the other is deleted
         * and vise versa. */
        set<pair<int, int> > final_mutex_pair;

        /* Pairs of vertices that cannot both be true at the same time
         * in the action description.*/
        set<pair<int, int> > mutex_pair;
        set<pair<int, int> > not_mutex_pair;
        
        /* Pairs of vertices that cannot both be false at the same time
         * in the action description.*/
        set<pair<int, int> > pos_mutex_pair;
        set<pair<int, int> > not_pos_mutex_pair;

        /* Variable symbols occurs as an add without items listed here as deletes.*/
        vector<set<int> > occurs_positively_as_effect_without;
        /* Variable symbols occurs as a delete without items listed here as adds.*/
        vector<set<int> > occurs_negatively_as_effect_without;
        
        /* Components that have no predecessor are in the first
         * layer. Then components that only have predecessors in the
         * first layer are in the second layer.  etc. */
        vector<set<int> > component_layers;
        
        /*Plan length.*/
        map<int,int > component_cost;
        
        /* Set of components that we do regression-based counting from.*/
        set<int> regression_counting_from;

        /* How many 2-sat clauses does the SCC have describing it.*/
        vector<int > component_mutex_count;
        vector<set<pair<int, int> >  > component_mutexes;
        vector<int > component_pos_mutex_count;
        vector<set<pair<int, int> >  > component_pos_mutexes;

        /*If 0 or 1, is a truth value, otherwise is not calculated.*/
        vector<int> cached__ancestors_dont_mention_goals;

        /* Graph on components. */
        map<int, set<int> > successor_components;
        map<int, set<int> > predecessor_components;

        /* For each component, the vertices in that component. */
        map<int, set<int> > component_contents;

        /* Jussi's dependency graph. Integers are fluent indexes. */
        map<int, set<int> > vertex_edges;
        
        map<int, set<int> > falsifying_edges__reversed;
        map<int, set<int> > truthifying_edges__reversed;
        map<int, set<int> > falsifying_edges;
        map<int, set<int> > truthifying_edges;

        /* Incomming directed edges.*/
        map<int, set<int> > vertex_edges_incoming;
        /* Outgoing directed edges.*/
        map<int, set<int> > vertex_edges_outgoing;

        std::vector<int> vertex_to_component;
//map<int, int>  vertex_to_component;

        /* Largest index that can be assigned to a fluent. */
        uint biggest;

        // typedef adjacency_list < listS, vecS, directedS,
        //                          Vertex,  no_property > Graph;
        typedef adjacency_list < listS, 
                                 vecS, 
                                 directedS,
                                 property< vertex_color_t, unsigned int >,
                                 no_property > Graph;
        Graph graph;
        UGraph ugraph;
        UGraph cliqueUGraph;
        std::vector< Planning::Clique > setOfCliques;
    };


}


ostream& operator<<(ostream&o, const Planning::SCCDiscovery::Proposition<>& in){
    return o<<in.id;
}


namespace Planning
{
    
    void SCCDiscovery::compute__unit_clause_seeds(){
        
        for(AssignmentToFluent::iterator p = assignment_to_fluent.begin()
                ; p !=  assignment_to_fluent.end()
                ; p++){
            int index = p->second;
            /* Things make it false, but never true...*/
            if(falsifying_edges__reversed.find(index) != falsifying_edges__reversed.end()
               && truthifying_edges__reversed.find(index) == truthifying_edges__reversed.end()){
                unit_clause_seed__false.insert(index);
            }
            /* Things make it false, but never true...*/
            if(falsifying_edges__reversed.find(index) == falsifying_edges__reversed.end()
               && truthifying_edges__reversed.find(index) != truthifying_edges__reversed.end()){
                unit_clause_seed__true.insert(index);
            }
        }
    }


    
    void SCCDiscovery::testAndAddMutexPair(Proposition<>& p1, Proposition<>& p2)
    {
        //cerr<<"Candidate mutex :: "<<p1<<" "<<p2<<std::endl;

        pair<int, int> frontways(p1.id, p2.id);
        pair<int, int> backways(p2.id, p1.id);
        
        if(mutex_pair.find(frontways) != mutex_pair.end() || mutex_pair.find(backways) != mutex_pair.end()){
            return;
        }
        mutex_pair.insert(pair<int, int>(p1.id, p2.id));
        
    }
    
    
    void SCCDiscovery::testAndAddNonMutexPair(Proposition<>& p1, Proposition<>& p2)
    {
        
        pair<int, int> frontways(p1.id, p2.id);
        pair<int, int> backways(p2.id, p1.id);
        
        if(not_mutex_pair.find(frontways) != not_mutex_pair.end() || 
           not_mutex_pair.find(backways) != not_mutex_pair.end()){
            return;
        }
        not_mutex_pair.insert(pair<int, int>(p1.id, p2.id));
        
    }
    



    
    void SCCDiscovery::testAndAddPosMutexPair(Proposition<>& p1, Proposition<>& p2)
    {
        //cerr<<"Candidate mutex :: "<<p1<<" "<<p2<<std::endl;

        pair<int, int> frontways(p1.id, p2.id);
        pair<int, int> backways(p2.id, p1.id);
        
        if(pos_mutex_pair.find(frontways) != pos_mutex_pair.end() || pos_mutex_pair.find(backways) != pos_mutex_pair.end()){
            return;
        }
        pos_mutex_pair.insert(pair<int, int>(p1.id, p2.id));
    }
    
    
    void SCCDiscovery::testAndAddNonPosMutexPair(Proposition<>& p1, Proposition<>& p2)
    {
        
        pair<int, int> frontways(p1.id, p2.id);
        pair<int, int> backways(p2.id, p1.id);
        
        if(not_pos_mutex_pair.find(frontways) != not_pos_mutex_pair.end() || not_pos_mutex_pair.find(backways) != not_pos_mutex_pair.end()){
            return;
        }
        not_pos_mutex_pair.insert(pair<int, int>(p1.id, p2.id));
        
    }
    









    
    bool  SCCDiscovery::isMutex(int i, int j){
        pair<int, int> or1(i, j);
        pair<int, int> or2(j, i);
        
        return  mutex_pair.find(or1) !=  mutex_pair.end() ||  mutex_pair.find(or2) !=  mutex_pair.end();

    }


    
    bool  SCCDiscovery::isPosMutex(int i, int j){
        pair<int, int> or1(i, j);
        pair<int, int> or2(j, i);
        
        return  pos_mutex_pair.find(or1) !=  pos_mutex_pair.end() ||  pos_mutex_pair.find(or2) !=  pos_mutex_pair.end();

    }


    
    void SCCDiscovery::initialiseFreeVariables(){

        for(AssignmentToFluent::iterator p = assignment_to_fluent.begin()
                ; p !=  assignment_to_fluent.end()
                ; p++){
            freeVariables.insert(p->second);

        }

    }
    
    
    void SCCDiscovery::freeVariablesByComponent(){


       component_freeVariables = vector<set<int > >(component_contents.size());
        for(map<int, set<int> >::iterator p =  component_contents.begin()
                ; p != component_contents.end()
                ; p++){

            for(set<int>::iterator q =  p->second.begin()
                    ; q != p->second.end()
                    ; q++){
                    
                if(freeVariables.find(*q) != freeVariables.end()){
                    component_freeVariables[p->first].insert(*q);
                }
            }
        }
    }

}



/* Interface function to Cachet */
//extern int ayPlan_main(std::set<std::pair<int, int> >& input);//, std::set<std::pair<int, int> >& positive);
//extern int ayPlan_main(std::set<int> freeVariables, std::set<std::pair<int, int> >& mutex, std::set<std::pair<int, int> >& equiv);
extern int ayPlan_main_2SAT(string name, std::set<int> freeVariables, std::set<std::pair<int, int> >& mutex, std::set<std::pair<int, int> >& equiv);


namespace Planning
{
    /* Each mutex eliminates a variable*/
    
    int  SCCDiscovery::worstCaseModelCount(int component, int vars, int mutexes)
    {
        if(cached__worstCaseModelCount.find(component) != cached__worstCaseModelCount.end()){
            return cached__worstCaseModelCount[component];
        }

        int n = component_contents[component].size();
        int max_mutex = floor(((n - 1)*(n-1) + n ) / 2);    
        int mutex_count = component_mutex_count[component];
        
        if(mutex_count != 0 && max_mutex != 0 && mutex_count == max_mutex){
            cerr<<"Fullmutex "<<n<<".\n";
            char ch;
            cin>>ch;
            if(n != vars) {cerr<<"Something went wrong.\n";exit(-1);}
            if(n==0) {cerr<<"empty component\n";exit(-1);}
            cached__worstCaseModelCount[component] = n - 1;

            return n - 1;
        } else if (!using_cachet){
            cout<<"Exponent!\n";
            cached__worstCaseModelCount[component] = (pow(2, vars) -1);
            return (pow(2, vars) -1);
        }

        
        cerr<<"Got this far...\n";

        if(component_equivalence[component].size() != 0){
            cerr<<"Component equivalence...\n";
            exit(-1);
        }

        
        cerr<<"Try again 1...\n";
        cout<<"Cachet "<<mutex_count<<" / "<<vars<<"\n";
        
        if(!component_mutexes[component].size()){// && !component_equivalence[component].size()){
            
        
            cerr<<"Try again 2...\n";
            cached__worstCaseModelCount[component] = (pow(2, vars) -1);
            return (pow(2, vars) - 1);
        } 

        cerr<<"Cachet backdrop...\n";

        int answer  = 0;
        cerr<<"vars :: "<<vars<<" "<<mutexes<<" models "<<component_freeVariables[component].size()<<" "<<answer<<std::endl;
        answer = ayPlan_main_2SAT(g_plan_filename, component_freeVariables[component], component_mutexes[component], component_equivalence[component]);//, component_pos_mutexes[component]);//mutex_pair);
        
        assert(answer > 1);

        cached__worstCaseModelCount[component] = answer - 1;

        assert(answer >= 0);
        return answer - 1;//pow(2, answer);
    }
    
    
    
    void SCCDiscovery::computeComponentEquivalence(){
        
       component_equivalence = vector<set<pair<int, int> > >(component_contents.size());
        for(map<int, set<int> >::iterator p =  component_contents.begin()
                ; p != component_contents.end()
                ; p++){

            for(set<int>::iterator q =  p->second.begin()
                    ; q != p->second.end()
                    ; q++){
                set<int>::iterator r =  q;
                for(r++
                        ; r != p->second.end()
                        ; r++){
                    
                    pair<int, int> fwd(*q, *r);
                    pair<int, int> bkw(*r, *q);
                    if(always_same_truth_value.find(fwd) != always_same_truth_value.end() || 
                       always_same_truth_value.find(bkw) != always_same_truth_value.end()){
                        
                        freeVariables.erase(*q);
                        freeVariables.erase(*r);
                        component_equivalence[p->first].insert(pair<int, int>(*q,*r));
                    }
                    

                    // if(isMutex(*q,*r)){
                    //     component_mutexes[p->first].insert(pair<int, int>(*q,*r));
                    //     component_mutex_count[p->first]++;
                    // }
                }
            }
        }
    }

    
    void  SCCDiscovery::computeComponentMutexes()
    {
       component_mutex_count = vector<int>(component_contents.size());
       component_mutexes = vector<set<pair<int, int> > >(component_contents.size());
        
        for(map<int, set<int> >::iterator p =  component_contents.begin()
                ; p != component_contents.end()
                ; p++){
            component_mutex_count[p->first] = 0;

            for(set<int>::iterator q =  p->second.begin()
                    ; q != p->second.end()
                    ; q++){
                set<int>::iterator r =  q;
                for(r++
                        ; r != p->second.end()
                        ; r++){
                    
                    pair<int, int> fwd(*q, *r);
                    pair<int, int> bkw(*r, *q);
                    if(final_mutex_pair.find(fwd) != final_mutex_pair.end() || 
                       final_mutex_pair.find(bkw) != final_mutex_pair.end()){
                        freeVariables.erase(*q);
                        freeVariables.erase(*r);

                        component_mutexes[p->first].insert(pair<int, int>(*q,*r));
                        component_mutex_count[p->first]++;
                    }
                    

                    // if(isMutex(*q,*r)){
                    //     component_mutexes[p->first].insert(pair<int, int>(*q,*r));
                    //     component_mutex_count[p->first]++;
                    // }
                }
            }
        }
    }
    
    void  SCCDiscovery::computeComponentAllMutexes(){
        computeComponentMutexes();
        computeComponentPosMutexes();
    }

    
    void  SCCDiscovery::computeComponentPosMutexes()
    {
       component_pos_mutex_count = vector<int>(component_contents.size());
       component_pos_mutexes = vector<set<pair<int, int> > >(component_contents.size());
        
        for(map<int, set<int> >::iterator p =  component_contents.begin()
                ; p != component_contents.end()
                ; p++){
            component_pos_mutex_count[p->first] = 0;

            for(set<int>::iterator q =  p->second.begin()
                    ; q != p->second.end()
                    ; q++){
                set<int>::iterator r =  q;
                for(r++
                        ; r != p->second.end()
                        ; r++){
                    
                    if(isPosMutex(*q,*r)){
                        component_pos_mutexes[p->first].insert(pair<int, int>(*q,*r));
                        component_pos_mutex_count[p->first]++;
                    }
                }
            }
        }
    }

    
    bool SCCDiscovery::disconnectedABWithout(int item, int a, int b, int component)
    {
        if(a == item) return false;
        if(b == item) return false;
        if(b == a) return false;
        
        
        /* I can only leave "a" on non-mutex arcs. Otherwise it is turned off...*/

        //cerr<<"Exploring from :: "<<a<<" trying to get to :: "<<b<<" -- without "<<item<<std::endl;

        set<int>& legal_vertices = component_contents[component];
        
        set<int> explored;
        set<int> left_to_explore;
        left_to_explore.insert(a);
        
        while(left_to_explore.size()){
            int exploring = *left_to_explore.begin();
            left_to_explore.erase(exploring);
            explored.insert(exploring);
            
            if(vertex_edges_outgoing.find(exploring) != vertex_edges_outgoing.end()){
                
                set<int>& next = vertex_edges_outgoing[exploring];
                for(set<int>::iterator n = next.begin()
                        ; n != next.end()
                        ; n++){
                    /* Ignore loops.*/
                    if(*n == exploring) continue;
                    if(*n == a) continue;
                    if(*n == item)continue;
                    if(*n == b)return false;
                    if(legal_vertices.find(*n) == legal_vertices.end())continue;

                    /*Already decided to explore this.*/
                    if(left_to_explore.find(*n) != left_to_explore.end()) continue;

                    /*Already explored this one.*/
                    if(explored.find(*n) != explored.end()) continue;
                    
                    /* Can't leave $exploring$ on a mutex edge.*/
                    pair<int, int> illegal_forward(a, *n);
                    pair<int, int> illegal_backward(*n, a); 
                    if(final_mutex_pair.find(illegal_forward) != final_mutex_pair.end() || 
                       final_mutex_pair.find(illegal_backward) != final_mutex_pair.end()){
                        continue;
                    }
                    
                    /*If exploring can't make n true, traversal is impossible.*/
                    if(truthifying_edges.find(exploring) != truthifying_edges.end()){
                        set<int>& truthifying = truthifying_edges[exploring];
                        if(truthifying.find(*n) == truthifying.end()) continue;
                    }

                    
                    
                    /* Haven't already explored $*n$ */
                    //cerr<<"Thinking of exploring :: "<<*n<<std::endl;
                    left_to_explore.insert(*n);
                }
                
            }
        }
        return true;
    }
    
    
    bool SCCDiscovery::disconnectedBAWithout(int item, int a, int b, int component)
    {
        return disconnectedABWithout(item, b, a, component);
    }

    
    bool SCCDiscovery::disconnectedWithout(int item, int a, int b, int component)
    {
        return disconnectedABWithout(item, a, b, component) && disconnectedBAWithout(item, a, b, component) ;
    }
    
    void SCCDiscovery::finalMutexTransitiveClosure(int component_id, set<int> vertices)
    {
        for(set<int>::iterator vertex =vertices.begin()
                ; vertex != vertices.end()
                ; vertex++){
            // if(vertex_edges_incoming.find(*vertex) == vertex_edges_incoming.end()) continue;
            if(vertex_edges_outgoing.find(*vertex) == vertex_edges_outgoing.end()) continue;
            
            set<int>& outgoing = vertex_edges_outgoing[*vertex];

            for(set<int>::iterator _a = outgoing.begin()
                    ; _a != outgoing.end()
                    ; _a++){
                if(*vertex == *_a) continue;
                
                int a = *_a;

                pair<int, int> avf(a, *vertex);
                pair<int, int> avr(*vertex, a);
                
                /* If there is a mutex relationship. */
                if( ( final_mutex_pair.find(avf) != final_mutex_pair.end() || 
                      final_mutex_pair.find(avr) != final_mutex_pair.end()) ) 
                {
                        
                    for(set<int>::iterator _b = vertices.begin()
                            ; _b != vertices.end()
                            ; _b++){
                        int b = *_b;
                        if(b == a) continue;
                        if(b == *vertex) continue;

                        pair<int, int> vbf(*vertex, b);
                        pair<int, int> vbr(b, *vertex);
                        /* make sure the mutex clause is not already in the incumbant problem description. */
                        if(final_mutex_pair.find(vbf) == final_mutex_pair.end() && 
                           final_mutex_pair.find(vbr) == final_mutex_pair.end()){
                            
                            //cerr<<".";
                            //cerr<<"Disconnection test :: without "<<a<<" from "<<*vertex<<" to "<<b<<endl;

                            /* If a and b are disconnected without v. */
                            if(disconnectedWithout(a/*without a*/, *vertex, b, component_id)){

                                //cerr<<"new mutex : "<<*vertex<<" "<<b<<std::endl;
                                /* then add transitive closure of mutex. */
                                final_mutex_pair.insert(vbf);
                            }
                        }
                    }
                }
            }
        }
    }
    
    void SCCDiscovery::finalMutexTransitiveClosure()
    {
        assert(component_contents.size());

        for(map<int, set<int> >::iterator comp = component_contents.begin()
                ; comp != component_contents.end()
                ; comp++){
            finalMutexTransitiveClosure(comp->first, comp->second);
            
        }
    }
    
    
    void SCCDiscovery::processFluentEquivalence(){
        
        set<pair<int, int> > adds_at_the_same_time = not_mutex_pair;
        for(set<pair<int, int> >::iterator p = mutex_pair.begin()
                ; p != mutex_pair.end()
                ; p ++){
            pair<int, int> reversed(*p);
            adds_at_the_same_time.erase(*p);
            adds_at_the_same_time.erase(reversed);
        }
        for(int i =0; i < occurs_positively_as_effect_without.size(); i++){
            int first = i;
            for(set<int>::iterator p = occurs_positively_as_effect_without[i].begin()
                    ; p != occurs_positively_as_effect_without[i].end()
                    ; p++){
                int second = *p;

                pair<int, int> forward(first, second);
                pair<int, int> reversed(second, first);
                adds_at_the_same_time.erase(forward);
                adds_at_the_same_time.erase(reversed);
            }
        }

        set<pair<int, int> > deletes_at_the_same_time = not_pos_mutex_pair;
        for(set<pair<int, int> >::iterator p = pos_mutex_pair.begin()
                ; p != pos_mutex_pair.end()
                ; p ++){
            pair<int, int> reversed(*p);
            deletes_at_the_same_time.erase(*p);
            deletes_at_the_same_time.erase(reversed);
        }
        for(int i =0; i < occurs_negatively_as_effect_without.size(); i++){
            int first = i;
            for(set<int>::iterator p = occurs_negatively_as_effect_without[i].begin()
                    ; p != occurs_negatively_as_effect_without[i].end()
                    ; p++){
                int second = *p;

                pair<int, int> forward(first, second);
                pair<int, int> reversed(second, first);
                deletes_at_the_same_time.erase(forward);
            }
        }
        
        for(set<pair<int, int> >::iterator p = deletes_at_the_same_time.begin()
                ; p != deletes_at_the_same_time.end()
                ; p ++){
            pair<int, int> reverse(p->second, p->first);
            if(adds_at_the_same_time.find(*p) != adds_at_the_same_time.end() ||
               adds_at_the_same_time.find(reverse) != adds_at_the_same_time.end()){
                always_same_truth_value.insert(*p);
            }
        }
        
        //cerr<<" Equivalence count :: "<<always_same_truth_value.size()<<std::endl;
    }

    

    // This finds if all the variables in an SCC are from the same SAS+ variable.
    void SCCDiscovery::sasplus_mutex(int component_id)
    {
        set<int>& variables = component_contents[component_id];

        map<int, set<int> > sas_variables;
        for(set<int>::iterator variable = variables.begin()
                ; variable != variables.end()
                ; variable++){
            
            assert(assignments.size() > *variable);
            
            Assignment& assignment = assignments[*variable];
            if(sas_variables.find(assignment.first) == sas_variables.end()){
                sas_variables[assignment.first] = set<int>();
            }
            //assert(assignment.second >= 0);
            assert(assignment.first >= 0);
            assert(*variable >= 0);
            sas_variables[assignment.first].insert(*variable);//assignment.second);
        }

        //cerr<<"Number of functions is :: "<<sas_variables.size()<<std::endl;

        for( map<int, set<int> >::iterator sas_variable = sas_variables.begin()
                 ; sas_variable !=  sas_variables.end()
                 ; sas_variable++){

            set<int>& component_variables = sas_variable->second;
            
            for(set<int>::iterator variable = component_variables.begin()
                    ; variable != component_variables.end()
                    ; variable++){
                set<int>::iterator variable2 = variable;
                
                for(variable2++
                        ; variable2 != component_variables.end()
                        ; variable2++){
                    pair<int, int> the_pair(*variable, *variable2);
                    assert(*variable >= 0);
                    assert(*variable2 >= 0);
                    if(*variable < 0) exit(-1);
                    if(*variable2 < 0) exit(-1);
                    //cerr<<"Mutex :: "<<*variable<<" "<<*variable2<<std::endl;

                    freeVariables.erase(*variable);
                    freeVariables.erase(*variable2);

                    final_mutex_pair.insert(the_pair);
                }
            }
        }
    }

    void SCCDiscovery::sasplus_mutex()
    {
        processFluentEquivalence();


        for(map<int, set<int> >::iterator component =  component_contents.begin()
                ; component != component_contents.end()
                ; component++){
            sasplus_mutex(component->first);
        }

        
        // processMutex();
        // processPosMutex();
        
        // for(set<pair<int, int> >::iterator p = pos_mutex_pair.begin()
        //         ; p != pos_mutex_pair.end()
        //         ; p++){
        //     pair<int, int> reverse(p->second, p->first);
        //     if(mutex_pair.find(*p) != mutex_pair.end() &&
        //        pos_mutex_pair.find(reverse) != pos_mutex_pair.end()){
        //         final_mutex_pair.insert(*p);
        //     }
        // }

        // cerr<<"We have :: "<<final_mutex_pair.size()<<" mutex pairs.\n";
        
        // int oldSize;// = final_mutex_pair.size();
        // do{
        //     oldSize = final_mutex_pair.size();
        //     //cerr<<"Mutex set size :: "<<oldSize<<std::endl;
        //     finalMutexTransitiveClosure();
        // }while(oldSize < final_mutex_pair.size());


        //cerr<<"Second take.. we have :: "<<final_mutex_pair.size()<<" mutex pairs.\n";
    }
    
    void SCCDiscovery::processAllMutex(){
        
        processFluentEquivalence();
        
        processMutex();
        processPosMutex();
        
        for(set<pair<int, int> >::iterator p = pos_mutex_pair.begin()
                ; p != pos_mutex_pair.end()
                ; p++){
            pair<int, int> reverse(p->second, p->first);
            if(mutex_pair.find(*p) != mutex_pair.end() ||
               mutex_pair.find(reverse) != mutex_pair.end()){
                final_mutex_pair.insert(*p);
            }
        }

        //cerr<<"We have :: "<<final_mutex_pair.size()<<" mutex pairs.\n";
        
        int oldSize;// = final_mutex_pair.size();
        do{
            oldSize = final_mutex_pair.size();
            //cerr<<"Mutex set size :: "<<oldSize<<std::endl;
            finalMutexTransitiveClosure();
        }while(oldSize < final_mutex_pair.size());


        //cerr<<"Second take.. we have :: "<<final_mutex_pair.size()<<" mutex pairs.\n";

    }
    
    
    void SCCDiscovery::processPosMutex(){
        
        for(set<pair<int, int> >::iterator p = not_pos_mutex_pair.begin()
                ; p != not_pos_mutex_pair.end()
                ; p++){
            
            
            pos_mutex_pair.erase(*p);
            pair<int, int> reversed(p->second, p->first);
            
            
            pos_mutex_pair.erase(reversed);
        }


        for(int i =0; i < occurs_negatively_as_effect_without.size(); i++){
            int first = i;
            for(set<int>::iterator p = occurs_negatively_as_effect_without[i].begin()
                    ; p != occurs_negatively_as_effect_without[i].end()
                    ; p++){
                int second = *p;

                pair<int, int> forward(first, second);
                pair<int, int> reversed(second, first);
                
                pos_mutex_pair.erase(forward);
                pos_mutex_pair.erase(reversed);
            }
        }
        
        //cerr<<pos_mutex_pair.size()<<std::endl;
        
        //exit(0);
    }
    
    
    void SCCDiscovery::processMutex(){
        

        for(set<pair<int, int> >::iterator p = not_mutex_pair.begin()
                ; p != not_mutex_pair.end()
                ; p++){
            
            
            mutex_pair.erase(*p);
            pair<int, int> reversed(p->second, p->first);
            
            
            mutex_pair.erase(reversed);
        }


        for(int i =0; i < occurs_positively_as_effect_without.size(); i++){
            int first = i;
            for(set<int>::iterator p = occurs_positively_as_effect_without[i].begin()
                    ; p != occurs_positively_as_effect_without[i].end()
                    ; p++){
                int second = *p;

                pair<int, int> forward(first, second);
                pair<int, int> reversed(second, first);
                mutex_pair.erase(forward);
                mutex_pair.erase(reversed);
            }
        }
        
        //cerr<<mutex_pair.size()<<std::endl;
        
    }


    
    SCCDiscovery::SCCDiscovery(vector<Operator>& problem, string name)
        :problem(problem)
    {
        g_plan_filename = name;
    }
    
    
    void SCCDiscovery::resetLength()
    {
        component_cost = map<int, int >();
    }
    


    
    int SCCDiscovery::maximumPlanLength(bool parallel)
    {
        
        /* Erase cached component costs.*/
        component_cost = map<int, int>();
        
        set<int>& last_layer = component_layers.back();

        assert(last_layer.size());

        int contributions =0;
        for(set<int>::iterator comp =  last_layer.begin()
                ; comp != last_layer.end()
                ; comp++){

            assert(component_contents.find(*comp) != component_contents.end());
            bool useful = false;
            for(std::set< Proposition<int> >::iterator goal =  goals.begin()
                    ; goal != goals.end()
                    ; goal++){
                assert(goal->id >= 0);
                if(component_contents[*comp].find(goal->id) != component_contents[*comp].end()){
                    useful = true;
                    break;
                }
            }
            if(!useful) continue;

            // cerr<<"Cost for ::"<<*comp<<std::endl;;
            if(parallel){
                contributions +=  maximumPlanLength(*comp, parallel);//std::max(contributions, maximumPlanLength(*comp, parallel));
            } else {
                contributions += maximumPlanLength(*comp, parallel);
            }
        }

        return contributions;
    }
    
    /*PTS', BEES & BEEPS*/
    
    int SCCDiscovery::maximumPlanLength(int i, bool parallel)
    {
        if(component_cost.find(i) != component_cost.end()) return component_cost[i];
        
        

        int predecessor_contribution = 0;
        if(predecessor_components.find(i) != predecessor_components.end()){
            predecessor_components[i];
            
            set<int>& predecessor_component = predecessor_components[i];
            for(set<int>::iterator comp = predecessor_component.begin()
                    ; comp != predecessor_component.end()
                    ; comp++){
                if(parallel){
                    predecessor_contribution = std::max(predecessor_contribution, maximumPlanLength(*comp, parallel));
                } else {
                    predecessor_contribution += maximumPlanLength(*comp, parallel);
                }
            }
        }
        assert(component_mutex_count.size() > i);
        int factor = worstCaseModelCount(i, component_contents[i].size(), component_mutex_count[i]);//pow(2, component_contents[i].size());//component_contents[i].size();//worstCaseModelCount(component_contents[i].size(), component_mutex_count[i]);//component_contents[i].size();//pow(2, component_contents[i].size());
        // cerr<<component_contents[i].size()<<"=N, so 2^N="<<factor<<std::endl;

        int answer = (predecessor_contribution)?(factor * predecessor_contribution):factor;
        
        component_cost[i] = answer;
        
        return answer;
    }

    
    void  SCCDiscovery::initialise__occurs_positively_as_effect_without(){
        
        int vertex_count = assignments.size();

        
        occurs_positively_as_effect_without = vector<set<int> >(vertex_count);
        
        set<int> initially;
        for(AssignmentToFluent::iterator p = assignment_to_fluent.begin()
                ; p !=  assignment_to_fluent.end()
                ; p++){
            initially.insert(p->second);

            
        }
        for(int i = 0; i < vertex_count; i++){
            occurs_positively_as_effect_without[i] = initially;
        }
    }
    
    
    void SCCDiscovery::update__occurs_positively_as_effect_without(int variable, set<int>& variables){
        

        assert(variable < occurs_positively_as_effect_without.size());

        for(set<int>::iterator p = variables.begin()
                ; p != variables.end()
                ; p++){
            occurs_positively_as_effect_without[variable].erase(*p);
        }
        //cerr<<variable<<" = "<<occurs_positively_as_effect_without[variable].size()<<"\n";
    }








    
    void  SCCDiscovery::initialise__occurs_negatively_as_effect_without(){
        

        int vertex_count = assignments.size();

        
        occurs_negatively_as_effect_without = vector<set<int> >(vertex_count);
        
        set<int> initially;
        for(AssignmentToFluent::iterator p = assignment_to_fluent.begin()
                ; p !=  assignment_to_fluent.end()
                ; p++){
            initially.insert(p->second);

        }
        for(int i = 0; i < vertex_count; i++){
            occurs_negatively_as_effect_without[i] = initially;
        }
    }
    
    
    void SCCDiscovery::update__occurs_negatively_as_effect_without(int variable, set<int>& variables){
        

        assert(variable < occurs_negatively_as_effect_without.size());

        for(set<int>::iterator p = variables.begin()
                ; p != variables.end()
                ; p++){
            occurs_negatively_as_effect_without[variable].erase(*p);
        }
    }













    
    
    void SCCDiscovery::discoverVariables()
    {
        int vertex_count = 0;

        
        for(vector<pair<int, int> >::iterator goal = g_goal.begin()
                ; goal != g_goal.end()
                ; goal++){
            int variable = goal->first;
            int value = goal->second;

            assert(variable >= 0);
            //assert(value >= 0);
            Assignment assignment(variable, value); 
            
            
            if(assignmentSet.find(assignment) == assignmentSet.end()){
                int index = assignments.size();
                assignments.push_back(assignment);
                assignmentSet.insert(assignment);
                assert(assignment_to_fluent.find(assignment) == assignment_to_fluent.end());
                assignment_to_fluent[assignment] = index;
                assert(assignment_to_fluent[assignment] >= 0);
                assert(index >= 0);
            }
            Proposition<> prop(assignment_to_fluent[assignment]);

            goals.insert(prop);
        }

        for(vector<Operator>::iterator p = problem.begin()
                ; p != problem.end()
                ; p++){
            GroundActionType action;

            std::vector<Prevail> prevail = p->prevail;
            std::vector<PrePost> pre_post = p->pre_post;
            
            for(std::vector<Prevail>::iterator prev = prevail.begin()
                    ; prev != prevail.end()
                    ; prev++){
                int variable = prev->var;
                int ass = prev->prev;
                assert(variable >= 0);
                //assert(ass >= 0);

                report_sasvariable_value(variable, ass);

                Assignment assignment(variable, ass);
                if(assignmentSet.find(assignment) == assignmentSet.end()){
                    int index = assignments.size();
                    assignments.push_back(assignment);
                    assignmentSet.insert(assignment);
                    assert(assignment_to_fluent.find(assignment) == assignment_to_fluent.end());
                    assignment_to_fluent[assignment] = index;
                    assert(assignment_to_fluent[assignment] >= 0);
                    assert(index >= 0);
                }
                Proposition<> prop(assignment_to_fluent[assignment]);
                action._prec.push_back(prop);

            }
            
            for(std::vector<PrePost>::iterator prep = pre_post.begin()
                    ; prep != pre_post.end()
                    ; prep++){
                std::vector<Prevail>& cond = prep->cond;
                if(0 != cond.size()) exit(-1);
                assert(0 == cond.size());
                
                int variable = prep->var;
                int assbefore = prep->pre;
                int assafter = prep->post;
                assert(variable >= 0);
                //assert(assbefore >= 0);
                //assert(assafter >= 0);

                
                report_sasvariable_value(variable, assafter);
                if(assbefore >=0)report_sasvariable_value(variable, assbefore);

                Assignment assignmentbefore(variable, assbefore);
                Assignment assignmentafter(variable, assafter);

                
                if(assbefore >= 0 && assignmentSet.find(assignmentbefore) == assignmentSet.end()){
                    int index = assignments.size();
                    assignments.push_back(assignmentbefore);
                    assignmentSet.insert(assignmentbefore);
                    assert(assignment_to_fluent.find(assignmentbefore) == assignment_to_fluent.end());
                    assignment_to_fluent[assignmentbefore] = index;
                    assert(assignment_to_fluent[assignmentbefore] >= 0);
                    assert(index >= 0);
                }
                if(assignmentSet.find(assignmentafter) == assignmentSet.end()){
                    int index = assignments.size();
                    assignments.push_back(assignmentafter);
                    assignmentSet.insert(assignmentafter);
                    assert(assignment_to_fluent.find(assignmentafter) == assignment_to_fluent.end());
                    assignment_to_fluent[assignmentafter] = index;
                    assert(assignment_to_fluent[assignmentafter] >= 0);
                    assert(index >= 0);
                }
                
                assert(assbefore < 0 || assignment_to_fluent.find(assignmentbefore) != assignment_to_fluent.end());
                assert(assignment_to_fluent.find(assignmentafter) != assignment_to_fluent.end());

                assert(assbefore < 0 || assignment_to_fluent[assignmentbefore] >= 0);
                assert(assignment_to_fluent[assignmentafter] >= 0);
                
                if(assbefore >= 0){
                    Proposition<> propbefore(assignment_to_fluent[assignmentbefore]);
                    action._prec.push_back(propbefore);
                    action._del.push_back(propbefore);
                } else {
                    complete_deletes.push_back(CompleteDelete(actions.size(), assignmentafter));
                }

                Proposition<> propafter(assignment_to_fluent[assignmentafter]);
                action._add.push_back(propafter);
            }
            actions.push_back(action);
        }

        complete_action_delete_effects();
        //cerr<<"Discovered :: "<<assignment_to_fluent.size()<<" variables.\n";
    }

    
    void SCCDiscovery::report_sasvariable_value(int variable, int ass){
        if(maxrange.find(variable) == maxrange.end()){
            maxrange[variable]=ass;
        } else if (maxrange[variable] < ass){
            maxrange[variable]=ass;
        }
    }
    
    void SCCDiscovery::complete_action_delete_effect(CompleteDelete& completeDelete){
        int action_index = completeDelete.action;
        Assignment assignment = completeDelete.assignment;
        int variable_index = assignment.first;
        int value = assignment.second;

        for(int v_imposs = 0; v_imposs < maxrange[variable_index] + 1;  v_imposs++){
            if(v_imposs == value)continue;
            
            Assignment novel_delete(variable_index, v_imposs);
            
            if(assignmentSet.find(novel_delete) == assignmentSet.end()){
                int index = assignments.size();
                assignments.push_back(novel_delete);
                assignmentSet.insert(novel_delete);
                assert(assignment_to_fluent.find(novel_delete) == assignment_to_fluent.end());
                assignment_to_fluent[novel_delete] = index;
                assert(assignment_to_fluent[novel_delete] >= 0);
                assert(index >= 0);
            }
            Proposition<> prop(assignment_to_fluent[novel_delete]);
            actions[action_index]._del.push_back(prop);
        }
        
    }
    
    void SCCDiscovery::complete_action_delete_effects()
    {
        for (int index = 0; index <  complete_deletes.size(); index++){
            complete_action_delete_effect(complete_deletes[index]);
        }  
    }

    void SCCDiscovery::discoverGraph()
    {
      sleep(15);
        assert(actions.size());

        for (typename std::vector<GroundActionType>::iterator action =  actions.begin()
                 ; action != actions.end()
                 ; action++){ 


            for(vector<Proposition<> >::iterator pre = action->_prec.begin(); pre != action->_prec.end(); pre++){
                assert(pre->id >=0);
                
                for(vector<Proposition<> >::iterator add = action->_add.begin(); add != action->_add.end(); add++){
                    assert(add->id >=0);
                    edges.insert(Edge(*pre,*add));
                    
                    for(vector<Proposition<> >::iterator add2 = add; add2 != action->_add.end(); add2++){
                        Proposition<>& p1 = *add;
                        Proposition<>& p2 = *add2;
                        testAndAddNonMutexPair(p1, p2);
                    }
                    
                    set<int> deletes;
                    for(vector<Proposition<> >::iterator del = action->_del.begin(); del != action->_del.end(); del++){
                        assert(del->id >=0);
                        Proposition<>& p1 = *add;
                        Proposition<>& p2 = *del;
                        testAndAddMutexPair(p1, p2);
                        
                        deletes.insert(del->id);
                    }
                    update__occurs_positively_as_effect_without(add->id, deletes);
                    
                    if(vertex_edges.find(pre->id) == vertex_edges.end())
                        vertex_edges[pre->id] = set<int>();
                    vertex_edges[pre->id].insert(add->id);
                    
                    if(vertex_edges_outgoing.find(pre->id) == vertex_edges_outgoing.end())
                        vertex_edges_outgoing[pre->id] = set<int>();
                    vertex_edges_outgoing[pre->id].insert(add->id);
                    if(vertex_edges_incoming.find(add->id) == vertex_edges_incoming.end())
                        vertex_edges_incoming[add->id] = set<int>();
                    vertex_edges_incoming[add->id].insert(pre->id);

                    
                    if(truthifying_edges.find(pre->id) == truthifying_edges.end())
                        truthifying_edges[pre->id] = set<int>();
                    truthifying_edges[pre->id].insert(add->id);
                    if(truthifying_edges__reversed.find(add->id) == truthifying_edges__reversed.end())
                        truthifying_edges__reversed[add->id] = set<int>();
                    truthifying_edges__reversed[add->id].insert(pre->id);
                }
                
                for(vector<Proposition<> >::iterator del = action->_del.begin(); del != action->_del.end(); del++){
                    assert(del->id >=0);
                    edges.insert(Edge(*pre,*del));
                    

                    for(vector<Proposition<> >::iterator del2 = del; del2 != action->_del.end(); del2++){
                        Proposition<>& p1 = *del;
                        Proposition<>& p2 = *del2;
                        testAndAddNonPosMutexPair(p1, p2);
                    } 
                    
                    set<int> adds;
                    for(vector<Proposition<> >::iterator add = action->_add.begin(); add != action->_add.end(); add++){
                        Proposition<>& p1 = *del;
                        Proposition<>& p2 = *add;
                        testAndAddPosMutexPair(p1, p2);
                        
                        adds.insert(add->id);
                    }
                    update__occurs_negatively_as_effect_without(del->id, adds);


                    if(vertex_edges.find(pre->id) == vertex_edges.end())
                        vertex_edges[pre->id] = set<int>();
                    vertex_edges[pre->id].insert(del->id);

                    if(vertex_edges_outgoing.find(pre->id) == vertex_edges_outgoing.end())
                        vertex_edges_outgoing[pre->id] = set<int>();
                    vertex_edges_outgoing[pre->id].insert(del->id);
                    if(vertex_edges_incoming.find(del->id) == vertex_edges_incoming.end())
                        vertex_edges_incoming[del->id] = set<int>();
                    vertex_edges_incoming[del->id].insert(pre->id);

                    if(falsifying_edges.find(pre->id) == falsifying_edges.end())
                        falsifying_edges[pre->id] = set<int>();
                    falsifying_edges[pre->id].insert(del->id);
                    if(falsifying_edges__reversed.find(del->id) == falsifying_edges__reversed.end())
                        falsifying_edges__reversed[del->id] = set<int>();
                    falsifying_edges__reversed[del->id].insert(pre->id);
                }

            }
        }
        

        graph = Graph(assignments.size());//edges.size());
        biggest = 0;
        for(typename Edges::iterator edge = edges.begin(); edge != edges.end(); edge++){
            typename boost::graph_traits<Graph>::vertex_descriptor u, v;
            u = vertex(edge->first.i.id, graph);
            v = vertex(edge->second.i.id, graph);

            if(edge->first.i.id > biggest) biggest = edge->first.i.id;
            if(edge->second.i.id > biggest) biggest = edge->second.i.id;

            // cerr<<edge->first.i.id<<"->"<<edge->second.i.id<<" ; "<<std::endl;
            // cerr<<edge->first.i<<"->"<<edge->second.i<<" ; "<<std::endl;
        }
    }


    
    void SCCDiscovery::writeComponentOrder()
    {
        cout<<"Layers are :: ";
        for( vector<set<int> >::iterator layer =  component_layers.begin()
                 ; layer !=  component_layers.end()
                 ; layer++){
            for(set<int>::iterator p = layer->begin()
                    ; p != layer->end()
                    ; p++){
                cout<<*p<<"("<<component_contents[*p].size()<<"), ";
            }
            cout<<";; ";
        }

        // exit(-1);
    }

    
    void SCCDiscovery::orderComponents()
    {
        orderComponents(predecessor_components, component_contents);
    }


    void SCCDiscovery::regression_counting()
    {
        
        if( component_contents.size() == 1) {
            regression_counting_from.insert(component_contents.begin()->first);
            return;
        }
        
        
        for(map<int, set<int> >::iterator comp =  component_contents.begin()
                ; comp != component_contents.end()
                ; comp++){
            int component = comp->first; 
            if(successor_components.find(component) == successor_components.end()){
                //cerr<<component<<" appears to be a first.\n";
                regression_counting_from.insert(component);
            }
        }
    }
    
    void SCCDiscovery::orderComponents(map<int, set<int> >& predecessor_components, 
                                       map<int, set<int> >& component_contents)
    {
        regression_counting();

        if(!component_contents.size())return;
        
        if( component_contents.size() == 1) {
            set<int> n;
            n.insert(component_contents.begin()->first);
            component_layers.push_back( n);
            return;
        }

        set<int> first_components;
        for(map<int, set<int> >::iterator comp =  component_contents.begin()
                ; comp != component_contents.end()
                ; comp++){
            int component = comp->first; 
            //cerr<<"Trying "<<component<<std::endl;
            if(predecessor_components.find(component) == predecessor_components.end()){
                //cerr<<component<<" appears to be a first.\n";
                first_components.insert(component);
            } else {
                //cerr<<"predecessors are :: \n";
                set<int>& tmp = predecessor_components[component];
                for (set<int>::iterator first = tmp.begin()
                         ; first != tmp.end()
                         ; first++){
                    //cerr<<*first<<", ";
                }//cerr<<"\n";
            }
        }        
        //cerr<<"\n";

        assert(first_components.size() != 0);
        
        component_layers.push_back(first_components);

        map<int, set<int> > copied_component_contents = component_contents;
        for (set<int>::iterator first =  first_components.begin()
                 ; first != first_components.end()
                 ; first++){
            copied_component_contents.erase(*first);
        }
            

        map<int, set<int> > copied_predecessor_components =  predecessor_components;
        set<int> components_to_remove;
        for(map<int, set<int> >::iterator comp = copied_predecessor_components.begin()
                ; comp != copied_predecessor_components.end()
                ; comp++){
            for (set<int>::iterator first =  first_components.begin()
                     ; first != first_components.end()
                     ; first++){
                comp->second.erase(*first);
            }

            if(0 == comp->second.size()){
                components_to_remove.insert(comp->first);
            }
        }

        for(set<int>::iterator comp =  components_to_remove.begin()
                ; comp !=  components_to_remove.end()
                ; comp++ ){
            copied_predecessor_components.erase(*comp);
        }

        orderComponents(copied_predecessor_components, copied_component_contents);        
    }
    
    
    void SCCDiscovery::componentsGraph()
    {
        assert(component_contents.size());

        /*For every component.*/
        for(map<int, set<int> >::iterator comp =  component_contents.begin()
                ; comp != component_contents.end()
                ; comp++){
            int component = comp->first; 
            //cerr<<"Component :: "<<component<<std::endl;

            /*For every vertex in the component.*/
            for(set<int>::iterator vert = comp->second.begin()
                    ; vert != comp->second.end()
                    ; vert++){
                int vertex = *vert;

                // cerr<<"Vertex :: "<<vertex<<std::endl;

                assert(vertex_to_component[vertex] == component);
                if(vertex_edges.find(vertex) == vertex_edges.end()) continue;
                
                set<int>& successors = vertex_edges[vertex];
                
                /*For every successor of every vertex in the component.*/
                for(set<int>::iterator succ = successors.begin()
                        ; succ != successors.end()
                        ; succ++)
                {
                    int successor_component = vertex_to_component[*succ];
                    
                    if(successor_component == component)  continue;

                    // cerr<<"Component :: "<<component<<" -> "<<successor_component<<std::endl;

                    if(successor_components.find(component) ==  successor_components.end())
                        successor_components[component] = set<int>();
                    successor_components[component].insert(successor_component);
                    
                    if(predecessor_components.find(successor_component) ==  predecessor_components.end())
                        predecessor_components[successor_component] = set<int>();
                    predecessor_components[successor_component].insert(component);
                }
            }
        }
    }

    
    int SCCDiscovery::connectedComponents()
    {
        typedef graph_traits<Graph>::vertex_descriptor LocalVertex;
        vertex_to_component = std::vector<int>(num_vertices(graph));
        std::vector<int> discover_time(num_vertices(graph));
        std::vector<default_color_type> color(num_vertices(graph));
        std::vector<LocalVertex> root(num_vertices(graph));
        int num = strong_components(graph, &vertex_to_component[0], 
                                    root_map(&root[0]).
                                    color_map(&color[0]).
                                    discover_time_map(&discover_time[0]));
    
        std::cout << "Total number of components: " << num << std::endl;
        std::vector<int>::size_type i;
        for (i = 0; i != vertex_to_component.size(); ++i){
            
            if(component_contents.find(vertex_to_component[i]) == component_contents.end()){
                component_contents[vertex_to_component[i]] = set<int>();
            }
            
            component_contents[vertex_to_component[i]].insert(i);
            
            // std::cout << "Vertex " << i
            //           <<" is in component " << vertex_to_component[i] << std::endl;
        }
        return num;
    }


    bool SCCDiscovery::ancestors_dont_mention_goals(int component_id, set<int>& predecessors_for_component){


        cerr<<"Component :: "<<component_id<<" is ><= :: "<<cached__ancestors_dont_mention_goals.size()<<endl;
        assert(cached__ancestors_dont_mention_goals.size() > 0);
        assert(cached__ancestors_dont_mention_goals.size() > component_id);

        if(cached__ancestors_dont_mention_goals[component_id] >= 0){
            return cached__ancestors_dont_mention_goals[component_id];
        }
        
        for(set<int>::iterator p = predecessors_for_component.begin()
                ; p != predecessors_for_component.end()
                ; p++){
             if(cached__ancestors_dont_mention_goals[*p] >= 0){
                 assert(cached__ancestors_dont_mention_goals.size() > *p);
                 if(!cached__ancestors_dont_mention_goals[*p]){
                     cached__ancestors_dont_mention_goals[component_id] = false;
                     return false;
                 }
                 continue;
             }

             for(std::set< Proposition<int> >::iterator goal =  goals.begin()
                     ; goal != goals.end()
                     ; goal++){

                 assert(component_contents.find(*p) != component_contents.end());
                 if(component_contents[*p].find(goal->id) != component_contents[*p].end()){
                     cached__ancestors_dont_mention_goals[component_id] = false;
                     return false;
                 }
             }
             
             set<int>& ancestors = predecessor_components[*p];
             
             if(!ancestors_dont_mention_goals(*p, ancestors)){
                 cached__ancestors_dont_mention_goals[component_id] = false;
                 return false;
             }
             
        }

        cached__ancestors_dont_mention_goals[component_id] = true;
        return true;
    }

    void SCCDiscovery::writeToGraphviz()
    {
        //cerr<<"Names copied.\n";
        std::ofstream dotfile ("varGraph.dot");
        std::ofstream dotfile2 ("varUGraph.dot");
        std::ofstream dotfile3 ("cliqueUGraph.dot");
	write_graphviz (dotfile2, ugraph);//, llw);
	write_graphviz (dotfile3, cliqueUGraph);//, llw);
	local_label_writer llw(vertex_to_component);
	write_graphviz (dotfile, graph, llw);

        printf("Size of nodes in directed graph = %d\r\n\r\n", num_vertices(graph));
        printf("Size of nodes in undirected graph = %d\r\n\r\n", num_vertices(ugraph));
        printf("Size of nodes in clique undirected graph = %d\r\n\r\n", num_vertices(cliqueUGraph));
        dotfile.close();
        dotfile2.close();
        dotfile3.close();
    }
}
  //SCCDiscovery


class CustomUndirectedGraph{
public:
  CustomUndirectedGraph(Planning::UGraph & ugraph);
  CustomUndirectedGraph();
  int addEdge(int vtx1, int vtx2);
  bool connected(int vtx1, int vtx2);
  void addVtx(int vtx);
  void delVtx(int vtx);
  void delEdge(int vtx1, int vtx2);
  std::set<int> getNeighbours(int vtx1);
  std::set<int> getVertices();
  std::set<std::pair<int,int> > getEdges();
  std::set<std::pair<int,int> > getVertexEdges(int vtx);
  void removeVertex(int vtx);
  void removeEdge(int vtx1 , int vtx2);
private:
  std::set<int> vertices_;
  std::set<std::pair<int,int> > edges_;
};
typedef std::set<int> CustomClique;

void operator << (std::ostream& f, std::set<int> s)
{
  cout << " " ;
  for (std::set<int>::iterator i = s.begin() ; i != s.end() ; i++)
    {
      f << *i << " ";
    }
}

void operator << (std::ostream& f, Planning::Clique c)
{
  f << "The clique is ";
  for (Planning::Clique::iterator i = c.begin() ; i != c.end() ; i++)
    {
      f << *i << " ";
    }
}

std::set<int> CustomUndirectedGraph::getVertices()
{
  return vertices_;
}

std::set<std::pair<int,int> > CustomUndirectedGraph::getEdges()
{
  return edges_;
}

void operator<<(std::ostream& f, std::pair<int,int> edge)
  {
    cout << "The edge " << edge.first << " to " << edge.second << endl;
  }

int CustomUndirectedGraph::addEdge(int vtx1, int vtx2)
{
  if(! ((vertices_.find(vtx1) != vertices_.end()) || (vertices_.find(vtx2) != vertices_.end())))
    {
      cout << "Problem adding edge, vertix(es) not found, exiting...\r\n";
      exit(1);
      return -1;
    }
  if(vtx1 == vtx2)
    {
      //cout << "Problem adding edge, cycles cant be added!!!!!!!\r\n";
      return 0;
    }
  // if(   (vtx1 == 1 &&  (vtx2 == 6 || vtx2 == 5  || vtx2 == 4  || vtx2 == 3 || vtx2 == 0)) ||
  //       (vtx2 == 1 &&  (vtx1 == 6 || vtx1 == 5  || vtx1 == 4  || vtx1 == 3 || vtx1 == 0)) ||
  //       (vtx1 == 6 && (vtx2 == 1  || vtx2 == 2  || vtx2 == 0  || vtx2 == 3 || vtx2 == 4)) ||
  //       (vtx2 == 6 && (vtx1 == 1  || vtx1 == 2  || vtx1 == 0  || vtx1 == 3 || vtx1 == 4)) ||
  //       (vtx1 == 2 && (vtx2 == 6 || vtx2 == 5)) ||
  //       (vtx2 == 2 && (vtx1 == 6 || vtx1 == 5)) ||
  //       (vtx1 == 5 && (vtx2 == 2 || vtx2 == 1)) ||
  //       (vtx2 == 5 && (vtx1 == 2 || vtx1 == 1)))
  //   {
  //     cout << "This is a banned edge!!!\r\n";
  //     return 0;
  //   }
  if(!(this->connected(vtx1, vtx2)) && vtx1 != vtx2)
      edges_.insert( std::pair<int, int> (vtx1, vtx2));
  return 0;  
}

bool CustomUndirectedGraph::connected(int vtx1, int vtx2)
  {
    return ((edges_.find(std::pair<int, int> (vtx1, vtx2)) != edges_.end()) 
	    || (edges_.find(std::pair<int, int> (vtx2, vtx1)) != edges_.end()) );
  }

void CustomUndirectedGraph::addVtx(int vtx)
  {
    vertices_.insert(vtx);
  }

void CustomUndirectedGraph::delVtx(int vtx)
  {
    vertices_.erase(vtx);
  }

void CustomUndirectedGraph::delEdge(int vtx1, int vtx2)
  {
    edges_.erase(std::pair<int, int> (vtx1, vtx2));
    edges_.erase(std::pair<int, int> (vtx2, vtx1));
  }

CustomUndirectedGraph::CustomUndirectedGraph()
  {
  }
CustomUndirectedGraph::CustomUndirectedGraph(Planning::UGraph& ugraph)
  {
    Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
    Planning::vertexIndexMap index = get(boost::vertex_index, ugraph);
    for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph); ugraphVerticesIter != ugraphVerticesIterEnd; ++ugraphVerticesIter)
      {
        this->addVtx(index[*ugraphVerticesIter]);
      }
    for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph); ugraphVerticesIter != ugraphVerticesIterEnd; ++ugraphVerticesIter)
      {
        Planning::UGraph::in_edge_iterator ugraphEdIter, ugraphEdIterEnd;
        for(boost::tie(ugraphEdIter, ugraphEdIterEnd) = in_edges(*ugraphVerticesIter, ugraph); ugraphEdIter != ugraphEdIterEnd ; ++ugraphEdIter)
          {
            this->addEdge( index[source(*ugraphEdIter, ugraph)], index[*ugraphVerticesIter]);
          }
      }
  }

std::set<int> CustomUndirectedGraph::getNeighbours(int vtx)
{
  std::set<int> neighbours;
  for( std::set< int> ::iterator it = this->vertices_.begin() ; it != vertices_.end() ; ++it) 
    {
      if(*it != vtx)
        {
          if(this->connected(*it, vtx))
            {
              neighbours.insert(*it);              
            }
        }
    }
  // cout << "The neighbours of " << vtx << " are ";
  // cout << neighbours;
  // cout << endl;
  return neighbours;
}


std::set<std::pair<int,int> > CustomUndirectedGraph::getVertexEdges(int vtx)
  {
    //cout << "Getting vertex edges \r\n";
    std::set <std::pair<int,int> > vtxEdges;
    for(std::set<std::pair<int,int> >::iterator i = this->edges_.begin() ; i != this->edges_.end() ; i++)
      {
        if(i->first == vtx)
          {
            vtxEdges.insert(*i);
            // cout << "Inserting edge ";
            // cout << *i;
            // cout << endl;
          }
        else
          {
            if(i->second == vtx)
              {
                vtxEdges.insert(*i);
                // cout << "Inserting edge ";
                // cout << *i;
                // cout << endl;
              }
          }
      }
    return vtxEdges;
  }

void CustomUndirectedGraph::removeVertex(int vtx)
  {
    //cout << "Node to remove " << vtx << endl;
    if(vertices_.find(vtx) != vertices_.end())
      {
        vertices_.erase(vertices_.find(vtx));
	std::set <std::pair<int,int> > vtxEdges = this->getVertexEdges(vtx);
        for(std::set <std::pair<int,int> >::iterator i = vtxEdges.begin() ; i != vtxEdges.end() ; i++)
          {
	    std::set < std::pair<int,int> >::iterator currentEdge = edges_.find(*i);
            if(currentEdge != edges_.end())
              {
                edges_.erase(currentEdge);
              }
            else
              {
                cout << "Error in removing vertex, exiting... \r\n";
                exit(1);
              }
          }
      }
    else
      {
        cout << "Error 2 in removing vertex, exiting... \r\n";
        exit(1);        
      }
  }

void CustomUndirectedGraph::removeEdge(int vtx1 , int vtx2)
  {
    std::set < std::pair<int,int> > ::iterator edgeToRemove = this->edges_.find(std::pair<int, int> (vtx1, vtx2));
    if(edgeToRemove != this->edges_.end())
      {
        // cout << "Edge to remove \r\n";
        // cout << vtx1 << " to " << vtx2 << endl;
        // cout << std::pair<int, int> (vtx1, vtx2) ;
        // cout << *edgeToRemove;
        this->edges_.erase(edgeToRemove);
      }
    else 
      {
        edgeToRemove = this->edges_.find(std::pair<int, int> (vtx2, vtx1));
        if(edgeToRemove != this->edges_.end())
          {
            // cout << "Edge to remove \r\n";
            // cout << vtx2 << " to " << vtx1 << endl;
            // cout << std::pair<int, int> (vtx2, vtx1) ;
            // cout << *edgeToRemove;
            this->edges_.erase(edgeToRemove);
          }
        else
          {
            cout << "Failed to remove the edge, exit....\r\n";
            exit(1);
          }
      }
  }


int getOtherVtx(int vtx, std::pair<int,int> edge)
  {
    // cout << edge;
    // cout << "The vertex is " << vtx << endl;
    if(vtx == edge.first)
      {
        return edge.second;
      }
    else if(vtx == edge.second)
      {
        return edge.first;
      }
    else
      {
        cout << "Problem getting other vertex\r\n";
        exit(1);
      }
  }

std::set<int> customUnion(std::set<int> s, std::set<int> t)
  {
    for( std::set< int> ::iterator it = t.begin() ; it != t.end() ; ++it) 
      {
        s.insert(*it);
      }
    return s;
  }


std::set<int> customInter(std::set<int> s, std::set<int> t)
  {
    std::set<int> interSet;
    for( std::set< int> ::iterator it = t.begin() ; it != t.end() ; ++it) 
      {
        if(s.find(*it) != s.end())
          {
            interSet.insert(*it);
          }
      }
    // cout << "Set 1 is ";
    // cout << s ;
    // cout << endl << "Set 2 is " ;
    // cout << t; 
    // cout << endl;
    // cout << "The intersection is ";
    // cout << interSet;
    // cout << endl;
    return interSet;
  }

int bronlevel = 0 ;
std::set<int> bronKerbosch(std::set<int> R, std::set<int> P, std::set<int> X, CustomUndirectedGraph ugraph)
  {
    bronlevel++;
    // cout << "call level " << bronlevel << " \r\n";
    if(P.size() == 0 && X.size() == 0)
      {
        // cout << "Max clique found !!!!!!" << endl ;
        // cout << R ; 
        // cout << endl;
        bronlevel --;
        return R;
      }
    std::set<int> maxClique;
    int currentVtx = -1;
    if(X.size() > 0 )
      {
        bool flag = true;
        for(std::set<int>::iterator i = P.begin(); i != P.end() ; i++)
          {
            for(std::set<int>::iterator j = X.begin() ; j != X.end() ; j++)
              {
                if(!ugraph.connected(*i,*j))
                  {
                    flag = false;
                    break;
                  }
              }
            if(!flag)
              break;
          }
        if(flag)
          {
            bronlevel --;            
            return maxClique;
          }
      }
    //for( std::set< int> ::iterator it = P.begin() ; it != P.end() ; ++it) 
    while(P.size() != 0)
      { 
        currentVtx = *P.begin();
        // cout << "Current R ";
        // cout << R;
        // cout << endl;
        // cout << "Current P ";
        // cout << P;
        // cout << endl;
        // cout << "Current X ";
        // cout << X;
        // cout << endl;
        // cout << "Current vertex " << currentVtx << endl;
        if(P.find(currentVtx) != P.end())
          P.erase(P.find(currentVtx));
        R.insert(currentVtx);
        std::set<int> currentVtxNeighours = ugraph.getNeighbours(currentVtx);
        std::set<int> tempClique = bronKerbosch(R,
                                                customInter(P, currentVtxNeighours),
                                                customInter(X, currentVtxNeighours),
                                                ugraph);
        if(tempClique.size() != 0 && (tempClique.size() > maxClique.size()))
          maxClique = tempClique;
        R.erase(R.find(currentVtx));
        X.insert(currentVtx);
      }
    bronlevel -- ;
    return maxClique;
  }




//template <typename CliqueT, typename GraphT>
struct max_clique_retainment_visitor{
  max_clique_retainment_visitor(std::size_t& max, Planning::cliqueVertices& cliqueVer, Planning::Clique& cliqueVerIn)
    : maximum(max), cliqueVertices(cliqueVer), cliqueVerticesIndices(cliqueVerIn)
{   }
void clique(const Planning::cliqueVertices& p, const Planning::UGraph& g)
  {
    BOOST_USING_STD_MAX();
    maximum = max BOOST_PREVENT_MACRO_SUBSTITUTION (maximum, p.size());
    if(cliqueVertices.size() < p.size()) //any tie breaking should be put here!!!!!!
      {
        cliqueVertices = p;
        cliqueVerticesIndices.clear();
        Planning::cliqueVertices::const_iterator i = p.begin();
        Planning::constVertexIndexMap index = get(boost::vertex_index, g);
        for(i = p.begin(); i != p.end(); i++) 
          {
            cliqueVerticesIndices.push_back(index[*i]);
            // cout<<"Adding vertex " << index[*i] <<" \r\n";
          }
	//        copyVertexIndices (cliqueVerticesIndices, p, g);
      }
   }
    std::size_t& maximum;
    Planning::cliqueVertices& cliqueVertices;
    Planning::Clique& cliqueVerticesIndices;
  };

int computeCliques(Planning::UGraph& ugraph, std::vector< Planning::Clique> &setOfCliques)
  {
    setOfCliques.erase(setOfCliques.begin(), setOfCliques.end());
    Planning::cliqueVertices c;
    Planning::Clique indices ;
    std::size_t zeroNodes = 0;
    max_clique_retainment_visitor vis(zeroNodes, c, indices);
    Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
    while (num_vertices(ugraph) != 0)
      {
        //getting the maximal clique and putting it in vis
        //TODO: this has to change to get size of 3 at least
	boost::bron_kerbosch_all_cliques(ugraph, vis, 1);
	//printf("Found clique size: %d\r\n",vis.cliqueVertices.size());
	Planning::Clique::iterator i = vis.cliqueVerticesIndices.begin();
        Planning::Clique foundCliqe;
        //removing all the clique members from the graph and adding them to the found clique set of vars
        printf("Graph size before clique removal: %d\r\n\r\n\r\n",num_vertices(ugraph));
        for (i = vis.cliqueVerticesIndices.begin(); i < vis.cliqueVerticesIndices.end(); i++)
          {
            bool foundDescriptor = false;
            Planning::vertexIndexMap index = get(boost::vertex_index, ugraph);
            for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph); ugraphVerticesIter != ugraphVerticesIterEnd; ++ugraphVerticesIter)
	      {
                //printf("Checking matches...\r\n");
                if(index[*ugraphVerticesIter] == *i)                     
                  {
                    foundDescriptor = true;
                    break;
                  }
             } 
            if(foundDescriptor)
	      {	    
                remove_vertex(*ugraphVerticesIter, ugraph);
                foundCliqe.push_back(*i);
	      }
            else
              {
                cerr<<"Clique extraction problem\r\n";
                exit(1);
              }
          }
        printf("Graph size after clique removal: %d\r\n\r\n\r\n",num_vertices(ugraph));
        printf("Found clique size: %d",vis.cliqueVerticesIndices.size());
        cout << " and contents ";
        cout << foundCliqe;
        cout << endl;
        setOfCliques.push_back(foundCliqe);
        //Emptying vis for a new clique         
        vis.cliqueVertices.erase(vis.cliqueVertices.begin(), vis.cliqueVertices.end());
        vis.maximum = 0;
        foundCliqe.clear();   
        //sleep(5);        
      }
    return setOfCliques.size();
  }


int computeCliquesWithSize(Planning::UGraph ugraph, std::vector< Planning::Clique> &setOfCliques, int cliqueSize)
  {
    setOfCliques.erase(setOfCliques.begin(), setOfCliques.end());
    Planning::cliqueVertices c;
    Planning::Clique indices ;
    std::size_t zeroNodes = 0;
    max_clique_retainment_visitor vis(zeroNodes, c, indices);
    Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
    while (1)
      {
        //getting the maximal clique and putting it in vis
        boost::bron_kerbosch_all_cliques(ugraph, vis, cliqueSize);
	//        printf("Found clique size: %d\r\n\r\n\r\n",vis.cliqueVertices.size());
        //printf("Found clique size: %d\r\n\r\n\r\n",vis.cliqueVerticesIndices.size());
        if(vis.cliqueVerticesIndices.size() == 0)
          {
            break;
          }
	Planning::Clique::iterator i = vis.cliqueVerticesIndices.begin();
        Planning::Clique foundCliqe;
        //removing all the clique members from the graph and adding them to the found clique set of vars
        //printf("Graph size before clique removal: %d\r\n\r\n\r\n",num_vertices(ugraph));
        for (i = vis.cliqueVerticesIndices.begin(); i < vis.cliqueVerticesIndices.end(); i++)
          {
            bool foundDescriptor = false;
            Planning::vertexIndexMap index = get(boost::vertex_index, ugraph);
            index = get(boost::vertex_index, ugraph);
            //printf("Checking matches for node %d \r\n", *i);
            for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph); ugraphVerticesIter != ugraphVerticesIterEnd; ++ugraphVerticesIter)
	      {
                if(index[*ugraphVerticesIter] == *i)                     
                  {
                    foundDescriptor = true;
                    break;
                  }
              }
            if(foundDescriptor)
	      {
                remove_vertex(*ugraphVerticesIter, ugraph);
                foundCliqe.push_back(*i);
	      }
            else
              {
                cerr<<"Clique extraction problem\r\n";
                exit(1);
              }
          }
        //printf("Graph size after clique removal: %d\r\n\r\n\r\n",num_vertices(ugraph));
        setOfCliques.push_back(foundCliqe);
        //Emptying vis for a new clique         
        vis.cliqueVerticesIndices.clear();
        vis.cliqueVertices.clear();
        vis.maximum = 0;
        foundCliqe.clear();   
        //sleep(5);        
      }
    //copying nodes which do not belong to any clique as single node cliques
    Planning::vertexIndexMap index = get(boost::vertex_index, ugraph);
    bool included = false;
    Planning::Clique tempCliques ;
    for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph); ugraphVerticesIter != ugraphVerticesIterEnd; ++ugraphVerticesIter)
      {
        //cout << "Adding vertex " << index[*ugraphVerticesIter] << " as a single node clique\r\n";
        tempCliques.push_back(index[*ugraphVerticesIter]);
        setOfCliques.push_back(tempCliques);
        tempCliques.clear();
      }
    return setOfCliques.size();
  }


int computeCliquesUGraph(Planning::UGraph& ugraph, std::vector< Planning::Clique > setOfCliques, Planning::UGraph &cliqueUGraph)
  {
    typename boost::graph_traits<Planning::UGraph>::edge_descriptor e;
    std::size_t cliquesI;
    std::size_t cliquesJ;
    std::size_t inClique1I;
    std::size_t inClique2I;
    cliqueUGraph = Planning::UGraph(setOfCliques.size());
    int cliqueID = 0;
    for(cliquesI = 0; cliquesI < setOfCliques.size(); cliquesI++)
      {  
        for(cliquesJ = cliquesI + 1; cliquesJ < setOfCliques.size(); cliquesJ++)
          {
            for(inClique1I = 0 ; inClique1I < setOfCliques[cliquesI].size(); inClique1I++)
             {
    	       for(inClique2I = 0 ; inClique2I < setOfCliques[cliquesJ].size(); inClique2I++)
               {
                 Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
		 int v1 = setOfCliques[cliquesI][inClique1I];
    		 int v2 = setOfCliques[cliquesJ][inClique2I];
                 //cout << "Checking for matches for nodes " << v1 << " and "<< v2 << endl; 
                 bool flag1 =false;
                 bool flag2 =false;
		 //                 printf("The process ID:\r\n");
		 //                 printf("%d\r\n\r\n\r\n", getpid());
                 // printf("num_vertices: %d\r\n", num_vertices(ugraph));
		 // printf("num_edges: %d\r\n", num_edges(ugraph));
    		 Planning::vertexIndexMap index = get(boost::vertex_index, ugraph);
		 Planning::MyVtx vtx1, vtx2;
                 for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph); ugraphVerticesIter != ugraphVerticesIterEnd; ++ugraphVerticesIter)
    		   {
		     // printf("Current graph node index is %d \r\n", index[*ugraphVerticesIter]);
                     if(index[*ugraphVerticesIter] == v1)                     
                       {
                         flag1 = true;
                         vtx1 = *ugraphVerticesIter;
                       }
                     if(index[*ugraphVerticesIter] == v2)                     
                       {
                         flag2 = true;
                         vtx2 = *ugraphVerticesIter;
                       }
                     if(flag1 && flag2)
                       {
                         break;
                       }
                   }
		 if(flag1 && flag2)
		   ;//printf("Matches FOUND:\r\n");
		 else
		   {
                     printf("Matches NOT FOUND. Problem getting clique graph. Exiting....\r\n");
                     exit(1);
                   }
                 bool e1 = edge(vtx1, vtx2, ugraph).second;
                 bool e2 = edge(vtx2, vtx1, ugraph).second;
                 typename boost::graph_traits<Planning::UGraph>::vertex_descriptor u, v;
                 u = vertex(cliquesI,cliqueUGraph.impl());
                 v = vertex(cliquesJ,cliqueUGraph.impl());
                 bool cliqueE1 = edge(v, u, cliqueUGraph).second;
                 bool cliqueE2 = edge(u, v, cliqueUGraph).second;
                 if((e1 || e2) && !(cliqueE1) && !(cliqueE2))
                   {
                     add_edge(u, v, cliqueUGraph);
                   }
               }
             }          
          }        
      }
  }



void getCliqueOfCliques(std::vector < Planning::UGraph> & graphLayers, std::vector < std::vector< Planning::Clique> > &setOfCliquesfCliques, int cliqueSize)
  {
    int oldCliqueSize = num_vertices(graphLayers[graphLayers.size() - 1]);
    Planning::UGraph cliqueUGraphTempold(num_vertices(graphLayers[graphLayers.size() - 1]));
    cliqueUGraphTempold = graphLayers[graphLayers.size() - 1] ;
    std::vector< Planning::Clique> setOfCliquesTemp;
    Planning::UGraph cliqueUGraphTempnew(num_vertices(graphLayers[graphLayers.size() - 1]));
    int i = 0 ;
    int newCliqueSize = computeCliquesWithSize(cliqueUGraphTempold, setOfCliquesTemp, cliqueSize);
    while ( newCliqueSize != oldCliqueSize)
      {
        cout << "Clique of cliques iteration " << i <<endl;
        computeCliquesUGraph(cliqueUGraphTempold, setOfCliquesTemp, cliqueUGraphTempnew);
        cout << "Size of clique of cliques graph in this iteration is " << num_vertices(cliqueUGraphTempnew) <<endl;
        setOfCliquesfCliques.push_back(setOfCliquesTemp);
        graphLayers.push_back(cliqueUGraphTempnew);
        cliqueUGraphTempold.clear();
        copy_graph(cliqueUGraphTempnew, cliqueUGraphTempold);
        oldCliqueSize = newCliqueSize;
        newCliqueSize = computeCliquesWithSize(cliqueUGraphTempold, setOfCliquesTemp, cliqueSize);
        i++;
      }
  }



bool sameSASVariable(Planning::Clique clique, Planning::UGraph ugraph, std::vector< Planning::Clique > setOfCliques)
  {
    return false;
  }

int intPow(int x, int p)
  {
    if (p == 0) return 1;
    if (p == 1) return x;
    return x * intPow(x, p-1);
  }



typedef int cliqueID;
typedef std::vector< cliqueID > cliqueSet;
typedef std::vector< cliqueSet > setOfCliqueSet;
typedef std::vector< Planning::UGraph > setOfUGraph;
extern int cut_limit;

#define CUT_LIMIT 4
void getChoicesRange(std::vector< std::vector<int> > &choices,  int maxCombinationSize)
  {
    choices.clear();
    int currentCombinationSize;
    for (currentCombinationSize = 1 ; currentCombinationSize <= min(cut_limit,maxCombinationSize) ; currentCombinationSize++)
      {
        std::vector<int> currentCombination;
        currentCombination.clear();
        for (int i=0; i<currentCombinationSize; i++)
 	  currentCombination.push_back(i);
        currentCombination[currentCombinationSize-1] = currentCombinationSize-1-1; // fill initial combination is real first combination -1 for last number, as we will increase it in loop
        do
        {
            if (currentCombination[currentCombinationSize-1] == (maxCombinationSize-1) ) // if last number is just before overwhelm
            {
                int i = currentCombinationSize-1-1;
                while (currentCombination[i] == (maxCombinationSize-currentCombinationSize+i))
                    i--;
                currentCombination[i]++;
                for (int j=(i+1); j<currentCombinationSize; j++)
                    currentCombination[j] = currentCombination[i]+j-i;
            }
            else
                currentCombination[currentCombinationSize-1]++;
	    for (int i=0; i<currentCombinationSize; i++)
      	      cout << currentCombination[i] << " ";
            cout << endl;
            choices.push_back(currentCombination);      
        } while (! ((currentCombination[0] == (maxCombinationSize-1-currentCombinationSize+1)) && (currentCombination[currentCombinationSize-1] == (maxCombinationSize-1))) );
      }
  }

void getPossibleCuts(setOfCliqueSet& setOfPossibleCuts, Planning::UGraph cliqueUGraph)
  { 
    //This should include empty set of cliques
    //This should not include all the cliques because it can lead to infinite recursion
    std::vector< std::vector<int> > choices;
    getChoicesRange(choices, num_vertices(cliqueUGraph));
    for(int i = 0 ; i < choices.size() ; i++)
      {
        cout << choices[i].size() << endl;              
      }
    Planning::vertexIndexMap index = get(boost::vertex_index, cliqueUGraph);
    Planning::Clique cut;
    //accepted cuts
    int acceptedCut = 0;
    for (int i = 0 ; i < choices.size() ; i++)
      {
        //making sure that it is not the full set of cliques
        if(choices[i].size() <= cut_limit  && choices[i].size() < num_vertices(cliqueUGraph))
          {
            Planning::Clique cut;
            cut.clear();
	    cout << "Cut " << i << " clique indices are:";
            Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
            for (int j = 0 ; j < choices[i].size() ; j++)
              {
                int k = 0;
                k = 0;
                for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(cliqueUGraph); ugraphVerticesIter != ugraphVerticesIterEnd; ++ugraphVerticesIter)
                  {
                    if( k == choices[i][j])
                      {
                        cut.push_back(index[*ugraphVerticesIter]);
		        cout << index[*ugraphVerticesIter] << " " ;                       
                      }
		    k++;
		  }
	      }
            setOfPossibleCuts.push_back(cut);       
	    cout << "\r\n";
	    // for( std::vector<int>::const_iterator i = setOfPossibleCuts[acceptedCut].begin(); i != setOfPossibleCuts[acceptedCut].end(); i++)
	    //    std::cout << *i << ' ';
	    // cout << "\r\n";
            // acceptedCut++;
          }
      }
    //adding the empty cut
    //    cut.clear();
    //    setOfPossibleCuts.push_back(cut);
  }

void removeCut(Planning::UGraph& cliqueGraphWithoutCut, cliqueSet cliquesToRemove, Planning::UGraph cliqueUGraph)
  {
    //    boost::copy_graph(cliqueUGraph, cliqueGraphWithoutCut);
    // std::ofstream dotfile ("dbgGraphxyz.dot");
    // boost::write_graphviz (dotfile, cliqueUGraph);//, llw);
    // dotfile.close();
    cliqueGraphWithoutCut.clear();
    cliqueGraphWithoutCut = cliqueUGraph;
    Planning::vertexIndexMap index = get(boost::vertex_index, cliqueGraphWithoutCut);
    Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
    bool flag1 = false;
    bool flag2 = true;
    //    printf("\r\n\r\n\r\n%d\r\n", getpid());
    //    sleep(10);
    for(int i = 0 ; i < cliquesToRemove.size() ; i ++)
      { 
        //cout << "Clique to remove idx: " << cliquesToRemove[i] << "\r\n";
        index = get(boost::vertex_index, cliqueGraphWithoutCut);
        flag1 = false;
        for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(cliqueGraphWithoutCut); ugraphVerticesIter != ugraphVerticesIterEnd; ++ugraphVerticesIter)
          {      
            //cout << "Current clique idx: " << index[*ugraphVerticesIter] << "\r\n";
            if(index[*ugraphVerticesIter] == cliquesToRemove[i])
              {
		boost::clear_vertex(*ugraphVerticesIter, cliqueGraphWithoutCut);
                boost::remove_vertex(*ugraphVerticesIter, cliqueGraphWithoutCut);
                //cout << "Removed clique " << cliquesToRemove[i] << endl;
                flag1 = true;
                break;
              }
          }
        flag2 = flag2 && flag1;
      } 
    if (!flag2)
      {
        //cout << "Problem removing cut!!!!!!!!! Exiting...." << endl;
        exit(1);
      }
  }

class bfs_time_visitor:public boost::default_bfs_visitor {
public:
  bfs_time_visitor(cliqueSet& cliqComp): cliquesComponent(cliqComp){ }
  //  template < typename Vertex, typename Graph >
  void discover_vertex(Planning::MyVtx u, const Planning::UGraph & g) const
  {
    Planning::constVertexIndexMap index = get(boost::vertex_index, g);
    cliquesComponent.push_back(index[u]);
  }
  cliqueSet& cliquesComponent;
};


void B_F_S(cliqueSet& cliqueComponent, Planning::UGraph::vertex_descriptor vtx, Planning::UGraph cliqueUGraph)
  {
    Planning::UGraph::in_edge_iterator ugraphEdIter, ugraphEdIterEnd;
    Planning::vertexIndexMap index = get(boost::vertex_index, cliqueUGraph);
    std::queue<Planning::UGraph::vertex_descriptor> q;
    q.push(vtx);
    cliqueComponent.push_back(index[vtx]);
    bool visited = false;
    Planning::UGraph::vertex_descriptor currentVtx;
    cout << "Starting BFS from node " << index[vtx] << endl;
    while(!q.empty() )
      {
        currentVtx = q.front();
        q.pop();
        for(boost::tie(ugraphEdIter, ugraphEdIterEnd) = in_edges( currentVtx, cliqueUGraph); ugraphEdIter != ugraphEdIterEnd ; ++ugraphEdIter)
          {
            cout << "Current edge is " << index[currentVtx] << " to " << index[source(*ugraphEdIter, cliqueUGraph)] << endl;
            for(int i = 0 ; i < cliqueComponent.size() ; i++)
              {
                if(cliqueComponent[i] == index[source(*ugraphEdIter, cliqueUGraph)])
                  {
                    visited = true;
                  }
              }
            if(!visited)
              {
                cout << "Adding node " << index[source(*ugraphEdIter, cliqueUGraph)] << endl;
                cliqueComponent.push_back(index[source(*ugraphEdIter, cliqueUGraph)]);
                q.push(source(*ugraphEdIter, cliqueUGraph));
              }
            visited = false;
          }
      } 
  }

void getConnectedCliques(setOfCliqueSet& setOfCliqueComponents, Planning::UGraph cliqueUGraph)
  {
    setOfCliqueComponents.clear();
    using namespace boost;
    Planning::vertexIndexMap index = get(boost::vertex_index, cliqueUGraph);
    Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
    boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(cliqueUGraph);
    while (num_vertices(cliqueUGraph) != 0)
      {
        cliqueSet cliqueComponent;
        boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(cliqueUGraph);
	bfs_time_visitor vis(cliqueComponent);
        vis.cliquesComponent.clear();
        cout << "Clique index " << index[*(ugraphVerticesIter)] <<endl;
        cout << "Current clique graph size " << num_vertices(cliqueUGraph)<<endl;;
        // boost::copy_graph(cliqueUGraph, graph);  
	B_F_S(vis.cliquesComponent, *(ugraphVerticesIter),  cliqueUGraph);
        //breadth_first_search(cliqueUGraph, *(ugraphVerticesIter), visitor(vis));
        //cout << "A components is: ";
        //for(int i = 0 ; i < vis.cliquesComponent.size() ; i++)
          //cout <<  vis.cliquesComponent[i] << " ";
	  //cout << endl;
        cout << "Added nodes ";
        for(int i = 0 ; i < vis.cliquesComponent.size() ; i++)
          { 
            index = get(boost::vertex_index, cliqueUGraph);
            for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(cliqueUGraph); ugraphVerticesIter != ugraphVerticesIterEnd; ++ugraphVerticesIter)
              { 
                if(index[*ugraphVerticesIter] == vis.cliquesComponent[i])
                  {
                    cout << index[*ugraphVerticesIter] << " ";
    		    boost::clear_vertex(*ugraphVerticesIter, cliqueUGraph);
                    boost::remove_vertex(*ugraphVerticesIter, cliqueUGraph);                   
                    break;
                  }
              }
          }
        cout << " to component " << setOfCliqueComponents.size() << endl;
        setOfCliqueComponents.push_back(vis.cliquesComponent);
      }
    // std::ofstream dotfile ("dbgGraph.dot");
    // boost::write_graphviz (dotfile, cliqueUGraph);//, llw);
    // dotfile.close();
    // boost::copy_graph(cliqueUGraph, graph);  
  }

void getRemainingGraphs(setOfUGraph& subGraphs, setOfCliqueSet setOfCliqueComponents, cliqueSet cutCliques, Planning::UGraph cliqueUGraph)
  {
    subGraphs.clear();
    Planning::UGraph ugraph_temp(cliqueUGraph);
    Planning::vertexIndexMap index;   
    for(int i = 0; i < setOfCliqueComponents.size() ; i++)
      {
        //cout << "The current clique component is:";
        for(int j = 0 ; j < setOfCliqueComponents[i].size() ; j++)
          {
	    //cout << setOfCliqueComponents[i][j] << " ";
          }
	//cout << endl;
	ugraph_temp = cliqueUGraph;
        Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
        boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph_temp);
        while(ugraphVerticesIter != ugraphVerticesIterEnd)
          {  
            bool found = false;
            found = false;
            for(int j = 0 ; j < setOfCliqueComponents[i].size() ; j++)
              {       
                if(index[*ugraphVerticesIter] == setOfCliqueComponents[i][j])
                  {
             	    //cout << setOfCliqueComponents[i][j] << " ";
                    found = true; 
                  }
              }
            for(int j = 0 ; j < cutCliques.size() ; j++)
              {       
                if(index[*ugraphVerticesIter] == cutCliques[j])
                  {
             	    //cout << setOfCliqueComponents[i][j] << " ";
                    found = true; 
                  }
              }
            if(!found)
              {  
                boost::clear_vertex(*ugraphVerticesIter, ugraph_temp);
                boost::remove_vertex(*ugraphVerticesIter, ugraph_temp);                   
		index = get(boost::vertex_index, ugraph_temp);
		boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph_temp);
	      }
            else
              {
                ugraphVerticesIter++;
              }
          }
        subGraphs.push_back(ugraph_temp);
	//cout << "Subgraph " << i << " size is " <<num_vertices(subGraphs[i]) ; 
        //cout << endl;
      }
    // std::ofstream dotfile ("dbgGraph.dot");
    // boost::write_graphviz (dotfile, subGraphs[0]);//, llw);
    // dotfile.close();
    // std::ofstream dotfile1 ("dbgGraph1.dot");
    // boost::write_graphviz (dotfile1, subGraphs[1]);//, llw);
    // dotfile1.close();
    // std::ofstream dotfile2 ("dbgGraph2.dot");
    // boost::write_graphviz (dotfile2, subGraphs[2]);//, llw);
    // dotfile2.close();
    // std::ofstream dotfile3 ("dbgGraph3.dot");
    // boost::write_graphviz (dotfile3, subGraphs[3]);//, llw);
    // dotfile3.close();
  }

int getCliqueBound(Planning::Assignments sasAss, Planning::Clique clique)
  {
    std::map <int,int> varNums;
    for(int i = 0 ; i < clique.size() ; i++)
      {
         varNums[sasAss[clique[i]].first]++;
      }
    int cliqueCard = 1;    
    for (std::map<int,int>::iterator it=varNums.begin(); it!=varNums.end(); ++it)
      {
        cout << "This clique has " << it->second << " variable belonging to SAS+ variable" << it->first << endl;        
        cliqueCard *= it->second;
      }
    cout << "Current clique has variables from " << varNums.size() << " SAS+ variables." << endl;
    return cliqueCard ;
  }

int getCliqueBound(Planning::Assignments sasAss, std::set<int> clique, std::map <int,Planning::Clique > sasVarMap)
  {
    std::map <int,int> varNums;
    for(std::set<int>::iterator i =  clique.begin(); i != clique.end() ; i++)
      {
         varNums[sasAss[*i].first]++;
      }
    int cliqueCard = 1;    
    for (std::map<int,int>::iterator it=varNums.begin(); it!=varNums.end(); ++it)
      {
        cout << "This clique has " << it->second << " variable belonging to SAS+ variable" << it->first << endl;        
        if(it->second == sasVarMap[it->first].size())
          {
            cout << "This includes all the members of the clique;\r\n";
            cliqueCard *= it->second;
          }
        else
          {
            cliqueCard *= (it->second + 1);
          }
        //check whether it is a full SAS variable or not

      }
    cout << "Current clique has variables from " << varNums.size() << " SAS+ variables." << endl;
    return cliqueCard ;
  }


int level = 0;
long long unsigned int computeBound( Planning::UGraph cliqueUGraph, Planning::UGraph ugraph, std::vector< Planning::Clique > setOfCliques, Planning::Assignments sasAss)
  {
    level ++;
    if(level == 2)
      {
        cout << "here\r\n";
      } 
    cout << "Level = " << level << endl;
    setOfCliqueSet setOfCliqueComponents;
    setOfCliqueSet setOfPossibleCuts;
    Planning::UGraph GraphWithoutCut;
    setOfUGraph subGraphs;
    Planning::vertexIndexMap cliqueIndices = get(boost::vertex_index, cliqueUGraph);
    bool bestboundChanged = false;
    long long unsigned int bestBound = 0;
    if(num_vertices(cliqueUGraph) <= 1)
      {
        Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
        boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(cliqueUGraph);      
        int cliqueIndex = cliqueIndices[*ugraphVerticesIter];
        cout << "This is a single clique with index " << cliqueIndex << " and size " << setOfCliques[cliqueIndex].size() << endl;
        if( sameSASVariable ( setOfCliques[cliqueIndex], ugraph, setOfCliques ) )
          {
            bestBound = setOfCliques[cliqueIndex].size();
            bestboundChanged = true;
            cout << "This clique is a single SAS variable" << endl;
            cout << "The clique bound is " << bestBound << endl; 
            level --;
            return bestBound; 
          }
        else
          {
            bestBound = getCliqueBound(sasAss, setOfCliques[cliqueIndex]); //intPow(2,setOfCliques[cliqueIndex].size());
 	    bestboundChanged = true;
            cout << "This clique is not a single SAS variable" << endl;
            cout << "The clique bound is " << bestBound << endl;
            level --;
            return bestBound; 
	  }
      }
    else
      {
        cout << "The size of this clique graph is:  " << num_vertices(cliqueUGraph) << endl;
        //get vertex cut candidates
        getPossibleCuts(setOfPossibleCuts, cliqueUGraph);
        cout << "The number of cuts suggested: " << setOfPossibleCuts.size() <<  endl;
        cliqueSet cliquesSet;
        long long unsigned int boundAccumilator = 0;
        long long unsigned int cutBound = 0;
        int bestCutIndex = -1;
        std::size_t i;
        for(i = 0; i < setOfPossibleCuts.size() ; i++)
          { 
            cout << "The current cut to try is " << i  << " at level " << level << endl;
            //remove cut from clique graph
            removeCut(GraphWithoutCut, setOfPossibleCuts[i], cliqueUGraph);
            cout << "The size of the remaining graph is : " << num_vertices(GraphWithoutCut) << endl;
            //determine the remaining components. 
            getConnectedCliques(setOfCliqueComponents, GraphWithoutCut);
            cout << "After cutting the graph contains " << setOfCliqueComponents.size() << " seperate components"  << endl;
	    if(setOfCliqueComponents.size() < 2)
	      {
		cout << "This suggestion doesnt cut, so it will be skipped."<< endl;                
              }
	    else
	      {
                //get cut bound
                setOfCliqueSet singletonCutComponent;
                setOfUGraph cutSingletonUGraph;
                cout << "current cut is of size " << setOfPossibleCuts[i].size() << endl;
                singletonCutComponent.push_back(setOfPossibleCuts[i]);
                // getRemainingGraphs(cutSingletonUGraph, singletonCutComponent, cliqueUGraph);
    	        //cout << "The cut clique component is of size " << num_vertices(cutSingletonUGraph[0]) << " components "<< endl;
                // cutBound =  computeBound(cutSingletonUGraph[0], ugraph, setOfCliques, sasAss);
                // cout << "The cut component " << i << " bound is " << cutBound << endl;
                //Get vector of remaining subgraphs after removing the cut
                getRemainingGraphs(subGraphs, setOfCliqueComponents, setOfPossibleCuts[i], cliqueUGraph);
                if(subGraphs.size() != setOfCliqueComponents.size())
                  {
                    cout << "Problem with subgraph computation. Exiting..." << endl;
                    exit(1);
                  }
                cout << "The sub graphs are " << subGraphs.size() << " after cutting."  << endl;
                std::size_t j;
                for(j = 0; j < subGraphs.size() ; j++)
 	          {
                    int childBound = 0;
                    cout << "Bound acummilator is " << boundAccumilator <<endl;      
                    childBound = computeBound(subGraphs[j], ugraph, setOfCliques, sasAss);
                    cout << "Child " << j << " bound " << childBound <<endl;
                    boundAccumilator = boundAccumilator + childBound;          
                  }
                cout << "The bound is " << boundAccumilator << " if cut " << i << " is chosen" <<  endl;
                if(!bestboundChanged || bestBound > boundAccumilator)
                  {
                    cout << "Cut " << i << " bound is the best bound until now." << endl;
                    bestBound = boundAccumilator; 
		    bestboundChanged = true;
                    bestCutIndex = i;
                  }      
                boundAccumilator = 0;
              }
          }
        if(!bestboundChanged)
          {
            cout << "This clique graph has no cut (i.e. a clique of cliques), computing as a single component" << endl ;
          }
        cout << "Computing the bound with out cuts\r\n";
        boundAccumilator = 1;
        long long unsigned int currentCliqueBound = 0;
        Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
        int cliqueIndex = cliqueIndices[*ugraphVerticesIter];
        for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(cliqueUGraph); ugraphVerticesIter != ugraphVerticesIterEnd; ++ugraphVerticesIter)
          {
            cliqueIndex = cliqueIndices[*ugraphVerticesIter];
            cout << "This is the clique with index " << cliqueIndex << " and size " << setOfCliques[cliqueIndex].size() << endl;
            if( sameSASVariable ( setOfCliques[cliqueIndex], ugraph, setOfCliques ) )
              {
                boundAccumilator = boundAccumilator * setOfCliques[cliqueIndex].size();
                cout << "This clique is a single SAS variable" << endl;
                cout << "The clique bound is " << setOfCliques[cliqueIndex].size() << endl; 
              }
            else
              {
                currentCliqueBound = getCliqueBound(sasAss, setOfCliques[cliqueIndex]);
                boundAccumilator = boundAccumilator * currentCliqueBound; //intPow(2,setOfCliques[cliqueIndex].size());intPow(2,setOfCliques[cliqueIndex].size());
                cout << "This clique is not a single SAS variable" << endl;
                cout << "The clique bound is " << currentCliqueBound << " and the current accumilator value is " << boundAccumilator << endl;//intPow(2,setOfCliques[cliqueIndex].size()) << endl;
              }                   
            }
        boundAccumilator--;
        cout << "The total bound is " << boundAccumilator << "for this clique graph without cutting it."<< endl;
        if(!bestboundChanged || bestBound > boundAccumilator)
          {
             cout << "The bound without cutting is the best bound." << endl;
             bestBound = boundAccumilator; 
             bestboundChanged = true;
             bestCutIndex = -1;
          }      
        cout << "The best bound is " << bestBound << " from cut index "<<bestCutIndex << endl;
        level --;
        return bestBound;
      }
  }


void getCliqueInterfaces(setOfCliqueSet& interfaces,
                         cliqueSet& adjacentCliques,
                         int cliqueIndex, 
                         Planning::UGraph& cliqueLevelUGraph,
                         Planning::UGraph& subCliqueLevelUGraph, 
                         std::vector< Planning::Clique> setOfCliques )
  {
    cout << "Getting clique " << cliqueIndex << " interfaces. Its members are ";
    for ( int i ; i < setOfCliques[cliqueIndex].size() ; i++)
      {
        cout << setOfCliques[cliqueIndex][i] << " ";
      } 
    cout << endl;    
    interfaces.clear();
    adjacentCliques.clear();
    Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
    Planning::vertexIndexMap cliqueIndices = get(boost::vertex_index, cliqueLevelUGraph);
    int cliqueIndex2;    
    bool found = false;
    cout << "Finding vertex descriptor, comparing indices: ";
    for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(cliqueLevelUGraph); ugraphVerticesIter != ugraphVerticesIterEnd; ++ugraphVerticesIter)
      {
        cout << cliqueIndices[*ugraphVerticesIter] << " ";
        cliqueIndex2 = cliqueIndices[*ugraphVerticesIter];
        if(cliqueIndex2 == cliqueIndex)
          {
            found = true;
            break;
          }        
      }    
    cout << endl;
    if(!found)
      {
        cout << "Problem in getting interfaces, exiting..." << endl;
        exit(1);
      }
    //finding adjacent nodes
    Planning::UGraph::in_edge_iterator ugraphEdIter, ugraphEdIterEnd;
    Planning::vertexIndexMap subCliqueIndices = get(boost::vertex_index, subCliqueLevelUGraph);
    for(boost::tie(ugraphEdIter, ugraphEdIterEnd) = in_edges( *ugraphVerticesIter, cliqueLevelUGraph); ugraphEdIter != ugraphEdIterEnd ; ++ugraphEdIter)
      {
        cliqueSet currentInterface;
        currentInterface.clear();
        int adjacentCliqueIdx = cliqueIndices[source(*ugraphEdIter, cliqueLevelUGraph)];
        cout << "Finding I/F to clique " << adjacentCliqueIdx << " whose size is " << setOfCliques[adjacentCliqueIdx].size() << " and whose members are ";
        for ( int i = 0; i < setOfCliques[adjacentCliqueIdx].size() ; i++)
          {
            cout << setOfCliques[adjacentCliqueIdx][i] << " ";
          } 
        cout << endl;        
        //Finding the interface to the current adjacent node        
        for(int i = 0 ; i < setOfCliques[cliqueIndex].size() ; i++)
          {
           //Getting the descriptor of the current member in the clique
           Planning::UGraph::vertex_iterator subCliqueUgraphVerticesIter, subCliqueUgraphVerticesEnd;
           found = false;
           Planning::vertexIndexMap subCliqueIndices = get(boost::vertex_index, subCliqueLevelUGraph);
           for(boost::tie(subCliqueUgraphVerticesIter, subCliqueUgraphVerticesEnd) = vertices(subCliqueLevelUGraph); 
               subCliqueUgraphVerticesIter != subCliqueUgraphVerticesEnd;
               ++subCliqueUgraphVerticesIter)
              {
                if( setOfCliques[cliqueIndex][i] == subCliqueIndices[*subCliqueUgraphVerticesIter])
                  {
                    found = true;
                    break;
                  }        
              }
            if(!found)
              {
                cout << "Problem in getting interfaces 2, exiting..." << endl;
                exit(1);
              }
            //Checking whether the current member of the clique is connected to a node in the adjacent clique, i.e. it is a member in the interface
            Planning::UGraph::in_edge_iterator ifEdIter, ifEdEnd;
            for(boost::tie(ifEdIter, ifEdEnd) = in_edges( *subCliqueUgraphVerticesIter, subCliqueLevelUGraph); ifEdIter != ifEdEnd ; ++ifEdIter)
              {
                int subCliqueAdjacentNodeIdx = subCliqueIndices[source(*ifEdIter, subCliqueLevelUGraph)];
                for(int j = 0 ; j < setOfCliques[adjacentCliqueIdx].size() ; j++)
                  {
                    if(setOfCliques[adjacentCliqueIdx][j] == subCliqueAdjacentNodeIdx)
                      {
                        cout << "Node " << setOfCliques[cliqueIndex][i] << " is added to the interface between cliques " << cliqueIndex << " and " << adjacentCliqueIdx <<endl;
                        currentInterface.push_back(setOfCliques[cliqueIndex][i]);
                        break;
                      }
                  }
              }
          }
        //if the interface is total, this is not a cut
        if(currentInterface.size() < setOfCliques[cliqueIndex].size())
          {
            interfaces.push_back(currentInterface);
            adjacentCliques.push_back(adjacentCliqueIdx);
          }
        else
          {
            cout << "Clique " << cliqueIndex << " and " << adjacentCliqueIdx << " are fully connected, no I/F between them." << endl;
          }
        currentInterface.clear();
      }
  }

int postProcessRemainingGraphs(setOfUGraph& remainingGraphs,
                                Planning::UGraph singletonCutGraph,
                                int adjacentCliqueIndex)
  {
    //For each of the remaining graphs: remove the non-interface nodes from the cut clique content  
    if (remainingGraphs .size() == 1)
      {
        cout << "This cut was on the tip of the graph. Adding the clique of the cut I/F as a seperate graph\r\n" ;
        remainingGraphs.push_back(singletonCutGraph);
      }
    cout << "Finding the component that has the adjacent clique to the cut. Comparing with graph: " ;
    int adjacentCliqueGraphIndex = 0;
    bool found = false;
    for(int i = 0 ; i < remainingGraphs.size() ; i ++) 
      {
        cout << i << " whose nodes are ";
        Planning::vertexIndexMap cliqueIndices = get(boost::vertex_index, remainingGraphs[i]);
        Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
        for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(remainingGraphs[i]); ugraphVerticesIter != ugraphVerticesIterEnd; ++ugraphVerticesIter)
          {
            cout << cliqueIndices[*ugraphVerticesIter] << " ";
            int cliqueIndex = cliqueIndices[*ugraphVerticesIter];
            if(adjacentCliqueIndex == cliqueIndex)
              {
                cout << "\r\nThe graph having the adjacent edge is " << i <<"\r\n";
                found = true;
                adjacentCliqueGraphIndex = i;
                break;
              }        
          }
        cout << endl;
      }
    if(!found)
      {
        cout << "Problem in getting processing remaining graphs after the cut, exiting..." << endl;
        exit(1);
      }
    return adjacentCliqueGraphIndex;
  }

void removeEdge(  Planning::UGraph &ugraphWithoutEdge,
                  Planning::UGraph ugraph,
                  int src,
                  int tgt)
{
  Planning::UGraph::vertex_descriptor vtx1, vtx2;
  Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
  Planning::vertexIndexMap cliqueIndices = get(boost::vertex_index, ugraph);
  bool found1 = false;
  bool found2 = false;
  for( boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph) ; ugraphVerticesIter != ugraphVerticesIterEnd; ++ugraphVerticesIter)
      {
        if(cliqueIndices[*ugraphVerticesIter] == tgt)
          {
            vtx1 = *ugraphVerticesIter;
            found1 = true;
          }
        else if(cliqueIndices[*ugraphVerticesIter] == src)
	  {
            vtx2 = *ugraphVerticesIter;
            found2 = true;
          }
      }
  if(!(found1 && found2))
    {
      cout << "Problem removing edge, exiting\r\n";
      exit(1);
    }
  while(edge(vtx1, vtx2, ugraph).second)
    {
      remove_edge(edge(vtx1, vtx2, ugraph).first, ugraph);
      cout << "Removing edge from " << src << " to " << tgt << endl;
    }
  while(edge(vtx2, vtx1, ugraph).second)
    {
      remove_edge(edge(vtx2, vtx1, ugraph).first, ugraph);
      cout << "Removing edge from " << tgt << " to " << src << endl;     
    }
  if(edge(vtx2, vtx1, ugraph).second || edge(vtx1, vtx2, ugraph).second)
    {
      cout << "The edge removal failed, exiting .....\r\n";
      exit(1);
    }
  ugraphWithoutEdge.clear();
  ugraphWithoutEdge = ugraph;
}

void filterGraph(Planning::UGraph& ugraph, std::vector< Planning::Clique> setOfCliques)
  {
    cout << "Size before filtering " << num_vertices(ugraph) << endl;
    Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
    Planning::vertexIndexMap cliqueIndices = get(boost::vertex_index, ugraph);
    bool found = false;
    boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph);
    while(ugraphVerticesIter != ugraphVerticesIterEnd)
      {
        for(int i = 0 ; i < setOfCliques.size() ; i++)
          {
            for(int j = 0 ; j < setOfCliques[i].size(); j++)
              {
                if(setOfCliques[i][j] == cliqueIndices[*ugraphVerticesIter]) 
                  {
                    cout << "Found node " << cliqueIndices[*ugraphVerticesIter] << endl;
                    found = true;
                    break;
                  }
              }
          }
        if(!found)
          {
            cout << "Removing node " << cliqueIndices[*ugraphVerticesIter] << endl;            
            boost::clear_vertex(*ugraphVerticesIter, ugraph);
            boost::remove_vertex(*ugraphVerticesIter, ugraph);
            cliqueIndices = get(boost::vertex_index, ugraph);   
            boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph);
          }
        found = false;
        ugraphVerticesIter++;
      }
    cout << "Node induces after filtering: ";
    for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph); (ugraphVerticesIter != ugraphVerticesIterEnd) ; ++ugraphVerticesIter)
      {
        cout << cliqueIndices[*ugraphVerticesIter] << " ";
      }
    cout << endl;
    cout << "Size after filtering " << num_vertices(ugraph) << endl;
  }

void customGetVertexIndex(Planning::UGraph  &  ugraph, Planning::UGraph::vertex_descriptor vtx, int& index)
  { 
    Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
    Planning::vertexIndexMap cliqueIndices = get(boost::vertex_index, ugraph);
    //cout << "Getting node index \r\n ";
    //cout << "Curent node: ";
    bool found = false;
    for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph); (ugraphVerticesIter != ugraphVerticesIterEnd) ; ++ugraphVerticesIter)
      {
        //cout << cliqueIndices[*ugraphVerticesIter] << " ";
        if(*ugraphVerticesIter == vtx)
          {
            found = true;
            index = cliqueIndices[*ugraphVerticesIter];
          }
      }
    if(!found)
      {
        cout << "Vertex not found....";
        index = -1;
      }
    cout << endl;
  }

int customGetVertexDescriptor(Planning::UGraph & ugraph, int index, Planning::UGraph::vertex_descriptor& vtx)
  { 
    Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
    Planning::vertexIndexMap cliqueIndices = get(boost::vertex_index, ugraph);
    //cout << "Getting node " << index << " descriptor \r\n ";
    //cout << "Curent node: ";
    bool found = false;
    for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph); (ugraphVerticesIter != ugraphVerticesIterEnd) ; ++ugraphVerticesIter)
      {
        //cout << cliqueIndices[*ugraphVerticesIter] << " ";
        if(cliqueIndices[*ugraphVerticesIter] == index)
          {
            found = true;
            vtx = *ugraphVerticesIter;
            return 0;
          }
      }
    if(!found)
      {
        cout << "Vertex not found....";
        return -1;
      }    
    cout << endl;
  }

void customCopyGraph(Planning::UGraph src, Planning::UGraph& dest)
  { 
    Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
    // Planning::vertexIndexMap cliqueIndices = get(boost::vertex_index, src);
    dest.clear();
    cout << "Copying nodes: ";
    int currentVertexIndex = -1;

    for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(src); (ugraphVerticesIter != ugraphVerticesIterEnd) ; ++ugraphVerticesIter)
      {
	customGetVertexIndex(src, *ugraphVerticesIter, currentVertexIndex);
        cout << currentVertexIndex << " ";
        dest.add_vertex(currentVertexIndex);
      }
    cout << endl;
    Planning::vertexIndexMap cliqueIndices = get(boost::vertex_index, dest);
    cout << "Copied nodes: ";
    for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(dest); (ugraphVerticesIter != ugraphVerticesIterEnd) ; ++ugraphVerticesIter)
      {
        int idx;
        customGetVertexIndex(dest, *ugraphVerticesIter, idx);
        cout << idx << " ";
      }
    cout << endl;
    cout << "Copying edges: ";
    Planning::UGraph::in_edge_iterator ugraphEdIter, ugraphEdIterEnd;
    Planning::UGraph::vertex_descriptor vtx1, vtx2;
    int idx1, idx2;
    for(boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(src); (ugraphVerticesIter != ugraphVerticesIterEnd) ; ++ugraphVerticesIter)
      {
        customGetVertexIndex(src, *ugraphVerticesIter, idx1);
        customGetVertexDescriptor(dest, idx1, vtx1);
        for(boost::tie(ugraphEdIter, ugraphEdIterEnd) = in_edges( *ugraphVerticesIter, src); ugraphEdIter != ugraphEdIterEnd ; ++ugraphEdIter)
          {
            customGetVertexIndex(src, source(*ugraphEdIter, src), idx2);
            if(customGetVertexDescriptor(dest, idx2, vtx2) == -1)
              {
                cout << "Problem finding vertex, exiting....\r\n";
                exit(1);
              }
            if(!(edge(vtx1, vtx2, dest).second || edge(vtx2, vtx1, dest).second))
              {
                cout << "From " << idx1 << " to " << idx2 << endl;
                add_edge(vtx1, vtx2, dest);
              }
          }
      }
    cout << endl;

  }

void filterCliqueSet(Planning::UGraph ugraph, std::vector< Planning::Clique>& setOfCliques)
  {
    Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
    Planning::vertexIndexMap cliqueIndices = get(boost::vertex_index, ugraph);
    bool found = false;
    Planning::Clique emptyClique;
    for(int i = 0 ; i < setOfCliques.size() ; i++)
      {
        for( boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph); ugraphVerticesIter != ugraphVerticesIterEnd; ++ugraphVerticesIter)
          {
	    if(i == cliqueIndices[*ugraphVerticesIter])
               found = true;
          }  
        if(!found)
          setOfCliques[i] = emptyClique;
        found = false;
      }  
  }




std::map <int,Planning::Clique > computeSASVarSets(Planning::UGraph& ugraph, std::vector< Planning::Clique> &setOfCliques, Planning::Assignments sasAss)
  {
    setOfCliques.clear();
    Planning::UGraph::vertex_iterator ugraphVerticesIter, ugraphVerticesIterEnd;
    Planning::vertexIndexMap varIndices = get(boost::vertex_index, ugraph);
    int varIndex = -1;
    std::map <int,Planning::Clique > sasVarMap;
    Planning::Clique tempClique;
    int k = 0 ;
    for( boost::tie(ugraphVerticesIter, ugraphVerticesIterEnd) = vertices(ugraph) ; ugraphVerticesIter != ugraphVerticesIterEnd; ++ugraphVerticesIter)
      {
        cout << "Adding bool var " << k << " to its SAS+ variable" << endl;
        varIndex = varIndices[*ugraphVerticesIter];
        bool notFound = false;
        notFound = false;
        tempClique.clear();
	tempClique.push_back(varIndex);
        sasVarMap[sasAss[varIndex].first].push_back(varIndex);
        k++;
      }
    cout << "The problem has  " << sasVarMap.size() << " SAS+ variables." << endl;
    k = 0 ;
    for (std::map<int,Planning::Clique>::iterator it=sasVarMap.begin(); it!=sasVarMap.end(); ++it)
      {
        if((it -> second).size() > 1)
          {
            setOfCliques.push_back(it -> second);
            cout << "SAS+ variable " << k << " has " << it -> second.size() << " variables." << endl;
          }
        else
          {
            cout << "SAS+ variable " << k << " has " << 1 << " variables, it will be discarded." << endl;
          }
        k ++;
      }
    return sasVarMap;
  }




void operator<<(std::ostream& f, CustomUndirectedGraph ug)
  {
    cout << "The graph has " << ug.getVertices().size() << " vertices, which are: ";
    int k = 0 ;
    std::set<int> vertices = ug.getVertices();
    for(std::set<int>::iterator i = vertices.begin() ; i != vertices.end() ; i++)
      {
        cout << *i << " ";
        k++;
      }
    cout << "Number of printed vertrices " << k << endl;
    cout << "The graph's edges: ";
    std::set<std::pair<int,int> > edges = ug.getEdges();
    for(std::set< std::pair<int,int> >::iterator i = edges.begin() ; i != edges.end() ; i++)
      {
        cout << *i;
      }
  }

void createCustomGraphLayers(std::vector < Planning::UGraph>  graphLayers, std::vector < CustomUndirectedGraph>&  customGraphLayers)
  {
    int k = 0;
    cout<< "Adding "<< graphLayers.size() << " graph layers" << endl;
    CustomUndirectedGraph tempCustomGraph(graphLayers[0]);
    for( std::vector < Planning::UGraph>::iterator i = graphLayers.begin() ; i != graphLayers.end() ; i++ )
      {
        cout<< "Adding graph layer " << k++ << endl;
        tempCustomGraph = *i;
        cout << tempCustomGraph;
        customGraphLayers.push_back(tempCustomGraph);
      }
  }


void filterCustomGraph(CustomUndirectedGraph& ugraph, std::vector< Planning::Clique> setOfCliques)
  {
    cout << "Size before filtering " << ugraph.getVertices().size() << endl;
    std::set<int>::iterator ugraphVerticesIter;
    bool found = false;
    std::set<int> graphVertices = ugraph.getVertices() ;
    ugraphVerticesIter = graphVertices.begin();
    while(ugraphVerticesIter != graphVertices.end())
      {
        for(int i = 0 ; i < setOfCliques.size() ; i++)
          {
            for(int j = 0 ; j < setOfCliques[i].size(); j++)
              {
                if(setOfCliques[i][j] == *ugraphVerticesIter) 
                  {
                    cout << "Found node " << *ugraphVerticesIter << endl;
                    found = true;
                    break;
                  }
              }
          }
        if(!found)
          {
            cout << "Removing node " << *ugraphVerticesIter << endl;          
            ugraph.removeVertex(*ugraphVerticesIter);
            graphVertices = ugraph.getVertices() ;
            ugraphVerticesIter = graphVertices.begin();
          }
        else
          ugraphVerticesIter++;
        found = false;

      }
    cout << "Node indices after filtering: ";
    graphVertices = ugraph.getVertices() ;
    for(ugraphVerticesIter = graphVertices.begin(); (ugraphVerticesIter != graphVertices.end()) ; ++ugraphVerticesIter)
      {
        cout << *ugraphVerticesIter << " ";
      }
    cout << endl;
    cout << "Size after filtering " << ugraph.getVertices().size() << endl;
  }

void filterCliqueSetCustom(CustomUndirectedGraph ugraph, std::vector< Planning::Clique>& setOfCliques)
  {
    std::set<int>::iterator ugraphVerticesIter;
    bool found = false;
    Planning::Clique emptyClique;
    for(int i = 0 ; i < setOfCliques.size() ; i++)
      {
        std::set<int> graphVertices = ugraph.getVertices() ;
        for( ugraphVerticesIter = graphVertices.begin(); ugraphVerticesIter != graphVertices.end(); ++ugraphVerticesIter)
          {
	    if(i == *ugraphVerticesIter)
              {
                found = true;
                break;
              }
          }  
        if(!found)
          setOfCliques[i] = emptyClique;
        found = false;
      }  
  }


void getCliqueInterfacesCustom(setOfCliqueSet& interfaces,
                         cliqueSet& adjacentCliques,
                         int cliqueIndex, 
                         CustomUndirectedGraph cliqueLevelUGraph,
                         CustomUndirectedGraph subCliqueLevelUGraph, 
                         std::vector< Planning::Clique> setOfCliques )
  {
    cout << "Getting clique " << cliqueIndex << " interfaces. Its members are ";
    for ( int i ; i < setOfCliques[cliqueIndex].size() ; i++)
      {
        cout << setOfCliques[cliqueIndex][i] << " ";
      } 
    cout << endl;    
    interfaces.clear();
    adjacentCliques.clear();
    bool connectedClique = false;
    //finding adjacent nodes
    std::set< std::pair<int,int> > vtxEdges = cliqueLevelUGraph.getVertexEdges(cliqueIndex);
    cout << "Number of edges connected to this clique " << vtxEdges.size() << endl; 
    std::set<std::pair<int,int> >::iterator ugraphEdIter = vtxEdges.begin();
    for(ugraphEdIter = vtxEdges.begin() ; ugraphEdIter != vtxEdges.end() ; ++ugraphEdIter)
      {
        cliqueSet currentInterface;
        currentInterface.clear();
	std::set<int> currentInterfaceSet;
        currentInterfaceSet.clear();
        int adjacentCliqueIdx = getOtherVtx(cliqueIndex, *ugraphEdIter);
        cout << "Finding I/F to clique " << adjacentCliqueIdx << " whose size is " << setOfCliques[adjacentCliqueIdx].size() << " and whose members are ";
        for ( int i = 0; i < setOfCliques[adjacentCliqueIdx].size() ; i++)
          {
            cout << setOfCliques[adjacentCliqueIdx][i] << " ";
          } 
        cout << endl;        
	bool found = false;
        //Finding the interface to the current adjacent node        
        for(int i = 0 ; i < setOfCliques[cliqueIndex].size() ; i++)
          {
            //Checking whether the current member of the clique is connected to a node in the adjacent clique, i.e. it is a member in the interface
            std::set<std::pair<int,int> >::iterator ifEdIter;
            std::set< std::pair<int,int> > vtxEdgesLowGraph = subCliqueLevelUGraph.getVertexEdges( setOfCliques[cliqueIndex][i]);
            for(ifEdIter  = vtxEdgesLowGraph.begin() ; ifEdIter != vtxEdgesLowGraph.end() ; ++ifEdIter)
              {
                int subCliqueAdjacentNodeIdx = getOtherVtx(setOfCliques[cliqueIndex][i], *ifEdIter);
                for(int j = 0 ; j < setOfCliques[adjacentCliqueIdx].size() ; j++)
                  {
                    if(setOfCliques[adjacentCliqueIdx][j] == subCliqueAdjacentNodeIdx)
                      {
                        if(currentInterfaceSet.find(setOfCliques[cliqueIndex][i]) == currentInterfaceSet.end())
                          {
                            cout << "Node " << setOfCliques[cliqueIndex][i] << " is added to the interface between cliques " << cliqueIndex << " and " << adjacentCliqueIdx <<endl;
                            currentInterfaceSet.insert(setOfCliques[cliqueIndex][i]);
                            currentInterface.push_back(setOfCliques[cliqueIndex][i]);
                            connectedClique = true;
                          }
                        break;
                      }
                  }
              }
          }
        //if the interface is total, this is not a cut
        if(currentInterface.size() < setOfCliques[cliqueIndex].size() && connectedClique)
          { 
          
            interfaces.push_back(currentInterface);
            adjacentCliques.push_back(adjacentCliqueIdx);
          }
        else if(!connectedClique)
          {
            cout << "Clique " << cliqueIndex << " and " << adjacentCliqueIdx << " are not connected, no I/F between them." << endl;
          }
        else
          {
            cout << "Clique " << cliqueIndex << " and " << adjacentCliqueIdx << " are fully connected, no I/F between them." << endl;
          }
        connectedClique = false;
        currentInterface.clear();
      }
  }

void B_F_S_Custom(cliqueSet& cliqueComponent, int vtx, CustomUndirectedGraph cliqueUGraph)
  {
    std::set< std::pair<int,int> >::iterator ugraphEdIter;
    std::queue<int> q;
    q.push(vtx);
    cliqueComponent.push_back(vtx);
    bool visited = false;
    int currentVtx;
    //cout << "Starting BFS from node " << vtx << endl;
    while(!q.empty() )
      {
        //cout << "XXXXXXX\r\n";
        currentVtx = q.front();
        q.pop();
        std::set< std::pair<int,int> > vtxEdges = cliqueUGraph.getVertexEdges(currentVtx);
        for(ugraphEdIter = vtxEdges.begin() ; ugraphEdIter != vtxEdges.end() ; ++ugraphEdIter)
          {
            //cout << "Current edge is " << currentVtx << " to " << getOtherVtx(currentVtx, *ugraphEdIter) << endl;
            for(int i = 0 ; i < cliqueComponent.size() ; i++)
              {
                if(cliqueComponent[i] == getOtherVtx(currentVtx, *ugraphEdIter))
                  {
                    visited = true;
                  }
              }
            if(!visited)
              {
                //cout << "Adding node " << getOtherVtx(currentVtx, *ugraphEdIter) << endl;
                cliqueComponent.push_back(getOtherVtx(currentVtx, *ugraphEdIter));
                q.push(getOtherVtx(currentVtx, *ugraphEdIter));
              }
            visited = false;
          }
      } 
  }

void getConnectedCliquesCustom(setOfCliqueSet& setOfCliqueComponents, CustomUndirectedGraph cliqueUGraph)
  {
    setOfCliqueComponents.clear();
    using namespace boost;
    std::set<int>::iterator ugraphVerticesIter;
    //REPLACE ALL getVertices like below!!!!!!!!!!!
    std::set<int> graphVertices = cliqueUGraph.getVertices();
    ugraphVerticesIter = graphVertices.begin();
    while (cliqueUGraph.getVertices().size() != 0)
      {
        cliqueSet cliqueComponent;
        graphVertices = cliqueUGraph.getVertices();
        ugraphVerticesIter = graphVertices.begin();
	bfs_time_visitor vis(cliqueComponent);
        vis.cliquesComponent.clear();
        cout << "Clique index " << *(ugraphVerticesIter) <<endl;
        cout << "Current clique graph size " << cliqueUGraph.getVertices().size() <<endl;;
        // boost::copy_graph(cliqueUGraph, graph);  
	B_F_S_Custom(vis.cliquesComponent, *(ugraphVerticesIter),  cliqueUGraph);
        //breadth_first_search(cliqueUGraph, *(ugraphVerticesIter), visitor(vis));
        //cout << "A components is: ";
        //for(int i = 0 ; i < vis.cliquesComponent.size() ; i++)
          //cout <<  vis.cliquesComponent[i] << " ";
	  //cout << endl;
        cout << "Added nodes ";
        for(int i = 0 ; i < vis.cliquesComponent.size() ; i++)
          {
            cout << vis.cliquesComponent[i] << " ";
            cliqueUGraph.removeVertex(vis.cliquesComponent[i]);
          }
        cout << " to component " << setOfCliqueComponents.size() << endl;
        setOfCliqueComponents.push_back(vis.cliquesComponent);
      }
    // std::ofstream dotfile ("dbgGraph.dot");
    // boost::write_graphviz (dotfile, cliqueUGraph);//, llw);
    // dotfile.close();
    // boost::copy_graph(cliqueUGraph, graph);  
  }


void getRemainingGraphsCustom(std::vector<CustomUndirectedGraph>& subGraphs,
                              setOfCliqueSet setOfCliqueComponents,
                              CustomUndirectedGraph cliqueUGraph)
  {
    subGraphs.clear();
    CustomUndirectedGraph ugraph_temp(cliqueUGraph);
    Planning::vertexIndexMap index;   
    for(int i = 0; i < setOfCliqueComponents.size() ; i++)
      {
        cout << "The current clique component is:";
        for(int j = 0 ; j < setOfCliqueComponents[i].size() ; j++)
          {
	    cout << setOfCliqueComponents[i][j] << " ";
          }
	cout << endl;
	ugraph_temp = cliqueUGraph;
	std::set<int>::iterator ugraphVerticesIter;
        std::set<int> graphVertices = ugraph_temp.getVertices();
        ugraphVerticesIter = graphVertices.begin();
        while(ugraphVerticesIter != graphVertices.end())
          {  
            bool found = false;
            found = false;
            for(int j = 0 ; j < setOfCliqueComponents[i].size() ; j++)
              {       
                if(*ugraphVerticesIter == setOfCliqueComponents[i][j])
                  {
             	    //cout << setOfCliqueComponents[i][j] << " ";
                    found = true; 
                  }
              }
            if(!found)
              {  
                ugraph_temp.removeVertex(*ugraphVerticesIter);                   
                graphVertices = ugraph_temp.getVertices();
                ugraphVerticesIter = graphVertices.begin();
	      }
            else
              {
                ugraphVerticesIter++;
              }
          }
        subGraphs.push_back(ugraph_temp);
	cout << "Subgraph " << i << " size is " << subGraphs[i].getVertices().size() ; 
        cout << endl;
      }
    // std::ofstream dotfile ("dbgGraph.dot");
    // boost::write_graphviz (dotfile, subGraphs[0]);//, llw);
    // dotfile.close();
    // std::ofstream dotfile1 ("dbgGraph1.dot");
    // boost::write_graphviz (dotfile1, subGraphs[1]);//, llw);
    // dotfile1.close();
    // std::ofstream dotfile2 ("dbgGraph2.dot");
    // boost::write_graphviz (dotfile2, subGraphs[2]);//, llw);
    // dotfile2.close();
    // std::ofstream dotfile3 ("dbgGraph3.dot");
    // boost::write_graphviz (dotfile3, subGraphs[3]);//, llw);
    // dotfile3.close();
  }


int postProcessRemainingGraphsCustom(std::vector<CustomUndirectedGraph>& remainingGraphs,
                                CustomUndirectedGraph singletonCutGraph,
                                int adjacentCliqueIndex)
  {
    //For each of the remaining graphs: remove the non-interface nodes from the cut clique content  
    int cutVertixIndex = *(singletonCutGraph.getVertices().begin());
    cout << "Finding the component that has the adjacent clique to the cut to add the cut clique to it. Comparing with graph: " ;
    int adjacentCliqueGraphIndex = -1;
    bool found = false;
    for(int i = 0 ; i < remainingGraphs.size() ; i ++) 
      {
        cout << i << " whose nodes are ";
	std::set<int>::iterator ugraphVerticesIter;
        std::set<int> graphVertices = remainingGraphs[i].getVertices();
        ugraphVerticesIter = graphVertices.begin();
        for(ugraphVerticesIter = graphVertices.begin(); ugraphVerticesIter != graphVertices.end(); ++ugraphVerticesIter)
          {
            cout << *ugraphVerticesIter << " ";
            int cliqueIndex = *ugraphVerticesIter;
            if(adjacentCliqueIndex == cliqueIndex)
              {
                cout << "The graph having the adjacent edge is " << i <<"\r\n";
                found = true;
                adjacentCliqueGraphIndex = i;
                cout << "Adding the cut clique to the component having the adjacent node \r\n";
                remainingGraphs[i].addVtx(cutVertixIndex);
                remainingGraphs[i].addEdge(cutVertixIndex, cliqueIndex);
                break;
              }        
          }
        if(found)
          break;
        cout << endl;
      }
    if(!found)
      {
        cout << "Problem in getting processing remaining graphs after the cut, exiting..." << endl;
        exit(1);
      }
    return adjacentCliqueGraphIndex;
  }

int vectorFind(int obj, vector<int> vec)
{
  cout << "Trying to find " << obj << endl;
  int index = -1;
  for(int i = 0 ; i < vec.size() ; i++)
    {
      if(vec[i] == obj)
        {
          cout << "Required object found at index " << i << endl;
          index = i;
          return index;
        }
    }
  return index;
}



void vectorCopyElements(vector<int> src, vector<int>& dest)
{
  for(int i = 0 ; i < src.size() ; i++)
    {
      if(vectorFind(src[i], dest) == -1)
        dest.push_back(src[i]);
    }
}

void getCliqueInterfacesBFS(   setOfCliqueSet& interfaces,
                               setOfCliqueSet& adjacentCliqueSets,
                               int cliqueIndex, 
                               CustomUndirectedGraph cliqueLevelUGraph,
                               CustomUndirectedGraph subCliqueLevelUGraph, 
                               std::vector< Planning::Clique> setOfCliques )
  {
    cout << "Getting clique " << cliqueIndex << " interfaces. Its members are ";
    for ( int i ; i < setOfCliques[cliqueIndex].size() ; i++)
      {
        cout << setOfCliques[cliqueIndex][i] << " ";
      } 
    cout << endl;    
    interfaces.clear();
    adjacentCliqueSets.clear();
    bool connectedClique = false;
    //finding adjacent nodes
    std::set< std::pair<int,int> > vtxEdges = cliqueLevelUGraph.getVertexEdges(cliqueIndex);
    cout << "Number of edges connected to this clique " << vtxEdges.size() << endl; 

    //Getting the components such that the interface of each component is a cut
    cliqueSet emptyIF;
    CustomUndirectedGraph cliqueLevelUGraphTemp = cliqueLevelUGraph;
    cliqueLevelUGraphTemp.removeVertex(cliqueIndex);
    getConnectedCliquesCustom(adjacentCliqueSets, cliqueLevelUGraphTemp);
    cout << "Found " << adjacentCliqueSets.size() << " componentns \r\n";
    for(int i = 0 ; i < adjacentCliqueSets.size(); i++)
      {
                interfaces.push_back(emptyIF);
      }
    std::set<std::pair<int,int> >::iterator ugraphEdIter = vtxEdges.begin();
    for(ugraphEdIter = vtxEdges.begin() ; ugraphEdIter != vtxEdges.end() ; ++ugraphEdIter)
      {
        cliqueSet currentInterface;
        currentInterface.clear();
	std::set<int> currentInterfaceSet;
        currentInterfaceSet.clear();
        int adjacentCliqueIdx = getOtherVtx(cliqueIndex, *ugraphEdIter);
        cout << "Finding I/F to neighbour clique set " << adjacentCliqueIdx << " whose size is " << setOfCliques[adjacentCliqueIdx].size() << " and whose members are ";
        for ( int i = 0; i < setOfCliques[adjacentCliqueIdx].size() ; i++)
          {
            cout << setOfCliques[adjacentCliqueIdx][i] << " ";
          } 
        cout << endl;        
        bool found = false;      
        //Finding the interface to which the current edge belongs
        for(int i = 0 ; i < setOfCliques[cliqueIndex].size() ; i++)
          {
            //Checking whether the current member of the clique is connected to a node in the adjacent clique, i.e. it is a member in the interface
            std::set<std::pair<int,int> >::iterator ifEdIter;
            std::set< std::pair<int,int> > vtxEdgesLowGraph = subCliqueLevelUGraph.getVertexEdges( setOfCliques[cliqueIndex][i]);
            for(ifEdIter  = vtxEdgesLowGraph.begin() ; ifEdIter != vtxEdgesLowGraph.end() ; ++ifEdIter)
              {
                int subCliqueAdjacentNodeIdx = getOtherVtx(setOfCliques[cliqueIndex][i], *ifEdIter);
                for(int j = 0 ; j < setOfCliques[adjacentCliqueIdx].size() ; j++)
                  {
                    if(setOfCliques[adjacentCliqueIdx][j] == subCliqueAdjacentNodeIdx)
                      {
                        if(currentInterfaceSet.find(setOfCliques[cliqueIndex][i]) == currentInterfaceSet.end())
                          {
                            cout << "Node " << setOfCliques[cliqueIndex][i] << " is added to the interface between cliques " << cliqueIndex << " and " << adjacentCliqueIdx <<endl;
                            currentInterfaceSet.insert(setOfCliques[cliqueIndex][i]);
                            currentInterface.push_back(setOfCliques[cliqueIndex][i]);
                            connectedClique = true;
                          }
                        break;
                      }
                  }
              }
          }
        //if the interface is total, this is not a cut
        if(currentInterface.size() < setOfCliques[cliqueIndex].size() && connectedClique)
          { 
            bool found = false;
            //Getting the neighbouring component to which the adjacent clique belongs
            int adjacenComponentIdx = 0;
            for(; adjacenComponentIdx < adjacentCliqueSets.size() ; adjacenComponentIdx++)
              {
                if(vectorFind(adjacentCliqueIdx, adjacentCliqueSets[adjacenComponentIdx]) != -1)
                  {
                    cout << "Found the component to which this interface belongs (subgraph " << adjacenComponentIdx << ")\r\n";
                    found = true;
                    break;
                  }
              }
            if(found)
              vectorCopyElements(currentInterface, interfaces[adjacenComponentIdx]);
            else 
	      {
	        cout << "Problem finding the component for this interface, exiting!!!!";
	        exit(1);
	      }
          }
        else if(!connectedClique)
          {
            cout << "Clique " << cliqueIndex << " and " << adjacentCliqueIdx << " are not connected in the lower level, no I/F between them. Exiting!!!!" << endl;
            exit(1);
          }
        else
          {
            cout << "Clique " << cliqueIndex << " and " << adjacentCliqueIdx << " are fully connected, no I/F between them." << endl;
          }
        connectedClique = false;
        currentInterface.clear();
      }
    int adjacenComponentIdx = 0 ;
    setOfCliqueSet::iterator adjacentCliqueSetsIt = adjacentCliqueSets.begin();
    setOfCliqueSet::iterator interfaceIt = interfaces.begin();
    while(1)
      {        
        if(interfaceIt->size() == setOfCliques[cliqueIndex].size() || interfaceIt->size() == 0)
          {
            cout << "Clique " << cliqueIndex << " and neighbouring component " << adjacenComponentIdx << " are fully connected, or there is no I/F between them." << endl;
            interfaces.erase(interfaceIt);
            adjacentCliqueSets.erase(adjacentCliqueSetsIt);
            adjacentCliqueSetsIt = adjacentCliqueSets.begin();
            interfaceIt = interfaces.begin();
          }
	if(adjacentCliqueSetsIt == adjacentCliqueSets.end())
          break;
        interfaceIt++;
        adjacentCliqueSetsIt++;
        adjacenComponentIdx++;
      }
  }


std::map<int,int> graphFileNo;
char* graphFilename = "debuging/Graph";
long long unsigned int 
   computeBoundFromCliqueOfCliquesCustom
                    ( std::vector < CustomUndirectedGraph> graphLayers,
                      std::vector < std::vector< Planning::Clique> > setOfCliquesfCliques,
                      Planning::Assignments sasAss,
                      std::map <int,Planning::Clique > sasVarMap)
  {
    level ++;
    char graphFileNameNum [100];
    strcpy( graphFileNameNum, graphFilename);
    char graphFileNum[10];
    // sprintf(graphFileNum, ".%d", level);
    sprintf(graphFileNum, ".%d", graphLayers.size());
    strcat(graphFileNameNum, graphFileNum);
    //sprintf(graphFileNum, ".%d",graphFileNo[level]);
    sprintf(graphFileNum, ".%d",graphFileNo[graphLayers.size()]);
    strcat(graphFileNameNum, graphFileNum);
    cout << "Graph layer file name is " << graphFileNameNum << endl;
    std::ofstream dotfile (graphFileNameNum);
    dotfile << graphLayers[graphLayers.size() - 1];
    dotfile.close();
    graphFileNo[graphLayers.size()]++;
    if(level == 2)
      {
        cout << "here\r\n";
      } 
    cout << "Call Level = " << level << endl;
    cout << "Clique graph level" << graphLayers.size() << endl;
    cout << "Size of this level's graph " << graphLayers[graphLayers.size() - 1].getVertices().size() << endl;
    cout << graphLayers[graphLayers.size() - 1];
    long long unsigned int bestBound = 0;
    int bestCutNode = -1;
    int bestCutIndex = -1;
    bool bestboundChanged = false;
    bool cutFound = false;
    int numberOfInterfacesToConsider = 1;
    std::set<int>::iterator ugraphVerticesIter, ugraphVerticesIterEnd;
    std::set<int> graphVertices = graphLayers[graphLayers.size() - 1].getVertices();
    ugraphVerticesIter = graphVertices.begin();
    ugraphVerticesIterEnd = graphVertices.end();
    int cliqueIndex = *ugraphVerticesIter;
    if(graphLayers.size() == 1 )
      {
        cout << "This is a base graph\r\n";        
        if(graphLayers[graphLayers.size() - 1].getEdges().size() < (graphVertices.size()*(graphVertices.size() - 1))/2)
          cout << "This base graph is a digested diamond of size "<< graphVertices.size() << "\r\nIt will be recliqued again";
        //     std::vector< Planning::Clique> setOfCliques;
        //     computeCliquesWithSize(graphLayers[0], setOfCliques, 3);
        //     CustomUndirectedGraph cliqueUGraph;
        //     computeCliquesUGraph(graphLayers[0], setOfCliques, cliqueUGraph);    
        //     graphLayers.push_back(cliqueUGraph);
        //     setOfCliquesfCliques.push_back(setOfCliques);
        //     bestBound = computeBoundFromCliqueOfCliquesCustom  
        //                                ( graphLayers,
        //                                  setOfCliquesfCliques,
        //                                  assignments,
        //                                  sasVarMap);
        //   }
        // else
	//   {
            bestBound = getCliqueBound(sasAss, graphVertices, sasVarMap);
          // }
        bestboundChanged = true;
        cout << "The clique bound is " << bestBound << endl;
        level --;
        cout << "Return Level = " << level << endl;
        return (bestBound - 1); 
      }
    else if( graphLayers[graphLayers.size() - 1].getVertices().size() == 1 )
      {
        cout << "This is a single clique graph\r\n";
        cout << "This is a high level graph\r\n";
        cout << "Size of lower level's graph before filtering " << graphLayers[graphLayers.size() - 2].getVertices().size() << endl;
        filterCustomGraph( graphLayers[graphLayers.size() - 2], setOfCliquesfCliques[setOfCliquesfCliques.size() - 1 ]);
        cout << "Size of lower level graph after filtering " << graphLayers[graphLayers.size() - 2].getVertices().size() << endl;
        if(graphLayers.size() > 2 )
          filterCliqueSetCustom(graphLayers[graphLayers.size() - 2], setOfCliquesfCliques[setOfCliquesfCliques.size() - 2 ]);
        graphLayers.pop_back();
        setOfCliquesfCliques.pop_back();
        bestBound = computeBoundFromCliqueOfCliquesCustom( graphLayers, setOfCliquesfCliques, sasAss, sasVarMap);
        bestboundChanged = true;
        cout << "The Graph bound is " << bestBound << endl;
        level --;
        cout << "Return Level = " << level << endl;
        return bestBound; 
      }
    else if(graphLayers.size() > 1)
      {
        cout << "Size of lower level graph before filtering " << graphLayers[graphLayers.size() - 2].getVertices().size() << endl;
        filterCustomGraph( graphLayers[graphLayers.size() - 2], setOfCliquesfCliques[setOfCliquesfCliques.size() - 1 ]);
        cout << "Size of lower level graph after filtering " << graphLayers[graphLayers.size() - 2].getVertices().size() << endl;
        for( ugraphVerticesIter = graphVertices.begin() ; ugraphVerticesIter != graphVertices.end() ; ++ugraphVerticesIter )
          {
            //get all possible cuts from current node i.e. clique interfaces. A clique interface has to be a subset of the clique
            setOfCliqueSet cuts;
            setOfCliqueSet adjacentCliqueSets;
            cout << "Getting the cut interfaces to " << *ugraphVerticesIter <<"\r\n";
            int currentCliqueIndex = *ugraphVerticesIter;
            getCliqueInterfacesBFS(cuts, adjacentCliqueSets, currentCliqueIndex, graphLayers[graphLayers.size() - 1], graphLayers[graphLayers.size() - 2], setOfCliquesfCliques[setOfCliquesfCliques.size() - 1]);
            if(cuts.size() == 0)
              {
                cout << "There are no cuts available, from this clique (i.e. it is fully connected to all its adjacent cliques)... \r\n";
              }
            else
              {
                cutFound = true;
                cout << "This graph has a boundary cut. Calculating the bound based on that. \r\n";
                int interfaceNum = 0;                    
                //add the cut clique to each of the remaining graphs, BUT remove all of its components that are not in the interface                    
                setOfCliqueSet adjacentCliqueSetsWithCut = adjacentCliqueSets;
                for(int i = 0 ; i < adjacentCliqueSetsWithCut.size() ; i++)
                  adjacentCliqueSetsWithCut[i].push_back(currentCliqueIndex);
                //get remaining graphs
                std::vector<CustomUndirectedGraph> remainingGraphs;
                getRemainingGraphsCustom(remainingGraphs, adjacentCliqueSetsWithCut, graphLayers[graphLayers.size() - 1]);                    
                cout << "After cutting the graph contains " << adjacentCliqueSetsWithCut.size() << " seperate components (without the cut clique on its own)"  << endl;
                cout << "The remaining components are \r\n" ;
                for (int i = 0 ; i < remainingGraphs.size() ; i ++ )
                  {
                    cout << remainingGraphs[i];
                    cout << endl;
                  }
                long long unsigned int currentCutBound = 0;
                std::vector < CustomUndirectedGraph> graphLayersTemp = graphLayers;
                for(int currentGraph = 0 ; currentGraph < remainingGraphs.size() ; currentGraph++)
                  {
                    long long unsigned int childBound = 0;
                    graphLayersTemp.pop_back();
                    graphLayersTemp.push_back(remainingGraphs[currentGraph]);
                    cout << "Calculating the bound of child graph " << currentGraph << " which contains ";
                    cout << remainingGraphs[currentGraph];
                    std::vector < std::vector< Planning::Clique> > setOfCliquesfCliquesTemp = setOfCliquesfCliques;
                    setOfCliquesfCliquesTemp[setOfCliquesfCliquesTemp.size() - 1][currentCliqueIndex] = cuts[currentGraph];
                    filterCliqueSetCustom(remainingGraphs[currentGraph],
                                 setOfCliquesfCliquesTemp[setOfCliquesfCliquesTemp.size() - 1]);
                    childBound = computeBoundFromCliqueOfCliquesCustom 
                                ( graphLayersTemp,
                                  setOfCliquesfCliquesTemp,
                                  sasAss,
                                  sasVarMap);
                    currentCutBound = currentCutBound + childBound;
                    cout << "Current child bound is " << childBound << " and current total bound " << currentCutBound << endl;
                  }
                cout << "Calculating the bound on the cut component on its own\r\n";
                std::vector < std::vector< Planning::Clique> > setOfCliquesfCliquesCompleteCut = setOfCliquesfCliques;
                CustomUndirectedGraph singletonCutGraph;
                singletonCutGraph.addVtx(currentCliqueIndex);
                filterCliqueSetCustom(singletonCutGraph,
                                      setOfCliquesfCliquesCompleteCut[setOfCliquesfCliquesCompleteCut.size() - 1]);
                graphLayersTemp.pop_back();
                graphLayersTemp.push_back(singletonCutGraph);
                long long unsigned int singletonCutBound = 0;
                singletonCutBound = computeBoundFromCliqueOfCliquesCustom
                                    ( graphLayersTemp,
                                      setOfCliquesfCliquesCompleteCut,
                                      sasAss,
                                      sasVarMap);
                currentCutBound = currentCutBound + singletonCutBound;
                cout << "Cut bound is " << singletonCutBound << " and current total bound " << currentCutBound << endl;
                if(!bestboundChanged || bestBound > currentCutBound)
                  {
                    bestBound = currentCutBound; 
                    bestboundChanged = true;
                    bestCutIndex = -2;
                    bestCutNode = *ugraphVerticesIter;
                  }    	  	      
             }
            if(cutFound)
              break;
          }
        // DONT FORGET TO GET ALL THE CONNECTED COMPONENTS WITHOUT REMOVING ANY CUTS AND CONTINUING WITH THE BELOW CODE
	if(!cutFound)
          {
            if(graphLayers[graphLayers.size() - 1].getEdges().size() < (graphVertices.size()*(graphVertices.size() - 1))/2)
              cout << "This is a diamond of size "<< graphVertices.size() << "\r\n";
            ugraphVerticesIter = graphVertices.begin();
            cout << "Constructing a single node graph..\r\n";
            int firstCliqueIndex = *ugraphVerticesIter;
            cout << "Merging all the members in the cliques into one clique and deleting corresponding nodes in this level's graph\r\n";
            cout << "Merging cliques ";
            graphVertices = graphLayers[graphLayers.size() - 1].getVertices();
            cout << "Size of graph before Merging " << graphVertices.size() << endl;
            for(int i = 0 ; i < setOfCliquesfCliques[setOfCliquesfCliques.size() - 1].size() ; i++)
              {
                if(i != firstCliqueIndex)
                  {
                    cout << i << " " << " (with members ";                      
                    for (int j = 0 ; j < setOfCliquesfCliques[setOfCliquesfCliques.size() - 1][i].size() ; j++)
                      {                
                        cout << setOfCliquesfCliques[setOfCliquesfCliques.size() - 1][i][j] << " " ;
                        setOfCliquesfCliques[setOfCliquesfCliques.size() - 1][firstCliqueIndex].push_back(setOfCliquesfCliques[setOfCliquesfCliques.size() - 1][i][j]) ;
                      }
                    cout << ")";
                    if(graphVertices.find(i) != graphVertices.end())
                      {                    
                        graphLayers[graphLayers.size() - 1].removeVertex(i);
                        graphVertices = graphLayers[graphLayers.size() - 1].getVertices();
                      }
                    cout << "Size of the graph after node removal " << graphVertices.size() << endl;
                    //setOfCliquesfCliques[setOfCliquesfCliques.size() - 1].erase(setOfCliquesfCliques[setOfCliquesfCliques.size() - 1][i]) ;
                  }
              }
            cout << " with node " << firstCliqueIndex << endl;
            long long unsigned int noCutBound = computeBoundFromCliqueOfCliquesCustom
                                                      ( graphLayers,
                                                        setOfCliquesfCliques,
                                                        sasAss,
                                                        sasVarMap);
            cout << "The no cut bound is " << noCutBound << endl;
            if(!bestboundChanged || bestBound > noCutBound)
              {
                bestBound = noCutBound;
                bestboundChanged = true;
                bestCutIndex = -1;
              }    
          }
      }
    cout << "The best bound reached is " << bestBound << " from cut " << bestCutIndex << "," << bestCutNode << endl;
    cout << "No cuts at all were found\r\n";
    cout << "TODO: DONT FORGET REMOVING THE 1 EXTRA ACTION!!!! \r\n";
    cout << "DONT FORGET TO GET ALL THE CONNECTED COMPONENTS WITHOUT REMOVING ANY CUTS AND CONTINUING WITH THE BELOW CODE\r\n";
    level--;
    cout << "Return Level = " << level << endl;
    return bestBound;
  }



int Planning::SCCDiscovery::connectedCliques()
  {
    std::size_t n_ver_min = num_vertices(ugraph);
    std::vector< Planning::Clique> setOfCliques2;
    std::map <int,Planning::Clique > sasVarMap = computeSASVarSets(ugraph, setOfCliques2, assignments);
    filterGraph(ugraph, setOfCliques2);

    //Planning::UGraph ugraph_temp;
    //boost::copy_graph(ugraph, ugraph_temp);
    computeCliquesWithSize(ugraph, setOfCliques, 3);

    printf("num_vertices: %d\r\n\r\n\r\n", num_vertices(ugraph));
    printf("num_edges: %d\r\n\r\n\r\n", num_edges(ugraph));

    computeCliquesUGraph(ugraph, setOfCliques, cliqueUGraph);    
    printf("The number of nodes in the clique graph = %d\r\n\r\n\r\n", num_vertices(cliqueUGraph));
    //printf("%d \r\n\r\n",getpid());
    //sleep(10);
    std::vector < Planning::UGraph>  graphLayers;
    std::vector < std::vector< Planning::Clique> > setOfCliquesfCliques;
    graphLayers.push_back(ugraph);
    graphLayers.push_back(cliqueUGraph);
    std::vector< Planning::Clique > emptyMap;
    setOfCliquesfCliques.push_back(emptyMap);
    setOfCliquesfCliques.push_back(setOfCliques);

    getCliqueOfCliques(graphLayers, setOfCliquesfCliques, 3);

    cout << "Number of clique graphs levels is " << graphLayers.size() << " and # maps is "  << setOfCliquesfCliques.size() << endl;
    std::vector < CustomUndirectedGraph>  customGraphLayers;
    // std::ofstream dotfile ("dbgGraph.dot");
    // boost::write_graphviz (dotfile, graphLayers[0]);//, llw);
    // dotfile.close();
    // std::ofstream dotfile2 ("dbgGraph2.dot");
    // boost::write_graphviz (dotfile2, graphLayers[1]);//, llw);
    // dotfile2.close();
    // std::ofstream dotfile3 ("dbgGraph3.dot");
    // boost::write_graphviz (dotfile3, graphLayers[2]);//, llw);
    // dotfile3.close();
    createCustomGraphLayers(graphLayers, customGraphLayers);
    // long long unsigned int bound = computeBound( cliqueUGraph, ugraph, setOfCliques, assignments);
    long long unsigned int bound = computeBoundFromCliqueOfCliquesCustom  
                                       ( customGraphLayers,
                                         setOfCliquesfCliques,
                                         assignments,
                                         sasVarMap);
    cout << "The bound is " <<  bound << endl;
    FILE * boundsFile = fopen ("Bounds", "a");
    fprintf(boundsFile, "%d \r\n", bound); 
    fclose(boundsFile);
    cout << "Only cuts of size " << cut_limit << " were tried!! \r\n" ;
    return setOfCliques.size();
  }


#include "hashsat_counting.hh"
