#!/bin/bash

THRESHOLD=200000

for f in okay_ones/RUN.*; do 
    echo $f; 
    cat $f > runit.sh ; 
    chmod +x runit.sh ;
    NUM=`wc -l runit.sh | awk '{print $1}'`
    for g in $(eval echo {1..$NUM}) ; do
	COMMAND=`head -$g runit.sh | tail -1`;
	echo $COMMAND > runme.sh
	chmod +x runme.sh ;
	#cat runme.sh ; 
	./runme.sh ./run_counts.sh 2> timing  | grep -w 'Maximum' | awk '{print $6}' > answer
	cat timing | grep "Max RSS"
	VAL=`cat answer`
	rm answer
	if [ -z $VAL ] ; then
	    cat runme.sh | awk '{print $2}'
	    echo "No maximum reported. Probably too long."
	else 
	    # if [   $VAL -gt $THRESHOLD ] ; then
	    #  	echo "Maximum parallel plan length is $VAL"
	    #  	echo "$VAL is too long, so quitting."
	    # 	break
	    # else 
		cat runme.sh | awk '{print $2}'
	     	echo "Maximum parallel plan length is $VAL"
	    # fi
	fi
    done
done
