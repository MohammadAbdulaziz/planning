begin_metric
1
end_metric
begin_variables
62
var0 6 -1
var1 6 -1
var2 3 -1
var3 2 -1
var4 2 -1
var5 2 -1
var6 2 -1
var7 2 -1
var8 2 -1
var9 2 -1
var10 2 -1
var11 2 -1
var12 2 -1
var13 2 -1
var14 2 -1
var15 2 -1
var16 2 -1
var17 2 -1
var18 2 -1
var19 2 -1
var20 2 -1
var21 2 -1
var22 2 -1
var23 2 -1
var24 2 -1
var25 2 -1
var26 2 -1
var27 2 -1
var28 2 -1
var29 2 -1
var30 2 -1
var31 2 -1
var32 2 -1
var33 2 -1
var34 2 -1
var35 2 -1
var36 2 -1
var37 2 -1
var38 2 -1
var39 2 -1
var40 2 -1
var41 2 -1
var42 2 -1
var43 2 -1
var44 2 -1
var45 2 -1
var46 2 -1
var47 2 -1
var48 2 -1
var49 2 -1
var50 2 -1
var51 2 -1
var52 2 -1
var53 2 -1
var54 2 -1
var55 2 -1
var56 2 -1
var57 2 -1
var58 2 -1
var59 2 -1
var60 2 -1
var61 2 -1
end_variables
begin_state
5
0
1
1
0
0
0
1
1
0
1
1
0
1
1
0
1
1
0
1
1
1
1
1
1
1
1
1
1
1
1
0
0
0
1
1
1
1
0
1
0
1
1
1
1
0
0
1
1
1
1
1
1
1
1
1
1
1
1
1
0
1
end_state
begin_goal
3
3 0
27 0
30 0
end_goal
358
begin_operator
grasp left shaker1
0
2
0 0 5 4
0 4 0 1
1
end_operator
begin_operator
grasp left shot1
0
2
0 0 5 2
0 46 0 1
1
end_operator
begin_operator
grasp left shot2
0
2
0 0 5 3
0 32 0 1
1
end_operator
begin_operator
grasp left shot3
0
2
0 0 5 0
0 18 0 1
1
end_operator
begin_operator
grasp left shot4
0
2
0 0 5 1
0 60 0 1
1
end_operator
begin_operator
grasp right shaker1
0
2
0 1 0 4
0 4 0 1
1
end_operator
begin_operator
grasp right shot1
0
2
0 1 0 5
0 46 0 1
1
end_operator
begin_operator
grasp right shot2
0
2
0 1 0 1
0 32 0 1
1
end_operator
begin_operator
grasp right shot3
0
2
0 1 0 3
0 18 0 1
1
end_operator
begin_operator
grasp right shot4
0
2
0 1 0 2
0 60 0 1
1
end_operator
begin_operator
leave left shaker1
0
2
0 0 4 5
0 4 -1 0
1
end_operator
begin_operator
leave left shot1
0
2
0 0 2 5
0 46 -1 0
1
end_operator
begin_operator
leave left shot2
0
2
0 0 3 5
0 32 -1 0
1
end_operator
begin_operator
leave left shot3
0
2
0 0 0 5
0 18 -1 0
1
end_operator
begin_operator
leave left shot4
0
2
0 0 1 5
0 60 -1 0
1
end_operator
begin_operator
leave right shaker1
0
2
0 1 4 0
0 4 -1 0
1
end_operator
begin_operator
leave right shot1
0
2
0 1 5 0
0 46 -1 0
1
end_operator
begin_operator
leave right shot2
0
2
0 1 1 0
0 32 -1 0
1
end_operator
begin_operator
leave right shot3
0
2
0 1 3 0
0 18 -1 0
1
end_operator
begin_operator
leave right shot4
0
2
0 1 2 0
0 60 -1 0
1
end_operator
begin_operator
clean-shaker left right shaker1
3
0 4
1 0
15 0
1
0 45 -1 0
1
end_operator
begin_operator
fill-shot shot1 ingredient1 left right dispenser1
2
0 2
1 0
4
0 6 0 1
0 38 0 1
0 49 -1 0
0 57 -1 0
10
end_operator
begin_operator
fill-shot shot1 ingredient2 left right dispenser2
2
0 2
1 0
4
0 6 0 1
0 38 0 1
0 43 -1 0
0 51 -1 0
10
end_operator
begin_operator
fill-shot shot1 ingredient3 left right dispenser3
2
0 2
1 0
4
0 6 0 1
0 19 -1 0
0 38 0 1
0 59 -1 0
10
end_operator
begin_operator
fill-shot shot2 ingredient1 left right dispenser1
2
0 3
1 0
4
0 9 0 1
0 12 0 1
0 24 -1 0
0 36 -1 0
10
end_operator
begin_operator
fill-shot shot2 ingredient2 left right dispenser2
2
0 3
1 0
4
0 9 0 1
0 12 0 1
0 26 -1 0
0 52 -1 0
10
end_operator
begin_operator
fill-shot shot2 ingredient3 left right dispenser3
2
0 3
1 0
4
0 8 -1 0
0 9 0 1
0 12 0 1
0 58 -1 0
10
end_operator
begin_operator
fill-shot shot3 ingredient1 left right dispenser1
2
0 0
1 0
4
0 10 -1 0
0 31 0 1
0 33 0 1
0 34 -1 0
10
end_operator
begin_operator
fill-shot shot3 ingredient2 left right dispenser2
2
0 0
1 0
4
0 14 -1 0
0 16 -1 0
0 31 0 1
0 33 0 1
10
end_operator
begin_operator
fill-shot shot3 ingredient3 left right dispenser3
2
0 0
1 0
4
0 23 -1 0
0 31 0 1
0 33 0 1
0 35 -1 0
10
end_operator
begin_operator
fill-shot shot4 ingredient1 left right dispenser1
2
0 1
1 0
4
0 5 0 1
0 29 -1 0
0 40 0 1
0 55 -1 0
10
end_operator
begin_operator
fill-shot shot4 ingredient2 left right dispenser2
2
0 1
1 0
4
0 5 0 1
0 13 -1 0
0 21 -1 0
0 40 0 1
10
end_operator
begin_operator
fill-shot shot4 ingredient3 left right dispenser3
2
0 1
1 0
4
0 5 0 1
0 7 -1 0
0 40 0 1
0 42 -1 0
10
end_operator
begin_operator
clean-shaker right left shaker1
3
0 5
1 4
15 0
1
0 45 -1 0
1
end_operator
begin_operator
fill-shot shot1 ingredient1 right left dispenser1
2
0 5
1 5
4
0 6 0 1
0 38 0 1
0 49 -1 0
0 57 -1 0
10
end_operator
begin_operator
fill-shot shot1 ingredient2 right left dispenser2
2
0 5
1 5
4
0 6 0 1
0 38 0 1
0 43 -1 0
0 51 -1 0
10
end_operator
begin_operator
fill-shot shot1 ingredient3 right left dispenser3
2
0 5
1 5
4
0 6 0 1
0 19 -1 0
0 38 0 1
0 59 -1 0
10
end_operator
begin_operator
fill-shot shot2 ingredient1 right left dispenser1
2
0 5
1 1
4
0 9 0 1
0 12 0 1
0 24 -1 0
0 36 -1 0
10
end_operator
begin_operator
fill-shot shot2 ingredient2 right left dispenser2
2
0 5
1 1
4
0 9 0 1
0 12 0 1
0 26 -1 0
0 52 -1 0
10
end_operator
begin_operator
fill-shot shot2 ingredient3 right left dispenser3
2
0 5
1 1
4
0 8 -1 0
0 9 0 1
0 12 0 1
0 58 -1 0
10
end_operator
begin_operator
fill-shot shot3 ingredient1 right left dispenser1
2
0 5
1 3
4
0 10 -1 0
0 31 0 1
0 33 0 1
0 34 -1 0
10
end_operator
begin_operator
fill-shot shot3 ingredient2 right left dispenser2
2
0 5
1 3
4
0 14 -1 0
0 16 -1 0
0 31 0 1
0 33 0 1
10
end_operator
begin_operator
fill-shot shot3 ingredient3 right left dispenser3
2
0 5
1 3
4
0 23 -1 0
0 31 0 1
0 33 0 1
0 35 -1 0
10
end_operator
begin_operator
fill-shot shot4 ingredient1 right left dispenser1
2
0 5
1 2
4
0 5 0 1
0 29 -1 0
0 40 0 1
0 55 -1 0
10
end_operator
begin_operator
fill-shot shot4 ingredient2 right left dispenser2
2
0 5
1 2
4
0 5 0 1
0 13 -1 0
0 21 -1 0
0 40 0 1
10
end_operator
begin_operator
fill-shot shot4 ingredient3 right left dispenser3
2
0 5
1 2
4
0 5 0 1
0 7 -1 0
0 40 0 1
0 42 -1 0
10
end_operator
begin_operator
empty-shot left shot1 ingredient1
1
0 2
2
0 6 -1 0
0 49 0 1
1
end_operator
begin_operator
empty-shot right shot1 ingredient1
1
1 5
2
0 6 -1 0
0 49 0 1
1
end_operator
begin_operator
clean-shot shot1 ingredient1 left right
3
0 2
1 0
6 0
2
0 38 -1 0
0 57 0 1
1
end_operator
begin_operator
clean-shot shot1 ingredient1 right left
3
0 5
1 5
6 0
2
0 38 -1 0
0 57 0 1
1
end_operator
begin_operator
empty-shot left shot1 ingredient2
1
0 2
2
0 6 -1 0
0 51 0 1
1
end_operator
begin_operator
empty-shot right shot1 ingredient2
1
1 5
2
0 6 -1 0
0 51 0 1
1
end_operator
begin_operator
clean-shot shot1 ingredient2 left right
3
0 2
1 0
6 0
2
0 38 -1 0
0 43 0 1
1
end_operator
begin_operator
clean-shot shot1 ingredient2 right left
3
0 5
1 5
6 0
2
0 38 -1 0
0 43 0 1
1
end_operator
begin_operator
empty-shot left shot1 ingredient3
1
0 2
2
0 6 -1 0
0 19 0 1
1
end_operator
begin_operator
empty-shot right shot1 ingredient3
1
1 5
2
0 6 -1 0
0 19 0 1
1
end_operator
begin_operator
clean-shot shot1 ingredient3 left right
3
0 2
1 0
6 0
2
0 38 -1 0
0 59 0 1
1
end_operator
begin_operator
clean-shot shot1 ingredient3 right left
3
0 5
1 5
6 0
2
0 38 -1 0
0 59 0 1
1
end_operator
begin_operator
empty-shot left shot2 ingredient1
1
0 3
2
0 12 -1 0
0 36 0 1
1
end_operator
begin_operator
empty-shot right shot2 ingredient1
1
1 1
2
0 12 -1 0
0 36 0 1
1
end_operator
begin_operator
clean-shot shot2 ingredient1 left right
3
0 3
1 0
12 0
2
0 9 -1 0
0 24 0 1
1
end_operator
begin_operator
clean-shot shot2 ingredient1 right left
3
0 5
1 1
12 0
2
0 9 -1 0
0 24 0 1
1
end_operator
begin_operator
empty-shot left shot2 ingredient2
1
0 3
2
0 12 -1 0
0 26 0 1
1
end_operator
begin_operator
empty-shot right shot2 ingredient2
1
1 1
2
0 12 -1 0
0 26 0 1
1
end_operator
begin_operator
clean-shot shot2 ingredient2 left right
3
0 3
1 0
12 0
2
0 9 -1 0
0 52 0 1
1
end_operator
begin_operator
clean-shot shot2 ingredient2 right left
3
0 5
1 1
12 0
2
0 9 -1 0
0 52 0 1
1
end_operator
begin_operator
empty-shot left shot2 ingredient3
1
0 3
2
0 12 -1 0
0 58 0 1
1
end_operator
begin_operator
empty-shot right shot2 ingredient3
1
1 1
2
0 12 -1 0
0 58 0 1
1
end_operator
begin_operator
clean-shot shot2 ingredient3 left right
3
0 3
1 0
12 0
2
0 8 0 1
0 9 -1 0
1
end_operator
begin_operator
clean-shot shot2 ingredient3 right left
3
0 5
1 1
12 0
2
0 8 0 1
0 9 -1 0
1
end_operator
begin_operator
empty-shot left shot3 ingredient1
1
0 0
2
0 10 0 1
0 33 -1 0
1
end_operator
begin_operator
empty-shot right shot3 ingredient1
1
1 3
2
0 10 0 1
0 33 -1 0
1
end_operator
begin_operator
clean-shot shot3 ingredient1 left right
3
0 0
1 0
33 0
2
0 31 -1 0
0 34 0 1
1
end_operator
begin_operator
clean-shot shot3 ingredient1 right left
3
0 5
1 3
33 0
2
0 31 -1 0
0 34 0 1
1
end_operator
begin_operator
empty-shot left shot3 ingredient2
1
0 0
2
0 16 0 1
0 33 -1 0
1
end_operator
begin_operator
empty-shot right shot3 ingredient2
1
1 3
2
0 16 0 1
0 33 -1 0
1
end_operator
begin_operator
clean-shot shot3 ingredient2 left right
3
0 0
1 0
33 0
2
0 14 0 1
0 31 -1 0
1
end_operator
begin_operator
clean-shot shot3 ingredient2 right left
3
0 5
1 3
33 0
2
0 14 0 1
0 31 -1 0
1
end_operator
begin_operator
empty-shot left shot3 ingredient3
1
0 0
2
0 33 -1 0
0 35 0 1
1
end_operator
begin_operator
empty-shot right shot3 ingredient3
1
1 3
2
0 33 -1 0
0 35 0 1
1
end_operator
begin_operator
clean-shot shot3 ingredient3 left right
3
0 0
1 0
33 0
2
0 23 0 1
0 31 -1 0
1
end_operator
begin_operator
clean-shot shot3 ingredient3 right left
3
0 5
1 3
33 0
2
0 23 0 1
0 31 -1 0
1
end_operator
begin_operator
empty-shot left shot4 ingredient1
1
0 1
2
0 5 -1 0
0 55 0 1
1
end_operator
begin_operator
empty-shot right shot4 ingredient1
1
1 2
2
0 5 -1 0
0 55 0 1
1
end_operator
begin_operator
clean-shot shot4 ingredient1 left right
3
0 1
1 0
5 0
2
0 29 0 1
0 40 -1 0
1
end_operator
begin_operator
clean-shot shot4 ingredient1 right left
3
0 5
1 2
5 0
2
0 29 0 1
0 40 -1 0
1
end_operator
begin_operator
empty-shot left shot4 ingredient2
1
0 1
2
0 5 -1 0
0 21 0 1
1
end_operator
begin_operator
empty-shot right shot4 ingredient2
1
1 2
2
0 5 -1 0
0 21 0 1
1
end_operator
begin_operator
clean-shot shot4 ingredient2 left right
3
0 1
1 0
5 0
2
0 13 0 1
0 40 -1 0
1
end_operator
begin_operator
clean-shot shot4 ingredient2 right left
3
0 5
1 2
5 0
2
0 13 0 1
0 40 -1 0
1
end_operator
begin_operator
empty-shot left shot4 ingredient3
1
0 1
2
0 5 -1 0
0 7 0 1
1
end_operator
begin_operator
empty-shot right shot4 ingredient3
1
1 2
2
0 5 -1 0
0 7 0 1
1
end_operator
begin_operator
clean-shot shot4 ingredient3 left right
3
0 1
1 0
5 0
2
0 40 -1 0
0 42 0 1
1
end_operator
begin_operator
clean-shot shot4 ingredient3 right left
3
0 5
1 2
5 0
2
0 40 -1 0
0 42 0 1
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot1 ingredient1 shaker1 left l0 l1
1
0 2
7
0 2 1 0
0 6 -1 0
0 15 0 1
0 28 -1 0
0 45 0 1
0 49 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot1 ingredient1 shaker1 right l0 l1
1
1 5
7
0 2 1 0
0 6 -1 0
0 15 0 1
0 28 -1 0
0 45 0 1
0 49 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot1 ingredient2 shaker1 left l0 l1
1
0 2
7
0 2 1 0
0 6 -1 0
0 15 0 1
0 39 -1 0
0 45 0 1
0 51 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot1 ingredient2 shaker1 right l0 l1
1
1 5
7
0 2 1 0
0 6 -1 0
0 15 0 1
0 39 -1 0
0 45 0 1
0 51 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot1 ingredient3 shaker1 left l0 l1
1
0 2
7
0 2 1 0
0 6 -1 0
0 15 0 1
0 19 0 1
0 45 0 1
0 56 -1 0
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot1 ingredient3 shaker1 right l0 l1
1
1 5
7
0 2 1 0
0 6 -1 0
0 15 0 1
0 19 0 1
0 45 0 1
0 56 -1 0
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot2 ingredient1 shaker1 left l0 l1
1
0 3
7
0 2 1 0
0 12 -1 0
0 15 0 1
0 28 -1 0
0 36 0 1
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot2 ingredient1 shaker1 right l0 l1
1
1 1
7
0 2 1 0
0 12 -1 0
0 15 0 1
0 28 -1 0
0 36 0 1
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot2 ingredient2 shaker1 left l0 l1
1
0 3
7
0 2 1 0
0 12 -1 0
0 15 0 1
0 26 0 1
0 39 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot2 ingredient2 shaker1 right l0 l1
1
1 1
7
0 2 1 0
0 12 -1 0
0 15 0 1
0 26 0 1
0 39 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot2 ingredient3 shaker1 left l0 l1
1
0 3
7
0 2 1 0
0 12 -1 0
0 15 0 1
0 45 0 1
0 56 -1 0
0 58 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot2 ingredient3 shaker1 right l0 l1
1
1 1
7
0 2 1 0
0 12 -1 0
0 15 0 1
0 45 0 1
0 56 -1 0
0 58 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot3 ingredient1 shaker1 left l0 l1
1
0 0
7
0 2 1 0
0 10 0 1
0 15 0 1
0 28 -1 0
0 33 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot3 ingredient1 shaker1 right l0 l1
1
1 3
7
0 2 1 0
0 10 0 1
0 15 0 1
0 28 -1 0
0 33 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot3 ingredient2 shaker1 left l0 l1
1
0 0
7
0 2 1 0
0 15 0 1
0 16 0 1
0 33 -1 0
0 39 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot3 ingredient2 shaker1 right l0 l1
1
1 3
7
0 2 1 0
0 15 0 1
0 16 0 1
0 33 -1 0
0 39 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot3 ingredient3 shaker1 left l0 l1
1
0 0
7
0 2 1 0
0 15 0 1
0 33 -1 0
0 35 0 1
0 45 0 1
0 56 -1 0
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot3 ingredient3 shaker1 right l0 l1
1
1 3
7
0 2 1 0
0 15 0 1
0 33 -1 0
0 35 0 1
0 45 0 1
0 56 -1 0
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot4 ingredient1 shaker1 left l0 l1
1
0 1
7
0 2 1 0
0 5 -1 0
0 15 0 1
0 28 -1 0
0 45 0 1
0 55 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot4 ingredient1 shaker1 right l0 l1
1
1 2
7
0 2 1 0
0 5 -1 0
0 15 0 1
0 28 -1 0
0 45 0 1
0 55 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot4 ingredient2 shaker1 left l0 l1
1
0 1
7
0 2 1 0
0 5 -1 0
0 15 0 1
0 21 0 1
0 39 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot4 ingredient2 shaker1 right l0 l1
1
1 2
7
0 2 1 0
0 5 -1 0
0 15 0 1
0 21 0 1
0 39 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot4 ingredient3 shaker1 left l0 l1
1
0 1
7
0 2 1 0
0 5 -1 0
0 7 0 1
0 15 0 1
0 45 0 1
0 56 -1 0
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot4 ingredient3 shaker1 right l0 l1
1
1 2
7
0 2 1 0
0 5 -1 0
0 7 0 1
0 15 0 1
0 45 0 1
0 56 -1 0
0 61 -1 0
1
end_operator
begin_operator
refill-shot shot1 ingredient1 left right dispenser1
3
0 2
1 0
57 0
2
0 6 0 1
0 49 -1 0
10
end_operator
begin_operator
refill-shot shot1 ingredient1 right left dispenser1
3
0 5
1 5
57 0
2
0 6 0 1
0 49 -1 0
10
end_operator
begin_operator
refill-shot shot1 ingredient2 left right dispenser2
3
0 2
1 0
43 0
2
0 6 0 1
0 51 -1 0
10
end_operator
begin_operator
refill-shot shot1 ingredient2 right left dispenser2
3
0 5
1 5
43 0
2
0 6 0 1
0 51 -1 0
10
end_operator
begin_operator
refill-shot shot1 ingredient3 left right dispenser3
3
0 2
1 0
59 0
2
0 6 0 1
0 19 -1 0
10
end_operator
begin_operator
refill-shot shot1 ingredient3 right left dispenser3
3
0 5
1 5
59 0
2
0 6 0 1
0 19 -1 0
10
end_operator
begin_operator
refill-shot shot2 ingredient1 left right dispenser1
3
0 3
1 0
24 0
2
0 12 0 1
0 36 -1 0
10
end_operator
begin_operator
refill-shot shot2 ingredient1 right left dispenser1
3
0 5
1 1
24 0
2
0 12 0 1
0 36 -1 0
10
end_operator
begin_operator
refill-shot shot2 ingredient2 left right dispenser2
3
0 3
1 0
52 0
2
0 12 0 1
0 26 -1 0
10
end_operator
begin_operator
refill-shot shot2 ingredient2 right left dispenser2
3
0 5
1 1
52 0
2
0 12 0 1
0 26 -1 0
10
end_operator
begin_operator
refill-shot shot2 ingredient3 left right dispenser3
3
0 3
1 0
8 0
2
0 12 0 1
0 58 -1 0
10
end_operator
begin_operator
refill-shot shot2 ingredient3 right left dispenser3
3
0 5
1 1
8 0
2
0 12 0 1
0 58 -1 0
10
end_operator
begin_operator
refill-shot shot3 ingredient1 left right dispenser1
3
0 0
1 0
34 0
2
0 10 -1 0
0 33 0 1
10
end_operator
begin_operator
refill-shot shot3 ingredient1 right left dispenser1
3
0 5
1 3
34 0
2
0 10 -1 0
0 33 0 1
10
end_operator
begin_operator
refill-shot shot3 ingredient2 left right dispenser2
3
0 0
1 0
14 0
2
0 16 -1 0
0 33 0 1
10
end_operator
begin_operator
refill-shot shot3 ingredient2 right left dispenser2
3
0 5
1 3
14 0
2
0 16 -1 0
0 33 0 1
10
end_operator
begin_operator
refill-shot shot3 ingredient3 left right dispenser3
3
0 0
1 0
23 0
2
0 33 0 1
0 35 -1 0
10
end_operator
begin_operator
refill-shot shot3 ingredient3 right left dispenser3
3
0 5
1 3
23 0
2
0 33 0 1
0 35 -1 0
10
end_operator
begin_operator
refill-shot shot4 ingredient1 left right dispenser1
3
0 1
1 0
29 0
2
0 5 0 1
0 55 -1 0
10
end_operator
begin_operator
refill-shot shot4 ingredient1 right left dispenser1
3
0 5
1 2
29 0
2
0 5 0 1
0 55 -1 0
10
end_operator
begin_operator
refill-shot shot4 ingredient2 left right dispenser2
3
0 1
1 0
13 0
2
0 5 0 1
0 21 -1 0
10
end_operator
begin_operator
refill-shot shot4 ingredient2 right left dispenser2
3
0 5
1 2
13 0
2
0 5 0 1
0 21 -1 0
10
end_operator
begin_operator
refill-shot shot4 ingredient3 left right dispenser3
3
0 1
1 0
42 0
2
0 5 0 1
0 7 -1 0
10
end_operator
begin_operator
refill-shot shot4 ingredient3 right left dispenser3
3
0 5
1 2
42 0
2
0 5 0 1
0 7 -1 0
10
end_operator
begin_operator
pour-shot-to-used-shaker shot1 ingredient1 shaker1 left l0 l1
2
0 2
61 0
4
0 2 1 0
0 6 -1 0
0 28 -1 0
0 49 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot1 ingredient1 shaker1 right l0 l1
2
1 5
61 0
4
0 2 1 0
0 6 -1 0
0 28 -1 0
0 49 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot1 ingredient2 shaker1 left l0 l1
2
0 2
61 0
4
0 2 1 0
0 6 -1 0
0 39 -1 0
0 51 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot1 ingredient2 shaker1 right l0 l1
2
1 5
61 0
4
0 2 1 0
0 6 -1 0
0 39 -1 0
0 51 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot1 ingredient3 shaker1 left l0 l1
2
0 2
61 0
4
0 2 1 0
0 6 -1 0
0 19 0 1
0 56 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot1 ingredient3 shaker1 right l0 l1
2
1 5
61 0
4
0 2 1 0
0 6 -1 0
0 19 0 1
0 56 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot2 ingredient1 shaker1 left l0 l1
2
0 3
61 0
4
0 2 1 0
0 12 -1 0
0 28 -1 0
0 36 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot2 ingredient1 shaker1 right l0 l1
2
1 1
61 0
4
0 2 1 0
0 12 -1 0
0 28 -1 0
0 36 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot2 ingredient2 shaker1 left l0 l1
2
0 3
61 0
4
0 2 1 0
0 12 -1 0
0 26 0 1
0 39 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot2 ingredient2 shaker1 right l0 l1
2
1 1
61 0
4
0 2 1 0
0 12 -1 0
0 26 0 1
0 39 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot2 ingredient3 shaker1 left l0 l1
2
0 3
61 0
4
0 2 1 0
0 12 -1 0
0 56 -1 0
0 58 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot2 ingredient3 shaker1 right l0 l1
2
1 1
61 0
4
0 2 1 0
0 12 -1 0
0 56 -1 0
0 58 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot3 ingredient1 shaker1 left l0 l1
2
0 0
61 0
4
0 2 1 0
0 10 0 1
0 28 -1 0
0 33 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot3 ingredient1 shaker1 right l0 l1
2
1 3
61 0
4
0 2 1 0
0 10 0 1
0 28 -1 0
0 33 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot3 ingredient2 shaker1 left l0 l1
2
0 0
61 0
4
0 2 1 0
0 16 0 1
0 33 -1 0
0 39 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot3 ingredient2 shaker1 right l0 l1
2
1 3
61 0
4
0 2 1 0
0 16 0 1
0 33 -1 0
0 39 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot3 ingredient3 shaker1 left l0 l1
2
0 0
61 0
4
0 2 1 0
0 33 -1 0
0 35 0 1
0 56 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot3 ingredient3 shaker1 right l0 l1
2
1 3
61 0
4
0 2 1 0
0 33 -1 0
0 35 0 1
0 56 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot4 ingredient1 shaker1 left l0 l1
2
0 1
61 0
4
0 2 1 0
0 5 -1 0
0 28 -1 0
0 55 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot4 ingredient1 shaker1 right l0 l1
2
1 2
61 0
4
0 2 1 0
0 5 -1 0
0 28 -1 0
0 55 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot4 ingredient2 shaker1 left l0 l1
2
0 1
61 0
4
0 2 1 0
0 5 -1 0
0 21 0 1
0 39 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot4 ingredient2 shaker1 right l0 l1
2
1 2
61 0
4
0 2 1 0
0 5 -1 0
0 21 0 1
0 39 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot4 ingredient3 shaker1 left l0 l1
2
0 1
61 0
4
0 2 1 0
0 5 -1 0
0 7 0 1
0 56 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot4 ingredient3 shaker1 right l0 l1
2
1 2
61 0
4
0 2 1 0
0 5 -1 0
0 7 0 1
0 56 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot1 ingredient1 shaker1 left l1 l2
1
0 2
7
0 2 0 2
0 6 -1 0
0 15 0 1
0 28 -1 0
0 45 0 1
0 49 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot1 ingredient1 shaker1 right l1 l2
1
1 5
7
0 2 0 2
0 6 -1 0
0 15 0 1
0 28 -1 0
0 45 0 1
0 49 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot1 ingredient2 shaker1 left l1 l2
1
0 2
7
0 2 0 2
0 6 -1 0
0 15 0 1
0 39 -1 0
0 45 0 1
0 51 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot1 ingredient2 shaker1 right l1 l2
1
1 5
7
0 2 0 2
0 6 -1 0
0 15 0 1
0 39 -1 0
0 45 0 1
0 51 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot1 ingredient3 shaker1 left l1 l2
1
0 2
7
0 2 0 2
0 6 -1 0
0 15 0 1
0 19 0 1
0 45 0 1
0 56 -1 0
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot1 ingredient3 shaker1 right l1 l2
1
1 5
7
0 2 0 2
0 6 -1 0
0 15 0 1
0 19 0 1
0 45 0 1
0 56 -1 0
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot2 ingredient1 shaker1 left l1 l2
1
0 3
7
0 2 0 2
0 12 -1 0
0 15 0 1
0 28 -1 0
0 36 0 1
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot2 ingredient1 shaker1 right l1 l2
1
1 1
7
0 2 0 2
0 12 -1 0
0 15 0 1
0 28 -1 0
0 36 0 1
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot2 ingredient2 shaker1 left l1 l2
1
0 3
7
0 2 0 2
0 12 -1 0
0 15 0 1
0 26 0 1
0 39 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot2 ingredient2 shaker1 right l1 l2
1
1 1
7
0 2 0 2
0 12 -1 0
0 15 0 1
0 26 0 1
0 39 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot2 ingredient3 shaker1 left l1 l2
1
0 3
7
0 2 0 2
0 12 -1 0
0 15 0 1
0 45 0 1
0 56 -1 0
0 58 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot2 ingredient3 shaker1 right l1 l2
1
1 1
7
0 2 0 2
0 12 -1 0
0 15 0 1
0 45 0 1
0 56 -1 0
0 58 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot3 ingredient1 shaker1 left l1 l2
1
0 0
7
0 2 0 2
0 10 0 1
0 15 0 1
0 28 -1 0
0 33 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot3 ingredient1 shaker1 right l1 l2
1
1 3
7
0 2 0 2
0 10 0 1
0 15 0 1
0 28 -1 0
0 33 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot3 ingredient2 shaker1 left l1 l2
1
0 0
7
0 2 0 2
0 15 0 1
0 16 0 1
0 33 -1 0
0 39 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot3 ingredient2 shaker1 right l1 l2
1
1 3
7
0 2 0 2
0 15 0 1
0 16 0 1
0 33 -1 0
0 39 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot3 ingredient3 shaker1 left l1 l2
1
0 0
7
0 2 0 2
0 15 0 1
0 33 -1 0
0 35 0 1
0 45 0 1
0 56 -1 0
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot3 ingredient3 shaker1 right l1 l2
1
1 3
7
0 2 0 2
0 15 0 1
0 33 -1 0
0 35 0 1
0 45 0 1
0 56 -1 0
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot4 ingredient1 shaker1 left l1 l2
1
0 1
7
0 2 0 2
0 5 -1 0
0 15 0 1
0 28 -1 0
0 45 0 1
0 55 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot4 ingredient1 shaker1 right l1 l2
1
1 2
7
0 2 0 2
0 5 -1 0
0 15 0 1
0 28 -1 0
0 45 0 1
0 55 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot4 ingredient2 shaker1 left l1 l2
1
0 1
7
0 2 0 2
0 5 -1 0
0 15 0 1
0 21 0 1
0 39 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot4 ingredient2 shaker1 right l1 l2
1
1 2
7
0 2 0 2
0 5 -1 0
0 15 0 1
0 21 0 1
0 39 -1 0
0 45 0 1
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot4 ingredient3 shaker1 left l1 l2
1
0 1
7
0 2 0 2
0 5 -1 0
0 7 0 1
0 15 0 1
0 45 0 1
0 56 -1 0
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-clean-shaker shot4 ingredient3 shaker1 right l1 l2
1
1 2
7
0 2 0 2
0 5 -1 0
0 7 0 1
0 15 0 1
0 45 0 1
0 56 -1 0
0 61 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot1 ingredient1 shaker1 left l1 l2
2
0 2
61 0
4
0 2 0 2
0 6 -1 0
0 28 -1 0
0 49 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot1 ingredient1 shaker1 right l1 l2
2
1 5
61 0
4
0 2 0 2
0 6 -1 0
0 28 -1 0
0 49 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot1 ingredient2 shaker1 left l1 l2
2
0 2
61 0
4
0 2 0 2
0 6 -1 0
0 39 -1 0
0 51 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot1 ingredient2 shaker1 right l1 l2
2
1 5
61 0
4
0 2 0 2
0 6 -1 0
0 39 -1 0
0 51 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot1 ingredient3 shaker1 left l1 l2
2
0 2
61 0
4
0 2 0 2
0 6 -1 0
0 19 0 1
0 56 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot1 ingredient3 shaker1 right l1 l2
2
1 5
61 0
4
0 2 0 2
0 6 -1 0
0 19 0 1
0 56 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot2 ingredient1 shaker1 left l1 l2
2
0 3
61 0
4
0 2 0 2
0 12 -1 0
0 28 -1 0
0 36 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot2 ingredient1 shaker1 right l1 l2
2
1 1
61 0
4
0 2 0 2
0 12 -1 0
0 28 -1 0
0 36 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot2 ingredient2 shaker1 left l1 l2
2
0 3
61 0
4
0 2 0 2
0 12 -1 0
0 26 0 1
0 39 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot2 ingredient2 shaker1 right l1 l2
2
1 1
61 0
4
0 2 0 2
0 12 -1 0
0 26 0 1
0 39 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot2 ingredient3 shaker1 left l1 l2
2
0 3
61 0
4
0 2 0 2
0 12 -1 0
0 56 -1 0
0 58 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot2 ingredient3 shaker1 right l1 l2
2
1 1
61 0
4
0 2 0 2
0 12 -1 0
0 56 -1 0
0 58 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot3 ingredient1 shaker1 left l1 l2
2
0 0
61 0
4
0 2 0 2
0 10 0 1
0 28 -1 0
0 33 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot3 ingredient1 shaker1 right l1 l2
2
1 3
61 0
4
0 2 0 2
0 10 0 1
0 28 -1 0
0 33 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot3 ingredient2 shaker1 left l1 l2
2
0 0
61 0
4
0 2 0 2
0 16 0 1
0 33 -1 0
0 39 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot3 ingredient2 shaker1 right l1 l2
2
1 3
61 0
4
0 2 0 2
0 16 0 1
0 33 -1 0
0 39 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot3 ingredient3 shaker1 left l1 l2
2
0 0
61 0
4
0 2 0 2
0 33 -1 0
0 35 0 1
0 56 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot3 ingredient3 shaker1 right l1 l2
2
1 3
61 0
4
0 2 0 2
0 33 -1 0
0 35 0 1
0 56 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot4 ingredient1 shaker1 left l1 l2
2
0 1
61 0
4
0 2 0 2
0 5 -1 0
0 28 -1 0
0 55 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot4 ingredient1 shaker1 right l1 l2
2
1 2
61 0
4
0 2 0 2
0 5 -1 0
0 28 -1 0
0 55 0 1
1
end_operator
begin_operator
pour-shot-to-used-shaker shot4 ingredient2 shaker1 left l1 l2
2
0 1
61 0
4
0 2 0 2
0 5 -1 0
0 21 0 1
0 39 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot4 ingredient2 shaker1 right l1 l2
2
1 2
61 0
4
0 2 0 2
0 5 -1 0
0 21 0 1
0 39 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot4 ingredient3 shaker1 left l1 l2
2
0 1
61 0
4
0 2 0 2
0 5 -1 0
0 7 0 1
0 56 -1 0
1
end_operator
begin_operator
pour-shot-to-used-shaker shot4 ingredient3 shaker1 right l1 l2
2
1 2
61 0
4
0 2 0 2
0 5 -1 0
0 7 0 1
0 56 -1 0
1
end_operator
begin_operator
shake cocktail3 ingredient1 ingredient2 shaker1 left right
2
0 4
1 0
5
0 11 -1 0
0 28 0 1
0 39 0 1
0 41 -1 0
0 61 0 1
1
end_operator
begin_operator
shake cocktail3 ingredient1 ingredient2 shaker1 right left
2
0 5
1 4
5
0 11 -1 0
0 28 0 1
0 39 0 1
0 41 -1 0
0 61 0 1
1
end_operator
begin_operator
shake cocktail1 ingredient3 ingredient1 shaker1 left right
2
0 4
1 0
5
0 11 -1 0
0 28 0 1
0 53 -1 0
0 56 0 1
0 61 0 1
1
end_operator
begin_operator
shake cocktail1 ingredient3 ingredient1 shaker1 right left
2
0 5
1 4
5
0 11 -1 0
0 28 0 1
0 53 -1 0
0 56 0 1
0 61 0 1
1
end_operator
begin_operator
shake cocktail2 ingredient2 ingredient3 shaker1 left right
2
0 4
1 0
5
0 11 -1 0
0 39 0 1
0 47 -1 0
0 56 0 1
0 61 0 1
1
end_operator
begin_operator
shake cocktail2 ingredient2 ingredient3 shaker1 right left
2
0 5
1 4
5
0 11 -1 0
0 39 0 1
0 47 -1 0
0 56 0 1
0 61 0 1
1
end_operator
begin_operator
empty-shaker left shaker1 cocktail3 l0 l0
2
0 4
2 1
3
0 11 0 1
0 15 -1 0
0 41 0 1
1
end_operator
begin_operator
empty-shaker left shaker1 cocktail3 l1 l0
1
0 4
4
0 2 0 1
0 11 0 1
0 15 -1 0
0 41 0 1
1
end_operator
begin_operator
empty-shaker left shaker1 cocktail3 l2 l0
1
0 4
4
0 2 2 1
0 11 0 1
0 15 -1 0
0 41 0 1
1
end_operator
begin_operator
empty-shaker left shaker1 cocktail1 l0 l0
2
0 4
2 1
3
0 11 0 1
0 15 -1 0
0 53 0 1
1
end_operator
begin_operator
empty-shaker left shaker1 cocktail1 l1 l0
1
0 4
4
0 2 0 1
0 11 0 1
0 15 -1 0
0 53 0 1
1
end_operator
begin_operator
empty-shaker left shaker1 cocktail1 l2 l0
1
0 4
4
0 2 2 1
0 11 0 1
0 15 -1 0
0 53 0 1
1
end_operator
begin_operator
empty-shaker left shaker1 cocktail2 l0 l0
2
0 4
2 1
3
0 11 0 1
0 15 -1 0
0 47 0 1
1
end_operator
begin_operator
empty-shaker left shaker1 cocktail2 l1 l0
1
0 4
4
0 2 0 1
0 11 0 1
0 15 -1 0
0 47 0 1
1
end_operator
begin_operator
empty-shaker left shaker1 cocktail2 l2 l0
1
0 4
4
0 2 2 1
0 11 0 1
0 15 -1 0
0 47 0 1
1
end_operator
begin_operator
empty-shaker right shaker1 cocktail3 l0 l0
2
1 4
2 1
3
0 11 0 1
0 15 -1 0
0 41 0 1
1
end_operator
begin_operator
empty-shaker right shaker1 cocktail3 l1 l0
1
1 4
4
0 2 0 1
0 11 0 1
0 15 -1 0
0 41 0 1
1
end_operator
begin_operator
empty-shaker right shaker1 cocktail3 l2 l0
1
1 4
4
0 2 2 1
0 11 0 1
0 15 -1 0
0 41 0 1
1
end_operator
begin_operator
empty-shaker right shaker1 cocktail1 l0 l0
2
1 4
2 1
3
0 11 0 1
0 15 -1 0
0 53 0 1
1
end_operator
begin_operator
empty-shaker right shaker1 cocktail1 l1 l0
1
1 4
4
0 2 0 1
0 11 0 1
0 15 -1 0
0 53 0 1
1
end_operator
begin_operator
empty-shaker right shaker1 cocktail1 l2 l0
1
1 4
4
0 2 2 1
0 11 0 1
0 15 -1 0
0 53 0 1
1
end_operator
begin_operator
empty-shaker right shaker1 cocktail2 l0 l0
2
1 4
2 1
3
0 11 0 1
0 15 -1 0
0 47 0 1
1
end_operator
begin_operator
empty-shaker right shaker1 cocktail2 l1 l0
1
1 4
4
0 2 0 1
0 11 0 1
0 15 -1 0
0 47 0 1
1
end_operator
begin_operator
empty-shaker right shaker1 cocktail2 l2 l0
1
1 4
4
0 2 2 1
0 11 0 1
0 15 -1 0
0 47 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot1 left shaker1 l1 l0
3
0 4
11 0
28 0
4
0 2 0 1
0 6 0 1
0 38 0 1
0 49 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot2 left shaker1 l1 l0
3
0 4
11 0
28 0
4
0 2 0 1
0 9 0 1
0 12 0 1
0 36 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot3 left shaker1 l1 l0
3
0 4
11 0
28 0
4
0 2 0 1
0 10 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot4 left shaker1 l1 l0
3
0 4
11 0
28 0
4
0 2 0 1
0 5 0 1
0 40 0 1
0 55 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot1 right shaker1 l1 l0
3
1 4
11 0
28 0
4
0 2 0 1
0 6 0 1
0 38 0 1
0 49 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot2 right shaker1 l1 l0
3
1 4
11 0
28 0
4
0 2 0 1
0 9 0 1
0 12 0 1
0 36 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot3 right shaker1 l1 l0
3
1 4
11 0
28 0
4
0 2 0 1
0 10 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot4 right shaker1 l1 l0
3
1 4
11 0
28 0
4
0 2 0 1
0 5 0 1
0 40 0 1
0 55 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot1 left shaker1 l1 l0
3
0 4
11 0
39 0
4
0 2 0 1
0 6 0 1
0 38 0 1
0 51 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot2 left shaker1 l1 l0
3
0 4
11 0
39 0
4
0 2 0 1
0 9 0 1
0 12 0 1
0 26 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot3 left shaker1 l1 l0
3
0 4
11 0
39 0
4
0 2 0 1
0 16 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot4 left shaker1 l1 l0
3
0 4
11 0
39 0
4
0 2 0 1
0 5 0 1
0 21 -1 0
0 40 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot1 right shaker1 l1 l0
3
1 4
11 0
39 0
4
0 2 0 1
0 6 0 1
0 38 0 1
0 51 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot2 right shaker1 l1 l0
3
1 4
11 0
39 0
4
0 2 0 1
0 9 0 1
0 12 0 1
0 26 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot3 right shaker1 l1 l0
3
1 4
11 0
39 0
4
0 2 0 1
0 16 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot4 right shaker1 l1 l0
3
1 4
11 0
39 0
4
0 2 0 1
0 5 0 1
0 21 -1 0
0 40 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot1 left shaker1 l1 l0
3
0 4
11 0
56 0
4
0 2 0 1
0 6 0 1
0 19 -1 0
0 38 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot2 left shaker1 l1 l0
3
0 4
11 0
56 0
4
0 2 0 1
0 9 0 1
0 12 0 1
0 58 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot3 left shaker1 l1 l0
3
0 4
11 0
56 0
4
0 2 0 1
0 31 0 1
0 33 0 1
0 35 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot4 left shaker1 l1 l0
3
0 4
11 0
56 0
4
0 2 0 1
0 5 0 1
0 7 -1 0
0 40 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot1 right shaker1 l1 l0
3
1 4
11 0
56 0
4
0 2 0 1
0 6 0 1
0 19 -1 0
0 38 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot2 right shaker1 l1 l0
3
1 4
11 0
56 0
4
0 2 0 1
0 9 0 1
0 12 0 1
0 58 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot3 right shaker1 l1 l0
3
1 4
11 0
56 0
4
0 2 0 1
0 31 0 1
0 33 0 1
0 35 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot4 right shaker1 l1 l0
3
1 4
11 0
56 0
4
0 2 0 1
0 5 0 1
0 7 -1 0
0 40 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot1 left shaker1 l1 l0
3
0 4
11 0
41 0
4
0 2 0 1
0 6 0 1
0 30 -1 0
0 38 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot2 left shaker1 l1 l0
3
0 4
11 0
41 0
4
0 2 0 1
0 9 0 1
0 12 0 1
0 54 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot3 left shaker1 l1 l0
3
0 4
11 0
41 0
4
0 2 0 1
0 31 0 1
0 33 0 1
0 48 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot4 left shaker1 l1 l0
3
0 4
11 0
41 0
4
0 2 0 1
0 5 0 1
0 40 0 1
0 44 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot1 right shaker1 l1 l0
3
1 4
11 0
41 0
4
0 2 0 1
0 6 0 1
0 30 -1 0
0 38 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot2 right shaker1 l1 l0
3
1 4
11 0
41 0
4
0 2 0 1
0 9 0 1
0 12 0 1
0 54 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot3 right shaker1 l1 l0
3
1 4
11 0
41 0
4
0 2 0 1
0 31 0 1
0 33 0 1
0 48 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot4 right shaker1 l1 l0
3
1 4
11 0
41 0
4
0 2 0 1
0 5 0 1
0 40 0 1
0 44 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot1 left shaker1 l1 l0
3
0 4
11 0
53 0
4
0 2 0 1
0 6 0 1
0 17 -1 0
0 38 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot2 left shaker1 l1 l0
3
0 4
11 0
53 0
4
0 2 0 1
0 9 0 1
0 12 0 1
0 27 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot3 left shaker1 l1 l0
3
0 4
11 0
53 0
4
0 2 0 1
0 22 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot4 left shaker1 l1 l0
3
0 4
11 0
53 0
4
0 2 0 1
0 5 0 1
0 20 -1 0
0 40 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot1 right shaker1 l1 l0
3
1 4
11 0
53 0
4
0 2 0 1
0 6 0 1
0 17 -1 0
0 38 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot2 right shaker1 l1 l0
3
1 4
11 0
53 0
4
0 2 0 1
0 9 0 1
0 12 0 1
0 27 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot3 right shaker1 l1 l0
3
1 4
11 0
53 0
4
0 2 0 1
0 22 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot4 right shaker1 l1 l0
3
1 4
11 0
53 0
4
0 2 0 1
0 5 0 1
0 20 -1 0
0 40 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot1 left shaker1 l1 l0
3
0 4
11 0
47 0
4
0 2 0 1
0 6 0 1
0 38 0 1
0 50 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot2 left shaker1 l1 l0
3
0 4
11 0
47 0
4
0 2 0 1
0 9 0 1
0 12 0 1
0 37 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot3 left shaker1 l1 l0
3
0 4
11 0
47 0
4
0 2 0 1
0 3 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot4 left shaker1 l1 l0
3
0 4
11 0
47 0
4
0 2 0 1
0 5 0 1
0 25 -1 0
0 40 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot1 right shaker1 l1 l0
3
1 4
11 0
47 0
4
0 2 0 1
0 6 0 1
0 38 0 1
0 50 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot2 right shaker1 l1 l0
3
1 4
11 0
47 0
4
0 2 0 1
0 9 0 1
0 12 0 1
0 37 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot3 right shaker1 l1 l0
3
1 4
11 0
47 0
4
0 2 0 1
0 3 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot4 right shaker1 l1 l0
3
1 4
11 0
47 0
4
0 2 0 1
0 5 0 1
0 25 -1 0
0 40 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot1 left shaker1 l2 l1
3
0 4
11 0
28 0
4
0 2 2 0
0 6 0 1
0 38 0 1
0 49 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot2 left shaker1 l2 l1
3
0 4
11 0
28 0
4
0 2 2 0
0 9 0 1
0 12 0 1
0 36 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot3 left shaker1 l2 l1
3
0 4
11 0
28 0
4
0 2 2 0
0 10 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot4 left shaker1 l2 l1
3
0 4
11 0
28 0
4
0 2 2 0
0 5 0 1
0 40 0 1
0 55 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot1 right shaker1 l2 l1
3
1 4
11 0
28 0
4
0 2 2 0
0 6 0 1
0 38 0 1
0 49 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot2 right shaker1 l2 l1
3
1 4
11 0
28 0
4
0 2 2 0
0 9 0 1
0 12 0 1
0 36 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot3 right shaker1 l2 l1
3
1 4
11 0
28 0
4
0 2 2 0
0 10 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient1 shot4 right shaker1 l2 l1
3
1 4
11 0
28 0
4
0 2 2 0
0 5 0 1
0 40 0 1
0 55 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot1 left shaker1 l2 l1
3
0 4
11 0
39 0
4
0 2 2 0
0 6 0 1
0 38 0 1
0 51 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot2 left shaker1 l2 l1
3
0 4
11 0
39 0
4
0 2 2 0
0 9 0 1
0 12 0 1
0 26 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot3 left shaker1 l2 l1
3
0 4
11 0
39 0
4
0 2 2 0
0 16 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot4 left shaker1 l2 l1
3
0 4
11 0
39 0
4
0 2 2 0
0 5 0 1
0 21 -1 0
0 40 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot1 right shaker1 l2 l1
3
1 4
11 0
39 0
4
0 2 2 0
0 6 0 1
0 38 0 1
0 51 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot2 right shaker1 l2 l1
3
1 4
11 0
39 0
4
0 2 2 0
0 9 0 1
0 12 0 1
0 26 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot3 right shaker1 l2 l1
3
1 4
11 0
39 0
4
0 2 2 0
0 16 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient2 shot4 right shaker1 l2 l1
3
1 4
11 0
39 0
4
0 2 2 0
0 5 0 1
0 21 -1 0
0 40 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot1 left shaker1 l2 l1
3
0 4
11 0
56 0
4
0 2 2 0
0 6 0 1
0 19 -1 0
0 38 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot2 left shaker1 l2 l1
3
0 4
11 0
56 0
4
0 2 2 0
0 9 0 1
0 12 0 1
0 58 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot3 left shaker1 l2 l1
3
0 4
11 0
56 0
4
0 2 2 0
0 31 0 1
0 33 0 1
0 35 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot4 left shaker1 l2 l1
3
0 4
11 0
56 0
4
0 2 2 0
0 5 0 1
0 7 -1 0
0 40 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot1 right shaker1 l2 l1
3
1 4
11 0
56 0
4
0 2 2 0
0 6 0 1
0 19 -1 0
0 38 0 1
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot2 right shaker1 l2 l1
3
1 4
11 0
56 0
4
0 2 2 0
0 9 0 1
0 12 0 1
0 58 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot3 right shaker1 l2 l1
3
1 4
11 0
56 0
4
0 2 2 0
0 31 0 1
0 33 0 1
0 35 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot ingredient3 shot4 right shaker1 l2 l1
3
1 4
11 0
56 0
4
0 2 2 0
0 5 0 1
0 7 -1 0
0 40 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot1 left shaker1 l2 l1
3
0 4
11 0
41 0
4
0 2 2 0
0 6 0 1
0 30 -1 0
0 38 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot2 left shaker1 l2 l1
3
0 4
11 0
41 0
4
0 2 2 0
0 9 0 1
0 12 0 1
0 54 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot3 left shaker1 l2 l1
3
0 4
11 0
41 0
4
0 2 2 0
0 31 0 1
0 33 0 1
0 48 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot4 left shaker1 l2 l1
3
0 4
11 0
41 0
4
0 2 2 0
0 5 0 1
0 40 0 1
0 44 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot1 right shaker1 l2 l1
3
1 4
11 0
41 0
4
0 2 2 0
0 6 0 1
0 30 -1 0
0 38 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot2 right shaker1 l2 l1
3
1 4
11 0
41 0
4
0 2 2 0
0 9 0 1
0 12 0 1
0 54 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot3 right shaker1 l2 l1
3
1 4
11 0
41 0
4
0 2 2 0
0 31 0 1
0 33 0 1
0 48 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail3 shot4 right shaker1 l2 l1
3
1 4
11 0
41 0
4
0 2 2 0
0 5 0 1
0 40 0 1
0 44 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot1 left shaker1 l2 l1
3
0 4
11 0
53 0
4
0 2 2 0
0 6 0 1
0 17 -1 0
0 38 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot2 left shaker1 l2 l1
3
0 4
11 0
53 0
4
0 2 2 0
0 9 0 1
0 12 0 1
0 27 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot3 left shaker1 l2 l1
3
0 4
11 0
53 0
4
0 2 2 0
0 22 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot4 left shaker1 l2 l1
3
0 4
11 0
53 0
4
0 2 2 0
0 5 0 1
0 20 -1 0
0 40 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot1 right shaker1 l2 l1
3
1 4
11 0
53 0
4
0 2 2 0
0 6 0 1
0 17 -1 0
0 38 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot2 right shaker1 l2 l1
3
1 4
11 0
53 0
4
0 2 2 0
0 9 0 1
0 12 0 1
0 27 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot3 right shaker1 l2 l1
3
1 4
11 0
53 0
4
0 2 2 0
0 22 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail1 shot4 right shaker1 l2 l1
3
1 4
11 0
53 0
4
0 2 2 0
0 5 0 1
0 20 -1 0
0 40 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot1 left shaker1 l2 l1
3
0 4
11 0
47 0
4
0 2 2 0
0 6 0 1
0 38 0 1
0 50 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot2 left shaker1 l2 l1
3
0 4
11 0
47 0
4
0 2 2 0
0 9 0 1
0 12 0 1
0 37 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot3 left shaker1 l2 l1
3
0 4
11 0
47 0
4
0 2 2 0
0 3 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot4 left shaker1 l2 l1
3
0 4
11 0
47 0
4
0 2 2 0
0 5 0 1
0 25 -1 0
0 40 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot1 right shaker1 l2 l1
3
1 4
11 0
47 0
4
0 2 2 0
0 6 0 1
0 38 0 1
0 50 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot2 right shaker1 l2 l1
3
1 4
11 0
47 0
4
0 2 2 0
0 9 0 1
0 12 0 1
0 37 -1 0
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot3 right shaker1 l2 l1
3
1 4
11 0
47 0
4
0 2 2 0
0 3 -1 0
0 31 0 1
0 33 0 1
1
end_operator
begin_operator
pour-shaker-to-shot cocktail2 shot4 right shaker1 l2 l1
3
1 4
11 0
47 0
4
0 2 2 0
0 5 0 1
0 25 -1 0
0 40 0 1
1
end_operator
begin_operator
empty-shot left shot1 cocktail3
1
0 2
2
0 6 -1 0
0 30 0 1
1
end_operator
begin_operator
empty-shot right shot1 cocktail3
1
1 5
2
0 6 -1 0
0 30 0 1
1
end_operator
begin_operator
empty-shot left shot2 cocktail3
1
0 3
2
0 12 -1 0
0 54 0 1
1
end_operator
begin_operator
empty-shot right shot2 cocktail3
1
1 1
2
0 12 -1 0
0 54 0 1
1
end_operator
begin_operator
empty-shot left shot3 cocktail3
1
0 0
2
0 33 -1 0
0 48 0 1
1
end_operator
begin_operator
empty-shot right shot3 cocktail3
1
1 3
2
0 33 -1 0
0 48 0 1
1
end_operator
begin_operator
empty-shot left shot4 cocktail3
1
0 1
2
0 5 -1 0
0 44 0 1
1
end_operator
begin_operator
empty-shot right shot4 cocktail3
1
1 2
2
0 5 -1 0
0 44 0 1
1
end_operator
begin_operator
empty-shot left shot1 cocktail1
1
0 2
2
0 6 -1 0
0 17 0 1
1
end_operator
begin_operator
empty-shot right shot1 cocktail1
1
1 5
2
0 6 -1 0
0 17 0 1
1
end_operator
begin_operator
empty-shot left shot2 cocktail1
1
0 3
2
0 12 -1 0
0 27 0 1
1
end_operator
begin_operator
empty-shot right shot2 cocktail1
1
1 1
2
0 12 -1 0
0 27 0 1
1
end_operator
begin_operator
empty-shot left shot3 cocktail1
1
0 0
2
0 22 0 1
0 33 -1 0
1
end_operator
begin_operator
empty-shot right shot3 cocktail1
1
1 3
2
0 22 0 1
0 33 -1 0
1
end_operator
begin_operator
empty-shot left shot4 cocktail1
1
0 1
2
0 5 -1 0
0 20 0 1
1
end_operator
begin_operator
empty-shot right shot4 cocktail1
1
1 2
2
0 5 -1 0
0 20 0 1
1
end_operator
begin_operator
empty-shot left shot1 cocktail2
1
0 2
2
0 6 -1 0
0 50 0 1
1
end_operator
begin_operator
empty-shot right shot1 cocktail2
1
1 5
2
0 6 -1 0
0 50 0 1
1
end_operator
begin_operator
empty-shot left shot2 cocktail2
1
0 3
2
0 12 -1 0
0 37 0 1
1
end_operator
begin_operator
empty-shot right shot2 cocktail2
1
1 1
2
0 12 -1 0
0 37 0 1
1
end_operator
begin_operator
empty-shot left shot3 cocktail2
1
0 0
2
0 3 0 1
0 33 -1 0
1
end_operator
begin_operator
empty-shot right shot3 cocktail2
1
1 3
2
0 3 0 1
0 33 -1 0
1
end_operator
begin_operator
empty-shot left shot4 cocktail2
1
0 1
2
0 5 -1 0
0 25 0 1
1
end_operator
begin_operator
empty-shot right shot4 cocktail2
1
1 2
2
0 5 -1 0
0 25 0 1
1
end_operator
0
