

(define (problem BW-rand-5)
(:domain blocksworld)
(:objects b1 b2 b3 b4 b5  - block)
(:init
(handempty)
(on b1 b2)
(on b2 b5)
(ontable b3)
(ontable b4)
(ontable b5)
(clear b1)
(clear b3)
(clear b4)
(= (total-cost) 0)
)
(:goal
(and
(on b2 b4)
(on b5 b3))
)
(:metric minimize (total-cost)))



