


(define (problem logistics-c4-s4-p4-a4)
(:domain logistics)
(:objects airplane0 airplane1 airplane2 airplane3  - airplane
          city0 city1 city2 city3  - city
          truck0 truck1 truck2 truck3  - truck
          loc0-0 loc1-0 loc2-0 loc3-0  - airport
          loc0-1 loc0-2 loc0-3 loc1-1 loc1-2 loc1-3 loc2-1 loc2-2 loc2-3 loc3-1 loc3-2 loc3-3  - location
          p0 p1 p2 p3  - package
)
(:init
(in-city  loc0-0 city0)
(in-city  loc0-1 city0)
(in-city  loc0-2 city0)
(in-city  loc0-3 city0)
(in-city  loc1-0 city1)
(in-city  loc1-1 city1)
(in-city  loc1-2 city1)
(in-city  loc1-3 city1)
(in-city  loc2-0 city2)
(in-city  loc2-1 city2)
(in-city  loc2-2 city2)
(in-city  loc2-3 city2)
(in-city  loc3-0 city3)
(in-city  loc3-1 city3)
(in-city  loc3-2 city3)
(in-city  loc3-3 city3)
(at truck0 loc0-3)
(at truck1 loc1-0)
(at truck2 loc2-1)
(at truck3 loc3-0)
(at p0 loc0-2)
(at p1 loc3-2)
(at p2 loc1-3)
(at p3 loc2-0)
(at airplane0 loc2-0)
(at airplane1 loc0-0)
(at airplane2 loc3-0)
(at airplane3 loc1-0)
(= (total-cost) 0)
)
(:goal
(and
(at p0 loc2-2)
(at p1 loc2-2)
(at p2 loc3-0)
(at p3 loc1-3)
)
)
(:metric minimize (total-cost))

)


