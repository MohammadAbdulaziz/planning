function DrawGraphs()

ipc2008_seq_opt_openstacks = [[31 37 43 40 55 61 67 73 70 91 103 115 ]
    [245 287 376 530 826 975 1207 1540 1660 2280 2782 3451 ]];
scatter(ipc2008_seq_opt_openstacks(1,:), ipc2008_seq_opt_openstacks(2,:), 'Marker', '+');
hold on;
IPC3_Driverlog  = [[34 41  41  51 59 78  84 97 114 119 176 195 197]
    [220 272  272  358 422 783  834 1536 2244  2793 6036 7173 8439]];
scatter(IPC3_Driverlog(1,:), IPC3_Driverlog(2,:), 'Marker', 'o');
hold on;


IPC5_TPP = [[10 18 26 34 56 99]
    [21 42 63 84 1435 18948]];
scatter(IPC5_TPP (1,:), IPC5_TPP (2,:), 'Marker', '*');
hold on;


IPC3_Zeno = [[18 40 46 52 52]
    [42 152 238 284 284]];
scatter(IPC3_Zeno (1,:), IPC3_Zeno (2,:), 'Marker', '.');
hold on;



    
IPC5_Rovers = [[16  40  24 54 25 27  199]
    [38  181  518 608 774 1734  3611]];
scatter(IPC5_Rovers (1,:), IPC5_Rovers (2,:), 'Marker', 's');
hold on;



ipc2011_Barman = [[133 133 133 171 171 171 213 213 291 291 291 291 343 343 343 343 441 441 441 441 505 505 617 617 617 617  691  373 373 373]
    [109 109 109 155 155 155 209 209 341 341 341 341 419 419 419 419 505 505 505 505 599 599 811 811 811 811  929  1055 1055 1055]];
scatter(ipc2011_Barman (1,:), ipc2011_Barman (2,:), 'Marker', 'd');
hold on;



AIPS98_Gripper = [[7 24 34 44 54 64 74 84 94 104 114 124 134 144 154 164 174 184]
    [11 232 492 848 1300 1848 2492 3232 4068 5000 6028 7152 8372 9688 11100 12608 14212 15912]];
scatter(AIPS98_Gripper (1,:), AIPS98_Gripper (2,:), 'Marker', '^');
hold on;


%IPC4_PSR = [[13 13 19 22 18 22 23 19 21 23 27 29 26 29 28 40]
%    [95 95 238 574 511 541 677 639 1279 1298 2638 2677 6143 7194 12287 33020 ]];
IPC4_PSR = [[13 13 19 22 18 22 23 19 21 23 27 29 26 29 28]
    [95 95 238 574 511 541 677 639 1279 1298 2638 2677 6143 7194 12287]];

scatter(IPC4_PSR (1,:), IPC4_PSR (2,:), 'Marker', 'v');
hold on;


ipc2011_seq_opt_Transport = [[72 72 56 90 104 104 126 150 166 176 193 142 132 ]
    [3952 3952 2394 6712 8390 8390 11688 15666 17904 20384 22932 15288 14390 ]];
scatter(ipc2011_seq_opt_Transport (1,:), ipc2011_seq_opt_Transport (2,:), 'Marker', '>');
hold on;



ipc2011_seq_opt_Elevators = [[343  77  86  89  73  93 86 136 85 100  154  109 140 103]
    [2967  3711  4170  2388  3956  3624 9065 2985 5439 5560  4530  4945 4948 7252]];
scatter(ipc2011_seq_opt_Elevators (1,:), ipc2011_seq_opt_Elevators (2,:), 'Marker', '<');
hold on;


% 
% 
  xlabel('Number of Boolean Variables');
  ylabel('Computed Bound');
  pbaspect([2 1 1]);
 %set(gca,'GTitle','Bound')
% 
 legend('location','NorthEast','OpenStacks', 'DriverLog', 'TPP', 'Zeno',  'Rovers' , 'Barman', 'Gripper', 'PSR', 'Transport', 'Elevator')
title('Upper Bounds in Planning Benchmarks');

 saveas(gcf, 'graph1.png');
 % 
clf;
 threshold = 5000;
 threshold2 = 200;
% a=[1,2,3,4,5];
% 
% a(3) = [];

k = 1;
for i = 1 : length (ipc2011_seq_opt_Elevators(1,:))
     if(ipc2011_seq_opt_Elevators(2,(i)) <= threshold && ipc2011_seq_opt_Elevators(1,(i)) <= threshold2 )
       histdata(k) = ipc2011_seq_opt_Elevators(1,(i));
     end
     k = k + 1;
end
for i = 1 : length (IPC3_Driverlog  (1,:))
     if(IPC3_Driverlog  (2,(i)) <= threshold && IPC3_Driverlog  (1,(i)) <= threshold2)
       histdata(k) = IPC3_Driverlog  (1,(i));
     end
     k = k + 1;
end
for i = 1 : length (IPC5_TPP   (1,:))
     if(IPC5_TPP  (2,(i)) <= threshold && IPC5_TPP  (1,(i)) <= threshold2)
       histdata(k) = IPC5_TPP (1,(i));
     end
     k = k + 1;
end
for i = 1 : length (IPC3_Zeno (1,:))
     if(IPC3_Zeno (2,(i)) <= threshold && IPC3_Zeno (1,(i)) <= threshold2 )
       histdata(k) = IPC3_Zeno (1,(i));
     end
     k = k + 1;
end
for i = 1 : length (IPC5_Rovers (1,:))
     if(IPC5_Rovers (2,(i)) <= threshold && IPC5_Rovers (1,(i)) <= threshold2)
       histdata(k) = IPC5_Rovers (1,(i));
     end
     k = k + 1;
end
for i = 1 : length (ipc2011_Barman (1,:))
     if(ipc2011_Barman (2,(i)) <= threshold && ipc2011_Barman (1,(i)) <= threshold2)
       histdata(k) = ipc2011_Barman (1,(i));
     end
     k = k + 1;
end
for i = 1 : length (AIPS98_Gripper (1,:))
     if(AIPS98_Gripper (2,(i)) <= threshold && AIPS98_Gripper (1,(i)) <= threshold2)
       histdata(k) = AIPS98_Gripper  (1,(i));
     end
     k = k + 1;
end
for i = 1 : length (IPC4_PSR (1,:))
     if(IPC4_PSR (2,(i)) <= threshold && IPC4_PSR (1,(i)) <= threshold2 )
       histdata(k) = IPC4_PSR (1,(i));
     end
     k = k + 1;
end
for i = 1 : length (ipc2011_seq_opt_Transport (1,:))
     if(ipc2011_seq_opt_Transport (2,(i)) <= threshold && ipc2011_seq_opt_Transport (1,(i)) <= threshold2)
       histdata(k) = ipc2011_seq_opt_Transport (1,(i));
     end
     k = k + 1;
end
for i = 1 : length (ipc2011_seq_opt_Elevators(1,:))
     if(ipc2011_seq_opt_Elevators(2,(i)) <= threshold && ipc2011_seq_opt_Elevators(1,(i)) <= threshold2)
       histdata(k) = ipc2011_seq_opt_Elevators(1,(i));
     end
     k = k + 1;
end
hist(histdata,10);
xlabel('Number of Boolean Variables');
ylabel('Instances with Practical Bound');
saveas(gcf, 'hist.png');



end