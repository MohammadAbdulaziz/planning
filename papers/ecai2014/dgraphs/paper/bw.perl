#!/usr/bin/perl

use strict;
use warnings;
use GD;
use Getopt::Long;


my $in_file   = "foo.png";
my $result = GetOptions ("file=s"   => \$in_file); #string


#my $in_file = qw[foo.png];
my $ni_file = "bw".$in_file;
my $image = GD::Image->new($in_file);

my $i = 0;
my $t = $image->colorsTotal();

while($i < $t) {
    my( @c ) = $image->rgb( $i );
    

    my $g = ( $c[0] + $c[1]  + $c[2]   )  / 3; # Color::Calc::grey

    print $g ;
    print " " ;
    print $c[0] ; 
    print " " ;
    print $c[1] ;
    print " " ;
    print $c[2];
    print "\n" ;


    # if ( ( $c[0] == 4 )  && ( $c[1] == 2 )  && ( $c[2] == 4 ) ){
	
    # 	$g = 55 ; 

    # } els
	if ($c[0] < 250){
	$g = 0;
	
    } 
    elsif ($c[1] < 250){
	$g = 0;
	
    }
    elsif ($c[2] < 250){
	$g = 0;
	
    } else {
	$g = 255;
    }

    # if( $g > 85) { #0x7F ) {
    # 	$g =  0; #255;
    # } if( $g < 5) { #0x7F ) {
    # 	$g =  100; #255;
    # } elsif ( $g < 250 )  {
    # 	$g =  0; #255;
    # } else {
    # 	$g = 50; #255 ;
    # }

    $image->colorDeallocate($i);
    $image->colorAllocate( $g, $g, $g );
    $i++;
}

write_file( $ni_file, $image->png );
sub write_file {
    my $i = shift;
    open DISPLAY, ">$i" or die "can't clobber $i $!";
    binmode DISPLAY;
    print DISPLAY @_;
    close DISPLAY;
}


