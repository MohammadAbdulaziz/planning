#!/bin/bash

pack_names=`cat illegal_packages.txt`

for pack in $pack_names; do
    cat paper.tex | grep -v $pack > paper_tmp
    mv paper_tmp paper.tex;
done
