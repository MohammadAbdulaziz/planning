#!/bin/bash
cd ../
../../HOL/bin/Holmake
cd final_version/
#rm sources/*.tex
rm *.png
rm *.sty
rm *.tex
projDir=../../../
cp $projDir/papers/latexIncludes/lotus.pdf .
cp $projDir/papers/latexIncludes/mathlbreak.tex .
cp $projDir/papers/latexIncludes/macros.tex .
cp $projDir/papers/latexIncludes/aaai.bst .
cp $projDir/papers/latexIncludes/aaai19.sty .
cp $projDir/papers/latexIncludes/paper.bib .
cp $projDir/papers/SODA/*.tex .
cp $projDir/papers/diameterUpperBounding/experiments/plots/log-labelled-Nalgo-acycNalgo-SASSCCs-TD-vs-Nalgo-acycNalgo-SASSCCs-bounds.png .
cp $projDir/papers/diameterUpperBounding/experiments/plots/log-labelled-Balgo-acycNalgo-SASSCCs-TD-vs-Balgo-acycNalgo-SASSCCs-bounds.png .
cp $projDir/papers/diameterUpperBounding/*.tex .
#cp $projDir/papers/diameterUpperBounding/*.png .
cp $projDir/papers/sspaceAcyclicity/*.tex .
cp $projDir/papers/TD/*.tex .
cp $projDir/papers/TD/*.png .
#Flatten
perl latexpand/latexpand TD.tex > out
rm *.tex
rm *~
mv out paper.tex
mkdir sources
# ./aaai_script.sh paper.tex
# cp sources/*.sty .
rm -rf sources
#rm *.txt
latexmk -f -gg -pdf paper.tex

## Make sure that the produced pdf is correct
# pdftotext paper.pdf
# pdftotext ../TD.pdf
# DIFF=$(diff ../TD.txt paper.txt) 
# if [ "$DIFF" != "" ] 
# then
#     echo "The pdf is not exactly as it should be. Check diff.txt."
#     echo $DIFF > diff.txt
#     exit
# fi

rm paper.pdf
pdflatex paper.tex
bibtex paper
pdflatex paper.tex
pdflatex paper.tex
#rm *.pdf
rm *.log
rm testOnly
rm eshell
rm __tmp*
rm aaai17.sty aaai.dot aaai.sty *.out IEEEtran.cls
rm *.idf *.mf *.log *.fd *.tfm *pk *.idx *.def *.out
rm *.pfb *.clo *.eps JAR_edit_report* *.ins flatten *.map *.dtx *.fdb_latexmk *.afm *.fls *.xml *.gf *diff* *.dtx README *.blg *. *.aux 
./remove_unused_macros.sh
./remove_illegal_packages.sh
./replace_subfigure_labels.sh

rm -rf AAAI-AbdulazizM.7899
mkdir AAAI-AbdulazizM.7899
cp *.tex *.png *.bib lotus.pdf AAAI-AbdulazizM.7899
mv AAAI-AbdulazizM.7899/paper.tex AAAI-AbdulazizM.7899/AAAI-AbdulazizM.7899.tex
zip -r AAAI-AbdulazizM.7899.zip AAAI-AbdulazizM.7899/
