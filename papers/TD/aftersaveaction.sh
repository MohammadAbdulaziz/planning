#!/bin/bash

if `ps -e | grep -q Holmake`; then
  exit
fi
rm TD.pdf
../../HOL/bin/Holmake
pdftk TD.pdf cat 1-8 output TD_paper.pdf
pdftk TD.pdf cat 9-10 output TD_additional.pdf
