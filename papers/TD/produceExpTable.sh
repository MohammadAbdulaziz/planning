#!/bin/bash


compare_bounds(){
# Usage: call this script and give it two algorithm names, a third optional argument which is the domain name, a fourth optional argument which is the directory where the exp data is
# Note that all data has to first be put in exp_results using the script rename_exp_data.sh
# Example usage:
# ./compare_bounds.sh Balgo.acycNalgo.SASSCCs.TD Balgo.acycNalgo.SASSCCs [newopen] [directory_of_exp_results]
leProbs=0
halfLeProbs=0
bound1Probs=0
bound2Probs=0
for file in `find $4 -iname "*$3*.algo.$1.bound.out"`; do
  algo1Bound=`awk '{print $1}' $file`
  algo2Bound=`awk '{print $1}' ${file/$1/$2}`
  if [[ $algo1Bound -lt 0 ]]; then
    algo1Bound=1000000000
  fi
  if [[ $algo2Bound -lt 0 ]]; then
    algo2Bound=1000000000
  fi
  if [[ $algo1Bound -lt 1000000000 ]]; then
    bound1Probs=`echo "$bound1Probs + 1" | bc`
  fi;
  if [[ $algo2Bound -lt 1000000000 ]]; then
    bound2Probs=`echo "$bound2Probs + 1" | bc`
  fi;


  if [[ $algo1Bound -lt $algo2Bound && $algo2Bound -lt 1000000000 ]]; then
    # echo -n ${file/exp_results//};
    # echo -n " "
    ratio=$(echo "`awk '{print $1}' $file`/`awk '{print $1}' ${file/$1/$2}`" | bc -l)
    if (( `echo $ratio '>=' 0.5 | bc -l `)); then
      halfLeProbs=`echo "$halfLeProbs + 1" | bc`
    fi
    # echo "`awk '{print $1}' $file`/`awk '{print $1}' ${file/$1/$2}`";
    leProbs=`echo "$leProbs + 1" | bc`
  fi;
done

echo -n "       &{\tiny $bound2Probs} &{\tiny $bound1Probs} &{\tiny $leProbs} &{\tiny $halfLeProbs}"

}


# This is used for TD in AAAI
produce_improvement_table () {
declare -A ALGNAMES
ALGNAMES[Malgo.noSASSCCs]=Msum
ALGNAMES[Nalgo.noSASSCCs]=Nsum
ALGNAMES[Malgo.noSASSCCs.noOverflow]=Msum
ALGNAMES[Nalgo.noSASSCCs.noOverflow]=Nsum
ALGNAMES[Salgo.noSASSCCs]=Smax
ALGNAMES[Balgo.noSASSCCs]=HYB
ALGNAMES[Nalgo.SASSCCs]="Nsum<EXP>"
ALGNAMES[Balgo.SASSCCs]="HYB<EXP>"
ALGNAMES[Nalgo.acycNalgo.SASSCCs]="Nsum<EXP>"
ALGNAMES[Balgo.acycNalgo.SASSCCs]="HYB<EXP>"
ALGNAMES[Nalgo.acycNalgo.noSASSCCs]="Nsum<EXP>"
ALGNAMES[Balgo.acycNalgo.noSASSCCs]="HYB<EXP>"
ALGNAMES[Nalgo.acycNalgo.SASSCCs.TD]="Nsum<ARB>"
ALGNAMES[Balgo.acycNalgo.SASSCCs.TD]="HYB<ARB>"
ALGNAMES[Nalgo.acycNalgo.noSASSCCs.TD]="Nsum<ARB>"
ALGNAMES[Balgo.acycNalgo.noSASSCCs.TD]="HYB<ARB>"
ALGNAMES[TD]="ARB"

declare -A CUTOFF
CUTOFF[Malgo.noSASSCCs]=2000000000
CUTOFF[Nalgo.noSASSCCs]=2000000000
CUTOFF[Malgo.noSASSCCs.noOverflow]=2000000000
CUTOFF[Nalgo.noSASSCCs.noOverflow]=2000000000
CUTOFF[Salgo.noSASSCCs]=800000000
CUTOFF[Balgo.noSASSCCs]=800000000
CUTOFF[Nalgo.SASSCCs]=2000000000
CUTOFF[Balgo.SASSCCs]=800000000
CUTOFF[Nalgo.acycNalgo.SASSCCs]=2000000000
CUTOFF[Balgo.acycNalgo.SASSCCs]=800000000
CUTOFF[Nalgo.acycNalgo.noSASSCCs]=2000000000
CUTOFF[Balgo.acycNalgo.noSASSCCs]=800000000
CUTOFF[Nalgo.acycNalgo.SASSCCs.TD]=1400000000
CUTOFF[Balgo.acycNalgo.SASSCCs.TD]=400000000
CUTOFF[Nalgo.acycNalgo.noSASSCCs.TD]=1400000000
CUTOFF[Balgo.acycNalgo.noSASSCCs.TD]=400000000
CUTOFF[TD]=10000000000
AlgosInTable="Nalgo.acycNalgo.SASSCCs Balgo.acycNalgo.SASSCCs"
echo "\begin{table}">> experiments_table.tex
echo "\begin{center}">> experiments_table.tex
echo "    \begin{tabularx}{0.45\textwidth}{| l | X | X | X |}">> experiments_table.tex
echo "    %p{5cm} |}">> experiments_table.tex
echo "    \hline">> experiments_table.tex
echo -n "          {\tiny }">> experiments_table.tex
echo -n "       &{\tiny ${ALGNAMES[$algo]}}" >> experiments_table.tex
for algo in $AlgosInTable; do
  echo -n "       &{\tiny ${ALGNAMES[$algo]}}" >> experiments_table.tex
done
echo -n "\\\\">> experiments_table.tex
echo " " >>experiments_table.tex
for dom in `cat ../diameterUpperBounding/experiments/plots/dom_keywords_no_IPC_no_SATUNSAT`; do
  echo $dom
  for algo in $AlgosInTable; do
    cat ../diameterUpperBounding/experiments/plots/${dom}_bounds_${algo}_assorted | awk '{print $2}' | sort -n > ${dom}_bounds_${algo}
  done
done
for dom in `cat ../diameterUpperBounding/experiments/plots/dom_keywords_no_IPC_no_SATUNSAT`; do
  echo -n "{\tiny ">> experiments_table.tex
  echo -n  ${dom} >> experiments_table.tex
  numInsts=`find ../diameterUpperBounding/experiments/exp_results -iname "*${dom}*.algo.${algo}.bound.out" | wc -l`
  echo -n "($numInsts)" >> experiments_table.tex
  echo -n "}" >> experiments_table.tex
  numTDInsts=`cat ../diameterUpperBounding/experiments/plots/${dom}_bounds_TD |  awk '($1 < 1000000000)' | wc -l`      
  echo -n "       &{\tiny " >> experiments_table.tex
  echo -n $numTDInsts >> experiments_table.tex
  echo -n "}" >> experiments_table.tex
  for algo in $AlgosInTable; do
    compare_bounds ${algo}.TD ${algo} ${dom} ../diameterUpperBounding/experiments/exp_results >> experiments_table.tex
  done
  echo -n "\\\\ ">> experiments_table.tex
  echo " " >> experiments_table.tex
done
}

produce_improvement_table
