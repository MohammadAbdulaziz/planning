% Copyright 2007 by Till Tantau
%
% This file may be distributed and/or modified
%
% 1. under the LaTeX Project Public License and/or
% 2. under the GNU Public License.
%
% See the file doc/licenses/LICENSE for more details.



\documentclass[t]{beamer}

%
% DO NOT USE THIS FILE AS A TEMPLATE FOR YOUR OWN TALKS¡!!
%
% Use a file in the directory solutions instead.
% They are much better suited.
%


% Setup appearance:
\useoutertheme{infolines}

%\usetheme{Darmstadt}
%\usetheme[sidebar=false]{NICTA}
\setbeamertemplate{navigation symbols}{}
%% \usefonttheme[onlylarge]{structurebold}
%% \setbeamerfont*{frametitle}{size=\normalsize,series=\bfseries}
%% \setbeamertemplate{navigation symbols}{}


% Standard packages
\usepackage[ruled,noline,noalgohanging]{algorithm2e}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}

\usepackage[makeroom]{cancel}

\usepackage{tikz}
\usetikzlibrary{arrows,positioning,decorations.markings}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{tabularx}
\captionsetup{compatibility=false}

\newtheorem{mylem}{Lemma}
\newtheorem{mythm}{HOL4 Theorem}
\newtheorem{mydef}{HOL4 Definition}
\newtheorem{myeg}{Example}

%\usepackage{holtexbasic}

%% \usepackage{alltt}
%% \usepackage{enumitem}
\usepackage[Euler]{upgreek}
\usepackage{xcolor,colortbl}
\renewcommand{\Pi}{\Uppi}

\newcommand{\vs}{\ensuremath{\mathit{vs}}}
%\newcommand{\subscriptsublist}{\preceq}

\newcommand{\pp}[1]{\text{\em #1}}
\newcommand{\lopt}{\textcolor[gray]{0.2}{[}}
\newcommand{\ropt}{\textcolor[gray]{0.2}{]}}
\usepackage[llparenthesis,rrparenthesis]{stmaryrd}
\newcolumntype{r}{>{\columncolor{red}}c}
\newcolumntype{g}{>{\columncolor{green}}c}
\input{../../latexIncludes/macros.tex}
%% \renewcommand{\HOLConst}[1]{\textsf{#1}}
%% \renewcommand{\HOLTyOp}[1]{\mbox{\fontencoding{T1}\fontfamily{bch}\fontseries{m}\fontshape{it}\fontsize{9.5}{10}\selectfont #1}}
%% \newcommand{\etc}{\textit{etc.}}
%% \newcommand{\ie}{\textit{i.e.}}

%% \usepackage{color}
%% \newcommand{\charles}[1]{\textcolor{red}{Charles: #1}}
%% \newcommand{\michael}[1]{\textcolor{blue}{Michael: #1}}
%% \newcommand{\calA}{\ensuremath{\mathbf{\cal A}}}

% Author, Title, etc.
\title[Plan-Length Bounds: Beyond 1-way Dependency]
% (optional, use only with long paper titles)
{Plan-Length Bounds: Beyond 1-way Dependency}
\author[Mohammad Abdulaziz]{Mohammad Abdulaziz
\\ Technical University of Munich}

\tikzset{
  textnode/.style={},
  varnode2/.style={draw},
  varnode2green/.style={draw,fill=green},
  varnode2red/.style={draw,fill=red},
  varnode2blue/.style={draw,fill=blue},
  varnode/.style={rectangle,draw},
  varnodegreen/.style={rectangle,draw,fill=green},
  varnodered/.style={rectangle,draw,fill=red},
  varnodeblue/.style={rectangle,draw,fill=blue},
  varnodepurple/.style={rectangle,draw,fill=purple},
  varnodeyellow/.style={rectangle,draw,fill=yellow},
  varnodeempty/.style={inner sep=0pt,fill},
  line/.style={=stealth,thick,fill=red},
  ourarrow/.style={>=stealth,thick,fill=red}
}


\setbeamertemplate{itemize/enumerate body begin}{\large}
\setbeamertemplate{itemize/enumerate subbody begin}{\large}
\setbeamertemplate{itemize/enumerate subsubbody begin}{\large}
\setbeamertemplate{itemize/enumerate subsubsubbody begin}{\large}
% The main document

\date{}
\begin{document}


\begin{frame}[t]
  \titlepage
\end{frame}


\begin{frame}[t]{Outline}
\begin{itemize}
  \item Background: what do we upper bound?
  \item Existing compositional bounding algorithms
  \begin{itemize}  
    \item Dependency, projection
    \item Problem: only work with acyclic dependencies
  \end{itemize}
  \item Bounding for strongly connected dependencies
\end{itemize}
\end{frame}

\begin{frame}{Propositionally Factored Transition Systems}
\only<1>{
    \begin{itemize}
      \item System $\delta$: a finite set of actions e.g. 
      \input{delta}
      \item Succinct representation of a state space (like STRIPS and SMV)
   \end{itemize}
   \input{deltaSspace}}
\end{frame}



\begin{frame}{Upper bounding state space topological properties}
\only<1>{
\begin{itemize}
      \item We upper bound state space topological properties
      \begin{itemize}
        \item e.g. diameter, recurrence diameter
      \end{itemize}
      \item Those properties are upper bounds on plan lengths
      \item For completeness of SAT planning and bounded model checking
\end{itemize}}
      \only<2>{
        Diameter: length of the longest shortest transition sequence between any two states
\input{fourClique}
diameter=?
}
      \only<3>{
        Diameter: length of the longest shortest transition sequence between any two states
\input{fourClique}
diameter=1
}
      \only<4>{
Recurrence diameter: length of the longest simple path
\input{fourClique}
recurrence diameter=?
}      \only<5>{
Recurrence diameter: length of the longest simple path
\input{fourClique}
recurrence diameter=3
}
\end{frame}

\begin{frame}{Diameter Upper Bounding}
  Challenge:
    \begin{itemize}
      \item State space can be exponentially larger than factored system
      \item Exponential blow in complexity from explicit to succinct
      \begin{itemize}
        \item Diameter:  $P$ $\Rightarrow$ $\Pi_2^P$-complete [Hemaspaandra 2010]
        \item Recurrence diameter: $NP$-hard $\Rightarrow$ $NEXP$-hard [Papadimitriou 1986]
      \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Solution: Compositional Algorithms}
    \begin{itemize}
      \item Compute upper bounds on abstractions' diameters
      \item Compose abstractions' bounds to approximate the concrete diameter
      \item<2-3> Why? abstraction state space exponentially smaller than input system's state space
      \item<3> Here we focus on projections
    \end{itemize}
\end{frame}

%% \begin{frame}{Compositional Algorithms}
%% \begin{itemize}
%%   \item Challenge: obtaining ``sound'' abstractions
%%   \begin{itemize}
%%     \item can we compose abstraction (recurrence) diameters?
%%     \item here we focus on projections
%%   \end{itemize}
%%   \item Solution: structure, abstraction, compositional algorithm
%%   \begin{itemize}
%%      \item the structure that can be tested automatically
%%      \item abstractions are computed automatically based on the structure
%%      \item prove the structure guarantees soundness of compositional algorithm
%%   \end{itemize}
%% \end{itemize}
%% \end{frame}
 
\begin{frame}{Previous Work: Projections and Acyclic Dependency}
\begin{itemize}
\item<only@1-.(2)>{
    Projection is a system over-approx.}
\item<only@1-.(2)>{
    Based on removing state variables from an object}
   \only<1>{
     \\ E.g. $\delta$
      \input{delta}
\input{deltaSspace}}
   \only<2>{
\ \\      E.g. $\proj{\delta}{\{\vara,\varb\}}$
      \input{deltaProj}
      \input{deltaProjSspace}}
\item<only@.(3)>{Unfortunately, in general, projections' diameters cannot be composed to upper bound the diameter [Abdulaziz 2017]}
\item<only@.(3)>{The same applies to the recurrence diameter}
\item<only@.(4)>{Natural idea: what if we constrain the system structure by dependency?}
\item<only@.(4)>{Many previous compositional planning techs used it
  \begin{itemize}
    \item e.g. Knoblock 1994, Williams and Nayak 1997
  \end{itemize}}
\item<only@.(5)>{A variable $v_i$ depends on $v_j$ iff a change in $v_j$ assignment can possibly influence the assignment of $v_i$}
 \only<5>{
      \input{delta}
\begin{figure}
\begin{subfigure}[b]{0.5\textwidth}
\centering
\begin{tikzpicture}
\node (v1) at (0,1)[varnode] {\footnotesize $\vara$ } ;
\node (v2) at (1,1)  [varnode] {\footnotesize $\varb$ } ;
\node (v3) at (0,0)  [varnode] {\footnotesize $\varc$ } ;
\node (v4) at (1,0)  [varnode] {\footnotesize $\vard$ } ;
\draw [->,ourarrow] (v1) -- (v2) ;
\draw [->,ourarrow] (v2) -- (v1) ;
\draw [->,ourarrow] (v1) -- (v3) ;
\draw [->,ourarrow] (v1) -- (v4) ;
\draw [->,ourarrow] (v2) -- (v3) ;
\draw [->,ourarrow] (v2) -- (v4) ;
\draw [->,ourarrow] (v3) -- (v4) ;
\draw [->,ourarrow] (v4) -- (v3) ;
\end{tikzpicture}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
\centering
\begin{tikzpicture}
\node (v12) at (0,1)[varnode] {\footnotesize $\{\vara,\varb\}$ } ;
\node (v34) at (0,0)  [varnode] {\footnotesize $\{\varc,\vard\}$ } ;
\draw [->,ourarrow] (v12) -- (v34) ;
\end{tikzpicture}
\end{subfigure}
\end{figure}}
\item<only@6-.(7)>{Unfortunately, no dependency structure guarantees sound composition of projections' diameters to bound the diameter  [Abdulaziz 2017]}
\item<only@6-.(7)>{The same for the recurrence diameter}
\item<only@.(6)>{But...}
\item<only@7-.(9)>{Acyclic dependencies allow diameter to be bounded by projections' recurrence diameters}
\item<only@8-.(9)>{Main idea of Baumgartner et al. 2003, Rintanen et al. 2013, and Abdulaziz et al. 2015}
\only<8>{\begin{itemize}
           \item Compute SCCs of the dependency graph
           \item Compute projections on SCCs
           \item Compute the recurrence diameters of projections
           \item Compose the recurrence diameters to upper bound the diameter
         \end{itemize}}
\item<only@.(9)>{This is the only ``reasonable`` projection based compositional bounding}
\item<only@.(9)>{What can be done about cyclic dependencies?}
\end{itemize}
\end{frame}

\begin{frame}{Traversal Diameter}
 \begin{itemize}
   \item<only@1-.(8)>{
    The traversal diameter is one less than the largest number of states that could be traversed by any path.}
     \only<1>{
      \ \\E.g. $\proj{\delta}{\{\vara,\varb\}}$
      \input{fourFlower}
      Diameter = ?
}
     \only<2>{
      \ \\E.g. $\proj{\delta}{\{\vara,\varb\}}$
      \input{fourFlower}
      Diameter = 2
}
     \only<3>{
      \ \\E.g. $\proj{\delta}{\{\vara,\varb\}}$
      \input{fourFlower}
      Recurrence diameter = ?
}
     \only<4>{
      \ \\E.g. $\proj{\delta}{\{\vara,\varb\}}$
      \input{fourFlower}
      Recurrence diameter = 2
}
     \only<5>{
      \ \\E.g. $\proj{\delta}{\{\vara,\varb\}}$
      \input{fourFlower}
      Traversal diameter = ?
}
     \only<6>{
      \ \\E.g. $\proj{\delta}{\{\vara,\varb\}}$
      \input{fourFlower}
      Traversal diameter = 3
}
   \item<only@7-.(8)>{
    It has the advantage of being bounded by the product of projections' traversal diameters.}
   \only<7>{
   \begin{itemize}
     \item Projections can be on any state variables partition
     \item Compositional bounding not contrained to acyclic deps! 
   \end{itemize}}
   \item<only@.(8)>{
    Another advantage: it can be computed in linear time in the state space.}
   \only<8>{
   \begin{itemize}
     \item Much better than the diameter and the recurrence diameter
   \end{itemize}}
 \end{itemize}
\end{frame}

\begin{frame}[t]{Bounding with the Traversal Diameter}
\only<1-3>{
\begin{itemize}
  \item Bounding a concrete system
  \only<1>{\begin{itemize}
    \item Get a partition of the state variables
    \begin{itemize}
      \item from Fast-Downward [Helmert 2006]
    \end{itemize}
    \item Project the given system on each equivalence class of variables
    \item Compute the traversal diameter of every projection
    \item The bound is the product of the projections' traversal diameters
  \end{itemize}}
\end{itemize}}
\only<2>{
\begin{itemize}
  \item Problem: the product of projections' traversal diameters is large!
\end{itemize}}
\only<3>{
\begin{itemize}
  \item Compared to the algorithm HYB from Abdulaziz et al. 2017
\end{itemize}
 \begin{tabularx}{\textwidth}{| X | X | X|}
    %p{5cm} |}
    \hline
          {\small Domain(\#inst.)}       &{\small ARB}        &{\small HYB}\\
\hline 
{\small logistics(407)}       &{\small 51}              &{\small 407}\\  
{\small elevators(210)}       &{\small 67}              &{\small 162}\\  
{\small rover(182)}       &{\small 28}              &{\small 176}\\  
{\small nomystery(124)}       &{\small 28}              &{\small 124}\\  
{\small zeno(50)}       &{\small 17}              &{\small 50}\\  
{\small TPP(120)}       &{\small 11}              &{\small 107}\\  
{\small Transport(197)}       &{\small 9}              &{\small 45}\\  
{\small openstacks(131)}       &{\small 8}             &{\small 116}\\  
{\small hyp(286)}       &{\small 1}              &{\small 285}\\  
{\small gripper(54)}       &{\small 7}              &{\small 39}\\  
{\small parcprinter(60)}       &{\small 0}              &{\small 27}\\  
{\small pipesworld(101)}       &{\small 2}              &{\small 55}\\  
         \hline
    \end{tabularx}
}
\only<4-6>{
\begin{itemize}
  \item Bound abstractions that HYB can no longer decompose
  \item<only@.(4)> Instead of using $2^N$
\only<5>{\begin{itemize}
    \item Get a partition of the state variables
    \begin{itemize}
      \item from Fast-Downward [Helmert 2006]
    \end{itemize}
    \item Project the given system on each equivalence class of variables
    \item Compute the traversal diameter of every projection
    \item The bound is the product of the projections' traversal diameters
  \end{itemize}}
\end{itemize}}
\only<6>{
\begin{itemize}
  \item Compared to the algorithm HYB from Abdulaziz et al. 2017
\end{itemize}
 \begin{tabularx}{1\textwidth}{| l | X | X | X |}
    %p{5cm} |}
    \hline
          {\small Domain(\#inst.)}       &{\small HYB} &{\small HYB with ARB} &{\small 50\% tighter}\\
\hline 
{\small logistics(407)}              &{\small 407} &{\small 407}  &{\small 365}\\  
{\small elevators(210)}              &{\small 162} &{\small 163}  &{\small 162}\\  
{\small rover(182)}                  &{\small 176} &{\small 177}  &{\small 6}\\  
{\small nomystery(124)}              &{\small 124} &{\small 124}  &{\small 124}\\  
{\small zeno(50)}                    &{\small 50} &{\small 50}  &{\small 50}\\  
{\small TPP(120)}                    &{\small 107} &{\small 108}  &{\small 19}\\  
{\small Transport(197)}              &{\small 45} &{\small 44}  &{\small 13}\\  
{\small openstacks(131)}             &{\small 116} &{\small 116}  &{\small 6}\\  
{\small hyp(286)}                     &{\small 285} &{\small 285} &{\small 33}\\  
{\small gripper(54)}                  &{\small 39} &{\small 39}  &{\small 7}\\  
{\small parcprinter(60)}              &{\small 27} &{\small 27}  &{\small 3}\\  
{\small pipesworld(101)}              &{\small 55} &{\small 53} &{\small 2}\\  
         \hline
    \end{tabularx}
}
\end{frame}

\begin{frame}[t]{Conclusions}
  \begin{itemize}
    \item New compositional algorithm for upper bounding diameter
    \begin{itemize}
      \item Based on ``traversal diameter``, a new topological property
      \item Decomposes systems with strongly connected dependencies
    \end{itemize}
    \item Works mainly in combination with existing bounding algorithms
    \item Remains to be resolved:
    \begin{itemize}
      \item How to decompose concrete systems with strongly connected dependencies
    \end{itemize}
  \end{itemize}
\end{frame}

\end{document}
