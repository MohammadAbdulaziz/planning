#!/bin/bash


function findPath()
{
  local old_IFS=$IFS
  IFS=':';
  found=0;
  file=$1
  currentdir=${PWD##*/}
  for dir in `awk '{print $3}' latexmkrc | cut -d "'" -f 2`; do
    if [ -f $dir/${file}.htex ]; then
      # if [ $found -eq "1" ]; then
      #   echo "$file found";
      # fi
      # echo $dir/${file}.htex
      # echo $dir
      if [ "$dir" = . ]; then
        dir=../$currentdir;
      fi
      sed -i 's@input{'"$file"'}@input{'"$dir"'/'"$file"'}@g' *.htex;
      found=1;
      break;
    fi
  done
  if [ $found -eq "0" ]; then
    echo "$file not found";
  fi
  IFS=$old_IFS
}


# findPath $1

IFS=$'\n';
for file in `grep input{ *.htex | grep -oP '(?<={).*?(?=})' | grep -v %`; do
  findPath $file
done



