#!/bin/sh
cd ../../../codeBase/Madagascar/
if [ ! -f "../../papers/sspaceAcyclicity/experiments/exp_results/$outName.M.plan.nobound.out" ]; then
  ./M -m 100000 $domName $probName >../../papers/sspaceAcyclicity/experiments/exp_results/$outName.M.plan.nobound.out 2>../../papers/sspaceAcyclicity/experiments/exp_results/$outName.M.plan.nobound.err
fi
