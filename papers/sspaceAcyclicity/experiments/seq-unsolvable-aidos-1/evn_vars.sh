#!/bin/sh
module load python
module load cmake
module load cplex/11.1
module unload intel-cc/14.0.3.174
module unload gcc
module unload intel-fc/14.0.3.174
module unload intel-mkl/11.1.3.174 
#replace compilerin builds/aidos_ipc/CMakeCache.txt

module load gcc
module load maker/2.31.8
export DOWNWARD_CPLEX_ROOT=/apps/cplex/12.5/cplex
export DOWNWARD_COIN_ROOT=/home/abd020/coin

