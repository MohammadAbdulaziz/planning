#!/bin/sh

module load cplex/12.5
export DOWNWARD_CPLEX_ROOT=/apps/cplex/12.5/cplex/
export DOWNWARD_COIN_ROOT=~/coin/
cd seq-unsolvable-aidos-1/
if [ ! -f "../exp_results/$outName.FDunsat.plan.out" ]; then
  ./plan $domName $probName >../../../../papers/sspaceAcyclicity/experiments/exp_results/$outName.FDunsat.plan.out 2>../../../../papers/sspaceAcyclicity/experiments/exp_results/$outName.FDunsat.plan.err
  cp planner.log ../exp_results/$outName.FDunsat.planner.log
  cp planner.err ../exp_results/$outName.FDunsat.planner.err
fi
