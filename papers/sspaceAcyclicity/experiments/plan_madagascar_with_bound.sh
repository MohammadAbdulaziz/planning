#!/bin/sh

cd ../../../codeBase/Madagascar/
echo $outName
modified_outName=`echo $outName | sed 's/_aux//g'`
echo $modified_outName
boundval=`cat ../../papers/sspaceAcyclicity/experiments/exp_results/$modified_outName.bound.out |  awk '{print $1}'`
echo $boundval
if echo $boundval | egrep -q '^[0-9]+$'; then #boundval is anumber
  if [ $boundval -gt 0 ]; then 
    if [ ! -f "../../papers/sspaceAcyclicity/experiments/exp_results/$outName.M.plan.withbound.out" ]; then
      ./M -m 100000 -F $boundval -T $boundval $domName $probName >../../papers/sspaceAcyclicity/experiments/exp_results/$outName.M.plan.withbound.out 2>../../papers/sspaceAcyclicity/experiments/exp_results/$outName.M.plan.withbound.err
    fi
  fi
fi
