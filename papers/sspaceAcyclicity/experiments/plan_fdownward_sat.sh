#!/bin/sh
cd ../../../codeBase/planning/FD/src/
if [ ! -f "../../../../papers/sspaceAcyclicity/experiments/exp_results/$outName.FDsat.plan.out" ]; then
./fast-downward.py $domName $probName --heuristic 'hlm,hff=lm_ff_syn(lm_rhw(reasonable_orders=true,lm_cost_type=one,cost_type=one))' --search 'lazy_greedy([hff,hlm],preferred=[hff,hlm])' >../../../../papers/sspaceAcyclicity/experiments/exp_results/$outName.FDsat.plan.out 2>../../../../papers/sspaceAcyclicity/experiments/exp_results/$outName.FDsat.plan.err
fi
