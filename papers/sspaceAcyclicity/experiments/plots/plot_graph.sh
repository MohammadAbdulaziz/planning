#!/bin/bash
#Putting all bounds in a csv file

# rm bounds_assorted
 

# # # This loop produces all data points in bound_stats files

min_data_points=5;
# for dom in `cat dom_keywords`; do
for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
  echo $dom
  for file in `ls ../exp4_data/*.bound.out | grep -i $dom`; do
    if [ -s $file ]; then
      if [ `gawk '{print $1}' $file` -gt 0 ];then 
         #  &&\
         # [ `gawk '{print $10}' $file` -lt 6000 ] &&\
         # [ `gawk '{print $1}' $file` -lt 900000 ]
        echo -n "$file ";
        cat $file;
      fi;
    fi;
  done > ${dom}_bounds_stat
  cat ${dom}_bounds_stat>>bounds_assorted
done

key_words_shell_var=`cat dom_keywords`

# #Plotting atom dom size vs concrete dom size for all domains together
#   gnuplot << text
#   set terminal png
#   set rmargin 0
#   set size square
#   set output "all-dom-vs-atom-dom.png"
#   set logscale y 10
#   set key outside
#   set nokey 
#   plot 'bounds_assorted' using 10:4
# text

#Plotting integrated and labelled atom dom size vs concrete dom size
  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set size square
  set output "labelled-all-dom-vs-atom-dom.png"
  set key off
  key_words_gnuplot_var = system('./print_domains_leq_datapts.sh ${min_data_points}')
  set xrange [1:200]
  set yrange [10:5000]
  set xlabel "Largest abstraction size computed by HYB (state variables)"
  show xlabel
  set ylabel "Concrete problem size (state variables)"
  show ylabel
  plot for [filename in key_words_gnuplot_var] filename. "_bounds_stat" using 10:4 title filename
text

convert -trim labelled-all-dom-vs-atom-dom.png labelled-all-dom-vs-atom-dom.png


# #Plotting atom actions size vs concrete actions size for all domains together
#   gnuplot << text
#   set terminal png
#   set rmargin 0
#   set size square
#   set logscale y 10
#   set output "all-actions-vs-atom-actions.png"
#   set key outside
#   set nokey 
#   plot 'bounds_assorted' using 11:5
# text

# #Plotting atom actions size vs concrete actions size
  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set size square
  set output "labelled-all-actions-vs-atom-actions.png"
  set key off
  key_words_gnuplot_var = system('./print_domains_leq_datapts.sh ${min_data_points}')
  plot for [filename in key_words_gnuplot_var] filename. "_bounds_stat" using 11:5 title filename
text

convert -trim labelled-all-actions-vs-atom-actions.png labelled-all-actions-vs-atom-actions.png

# #Plotting domain size vs bound for all domains together
#   gnuplot << text
#   set terminal png
#   set rmargin 0
#   set size square
#   set logscale x 10
#   set output "all-dom-vs-bound.png"
#   set key outside
#   set nokey 
#   plot 'bounds_assorted' using 2:4
#   replot x
# text

# #Plotting concrete dom size vs bound
  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set lmargin 0
  set rmargin 0
  set size square
  set output "labelled-all-dom-vs-bound.png"
  set key inside left height 0
  key_words_gnuplot_var = system('./print_domains_leq_datapts.sh ${min_data_points}')
  set yrange [10:5000]
  set xlabel "Bound computed by HYB"
  show xlabel
  set ylabel "Concrete problem size (state variables)"
  show ylabel
  plot for [filename in key_words_gnuplot_var] filename. "_bounds_stat" using 2:4 title filename
text

convert -trim labelled-all-dom-vs-bound.png labelled-all-dom-vs-bound.png

##########################################################################################

# # This loop plots a .png per dom from dom_keyords
# for dom in `cat dom_keywords`; do
#   echo $dom
#   for file in `ls ../exp4_data/$dom*.bound.out | grep -v gripper`; do
#     if [ -s $file ]; then
#       if [ `gawk '{print $1}' $file` -gt 0 ] &&\
#          [ `gawk '{print $10}' $file` -lt 6000 ] &&\
#          [ `gawk '{print $1}' $file` -lt 900000 ]; then
#         echo -n "$file ";
#         cat $file;
#       fi;
#     fi;
#   done > ${dom}_bounds_stat
#   cat ${dom}_bounds_stat>>bounds_assorted

# #Plotting atom dom size vs concrete dom size
#   gnuplot << text
#   set terminal png
#   set rmargin 0
#   set size square
#   set output "${dom}_dom_vs_atom_dom.png"
#   set key outside
#   plot '${dom}_bounds_stat' using 10:4
# text

# #Plotting atom actions size vs concrete actions size
#   gnuplot << text
#   set terminal png
#   set rmargin 0
#   set size square
#   set output "${dom}_actions_vs_atom_actions.png"
#   set key outside
#   plot '${dom}_bounds_stat' using 11:5
# text

# #Plotting concrete dom size vs bound
#   gnuplot << text
#   set terminal png
#   set rmargin 0
#   set size square
#   set output "${dom}_dom_vs_bound.png"
#   set key outside
#   plot '${dom}_bounds_stat' using 4:2
# text

# done

####################################################################################

  # # Print all bounds computed in less than 60 sec with/ w/o snapshotting
cp ../exp4_data/HotelKey_* ../bounds_withsnapshots/
for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
  echo $dom
  for file in `ls ../bounds_withsnapshots/*.bound.out |  grep -i $dom`; do
  # for file in `ls ../exp6_data/*.bound.out | grep -v gripper | grep -iv hotel`; do
    if [ -s $file ]; then
      runtime=`gawk '{print $2}' $file`
      # if [ `gawk '{print $10}' $file` -lt 500 ] &&\
      #    #[ `echo "$runtime < 60" | bc -l` -eq 1 ];then
      #    [ `gawk '{print $1}' $file` -gt 0 ] &&\
      #    [ `gawk '{print $1}' $file` -lt 900000 ]; then
        echo -n $file
        echo -n " "
        echo `cat $file`
      # fi;
    fi;
  done | sed 's/..\/bounds_withsnapshots\///g' > ${dom}_bounds_withsnapshots_assorted
#  done | sed 's/..\/exp4_data\///g' > ${dom}_bounds_withsnapshots_assorted
done

  for file in `ls ../bounds_withsnapshots/*.bound.out`; do
  # for file in `ls ../exp6_data/*.bound.out | grep -v gripper | grep -iv hotel`; do
    if [ -s $file ]; then
      runtime=`gawk '{print $2}' $file`
      # if [ `gawk '{print $1}' $file` -gt 0 ]; then
      #    # [ `gawk '{print $10}' $file` -lt 500 ] &&\
      #    # #[ `echo "$runtime < 60" | bc -l` -eq 1 ];then
      #    # [ `gawk '{print $1}' $file` -gt 0 ] &&\
      #    # [ `gawk '{print $1}' $file` -lt 900000 ]; then
        echo -n $file
        echo -n " "
        echo `cat $file`
      # fi;
    fi;
#  done | sed 's/..\/exp4_data\///g' > bounds_withsnapshots_assorted
  done | sed 's/..\/bounds_withsnapshots\///g' > bounds_withsnapshots_assorted



for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
  echo $dom
  for file in `ls ../bounds_nosnapshots/*.bound.out |  grep -i $dom`; do
  # for file in `ls ../exp6_data/*.bound.out | grep -v gripper | grep -iv hotel`; do
    if [ -s $file ]; then
      runtime=`gawk '{print $2}' $file`
      # if [ `gawk '{print $10}' $file` -lt 500 ] &&\
      #    #[ `echo "$runtime < 60" | bc -l` -eq 1 ];then
      #    [ `gawk '{print $1}' $file` -gt 0 ] &&\
      #    [ `gawk '{print $1}' $file` -lt 900000 ]; then
        echo -n $file
        echo -n " "
        echo `cat $file`
      # fi;
    fi;
  done | sed 's/..\/bounds_nosnapshots\///g' > ${dom}_bounds_nosnapshots_assorted
done

  for file in `ls ../bounds_nosnapshots/*.bound.out`; do
  # for file in `ls ../exp6_data/*.bound.out | grep -v gripper | grep -iv hotel`; do
    if [ -s $file ]; then
      runtime=`gawk '{print $2}' $file`
      # if  [ `gawk '{print $1}' $file` -gt 0 ];then
      #    # [ `gawk '{print $10}' $file` -lt 500 ] &&\
      #    # #[ `echo "$runtime < 60" | bc -l` -eq 1 ];then
      #    # [ `gawk '{print $1}' $file` -gt 0 ] &&\
      #    # [ `gawk '{print $1}' $file` -lt 900000 ]; then
        echo -n $file
        echo -n " "
        echo `cat $file`
      # fi;
    fi;
  done | sed 's/..\/bounds_nosnapshots\///g' > bounds_nosnapshots_assorted


  # for file in `ls ../bounds_nosnapshots/*.bound.out | grep -v gripper | grep -iv hotel`; do
  # # for file in `ls ../exp6_data/*.bound.out | grep -v gripper | grep -iv hotel`; do
  #   if [ -s $file ]; then
  #       echo -n $file
  #       echo -n " "
  #       echo `cat $file`
  #   fi;
  # done | sed 's/..\/bounds_nosnapshots//g' > bounds_nosnapshots_assorted

  # for file in `ls ../bounds_withsnapshots/*.bound.out | grep -v gripper | grep -iv hotel`; do
  #   if [ -s $file ]; then
  #     if [ `gawk '{print $1}' $file` -lt 50000 ] &&\
  #        [ `gawk '{print $1}' $file` -gt 0 ];then
  #     #    [ `gawk '{print $10}' $file` -lt 6000 ] &&\
  #     #    [ `gawk '{print $1}' $file` -lt 900000 ]; then
  #       echo $file
  #       # echo -n " "
  #       # echo `cat $file`
  #     fi;
  #   fi;
  # done #| sed 's/..\/bounds_nosnapshots//g' > bounds_nosnapshots_assorted


  # IFS=$'\n'
  # for line in `cat bounds_withsnapshots_nosnapshots_joined`; do
  #   rt1=`echo $line | gawk '{print $10}'`
  #   rt2=`echo $line | gawk '{print $10}'`
  #   # if [ `echo $line | gawk '{print $10}'` -lt  `echo $line | gawk '{print $20}'` ];then

  #      echo yyy

  #   # fi
  # done #| sed 's/..\/bounds_nosnapshots//g' > bounds_nosnapshots_assorted

join <(sort -k1,1 bounds_withsnapshots_assorted) <(sort -k1,1 bounds_nosnapshots_assorted) | tr -d "\r" |  awk '$2>0' |  awk '$12>0'  > bounds_withsnapshots_nosnapshots_joined

for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
  echo $dom
  join <(sort -k1,1 bounds_withsnapshots_assorted) <(sort -k1,1 ${dom}_bounds_nosnapshots_assorted) | tr -d "\r" | awk '$2>0' |  awk '$12>0' > ${dom}_bounds_withsnapshots_nosnapshots_joined
done

  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10 
  set lmargin 0
  set rmargin 0
  set size square
  set output "labelled-atom-dom-withsnapshot-vs-atom-dom-nosnapshot.png"
  key_words_gnuplot_var = system('./print_domains_leq_datapts.sh ${min_data_points}')
  # set xrange [1:350]
  # set yrange [1:350]
  set nokey 
  set xlabel "Largest abstraction size computed by HYB (state variables)"
  show xlabel
  set ylabel "Largest abstraction size computed by Nsum (state variables)"
  show ylabel
  plot for [filename in key_words_gnuplot_var] filename. "_bounds_withsnapshots_nosnapshots_joined" using 10:20 title filename
text

convert -trim labelled-atom-dom-withsnapshot-vs-atom-dom-nosnapshot.png labelled-atom-dom-withsnapshot-vs-atom-dom-nosnapshot.png

  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10 
  set rmargin 0
  set size square
  set output "atom-dom-withsnapshot-vs-atom-dom-nosnapshot.png"
  set key outside
  # set xrange [1:350]
  # set yrange [1:350]
  set nokey
  plot 'bounds_withsnapshots_nosnapshots_joined' using 10:20
  plot x
text

convert -trim atom-dom-withsnapshot-vs-atom-dom-nosnapshot.png atom-dom-withsnapshot-vs-atom-dom-nosnapshot.png

  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set lmargin 0
  set rmargin 0
  set size square
  set output "labelled-timewithsnapshot-vs-timenosnapshot.png"
  key_words_gnuplot_var = system('./print_domains_leq_datapts.sh ${min_data_points}')
  set nokey 
  set xlabel "Bound computation time for HYB (seconds)"
  show xlabel
  set ylabel "Bound computation time for Nsum (seconds)"
  show ylabel
  plot for [filename in key_words_gnuplot_var] filename. "_bounds_withsnapshots_nosnapshots_joined" using 3:13 title filename
text

convert -trim labelled-timewithsnapshot-vs-timenosnapshot.png labelled-timewithsnapshot-vs-timenosnapshot.png

  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set lmargin 0
  set rmargin 0
  set size square
  set output "timewithsnapshot-vs-timenosnapshot.png"
  set nokey 
  plot 'bounds_withsnapshots_nosnapshots_joined' using 3:13
text

convert -trim timewithsnapshot-vs-timenosnapshot.png timewithsnapshot-vs-timenosnapshot.png


  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set lmargin 0
  set rmargin 0
  set size square
  set output "labelled-boundwithsnapshot-vs-boundnosnapshot.png"
  key_words_gnuplot_var = system('./print_domains_leq_datapts.sh ${min_data_points}')
  set nokey 
  set xlabel "Bound computed by HYB"
  show xlabel
  set ylabel "Bound computed by Nsum"
  show ylabel
  plot for [filename in key_words_gnuplot_var] filename. "_bounds_withsnapshots_nosnapshots_joined" using 2:12 title filename
text

convert -trim labelled-boundwithsnapshot-vs-boundnosnapshot.png labelled-boundwithsnapshot-vs-boundnosnapshot.png

  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set lmargin 0
  set rmargin 0
  set size square
  set output "boundwithsnapshot-vs-boundnosnapshot.png"
  set nokey 
  plot 'bounds_withsnapshots_nosnapshots_joined' using 2:12
text

convert -trim boundwithsnapshot-vs-boundnosnapshot.png boundwithsnapshot-vs-boundnosnapshot.png


#   gnuplot << text
#   set terminal png
#   set rmargin 0
#   set size square
#   set output "timewithsnapshot-vs-timenosnapshot.png"
#   #set key outside
#   set logscale x 10
#   # set xrange [0:3.3]
#   set logscale y 10
#   # set yrange [0:20]
#   set nokey 
#   plot 'bounds_withsnapshots_nosnapshots_joined' using 3:13
#   replot x
# text
