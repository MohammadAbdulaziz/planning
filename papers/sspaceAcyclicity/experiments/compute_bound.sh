#! /bin/bash
cd ../../../codeBase/planning/stripsBound/

if [ ! -f "../../../papers/sspaceAcyclicity/experiments/exp_results/$outName.bound.out" ]; then
  ./run.sh $domName $probName | grep -v necessary | grep -v acyclic | grep -v over >../../../papers/sspaceAcyclicity/experiments/exp_results/$outName.bound.out 2>../../../papers/sspaceAcyclicity/experiments/exp_results/$outName.bound.err
fi
