#!/bin/bash
modified_outName=`echo $outName | sed 's/_aux//g'`
#modified_outName=$outName


 # forfile in `ls exp6_data/*.bound.out | grep -v Hotel`; do
 #    if [ -s $file ]; then
 #      if [ `gawk '{print $1}' $file` -gt 0 ]  && [ `gawk '{print $1}' $file` -lt 1000000 ]; then
 #        echo "$file";
 #      fi;
 #    fi;
 #  done

# if [ ! -f exp6_data/${modified_outName}.FDsat.plan.out ] ||\
#    [ ! -f exp6_data/${modified_outName}.FDunsat.plan.out ] ||\
#    [ ! -f exp6_data/${modified_outName}.M.plan.nobound.out ] ||\
#    [ ! -f exp6_data/${modified_outName}.Mp.plan.nobound.out ] ||\
#    [ ! -f exp6_data/${modified_outName}.MpC.plan.nobound.out ] ||\
#    [ ! -f exp6_data/${modified_outName}.M.plan.withbound.out ] ||\
#    [ ! -f exp6_data/${modified_outName}.Mp.plan.withbound.out ] ||\
#    [ ! -f exp6_data/${modified_outName}.MpC.plan.withbound.out ]; then
#   exit
# fi

if [ -f exp6_data/${modified_outName}.FDsat.plan.out ]; then
  if grep -q "Solution found." exp6_data/${modified_outName}.FDsat.plan.out; then
    totalTime=`cat exp6_data/${modified_outName}.FDsat.plan.out | grep "Total time:" | gawk '{print $3}' | sed 's/s//g'`
    echo ${modified_outName} $totalTime >>FDsat.sat.solvedprobs
  fi
else
  echo "exp6_data/${modified_outName}.FDsat.plan.out" not found
fi

if [ -f exp6_data/${modified_outName}.FDunsat.plan.out ]; then
  if grep -q "unsolvable" exp6_data/${modified_outName}.FDunsat.plan.out && ! grep -q "$modified_outName" FDsat.sat.solvedprobs_ && ! grep -q "$modified_outName" M.withbound.sat.solvedprobs_ && ! grep -q "$modified_outName" M.nobound.sat.solvedprobs_; then
    if ! grep -q "Solution found." exp6_data/${modified_outName}.FDsat.plan.out 2>/dev/null && ! grep -q "Solution found." exp6_data/${modified_outName}.FDunsat.planner.log 2>/dev/null; then
      totalTime=`cat exp6_data/${modified_outName}.FDunsat.planner.log | grep "Total time" | gawk '{print $3}' | sed 's/s//g'`
      echo ${modified_outName} $totalTime >>FDunsat.unsat.solvedprobs
    else
      echo ${modified_outName} $totalTime >>FDunsat.unsat.solvedprobs.debug
    fi
  fi
else
  echo "exp6_data/${modified_outName}.FDunsat.plan.out" not found
fi


if [ -f exp6_data/${modified_outName}.M.plan.nobound.out ]; then
  if grep -q "PLAN FOUND:" exp6_data/${modified_outName}.M.plan.nobound.out; then
    totalTime=`cat exp6_data/${modified_outName}.M.plan.nobound.out | grep "total time" | gawk '{print $3}'`
    echo ${modified_outName} $totalTime >>M.nobound.sat.solvedprobs
  fi
else
  echo "" not found
fi

if [ -f exp6_data/${modified_outName}.Mp.plan.nobound.out ]; then
  if grep -q "PLAN FOUND:" exp6_data/${modified_outName}.Mp.plan.nobound.out; then
    totalTime=`cat exp6_data/${modified_outName}.Mp.plan.nobound.out | grep "total time" | gawk '{print $3}'`
    echo ${modified_outName} $totalTime >>Mp.nobound.sat.solvedprobs
  fi
else
  echo "exp6_data/${modified_outName}.M.plan.nobound.out" not found
fi

if [ -f exp6_data/${modified_outName}.MpC.plan.nobound.out ]; then
  if grep -q "PLAN FOUND:" exp6_data/${modified_outName}.MpC.plan.nobound.out; then
    totalTime=`cat exp6_data/${modified_outName}.MpC.plan.nobound.out | grep "total time" | gawk '{print $3}'`
    echo ${modified_outName} $totalTime >>MpC.nobound.sat.solvedprobs
  fi
else
  echo "exp6_data/${modified_outName}.MpC.plan.nobound.out" not found
fi


if [ -f exp6_data/${modified_outName}.M.plan.withbound.out ]; then
  if grep -q "PLAN FOUND:" exp6_data/${modified_outName}.M.plan.withbound.out; then
    totalTime=`cat exp6_data/${modified_outName}.M.plan.withbound.out | grep "total time" | gawk '{print $3}'`
    echo ${modified_outName} $totalTime >>M.withbound.sat.solvedprobs
  fi
else
  echo "exp6_data/${modified_outName}.MpC.plan.nobound.out" not found
fi 

if [ -f exp6_data/${modified_outName}.Mp.plan.withbound.out ]; then
  if grep -q "PLAN FOUND:" exp6_data/${modified_outName}.Mp.plan.withbound.out; then
    totalTime=`cat exp6_data/${modified_outName}.Mp.plan.withbound.out | grep "total time" | gawk '{print $3}'`
    echo ${modified_outName} $totalTime >>Mp.withbound.sat.solvedprobs
  fi
else
  echo "exp6_data/${modified_outName}.MpC.plan.nobound.out" not found
fi 

if [ -f exp6_data/${modified_outName}.MpC.plan.withbound.out ]; then
  if grep -q "PLAN FOUND:" exp6_data/${modified_outName}.MpC.plan.withbound.out; then
    totalTime=`cat exp6_data/${modified_outName}.MpC.plan.withbound.out | grep "total time" | gawk '{print $3}'`
    echo ${modified_outName} $totalTime >>MpC.withbound.sat.solvedprobs
  fi
else
  echo "exp6_data/${modified_outName}.MpC.plan.nobound.out" not found
fi 


# if [ -f exp6_data/${modified_outName}.M.plan.withbound.out ]; then
#   if grep -q "PLAN NOT FOUND:" exp6_data/${modified_outName}.M.plan.withbound.out;then
#     if ! grep -q "Solution found." exp6_data/${modified_outName}.FDsat.plan.out 2>/dev/null &&\
#        ! grep -q "PLAN FOUND" exp6_data/${modified_outName}.M.plan.withbound.out 2>/dev/null &&\
#        ! grep -q "PLAN FOUND" exp6_data/${modified_outName}.M.plan.nobound.out 2>/dev/null &&\
#        ! grep -q "PLAN FOUND" exp6_data/${modified_outName}.Mp.plan.withbound.out 2>/dev/null &&\
#        ! grep -q "PLAN FOUND" exp6_data/${modified_outName}.Mp.plan.nobound.out 2>/dev/null &&\
#        ! grep -q "PLAN FOUND" exp6_data/${modified_outName}.MpC.plan.withbound.out 2>/dev/null &&\
#        ! grep -q "PLAN FOUND" exp6_data/${modified_outName}.MpC.plan.nobound.out 2>/dev/null ; then
#      totalTime=`cat exp6_data/${modified_outName}.M.plan.withbound.out | grep "total time" | gawk '{print $3}'`
#      echo ${modified_outName} $totalTime >>M.withbound.unsat.solvedprobs
#     else
#      totalTime=`cat exp6_data/${modified_outName}.M.plan.withbound.out | grep "total time" | gawk '{print $3}'`
#      echo ${modified_outName} $totalTime >>M.withbound.unsat.solvedprobs.debug
#     fi
#   fi
# else
#   echo "exp6_data/${modified_outName}.MpC.plan.nobound.out" not found
# fi


if [ -f exp6_data/${modified_outName}.M.plan.withbound.out ]; then
  if grep -q "PLAN NOT FOUND:" exp6_data/${modified_outName}.M.plan.withbound.out;then
    if ! grep -q "PLAN FOUND" exp6_data/${modified_outName}.M.plan.nobound.out 2>/dev/null; then
     totalTime=`cat exp6_data/${modified_outName}.M.plan.withbound.out | grep "total time" | gawk '{print $3}'`
     echo ${modified_outName} $totalTime >>M.withbound.unsat.solvedprobs
    else
     totalTime=`cat exp6_data/${modified_outName}.M.plan.withbound.out | grep "total time" | gawk '{print $3}'`
     echo ${modified_outName} $totalTime >>M.withbound.unsat.solvedprobs.debug
    fi
  fi
else
  echo "exp6_data/${modified_outName}.MpC.plan.nobound.out" not found
fi


if [ -f exp6_data/${modified_outName}.Mp.plan.withbound.out ]; then
  if grep -q "PLAN NOT FOUND:" exp6_data/${modified_outName}.Mp.plan.withbound.out;then
    if ! grep -q "PLAN FOUND" exp6_data/${modified_outName}.Mp.plan.nobound.out 2>/dev/null; then
     totalTime=`cat exp6_data/${modified_outName}.Mp.plan.withbound.out | grep "total time" | gawk '{print $3}'`
     echo ${modified_outName} $totalTime >>Mp.withbound.unsat.solvedprobs
    else
     totalTime=`cat exp6_data/${modified_outName}.Mp.plan.withbound.out | grep "total time" | gawk '{print $3}'`
     echo ${modified_outName} $totalTime >>Mp.withbound.unsat.solvedprobs.debug
    fi
  fi
else
  echo "exp6_data/${modified_outName}.MpC.plan.nobound.out" not found
fi


if [ -f exp6_data/${modified_outName}.MpC.plan.withbound.out ]; then
  if grep -q "PLAN NOT FOUND:" exp6_data/${modified_outName}.MpC.plan.withbound.out;then
    if ! grep -q "PLAN FOUND" exp6_data/${modified_outName}.MpC.plan.nobound.out 2>/dev/null; then
     totalTime=`cat exp6_data/${modified_outName}.MpC.plan.withbound.out | grep "total time" | gawk '{print $3}'`
     echo ${modified_outName} $totalTime >>MpC.withbound.unsat.solvedprobs
    else
     totalTime=`cat exp6_data/${modified_outName}.MpC.plan.withbound.out | grep "total time" | gawk '{print $3}'`
     echo ${modified_outName} $totalTime >>MpC.withbound.unsat.solvedprobs.debug
    fi
  fi
else
  echo "exp6_data/${modified_outName}.MpC.plan.nobound.out" not found
fi

# join <(cat MpC.withbound.sat.solvedprobs | sort) <(cat MpC.nobound.sat.solvedprobs | sort) > MpC.withbound.nobound.sat.solvedprobs.joined

  # IFS=$'\n'
  # for line in `cat MpC.withbound.nobound.sat.solvedprobs.joined`; do
  #   rt1=`echo $line | gawk '{print $2}'`
  #   rt2=`echo $line | gawk '{print $3}'`
  #   if [ `echo "$rt1 < $rt2" | bc -l` -eq 1 ];then
  #   # if [ `echo $line | gawk '{print $10}'` -lt  `echo $line | gawk '{print $20}'` ];then

  #      echo yyy
  #   fi
  #   # fi
  # done #| sed 's/..\/bounds_nosnapshots//g' > bounds_nosnapshots_assorted


#echo "UNSAT Probs Solved with Aidos"
# wc -l ./FDunsat.unsat.solvedprobs
# echo "UNSAT Probs Solved with Bounds"
# wc -l ./M.withbound.unsat.solvedprobs
# echo "SAT Probs Solved with Bounds"
# wc -l ./M.withbound.sat.solvedprobs
# echo "SAT Probs Solved without Bounds"
# wc -l ./M.nobound.sat.solvedprobs






# if [ -f "exp6_data/${modified_outName}.bound.out" ]; then

#   bound=`cat exp6_data/${modified_outName}.bound.out | gawk '{print $1}'`
  
#   # if [ $bound gt 0 ]; then 
    
  
#   # fi    
# fi
