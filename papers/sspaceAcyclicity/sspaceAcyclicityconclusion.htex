\section{Conclusions and Future Work}
\label{sec:conclusion}


%% Computing upper bounds on the diameter or the recurrence diameter is a necessay ingredient for bounded model-checking and planning as SAT.

%% The completeness of SAT based planning and bounded model-checking is hindered by the availability, and otherwise computational cost of obtaining upper bounds~\cite{clarke2004completeness}.
%% Upper bounds also have application in state based solution algorithms. 
%% For example, bounds were recently used to help quickly identify small sub-goals that have no solution~\cite{Rintanen:Gretton:2013}.
%% Compositional approaches are able to yield useful bounds with relatively little computational effort. 
%%
The practical incompleteness of SAT based planning and model-checking algorithms---due to the absence of upper-bounding methods---has for some years been noted as a significant problem~\cite{clarke2004completeness}.
It is perceived as a deficiency of SAT methods in making comparisons with state based methods.
We have addressed that deficiency by advancing  the compositional approach to computing upper bounds.
%% To our knowledge that is the only feasible approach when treating factored representations of transition system problems with very large state spaces.
Our advance is to exploit state space acyclicity, giving significantly finer grained decompositions compared to previous works.
The resulting algorithm is able to achieve exponentially tighter bounds relative to comparable recent studies.
That benefit comes with the risk of an exponential explosion in the number of subproblems considered by the algorithm.
The runtime measurements we made experimentally suggest that this theoretical risk is not realised in practice.
Bounds computed using our approach enabled a SAT based planning system to prove the unsatisfiability of planning benchmarks (most notably the hotel key protocol) that severely challenge state-of-the-art state-search based tools.


Future research should investigate bounding using functions that are tighter than the diameter.
One such bound is the \emph{radius}, which is the longest shortest path from the \emph{initial state} to any other state.
We conjecture that our analysis  shall carry over to that setting.
Further study should also develop compositional bounds using a more sophisticated base-case function.
%%Existing empirical work has only considered using relatively simple functions, such as $\sasdom$.
%%
Base-case functions, such as recurrence and sublist diameters could yield superior bounds compared to those we report, however are NP-hard to evaluate.
%%
%%We note that the increased decompositionality we provide has the potential to make compositional bounding exponentially faster,
Because the size of abstractions evaluated in the base-case using our method are relatively small compared to other compositional approaches, one can expect exponentially faster bounding using such sophisticated base-case functions.

%% Our improvement comes from the fact that we exploit acyclicity in the state space, giving significantly finer grained decompositions.
%% This can lead to exponentially tighter bounds if the base-case function is the size of the domain of state-variables, as is the case with \cite{Rintanen:Gretton:2013,abdulaziz2015verified} and in this current work.
%%On the other hand, if the base-case function is the recurrence diameter (an NP-hard problem), then the evaluation of the bound will be over smaller (possibly exponentially easier) problems.
%%It is also worth noting that combining sub-system bounds in the case of state space acyclicity is done through addition, in contrast to the case of acyclicity in variable dependency, the combination is multiplicative.
%%This comes at the cost of potential intractability of computing decompositions based on state space acylicity.
%% An important item for future work, is to examine the trade-off between computation time and compositional bound tightness.
%% Our approach has application in compositional bounding using direct calculations of the recurrence diameter in evaluating $b$, as described by~\cite{BiereCCZ99}. 

%% An important point to investigate in the future is whether our approach applies to something tighter than the diameter, which takes into consideration the initial state and/or goal(s) of the transition system.
%% One such bound is the \emph{radius}, which the longest shortest path from the \emph{initial state} to any other state.
%% We conjecture that our analyses apply to the radius.

%% \charles{we can talk about the radius, and say we conjecture that all of our analyses apply to the radius, but doing them on the diameter is less complicated and the radius has to do with the initial state}



%% \citeauthor{abdulaziz2015verified}~(\citeyear{abdulaziz2015verified}) described {\em sublist diameter}, which is exponentially tighter than the recurrence diameter.
