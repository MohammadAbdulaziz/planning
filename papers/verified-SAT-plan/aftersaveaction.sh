#!/bin/bash

if `ps -e | grep -q Holmake`; then
  exit
fi
rm verified-SAT-plan.pdf
latexmk -f -gg -pdf verified-SAT-plan.tex
pdftk verified-SAT-plan.pdf
