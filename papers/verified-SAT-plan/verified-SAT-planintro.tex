\section{Introduction}
\label{sec:intro}

As witnessed by the different planning competitions~\cite{ipc98,DBLP:journals/aim/ColesCOJLSY12,vallati20152014}, planning algorithms and systems are becoming more and more scalable and efficient, which makes them suited for more realistic applications.
Nonetheless, since many applications of planning are safety-critical, increasing the trustworthiness of planning algorithms and systems (i.e. the likelihood that they compute correct results) could help with their widespread adoption.
Thus, there is currently increased efforts to improve the trustworthiness of planning systems~\cite{howey2004val,eriksson2017unsolvability,JAR2017,abdulaziz2018formally}.

Increasing trustworthiness of complex algorithms and software is a well-studied problem.
\citeauthor{DBLP:conf/mfcs/AbdulazizMN19} provide a good characteristation of different existing methods to increase the trustworthiness of algorithms and their implementations, and they categorise them into one of three approaches.
Firstly, a system's trustworthiness can be boosted by the different practices of software engineering, such as writing clean code with the right levels of abstraction, as well as testing.
Although this is relatively easy to achieve, it has the fundamental problem of being incomplete: a system is only guaranteed to operate correctly on cases it has been tested on.

Secondly, there is the approach of certified computation, where the given system computes, along with its output, a certificate that shows why this output is correct. 
That certificate should then be checkable with a simple certificate checker, and preferably the certificate checking should be asymptotically easier than computing the output.
The main advantage of this method is that it relegates the burden of trustworthiness to the checker, which should be much simpler than the system whose out is to be certified, and thus is less error prone.
The approach of certification has been pioneered by~\citeauthor{DBLP:conf/mfcs/MehlhornN98} who used it in their LEDA library~\cite{DBLP:conf/mfcs/MehlhornN98}, where they devised programs that produce certificates for solutions of different combinatorial optimisation problems.
In the realm of planning, this approach was pioneered by~\citeauthor{howey2004val} who developed VAL~\cite{howey2004val} that, given a planning problem and potential solution, certifies that the solution actually solves the given problem.
Certifying unsolvability for planning was also tackled by~\citeauthor{eriksson2017unsolvability}~(\citeyear{eriksson2017unsolvability}) who provided unsolvability certificates and checkers for state-space search algorithms and by~\citeauthor{DBLP:conf/aips/ErikssonH20}~(\citeyear{DBLP:conf/aips/ErikssonH20}) for property directed SAT-based planning.
Nonetheless, this approach has its limitations.
For instance the algorithm whose output is to be certified has to be modified to produce the certificates, and this has to be done in a way that does not significantly hurt the performance of the algorithm.
More fundamentally, for some problems there are not succinct certificates (i.e.\ at most polynomially larger than the problem instance), unless the polynomial hierarchy collapses~\cite{Vazirani}.
This means that in the worst case certificate checking can take as long as computing the output which is certified.
Classical planning, as well as other PSPACE-complete problems, is one such problem with no succinct certificates.

Thirdly, a system's trustworthiness can be increases using formal verfication, where the system is mathematically proven to always produce the correct output.
Such a mathematical proof is then checked by a proof checker.
This approach gives the highest possible trust in an algorithm or a program.
In addition to guaranteeing that the outputs are correct, as is the case with certified computations, this approach guarantees completeness of the program, i.e.\ it will always generate an output.
On the other hand, this approach has the disadvantage that it is very costly in terms of effort.
Also, it is usually much harder for a formally verified program to perform as well as an unverified one since verifying all performance optimisations is usually not feasible.
Despite the previously stated difficulties, formal verification has seen wide-spread use recently.
Notable applications include a verified OS kernel\cite{klein2009sel4L}, a verified SAT-solver~\cite{DBLP:journals/jar/BlanchetteFLW18}, a verified model checker~\cite{esparza2013fully}, a verified conference system~\cite{DBLP:conf/cav/KanavL014}, and a verified optimised C compiler~\cite{leroy2009formal}.
In the context of planning, however, formal verification was only applied to verify a plan validator~\cite{abdulaziz2018formally}, and algorithms for computing upper bounds on plan lengths~\cite{JAR2017}.


Encoding planning problems as logical formulae has a long history~\cite{mccarthy1981some}.
SAT-based planning~\cite{kautz:selman:92} is the most successful such approach.
It depends on encoding the question of existence of a solution, of a bounded length, for a given problem as a propositional satisfiability formulae.
A series of such formulae for increasing solution lengths are passed to a SAT solver until a plan is found.
The initial encoding of~\citeauthor{kautz:selman:92} was significantly improved by exploiting symmetries in the problem~\cite{rintanen2003symmetry}, state invariants~\cite{}, and by parallelising the encodings~\cite{rintanen2006planning}.
In this paper we present a formally verified $\forall$-step parallel encoding of classical planning~\cite{kautz:selman:96,rintanen2006planning}.

Before we delve deeper in the details of the verification, we find it sensible to answer the question of why verify a SAT encoding of planning?
SAT encodings of planning are particularly suited for formal verification since the process of encoding the problem into a SAT formula is not the computational bottleneck.
This means that not much effort needs to be put into verifying different implementation optimisations.
Another important advantage is that verified SAT solving technology is continuously advancing, which means that the performance of verified planning using the verified SAT-based encoding will continuously increase.
For instance, the current version of the formally verified SAT solver by~\citeauthor{DBLP:journals/jar/BlanchetteFLW18} performs as well as Minisat~\cite{DBLP:conf/sat/EenS03}.
Also, the formally verified unsatisfiability checker GRAT~\cite{DBLP:conf/sat/Lammich17} is faster than the standard checker DRAT-trim~\cite{DBLP:conf/sat/WetzlerHH14}.

\subsection{Approach}

We formally verified the SAT encoding using the interactive theorem prover Isabelle/HOL~\cite{DBLP:books/sp/NipkowPW02}, where we constructed a formal proof of correctness of our SAT encoding.
In particular, we formally verified two programs.
The first one, given an input multi-valued planning problem in the format of Fast-Downward's translator~\cite{Helmert06}, produces a clause normal format (CNF) SAT formula in the standard DIMACS-CNF format.
The second program is a decoding program that, given a list of literals and a planning problem, will produce a plan only if this list of literals constitute a model for the CNF encoding of the given planning problem.
We paid particular attention to reducing the trusted code base in, both, the encoder and the decoder.
The only trusted parts are a minimalist parser that parse a problem file and produce an equivalent abstract syntax tree (AST), and a similar parser that parses lists on integers that represent models of CNF formulae.

Formally verifying a reasonably big piece of software can be a daunting task.
For instance, it has been reported that the size of the proof scripts for verifying the OS kernel sel4 is quadratically as big as the C implementation of the kernel~\cite{klein2009sel4L}.
Thus, a major verification effort like verifying a state-of-the art encoding needs careful engineering and modularisation.
For complicated algorithms, the most successful approach to obtain formally verified efficient implementation is the stepwise refinement approach, where one starts by an abstract version of the algorithm and verified it, and then one keeps devising more optimised versions of the algorithm, and showing them equivalent to the less optimised versions.
This approach was used in most successful serious algorithm verification efforts~\cite{klein2009sel4L,DBLP:journals/jar/BlanchetteFLW18,esparza2013fully,DBLP:conf/cav/KanavL014}.

For compilers, a slightly different approach is used: one splits the compiler into smaller transformation steps, and verifies each one of those steps separately.
In the end, the composition of those verified transformations is the verified compiler.
This approach was used in all notable verified compilers~\cite{leroy2009formal,DBLP:conf/popl/KumarMNO14}.
This methodology was necessary for our development since the SAT encoding that we verify cannot be feasibly reasoned about at the level of Fast-Downward translator format ASTs or DIMACS ASTs.
Thus, we introduced multiple verified translation steps that firstly transform a Fast-Downward translator format AST into an abstract representation of multi-valued planning problems.
Then a SAT encoding would translate this abstract planning problem into an abstract SAT formula.
This formula is then translated into DIMACS.
In the rest of the paper we provide some details about these steps, and the different tradeoffs we took into consideration we splitting the encoding into the steps we did.
We made a conscious choice to show details of the formalisation in the hope that it would be instructive for future similar verification projects, and to give a sense to the reader of the scale of what is possible.

\subsection{Isabelle/HOL}
Isabelle/HOL~\cite{paulson1994isabelle} is a theorem prover based on Higher-Order Logic. 
Roughly speaking, Higher-Order Logic can be seen as a combination of functional programming with logic.
Isabelle/HOL supports the extraction of the functional fragment to actual code in various languages~\cite{haftmann2007code}.

Isabelle's syntax is a variation of Standard ML combined with (almost) standard mathematical notation.
Function application is written infix, and functions are usually Curried (i.e., function $f$ applied to arguments $x_1~\ldots~x_n$ is written as $f~x_1~\ldots~x_n$ instead of the standard notation $f(x_1,~\ldots~,x_n)$).
We explain non-standard syntax in the paper where it occurs.

Isabelle is designed for trustworthiness: following the LCF approach~\cite{milner1972logic}, a small kernel implements the inference rules of the logic, and, using encapsulation features of ML, it guarantees that all theorems are actually proved by this small kernel.
% Isabelle follows the Logic for Computable Functions architecture~\cite{milner1972logic}: it is designed for trustworthiness, where a small kernel implements the inference rules of the logic, and, using encapsulation features of ML, it is guaranteed that all theorems are actually proved by this small kernel.
Around the kernel, there is a large set of tools that implement proof tactics and high-level concepts like algebraic datatypes and recursive functions.
Bugs in these tools cannot lead to inconsistent theorems being proved, but only to error messages when the kernel refuses a proof.
