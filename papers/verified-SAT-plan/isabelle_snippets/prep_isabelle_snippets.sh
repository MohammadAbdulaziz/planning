#!/bin/bash

rm *.thy
rm output/document/*.tex
cp edited_thys/*.thy .

isabelle="/home/ladmin/Isabelle2020/bin/isabelle"

sessions="Verified_SAT_Based_AI_Planning\
          AI_Planning_Languages_Semantics\
          Propositional_Proof_Systems\
          List-Index"


thms="decode_def\
      encode_def\
      decode_sound\
      decode_complete\
      encode_sound\
      encode_complete\
      ast_problem.wf_partial_state_def\
      ast_problem.wf_operator_def\
      ast_problem.well_formed_def\
      ast_problem.valid_states_def\
      ast_problem.subsuming_states_def\
      ast_problem.enabled_def\
      ast_problem.eff_enabled_def\
      ast_problem.execute_def\
      ast_problem.path_to.simps\
      ast_problem.valid_plan_def\
      dimacs_model_def\
      rem_implicit_pres_ops.simps\
      rem_implicit_pres_ops_path_to\
      rem_implicit_pres_ops_valid_plan\
      ast_problem.abs_ast_goal_def\
      ast_problem.abs_ast_initial_state_def\
      ast_problem.abs_range_map_def\
      ast_problem.abs_ast_operator_def\
      abs_ast_prob.is_serial_sol_then_valid_plan\
      ast_problem.decode_abs_plan_def\
      abs_ast_prob.valid_plan_then_is_serial_sol\
      cnf_to_dimacs.var_to_dimacs.simps\
      cnf_to_dimacs.dimacs_to_var_def\
      sat_solve_sasp.planning_by_cnf_dimacs_complete\
      sat_solve_sasp.planning_by_cnf_dimacs_sound\
      cnf_to_dimacs.dimacs_to_sat_plan\
      disj_to_dimacs.simps\
      cnf_to_dimacs.simps
      model_to_dimacs_model_sound\
      model_to_dimacs_model_complete\
      model_to_dimacs_model_sound_exists\
      dimacs_model_to_abs_def"

types="name\
       ast_variable\
       ast_effect\
       ast_operator\
       ast_problem\
       state\
       assignment\
       sas_plus_operator\
       sas_plus_problem"

rm ROOT

print_thms () {
thms=$1
antiquot=$2
ops=$3
for thm in $thms; do
  thy_file_name=${thm//./_}
  echo "    \"$thy_file_name\"" >> ROOT;
  if [ ! -f ${thy_file_name}.thy ]; then
    echo "  (*<*)" >> ${thy_file_name}.thy
    echo "theory ${thy_file_name}" >> ${thy_file_name}.thy
    echo "  imports \"Verified_SAT_Based_AI_Planning.Solve_SASP\"" >> ${thy_file_name}.thy
    echo "begin" >> ${thy_file_name}.thy
    echo "(*>*)" >> ${thy_file_name}.thy
    echo "text \"@{${antiquot}[display] ${thm}${ops}}\"" >> ${thy_file_name}.thy
    echo "(*<*)" >> ${thy_file_name}.thy
    echo "end" >> ${thy_file_name}.thy
    echo "(*>*)" >> ${thy_file_name}.thy
  fi
done
}

echo "session Isabelle_Snippets = HOL +" >> ROOT
echo "  description {* Isabelle Snippets *}" >> ROOT
echo "  options [document = pdf," >> ROOT
echo "           document_output = \"output\"," >> ROOT
echo "           document_variants = \"document=-theory,/proof,/ML\"] " >> ROOT
echo "  sessions" >> ROOT
for session in $sessions; do
  echo "    \"$session\"" >> ROOT
done
echo "  theories" >> ROOT
print_thms "$thms" "thm" "[no_vars,names_short]"
print_thms "$types" "typ" ""
echo "  document_files" >> ROOT
echo "    \"root.tex\"" >> ROOT


$isabelle build -cv -o threads=4 -o document=pdf -o document_output=. -d ./ Isabelle_Snippets

sed -i 's/ problem/ $\\Pi$/g' output/document/*.tex
sed -i 's/ dimacs_M/ dM/g' output/document/*.tex
