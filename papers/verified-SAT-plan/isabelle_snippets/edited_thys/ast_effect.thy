  (*<*)
theory ast_effect
  imports "Verified_SAT_Based_AI_Planning.Solve_SASP"
begin
(*>*)
type_synonym ast_effect = "ast_precond list \<times> nat \<times> nat option \<times> nat"
(*<*)
end
(*>*)
