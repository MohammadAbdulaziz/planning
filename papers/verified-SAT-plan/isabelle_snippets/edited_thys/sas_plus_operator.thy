  (*<*)
theory sas_plus_operator
  imports "Verified_SAT_Based_AI_Planning.Solve_SASP"
begin
(*>*)
record  ('variable, 'domain) sas_plus_operator = 
  precondition_of :: "('variable, 'domain) assignment list" 
  effect_of :: "('variable, 'domain) assignment list" 
(*<*)
end
(*>*)
