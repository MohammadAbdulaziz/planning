  (*<*)
theory state
  imports "Verified_SAT_Based_AI_Planning.Solve_SASP"
begin
(*>*)
type_synonym ('variable, 'domain) state = "'variable \<rightharpoonup> 'domain"
(*<*)
end
(*>*)
