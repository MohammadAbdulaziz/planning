  (*<*)
theory ast_variable
  imports "Verified_SAT_Based_AI_Planning.Solve_SASP"
begin
(*>*)
type_synonym ast_variable = "name \<times> nat option \<times> name list"
(*<*)
end
(*>*)
