  (*<*)
theory ast_problem
  imports "Verified_SAT_Based_AI_Planning.Solve_SASP"
begin
(*>*)
type_synonym ast_problem = 
  "ast_variable_section \<times> ast_initial_state \<times> ast_goal \<times> ast_operator_section"
(*<*)
end
(*>*)
