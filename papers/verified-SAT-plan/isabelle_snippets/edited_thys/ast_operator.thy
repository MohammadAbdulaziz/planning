  (*<*)
theory ast_operator
  imports "Verified_SAT_Based_AI_Planning.Solve_SASP"
begin
(*>*)
type_synonym ast_operator = "name \<times> ast_precond list \<times> ast_effect list \<times> nat"
(*<*)
end
(*>*)
