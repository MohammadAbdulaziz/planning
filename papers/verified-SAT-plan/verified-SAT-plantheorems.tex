\section{Background and Main Theorem}
\label{sec:bg}


As we stated earlier, one goal of our development is to keep the amount of untrusted (i.e.\ unverified) code to a minimum.
Because of that, we aimed at having our verified program to \begin{enumerate*} \item take as input as close a representation as possible to the actual input format (i.e.\ Fast-Downward's representation of multivalued-planning problems) and \item to the produce as output a format that is as close to the output file format as possible (i.e.\ the DIMACS format for CNF SAT formulae).\end{enumerate*}
Accordingly, we used as input and as output the abstract syntax tree (AST) of these formats.

For the output, we target the standard DIMACS format of CNF SAT formulae\cite{DIMACS}.
The AST of DIMACS is simply a list of lists of integers, where each list represents a line in the DIMACS file, i.e. a clause, and each integer represents a literal.
A model for a formula represented as a DIMACS AST is a list of integers.
The precise semantics of formulae in this representation and their models is given by the following predicate that characterises what it is for a model $ls$ to satisfy a CNF formula $cs$, which is a list of lists of integers.
One literal has to be satisfied for each clause, and the model has to map each variable to one value.
\inputisabellesnippet{dimacs_model_def}



For the input, we use the same AST of Fast-Downward's translator output format that was developed by Abdulaziz and Lammich\cite{ICTAI,AFP}.
However, we reiterate the basic definitions here for completeness.
We start by formally defining the abstract syntax tree of the output of the Translator as described on Fast-Downward's web-page\footnote{A link will be provided in case of acceptance}.
For that, we first define the concepts that constitute the syntax tree. 
We declare those concepts as \emph{type synonyms}, i.e. abbreviations for types. The most basic concept is that of a name, which is defined as an Isabelle/HOL string.\\
\inputisabellesnippet{name}

A \emph{SAS+ variable} is defined as a triple made of \begin{enumerate*} \item the name of the variable, \item a \emph{nat option} which is the axiom layer of the variable, and \item a list of names, each of which represents an assignment of that variable.\end{enumerate*}
\emph{nat option} is a type with either no value at all (in case the variable has no axiom layer), or some value of type \emph{nat}, a natural number.\\
\inputisabellesnippet{ast_variable}

We model partial states (i.e. an assignment of a set of variables) as lists of pairs of natural numbers, representing the variable ID and the assigned value.
The variable ID is in the order of declaration of the variables.
% The first of each pair is a variable ID which is the order in which the variable was declared, and the second is the assignment.
Partial states are used to model preconditions and the goal. 

We model an effect as the following 4-tuple, whose members are \begin{enumerate*}\item the effect preconditions, \item the affected variable, \item the implicit precondition (if any), which is an assignment that the affected variable needs to have for the effect to take place, \item and the new value for the variable.\end{enumerate*}
\inputisabellesnippet{ast_effect}

The type of an operator is the following 4-tuple, which has the operator name, preconditions, effects and cost.
\inputisabellesnippet{ast_operator}

Finally, we model a problem as follows.\\
\inputisabellesnippet{ast_problem}

The variable and operator sections are lists of variables and operators, respectively.
The initial state is a list of natural numbers (assignments), whose length has to be equal to the number of state variables, and the goal is a partial state.
We functions to access the state variables, initial state, the goal and the set of actions of a given problem AST, and refer to them as \emph{astD}, \emph{astI}, \emph{astG} and \emph{ast$\delta$}, respectively.

We now have defined the types of the objects that constitute the abstract syntax tree.


%% Before we specify the predicates for its well-formedness, we briefly introduce the \emph{locale} feature of Isabelle/HOL.

%% \subsubsection{Isabelle/HOL Locales}
%% \textcolor{red}{remove locales}

%% A locale is a declared scope that fixes some identifiers and makes some assumptions that are to be shared among all the definitions and theorems 
%% declared within the locale.
%% Both, fixed identifiers and assumptions, are optional.
%% Locales are useful if one does not want to repeat the respective identifiers or assumptions for every definition.
%% % and it includes some bound variable names or assumptions that are to be shared among all the definitions and theorems declared within the locale.
%% % It is useful in situations where one would not want to repeat the respective variable name or a set of assumptions.
%% To define the predicates of well-formed variables, actions, and problems we declare the following locale.

%% \isacommand{locale}\isamarkupfalse%
%% \ ast{\isacharunderscore}problem\ {\isacharequal}\isanewline
%% \ \ \ \ \isakeyword{fixes}\ problem\ {\isacharcolon}{\isacharcolon}\ ast{\isacharunderscore}problem

%% This locale fixes the identifier \emph{problem} and asserts that it is of type \emph{ast{\isacharunderscore}problem}, i.e. it is an abstract syntax tree of a problem.
%% Also in this locale, we define the following function to access the set of state variables in a problem without needing to list \emph{problem} as one of the arguments since the function is defined in the locale.

%% \isacommand{definition}\isamarkupfalse%
%% \ astDom\ {\isacharcolon}{\isacharcolon}\ ast{\isacharunderscore}variable{\isacharunderscore}section\ \isanewline
%% \ \ \ \ \isakeyword{where}\ {\isachardoublequoteopen}astDom\ {\isasymequiv}\ case\ problem\ of\ {\isacharparenleft}D{\isacharcomma}I{\isacharcomma}G{\isacharcomma}{\isasymdelta}{\isacharparenright}\ {\isasymRightarrow}\ D{\isachardoublequoteclose}


\subsubsection{Well-Formedness}

The following predicate then defines a well-formed (partial) state  w.r.t. \emph{problem}\footnote{\emph{map} is the high-order function that applies a function to each element of a list.}.\\
\inputisabellesnippet{ast_problem_wf_partial_state_def}

For a state to be well-formed it has to: \begin{enumerate*} \item map every variable to a value only once, and \item map only valid variables to valid values for that variable.
\end{enumerate*}
Next, we define a well-formed operator as follows.\\
\inputisabellesnippet{ast_problem_wf_operator_def}

That predicate is a lambda expression that takes an operator.
This operator is well-formed iff \begin{enumerate*} \item the preconditions are a well-formed partial state, \item every effect changes a different variable, and \item for every effect: \begin{itemize*} \item its  preconditions are well-formed, \item the effect changes a state variable, \item it changes the variable to a permissible assignment, and \item if it requires that variable to have some value as a precondition, this value needs to be a permissible value for that variable.\end{itemize*} \end{enumerate*}

Lastly, based on the above predicates, a well-formed problem is characterised as follows:\\
\inputisabellesnippet{ast_problem_well_formed_def}

A problem is well-formed iff \begin{enumerate*} \item  its initial state assigns \emph{all} state variables to permissible values, \item the goal is a well-formed partial state, \item all operators have distinct names, and \item every operator is well-founded.\end{enumerate*}
\subsection{Semantics of SAS+ Planning Problems}

To formalise the semantics in the language of Isabelle/HOL we define predicates and functions that describe execution.
We first define two functions that, w.r.t. the problem, return the set of valid states and the set of valid states subsuming a given state.\\
\inputisabellesnippet{ast_problem_valid_states_def}\\
\inputisabellesnippet{ast_problem_subsuming_states_def}

Here, the syntax $\{x\in S.\ P\}$ denotes the subset of elements of set $S$ for which $P$ holds, and $f \subseteq_m g$ means that any argument mapped to a value by $f$ is mapped to the same value by $g$.

We now define two predicates that define when an operator and an effect are enabled in a given state, respectively.
An operator can execute in a given state iff: \begin{enumerate*} \item the operator belongs to the problem, \item the state at which the operator is executed subsumes the operator's prevail preconditions, and \item every effect's implicit precondition is subsumed by the state at which the operator is executed\footnote{\emph{map\_of}  takes a list of pairs and returns a function that corresponds to that list.}.\end{enumerate*}\\
\inputisabellesnippet{ast_problem_enabled_def}

An effect on the other hand is enabled at a state iff its preconditions are subsumed by the state.\\
\inputisabellesnippet{ast_problem_eff_enabled_def}

Now we define the function that returns the state resulting from executing an operator in a given state, assuming that the operator is enabled in that state.
The function takes an operator name.
If it exists in the given problem's list of operators then the operator is executed.
This is done by modifying every assignment of every variable in the given state to match its corresponding assignment in the operator's effects, given that the effect is enabled.
If the given operator name does not exist the function's return value is not defined\footnote{In the above definition \emph{{\isacharplus}{\isacharplus}} takes two functions and merges them, with precedence to its right argument. 
\emph{filter} is a high-order function that removes members that satisfy a given predicate from a list.}.\\
\inputisabellesnippet{ast_problem_execute_def}

Given the previous execution semantics, a valid path between two states is recursively defined as follows.
Note: every action in the path has to be enabled before executing it.
\inputisabellesnippet{ast_problem_path_to_simps}

Lastly, the following predicate specifies when is an action sequence a plan.\\
\inputisabellesnippet{ast_problem_valid_plan_def}

In the predicate above \emph{G} is the set of goal states and \emph{I} is a function that given a state variable returns the assignment of that variable in the initial state.


We note that Abdulaziz and Lammich proved sanity checking theorems for those semantics, ensuring that they are not contradictory and that there no unintended mistakes in those semantics.
For instance, they showed that executing a well-formed action on a valid state, leads to a valid state.
They also developed a plan validator that is formally verified wrt those semantics, which was used to check thousands of solutions of standard benchmarks.

\subsection{Main Theorem}
\label{sec:thms}

Now that we have defined the ASTs of the input and the output, we state the two main theorems regarding our verified SAT-based planner.
The first one states completeness, i.e.\ if there is a valid plan for a given planning problem whose length is shorter than the horizon, then there is a model that satisfies the SAT encoding of the planning problem.
The function \emph{compute\_encoding} that, given a horizon and a planning problem AST, computes the SAT encoding returns one of two things: either \emph{Inl} and a SAT formula, in which case the encoding was successfully computed, or \emph{Inr} and an error message.
In order to state completeness, we formalised the following two theorems.
%% That is, we have proved that our validator returns the value \emph{Inr\ ()} if and only if the problem is well-formed and the plan is valid. 
%% Otherwise, it returns a value \emph{Inl\ msg} containing an error message. Note that, for technical reasons, the problem is passed as explicit 
%% parameter here, while it was implicit via a locale before.\\
\inputisabellesnippet{encode_sound}
\inputisabellesnippet{encode_complete}
The two formalised theorems state that if the function successfully computes a formula, then there is a model for that formula, given that there is a plan for the problem whose length is bounded by the given horizon.
The other theorem states the encoding will not be computed if the given planning problem is either:\begin{enumerate*}\item not well-formed, \item has inconsistent effects, or \item has conditional effects.\end{enumerate*}

The theorems formalising the soundness of the encoding are below.
They state that, if there is a model that satisfies the SAT-encoding of the planning problem, then that model can be decoded with the function \emph{decode} into a valid plan for the planning problem,
\inputisabellesnippet{decode_sound}
\inputisabellesnippet{decode_complete}

Since the above theorems state that the encoding is sound and complete, and assuming that the kernel of Isabelle/HOL has no errors, then, in principle, there no need to see the details of both the encoding and the decoding functions, at least correctness-wise.
Nonetheless, in the rest of this paper we discuss some details about the encoding and the decoding functions to show what precise encoding we are using and to show that the encoding function is executable with a good running time, since, ultimately, there are many sound and complete SAT encodings of classical planning.
Also, we given an overview of our formal proof, e.g.\ how did we partition the translation steps, to demonstrate the different capabilities of Isabelle/HOL.
We also hope this this could be of help for similar formal verification efforts.

\section{Problem ASTs to an Abstract Representation}
\label{sec:FDtoSASP}

To be able to feasibly perform formal reasoning about the SAT encoding, we need to define it on a representation of planning problems that is more abstract than the previously defined AST.
Thus, we use a more abstract definition of multi-valued planning problems.

\subsection{Abstract planning problems}
\label{sec:abstractSASP}

In this representation a \emph{state} is a \emph{map} from a variable type to a domain type, where a map is an Isabelle/HOL function that is only defined for a subset of its domain type.
\inputisabellesnippet{state}

An \emph{assignment} is a pair, the first member of which is a variable and the second is a value.
\inputisabellesnippet{assignment}

Note: for states and assignments, \emph{'variable} and \emph{'domain} are not concrete type, but rather type parameters that can be instantiated.
These type variables are instantiated with the type \emph{nat} when we translate the AST planning problem to the abstract representation.

An operator is defined as a record with two lists of assignments, one as preconditions and the second as effects.

\inputisabellesnippet{sas_plus_operator}

A planning problem is a record with a list of variables, a list of operators, two lists of assignments as initial state and a goal partial state, and, more interestingly, a mapping \emph{range\_of} which maps a variable to a list of assignments it could possibly take.

\inputisabellesnippet{sas_plus_problem}

In analogy to the AST, we defined a predicate \emph{valid\_problem} that characterise what a valid problem is.
This boils down to stating that all assignments in the operators and the initial state are consistent (i.e.\ no one variable is assigned to more than one assignment), that they refer to variables that are in the problem's variables, and that every variable is assigned to an assignment that is in its range.

The execution semantics for this abstract representation of planning problems differs from that of the AST in one main way: the result of executing an action sequence is the same as the result of executing that sequence up to the first action with unsatisfied preconditions, i.e.\ all sequence of actions can successfully execute.
\textcolor{red}{Having such a total execution function makes some of the proofs easier.}

\subsection{The translation}
\label{sec:FDtoSASPsteps}

Translating ASTs of Fast-Downward's representation into abstract planning problems is done in two steps.
In the first step, we remove the effect preconditions and add them all to the operator's prevail preconditions, while leaving the names of the operators the same.
This is done with the function \emph{rem\_implicit\_pres\_ops}.
The following theorem shows that this translation is sound and complete.

\inputisabellesnippet{rem_implicit_pres_ops_valid_plan}

The next step is to translate an AST whose operators have no effect preconditions to the abstract problem.
The variables in the abstract problem are the list of natural numbers below the number of variables in the AST.
The goal is translated as follows.

\inputisabellesnippet{ast_problem_abs_ast_goal_def}

Since the AST initial state is a list of values, the initial state is translated by just adding to every one of those values the variable to which it is assigned as follows.

\inputisabellesnippet{ast_problem_abs_ast_initial_state_def}

Given two lists of equal length, the function \emph{zip} above returns a list of equal length, s.t.\ position $i$ in the output list is the pair of elements at position $i$ in the two given lists.

The range map is constructed by constructing a map that maps every variable to a list of natural numbers from 0 up to one less than the number of assignments this variable can take in the AST.
This is done using the following function:

\inputisabellesnippet{ast_problem_abs_range_map_def}
Note: \emph{abs\_ast\_variable\_section} gives the list of variables in the translated abstract problem. 


The operators are translated in a straightforward way too: the preconditions are taken as they are, and the effects are taken after removing the effect preconditions.

\inputisabellesnippet{ast_problem_abs_ast_operator_def}

The most difficult thing to prove show about translating the AST to the abstract representation was that translating a well-formed AST problem will result into a valid abstract problem.
Proving that depends on the assumption that the AST actions have no conditional actions.
This theorem is necessary for being able to use the different theorems about the abstract representation of planning problems.

The second theorem that we prove about this translation is that it preserves completeness, as follows.

\inputisabellesnippet{abs_ast_prob_valid_plan_then_is_serial_sol}

The third theorem that we prove about this translation is that it is sound.
To show soundness, the plan of the abstract problem is decoded into a plan for the AST problem using the following function.
The decoding function firstly removes actions whose preconditions are not satisfied, if the given action sequence is executed at the initial state of the problem.
This step is to accommodate for the difference between execution semantics of the AST and the abstract planning problem representation.
The next step replaces each one of the abstract operators with the equivalent name of the AST action, by firstly finding the equivalent AST operator and then extracting its name.
\inputisabellesnippet{ast_problem_decode_abs_plan_def}
\inputisabellesnippet{abs_ast_prob_is_serial_sol_then_valid_plan}


\subsection{Adding NO-OP Actions}

The next, rather simple, step in the translation is one where we add a NO-OP action, i.e.\ an action with no effects nor preconditions to the abstract problem.
This is necessary to have a completeness theorem that states the completeness of the encoding for horizons that are longer than the length of the existing plan (recall the assumption \emph{length $\pi$s $\leq h$}).
Showing completeness of this step is obvious.
To show it is sound, we use a decoding function that removes the NO-OP action from a given plan.


\section{Multi-Valued Problems to STRIPS}
\label{sec:sasptostrips}

\section{STRIPS to Abstract SAT Formulae}
\label{sec:stripstosat}

Explain Nipkows proof system library.


\section{Abstract SAT Formulae to DIMACS}
\label{sec:sattodimacs}

As a last step, we translate the high-level CNF formula to ASTs of DIMACS, i.e.\ a list of lists of integers.
We first change the atoms of the CNF formula from having a composite type, i.e.\ the algebraic data type that effectively states whether the literals refers to a state variable or an actions components, to atoms that are integers.
We use the following encoding to integers, which is adapted from \cite{knuth}'s encoding of numbers with arbitrary radixes.
\inputisabellesnippet{cnf_to_dimacs_var_to_dimacs_simps}
The above encoding function is defined for the two types of propositional variables: the ones representing actions and the ones representing state variables.
It depends on the horizon and the number of operators in the given abstract problem.
We prove a completeness theorem for this translation of the atoms of the formulae.
\inputisabellesnippet{sat_solve_sasp_planning_by_cnf_dimacs_complete}
Note: the function \emph{map\_formula} maps the atoms of a given formula to new atoms using its first argument, which is a function that maps one atom type to another.

To show soundness, we need to be able to take a model for a formula defined on integers and decode it into a model for the equivalent formula that is defined on structured variables.
The following theorem shows how to do that in general by composing a given model with the function that was used to change the atoms of a given formula.
\inputisabellesnippet{cnf_to_dimacs_dimacs_to_sat_plan}
%% To decode integer variables back to a structured atom, and use that function to show the soundness of this translation step.
%% %% \inputisabellesnippet{cnf_to_dimacs_dimacs_to_sat_plan_variable_def}
%% \inputisabellesnippet{sat_solve_sasp_planning_by_cnf_dimacs_sound}

The second part of this translation step is to the DIMACS file AST, which is done by the following two functions.
\emph{disj\_to\_dimacs} takes a disjunction of atoms, and returns a list of literals, and \emph{cnf\_to\_dimacs} takes a CNF formula and returns an equivalent list of lists of integers.
\inputisabellesnippet{disj_to_dimacs_simps}
\inputisabellesnippet{cnf_to_dimacs_simps}
Note: $@$ symbol stands for appending two lists.


The following lemma shows the completeness of this translation.

\inputisabellesnippet{model_to_dimacs_model_sound_exists}

To show its soundness, we devise the following function that converts a DIMACS model, i.e.\ a list of integers into a model for a formula.
\inputisabellesnippet{dimacs_model_to_abs_def}
That decoding function takes two arguments: a model \emph{dimacs\_M} in the DIMACS format (i.e.\  a list of integers) and a model \emph{M} for a formula (i.e.\ a valuation).
It goes through every entry in \emph{dimacs\_M} and updates \emph{M} with a corresponding assignment.
The following is the soundness theorem.
\inputisabellesnippet{model_to_dimacs_model_complete}
The initial valuation given to the model decoding function is one that maps every variable to false.

\section{Parsing Problems and Code Generation}
\label{sec:code}

Isabelle/HOL's code generator can generate implementations of functions in different languages, given those functions are computable.
The encoding and decoding functions are, except for \emph{map\_of}, within the computable fragment of Isabelle/HOL's language.
To be able to generate code, we provide a computable implementation of \emph{map\_of} based on an implementation of maps that uses red black tree and prove that this implementation is equivalent to the original.

We also provide an alternative implementation for the predicate \emph{dimacs\_model}.
Although the original definition is executable, it is rather inefficient since its worst case running time is quadratic in the number of propositional variables in the given formula.
This worst case running time translates to minutes for checking models of SAT formulae that encode relatively small planning problems (e.g.\ a gripper domain with 20 balls).
We reimplement this function using a red black tree implementation of sets, and prove that this implementation is equivalent to the slower one.

After we did those two reimplementations, we used Isabelle/HOL code generation abilities to generate Standard ML implementations of the functions \emph{encode} and \emph{decode}.

The remaining part is parsing a problem file in the format of Fast-Downward's translator format and, in the case of the decoding function, a list of integers representing a model for a SAT encoding of a planning problem.
We do that using an open source parser combinator library written in Standard ML\footnote{We would refer to its repository in case of acceptance.}, in which a description of the syntax of the language to parse is specified in terms of parsing primitives, that are themselves Standard ML functions.
We note, however, that parsing is a trusted part of our validator, i.e.\ we have no formal proof that the parser actually recognises the desired grammar and produces the correct abstract syntax tree. However, the parsing combinator approach allows to write concise, clean, and legible parsers, which can be easily checked manually.
