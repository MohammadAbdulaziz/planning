\section{Computing the Set of Instantiations}

Recapping, symmetries in $\Pi$ are the basis of a partition ${\cal P}$ of its domain $\dom(\Pi)$ into orbits.
That exposes the \emph{descriptive quotient}, $\Pi/{\cal P}$, an abstract problem whose variables correspond to orbits of concrete symbols.
Our task now is to compute a set of \emph{instantiations} of the quotient which \emph{cover} all the goal variables $\dom(G)$.
Called a covering set of isomorphic subproblems, by instantiating a quotient plan for each subproblem and concatenating the results we intend to arrive at a concrete plan.
We describe a pragmatic approach to obtaining that covering set. 
We establish that a covering set is not guaranteed to exists, and give an approach to {\em refining} partitions to mitigate that fact.
We derive a theoretical bound on the necessary size of a covering set, and prove that a general graph formulation of the problem of computing a covering instantiation is NP-complete.


\begin{definition}[Transversals, Instantiations]
\label{def:transversal}
%
A transversal $\tvsal$ is an injective choice function over ${\cal P}$ such that $\tvsal(c)\in c$ for every equivalence class $c\in{\cal P}$.
A transversal {\em covers} $v$ if $v \in \codom (\tvsal)$.
%
%We will write $\tvsal(v)$ to denote the element of $v$ chosen by $\tvsal$.
%% \end{definition}
%% \label{sec:sigma}
%% \begin{definition}[Instantiations]
%% \label{def:Inst}
%The instantiation of a problem $\Pi/{\cal P}$ is a transversal $\tvsal$ of ${\cal P}$.
%Where it exists, $\delta^{-1}(v)$ is called {\em quotient variable} of $v$.
If ${\cal P}$ is a partition of $\dom(\Pi)$, we refer to a transversal $\tvsal$ of ${\cal P}$ as an \emph{instantiation} of $\Pi/{\cal P}$.
An instantiation $\tvsal$ is \emph{consistent} with a concrete problem $\Pi$ if $\Img{\inst}{\Pi/{\cal P}} \subseteq \Pi$.
When we use the term ``problem'' discussing an instantiation $\inst$, we intend $\Img{\inst}{\Pi/{\cal P}}$.
Note that $\dom (\Img{\inst}{\Pi/{\cal P}})=\codom(\inst)$.
\end{definition}
\begin{myeg}
\label{eg:eg1}
Consider the set $s= \{a,b,c,d,e,f\}$, and the equivalence classes $c_1 = \{a,b\}$, $c_2 = \{c,d\}$ and $c_3 = \{e,f\}$ of its members.
For the partition ${\cal P} = \{c_1,c_2,c_3\}$ of $s$, $\tvsal_1 = \{a,c,e\}$ and $\tvsal_2 = \{b,c,f\}$ are two transversals.
\end{myeg}



 %% To identify a consistent instantiation of the quotient which covers a symbol from the concrete problem, we seek to solve the following for each goal variable in $\Pi.G$:

\noindent For each goal variable in the problem, our approach shall need to find a consistent instantiation of the quotient which covers that.
We thus face the following problem.

\begin{myprob}
\label{prob:prob3}
Given $\Pi$ and a partition ${\cal P}$ of $\dom(\Pi)$, is there an instantiation of $\Pi/{\cal P}$ consistent with $\Pi$ that covers a variable $v \in \dom(\Pi)$?
\end{myprob}

%%\noindent We make these ideas concrete with an example.

\begin{myeg}
\label{eg:eg2}
\newcommand{\egprob}{\Pi_{\ref{eg:eg2}}}
Consider the planning problem $\egprob$ with 
\begin{eqnarray*}
\egprob.I &=& \{x, a, b, m, n\}\\
\egprob.A &=& \{(\{x, a\},\{\negate{m}, \negate{x} \}), (\{\negate{x}, b\},\{\negate{n}, \negate{x}\}), (\{\},\{x\})\}\\
\egprob.G &=& \{\negate{m}, \negate{n}\}
\end{eqnarray*}
Let ${\cal P}$ be $\{p_1 = \{x\}, p_2 = \{a, b\}, p_3 = \{m, n\}\}$.
%, where $p_1 = \{x\}$, $p_2 = \{a, b\}$, and $p_3 = \{m, n\}$.
Therefore
\begin{eqnarray*}
(\egprob/{\cal P}).I &=& \{p_1, p_2, p_3\}\\
(\egprob/{\cal P}).A &=& \{ (\{p_1, p_2\},\{\negate{p_3}, \negate{p_1} \}), (\{\},\{p_1 \}) \}\\
(\egprob/{\cal P}).G &=& \{\negate{p_3}\}
\end{eqnarray*}
Let instantiation $\inst$ be $\{p_1\mapsto x, p_2\mapsto a, p_3\mapsto m\}$.
Then, $\inst$ covers $m$, and $\Img{\inst}{\egprob/{\cal P}}$ has
    $I = \{x,a,m\}$, 
    $A = \{(\{x,a\}, \{\negate{m}, \negate{x}\}), (\{\}, \{x\})\}$ and 
    $G = \{\negate{m}\}$.
Thus, $\Img{\inst}{\egprob/{\cal P}}\subseteq \egprob$, making $\inst$ a consistent instantiation and so a solution to Problem~\ref{prob:prob3} with inputs $\egprob$, ${\cal P}$ and $m$.
\end{myeg}


\subsection{Finding Instantiations: Practice}


We now describe how we obtain a set of isomorphic subproblems of $\Pi$ that covers the goal variables.
We compute a set of instantiations, $\insts$, of the quotient $\Pi/{\cal P}$. 
Our algorithm first initialises that set of instantiations, $\insts := \emptyset$.
Every iteration of the {\em main loop} computes a consistent instantiation that covers at least one variable $v \in \dom(\Pi.G)$.
This is done as follows:
we create a new (partial) instantiation $\tvsal = p_v \mapsto v$, where $p_v$ is the set in ${\cal P}$ containing  $v$.
Then we determine whether $\tvsal$ can be completed, to instantiate every set in ${\cal P}$, while being consistent at the same time.
%%
This determination is achieved by posing the problem in the theory of uninterpreted functions, and using a satisfiability modulo theory~(SMT) solver.
%%
Our encoding in SMT is constructive, and if a completion of $\inst$ is possible the solver provides it.
If successful, we set $\insts := \insts \cup \{\inst\}$ and $G := G \setminus \codom(\inst)$, and loop.
In the case the SMT solver reports failure, the concrete problem cannot be covered by instantiations of the descriptive quotient, and we report failure.
%%
In the worst case of $\dom(\Pi.G) = \dom(\Pi)$, our {\em main loop} executes $\Sigma_{p \in {\cal{P}}}|p| - |{\cal{P}}| + 1$ times, and hence we have that many instantiations.

%% Suppose ${{\cal{P}}} = O_G$ for some $G\leq Aut(\Pi)$, which is the case for the parition produced by nauty, we can show that the algorithm will iterate at most $max_{o \in O_{G}} |o|.ln(\Sigma_{o \in O_G}|o|)$, and this is based on the following theorem:
%%\input{rewrote_bound_proof.tex}

%We alluded earlier to problems and partitions that admit no consistent instantiations.
Scenarios need not admit a consistent instantiation.
%%That statement is now made concrete using an example, and then we demonstrate how to mitigate this.%a partition corresponding to orbits can be refined. 

\begin{myeg}
\label{eg:eg3}
\newcommand{\egprob}{\Pi_{\ref{eg:eg3}}}
Take $\egprob$ with 
\begin{eqnarray*}
\egprob.I &=& \{\negate{m}, \negate{n}, l\}\\
\egprob.A &=& \{ (\{\negate{m}\},\{ n, \negate{l}\}),(\{\negate{n}\},\{ m, \negate{l}\})\}\\
\egprob.G &=& \{\negate{l}, \negate{m}, \negate{n}\}
\end{eqnarray*}
\noindent Let ${\cal P} = \{p_1 = \{m,n\},p_2 = \{l\}\}$.
The quotient $\egprob/{\cal P}$ has \begin{eqnarray*} 
(\egprob/{\cal P}).I &=&\{\negate{p_1},p_2\}\\
(\egprob/{\cal P}).A &=&\{ (\{\negate{p_1}\},\{p_1, \negate{p_2}\})\}\\
(\egprob/{\cal P}).G &=& \{\negate{p_1}, \negate{p_2}\}\\
\end{eqnarray*}
There are no consistent instantiations of $\egprob/{\cal P}$ because $m$ and $n$ are in the same equivalence class and also occur together in the same action.
\end{myeg}

%% (\ie by splitting the equivalence classes into smaller ones)

Our example demonstrates a common scenario in the IPC benchmarks, where a partition ${\cal{P}}$ of $\dom(\Pi)$ does not admit a consistent instantiation because variables that occur in the same action coalesce in the same member of ${\cal{P}}$.
We resolve this situation by refining the partition produced using \nauty\ to avoid having such variables coalesce in the same orbit.
%%
For a $p \in {\cal{P}}$, consider the graph $\Gamma(p)$ with vertices $p$ and edges  $\{\{x,y\}\mid  \exists a \in \Pi.A \wedge \{x,y\} \subseteq \dom(a) \wedge x\neq y\}$.
%%
The {\em chromatic number} $N$ of $\Gamma(p)$ gives us the number of colours needed to colour the corresponding vertices $\hat{p}$ in the PDG. 
Where two variables occur in the same action, their vertices in $\hat{p}$ are coloured differently.
For every $p \in {\cal P}$, we use an SMT solver to calculate the required chromatic numbers $N$ and graph colourings for $\Gamma(p)$, then we colour the corresponding vertices in the PDG according to the computed $N$-colouring of $\Gamma(p)$.
%% different members of ${\cal P}$ have different colours in the resulting PDG.
Lastly, we again pass the PDG to a \nauty{} after it is recoloured.
In $4$ benchmark sets the thus-revised partition admits a consistent instantiation where the initial partition does not.
Although the {\em chromatic number} problem is NP-complete, this step is not a bottleneck in practice because the size of the instances that need to be solved is bounded above by the largest number of literals stated in any action.

\subsection{Finding Instantiations: Theory}

We first develop a theoretical bound on the number of required instantiations---by treating abstract covering transversals---that is tight compared to our pragmatic solution above.
To characterise the complexity of our instantiation problem, we then study the general problem of computing covering transversals.

\newcommand{\fcomp}{\ensuremath{\bullet}}
%\newcommand{\orbitsizes}{{\cal O}}
\newcommand{\orbitsizes}{S}

\begin{mythm}[B.~McKay, 2014]
\label{thm:upperBoundlog}
Let ${\cal G}$ be a group acting on a set $V$ (\eg, $\dom(\Pi)$).
Suppose $\tvsal$ is a transversal of $O$, a set of orbits induced by the action of ${\cal G}$ on $V$.
Take $\orbitsizes =\Sigma_{o\in O} |o|$ and $M=\max_{o\in O}|o|$.
Where $\fcomp$ is composition, there will always be a set of transversals ${\cal T}$ with size $\leq M\ln (\orbitsizes)$ such that \begin{enumerate} \item Each element $\tvsal'\in {\cal T}$ satisfies $\tvsal' = \perm\fcomp\tvsal$ for some $\perm\in {\cal G}$, and \item For every $v \in V$, it has an element $\tvsal'$ that covers $v$.
%\footnote{This theorem is due to Brendan D. McKay on 11/12/2014.}
\end{enumerate}
\end{mythm}
\begin{proof}
%\newcommand{\rset}{{\cal T}}
\newcommand{\rset}{R}
Take $N=M \ln ( \orbitsizes )$  and let $H$ be a subset of ${\cal G}$ obtained by drawing $N$ permutations of $V$ independently at random with replacement. 
%%
For any orbit $o\in O$, the probability for a $v \in o$ is not in $\codom(\perm\fcomp\tvsal)$ for a randomly drawn $\perm\in H$ is $1 - 1/|o|$.
Let ${\cal T} = \{\perm\fcomp\tvsal \mid \perm\in H \}$ and $\rset = \bigcup (\Img{\codom}{{\cal T}})$.
Drawing $N$ times from ${\cal G}$ to construct $H$, the probability that $v\not\in \rset$ is $(1 - 1/|o|)^{N}$.
%%
Consider the random quantity $Z = | V \setminus \rset|$ with expected value $ \mathbb{E}(Z) = \Sigma_{o \in O} |o| (1-1/|o|)^N$.
%%
Since $1 - x < e^{-x}$ for $x>0$, we obtain $\mathbb{E}(Z) < \Sigma_{o \in O} |o| e^{(-N/|o|)} \leq \orbitsizes e^{-N/M}  = \orbitsizes e^{-\ln(\orbitsizes)}$.
From  $xe^{-\ln(x)}=1$, it follows that $\mathbb{E}(Z) < 1$. 
Since $Z \in \mathbb{N}$, then probability $Z=0$ is more than $0$, and thus $N$ transversals of $O$ suffice to cover $V$. 
\end{proof}
\noindent We conjecture that a much smaller number of transversals is actually required, and in all our experimentation have found that the following conjecture is not violated:
\begin{myconj}
\label{conj:upperBound}
Let ${\cal G}$ be a group acting on a set $V$ (\eg, $\dom(\Pi)$).
Suppose $\tvsal$ is a transversal of $O$, a set of orbits induced by the action of ${\cal G}$ on $V$.
Take $M=\max_{o\in O}|o|$.
Where $\fcomp$ is composition, there will always be a set of transversals ${\cal T}$ with size $\leq M$ such that \begin{enumerate} \item Each element $\tvsal'\in {\cal T}$ satisfies $\tvsal' = \perm\fcomp\tvsal$ for some $\perm\in {\cal G}$, and \item For every $v \in V$, it has an element $\tvsal'$ that covers $v$.\end{enumerate}
\end{myconj}


%% Ste(ven)(phen) Fry 

We are left to formulate and study a general problem of instantiation, treating it as one of finding graph transversals.

%%This general formulation is intended to find application in treating symmetries beyond planning.

\begin{definition}[Consistent Graph Transversals]
\label{def:graphtransversal}
For a graph $\Gamma$ and a partition $\cal{P}$ of $V(\Gamma)$, a transversal $\tvsal$ of $\cal{P}$ is consistent with $\Gamma$ when an edge between $p_1$ and $p_2$ in $E(\Gamma/\cal P)$ exists iff there is an edge between $\tvsal(p_1)$ and $\tvsal(p_2)$ in $E(\Gamma)$.
\end{definition}
%% \begin{figure}
%% \begin{center}
%% \begin{tikzpicture}
%% \newcommand{\firstXshift}{-2}
%% \newcommand{\scale}{1.5}
%% \node (a) at (0.5/\scale+\firstXshift/\scale,-1/\scale) [varnode] { $a$ } ; 
%% \node (b) at (-0.5/\scale+\firstXshift/\scale,-1/\scale) [varnode] { $b$ } ; 
%% \node (c) at (-1/\scale+\firstXshift/\scale,0/\scale) [varnode] { $c$ } ; 
%% \node (d) at (-0.5/\scale+\firstXshift/\scale,1/\scale) [varnode] { $d$ } ; 
%% \node (e) at (0.5/\scale+\firstXshift/\scale,1/\scale) [varnode] { $e$ } ; 
%% \node (f) at (1/\scale+\firstXshift/\scale,0/\scale) [varnode] { $f$ } ; 
%% \draw [-] (a) -- (b) ; 
%% \draw [-] (b) -- (c) ; 
%% \draw [-] (c) -- (d) ; 
%% \draw [-] (d) -- (e) ; 
%% \draw [-] (e) -- (f) ; 
%% \draw [-] (f) -- (a) ; 
%% \draw [red] plot [smooth cycle] coordinates {(0.75/\scale+\firstXshift/\scale,-1/\scale) (0/\scale+\firstXshift/\scale,-1.25/\scale) (-0.75/\scale+\firstXshift/\scale,-1/\scale) (0/\scale+\firstXshift/\scale,-0.75/\scale)};
%% \draw [red] plot [smooth cycle] coordinates {(-0.49/\scale+\firstXshift/\scale+1.6/\scale,-1.149/\scale+1/\scale) (-1.08/\scale+\firstXshift/\scale+1.6/\scale,-0.625/\scale+1/\scale) (-1.24/\scale+\firstXshift/\scale+1.6/\scale,0.15/\scale+1/\scale) (-0.65/\scale+\firstXshift/\scale+1.6/\scale,-0.375/\scale+1/\scale)};
%% \draw [red] plot [smooth cycle] coordinates {(-1.25/\scale+\firstXshift/\scale+0.08/\scale,-0.15/\scale) (-1.08/\scale+\firstXshift/\scale+0.08/\scale,.625/\scale) (-0.49/\scale+\firstXshift/\scale+0.08/\scale,1.15/\scale) (-0.65/\scale+\firstXshift/\scale+0.08/\scale,0.375/\scale)};
%% \node (a) at (0.5/\scale-\firstXshift/\scale,-1/\scale) [varnode] { $a$ } ; 
%% \node (b) at (-0.5/\scale-\firstXshift/\scale,-1/\scale) [varnode] { $b$ } ; 
%% \node (c) at (-1/\scale-\firstXshift/\scale,0/\scale) [varnode] { $c$ } ; 
%% \node (d) at (-0.5/\scale-\firstXshift/\scale,1/\scale) [varnode] { $d$ } ; 
%% \node (e) at (0.5/\scale-\firstXshift/\scale,1/\scale) [varnode] { $e$ } ; 
%% \node (f) at (1/\scale-\firstXshift/\scale,0/\scale) [varnode] { $f$ } ; 
%% \draw [-] (a) -- (b) ; 
%% \draw [-] (b) -- (c) ; 
%% \draw [-] (c) -- (d) ; 
%% \draw [-] (d) -- (e) ; 
%% \draw [-] (e) -- (f) ; 
%% \draw [-] (f) -- (a) ; 
%% \draw [red] plot [smooth cycle] coordinates { (-0.75/\scale-\firstXshift/\scale,-1/\scale) (-0.49/\scale-\firstXshift/\scale+1.6/\scale,-1.149/\scale+1.4/\scale) (0.75/\scale-\firstXshift/\scale,-1/\scale)};
%% \draw [red] plot [smooth cycle] coordinates { (-1.25/\scale-\firstXshift/\scale+0.08/\scale,-0.19/\scale)(-0.49/\scale-\firstXshift/\scale+0.08/\scale,1.15/\scale) (-1.24/\scale-\firstXshift/\scale+1.9/\scale,0.08/\scale+1/\scale) };
%% \end{tikzpicture}
%% \end{center}
%% \caption[eg1fig] {\label{fig:graphtransversalEg}
%%  Two possible partitions of a hexagon's vertices.}
%% \end{figure}

\begin{myeg}
\label{eg:graphtransversalEg}
Take $\Gamma$  to be the hexagon that we have illustrated twice below in order to depict partitions ${\cal P}_1 = \{\{a,b\},\{c,d\},\{e,f\}\}$ and ${\cal P}_2=\{p' = \{a,b,f\},p'' = \{c,d,e\}\}$ on the left and right respectively.
\begin{center}
\begin{tikzpicture}
\newcommand{\firstXshift}{-2}
\newcommand{\scale}{1.5}
\node (a) at (0.5/\scale+\firstXshift/\scale,-1/\scale) [varnode] { $a$ } ; 
\node (b) at (-0.5/\scale+\firstXshift/\scale,-1/\scale) [varnode] { $b$ } ; 
\node (c) at (-1/\scale+\firstXshift/\scale,0/\scale) [varnode] { $c$ } ; 
\node (d) at (-0.5/\scale+\firstXshift/\scale,1/\scale) [varnode] { $d$ } ; 
\node (e) at (0.5/\scale+\firstXshift/\scale,1/\scale) [varnode] { $e$ } ; 
\node (f) at (1/\scale+\firstXshift/\scale,0/\scale) [varnode] { $f$ } ; 
\draw [-] (a) -- (b) ; 
\draw [-] (b) -- (c) ; 
\draw [-] (c) -- (d) ; 
\draw [-] (d) -- (e) ; 
\draw [-] (e) -- (f) ; 
\draw [-] (f) -- (a) ; 
\draw[red, fill opacity=0.2] ($ (a) !.5! (b) $) ellipse (6mm and 3mm);
\draw[red, rotate=55, fill opacity=0.2] ($ (c) !.5! (d) $) ellipse (6mm and 3mm);
\draw[red, rotate=120, fill opacity=0.2] ($ (e) !.5! (f) $) ellipse (6mm and 3mm);
%% \draw [red] plot [smooth cycle] coordinates {(1/\scale+\firstXshift/\scale,-1/\scale) (0/\scale+\firstXshift/\scale,-1.4/\scale) (-1/\scale+\firstXshift/\scale,-1/\scale) (0/\scale+\firstXshift/\scale,-0.6/\scale)};
%% \draw [red] plot [smooth cycle] coordinates {(-0.49/\scale+\firstXshift/\scale+1.6/\scale,-1.149/\scale+1/\scale) (-1.08/\scale+\firstXshift/\scale+1.6/\scale,-0.625/\scale+1/\scale) (-1.24/\scale+\firstXshift/\scale+1.6/\scale,0.15/\scale+1/\scale) (-0.65/\scale+\firstXshift/\scale+1.6/\scale,-0.375/\scale+1/\scale)};
%% \draw [red] plot [smooth cycle] coordinates {(-1.25/\scale+\firstXshift/\scale+0.08/\scale,-0.15/\scale) (-1.08/\scale+\firstXshift/\scale+0.08/\scale,.625/\scale) (-0.49/\scale+\firstXshift/\scale+0.08/\scale,1.15/\scale) (-0.65/\scale+\firstXshift/\scale+0.08/\scale,0.375/\scale)};
\node (a) at (0.5/\scale-\firstXshift/\scale,-1/\scale) [varnode] { $a$ } ; 
\node (b) at (-0.5/\scale-\firstXshift/\scale,-1/\scale) [varnode] { $b$ } ; 
\node (c) at (-1/\scale-\firstXshift/\scale,0/\scale) [varnode] { $c$ } ; 
\node (d) at (-0.5/\scale-\firstXshift/\scale,1/\scale) [varnode] { $d$ } ; 
\node (e) at (0.5/\scale-\firstXshift/\scale,1/\scale) [varnode] { $e$ } ; 
\node (f) at (1/\scale-\firstXshift/\scale,0/\scale) [varnode] { $f$ } ; 
\draw [-] (a) -- (b) ; 
\draw [-] (b) -- (c) ; 
\draw [-] (c) -- (d) ; 
\draw [-] (d) -- (e) ; 
\draw [-] (e) -- (f) ; 
\draw [-] (f) -- (a) ; 
\draw[red, rotate=35, fill opacity=0.2] ($  (a) !.33! (b) !.33! (f) $) ellipse (10mm and 4.5mm);
\draw[red, rotate=35, fill opacity=0.2] ($  (c) !.33! (d) !.33! (e) $) ellipse (10mm and 4.5mm);
%% \draw [red] plot [smooth cycle] coordinates { (-0.75/\scale-\firstXshift/\scale,-1/\scale) (-0.49/\scale-\firstXshift/\scale+1.6/\scale,-1.149/\scale+1.4/\scale) (0.75/\scale-\firstXshift/\scale,-1/\scale)};
%% \draw [red] plot [smooth cycle] coordinates { (-1.25/\scale-\firstXshift/\scale+0.08/\scale,-0.19/\scale)(-0.49/\scale-\firstXshift/\scale+0.08/\scale,1.15/\scale) (-1.24/\scale-\firstXshift/\scale+1.9/\scale,0.08/\scale+1/\scale) };
\end{tikzpicture}
\end{center}
The vertices of $\Gamma/{\cal P}_1$ (a 3-clique) and $\Gamma/{\cal P}_2$ (a 2-clique) are indicated above by red outlines.
There is no consistent transversal of ${\cal P}_1$ (LHS) because there is no 3-clique subgraph of $\Gamma$ with one vertex from each set in ${\cal P}_1$.
For $\Gamma/{\cal P}_2$ (RHS), $\tvsal_1 =\{p' \mapsto f, p'' \mapsto e\}$ is a transversal of ${\cal P}_2$ consistent with $\Gamma$, because the subgraph of $\Gamma$ induced by $\{e,f\}$ is a 2-clique with one vertex from each of $p'$ and $p''$.
\end{myeg}

\noindent A transversal of a well-formed $\hat{\cal P}$ consistent with $\Gamma(\Pi)$ is isomorphic to an instantiation of $\Pi/\cal P$ consistent with $\Pi$.
Accordingly, Problem~\ref{prob:prob3} is an instance of the following.
%Accordingly solving Problem~\ref{prob:prob3} for an input $\Pi$ and ${\cal{P}}$ can be done by solving the equivalent instance of the following.

\begin{myprob}
\label{prob:prob4}
Given $\Gamma$, a partition ${\cal{P}}$ of $V(\Gamma)$ and $v \in V(\Gamma)$, is there a consistent transversal of ${\cal{P}}$ which covers $v$?
\end{myprob}
\noindent We now derive the complexity of Problem~\ref{prob:prob4}.
We first show that the following problem is NP-complete, and use that result to show that Problem~\ref{prob:prob4} is also NP-complete.



\begin{myprob}
\label{prob:prob5}
Given graph $\Gamma$ and a partition ${\cal{P}}$ of $V(\Gamma)$, is there a transversal of ${\cal P}$ consistent with $\Gamma$?
\end{myprob}


\begin{mylem}
\label{lem:lem4}
Problem~\ref{prob:prob5} is NP-complete.
\end{mylem}
\begin{proof}
Membership in NP is given because a transversal's consistency can clearly be tested in polynomial-time.
We then show the problem is NP-hard by demonstrating a polynomial-time reduction from SAT.
Consider SAT problems given by formulae $\varphi$ in conjunctive normal form.
Assume every pair of clauses is mutually satisfiable---\ie, for clauses $c_1,c_2 \in \varphi$, for two literals $\ell_1 \in c_1$ and $\ell_2 \in c_2$ we have $\ell_1 \neq \lnot \ell_2$ (when this assumption is violated, unsatisfiability can be decided in polynomial-time).
Consider the graph $\Gamma(\varphi)$, where  $V(\Gamma(\varphi)) = \{v_{\ell c} \mid c \in \varphi, \;\ell \in c\}$, and  $E(\Gamma(\varphi)) =\{\{v_{\ell_1 c_1}, v_{\ell_2 c_2}\}\mid c_1,c_2\in \varphi, \ell_1 \in c_1, \ell_2 \in c_2, \ell_1 \neq \neg \ell_2 \}$.
Now let ${\cal P}=\{\{v_{\ell c}\mid \ell\in c\}\mid c \in \varphi\}$.
Note that ${\cal P}$ is a partition of $V(\Gamma(\varphi))$ and every set in ${\cal P}$ corresponds to a clause in $\varphi$.
Because all the clauses in $\varphi$ are mutually satisfiable, the quotient $\Gamma(\varphi)/{\cal P}$ is a clique.
Now we prove there is a model for $\varphi$ iff there is a transversal of ${\cal P}$ consistent with $\Gamma(\varphi)$.
%%
$(\boldsymbol\Rightarrow)$ A model ${\cal M}$ has the property  $\forall c\in \varphi.\;\exists\ell\in c. \;{\cal M} \models \ell$.
Due to the correspondence between sets in ${\cal P}$ and clauses in $\varphi$, a transversal $\tvsal$ of ${\cal P}$ can be constructed by selecting one satisfied literal from each clause.
Based on a model, $\tvsal$ will never select conflicting literals.
All members of $\codom(\tvsal)$ are pairwise connected, so $\tvsal$ is consistent with $\Gamma(\varphi)$ as required.
%%
$(\boldsymbol\Leftarrow)$  %By definition $\tvsal$ is consistent with $\Gamma(\varphi)$, so when it makes $c_1$ true by selecting $\ell_1$, all other clauses (all $c_1$'s neighbors, as the graph is a clique) will have compatible literals chosen to make those clauses true as well.
By definition $\tvsal$ is consistent with $\Gamma(\varphi)$, so the subgraph of $\Gamma$ induced by $\codom(\tvsal)$ is a clique.
Let ${\cal L}$ be literals corresponding to $\codom(\tvsal)$, and note that its elements are pairwise consistent.
A model ${\cal M}$ for $\varphi$ is constructed by assigning $v$ to $\top$ where $v$ occurs positively in ${\cal L}$, and to $\bot$ otherwise.
\end{proof}

\begin{mythm}
\label{thm:thm4}
Problem~\ref{prob:prob4} is NP-complete.
\end{mythm}
\begin{proof}
Consistency of a transversal $\tvsal$ is clearly polynomial-time testable, as is the coverage condition that $v \in \codom(\tvsal)$.
Thus, we have membership in NP. 
NP-hardness follows from the following reduction from  Problem~\ref{prob:prob5}~(P\ref{prob:prob5}).
%%
Taking ${\cal P}$ and $\Gamma$ as inputs to P\ref{prob:prob5}, construct the graph 
 $\Gamma'$, where: $V(\Gamma') = V(\Gamma) \cup \{v\}$, $v$ an auxiliary vertex; and $E(\Gamma') = E(\Gamma) \cup \{\{v,u\}\mid u\in V(\Gamma)\}$.
%%
There is a solution to P\ref{prob:prob5} with inputs $\Gamma$ and ${\cal P}$ iff P\ref{prob:prob4} is soluble with inputs $\Gamma'$, ${\cal P} \cup \{\{v\}\}$ (= ${\cal P'}$) and $v$.
%%
$(\boldsymbol\Rightarrow)$ If $\tvsal$ is a transversal of ${\cal P}$ consistent with $\Gamma$, then $\tvsal \cup \{\{v\} \mapsto v\}$($=\tvsal'$) is a transversal of ${\cal P'}$.
As $v$ is fully connected, $\tvsal'$ is consistent with $\Gamma'$.
$\tvsal'$ is a solution to P\ref{prob:prob4} because $v\in \codom(\tvsal')$.
%%
$(\boldsymbol\Leftarrow)$ If $\tvsal'$ is a solution to P\ref{prob:prob4} with inputs $\Gamma'$, ${\cal P'}$ and $v$, then $\tvsal'$ is a transversal of ${\cal P'}$, and also ${\cal P}$.
All edges in $E(\Gamma')$ are in $E(\Gamma)$, with the exception of those adjacent $v$, and since $\tvsal'$ is consistent with $\Gamma'$, then $\tvsal'$ is consistent with $\Gamma$.
Thus, $\tvsal'$ solves P\ref{prob:prob5}.
\end{proof}

In concluding, we note that the NP-hard canonicalisation problem---the optimisation problem posed at each state encountered by orbit search---is not known to be in NP. 
Our above results imply that exploitation of symmetry via instantiation poses a decision problem in NP.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "ijcai2015_submission"
%%% End:
