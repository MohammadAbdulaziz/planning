
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  INTRODUCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

The planning and model checking communities have for some time sought  methods to exploit symmetries that occur in transition systems.
%%
The quintessential planning scenario which exhibits symmetries is {\sc gripper}.
This comprises a robot whose left and right grippers can be used interchangeably in the task of moving a set of $N$ indistinguishable packages from a source location to a goal location.  
%%
Intuitively the left and right grippers are symmetric because if we changed their names, by interchanging the terms left and right in the problem description, we are left with an identical problem. 
%%
Packages are also interchangeable and symmetric.

%% \cite{pochter2011exploiting,shleyfman2015heuristics}~(\citeyear{shleyfman2015heuristics}) develop {\em structural symmetries} that are safe to exploit along side many common heuristics.

One method to exploit symmetry is to perform checking of properties of interest---\eg, goal reachability---in a {\em quotient} system, which corresponds to a (sometimes exponentially) smaller bisimulation of the system at hand. 
%%
\citeauthor{WahlD10}~(\citeyear{WahlD10}) review the related model checking literature.
Such methods were recently adapted for planning and studied by~\citeauthor{pochter2011exploiting}~(\citeyear{pochter2011exploiting}),~\citeauthor{domshlak2012enhanced}~(\citeyear{domshlak2012enhanced,DomshlakKS13}) and~\citeauthor{shleyfman2015heuristics}~(\citeyear{shleyfman2015heuristics}).
Related work about state-based planning in equivalence classes of symmetric states includes~\cite{GuereA01,FoxL99,FoxL02}.


Planning in a quotient system, a state $s$ is represented by a {\em canonical} element from its {\em orbit}, the set of states which are symmetric to $s$. 
Giving integer labels to packages in {\sc gripper}, when the search encounters a state where the robot is holding 1 package using 1 gripper, this is represented using the canonical state where, for example, the left gripper is holding package with identity ``1''.
Orbit search explores the quotient system by simulating actions from known canonical states, and then computing the canonical representations of resultant states.
That canonicalisation step requires the solution to the {\em constructive orbit problem}~\cite{clarke1998symmetry} which is {\em NP-hard}~\cite{eugene1993permutation}.
A key weakness, is that for each state encountered by the search an intractable canonicalisation operation is performed.
This is mitigated in practice by using approximate canonicalisation. 
By forgoing exact canonicalisation, one encounters a much larger search problem than necessary.
For a {\sc gripper} instance with $42$ packages, the breadth-first orbit search with approximate canonicalisation by~\citeauthor{pochter2011exploiting} reportedly performs $60.5K$ state expansion operations, far more than necessary. 

Following the seminal work by~\citeauthor{crawford1996symmetry}~(\citeyear{crawford1996symmetry}) and \citeauthor{BrownFP96}~(\citeyear{BrownFP96}), when planning \emph{via} constraint satisfaction, known symmetries are exploited by: \begin{enumerate*}\item  including symmetry breaking constraints, either directly as part of the problem expression~\cite{JoslinR97,rintanen2003symmetry}, or \item otherwise dynamically as suggested by~\cite{miguel2001symmetry} as part of a {\em nogood} mechanism.\end{enumerate*}
In {\sc gripper}, we can statically require that if no package is yet retrieved, then any retrieval targets package 1 with the left gripper.
Dynamically, having proved that no 3-step plan exists retrieving package 1 with the left gripper, then no plan of that length exists retrieving package $i\neq 1$ using either gripper.
Searching using the proposed dynamic approach is quite weak, as symmetries are only broken as nogood information becomes available.
A weakness of both approaches is that problems expressed as CSPs include variables describing the state of the transition system at different plan steps.
Existing approaches do not break symmetries across steps, and can therefore waste effort exploring partial assignments that express executions which visit symmetric states at different steps.


Our contribution is a novel procedure for domain-independent planning with symmetries.
%%
Following, \eg, \citeauthor{pochter2011exploiting}, in a  first step we infer knowledge about problem symmetries from the description of the problem.
%%
Then departing from existing approaches, our second step uses that knowledge to obtain a quotient of the concrete  problem description.
Called the {\em descriptive quotient}, this describes any element in the set of isomorphic subproblems which abstractly model the concrete problem.
Third, we invoke a planner once to solve the small problem posed by that descriptive quotient.
%%
In the fourth and final step, a concrete plan is synthesized by concatenating instantiations of that plan for each isomorphic subproblem.
%%
The descriptive quotient of the aforementioned 42-package \problem{gripper} instance is solved by breadth-first search expanding $6$ states.
Using our approach, a concrete plan is obtained in under a second. 
For comparison, \solver{LAMA}~\cite{richter2010lama} takes about 28 seconds.


The non-existence of a plan for the descriptive quotient does not exclude the possibility of a plan for the concrete problem.
Although sound, in that respect our approach is incomplete.
Having an optimal plan for the quotient does not guarantee optimality in the concatenated plan.
%%Thus, our approach is satisficing.
%%
Aside from computing a plan for the descriptive quotient, the computationally challenging steps  occur in preprocessing: \begin{enumerate}
\item Identification of problem symmetries from the original description, a problem as hard as graph isomorphism, which is not known to be tractable, and 
\item Computing an appropriate set of sub-problems isomorphic to the quotient.
\end{enumerate} 
We introduce the general version of the latter problem, and prove it is NP-complete.







%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ijcai2015_submission"
%%% End: 
