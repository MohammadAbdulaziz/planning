\documentclass[final]{beamer}
\usetheme{NICTAposter}
%for default nicta style use:
%\usetheme{NICTAposter}
\usepackage[orientation=portrait,size=a0,scale=1.4,debug]{beamerposter}
\usepackage[labelformat=empty]{caption}
\usepackage{subcaption}
\captionsetup{compatibility=false}
\setlength{\TPHorizModule}{1cm}
\setlength{\TPVertModule}{1cm}




% Standard packages

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}
\usepackage{alltt}
\usepackage[makeroom]{cancel}
% Setup TikZ

\usepackage{tikz}
\usetikzlibrary{arrows,positioning,decorations.markings}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\newtheorem{mylem}{Lemma}
\newtheorem{mythm}{Theorem}
\newtheorem{mydef}{Definition}
\newtheorem{myeg}{Example}

%\usepackage{holtexbasic}

%% \usepackage{alltt}
%% \usepackage{enumitem}
\usepackage[Euler]{upgreek}
\usepackage{xcolor,colortbl}
\renewcommand{\Pi}{\Uppi}
\newcommand{\act}{\ensuremath{a}}
\newcommand{\as}{\ensuremath{\dot{\pi}}}
\newcommand{\dep}[3]{\ensuremath{#2 \rightarrow #3}}
\newcommand{\negdep}[3]{\ensuremath{#2 \not\rightarrow #3}}

\newcommand{\proj}[2]{\ensuremath{#1{\downharpoonright}_{#2}}}
\newcommand{\vs}{\ensuremath{\mathit{vs}}}
%\newcommand{\sublist}[2]{ \ensuremath{#1} \preceq\!\!\!\raisebox{.4mm}{\ensuremath{\cdot}}\; \ensuremath{#2}}
\newcommand{\sublist}[2]{\ensuremath{#1} \preceq \ensuremath{#2}}
%\newcommand{\subscriptsublist}{\tiny\preceq\!\raisebox{.05mm}{\ensuremath{\cdot}}}
\newcommand{\subscriptsublist}{\preceq}

\newcommand{\pp}[1]{\text{\em #1}}
\newcommand{\lopt}{\textcolor[gray]{0.2}{[}}
\newcommand{\ropt}{\textcolor[gray]{0.2}{]}}
\usepackage[llparenthesis,rrparenthesis]{stmaryrd}
\newcommand{\Img}[2]{#1\llparenthesis#2\rrparenthesis}

\tikzset{
  varnode2/.style={draw},
  varnode2green/.style={draw,fill=green},
  varnode2red/.style={draw,fill=red},
  varnode2blue/.style={draw,fill=blue},
  varnode/.style={rectangle,draw},
  varnodegreen/.style={rectangle,draw,fill=green},
  varnodered/.style={rectangle,draw,fill=red},
  varnodeblue/.style={rectangle,draw,fill=blue},
  varnodepurple/.style={rectangle,draw,fill=purple},
  varnodeyellow/.style={rectangle,draw,fill=yellow},
  varnodeempty/.style={inner sep=0pt,fill},
  line/.style={=stealth,thick,fill=red},
  ourarrow/.style={>=stealth,thick,fill=red}
}

\title{\LARGE Exploiting Symmetries by Planning for a Descriptive Quotient}
\author{M. Abdulaziz, M. Norrish and C. Gretton$^1$}


%\vskip.01\textheight	

\begin{document}
\begin{frame}
\begin{columns}
\begin{column}{0.48\linewidth}

\begin{block}{Abstract}
\begin{itemize}
  \item We exploit symmetries in classical satisficing planning
  \item We plan using a quotient of the problem description
  \item Advantages over existing symmetry exploitation methods
  \begin{itemize}
    \item We eliminate symmetries computationally cheaply, and prior to plan search %% Descriptive quotient is computationally cheap to obtain %% Computing the descriptive quotient is significantly easier than additional processing required for other ways of explioting symmetries
    \item State space encountered during plan search can be  exponentially smaller %% The state space of the descriptive quotient can be sginificantly smaller than state spaces encountered by other methods
  \end{itemize}
\end{itemize}
\end{block}

\begin{block}{Our Approach}

\begin{enumerate}
\item Detect symmetries in problem description~[\textit{Graph Isomorphism}]

\item Obtain a {\em descriptive quotient}, a problem without symmetries

\item Compute {\em instantiations} of the descriptive quotient~[\textit{NP-Complete}]

\item {\em Augment} the goal of that quotient to enable resource re-use

\item Compute a plan for augmented quotient using (favourite) planner

\item Concatenate instantiations of that plan to solve original problem

\end{enumerate}
\end{block}


%% \begin{block}{STRIPS-like Representation of Transition System}
%% \begin{itemize}
%% \item A transition system ($\Pi$) can be defined as :
%%     \begin{itemize}
%%       \item Domain ($D$) : a finite set of Boolean variables representing the system states.
%%       \item Initial state ($I$) : a map from $D$ to Boolean
%%       \item Actions ($A$) : a finite set of tuples $(p,e)$
%%       \item Goal state ($G$) : a partial map from $D$ to Boolean
%%    \end{itemize}
%% \item A plan is a sequence of actions ($\as$) from $A$ that reaches $G$ from $I$.
%% \end{itemize}
%% \end{block}

\begin{block}{Problem~($\Pi$), Plan, State Space, Symmetries and Orbits}
\begin{itemize}
\item $I = \{x, $ \textcolor{green}{$ m$} $,$ \textcolor{red}{$ n$} $\}$,
\item $A = \{ (\{x\},\{ $ \textcolor{green}{$\overline{m}$} $, \overline{x} \}), (\{x\},\{$ \textcolor{red}{$\overline{n}$}$, \overline{x}\}), (\{\},\{x\})\}$, and
\item $G = \{ $\textcolor{green}{$\overline{m}$} $, $ \textcolor{red}{$\overline{n}$}$\}$.
\end{itemize}

\begin{figure}
\begin{center}
\begin{tikzpicture}
\node (3) at (0,0) [varnode] {\scriptsize 3} ;
\node (7) at (4,0) [varnode] {\scriptsize 7} ;
\node (1) at (8,2) [varnodegreen] {\scriptsize 1} ;
\node (2) at (8,-2) [varnodered] {\scriptsize 2} ;
\node (5) at (12,2) [varnodegreen] {\scriptsize 5} ;
\node (6) at (12,-2) [varnodered] {\scriptsize 6} ;
\node (0) at (16,0) [varnode] {\scriptsize 0} ;
\node (4) at (20,0) [varnode] {\scriptsize 4} ;

\draw [->,ourarrow] (3) -- (7) ;
\draw [->,ourarrow] (7) -- (1) ;
\draw [->,ourarrow] (7) -- (2) ;
\draw [->,ourarrow] (1) -- (5) ;
\draw [->,ourarrow] (2) -- (6) ;
\draw [->,ourarrow] (5) -- (0) ;
\draw [->,ourarrow] (6) -- (0) ;
\draw [<->,ourarrow] (0) -- (4) ;
\end{tikzpicture}
\end{center}
\end{figure}
Solution plan: $[(\{\},\{x\}) ; (\{x\},\{\textcolor{green}{\overline{m}}, \overline{x} \}); (\{\},\{x\}); (\{x\},\{\textcolor{red}{\overline{n}}, \overline{x}\})]$
\end{block}

\begin{block}{State-of-the-art Methods Plan via Orbit Search}
\begin{figure}
\begin{center}
\begin{tikzpicture}
\node (3) at (0,0) [varnode] {\scriptsize $O_3$} ;
\node (7) at (4,0) [varnode] {\scriptsize $O_7$} ;
\node (1) at (8,0) [varnodegreen] {\scriptsize $O_1$} ;
\node (5) at (12,0) [varnodegreen] {\scriptsize $O_5$} ;
\node (0) at (16,0) [varnode] {\scriptsize $O_0$} ;
\node (4) at (20,0) [varnode] {\scriptsize $O_4$} ;
\draw [->,ourarrow] (3) -- (7) ;
\draw [->,ourarrow] (7) -- (1) ;
\draw [->,ourarrow] (1) -- (5) ;
\draw [->,ourarrow] (5) -- (0) ;
\draw [<->,ourarrow] (0) -- (4) ;
\end{tikzpicture}
\end{center}
\end{figure}
\begin{figure}
\begin{center}
\begin{tikzpicture}
\node (3) at (0,0) [varnode] {\scriptsize 3} ;
\node (7) at (4,0) [varnode] {\scriptsize 7} ;
\node (1) at (8,2) [varnodegreen] {\scriptsize 1} ;
\node (5) at (12,2) [varnodegreen] {\scriptsize 5} ;
\node (0) at (16,0) [varnode] {\scriptsize 0} ;
\node (4) at (20,0) [varnode] {\scriptsize 4} ;
\draw [->,ourarrow] (3) -- (7) ;
\draw [->,ourarrow] (7) -- (1) ;
\draw [->,ourarrow] (1) -- (5) ;
\draw [->,ourarrow] (5) -- (0) ;
\draw [<->,ourarrow] (0) -- (4) ;
\end{tikzpicture}
\end{center}
\end{figure}
\begin{itemize}
  \item Only expand the lexmin state in every orbit during search
  \item Requires solution to NP-Hard \emph{constructive orbit problem} (COP)
  \item Approximate solution used in practice; Not all symmetries broken
\end{itemize}
\end{block}

\begin{block}{Descriptive Quotient}
\begin{itemize}
  \item Take the quotient of the factored system representation
  \item $\Pi/{\cal P}=$
{\small        
          $\langle I = \{O_x,  O_m \}$,
          $A = \{ (\{O_x\},\{\overline{O_m}, \overline{O_x} \}), (\{\},\{O_x\})\}$,
          $G = \{\overline{O_m}\}\rangle$}
\item The state-space of $\Pi/{\cal P}$ is:
\end{itemize}
\begin{figure}
\begin{center}
\begin{tikzpicture}
\node (1) at (0,0) [varnode] { 1} ;
\node (3) at (4,0) [varnode] { 3} ;
\node (0) at (8,0) [varnode] { 0} ;
\node (2) at (12,0) [varnode] { 2} ;
\draw [->,ourarrow] (1) -- (3) ;
\draw [->,ourarrow] (3) -- (0) ;
\draw [<->,ourarrow] (0) -- (2) ;
\end{tikzpicture}
\end{center}
\end{figure}
\end{block}

\begin{block}{Using the Descriptive Quotient}
\begin{itemize}
  \item To solve the original problem:
  \begin{itemize}
    \item Infer and solve the quotient of the problem description, then
    \item Repeatedly \emph{instantiate} plan for descriptive quotient to solve concrete problem
  \end{itemize}
  \item Advantages: No solving the COP for every state orbit
  \item Advantages: $SS(\Pi/{\cal P})$ is (exponentially) smaller than $SS(\Pi)/\hat{\cal P}$
  \item Neither sound (see resolution below),  complete, nor optimal
\end{itemize}
\end{block}


\begin{block}{[{\bf Theorem 4}] Soundness Conditions}
\begin{itemize}
  \item {\em Covering Instantiations:} Concrete problem has to be constructable from subproblems that are ``instantiations'' of the descriptive quotient
  \item {\em Sustainable Common Resources:} Precondition literals common between all instantiations have to by ``left as found''
  \begin{itemize}\item By {\em augmenting} goals with resource-sustainment conditions  \end{itemize}
\end{itemize}
\end{block}


\hrulefill
{\small
 \begin{enumerate}
   \item NICTA and Australian National University, Canberra, Australia.  
 \end{enumerate}}
     
\end{column}

\begin{column}{0.48\linewidth}

\vspace{-1ex}
\begin{block}{Covering Set of Quotient Instantiations~\textbf{[Step 3]}}%%Descriptive Quotient: Covering Quotient Instantiations}
\begin{itemize}
    \item{} [{\bf Theorem 2}] NP-Complete decision problem
    \item Instantiations of the descriptive quotient that:
    \begin{itemize}
      \item Are sub-problems of the concrete problem, {\bf and}
      \item Cover the goal of the concrete problem, ...
    \end{itemize}
    \item \textit{e.g.}
    \begin{itemize}
{\small
    \item $\pitchfork_1 = \{O_x \mapsto x, $ \textcolor{green}{$O_m \mapsto m$}$\}$
    \item $\Img{\pitchfork_1} {\Pi / {\cal P}} =$
           $\langle I = \{x,  m \}$,
           $A = \{ (\{x\},\{\overline{m}, \overline{x} \}), (\{\},\{x\})\}$,
           $G = \{\overline{m}\} \rangle$
    \item $\Img{\pitchfork_1}{\Pi / {\cal P}} \subseteq \Pi$
    \item $\pitchfork_2 = \{O_x \mapsto x, $ \textcolor{red}{$O_m \mapsto n$}$\}$
    \item $\Img{\pitchfork_2}{\Pi / {\cal P}}=$
           $\langle I = \{x,  n \},$
           $A = \{ (\{x\},\{\overline{n}, \overline{x} \}), (\{\},\{x\})\},$
           $G = \{\overline{n}\} \rangle$
    \item $\Img{\pitchfork_2}{\Pi / {\cal P}} \subseteq \Pi$}
    \end{itemize}
    \item{} [{\bf Theorem 1}] If there is a set of covering instantiations, then there is always a set of size $(\ln|D|)(\underset{p \in {\cal P}}{max}\; |p|)$
    \item{} [{\bf Conjecture 1}] If there is a set of covering instantiations, then there is always a set of size $\underset{p \in {\cal P}}{max}\; |p|$
\end{itemize}
\end{block}


\begin{block}{Augment Quotient Goal for Resource Re-use~\textbf{[Step 4]}}%Descriptive Quotient: Sustainable Common Resources}

\begin{itemize}
  \item \emph{Common needed literals}: every shared (by instantiations) action precondition that is satisfied by the initial state should be satisfied by goals of the instantiations.
  \item Why?
  \begin{itemize}
    \item Sometimes, even if there is a set of covering instantiations for the descriptive quotient, repetitive instantiations of the quotient solution do not solve the concrete problem, \textit{e.g.}
    \item $\as \equiv [(\{O_x\},\{\overline{O_m}, \overline{O_x} \})]$ is a solution to $\Pi/{\cal P}$, but $\Img{\pitchfork_1}{\as}$ and $\Img{\pitchfork_2}{\as}$ cannot be concatenated to solve $\Pi$.
    \item the common needed literal here is $\{x\}$.
  \end{itemize}
  \item To guarantee that this condition holds: add the common needed literals to the goal of the quotient.
  \item \textit{e.g.} the goal augmented quotient is
  \begin{itemize}
  \item  $\Pi^q=$
    {\small
          $\langle I = \{O_x,  O_m \}$,
          $A = \{ (\{O_x\},\{\overline{O_m}, \overline{O_x} \}), (\{\},\{O_x\})\}$,
          $G = \{\overline{O_m}, $\textcolor{blue}{$O_x$} $\} \rangle$}
    \item $\as \equiv [(\{O_x\},\{\overline{O_m}, \overline{O_x} \})\;; (\{\},\{O_x\})]$ is a solution to $\Pi^q$
    \item $\Img{\pitchfork_1}{\as}$ and $\Img{\pitchfork_2}{\as}$ can be concatenated in any order to solve $\Pi$.
  \end{itemize}
\end{itemize}



\end{block}

\begin{block}{Experiments}
\begin{itemize}
  \item 16 benchmarks with exploitable symmetry via a descriptive quotient
  \begin{itemize}
    \item Symmetries due to repetitions of identical subtasks
  \end{itemize}
  \item \# states expanded can be small compared to orbit search 
    \begin{itemize}
    \item Descriptive quotient: one robot,  one ball, and  blind search expands 6 states
    \item Contrast:  42-{\em package} {\bf Gripper}, [Pochter \emph{et al.}, 2011] expands $60.5K$ states 
      
  \end{itemize}
    \item Detection of symmetries can be a bottleneck
\end{itemize}
\begin{figure}[h]
\begin{subfigure}[b]{0.45\textwidth}
  \includegraphics[]{../Figures/speedup_plot.pdf}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
  \includegraphics[]{../Figures/slowdown_plot.pdf}
\end{subfigure}
\end{figure}
\end{block}

\begin{block}{Conclusion and Future Work}
\begin{itemize}
  \item Many IPC benchmarks with symmetries to be exploited efficiently using a quotient of problem description
  \item Future work:
  \begin{itemize}
    \item {\bf Optimality and Completeness}
    \item {\bf Beyond symmetry-based quotients} to more general partitions
  \end{itemize}
\end{itemize}
\end{block}

\end{column}


\end{columns}
\end{frame}
\end{document}
