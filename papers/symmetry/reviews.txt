Comments to author(s)
On page 2, definition 2, "from ariables to Booleans", typo on variable. And you never defined the domain of the initial state. Are you sure the definition of a valid state s is D(s) = D(I) rather than D(s)\subseteq D(I)? 
At the first paragraph of section 3, please give the citation of Pochter et al.
On page 2, definition 6, please use other notations for "v_?".
On page 3, definition 9, please use comma rather than full stop in the set definition of P. 
Typo on ".h" at the end of example 9. 
In example 4, "because both plans require \neg x initially", should be "x" rather than "\neg x".
On page 6, I could not understand "In 79% (96%) of cases 99% (95%) of the runtime of our approach is executing the base planner.". How come 3% of time is spent in instantiation and finding chromatic numbers?
Use eps file for figures. 


Summary of review
This paper proposes a method on symmetry elimination in the planning problem. They use the symmetry information to obtain a descriptive quotient which describes any element in the set of isomorphic subproblems to abstractly model the concrete problem. A concrete plan is finally obtained by concatenating instantiations of the plan for the descriptive quotient. 
The idea is novel and interesting. However, the definitions are technical and hard to follow. It would be great if examples can be given after definitions and theorems. Moreover, you only compare your method with initial planning problems.
Rebuttal Questions:
- For your experiments, you said "We identified benchmarks with soluble descriptive quotients whose solutions can be instantiated to concrete plans", but I would like to know how often it is the case that a problem cannot find soluble descriptive quotients whose solutions can be instantiated to concrete plans?
- Since your method is to eliminate symmetries, why don't you compare your method with other existing state-based symmetry exploiting methods using quotient system? Only comparing with the initial problem is not convincing enough of your method's efficiency. 


Comments to author(s)
-- GENERAL COMMENTS --

The paper proposes to plan with descriptive quotients extracted from the Problem Description Graph with a colored graph isomorphism procedure. This allows to remove symmetries before searching. Thus, the initial planning problem is decomposed into subproblems given by the different instantiations of the quotient. Under several conditions, defined by a theorem(4), if the goal of the quotient problem is augmented with some assignments in the initial state, the
concatenation of solutions to subproblems in any order is a solution to the initial problem.

This paper is rather impressive. I think it is a very good paper. The PDG has been used before to detect symmetries, but to my knowledge any other work build subproblems in the way this paper is proposing. Usually, detected symmetries are used to prune alternatives during search, and not before search. Also, the method to divide the orbits of variables in the same action and the use of the chromatic here is new. The paper includes both practical and theoretical contributions as well as several interesting concepts. The experimental section shows clearly the improvement when planning with quotients.

My only criticism of the paper is related to clarity. I know it is difficult to synthesize a paper like this in six pages. The paper makes an intensive use of notation and is difficult to follow. It can take many time, pen and paper to understand it. Knowing previous works on detecting symmetries helps but even in this case is very easy to be lost in the notation. The good part is that examples help a lot, so thanks for them.

The solution is given by concatenating solutions to subproblems. Theorem 4 proves solvability. What about optimality?


-- SPECIFIC COMMENTS --

1 Introduction:

- Which gripper version? In the version from IPC-11 learning, there could be more than two rooms. Balls have an initial room and the goals describe the final room for some balls. So, balls are not necessarily interchangeable.

- Introduction. First sentence of second paragraph: this sentence is difficult to understand at this point.

- "syntactic" description of the problem. Why quotes? The description of the planning problem is just syntactic, so I can not imagine that any other description can be used.

- ii) computing an appropriate set of instantiations of the quotient: this sentence is difficult to understand at this point

2 Definitions and Notations

- Why do you need to define explicitly the preconditions for a set of actions pre(A)?

- (Subproblem) It is strange that in a subproblem there is no relation between its goals and the goals of the problem subsuming it. Can you explain why?

- (Action execution) Can you explain why the notation is simplified if you consider actions applicable at every state and then consider the resulting state for non-applicable actions as invalid.

- The notation of last paragraph is confusing. Maybe this can be rewritten. I believe it would be better to say first that |m| represents the set of finite maps in m. Then, that the function f applied to a set of finite maps f(|m|) is the set of finite maps f(v)->b such that v->b appears in m, where the set structure of m is preserved.

3 Computing problem symmetries

- References missing for Definition 5 and Definition 6. Not clear if also Definition 7. I think the paper should make clear what are contributions and what is related work. Is there any contribution until section 4?

4 Computing the Set of Instantiations

- Definition 9 (transversals): I believe the concepts expressed by this definition are quite easy to understand, but the current form of the definition is complicated to follow. At least, it would be good if you explain the notion of consistency with words.

4.1 Finding instantiations: practice

- First paragraph: I imagine the goal is to find a set (all?) of consistent instantiations covering all goals. But if your main loop is for each goal variable, it seems it can assign more than one instantiation for the same partition. Also, it is not clear if you find all consistent instantiations covering goals or a subset of it. If it is a subset, what does this subset represent? Could you clarify this at this point?

- Example2: What does .h (after definition of (\Pi_2/\mathcal{P}).G) mean? Is that a typo?

5 Concrete plans from quotient plan

- Example 5. I do not fully understand the second part of definition 1 (ii) in this example. G_2 does not contain assignments in G_1. Assignments in G1 are m <-F and n<-F while G_2 contains m<-T and n<-T, so I do not see why G_2 projected to \P1 is G_1, I believe this is {m,n} instead of G1={\neg m, \neg n}. Sure I'm missing something. 

6 Experimental results

- How did you identify benchmarks with descriptive quotients whose solutions can be instantiated to concrete plans?

- What is the meaning of the parenthesis in 79%(96%) and 99%(95%). Are they different cases?

-- TYPOS --

page 2: ariables -> variables
section 5: "that cover the goal" -> that cover the goals
In Figure 1-Top plots -> Figure 1_top plots


Summary of review
In my opinion this is a very good paper. 




Comments to author(s)

This is a very interesting paper with a very good idea in it. There are some good results in it, mainly theoretical with some potentially interesting practical results. The good idea is to abstract a planning problem via symmetry and to search in the reduced problem, lifting up solutions to the original. 

I do have some mixed feelings about this paper however. The most important issue is that I am not sure that either the theoretical or empirical contributions are significant enough. I think forming the quotient problem is interesting, but there is no guarantee (as the authors say) that results will lift. And again as stated by the authors, finding the quotient can be (theoretically) hard. The approach is still interesting, but in a very computationally expensive domain like planning I might have hoped for better speedups and found more often than the authors report in their experiments. In fact if anything I was rather surprised (in fig 1 top) how little the state space was reduced. I had expected much more dramatic improvements.

On balance I move slightly towards marginal accept. I think this paper has an interesting idea in it, which may well be of interest going forward.

I may be missing something either in the text or as it is so obvious that the authors don’t think it needs to be said but … I couldn’t find anywhere a statement that a plan in the original problem necessarily corresponds to a correct plan in the quotient problem. Is this actually the case? If so and it’s not there can you state it clearly. If not, then isn’t it rather odd that the reduction is neither sound nor complete? Note in the latter case it could still be useful heuristically, but some discussion would be useful.

Related to this I wonder (a question for future work not this paper) if the symmetry is really critical to the approach. That is, a lot of planning problems have “almost symmetry”, see e.g. Porteous, Fox, Long, http://strathprints.strath.ac.uk/2693/1/strathprints002693.pdf. Since your method is not guaranteed to be complete, you might investigate using almost symmetries or similar for the quotient problem. Some theoretical properties would be lost but the practical value might be greater. 

I dislike the bottom of figure 1 intensely. It only shows instances where the new techniques win, instead of showing both wins and losses. While the authors at least state this clearly, there’s no justification for this choice, and one has to look in the main text to find out how often their techniques do in fact lose. Clearly a graph in some form should have been designed to show both. Since they state that their techniques win 68% of the time, this would still look good for them. 

The paper is generally well written. It did get rather lost in notation in the middle of the paper (for example I was not familiar with transversals or the pitchfork symbol), but I’m not sure how this could have been avoided. The authors gave examples to help, which is good. 

I didn’t find any mistakes in proofs but equally I am not an expert in planning, so perhaps could have missed them. 

The authors clearly have a good command of the literature and give lots of pointers throughout. 

“ariables” in Definition 2. 

In Definition 6 the “? \in … “ looks like a typo but I think it’s deliberate. Possibly pick another symbol? 

Question: you mention that the NPC step of graph colouring was not an issue in practice. It wasn’t clear to me if it was essential for you to get the true chromatic number, or just a bound on it? If the latter then you can always solve graph colouring non-optimally to some degree. If the former then perhaps you might just have got lucky in experiments.

Summary of review
Some pros and cons but I like the main idea enough to hesitantly suggest acceptance.
