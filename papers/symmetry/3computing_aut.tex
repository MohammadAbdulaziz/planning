
\section{Computing Problem Symmetries}
\label{sec:computingSymmetry}
To exploit problem symmetries we must first discover them.
We follow the discovery approach from~\cite{pochter2011exploiting}, restricting ourselves to Boolean-valued variables in $\dom(\Pi)$.
We assume familiarity with  \emph{groups, subgroups, group actions}, and \emph{graph automorphism groups}.
Symmetries in a problem description are defined by an \emph{automorphism group}.
\begin{definition}[Problem Automorphism Group]
\label{def:Aut}
The automorphism group of $\Pi$ is: $Aut(\Pi) = \{\perm\mid\Img{\perm}{\Pi} = \Pi\}$. Members of $\Aut(\Pi)$ are permutations on $\dom(\Pi)$.
%%
%Problem automorphisms correspond to automorphisms on the equivalent graphs.
%An automorphism group ${\cal G}$ partitions a graph/problem vertices/variables into equivalence classes corresponding to the orbits induced by the action of ${\cal G}$ over the vertices/variables.
%We write $O_{\cal G}$ for the partition corresponding to the set of orbits, and $H\leq {\cal G}$ for a subgroup $H$ of ${\cal G}$.
\end{definition}
A graphical representation of $\Pi$ is constructed so vertex symmetries in it correspond to variable symmetries in $\Pi$.
We follow the graphical representation introduced in \cite{pochter2011exploiting}.


\begin{definition}[Problem Description Graph (PDG)]
\label{def:Graphs}
The (undirected) graph $\Gamma(\Pi)$ for a planning problem $\Pi$, is defined as follows:
\begin{enumerate}
\item $\Gamma(\Pi)$ has two vertices, $v^\top$ and $v^\bot$, for every variable $v\in \dom(\Pi)$; two vertices, $a_p$ and $a_e$, for every action $a \in A$; and vertex $v_I$ for $I$ and $v_G$ for $G$.
\item $\Gamma(\Pi)$ has an edge
 between $v^\top$ and $v^\bot$ for all $v \in \dom(\Pi)$;
 between $a_p$ and $a_e$ for all $a \in A$;
 between $a_p$ and $v^*$ if $(v \mapsto *) \in\apre(a)$, and 
 between $a_e$ and $v^*$ if $(v \mapsto *)\in\aeff(a)$;
 between $v^*$ and $v_I$ if $(v \mapsto *)$ occurs in $I$; and
 between $v^*$ and $v_G$ if $(v \mapsto *)$ occurs in $G$.
\end{enumerate}
We write $V(\Gamma)$ for the set of vertices, and $E(\Gamma)$ for edges of a graph $\Gamma$.
\end{definition}
\noindent 
The automorphism group of the PDG, $\Aut(\Gamma(\Pi))$, is identified by solving an undirected graph isomorphism problem.
The action of a subgroup of $\Aut(\Gamma(\Pi))$ on $V(\Gamma(\Pi))$ induces a {\em partition}, called the \emph{orbits}, of $V(\Gamma(\Pi))$.
%The set of orbits corresponds to a {\em partition} of the PDG vertices.
We can now define our {\em quotient} structures based on partitions ${\cal P}$ of $\dom(\Pi)$.
%%In this work members of ${\cal P}$ shall be sets of symmetrically equivalent variables, called {\em equivalence classes}.

\begin{definition}[Quotient Problems and Graphs]
\label{def:quotientProb}
Given partition ${\cal{P}}$ of $\dom(\Pi)$, let ${\cal Q}$ map members of $\dom(\Pi)$ to their equivalence class in ${\cal P}$.
The descriptive quotient is $\Pi/{\cal P}=\Img{{\cal Q}}{\Pi}$, the image of $\Pi$ under ${\cal Q}$.
This is well-defined if ${\cal P}$ is a set of orbits.
We assume that quotient problems are well-defined.

For graph $\Gamma$ and a partition ${\cal P}$ of its vertices, the quotient $\Gamma/{\cal P}$ is the graph with a vertex for each $p \in {\cal P}$.
$\Gamma/{\cal P}$ has an edge between any $p_1,p_2 \in {\cal P}$ iff $\Gamma$ has an edge between any $v \in p_1$ and $u \in p_2$.
\end{definition}

To ensure correspondence between PDG symmetries and problem symmetries, we must ensure incompatible descriptive elements do not coalesce in the same orbit.
For example, we cannot have action precondition symbols and state-variables in the same orbit.

%% With problem graphs containing vertices corresponding to actions, as well as the original problem's $I$ and $G$, it is not obvious that a symmetry on a problem graph will necessarily give rise to a partition over just a problem's variables.
%% To ensure this, we require that \nauty{} only produce vertex-partitions that are \emph{well-formed}:

\begin{definition}[Well-formed Partitions]
A partition of  $V(\Gamma(\Pi))$ is \emph{well-formed} iff: \begin{enumerate}\item Positive ($v^\top$) and negative ($v^\bot$) variable assignment vertices only coalesce with ones of the same parity; \item Precondition ($a_p$) and effect ($a_e$) vertices only coalesce with preconditions and effects respectively, and \item Both $v_I$ and $v_G$  are always in a singleton.\end{enumerate}
A well-formed partition $\cal \hat{P}$ defines a corresponding partition $\cal P$ of $\dom(\Pi)$, so that $\Gamma(\Pi)/{\cal \hat{P}} = \Gamma(\Pi/{\cal P})$
\end{definition}

To ensure well-formedness, vertex symmetry is calculated using the {\em coloured} graph-isomorphism procedure (\nauty).
Vertices of distinct colour cannot coalesce in the same orbit.
Vertices of $\Gamma(\Pi)$ are coloured to ensure the orbits correspond to a well-formed partition.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "ijcai2015_submission"
%%% End:
