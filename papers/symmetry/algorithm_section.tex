%%\section{Computing a Plan}
\section{Concrete Plan from Quotient Plan}
\label{sec:algo}

\newcommand{\setd}{\backslash}

Having computed isomorphic subproblems $\insts$ that cover the goals of $\Pi$, a concrete plan is a concatenation of plans for members of $\insts$. 
However, this is not always straightforward.

\begin{myeg}
\label{eg:eg4}
\newcommand{\egprob}{\Pi_{\ref{eg:eg2}}}
Take $\egprob$, $\cal P$ and $\inst$ from Example~\ref{eg:eg2}.
Note $\inst' = \{p_1\mapsto x, p_2\mapsto b, p_3\mapsto n\}$ is also an instantiation of $\egprob/{\cal P}$ consistent with $\egprob$.
Observe that $\{\Img{\inst}{\egprob/{\cal P}}, \Img{\inst'}{\egprob/{\cal P}} \}$ covers the concrete goal $G = \{\negate{m}, \negate{n}\}$.
A plan for $\egprob/{\cal P}$ is $\as' = (\{p_1,p_2\}, \{\negate{p}_3, \negate{p}_1 \})$ and its two instantiations are $\Img{\inst}{\as'} = (\{x,a\}, \{\negate{m}, \negate{x}\})$ and $\Img{\inst'}{\as'} = (\{x,b\}, \{\negate{n}, \negate{x}\})$.
Concatenating $\Img{\inst}{\as'}$ and $\Img{\inst'}{\as'}$ in any order does not solve $\egprob$ because both plans require $x$ initially, but do not establish it.
%%
To overcome this issue in practice, before we solve $\egprob/{\cal P}$ we augment its goal with the assignment $p_1\mapsto \top$.
\end{myeg}


%% \noindent Prior to solving the descriptive quotient $\Pi/{\cal P}$, we augment its goal with the assignment $p_1$, we ensure that concatenation will yield a valid concrete plan.
We now give conditions under which concatenation is valid, and detail the quotient-goal augmentation step we use to ensure validity in practice.
%%
We shall use the well-known notion of {\em projection}, where $\proj{Y}{X}$ is a version of $Y$ with all elements mentioning an element not in $X$ removed.\footnote{A formal definition was given recently by~\citeauthor{helmert2014merge}~(\citeyear{helmert2014merge}).}
In addition, we will write $\probproj{Y}{X}$ to mean $\proj{Y}{\dom(X)}$, for the common case where we wish to project with respect to all the variables in the domain of a problem, state or set of assignments.


\begin{definition}[Needed Assignments, Preceding Problem]
\label{def:OrdProb}
Needed assignments, ${\cal N}(\Pi)$, are assignments in the preconditions of actions and goal conditions that also occur in $I$, \ie, ${\cal N}(\Pi)=(\apre(A) \cap I) \cup (G \cap I)$. 
%%
Problem $\Pi_1$ is said to precede $\Pi_2$, written $\Pi_1\preced \Pi_2$, iff
\[
  \probproj{G_1}{{\cal N}(\Pi_2)} = \probproj{(\probproj{I_2}{\Pi_1})}{{\cal N}(\Pi_2)} \;\land\;
  \probproj{G_1}{\Pi_2} = \probproj{G_2}{\Pi_1}
\]
In words, \begin{enumerate} \item The needed assignments of $\Pi_2$ which a plan for $\Pi_1$ could modify occur in $G_1$, and \item $G_2$ contains all the assignments in $G_1$ which a plan for $\Pi_2$ might modify.\end{enumerate}
%For any state $s$, if $\dom(s) \subseteq \dom(\Pi)$ then $\proj{s}{\dom(\Pi) \cap V} = \proj{s}{V}$.
\end{definition}


%% \noindent Follows is an example that demonstrates the $\preced$ relation.

\begin{myeg}
\label{eg:eg5}
\newcommand{\egpione}{\Pi_{\ref{eg:eg2}}}
\newcommand{\egpitwo}{\Pi_{\ref{eg:eg3}}}
Consider $\egpione$ and $\egpitwo$ from Examples~\ref{eg:eg2} and \ref{eg:eg3} respectively.
${\cal N}(\egpione) = \{x,a,b\}$ and ${\cal N}(\egpitwo) = G_1=\{\negate{m}, \negate{n}\}$.
Since $\probproj{G_1}{{\cal N}(\egpitwo)} = G_1$, $\probproj{(\probproj{I_2}{\egpione})}{{\cal N}(\egpitwo)} = G_1$, $\probproj{G_1}{\egpitwo} = G_1$ and $\probproj{G_2}{\egpione} = G_1$, we have $\egpione \preced \egpitwo$.
%However, $\probproj{G_2}{{\cal N}(\egpione)} = \{\}$, $\probproj{(\probproj{I_1}{\egpitwo})}{{\cal N}(\egpione)} = \{\}$, $\probproj{G_1}{\egpione} = \{\}$ and $\probproj{G_2}{\egpitwo} = \{\}$, and accordingly $\egpitwo \nleq \egpione$.
\end{myeg}
\noindent Writing $\as_i\cat\as_j$ for concatenation of plans $\as_i,\as_{i+1}\dots\as_j$, a simple inductive argument gives the following:

\begin{mylem}
\label{lem:thm1_lem}
Let $\Pi_1\dots\Pi_N$ be a set of problems satisfying $\Pi_{j} \preced \Pi_{k}$ for all $j < k \leq N$.
For $1 \leq i \leq N$ if state $s$ satisfies $I_i \subseteq s$  and $\as_i$ solves $\Pi_i$, then $\probproj{\exec{s}{\as_1\cat\as_N}}{G_i} = G_i$.

%% For a list of planning problems $\problist$ that is sorted w.r.t. $\probleq$ and a state $s$ such that $\forall \Pi \in \problist. \Pi.I \subseteq s$, then if $f$ maps every member of $\problist$ to a plan that solves it, then $\forall \Pi \in \problist.\ \proj{\exec{s}{ \map\ f\ \problist}}{\Pi.G} = \proj{\exec{\Pi.I} {f(\Pi)}}{\Pi.G}$.
\end{mylem}

\Omit{
\begin{proof}
By induction on $N$.
First, note $\probproj{\exec{s}{\as_i}}{G_i} = \probproj{\exec{I_i}{\as_i}}{G_i}$.
Our proof is by induction on $N$, and the base case $N=1$ is trivial.
We now show that if the theorem is true for $N$, then the theorem also holds for $N+1$.
%We have two cases that we need to prove: i) $\proj{\exec{s}{\as_1..\as_N}}{G_N} = \proj{\exec{I_i}{\as_1..\as_N}}{G_N}$,and ii) $\proj{\exec{s}{\as_1..\as_N}}{G_i} = \proj{\exec{I_i}{\as_1..\as_N}}{G_i}$, for $1 \leq i\leq N-1$.
Below, let $s' =\exec{s}{\as_1\cat\as_N}$.

Note ${\cal N}(\Pi_{N+1}) \subseteq s'$, because for each $i$ : \begin{enumerate} 
\item assignments modified by $\as_i$ whose variables are the subject of a needed assignment from $\Pi_{N+1}$ are in  $G_i$, because $\Pi_i \preced \Pi_{N+1}$ and thus $\probproj{G_i}{{\cal N}(\Pi_{N+1})} = \probproj{(\probproj{I_{N+1}}{\Pi_i})}{{\cal N}(\Pi_{N+1})}$  
\item the goals of any $\Pi_i$ are in $s'$ because the inductive hypothesis gives $\probproj{s'}{G_i} = G_i$
\item any assignments in $I_{N+1}$ not modified by exeuction $\as_1\cat\as_N$ from $s$ are in $s'$, because $I_{N+1} \subseteq s$.
\end{enumerate}
%%All preconditions in $\as_{N+1}$ that are in $I_{N+1}$ are also in $s'$, because: \begin{enumerate*}\item $\Pi_i \preced \Pi_{N+1}$, \item the induction hypothesis, \ie, $\probproj{s'}{G_i} = \probproj{\exec{s}{\as_i}}{G_i}$, and \item $I_{N+1} \subseteq s$, hold.\end{enumerate*}
Because $\as_{N+1}$ is a plan for $\Pi_{N+1}$, $\probproj{\exec{s'} {\as_{N+1}}} {G_{N+1}} = G_{N+1}$.

Since $\Pi_i \preced \Pi_{N+1}$, any assignment in $s'$ that is in some $G_i$ is also in $\exec{s'}{\as_{N+1}}$.
The inductive hypothesis gives $G_i=\probproj{s'}{G_i}$ thus  $G_i = \probproj{\exec{s'}{\as_{N+1}}}{G_i}$.
Because $\exec{s}{\as_1\cat\as_{N+1}} = \exec{s'}{\as_{N+1}}$ we have $\probproj{\exec{s}{\as_1\cat\as_{N+1}}}{G_i} = G_i$.
\end{proof}
}

\noindent Take problem $\Pi$ and a set of problems $\probset$, we say that $\probset$ covers $\Pi$ iff $\forall g \in G. \;\exists \Pi' \in \probset. \;g \in G'$ and $\forall \Pi' \in \probset. \;\Pi' \subseteq \Pi$. \Ie, every goal from $\Pi$ is stated in one or more members of $\probset$, a set of subproblems of $\Pi$.

\begin{mythm}
\label{thm:thm1}
Consider a set $\Pi_1\dots\Pi_N$ of problems that covers $\Pi$,  satisfying $\Pi_{j} \preced \Pi_{k}$ for all $j < k \leq N$.
For $1 \leq i \leq N$ if $\as_i$ is a plan for $\Pi_i$, then $\as_1\cat\as_N$ is a plan for $\Pi$.
\end{mythm}

\noindent This theorem follows directly from the fact that the set of problems covers $\Pi$ and from Lemma~\ref{lem:thm1_lem}.


We now address the question: When can plans for a set of isomorphic subproblems be concatenated to provide a concrete plan?
%Firstly, we represent a set of isomorphic subproblems, as a cononical problem and a set of instantiations of the canonical problem, where each of them represents a subproblem.
%%We derive a condition on the quotient problem and the set of instantiations that is suffecient for Theorem~\ref{thm:thm1} to apply to the set of isomorphic problems (the instantiations of the quotient).
%%Consisder the following two definitions.
We provide sufficient conditions in terms of the concepts of {\em common} and {\em sustainable} variables. 

\begin{definition}[Common Variables]
\label{def:CommVar}
For a set of instantiations $\insts$, the set of \emph{common variables}, written $\CommVar \insts$, comprises variables in $\bigcup_{\inst \in \insts}\codom(\inst)$ that occur in the ranges of more than one member of $\insts$.

%% of $\insts$ is the set of variables in $X$ that each is in the range more than one element of $\insts$, written as $\CommVar \insts$.
\end{definition}

\begin{definition}[Sustainable Variables]
\label{def:REversible}
A set of variables $V$ in a problem $\Pi$ is sustainable iff $\proj{I}{V}=\proj{G}{V}$.
\end{definition}


\begin{mythm}
\label{thm:thm2}

Take problem $\Pi$, partition ${\cal P}$ of $\dom(\Pi)$ where the quotient $\Pi/{\cal P}$($= \Pi'$) is well-defined, with solution $\as'$, and consistent instantiations $\insts$.
Suppose $\{\Img{\inst}{\Pi'}\mid\inst\in\insts\}$($=\probset$) covers $\Pi$, and $\Img{{\cal Q}}{\CommVar \insts} \cap \dom({\cal N}(\Pi'))$---\ie, based on Definition~\ref{def:quotientProb}, the orbits of common variables from needed assignments---are sustainable in $\Pi'$. 
Then any concatenation of the plans $\{\Img{\inst}{\as'}\mid\inst\in\insts\}$ solves $\Pi$.


\end{mythm}


\begin{proof}
%Our proof is by instantiating Corollary~\ref{cor:corSet} with the problem $\Pi$, the set of problems $\{\Img {\inst}{\Pi'} \mid\ \inst \in \insts\}$ ($=\probset$) and the map $f$ that maps every $\Img{\inst}{\Pi'}$, for $\inst \in \insts$, to a plan $\map\ \inst\ \as'$ that solves.

Identify the elements in $\insts$ by indices in $\{1..|\insts|\}$.
Let $k \in \{1..|\insts|\}$ and $\Pi_k = \Img{\inst_k}{\Pi'}$, and note $\codom(\tvsal_k) = \dom(\Pi_k)$.
Take $\inst_i,\inst_j\in\insts$, where $i,j \in \{1..|\insts|\}$ and $i \neq j$.
We first show that $\Pi_i \preced \Pi_j$.
%%
For any $p\in{\cal P}$, $\inst_i(p)=\inst_j(p)$ if $\inst_i(p)\in \codom(\inst_i) \cap \codom(\inst_j)$.
Therefore, $\probproj{I_i}{\Pi_j}=\probproj{I_j}{\Pi_i}$ and $\probproj{G_i}{\Pi_j}=\probproj{G_j}{\Pi_i}$, providing the right conjunct in Definition~\ref{def:OrdProb}.
For the left conjunct, note $\dom({\cal N}(\Pi_j)) \subseteq \dom(\Pi_j)$ and $\dom(\Pi_i) \cap \dom(\Pi_j) \subseteq \CommVar\insts$.
The sustainability premise---${\cal Q}(\CommVar \insts) \cap \dom({\cal N}(\Pi'))$ is sustainable in~$\Pi'$---then provides that $\dom(\Pi_i) \cap \dom({\cal N}(\Pi_j))$ is sustainable in $\Pi_i$---\ie,  $\probproj{I_i}{{\cal N}(\Pi_j)} = \probproj{G_i}{{\cal N}(\Pi_j)}$.
%% Thus, we have the first conjuct $\probproj{G_i}{{\cal N}(\Pi_j)} = \probproj{(\probproj{I_j}{\Pi_i})}{{\cal N}(\Pi_j)}$ in 
Thus $\probproj{G_i}{{\cal N}(\Pi_j)} = \probproj{(\probproj{I_j}{\Pi_i})}{{\cal N}(\Pi_j)}$, and we conclude $\Pi_i\preced \Pi_j$.
Since a plan $\Img{\inst_k}{\as'}$ solves $\Img{\inst_k}{\Pi'}$,  $(\Img{\inst_k}{\Pi'}).I \subseteq \Pi.I$, therefore as per Theorem~\ref{thm:thm1} a solution to $\Pi$ is $\as_1\cat\as_N$.\end{proof}


%% \begin{proof}
%% %Our proof is by instantiating Corollary~\ref{cor:corSet} with the problem $\Pi$, the set of problems $\{\Img {\inst}{\Pi'} \mid\ \inst \in \insts\}$ ($=\probset$) and the map $f$ that maps every $\Img{\inst}{\Pi'}$, for $\inst \in \insts$, to a plan $\map\ \inst\ \as'$ that solves.
%% We now show that for any $\inst_1,\inst_2 \in \insts$, $\Pi_1 \preced \Pi_2$, where $\Pi_1 = \Img{\inst_1}{\Pi'}$ and $\Pi_2 = \Img{\inst_2}{\Pi'}$.
%% Since $\Pi_1$ is the instantiation of a quotient problem over a partition, any variable $v \in \codom(\inst_1) \cap \codom(\inst_2)$ will be the assignment of the same orbit $p \in {\cal P}$ and accordingly $\probproj{I_1}{\Pi_2}=\probproj{I_2}{\Pi_1}$ and $\probproj{G_1}{\Pi_2}=\probproj{G_2}{\Pi_1}$, which proves the right conjunct in Definition~\ref{def:OrdProb}.
%% Because ${\cal Q}(\CommVar \insts) \cap \dom({\cal N}(\Pi'))$ is sustainable w.r.t.~$\Pi'$ and $\codom(\inst_1) \cap \codom(\inst_2)\subseteq \CommVar\insts$,  $\codom(\inst_1) \cap \codom(\inst_2) \cap \dom({\cal N}(\Pi_2))$ is sustainable w.r.t. $\Pi_1$.
%% Since $\codom(\inst_1) = \dom(\Pi_1)$, $\codom(\inst_2) = \dom(\Pi_2)$, and $ \dom({\cal N}(\Pi_2)) \subseteq \dom(\Pi_2)$, therefore $\dom(\Pi_1) \cap \dom({\cal N}(\Pi_2))$ is sustainable w.r.t.~$\Pi_1$, \ie,  $\probproj{I_1}{{\cal N}(\Pi_2)} = \probproj{G_1}{{\cal N}(\Pi_2)}$.
%% Then $\probproj{G_1}{{\cal N}(\Pi_2)} = \probproj{(\probproj{I_2}{\Pi_1})}{{\cal N}(\Pi_2)}$, which proves that $\Pi_1\preced \Pi_2$.
%% Since for any $\inst \in \insts$, it is easy to that $\Img{\inst}{\as'}$ solves $\Img{\inst}{\Pi'}$ and based on Theorem~\ref{thm:thm1}, we have a proof.
%% \end{proof}

%

In practice we take $V^*=\Img{{\cal Q}}{\CommVar \insts} \cap \dom({\cal N}(\Pi/{\cal P}))$, and augment the goal of the quotient $\Pi/{\cal P}$ by adding $\probproj{(\Pi/{\cal P}).I}{V^*}$. 
Call the resulting problem $\Pi^q$ and its solution $\as^q$.
Theorem~\ref{thm:thm2} shows that any concatenation of the plans $\{\Img{\inst}{\as^q}\mid\inst\in\insts\}$ solves $\Pi$. Thus, the proposed approach is sound.

%%The following example shows how Theorem~\ref{thm:thm2} works in practice.

\begin{myeg}
\label{eg:eg6}
\newcommand{\egprob}{\Pi_{\ref{eg:eg2}}}
Take $\egprob$, $\cal P$, $\inst$ and $\inst'$ from Example~\ref{eg:eg4}.
There is one common variable, $\{x\}=\CommVar \insts$ from the orbit $\{p_1\}=\Img{\quotfun}{\CommVar \insts}$.
Here ${\cal N}(\egprob/{\cal P}) = \{p_1, p_2\}$, and the orbit of the common needed variable is $\{p_1\} = \Img{\quotfun}{\CommVar \insts} \cap {\cal N}(\egprob/{\cal P})$.
To solve $\egprob$ via solving $\egprob/{\cal P}$,  we augment the goal $(\egprob/{\cal P}).G$ with the assignment to $p_1$ in $(\egprob/{\cal P}).I$.
The resulting problem, $\egprob^q$, is equal to $\egprob/{\cal P}$ except that it has the goals $\egprob^q.G=\{p_1,\negate{p}_3\}$.
A plan for $\egprob^q$ is $\as^q = (\{p_1,p_2\}, \{\negate{p}_3, \negate{p}_1\}) (\{\}, \{p_1\})$, and two instantiations of it are $\Img{\inst}{\as^q} = (\{x,a\}, \{\negate{m}, \negate{x}\}) (\{\}, \{x\})$ and $\Img{\inst'}{\as^q} = (\{x,b\}, \{\negate{n}, \negate{x}\}) (\{\}, \{x\})$.
Concatenating $\Img{\inst}{\as^q}$ and $\Img{\inst'}{\as^q}$ in any order solves $\egprob$.
%%
%%For the concatenation of instantiations of plans for $\Pi/{\cal P}$ solve $\Pi$, we augment its goal with the assignment to $\{p_1\}$ in $\Pi/{\cal P}.I$.
%%That augmented problem, $\Pi^q$, has the same initial state and actions as $\Pi/{\cal P}$, and its goal is $\Pi^q.G=\{p_1,\negate{p}_3\}$.
%Note $\inst' = \{p_1\mapsto x, p_2\mapsto b, p_3\mapsto n\}$ is also an instantiation consistent with $\egprob$.
%Observe that $\{\Img{\inst}{\Pi/{\cal P}}, \Img{\inst'}{\Pi/{\cal P}} \}$ covers the concrete goal $G = \{\negate{m}, \negate{n}\}$.
\end{myeg}

%% Theorem~\ref{thm:thm2} 

%% Based on Theorem~\ref{thm:thm2}, augmenting the quotient problem's goal with the parital state $\proj{\Pi/O_{{\cal G}}.I}{}$ guarantees that the concatenation of instantiations of the modified quotient's plan, if there is any, in any order is a plan for $\Pi$.
%% %Note that any order of concatenations of the instantiated quotient plans will achieve $\Pi.G$.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "ijcai2015_submission"
%%% End:


