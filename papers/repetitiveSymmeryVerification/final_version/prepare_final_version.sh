#!/bin/bash
cd ../
../../HOL/bin/Holmake
cd final_version/
#rm sources/*.tex
rm *.png
rm *.sty
rm *.tex
projDir=../../../
cp $projDir/papers/repetitiveSymmeryVerification/*.tex .
cp $projDir/papers/latexIncludes/macros.tex .
cp $projDir/papers/latexIncludes/orcid.pdf .
cp $projDir/papers/latexIncludes/cc-by.pdf .
cp $projDir/papers/latexIncludes/lipics-logo-bw.pdf .
cp $projDir/papers/latexIncludes/lipics-v2019.cls .
cp $projDir/papers/latexIncludes/holtexbasic.sty .
cp $projDir/papers/latexIncludes/paper.bib .
#Flatten
perl latexpand/latexpand repetitiveSymmeryVerification.tex > out
rm *.tex
rm *~
mv out paper.tex
mkdir sources
./aaai_script.sh paper.tex
cp sources/*.sty .
rm -rf sources
rm pgf*
latexmk -f -gg -pdf paper.tex

## Make sure that the produced pdf is correct
# pdftotext paper.pdf
# pdftotext ../TD.pdf
# DIFF=$(diff ../TD.txt paper.txt) 
# if [ "$DIFF" != "" ] 
# then
#     echo "The pdf is not exactly as it should be. Check diff.txt."
#     echo $DIFF > diff.txt
#     exit
# fi

rm paper.pdf
pdflatex paper.tex
bibtex paper
pdflatex paper.tex
pdflatex paper.tex
#rm *.pdf
rm *.log
rm testOnly
rm eshell
rm __tmp*
rm aaai17.sty aaai.dot aaai.sty *.out IEEEtran.cls
rm *.idf *.mf *.log *.fd *.tfm *pk *.idx *.def *.out
rm *.pfb *.clo *.eps JAR_edit_report* *.ins flatten *.map *.dtx *.fdb_latexmk *.afm *.fls *.xml *.gf *diff* *.dtx README *.blg *. *.aux 

rm -rf source
mkdir source
cp holtexbasic.sty *.tex *.png *.bib lipics-logo-bw.pdf lipics-v2019.cls source
zip -r source.zip source/
