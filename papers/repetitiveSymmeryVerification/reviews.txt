ITP2019 reviews:
===============

After acceptance.


Dear Charles Gretton,

On behalf of the ITP2019 Program Committee, we are happy to inform you that
your submission titled

A Verified Compositional Algorithm for AI Planning

has been accepted for presentation at the conference. Congratulations!

Reviews for your submission are appended below.  Please take the reviewers'
comments and suggestions into account when preparing the final version of
your paper, which is due JULY 1st. Detailed instructions about preparation and submission of the camera-ready version will be sent shortly.

If you have any additional questions, please feel free to
contact us.

Sincerely,
  John O'Leary
  John Harrison
  Andrew Tolmach

SUBMISSION: 34
TITLE: A Verified Compositional Algorithm for AI Planning


----------------------- REVIEW 1 ---------------------
SUBMISSION: 34
TITLE: A Verified Compositional Algorithm for AI Planning
AUTHORS: Mohammad Abdulaziz, Charles Gretton and Michael Norrish

----------- Overall evaluation -----------
SCORE: 2 (accept)
----- TEXT:
Summary: The paper presents formalization and verification of a compositional AI algorithm for planning using HOL4. This is achieved by dividing the planning problem in to isomorphic sub-problems and verifying them, and finally concatenating the sub-solutions to reason about the verification of the complete problem. The algorithm is based on the earlier work of the authors, and the formal analysis presented in this work resulted in identifying a bug in the algorithm. The formalization also shows the application of the algorithm to infinite states, thus allowing for scalability.

Strengths: The theorems presented show the utility of theorem prover for formalization and analysis of AI planning. The use of formal techniques for verification of AI algorithms is appreciated.

Weakness: Although, the paper is overall well written, yet, it should have more motivational text and a  comparison section with earlier works is also missing.  

Major Comments:
1.      Motivational text about the use of a theorem prover (or formal methods) for analysis of AI algorithms should be added. How the presented approach would compare (in terms of effort, scalability etc.) to the use of formal techniques like model checking that are inherently suitable for modeling and analyzing transition-based systems? Similarly, a few comments should be added about the choice of HOL4.
2.      It would be great to include a flowchart for the AI algorithm highlighting the difference from their earlier version, followed by description. In the current form, I do not find a proper description of the algorithm before the actual formalization.

[*CGDONE*]3.      In Definition 4 of the planning problem, the codomain of goal states is Boolean, whereas the description claims that the codomains are not restricted to Boolean.

4.      The related work section could be improved. Similarly, a comparison of the current work with the application of other formal techniques in AI planning could be added. 
5.      Although, the over all paper is well written, yet, a few typos and suggestions are given in the minor comments.

Minor Comments:

[*CGDONE - sidestep*]1.      Formally define “descriptive quotient”.
[did nothing]2.      Write comma “,” after “i.e.” and “e.g.” throughout the text.
[did nothing]3.      Page 3, Definition 1: What do v and b represent? In general, it is recommended to explicitly describe / define the variables or symbols when introduced in the text.
[*CGDONE* - using figure]4.      Use either “Figure” or “Fig.” for labeling and referencing. 
[*CGDONE* - done]5.      The figures should be moved closer to the occurrence of the related text, otherwise it adversely affects readability.
[*CGDONE* - done]6.      Page 5, What do “O”s represent in Figure 2? I think it is important to explain the execution of orbit search (in Figure 2) in order to appreciate the difference between the state-spaces of Figures 1, 2 and 3.
[*CGDONE* - removed]7.      Page 5, Define partial state.
[*CGDONE* - removed]8.      Page 6, Avoid using a.k.a. Use “i.e.” or full form “also known as” if possible.
[*CG???? Is he talking about our paper?]9.      Page 7, Para 1, “The first step of formally develop these …..”. Rephrase.
[did nothing]10.     Page 7, Try to replace “—” by “,”.
[*CGDONE*]11.     Page 9, “…. Section 6, below.” Change to “…. Section 6.”
[*CGDONE*]12.     Page 12, “…. as it does in a goal state”. Change to “…. as they do in a goal state”.
[did nothing]13.     Page 12, “…. as follows.”. Change to “…. as follows:”.

Score: 2, Accept



----------------------- REVIEW 2 ---------------------
SUBMISSION: 34
TITLE: A Verified Compositional Algorithm for AI Planning
AUTHORS: Mohammad Abdulaziz, Charles Gretton and Michael Norrish

----------- Overall evaluation -----------
SCORE: 2 (accept)
----- TEXT:
The paper presents a HOL4 verification of a compositional planning
algorithm proposed by the authors and published in 2015 as an IJCAI
paper. The planning setting and its terminology are introduced and
formally defined in HOL4, the main idea of using symmetry to break
large problems into smaller ones is explained, followed by definitions
and lemmas needed to show the correctness of the algorithm.

The verification has found and corrected a bug in the original paper
concerning spurious actions. The bug seems to be a corner case that
has never manifested in testing. The formalization has also led to a
generalization of the formal setting that might be applicable to
temporal/numerical planning, etc.

The formal HOL4 SML code has about 15000 lines and seems nontrivial
and corresponding to the paper (I have not checked too much).

I believe this is useful and quite extensive work. Verification of AI
algorithms is a very timely task. I also like the fact that this is a
relatively new result considered important by a top AI conference, and
that the authors themselves have decided to formalize it.  It also
seems to be the first formalization of such planning algorithm.

On the other hand, the theories, mathematics and methods required to
carry out the formalization are quite standard. There don't seem to be
serious technical or foundational issues that would require
introduction of new methods or advanced math apparatus in HOL4.

One obvious question is executability and/or code extraction. Perhaps
the paper could discuss how much harder (if feasible at all) it would
be to have a formalization that can be run, i.e., results in a
verified executable version of the algorithm?



----------------------- REVIEW 3 ---------------------
SUBMISSION: 34
TITLE: A Verified Compositional Algorithm for AI Planning
AUTHORS: Mohammad Abdulaziz, Charles Gretton and Michael Norrish

----------- Overall evaluation -----------
SCORE: 3 (strong accept)
----- TEXT:
The paper presents a formalization and verification of a earlier
proposed compositional algorithm for AI planning inside the HOL4
theorem prover. The planning algorithm works on a particular class of
AI planning problems that can be characterized as factored transition
systems -- (a transition maps variables to values of some domain). An
assumption of the algorithm is that a given problem has a (computable)
descriptive quotient, which is used to under-approximate the problem
into sub-problems. These sub-problems are solved separatedly and their
solutions are composed together to obtain a solution to the original
problem. It has been shown earlier that the compositional algorithm
performs superior than the other non-compositional algorithms. In this
paper, the authors describe a theory (with detailed definitions and
examples) to formalize the algorithm in HOL4. This formalization not
only proves the correctness of the algorithm, but also enabled the
authors to present the earlier algorithm in a more general
setting. Moreover, the authors were able to find a bug in the original
algorithm. The authors have also provided a link to the HOL4 library
of the work.

I think the paper is relevant to the conference. Overall the paper is
well-written with proper use of the formalism in the definitions and
of examples for better understanding. It flows very smoothly. I
very much liked the section 2 about the preliminaries. I also
appreciate that the authors have mentioned that they found an error in
their work while proving the algorithm correctness. I would strongly
recommend for acceptance of the paper. 


Here are some minor comments / Suggestions:

[did nothing, had no internet. Mohammad, I suggest adding the citation]- page 1: there is also a newer extended version of the SMV
  language [nuxmv].

- page 4: line 2-4 of the first paragraph is a bit confusing, please
  rephrase it.

[*CGDONE - I made a change to address this around Thm2*]- page 5: descriptive quotient is used without properly introducing
  the definition. I see it is discussed later with an example, but it
  does not give a precise definition.

[did nothing]- page 6: please briefly explain how the descriptive quotient is
  calculated and also give a reference to it.

[not sure what this means]- page 14: it is mentioned that in (\alpha, \beta) planning problem
  \beta can be values of any type. Can it also be constraints?

- page 16: the connection of the presented theory with
  the temporal planning, numeric planning, hybrid planning or model
  checking is not well explained. These other problems can also use
  constraints to symbolically represent the states, and this paper
  presents the theory as value assigments. Can the theory handle such
  problems? Please briefly discuss.

[Michael, thoughts?!]- Perhaps, the Lean theorem prover can also be mentioned.


[nuxmv] Roberto Cavada, Alessandro Cimatti, Michele Dorigatti, Alberto
Griggio, Alessandro Mariotti, Andrea Micheli, Sergio Mover, Marco
Roveri, Stefano Tonetta. The nuXmv symbolic model checker. CAV 2014.


ITP2018 reviews:
================


----------------------- REVIEW 1 ---------------------
PAPER: 61
TITLE: A Verified Compositional Reachability Algorithm for Factored Transition Systems
AUTHORS: Mohammad Abdulaziz, Michael Norrish and Charles Gretton

Overall evaluation: 0 (borderline paper)

----------- Overall evaluation -----------
The authors present their formalisation in HOL4 of their own, previously
published, planning algorithm: Given a planning problem decomposed into
several identical partitions, they show that given a solution for a partition,
a solution for the planning problem can be derived by removing some
the transitions. The proven theorem is slightly more general than the
original (paper) version in which a mistake they have found.



1 Main issues
════════════════

  1. The main issue is that the paper isn't particularly informative or
     insightful. I don't feel I've learned anything, nor did I enjoy going
     through what sometimes looks like an Archive of Formal Proof entry
     with some comments thrown in between. The paper is very technical
     and formalistic. Simple examples, diagrams, anything that would have
     made it more appealing is missing. The writing is robotic and tiring;
     every section seems to start with "We now describe" or "We now
     review", which is not the hallmark of a seductive narrative. Along
     similar lines, to an ITP audience it's hard for me to understand the
     value of spending 2 pages on presenting a counterexample, when
     I barely understand what the planning problems are in the first
     place (due to the formalistic presentation). Instead, I want to hear
     more about how you used HOL4, what idioms you developed, etc.!
     Given that [1,2,3,5] already exist (and I have read some of them),
     I wonder what the novelty is, from an ITP perspective.

  2. Given that the main scientific value of formalization work is that
     it constitutes a case study in the use of a proof assistant,
     I would be interested in more discussion: it does not happen so often
     that people formalise their own proofs. Were there some subtle
     points that you missed? Any comments on proof step that should be
     added to make sure that no error like the one described in Section
     5 can happen again? Will it change how you will write your future
     paper proofs? The sentence "It is worth noting that HOL4 is able to
     prove the above goal entirely automatically" is not helping much,
     without more context as to which lemmas and proof methods were
     used here!

  3. The HOL4 theorems are not explained and not understandable
     without prior knowledge of HOL4:
     ⁃ functions (like MEM and FOLDR) must be defined. From the context,
       I assume that "MEM x xs" means "x ∈ set xs" (but given that set has been
       introduced on the previous line, I still don't know why the
       easier-to-understand version is not used).
     ⁃ same for notations: what is ":α"?
     ⁃ even when presenting HOL4 output, tuning the notation is
       necessary (space after comma, like in "ex(x,FLAT (MAP f Π))"
       should be "ex(x, FLAT (MAP f Π))", additional line breaks,…).

  4. Even theorems where all functions can be guessed like Theorem 2,
     are hardly readable and should be explained by a sentence (a
     reference to [4] does not count).

  Would it be possible to generalise the HOL4 Theorem 2, such that not
  all problems are in a quotient, but only a subset of the decompostion
  is?


2 Minor problems
════════════════

  ⁃ \usepackage{cite} to sort references like [6,21,22] instead of the
    [22,21,6]
  ⁃ "with SAT-based planning methods" add a reference
  ⁃ "a set δ of actions (α \mapsto β) × (α \mapsto β) → bool": why two
    different arrows. If HOL4 does not have proper sets, I would advise
    to write "(α \mapsto β) × (α \mapsto β) set".
  ⁃ use a single numbering for all theorems "HOL4 Definition 4" should
    come after Definition 3. And please do that for all
    publications. How am I supposed to guess that Theorem 3 from [4]
    appears after Example 6, Lemma 3, and before Definitions 12
  ⁃ "If the state satisfies the preconditions then the state resulting
    from the execution is the same as the original state, but amended by
    the effects of the executed action, otherwise the result of the
    execution is the same as the original state." Yes this is exactly
    the definitions from the previous page. What is the point of
    repeating it?
  - The abstract is quite long, especially the two sentences "The
     algorithm, which we originally published in the context of AI
     planning, is based on an abstraction called “the descriptive
     quotient”, which effectively partitions a system into identical
     subsystems before solving it. This formalisation helped us find and
     correct errors in the algorithm as well as show that, in theory, it
     is general enough to be applied to domains with infinite states,
     instead of only finite ones".

2.1 Section 2
─────────────

2.1.1 Section 2.2 Planning Problems in HOL4
╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌

  ⁃ "In the above definition, Π.I is the initial state": is a "Given a
    planning problem Π of type (α, β) planningProblem" missing?
  ⁃ HOL4 Definition 5 what is "planning_prob_union"? and if it is the
    operator written ∪ on the previous line, why the notation change?


2.1.2 Section 2.3 Planning via descriptive quotient
╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌

  ⁃ how are descriptive quotients calculated? Heuritics? please add a
    reference.


2.2 Conclusion
──────────────

  ⁃ "induction on the tail of the list". Strange and not so interesting
    remark: beyond being more natural because of the recursive
    definition, I don't see why there would be any difference in the
    proof obligations.


3 Typos
═══════

  - "subproblems" vs. "sub-problems", inconsistencies throughout the
    paper. Same, perhaps, for other sub- words.
  ⁃ "that issue is referred to as the state explosion problem ([12])":
    remove the parentheses.
  ⁃ "seek formalise an answer": seek _to_ formalise
  ⁃ Definition 2: "systetm" should be system (only one t), "δ We write":
    remove the first δ.
  ⁃ Definition 2: "ex(x, π) = if p ⊄ x then x else e \uplus x" why not
    use the same font for the if in the HOL4 definitions?
  ⁃ \vec{\pi})"
  ⁃ HOL4 Definition 6: two spaces between p and e in "∃ p e"; "v ∈ D"
    should be indented (only as much as the first parentheses)
  ⁃ "when can plans for two problems be" : "when plans for two problems
    can be"
  ⁃ "then the a plan Π_1" either "a" or "the"
  ⁃ HOL4 Lemma 4 indent the ∀Π from the conclusion at the correct level,
    i.e. at the level of the ∀Π from the assumption, not more.
  ⁃ HOL Definition 11: why not write the "x ∈ D" on the same line as "∀
    x"
  ⁃ |- ex(x. \vec{\pi}) = ex(x, rem-condless (x, [], \vec{\pi})) beyond
    the space after the comma, why not use "⇔" instead of "="?
  ⁃ "An instantiation is a map that maps objects": remove the "is a map
    that" (maps usually map objects to other objects…)
  ⁃ "was a surprisingly challenge to": "was surprisingly challenging"?
  ⁃ "The outline is as follows": "the outline of the proof"
  ⁃ HOL4 Definition 20: add a line break
  ⁃ "This is shown in the following theorem … (∩_v (set Δ) D(Π^q) ∩
    N_D(Π^q))": why is the space between "∩_v" and "(set Δ)" larger than
    the space between "D(Π^q)" and "∩ N_D(Π^q)"? and around the "∩"
    operator?
  - "Counter Example" -> "Counter-Example" or "Counterexample"
  ⁃ "in this section , we elaborate more": remove the space.
  ⁃ A solution for Π^q is π^q = […] please add a line break to make the
    formula fit.
  ⁃ "Doczkalet al": missing space between "Doczkal" and "et al"
  ⁃ "Wu et al.[38]" add a (non-breakable) space ("~")
  - Although/,// the algorithm
  ⁃ "they cannot be tolerated in safety critical…, thus, their existence
    makes": avoid the comma splice.
  ⁃ "induction on the tail of the list" weird wording.  "induction on
    the reverse of the list".
  ⁃ link to formalisation: put a link the bitbucket page, not a direct
    link to a git repository I am supposed to clone via "git clone
    <url>".

3 Further Nit-picky comments
═════════════════

p. 1

"Artificial Intelligence (AI)" -> "artificial intelligence (AI)" (suggestion).
Similarly for HOL on p. 2. Otherwise, model-checking researchers would have
reasons to complain about the lowercase spelling for "model checking".

"model-checking" -> "model checking" (except in phrases like "model-checking
algorithm", where the hyphen is possible, albeit chiefly American English).

p. 2

"In many practical cases" -> "Often"?

"that it is not always the case that it is usefully applicable to the given
transition system" ->
"it is not always usefully applicable to the given transition system".
(And what does "usefully applicable" actually mean?)

"We seek //to/ formalise"

"that we published in [4]". Style books such as van Leunen's and respected
researchers such as Lawrence Paulson advocate not treating references as
names. See

https://www.cl.cam.ac.uk/~lp15/Pages/Scream.html

In this case you can easily write "that we developed earlier [4]" or the like.
Another case on the same page is "and used in [1,2,3,5]".

"Ip and Dill [25,26], that is used": Either " that is used" (no comma) or
", which is used" -- in British English, also " which is used" (no comma).
But ", that is used" exists nowhere.

"There are two /important/main OR principal/ outcomes". "Important" sounds,
how shall I say, self-important? Seriously, please leave it to the readers to
determine how important they find your work.

"a bug that we /did not discover/had not discovered/"

"despite the fact that the algorithm was tested" ->
"even though we had tested the algorithm".
See Strunk & White about the vacuous "the fact that".

"especially/,// if those".

p. 3

"described in terms of 'actions'. 1": Spurious space before the footnote call.

> A state/,// x/,//

> {v → b |if (v ∈ D(x1)) then b = x1(v) else b = x2(v)}

> syste/t//m

There's a strange \delta right in front of "We write".

p. 4

The font for "if ... then ... else" is quite different from Definition 1.

p. 5

"are subtle but non-trivial": Drop "but non-trivial"? I'd assume that a subtle
mistake is nontrivial by definition.

"In terms of generalising the algorithm, we did two things." -> "We
generalised the algorithm along two axes."? Then weird tense switch ("we did"
vs. "we prove"). In general, there are many issues throughout with the tenses.
Tenses are hard to get right in formalization paper (the formalization effort is
in the past, but the math is timeless); but clearly there's something suboptimal
with the conclusion, which starts with "In this work we verify ..." and switches
to "During this work, ... we found mistakes". At least the conclusion should use
the past tense throughout.

p. 16

"thus, their existence makes a strong case for the utility of mechanical verification"
Comma splice. See https://en.wikipedia.org/wiki/Comma_splice .


----------------------- REVIEW 2 ---------------------
PAPER: 61
TITLE: A Verified Compositional Reachability Algorithm for Factored Transition Systems
AUTHORS: Mohammad Abdulaziz, Michael Norrish and Charles Gretton

Overall evaluation: 1 (weak accept)

----------- Overall evaluation -----------
The paper presents the formalization of a methodology for decompositional model checking of reachability properties of transition systems.
This formalization shows that a solution for a problem can be composed out of sub-solutions.
This applies to factored transition systems in which the step function monotonically adds information to the state.

The paper is generally well-written.
The fact that the authors exposed a bug in the original proof/algorithm, due to this formalization, is very interesting.
It makes this paper a nice show-case for the use of formalization of algorithms.

After reading, I was left with two questions.
First: the reason to do decomposition is to achieve better scalability.
That issue is not addressed here: how much faster is the decomposed algorithm to a monolithic one?
Specifically, the issue found during this formalization enforces an additional step in the corrected algorithm (rem-condless).
I have no insight in what the impact of that additional step is on the scalability of the corrected algorithm.

Secondly, the notion of "identical" subsystems (also called "descriptive quotients") was unclear to me, after reading the paper.
To the best of my knowledge, a quotient system is based on an equivalence relation.
I could not find that equivalence relation in the paper.
Are "identical" subsystems the same as "orbits" in the terminology of symmetry reduction?
Are "identical" subsystems bisimilar?


Minor issues:
- p2: "We seek formalise an answer" --> "We seek to formalise an answer"
- p2: "applicable more than" --> "applicable to more than"
- Def 3.: "systetm" --> "system". Also, there is an extra \delta here, after the first sentence.
- Sec2.2: "to be Boolean" --> "are Boolean"
- there are some overflows in the right margin
- Def 6. : I don't know the precedence rules of /\and \/ of HOL4, but for sake of readability, one might add parentheses around the third disjunct (i.e., the last line of the definition)
- Sec3: "all action" --> "all actions"
- Sec 4.1: "surprisingly challenge" and "instantiantion"
- Below Def. 21 you refer to Theorem 4, should be Lemma 4, I think


----------------------- REVIEW 3 ---------------------
PAPER: 61
TITLE: A Verified Compositional Reachability Algorithm for Factored Transition Systems
AUTHORS: Mohammad Abdulaziz, Michael Norrish and Charles Gretton

Overall evaluation: 1 (weak accept)

----------- Overall evaluation -----------
The paper presents a HOL4 formalisation of some of the results of [4], including a correction
to one of that paper's results. 

The main problem I see is that the paper is not self-contained.  It assumes the reader 
is very familiar with [4].  That's too bad.  I think with a small summary of the results
and concepts of [4] is in order.  For example, the central concepts --- descriptive quotient ---
is not well described or motivated in the current paper.   And theorems 3 and 4?  Do we
have to consult [4] to see what they are?  A short summary of [4] at the start of the paper 
could solve these problems.  The constant refs to [4] could be replaced with refs back to 
this summary section. (Yes, I understand the space constraints, but I feel this is important
for readability.) 

Other than this main problem, the paper is well written and structured in a nice way. 
Section 5 (the counter example discovered in the process) is interesting.


----------------------- REVIEW 4 ---------------------
PAPER: 61
TITLE: A Verified Compositional Reachability Algorithm for Factored Transition Systems
AUTHORS: Mohammad Abdulaziz, Michael Norrish and Charles Gretton

Overall evaluation: -1 (weak reject)

----------- Overall evaluation -----------
This paper describes interesting and valuable work, but the presentation makes it hard to appreciate it. In part, it is because the definitions and terminology are sloppy, and the informal text is imprecise and confusing. A bigger problem is much of the exposition consists of raw HOL4 statements, which are difficult to read and understand. (The fault is not HOL4; it is hard to read formal text in general.)

I hope the authors will improve the presentation in the future. Definitions should be stated clearly and precisely, and they should properly motivated and explained. Theorems can be stated and their proofs sketched in ordinary mathematical language. Readers can consult the formal proof scripts to see the formal text; the only reason to include formal snippets in the presentation is to point out interesting aspects of the formalization and design decisions, and explain them and the effects they had on the work.

Specific comments:

p 2 l -1: "applicable *to* more

p 3 Definition 1: {v | v -> b in x *for some b*}

p 3 Definition 1: {v -> b | ... } is confusing. It looks like you are writing down a set of mappings, but you are defining a mapping, not a set of mappings. I think you mean {(v, b) | ... }. Or better, just write 

  the mapping v -> if v in D(x1) then x1(v) else v2(v)

Definition 2: when you say "an action pi (= (p, e))" tell us what sorts of things p and e are supposed to be. Don't make us carry out the type inference ourselves.

Definition 3: "systetm", and after the period there is a spurious delta. "the unions of the domains of all the actions": an action is a pair. What is the domain of an action?

Section 2.3, line 2: "the algorithm had" -> "the algorithm has"

Section 3: The discussion of "needed assignments" is confusing. Pi.I is a set of states, and it sounds like you are calling states assignments, but later it seems what you really mean is modifying the states in Pi.I, not Pi.I itself.

Section 4.1: "a surprisingly challenge" -> "a surprising challenge" or "surprisingly challenging"
