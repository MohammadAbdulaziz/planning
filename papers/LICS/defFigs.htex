\begin{figure}[t]
\centering
\begin{subfigure}[b]{0.2\textwidth}
\begin{tikzpicture}
\node (s0) at (4,1) [varnode] {\scriptsize $0$ } ;
\node (s2) at (3,0)   [varnode] {\scriptsize $2$ } ;
\node (s4) at (5,0)   [varnode] {\scriptsize $4$ } ;
\node (s6) at (4,-1)[varnode] {\scriptsize $6$ } ;
\node (s1) at (6,-1)  [varnode] {\scriptsize $1$ } ;
\node (s3) at (6,-.33)  [varnode] {\scriptsize $3$ } ;
\node (s5) at (6,.33)  [varnode] {\scriptsize $5$ } ;
\node (s7) at (6,1) [varnode] {\scriptsize $7$ } ;
\draw [->,ourarrow] (s0) -- (s2) ;
\draw [->,ourarrow] (s2) -- (s0) ;
\draw [->,ourarrow] (s0) -- (s4) ;
\draw [->,ourarrow] (s4) -- (s0) ;
\draw [->,ourarrow] (s0) -- (s6) ;
\draw [->,ourarrow] (s6) -- (s0) ;
\draw [->,ourarrow] (s6) -- (s2) ;
\draw [->,ourarrow] (s2) -- (s6) ;
\draw [->,ourarrow] (s4) -- (s2) ;
\draw [->,ourarrow] (s2) -- (s4) ;
\draw [->,ourarrow] (s4) -- (s6) ;
\draw [->,ourarrow] (s6) -- (s4) ;
\draw [->,ourarrow] (s1) -- (s3) ;
\draw [->,ourarrow] (s3) -- (s5) ;
\draw [->,ourarrow] (s5) -- (s7) ;
\end{tikzpicture}
\caption{ \label{fig:stateDiag}}
\end{subfigure}
\begin{subfigure}[b]{0.1\textwidth}
\begin{tikzpicture}
\node (s0) at (4,1) [varnode] {\scriptsize $0$ } ;
\node (s2) at (3,0)   [varnode] {\scriptsize $1$ } ;
\node (s4) at (5,0)   [varnode] {\scriptsize $2$ } ;
\node (s6) at (4,-1)[varnode] {\scriptsize $3$ } ;
\draw [->,ourarrow] (s0) -- (s2) ;
\draw [->,ourarrow] (s2) -- (s0) ;
\draw [->,ourarrow] (s0) -- (s4) ;
\draw [->,ourarrow] (s4) -- (s0) ;
\draw [->,ourarrow] (s0) -- (s6) ;
\draw [->,ourarrow] (s6) -- (s0) ;
\draw [->,ourarrow] (s6) -- (s2) ;
\draw [->,ourarrow] (s2) -- (s6) ;
\draw [->,ourarrow] (s4) -- (s2) ;
\draw [->,ourarrow] (s2) -- (s4) ;
\draw [->,ourarrow] (s4) -- (s6) ;
\draw [->,ourarrow] (s6) -- (s4) ;
\end{tikzpicture}
\caption{ \label{fig:stateDiagProj}}
\end{subfigure}
\begin{subfigure}[b]{0.075\textwidth}
\begin{tikzpicture}
\node (Z) at (3,1) [varnodenoperi] {\scriptsize$z$ } ;
\node (X) at (2.5,0) [varnodenoperi] {\scriptsize$x$ } ;%
\node (Y) at (3.5,0) [varnodenoperi] {\scriptsize$y$ } ;
\draw [->,ourarrow] (Z) -- (X) ;
\draw [->,ourarrow] (Z) -- (Y) ;
\draw [<->,ourarrow] (X) -- (Y) ;
\end{tikzpicture}
\caption[depGraph]{\label{fig:depGraph} }
\end{subfigure}
\begin{subfigure}[b]{0.075\textwidth}
\begin{tikzpicture}
\node (W) at (3,1) [varnodenoperi] {$\{z\}$ } ;
\node (X) at (3,0) [varnodenoperi] {$\{x, y\}$ } ;
%\draw [->,ourarrow] (W) -- (Z) ;
\draw [->,ourarrow] (W) -- (X) ;
%\draw [<->,ourarrow] (X) -- (Z) ;
\end{tikzpicture}
\caption[proj]{\label{fig:liftedDepGraph} }
\end{subfigure}
\caption{ \label{fig:def} (a) The state space of $\delta$ in Example~\ref{eg:factored}.
(b) The state space of the projection of $\delta$ on $\{x,y\}$, a contraction of (a).
(c) the dependency graph of $\delta$ , and (d) a lifted dependency graph of $\delta$.
Action induce edges in (c): $x$ and $y$ coalesce in action effects, while $z$ only happens to be in the precondition.}
\end{figure}
