#!/bin/bash

if `ps -e | grep -q build.sh`; then
  exit
fi

./build.sh
