\section{Upper-Bounding the Diameter with Decomposition}
\label{sec:theorems}

% 1. observation that d and decomposition do not mesh well

% 2. introduce our l.

% 3. Prove l works with decomposition via "N result"
%  - corollaries are many and lovely

% 4. N generalised with base-case function gives algorithm, correct when base-case algorithm is correct.

% 5. general results about d, l and base-case estimators (invariants, exponential count etc)


We define the diameter of a transition system in terms of its factored representation as
\footnote{With this definition the diameter will be one less than how it was defined in \cite{DBLP:journals/ac/BiereCCSZ03}.}
\input{diamDef}
If the transition system under consideration exhibits a modular or hierarchical structure, upper-bounding its diameter compositionally would be of great utility in terms of computational cost; \ie, if the whole system's diameter is bounded by a function of its components' diameters.
One source of structure in transition systems, especially hierarchical structure, is \emph{dependency} between state variables.
\input{depDef}
%%
One tool used in analysing dependency structures is the \emph{dependency graph}.
The dependency graph of a factored transition system \(\Pi\) is a directed graph, written $G$, describing variable dependencies.
We use it to analyse hierarchical structures in transition systems.
This graph was conceived under different guises in~\cite{williams:nayak:97} and~\cite{bylander:94}, and is also commonly referred to as a {\em causal graph}.
\input{depGraphDef}
The structures we aim to exploit are found via dependency graph analysis, where, for example, the lifted dependency graph exhibits a {\em parent-child} structure, defined as follows:
\input{parentChildDef}
\noindent It is intuitive to seek an expression $f$ that is no worse than multiplication to combine subsystem diameters, or an upper bound for it, into a bound for the entire system's diameter.
%We call upper bounds on the diameter for which there is such an $f$ \emph{decomposable}.
For instance, for systems with the parent-child structure, previous work in the literature suggests that $(b(\proj{\Pi}{\vs}) + 1)(b(\proj{\Pi}{\overline{\vs}}) + 1)$ should be an upper bound on $b(\Pi)$, for some function $b:\Pi \rightarrow \mathbb{N}$ that is an upper bound on the diameter to be considered \emph{decomposable} for parent-child structures.
The following example shows that the diameter is not a decomposable bound for parent-child structures.
\input{diamNotDecompFig}
\input{diamNotDecompEg}

\subsection{Decomposable Diameters}
Baumgartner \emph{et al}~\cite{baumgartner2002property} show that the \emph{recurrence diameter} gives a decomposable upper bound on diameter.
\input{rdDef}
\noindent We provide a tighter and decomposable diameter: the \emph{sublist diameter}, $\ell$.
%Adopting that new definition we describe our mechanised proof of the inequality that featured in Not-A-Theorem~\ref{thm:lem1}.
%We also develop novel bounds and compare them to the inequalities suggested in \RG, showing that in some cases our novel bounds dominate.
%Our definition of $\ell$ appeals to $\Pi^{\subscriptsublist}(s,\as)=\{\as' |\exec{s}{\as} = \exec{s}{\as'} \wedge  \sublist{\as'}{\as}\}$ -- \ie, the set of executions from $s$ that are equivalent to $\as$ and also scattered sublists of $\as$.
\input{ellDef}
\noindent It should be clear that $\ell(\Pi)$ is an upper bound on $d(\Pi)$ and that it is also a lower bound on $\recurrenceDiam(\Pi)$, as demostrated in the following theorem.
\begin{alltt}
\HOLthm{topologicalProps.sublistD_bounds_D_and_RD_bounds_sublistD}
\end{alltt}
The sublist diameter is tighter than the recurrence diameter because it exploits the factored representation of transitions as actions, as shown in the next example.
\input{diamLEellLErdEg}
\noindent The other important property of $\ell$ is that it is decomposable as demonstrated in the following example.
\input{ellDecompEg}
\noindent Now we prove that $\ell$ is decomposable.
%%Using $\ell$ we can prove theorems that are important for understanding and justifying existing bounds.
\input{parentChildellProofFig}

\input{parentChildellThm}
\input{parentChildellProof}
%%
%Here we provide the full details of that construction, which is key to understanding our later analysis.
%%
%% From Definition~\ref{def:Lfinal} we have that if the abstract problem \proj{\Pi}{\vs_2} is solvable, then a plan for that with at most $\ell(\proj{\Pi}{\vs_2})$ actions exists.
%%


\noindent The above construction can be illustrated using the following example.
\input{parentChildellProofEg}

\section{Decomposition for Tighter Bounds}

So far we have seen how to reason about bounds on underlying transition system sublist diameters by treating sublist diameters of subsystems separately in an example structure (\ie the parent child structure).
We now discuss how to exploit a more general dependency structure to compute an upper bound on the sublist diameter of a system, after separately computing subsystems' sublist diameters.
We exploit branching one-way variable dependency structures.
An example of that type of dependency structure is exhibited in Figure~\ref{fig:GS4}, where $S_i$ are sets of variables each of which forms a vertex in the lifted dependency graph.
Recall that an edge in this graph from a vertex $S_i$ to a node $S_j$ means $\dep{\Pi}{S_i}{S_j}$, which means that there is at least one edge from a variable in $S_i$ to one in $S_j$.
Also, an absence of an edge from a vertex $S_i$ to a vertex $S_j$ means that $\negdep{\Pi}{S_i}{S_j}$, and which means that is not a variable in $S_j$ that depends on a variable in $S_i$.

In this section we present a general theorem about the decompositional properties of the sublist diameter to treat the more general structures.
Then we provide a verified upper-bounding algorithm based on it.
Consider the following more general form of the parent child structure:
\input{genParentChild}
\noindent To prove the more general theorem we need the following lemma:
\input{nellLem}
\input{nellProof}

\input{nellSkolemCor}

Branching one-way variable dependencies are captured when $\graph_\vstype$ is a directed acyclic graph (DAG).
\input{depGraphDAGDef}
\noindent We now use Corollary~\ref{cor:nellSkolem} to prove the following theorem:
\input{NalgoValidellThm}
\input{NalgoValidellProof}



\subsection{A Bounding Algorithm}
\label{sec:boundingalgo}
We now discuss an upper-bounding algorithm that we prove is valid.
% is correct and then compare it to the algorithm suggested by \RG.
Consider the function $\Nbrace{\basecasefun}$, defined over the vertex of a lifted dependency DAG as: \[\Nbrace{\basecasefun}(\vs) = \basecasefun(\proj{\Pi}{\vs})(\Sigma_{C \in \children{\vs}{\graph_\vstype}}\Nbrace{\basecasefun}(C) + 1)\]
Note that $\Nbrace{\basecasefun}$ is a general form of the function $\Nname$ defined in Theorem~\ref{thm:NalgoValidell}, parametrised over a \emph{base case} function $\basecasefun:\Pi \rightarrow \mathbb{N}$.
Viewing $\Nalgobrace{\basecasefun} = \Sigma_{\vs \in \graph_\vstype} \Nbrace{\basecasefun}(\vs)$ as an algorithm, the following theorem shows that it calculates a valid upper bound for a factored transition system's sublist diameter if the base case calculation is a valid bound.

\input{NalgoGenValidellThm}
%[def,>>,conj1]{boundingAlgorithms.N_gen_def}
%[def,>>,conj2]{boundingAlgorithms.N_gen_def}

Without any detailed analysis we are able to take $\basecasefun(\Pi)=2^{\cardinality{\dom(\Pi)}}-1$. 
In other words, an admissible base case is one less than the number of states representable by the set of state variables of the system being evaluated. 
That is a valid upper bound for both the recurrence and sublist diameters. 

%% A (worst-case) base-case function that one might use for parameter $b$ above would be one that returned the (exponential) size of its argument's state space ($2^{|\dom(\Pi)|}$), because this is always an upper bound on the sublist diameter.
%We now show some base-case functions that can estimate the state space size, for which $\Nbrace{\basecasefun}$ is a valid upper bound on the sublist diameter.
%Exponential
%\subsection{Invariants}
%% However, one easy to compute base-case function is a valid upper bound on the sublist diameter when the transition system has an \emph{invariant} property, which we define below.
%% \begin{mydef}[Invariance]
%% \label{def:invariant}
%% A state $s$ is invariant w.r.t. $\vs$, written as $\invariant{s}{\vs} $, iff $\forall x \in \vs. s(x) \Rightarrow (\forall x' \in \vs. x \neq x' \Rightarrow \neg s(x'))$.
%% For a $\vs$, we refer to the set $\{s | \invariant{s}{\vs} \wedge \dom(s) = \vs\}$ as $\invstates{s}$.
%% A state space $\cal S$ is invariant w.r.t. to a set of sets of variables $\VS$, written as $\invariant{\cal S}{\VS}$, iff $\forall s \in {\cal S}. \forall vs \in \VS. \invariant{s} {vs}$.
%% A transition system $\Pi$ is invariant w.r.t. to a set of sets of variables $\VS$, written as $\invariant{\Pi}{\VS}$, iff $\invariant{\probss{\Pi}}{\VS}$.
%% \end{mydef}
%% \noindent The following lemma follows from the definition of invariance for state spaces.
%% \begin{mylem}
%% \label{lem:invariantstatespacethm}%invariant_state_space_thm
%% If ${\cal S}$ is a state space over some $\dom \subseteq \bigcup \VS$, where all the members of $\VS$ are disjoint, and $\invariant{\cal S}{\VS}$, then ${\cal S} \subseteq \proj{(\prod{\vs \in \VS} {\invstates{\vs}})}{\dom}$.
%% \end{mylem}
%% \begin{proof}
%% The proof is by induction on ${\cal S}$.
%% \end{proof}
%% \noindent Accordingly the following corollary follows:
%% \begin{mycor}
%% \label{lem:invariantprobreachablestatesthm}%invariant_prob_reachable_states_thm
%% For a transition system $\Pi$, if $\invariant{\Pi}{\VS}$, where all the members of $\VS$ are disjoint, then $\probss{\Pi} \subseteq \prod{\vs \in \VS} {\invstates{\vs}}$.
%% \end{mycor}
%% \noindent One base function, which is an upper bound on the sublist diameter, that was suggested by \RG is to enumerate the states constituting $\prod{\vs \in \VS} {\invstates{\vs}}$.
%% They used 2-SAT model counting, for which no tractable solutions are known.
%% However, we suggest a different approach that depends on the following observation.
%% \begin{mylem}
%% \label{thm:invariantlemma}%invariant_lemma
%% $|\invstates{\vs}| = |\vs| + 1$
%% \end{mylem}
%% \begin{proof}
%% By induction on $\vs$.
%% \end{proof}
%% \noindent Based on Lemma~\ref{lem:invariantstatespacethm}, we derive the following upper bound on the sublist diameter of a transition system.
%% \begin{mythm}
%% \label{thm:invariantprobdiameterboundthm}%invariant_prob_diameter_bound_thm
%% For a transition system $\Pi$, if $\invariant{\Pi}{\VS}$, where all the members of $\VS$ are disjoint, then $\prod{\vs \in \VS} {(|\vs|+1)}$ is a valid upper bound on $\ell(\Pi)$.
%% \end{mythm}
%% \begin{proof}
%% Since $|\prod{\vs \in \VS}{\invstates{\vs}}| \leq \prod{\vs \in \VS}{|\invstates{\vs}|}$ and because of Lemma~\ref{thm:invariantlemma}.
%% \end{proof}
%% \noindent In order to use $\prod{\vs \in \VS} {(|\vs|+1)}$ as a base-case function for $\Nbrace{\basecasefun}$, we need a way to compute, for a $\Pi$, a set of disjoint sets of variables $\VS$ such that $\invariant{\Pi}{\VS}$ holds.
%% If we can compute such a $\VS$ via an oracle $f$, the following theorem will be true.\footnote{We use fast downward to do this computation}
%% \begin{mycor}
%% \label{thm:invariantprobdiameterboundingalgorithmthm}%invariant_prob_diameter_bounding_algorithm_thm
%% For $\Pi$ and an oracle $f$ such that $\invariant{\Pi}{f(\Pi)}$, then $\prod{\vs \in f(\Pi)} {(|\vs|+1)}$ is a valid upper bound on $\ell(\Pi)$.
%% \end{mycor}
