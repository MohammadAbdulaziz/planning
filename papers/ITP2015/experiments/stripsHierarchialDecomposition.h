#include "stripsProblem.h"
#include <stdlib.h>
#include <time.h>

#include <boost/graph/adjacency_list.hpp>
//#include <boost/tuple/tuple.hpp>
//#include <boost/graph/graph_traits.hpp>
#include <boost/graph/strong_components.hpp>


#ifndef STRIPS_HIER_DECOMP
#define STRIPS_HIER_DECOMP

class StripsHierDecomp{
 private:
  StripsProblem problem;
  void constructDependencyGraph();
  set<int> addOnly, delOnly;// variables that are stated as add or delete only
  
 public:
  StripsHierDecomp(const StripsProblem& problemIn):problem (problemIn){
    constructDependencyGraph();
  }
  typedef boost::adjacency_list < boost::listS,
    boost::vecS,
    boost::directedS,
    boost::property< boost::vertex_color_t, unsigned int >,
    boost::no_property > Graph;
  Graph dependencyGraph;
  Graph liftedDependencyGraph; //SCCs
};
#endif
