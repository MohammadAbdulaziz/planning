(define (domain hotelKey)
   (:predicates (key ?k)
   		(guest ?g)
                (room ?r)
                (has-key ?r ?k)
                (current-key ?r ?k)
                (last-key ?r ?k)
                ;; (occupant ?r ?g)
                (guest-has-key ?k ?g)
                (next-key ?k ?k)
                (safe ?r)
                (goal-var)
                )

   (:action enter-next-key-first-time
       :parameters  (?guest ?room ?key ?key2)
       :precondition (and  (guest ?guest) (room ?room) (key ?key) (key ?key2) (guest-has-key ?key2 ?guest) (current-key ?room ?key) (next-key ?key ?key2) (has-key ?room ?key2) (last-key ?room ?key2))
       :effect (and  (current-key ?room ?key2) ( not (current-key ?room ?key)) (safe ?room)))

   (:action check-in
       :parameters  (?guest ?room ?key ?key2)
       :precondition (and  (guest ?guest) (room ?room) (key ?key) (key ?key2) ;; (forall (?guest) (not (occupant ?room ?guest)))
			   (next-key ?key ?key2) (has-key ?room ?key) (has-key ?room ?key2) (last-key ?room ?key))
       :effect (and  (guest-has-key ?key2 ?guest) ;; (occupant ?room ?guest)
		     (last-key ?room ?key2) (not (last-key ?room ?key)) (not (safe ?room))))

   ;; (:action check-out
   ;;     :parameters  (?guest ?room)
   ;;     :precondition (and  (guest ?guest) (room ?room) (occupant ?room ?guest))
   ;;     :effect (and (not (occupant ?room ?guest))) )

   (:action goal-finish
     :parameters (?guest1 ?guest2 ?key ?room)
     :precondition (and (guest ?guest1) (guest ?guest2) (key ?key) (room ?room) (guest-has-key ?key ?guest1)
                        (current-key ?room ?key) (not (last-key ?room ?key)) ;; (not (occupant ?room ?guest1)) (occupant ?room ?guest2)
			(safe ?room))
     :effect (goal-var))
)
