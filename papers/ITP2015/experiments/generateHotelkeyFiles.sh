#/bin/sh
echo "(define (problem hotelKey-r-1-k-1-g-1)"
echo "  (:domain hotelKey)"
echo "  (:objects "
guest_c=1;
while [ "$guest_c" -le "$1" ] ;
do
  echo guest$guest_c
  let guest_c=guest_c+1
done


room_c=1;
while [ "$room_c" -le "$2" ] ;
do
  echo room$room_c
  let room_c=room_c+1
done


key_c=1;
while [ "$key_c" -le "$(($2 * $3))" ] ;
do
  echo key$key_c
  let key_c=key_c+1
done
echo ")"

echo "(:init "
guest_c=1;
while [ "$guest_c" -le "$1" ] ;
do
  echo "(guest guest$guest_c)"
  let guest_c=guest_c+1
done


room_c=1;
while [ "$room_c" -le "$2" ] ;
do
  echo "(room room$room_c)"
  let room_c=room_c+1
done


key_c=1;
while [ "$key_c" -le "$(($2 * $3))" ] ;
do
  echo "(key key$key_c)"
  if [ "$key_c" -lt "$(($2 * $3))" ] ;
    then echo "(next-key key$key_c key$(($key_c + 1)))"
  fi
  let key_c=key_c+1
done


room_c=1;
while [ "$room_c" -le "$2" ] ;
do
  key_c=1;
  while [ "$key_c" -le "$3" ] ;
  do
    echo "(has-key room$room_c key$(( $(($3 * $(($room_c - 1)))) + $key_c)) )"
    let key_c=key_c+1
  done
  echo "(current-key room$room_c key$(( $(($3 * $(($room_c - 1)))) + 1)) )"
  echo "(last-key room$room_c key$(( $(($3 * $(($room_c - 1)))) + 1)) )"
  let room_c=room_c+1
done
echo ")"


echo "(:goal (or"


room_c=1;
while [ "$room_c" -le "$2" ] ;
do
  key_c=1;
  while [ "$key_c" -le "$(( $3 * $2 ))" ] ;
  do
    guest_c=1;
    while [ "$guest_c" -le "$1" ] ;
    do
      guest2_c=1;
      while [ "$guest2_c" -le "$1" ] ;
      do
        echo "(and (guest-has-key key$key_c guest$guest_c) (current-key room$room_c key$key_c) (not (occupant room$room_c guest$guest_c)) (occupant room$room_c guest$guest2_c))"
      
        let guest2_c=guest2_c+1
      done
      let guest_c=guest_c+1
    done
    let key_c=key_c+1
  done
  let room_c=room_c+1
done


echo "))"






echo ")"



