#! /bin/sh
benchmarks_path=/home/mabdula/planningBenchmarks/
#rm exp_results




for problem in `ls ${benchmarks_path}/gripper/strips/ | grep -v domain | grep -v \#`; do
echo ./run.sh  ${benchmarks_path}/gripper/strips/domain.pddl ${benchmarks_path}/gripper/strips/$problem 
done


for domain in `ls ${benchmarks_path}/IPC8/seq-sat/ | grep -iv stacks`; do
  for problem in `ls ${benchmarks_path}/IPC8/seq-sat/${domain}/ | grep -v domain| grep -v \#`; do
  echo ./run.sh  ${benchmarks_path}/IPC8/seq-sat/${domain}/domain.pddl ${benchmarks_path}/IPC8/seq-sat/${domain}/$problem
  done
done

for domain in `ls ${benchmarks_path}/IPC5/ `; do
  for problem in `ls ${benchmarks_path}/IPC5/${domain}/Propositional | grep -v domain| grep -v \#`; do
  echo ./run.sh  ${benchmarks_path}/IPC5/${domain}/Propositional/domain.pddl ${benchmarks_path}/IPC5/${domain}/Propositional/$problem 
  done
done

for domain in `ls ${benchmarks_path}/IPC8/seq-sat/ | grep -i stacks`; do
  for problem in `ls ${benchmarks_path}/IPC8/seq-sat/${domain}/ | grep -v domain| grep -v \#`; do
  echo ./run.sh  ${benchmarks_path}/IPC8/seq-sat/${domain}/domain_${problem} ${benchmarks_path}/IPC8/seq-sat/${domain}/$problem
  done
done


for domain in `ls ${benchmarks_path}/seq-opt/ | grep -iv stack | grep -iv printer`; do
  for problem in `ls ${benchmarks_path}/seq-opt/${domain}/problems`; do
  echo ./run.sh  ${benchmarks_path}/seq-opt/${domain}/domain/domain.pddl ${benchmarks_path}/seq-opt/${domain}/problems/$problem
  done
done


  for problem in `ls ${benchmarks_path}/seq-opt/openstacks/problems`; do
  echo ./run.sh  ${benchmarks_path}/seq-opt/openstacks/domain/ echo `$problem%.pddl`-domain.pddl ${benchmarks_path}/seq-opt/openstacks/problems/$problem
  done

  for problem in `ls ${benchmarks_path}/seq-opt/parcprinter/problems`; do
  echo ./run.sh  ${benchmarks_path}/seq-opt/parcprinter/domain/{ 
  echo {`$problem%.pddl`-domain.pddl ${benchmarks_path}/seq-opt/parcprinter/problems/$problem
  done



