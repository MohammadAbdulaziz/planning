#include "util.h"
#include <src/preprocess/helper_functions.h>
#include "stripsProblem.h"
#include "stripsHierarchialDecomposition.h"

#include <src/preprocess/state.h>
#include <src/preprocess/mutex_group.h>
#include <src/preprocess/axiom.h>
#include <src/preprocess/operator.h>
#include <src/preprocess/variable.h>
#include <src/preprocess/successor_generator.h>
#include <src/preprocess/causal_graph.h>

class State;
class MutexGroup;
class Operator;
class Axiom;



int main()
{
  bool metric;
  vector<Variable> internal_variables;
  vector<Variable *> variables;
  vector<MutexGroup> mutexes;
  State initial_state;
  vector<std::pair<Variable *, int> > goals;
  vector<Operator> operators;
  vector<Axiom> axioms;
  read_preprocessed_problem_description(cin,
                                        metric,
					internal_variables,
					variables,
					mutexes,
					initial_state,
					goals,
					operators,
					axioms);
  char*name = (char*)"Prob";
  StripsProblem prob(variables, initial_state, goals, operators, name);
  prob.writePorblemPDDL();
  prob.searchPlan2();
  prob.validatePlan();
}
