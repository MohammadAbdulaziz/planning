#/bin/sh                                                                                                                                                                                                                                      
rm generatedHotelkeyInstances/*
room_c=1;
while [ "$room_c" -le "$1" ] ;
do
  guest_c=1;
  while [ "$guest_c" -le "$2" ] ;
  do
    key_per_room_c=1;
    while [ "${key_per_room_c}" -le "$3" ] ;
    do
      ./generateHotelkeyFiles.sh ${guest_c} ${room_c} ${key_per_room_c} > generatedHotelkeyInstances/HotelKey_${guest_c}_Guests_${room_c}_Rooms_${key_per_room_c}_Key_per_room.pddl
      let key_per_room_c=key_per_room_c+1
    done
    let guest_c=guest_c+1
  done
  let room_c=room_c+1
done
