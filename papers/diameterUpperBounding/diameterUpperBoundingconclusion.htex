\section{Conclusion and Open Questions}
\label{sec:conclusion}

In this work we provide the first relatively comprehensive study of the compositional approach for upper-bounding topological properties of digraphs which are succinctly represented as factored transition systems.
For such digraphs, the compositional approach is virtually the only practical approach for bounding topological properties.
Although the compositional approach was intensely investigated to solve other graph theoretic questions for succinct representations, most notably reachability, there is not as much work on compositionally upper-bounding topological properties, except for Baumgartner et al.~\cite{baumgartner2002property} and Rintanen~and~Gretton~\cite{Rintanen:Gretton:2013}.
This lack of bounding techniques resulted in a practical incompleteness of SAT-based planning and bounded model checking which was noted as a significant problem by Clarke et al.~\cite{clarke2004completeness}, and which is perceived as a deficiency of SAT methods compared to state based methods.

We considered two compositional approaches.
The first is based on the well studied abstraction known as \emph{projection}, where those projections are computed based on analysing a well-studied structure of transition systems, namely, \emph{state variable dependency}.
The second approach is based on a new abstraction which we introduced, namely, \emph{snapshotting}, and in that method snapshots are computed based on analysing \emph{acyclicity in the state space}.

Our results are both negative and positive.
We showed that the projection/variable dependencies based approach cannot be used to compositionally upper-bound the diameter and the recurrence diameter, which are the two most interesting topological properties.
However, we defined new topological properties, namely, the \emph{sublist diameter} and the \emph{traversal diameter}, which can be upper-bounded compositionally using the projections/variable dependencies approach, and those new properties can be used to upper-bound the diameter and/or the recurrence diameter.

Other results include theorems that, in essence, imply that our new compositional approaches cannot be improved without analysing more system structure.
In particular, the projection (snapshotting) method can only be improved by analysing structures other than the variable dependencies (state space structure).
The fact that there is not a method better than ours for such a well-studied abstraction and a structure like projection and variable dependency indicates the difficulty of compositional bounding and that there is a lot more work to be done.
It would be interesting to see if our methods could actually be improved by analysing more than just the dependency graph.
Another interesting direction would be devising abstractions and system structures that can yield new sound compositional bounding methods.

Those negative results, however, can be circumvented by combining the two compositional bounding procedures into a hybrid procedure.
That hybrid procedure produces very fine grained abstractions and very tight bounds compared to its two constituents, which in turn outperform existing compositional bounding procedures.
Nonetheless, since since we did not show that our hybrid bounding algorithm is optimal it would be interesting to see whether the projection and snapshotting based algorithms can be combined in a better way.

Another important contribution was our defining new topological properties like the sublist diameter and the traversal diameter.
In addition to being compositionally bounded, those new topological properties can be exponentially smaller than topological properties used in previous works to compositionally upper-bound the diameter and the recurrence diameter (see Figure~\ref{fig:topProps}).
Other interesting results concerning those new properties include the fact that the traversal diameter can be computed in linear time in the size of explicitly represented digraph, and thus can be generally used as a completeness threshold instead of the recurrence diameter, which is NP-hard in the explicit digraph.
Another interesting result is that the sublist diameter exploits the factored representation: its value depends on the factored representation rather than the digraph modelling the state space.
This last result begs the following question: does every digraph have a factored representation with a sublist diameter equal to the diameter, and how hard is it obtain this factorisation, if one exists?
Answering this question could lead to the computation of very tight bounds.
Another interesting question concerning new topological properties looks at them from a model-checking perspective.
As we state earlier, the diameter is a minimal completeness threshold for safety temporal formulae and the recurrence diameter is a minimal completeness threshold for liveliness formulae.
The question is: are there interesting classes of formulae for which the sublist diameter and/or the traversal diameter are minimal completeness thresholds?



