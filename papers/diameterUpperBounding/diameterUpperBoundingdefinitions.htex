\section{Basic Concepts and Notations}
\label{sec:defs}
\setcounter{mylem}{0}
\setcounter{mythm}{0}
%\input{../SODA//listDef}
\input{../SODA//digraphDef}
%\input{../SODA//digraphDAGDef}

With the above definitions at hand we can proceed to the topic of factored descriptions of digraphs that model transition systems.
The edges of such a graph are described compactly by a set of {\em actions}, and the vertices correspond to {\em states}, each of which maps to a unique truth assignment over the set of state characterising propositions.
%%These representations are propositionally factored in the sense that ``states'' correspond to vertices that are characterised by  and sets of edges are compactly described in terms of ``actions''.
In this formalism edges and paths between vertices correspond to executing actions and action sequences, respectively,  between states.
%%
We depart slightly from conventional expositions in the related literature, as we do not treat  {\em initial conditions}, i.e. the state a system begins/starts in, nor do we treat {\em goal/safety} criteria, i.e. intuitively the description of the ``system states we are searching for''.
The bounds and the different topological properties that we study here are independent of those features, and so these aspects are peripheral to our work.
But for this small departure, the system representation we give here is equivalent to representations commonly used in the model checking and AI planning communities, such as  STRIPS~\cite{fikes1971strips} and SMV~\cite{mcmillan1993symbolic}.

\input{../SODA//StatesActionsDef}
\input{../SODA//execDef}
We give examples of states and actions using sets of literals.
For example, $\{\vara,\overline{\varb}\}$ is a state where state variables $\vara$ is (maps to) true, and $\varb$ is false and its domain is $\{\vara,\varb\}$. 
$(\{\vara,\overline{\varb}\}, \{\varc\})$ is an action that if executed in a state where $\vara$ and $\overline{\varb}$ hold, it sets $\varc$ to true.
$\dom((\{\vara,\overline{\varb}\}, \{\varc\})) = \{\vara,\varb,\varc\}$.
\input{../SODA//FactoredDigraphDef}
\input{FactoredDigraphFig}
\input{../SODA//FactoredDigraphEg}

%%

Treating system dynamics only, factored transition systems are equivalent to the STRIPS representation~\cite{fikes1971strips} of AI planning problems with the possibility of having negations in the preconditions, and to the SMV formalism~\cite{mcmillan1993symbolic} of model checking problems.
%%Factored transition systems are equivalent to the STRIPS representation~\cite{fikes1971strips} of AI planning problems with the possibility of having negations in the preconditions, and to the SMV formalism~\cite{mcmillan1993symbolic} of model checking problems.
To put them into context w.r.t. other succinct graph representations, factored transition systems effectively represent digraphs as propositional formulae in disjunctive normal form (DNF).
Accordingly, factored systems can be exponentially smaller than equivalent OBDDs~\cite{feigenbaum1998complexity} and exponentially larger than equivalent Boolean circuits~\cite{galperin1983succinct}.
E.g. a factored system comprised of the two actions $(\{\negate{c}\},\{\negate{a}\})$ and $(\{a,\negate{b}\},\{c\})$ is equivalent to the DNF formula $(\negate{c_1}\wedge\negate{a_2})\vee(a_1\wedge\negate{b_1}\wedge c_2)$ that takes as input variables $\{a_1,b_1,c_1\}$ and $\{a_2,b_2,c_2\}$ representing two vertices.
Note: action preconditions and effects are restricted to conjunctions of literals so that representing planning problems as factored systems solves the \emph{frame problem}~\cite{mccarthy1981some,lifschitz1987semantics}.
An additional advantage of factored systems is that they enable computing state-variable dependencies in linear time, which is critical for the efficiency of the majority of compositional algorithms.
