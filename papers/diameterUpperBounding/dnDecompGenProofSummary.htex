%% These paths are constructed such that for two paths $\path_n^i$ and $\path_m^j$, if $i \neq j$ then $\dom(\path_n^i) \cap \dom(\path_m^j) = \emptyset$, 


%% Also, let $\clique_n^i$ denote a factored system whose state space contains a clique of size $n$.
\begin{proof}
Our proof is constructive.
First let $n = f(n_1,n_2,..,n_k) + 1$.
Let $\path^i$ denote the simple path $\path_{n+n_1+1}^1$ for $i=1$ and $\path_{n_i}^i$ otherwise.
Firstly, we construct a system $\delta_1$ whose state space has a simple path with $n_1+1$ states and a clique with $n$ states attached to the second state in the path.
Formally $\delta_1=\{(\state_i^1,\state_{i+1}^1)\mid 1\leq i \leq n_1\} \cup \{(\state_i^1,\state_j^1) \mid n_1\leq i,j \leq n+n_1+1\}$.
Thus, the diameter of $\delta_1$ will be $n_1$.

Conisder a variable $z\not\in\dom(\path^i)$.
Let $\delta_1' = \{(\{z\}\uplus p, e) \mid (p,e) \in \delta_1 \} \cup \{(\{\overline{z}\}\uplus p,e) \mid (p,e) \in \path^1 \}$, i.e. a transition system with two modes of operation determined by the value of $z$.
If $z$ is true then it operates as a path with a clique attached to its end, i.e. exactly like $\delta_1$.
Otherwise, it operates as a path of length $n+n_1+1$, i.e. exactly like $\path^1$.
Thus $d(\delta_1') = n+n_1$.

Let $\delta_2 = \{(\{\overline{z}\}\uplus p, \{\overline{z}\}\uplus e) \mid (p,e) \in \path^2\}$.
This system operates as a path when $z$ is true and it does not change the value of $z$.
Accordingly $d(\delta_2) = n_2$.
We now show that $\delta = \delta_1'\cup\delta_2\cup \bigcup_{3\leq i\leq k} \path^i$ is the required witness.

To show that $\delta$ satisfies the first requirement, consider the partition $\partition = \{\dom(\delta_1),\dom(\delta_2)\}\cup \{\dom(\path^i)\mid 3\leq i \leq k\}$ of $\dom(\delta)$.
$\probproj{\delta}{\delta_1} = \delta_1 \cup \path^1$.
Although this abstraction has the actions of both $\delta_1$ and $\path^1$, the diameter of this abstraction will be $d(\probproj{\delta}{\delta_1}) = n_1$.
This is because $z$ is no longer in the precondition of any action in that abstraction, and accordingly it operates exactly like $\delta_1$, in terms of distances between states.
Also, $\probproj{\delta}{\delta_2} = \delta_2$, so $d(\probproj{\delta}{\delta_2}) = n_2$.
Finally, $\probproj{\delta}{\path^i} = \path^i$, for $3\leq i\leq k$, and accordingly $d(\probproj{\delta}{\path^i}) = n_i$.

To show that $\delta$ satisfies the second requirement, consider the states $\state_1=\{\overline{z}\}\uplus\state_1^1\uplus\state_1^2..\state_1^k$ and $\state_2=\{\overline{z}\}\uplus\state_{n+n_1+1}^1\uplus\state_{n_1}^2..\state_{n_1}^k$.
The action sequence $\as=[(\{\overline{z}\}\uplus\state_1^1,\state_{2}^1); (\{\overline{z}\}\uplus\state_2^1,\state_3^1).., ;(\{\overline{z}\}\uplus\state_{n+n_1}^i,\state_{n+n_1+1}^1)]$ reaches $\state_2$ if executed at $\state_1$, and there is not an action sequence from $\ases{\delta}$ that can reach $\state_2$ from $\state_1$ that is shorter than $\as$.
This means that the diameter of $\delta$ is more than the length of $\as$, which is $n+n_1$.
This finishes our proof.
\end{proof}
