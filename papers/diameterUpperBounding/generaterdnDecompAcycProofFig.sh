#!/bin/bash

#This generates a dot file that can be drawn using dot,
#the command is fdp -Tpng <(./generaterdnDecompAcycProofFig.sh) -o out.png
#Then rotate with convert out.png -rotate 90 rdnDecompAcycProofFig.png

echo "strict digraph gr {"
echo "splines=true;"
echo "sep=\"+25,25\";"
echo "graph [fontsize=9 ];"
echo "node [style=filled, fillcolor=white, shape=rectangle,ranksep=0.2]"

# echo "{rank=same;"

# for X1 in 0 1 2 3 4
# do
#   for X2 in 0 1 2 3 4
#   do
#     if [ "$X1" = "0" -o "$X2" = "0" ] ; then
#       echo -n "X1${X1}X2${X2} "
#    fi
#   done
# done

# echo "}"


#echo "concentrate=true"
echo "X11X21X31 [fillcolor=blue,margin=0,height=0,width=0.03,fontsize=24,label=111]"
echo "X10X21X31 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=011]"
echo "X12X21X31 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=211]"
echo "X12X20X31 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=201]"
echo "X12X22X31 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=221]"
echo "X12X22X30 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=220]"
echo "X12X22X32 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=222]"


echo "X10X22X32 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=022]"
echo "X13X22X32 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=322]"
echo "X13X20X32 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=302]"
echo "X13X23X32 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=332]"
echo "X13X23X30 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=330]"
echo "X13X23X33 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=333]"

echo "X10X23X33 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=033]"
echo "X14X23X33 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=433]"
echo "X14X20X33 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=403]"
echo "X14X24X33 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=443]"
echo "X14X24X30 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=440]"
echo "X14X24X34 [fillcolor=green,margin=0,height=0,width=0.03,fontsize=24,label=444]"

for X1 in 0 1 2 3 4
do
  for X2 in 0 1 2 3 4
  do
    for X3 in 0 1 2 3 4
    do
      echo "X1${X1}X2${X2}X3${X3} [margin=0,height=0,width=0.03,fontsize=24,label=${X1}${X2}${X3}]"
    done
  done
done
for X1 in 0 1 2 3 4
do
  for X2 in 0 1 2 3 4
  do
    for X3 in 0 1 2 3 4
    do
              if [ "$X3" = "0" ] ; then 
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X2${X2}X31 [type=s,weight=10];"
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X2${X2}X32 [type=s,weight=10];"
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X2${X2}X33 [type=s,weight=10];"
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X2${X2}X34 [type=s,weight=10];"
              else
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X2${X2}X30 [type=s,weight=10];"                
              fi
              if [ "$X2" = "0" ] ; then 
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X21X3${X3} [type=s,weight=10];"
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X22X3${X3} [type=s,weight=10];"
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X23X3${X3} [type=s,weight=10];"
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X24X3${X3} [type=s,weight=10];"
              else
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X20X3${X3} [type=s,weight=10];"
              fi
              if [ "$X1" = "0" ] ; then 
                echo "X1${X1}X2${X2}X3${X3} -> X11X2${X2}X3${X3} [type=s,weight=10];"
                echo "X1${X1}X2${X2}X3${X3} -> X12X2${X2}X3${X3} [type=s,weight=10];"
                echo "X1${X1}X2${X2}X3${X3} -> X13X2${X2}X3${X3} [type=s,weight=10];"
                echo "X1${X1}X2${X2}X3${X3} -> X14X2${X2}X3${X3} [type=s,weight=10];"
              else
                echo "X1${X1}X2${X2}X3${X3} -> X10X2${X2}X3${X3} [type=s,weight=10];"
              fi
    done
  done
done

echo "}"
