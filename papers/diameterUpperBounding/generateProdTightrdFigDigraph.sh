#!/bin/bash

#This generates a dot file that can be drawn using dot,
#the command is dot -Tpng <(./generateProdTightrdFigDigraph.sh) -o out.png
#Then rotate with convert out.png -rotate 90 prodTightrdFigDigraph.png

echo "strict digraph gr {"
echo "node [style=filled, fillcolor=white]"
for S0 in 0 1 2
do
  for S1 in 0 1 2 3
  do
    for S2 in 0 1 2
    do
      for S0_ in 0 1 2
      do
        for S1_ in 0 1 2 3
        do
          for S2_ in 0 1 2
          do
            #if [ "$S0" != "$S0_" -o "$S1" != "$S1_" -o "$S2" != "$S2_" ] ; then
              if [ \( "$S0" = 0 \) -a "$S1" = "$S1_" -a "$S2" = "$S2_" ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S01S1${S1_}S2${S2_} [type=s];"
              fi
              if [ \( "$S0" = 1 \) -a "$S1" = "$S1_" -a "$S2" = "$S2_" ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S02S1${S1_}S2${S2_} [type=s];"
              fi

              if [ \( "$S0" = 2 \) -a "$S1" = "0" -a "$S0" = "$S0_" -a "$S2" = "$S2_" ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S0${S0_}S11S2${S2_} [type=s];"
              fi

              if [ "$S0" = "$S0_" -a \( "$S1" = 0 \) -a "$S2" = "$S2_" ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S0${S0_}S11S2${S2_} [type=s];"
              fi
              if [ "$S0" = "$S0_" -a \( "$S1" = 1 \) -a "$S2" = "$S2_" ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S0${S0_}S12S2${S2_} [type=s];"
              fi
              if [ "$S0" = "$S0_" -a \( "$S1" = 2 \) -a "$S2" = "$S2_" ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S0${S0_}S13S2${S2_} [type=s];"
              fi
              if [ "$S0" = "$S0_" -a \( "$S1" = 3 \) -a "$S2" = "$S2_" ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S0${S0_}S10S2${S2_} [type=s];"
              fi

              if [ "$S0" = "$S0_" -a "$S1" = "$S1_" -a \( "$S2" = 0 \) ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S0${S0_}S1${S1_}S21 [type=s];"
              fi
              if [ "$S0" = "$S0_" -a "$S1" = "$S1_" -a \( "$S2" = 1 \) ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S0${S0_}S1${S1_}S22 [type=s];"
              fi
              if [ "$S0" = "$S0_" -a "$S1" = "$S1_" -a \( "$S2" = 2 \) ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S0${S0_}S1${S1_}S20 [type=s];"
              fi
            #fi
          done
        done
      done
    done
  done
done

echo "}"
