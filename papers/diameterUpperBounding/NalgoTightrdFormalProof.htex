We prove Theorem~\ref{thm:NalgoTightd} only for $\ell$.
The proof for $rd$ is essentially the same as outlined in the proof sketch above.
To prove Theorem~\ref{thm:NalgoTightd}, firstly, we define the following function that does the action interleaving for the constructions showed in Example~\ref{eg:prodTighttdAcycProof}.
\input{../SODA//stuffDef}
\input{../SODA//stuffLenProp}
We now define the function that given a $\mathbb{N}$-DAG, constructs an action sequence as described above in Example~\ref{eg:prodTighttdAcycProof}.
\input{../SODA//nDAGtoasDef}
We now define a version of the polynomial generating function $\NalgoName$, that takes a DAG labelled with natural numbers.
\input{../SODA//NalgoNDef}
\input{../SODA//NalgoNEqNalgoComposeProp}
\input{../SODA//nDAGtoasLenEqNalgoNLemma}
\noindent To prove this lemma, we state the following propositions on DAG's.
\input{../SODA//dagPreIsDAGProp}
\input{../SODA//nDAGtoasHasDepGraphProp}
\input{../SODA//NalgoNRecursiveProp}
\input{../SODA//nDAGtoasLenEqNalgoNProof}

We now define a function that given a graph of natural numbers returns a state.
This state is constructed such that dropping any action from the outcome of $\ngrtoas$, will always result in a different execution outcome from that state.
\input{../SODA//initStateConsDef}
\input{../SODA//satprecondnDAGtoasProp}

\input{../SODA//nDAGtoasNotEqRemActionLemma}

\noindent To prove this lemma we use the following propositions that
follow from Definition~\ref{def:exec} and Definition~\ref{def:ell}.
\input{../SODA//execCatProp}
\input{../SODA//sublistSubsetProp}
\input{../SODA//projSublistProp}
\input{../SODA//projLTProp}
\input{../SODA//ellLBoundProp}
\input{../SODA//nDAGtoasNotEqRemActionProof}

\begin{proof}[Proof of Theorem~\ref{thm:NalgoTightd}]

This lemma follows from Corollary~\ref{cor:NalgoValidd}, Proposition~\ref{prop:NalgoNEqNalgoCompose}, Proposition~\ref{prop:nDAGtoasHasDepGraph}, Proposition~\ref{prop:ellLBound},  Lemma~\ref{lem:nDAGtoasNotEqRemAction}, and Lemma~\ref{lem:nDAGtoasLenEqNalgoN}.
\end{proof}
