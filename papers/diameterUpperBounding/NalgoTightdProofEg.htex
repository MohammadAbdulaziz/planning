\input{NalgoTightdProofEgFig}
{
\input{mathlbreak}
\begin{myeg}
\label{eg:NalgoTightdProof}
For the $\mathbb{N}$-DAG shown in Figure~\ref{fig:graphN}, a reverse topological ordering of the vertices in this graph is the list $[\vertexb;\vertexc;\vertexa]$, where leaves $\vertexb$ and $\vertexc$ come first.
We first build an inverted flower system to label each of the vertices (Figures~\ref{fig:invertedflower1}-\ref{fig:invertedflower2}).
As $\vertexb$ is a leaf, $p_\vertexb = 0$, so we construct an inverted flower $\invlotus_{2,3}^\vertexb$ with two petals and a path with two states. 
%Since $\DAG_\mathbb{N}(\vertexb)$ is odd, the first petal will be of size $3$, while the second will be of size $2$.
Concretely, $\invlotus_{2,3}^\vertexb=\{(\state_1^\vertexb, \state_2^\vertexb), (\state_2^\vertexb, \state_3^\vertexb), (\state_2^\vertexb, \state_4^\vertexb), (\state_3^\vertexb, \state_1^\vertexb), (\state_4^\vertexb, \state_1^\vertexb)\}$.
The states are $\state_1^\vertexb = \{\overline{\vard},\overline{\vare}\}$, $\state_2^\vertexb = \{\overline{\vard},\vare\}$, $\state_3^\vertexb = \{\vard,\overline{\vare}\}$ (the first petal), $\state_4^\vertexb = \{\vard,\vare\}$ (the second petal).
For $\vertexc$ we construct $\invlotus_{2,2}^\vertexc = \{(\state_1^\vertexc, \state_3^\vertexc), (\state_1^\vertexc, \state_2^\vertexc), (\state_3^\vertexc, \state_1^\vertexc), (\state_2^\vertexc, \state_1^\vertexc)\}$.
The states are $\state_1^\vertexc = \{\overline{\varf},\overline{\varg}\}$, $\state_2^\vertexc = \{\overline{\varf},\varg\}$ (the first petal), and $\state_3^\vertexc = \{\varf,\overline{\varg}\}$ (the second petal).
For the root vertex $\vertexa$, $p_\vertexa = 7$, as inverted flowers labelling its children have 5 actions, so $\invlotus_{7,2}^\vertexa$ will have 7 petals.
Concretely, $\invlotus_{7,2}^\vertexa = \bigcup \{\{(\state_1^\vertexa, \state_j^\vertexa), (\state_j^\vertexa, \state_1^\vertexa)\} \mid 2 \leq j \leq 8\}$.
The states are $\state_1^\vertexa = \{\overline{\vara}, \overline{\varb}, \overline{\varc}\}$, $\state_2^\vertexa = \{\overline{\vara}, \overline{\varb}, \varc\}$, $\state_3^\vertexa = \{\overline{\vara}, \varb, \overline{\varc}\}$, $\state_4^\vertexa = \{\overline{\vara}, \varb, \varc\}$, $\state_5^\vertexa = \{\vara, \overline{\varb}, \overline{\varc}\}$, $\state_6^\vertexa = \{\vara, \overline{\varb}, \varc\}$, $\state_7^\vertexa = \{\vara, \varb, \overline{\varc}\}$, and $\state_8^\vertexa = \{\vara, \varb, \varc\}$.

After constructing the inverted flowers we construct the system $\delta$ and the action sequence $\as$.
Based on Algorithm~\ref{alg:asconstructdHalfNalgo}, we start from the leaves, and initially $\as=[]$ and $\delta =\invlotus_{2,3}^\vertexb \cup \invlotus_{2,2}^\vertexc \cup \invlotus_{7,2}^\vertexa$.
$\vertexb$ has no children, i.e. $\childrensymbol=\emptyset$, so we skip the inner for-loop and concatenate the shortest action sequence that reaches the second petal from the first one in the inverted flower associated with $\vertexb$, so $\as = \as_{3\mapsto 4}^\vertexb = [(\state_3^\vertexb,\state_1^\vertexb); (\state_1^\vertexb,\state_2^\vertexb); (\state_2^\vertexb,\state_4^\vertexb)]$.
Similarly, since $\vertexc$ has no children, we concatenate $\as_{1\mapsto 2}^\vertexc$ to $\as$, so 
$\as = \as_{3\mapsto 4}^\vertexb \cat \as_{2\mapsto 3}^\vertexc = [(\state_3^\vertexb,\state_1^\vertexb); (\state_1^\vertexb,\state_2^\vertexb); (\state_2^\vertexb,\state_4^\vertexb); (\state_2^\vertexc,\state_1^\vertexc); (\state_1^\vertexc,\state_3^\vertexc)]$.

For $\vertexa$, $\childrensymbol=\invlotus_{2,3}^\vertexb\cup\invlotus_{2,2}^\vertexc$, which means that the inner for-loop is executed, and it iterates over each action in $\as$.
The first action in $\as$, $(\state_3^\vertexb,\state_1^\vertexb)$ comes from the inverted flower labelling $\vertexb$, which is a child of $\vertexa$.
Accordingly, we remove $(\state_3^\vertexb,\state_1^\vertexb)$ from $\delta$, add to its precondition the second petal ($\state_3^\vertexa$) in the inverted flower labelling $\vertexa$, which makes the action $(\state_3^\vertexb\uplus\state_3^\vertexa,\state_1^\vertexb)$ and add the augmented action to $\delta$.
Then we add before that action, the shortest action sequence joining the first and the second petals in the inverted flower of $\vertexa$, so
$\as = \as_{2\mapsto 3}^\vertexa\cat[(\state_3^\vertexa\uplus\state_3^\vertexb,\state_1^\vertexb); (\state_1^\vertexb,\state_2^\vertexb); (\state_2^\vertexb,\state_4^\vertexb); (\state_2^\vertexc,\state_1^\vertexc); (\state_1^\vertexc,\state_3^\vertexc)]$.
This is repeated for the remaining actions, so $\as = \as_{2\mapsto 3}^\vertexa\cat [(\state_3^\vertexa\uplus\state_3^\vertexb,\state_1^\vertexb)]\cat\as_{3\mapsto 4}^\vertexa\cat [(\state_4^\vertexa\uplus\state_1^\vertexb,\state_2^\vertexb)]\cat \as_{4\mapsto 5}^\vertexa\cat[(\state_5^\vertexa\uplus\state_2^\vertexb,\state_4^\vertexb)]\cat \as_{5\mapsto 6}^\vertexa\cat
[(\state_6^\vertexa\uplus\state_2^\vertexc,\state_1^\vertexc)]\cat \as_{6\mapsto 7}^\vertexa\cat
[(\state_7^\vertexa\uplus\state_1^\vertexc,\state_3^\vertexc)]$.
All the modified actions were replaced in $\delta$ with themselves, but with augmented preconditions.
After the inner for loop ends, $\as_{7\mapsto 8}^\vertexa$ is concatenated, so $\as = \as_{2\mapsto 3}^\vertexa\cat [(\state_3^\vertexa\uplus\state_3^\vertexb,\state_1^\vertexb)]\cat$ $\as_{3\mapsto 4}^\vertexa\cat [(\state_4^\vertexa\uplus\state_1^\vertexb,\state_2^\vertexb)]\cat \as_{4\mapsto 5}^\vertexa\cat[(\state_5^\vertexa\uplus\state_2^\vertexb,\state_4^\vertexb)]\cat \as_{5\mapsto 6}^\vertexa\cat
[(\state_6^\vertexa\uplus\state_2^\vertexc,\state_1^\vertexc)]\cat \as_{6\mapsto 7}^\vertexa\cat
[(\state_7^\vertexa\uplus\state_1^\vertexc,\state_3^\vertexc)]\cat\as_{7\mapsto 8}^\vertexa$.

Consider the states $\state_1 = \state_2^\vertexa\uplus\state_3^\vertexb\uplus\state_2^\vertexc = \{ \overline{\vara}, \overline{\varb}, \varc, \overline{\vard}, \vare , \overline{\varf},\varg\}$ and $\state_2 = \state_8^\vertexa\uplus\state_4^\vertexb\uplus\state_3^\vertexc = \{\vara, \varb, \overline{\varc}, \overline{\vard}, \vare , \overline{\varf},\varg\}$.
$\exec{\state_1}{\as} = \state_2$, and the path traversed by $\as$ in $\graph(\delta)$ is shown in Figure~\ref{fig:NalgoTightdWitness},
There is not an action sequence in $\ases{\delta}$ shorter than $\as$, whose length is 17, that can reach $\state_2$ from $\state_1$.
Accordingly the diameter of $\delta$ is at least 17.
Letting $\Nname(\vs) = \Nbrace{\recurrenceDiam}(\vs,\delta,\DAG_\vstype)$, we have $\Nname(\{\vard, \vare\}) = \recurrenceDiam(\proj{\delta}{\{\vard, \vare\}}) = \recurrenceDiam(\invlotus_{2,3}^\vertexb) = 3$, $\Nname(\{\vard, \vare\}) = \recurrenceDiam(\proj{\delta}{\{\varf, \varg\}}) = \recurrenceDiam(\invlotus_{2,2}^\vertexc) = 2$, 
$\Nname(\{\vara,\varb,\varc\}) = \recurrenceDiam(\proj{\delta}{\{\vara, \varb, \varc\}}) (1 + \Nname(\{\vard, \vare\}) + \Nname(\{\varf, \varg\})) = 2 (1 + 3 + 2) = 12$, and $\Nalgobrace{\recurrenceDiam}(\delta, \DAG_\vstype) = \Nname(\{\vara,\varb,\varc\}) + \Nname(\{\vard, \vare\}) + \Nname(\{\varf, \varg\}) = 12 + 3 + 2 = 17$,
so $\Nalgobrace{\recurrenceDiam}(\delta, \DAG_\vstype) \leq d(\delta)$.


The domain of $\delta$ is $\{\vara,\varb,\varc,\vard,\vare,\varf,\varg\}$ and a partition of it is $\{\dom(\invlotus_{7,2}^\vertexa),\dom(\invlotus_{2,3}^\vertexb),\dom(\invlotus_{2,2}^\vertexc)\} = \{\{\vara,\varb,\varc\}, \{\vard,\vare\}, \{\varf,\varg\}\}$.
A lifted dependency graph of $\delta$, $\DAG_\vstype$, has the following labels: $\DAG_\vstype(\vertexa)=\{\vara,\varb,\varc\}$, $\DAG_\vstype(\vertexb)=\{\vard,\vare\}$, and $\DAG_\vstype(\vertexc)=\{\varf,\varg\}$ (shown in Figure~\ref{fig:DAGvstype}).
It should be clear that $\DAG_\vstype = \Img{\dom}{\DAG_\Delta}$.


\end{myeg}
}
