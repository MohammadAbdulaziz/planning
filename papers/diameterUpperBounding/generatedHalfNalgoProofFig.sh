#!/bin/bash

#This generates a dot file that can be drawn using dot,
#the command is dot -Tpng <(./generatedHalfNalgoProofFig.sh) -o dHalfNalgoProofFig.png

echo "strict digraph gr {"
echo "graph [fontsize=9 ];"
echo "node [style=filled, fillcolor=white, shape=rectangle,ranksep=0.2]"

# echo "{rank=same;"

# for X1 in 0 1 2 3 4
# do
#   for X2 in 0 1 2 3 4
#   do
#     if [ "$X1" = "0" -o "$X2" = "0" ] ; then
#       echo -n "X1${X1}X2${X2} "
#    fi
#   done
# done

# echo "}"


echo "concentrate=true"
echo "X11X21X31 [fillcolor=blue,margin=0,height=0,width=0.03,fontsize=24,label=111]"
echo "X11X21X30 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=110]"
echo "X11X21X32 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=112]"
echo "X10X21X32 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=012]"
echo "X10X21X30 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=010]"
echo "X10X21X33 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=013]"
echo "X12X21X33 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=213]"
echo "X12X21X30 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=210]"
echo "X12X21X34 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=214]"
echo "X12X20X34 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=204]"
echo "X12X20X30 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=200]"
echo "X12X20X35 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=205]"
echo "X12X22X35 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=225]"
echo "X12X22X35 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=225]"
echo "X12X22X30 [fillcolor=red,margin=0,height=0,width=0.03,fontsize=24,label=220]"
echo "X12X22X36 [fillcolor=green,margin=0,height=0,width=0.03,fontsize=24,label=226]"

for X1 in 0 1 2 
do
  for X2 in 0 1 2
  do
    for X3 in 0 1 2 3 4 5 6 7
    do
      echo "X1${X1}X2${X2}X3${X3} [margin=0,height=0,width=0.03,fontsize=24,label=${X1}${X2}${X3}]"
      if [ "$X1" = "1" -a "$X3" = "2" ] ; then
        echo "X1${X1}X2${X2}X3${X3} -> X10X2${X2}X3${X3} [type=s,weight=2];"
      fi        
      if [ "$X1" = "0" -a "$X3" = "3" ] ; then
        echo "X1${X1}X2${X2}X3${X3} -> X12X2${X2}X3${X3} [type=s,weight=2];"
      fi        
      if [ "$X2" = "1" -a "$X3" = "4" ] ; then
        echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X20X3${X3} [type=s,weight=2];"
      fi        
      if [ "$X2" = "0" -a "$X3" = "5" ] ; then
        echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X22X3${X3} [type=s,weight=2];"
      fi        


      if [ "$X3" = "0" ] ; then         
        echo "X1${X1}X2${X2}X30 -> X1${X1}X2${X2}X31 [type=s,weight=2];"
        echo "X1${X1}X2${X2}X30 -> X1${X1}X2${X2}X32 [type=s,weight=2];"
        echo "X1${X1}X2${X2}X30 -> X1${X1}X2${X2}X33 [type=s,weight=2];"
        echo "X1${X1}X2${X2}X30 -> X1${X1}X2${X2}X34 [type=s,weight=2];"
        echo "X1${X1}X2${X2}X30 -> X1${X1}X2${X2}X35 [type=s,weight=2];"
        echo "X1${X1}X2${X2}X30 -> X1${X1}X2${X2}X36 [type=s,weight=2];"
        echo "X1${X1}X2${X2}X30 -> X1${X1}X2${X2}X37 [type=s,weight=2];"
      else
        echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X2${X2}X30 [type=s,weight=2];"
      fi
    
    
    done
  done
done

echo "}"
