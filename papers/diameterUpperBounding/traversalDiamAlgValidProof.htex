%% \begin{mylem}
%% For any factored system $\delta$, a partition $\partition$ of $\uniStates(\delta)$, if $\graph(\delta)/\partition$ is acyclic, then $\Salgo{\mathbb{C}}(\graph(\delta)/\partition)$
%% \end{mylem}

\begin{proof}
For notational brevity, let $\graph = \graph(\delta)/\sccset$, and for a strongly connected component $\scc$, $\Sname(\scc) = \Sbrace{\mathbb{C}}(\scc,\graph)$ and $\childrensymbol(\scc) = \children{\scc}{\graph}$.
Since $\graph$ is a DAG, its vertices can be topologically ordered in a list $l_\sccset$.

Firstly, we prove $\traversalDiamAlgo(\delta) \leq \traversalDiam(\delta)$.
We show that for any strongly connected component $\scc \in \graph$ there is an action sequence $\as_\scc\in\ases{\delta}$ and a state $\state_\scc\in \scc$, such that $\Sname(\scc)\leq \cardinality{\sset(\state_\scc,\as_\scc)} - 1$, which from Definitions~\ref{def:td} and \ref{def:salgoDigraph}, implies $\traversalDiamAlgo(\delta) \leq\traversalDiam(\delta)$.
We prove this by induction on $l_\sccset$.
The base case, $l_\sccset=[]$, is straightforward.
For the step case $l_\sccset = \scc::l_\sccset'$,\footnote{For a list $l$, $h::l$ is $l$ with the element $h$ appended to its front.} and for any $\scc'\in l_\sccset'$, there is a state $\state'\in \scc'$ and an action sequence $\as'\in\ases{\delta}$ where $\Sname(\scc')\leq \cardinality{\sset(\state',\as')} - 1$.
Since $\scc$ is a strongly connected component of states in $\graph(\delta)$, then there is $\as_\scc'\in\ases{\delta}$ and a state $\state_\scc\in \scc$, where $\as_\scc$ traverses exactly all the states in $\scc$, if executed at $\state_\scc\in \scc$, i.e. $\sset(\state_\scc,\as_\scc')=\scc$.
We have two cases:
\begin{mycase}[$\childrensymbol(\scc)=\emptyset$]
From Definition~\ref{def:salgoDigraph}, $\Sname(\scc) = \cardinality{\scc} - 1 = \cardinality{\sset(\state_\scc,\as_\scc')} - 1$ holds for this case.
Accordingly the required witness $\as_\scc$ is the same as $\as_\scc'$.
\end{mycase}
\begin{mycase}[$\childrensymbol(\scc)\neq\emptyset$]
Let $\scc_{\max}$ be a strongly connected component where $\forall\;\scc'\in\childrensymbol(\scc).\;\Sname(\scc') \leq \Sname(\scc_{\max})$. %${\argmax}_{\scc'\in\childrensymbol(\scc)} \Sname(\scc')$.
Because $\scc_\max\in \childrensymbol(\scc)$ we have $\scc_\max\in l_\sccset'$, and accordingly from the inductive hypothesis there are $\state_\max\in\scc_\max$ and $\as_\max\in\ases{\delta}$ such that $\Sname(\scc_\max)\leq \cardinality{\sset(\state_\max,\as_\max)} - 1$.($\dagger$)
Also, because $\scc_\max\in \childrensymbol(\scc)$ and since both $\scc$ and $\scc_\max$ are strongly connected components, there must be $\as'\in\ases{\delta}$, where $\exec{\state_\scc}{\as_\scc'\cat\as'} = \state_{\max}$.%
\footnote{For two lists $l_1$ and $l_2$, $l_1\cat l_2$ denotes their concatenation.}
We now show that $\as_\scc = \as_\scc'\cat\as'\cat\as_\max$ is the required witness.
First, it is easy to see that $\sset(\state_\scc,\as_\scc')\cup \sset(\state_\max,\as_\max) \subseteq \sset(\state_\scc,\as_\scc)$.
Since $\scc_\max\in \childrensymbol(\scc)$, then $\sset(\state_\max,\as_\max)$ is disjoint with $\scc$.
Accordingly $\cardinality{\sset(\state_\scc,\as_\scc')} + \cardinality{\sset(\state_\max,\as_\max)} \leq \cardinality{\sset(\state_\scc,\as_\scc)}$.
From this, ($\dagger$), and Definition~\ref{def:salgoDigraph} we have $\Sname(\scc) \leq \cardinality{\sset(\state_\scc,\as_\scc)} - 1$.
\end{mycase}
\setcounter{mycase}{0}

Secondly, we prove $\traversalDiam(\delta)\leq\traversalDiamAlgo(\delta)$ by showing that for any $\scc \in \graph$, $\state_\scc\in \scc$, and $\as_\scc\in \ases{\delta}$, we have $\cardinality{\sset(\state_\scc,\as_\scc)} - 1\leq\Sname(\scc)$.
Our proof is again by induction on the list $l_\sccset$.
The base case, $l_\sccset=[]$, is straightforward.
The step case $l_\sccset = \scc::l_\sccset'$, and we have that for any $\scc'\in l_\sccset'$, $\state'\in \scc'$, and $\as'\in\ases{\delta}$, $\cardinality{\sset(\state',\as')} - 1\leq\Sname(\scc')$ holds.
We have two cases:
\begin{mycase}[$\sset(\state_\scc,\as_\scc)\subseteq \scc$]
Since $\sset(\state_\scc,\as_\scc)\subseteq \scc$ then $\cardinality{\sset(\state_\scc,\as)}\leq \cardinality{\scc}$.
From Definition~\ref{def:salgoDigraph}, we know that $\cardinality{\scc} - 1\leq\Sname(\scc)$, and accordingly $\cardinality{\sset(\state_\scc,\as_\scc)} - 1 \leq \Sname(\scc)$.
\end{mycase}

\begin{mycase}[ $\sset(\state_\scc,\as_\scc)\not\subseteq \scc$]
Since $\sset(\state_\scc,\as_\scc)\not\subseteq \scc$, then there are $\as_\scc$, $\act$, and $\as_\childrensymbol$ such that:
\begin{enumerate*}
  \item $\as_\scc=\as_\scc'\cat\act::\as_\childrensymbol$,
  \item $\sset(\state_\scc,\as_\scc') \subseteq \scc$, and
  \item letting $\state_\childrensymbol=\exec{\exec{\state_\scc}{\as_\scc'}}{\act}$, $\state_\childrensymbol \in \scc_\childrensymbol$ holds, for some $\scc_\childrensymbol \in \childrensymbol(\scc)$.
\end{enumerate*}
Using the same argument as the last case, we have $\cardinality{\sset(\state_\scc,\as_\scc')}\leq\cardinality{\scc}$.(*)
Since $\state_\childrensymbol\in \scc_\childrensymbol$, and from the inductive hypothesis, we have that $\cardinality{\sset(\state_\childrensymbol,\as_\childrensymbol)} - 1\leq \Sname(\scc_\childrensymbol)$.
Then using (*) and since $\sset(\state_\childrensymbol,\as_\childrensymbol)$ and $\scc$ are disjoint, we have $\cardinality{\sset(\state_\scc,\as_\scc)} - 1\leq \cardinality{\scc}+\Sname(\scc_\childrensymbol)$.
From Definition~\ref{def:salgoDigraph}, we have $\cardinality{\scc}+\Sname(\scc_\childrensymbol)\leq\Sname(\scc)$ and accordingly $\cardinality{\sset(\state_\scc,\as_\scc)} - 1\leq\Sname(\scc)$.
\end{mycase}
\setcounter{mycase}{0}
\end{proof}
