#!/bin/bash

#This generates a dot file that can be drawn using dot,
#the command is fdp -Tpng <(./generaterdnDecompProofFig.sh) -o rdnDecompProofFig.png


echo "strict digraph gr {"
echo "splines=true;"
echo "sep=\"+25,25\";"
echo "graph [fontsize=9 ];"
echo "node [style=filled, fillcolor=white, shape=rectangle,ranksep=0.2]"

# echo "{rank=same;"

# for X1 in 0 1 2 3 4
# do
#   for X2 in 0 1 2 3 4
#   do
#     if [ "$X1" = "0" -o "$X2" = "0" ] ; then
#       echo -n "X1${X1}X2${X2} "
#    fi
#   done
# done

# echo "}"


#echo "concentrate=true"
echo "X11X21 [fillcolor=blue,margin=0.03,height=0.03]"
echo "X10X21 [fillcolor=red,margin=0.03,height=0.03]"
echo "X12X21 [fillcolor=red,margin=0.03,height=0.03]"
echo "X12X20 [fillcolor=red,margin=0.03,height=0.03]"
echo "X12X22 [fillcolor=red,margin=0.03,height=0.03]"
echo "X10X22 [fillcolor=red,margin=0.03,height=0.03]"
echo "X13X22 [fillcolor=red,margin=0.03,height=0.03]"
echo "X13X20 [fillcolor=red,margin=0.03,height=0.03]"
echo "X13X23 [fillcolor=red,margin=0.03,height=0.03]"
echo "X10X23 [fillcolor=red,margin=0.03,height=0.03]"
echo "X14X23 [fillcolor=red,margin=0.03,height=0.03]"
echo "X14X20 [fillcolor=red,margin=0.03,height=0.03]"
echo "X14X24 [fillcolor=green,margin=0.03,height=0.03]"

for X1 in 0 1 2 3 4
do
  for X2 in 0 1 2 3 4
  do
      echo "X1${X1}X2${X2} [margin=0.03,height=0.03]"
  done
done
for X1 in 0 1 2 3 4
do
  for X2 in 0 1 2 3 4
  do
              if [ "$X2" = "0" ] ; then 
                echo "X1${X1}X2${X2} -> X1${X1}X21 [type=s,weight=10];"
                echo "X1${X1}X2${X2} -> X1${X1}X22 [type=s,weight=10];"
                echo "X1${X1}X2${X2} -> X1${X1}X23 [type=s,weight=10];"
                echo "X1${X1}X2${X2} -> X1${X1}X24 [type=s,weight=10];"
              else
                echo "X1${X1}X2${X2} -> X1${X1}X20 [type=s,weight=10];"                
              fi
              if [ "$X1" = "0" ] ; then 
                echo "X1${X1}X2${X2} -> X11X2${X2} [type=s,weight=10];"
                echo "X1${X1}X2${X2} -> X12X2${X2} [type=s,weight=10];"
                echo "X1${X1}X2${X2} -> X13X2${X2} [type=s,weight=10];"
                echo "X1${X1}X2${X2} -> X14X2${X2} [type=s,weight=10];"
              else
                echo "X1${X1}X2${X2} -> X10X2${X2} [type=s,weight=10];"                
              fi
  done
done

echo "}"
