#!/bin/bash

#This generates a dot file that can be drawn using dot,
#the command is fdp -Tpng <(./generateNalgoTightdProofFig.sh) -o NalgoTightdProofFig.png

echo "strict digraph gr {"
echo "splines=true;"
echo "sep=\"+2,2\";"
echo "graph [fontsize=10 ];"
echo "node [style=filled, fillcolor=white, shape=rectangle,ranksep=1]"

# echo "{rank=same;"

# for X1 in 0 1 2 3 4
# do
#   for X2 in 0 1 2 3 4
#   do
#     if [ "$X1" = "0" -o "$X2" = "0" ] ; then
#       echo -n "X1${X1}X2${X2} "
#    fi
#   done
# done

# echo "}"


echo "concentrate=true"
echo "X13X22X32 [fillcolor=blue,margin=0,height=0,width=0.03,label=322]"
echo "X13X22X31 [fillcolor=red,margin=0,height=0,width=0.03,label=321]"
echo "X13X22X33 [fillcolor=red,margin=0,height=0,width=0.03,label=323]"
echo "X11X22X33 [fillcolor=red,margin=0,height=0,width=0.03,label=123]"
echo "X11X22X31 [fillcolor=red,margin=0,height=0,width=0.03,label=121]"
echo "X11X22X34 [fillcolor=red,margin=0,height=0,width=0.03,label=124]"
echo "X12X22X34 [fillcolor=red,margin=0,height=0,width=0.03,label=224]"
echo "X12X22X31 [fillcolor=red,margin=0,height=0,width=0.03,label=221]"
echo "X12X22X35 [fillcolor=red,margin=0,height=0,width=0.03,label=225]"
echo "X14X22X35 [fillcolor=red,margin=0,height=0,width=0.03,label=425]"
echo "X14X22X31 [fillcolor=red,margin=0,height=0,width=0.03,label=421]"
echo "X14X22X36 [fillcolor=red,margin=0,height=0,width=0.03,label=426]"
echo "X14X21X36 [fillcolor=red,margin=0,height=0,width=0.03,label=416]"
echo "X14X21X31 [fillcolor=red,margin=0,height=0,width=0.03,label=411]"
echo "X14X21X37 [fillcolor=red,margin=0,height=0,width=0.03,label=417]"
echo "X14X23X37 [fillcolor=red,margin=0,height=0,width=0.03,label=437]"
echo "X14X23X31 [fillcolor=red,margin=0,height=0,width=0.03,label=431]"
echo "X14X23X38 [fillcolor=green,margin=0,height=0,width=0.03,label=438]"

for X1 in 1 2 3 4
do
  for X2 in 1 2 3
  do
    for X3 in 1 2 3 4 5 6 7 8
    do
      echo "X1${X1}X2${X2}X3${X3} [margin=0,height=0,width=0.03,label=${X1}${X2}${X3}]"
      if [ "$X1" = "3" -a "$X3" = "3" ] ; then
        echo "X1${X1}X2${X2}X3${X3} -> X11X2${X2}X3${X3} [type=s,weight=2];"
      fi
      if [ "$X1" = "1" -a "$X3" = "4" ] ; then
        echo "X1${X1}X2${X2}X3${X3} -> X12X2${X2}X3${X3} [type=s,weight=2];"
      fi
      if [ "$X1" = "2" -a "$X3" = "5" ] ; then
        echo "X1${X1}X2${X2}X3${X3} -> X14X2${X2}X3${X3} [type=s,weight=2];"
      fi
      if [ "$X1" = "4" ] ; then
        echo "X1${X1}X2${X2}X3${X3} -> X11X2${X2}X3${X3} [type=s,weight=2];"
      fi
      if [ "$X1" = "2" ] ; then
        echo "X1${X1}X2${X2}X3${X3} -> X13X2${X2}X3${X3} [type=s,weight=2];"
      fi
      if [ "$X2" = "2" -a "$X3" = "6" ] ; then
        echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X21X3${X3} [type=s,weight=2];"
      fi
      if [ "$X2" = "1" -a "$X3" = "7" ] ; then
        echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X23X3${X3} [type=s,weight=2];"
      fi

      if [ "$X2" = "3" ] ; then
        echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X21X3${X3} [type=s,weight=2];"
      fi
      if [ "$X2" = "1" ] ; then
        echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X22X3${X3} [type=s,weight=2];"
      fi


      if [ "$X3" = "1" ] ; then         
        echo "X1${X1}X2${X2}X31 -> X1${X1}X2${X2}X32 [type=s,weight=2];"
        echo "X1${X1}X2${X2}X31 -> X1${X1}X2${X2}X33 [type=s,weight=2];"
        echo "X1${X1}X2${X2}X31 -> X1${X1}X2${X2}X34 [type=s,weight=2];"
        echo "X1${X1}X2${X2}X31 -> X1${X1}X2${X2}X35 [type=s,weight=2];"
        echo "X1${X1}X2${X2}X31 -> X1${X1}X2${X2}X36 [type=s,weight=2];"
        echo "X1${X1}X2${X2}X31 -> X1${X1}X2${X2}X37 [type=s,weight=2];"
        echo "X1${X1}X2${X2}X31 -> X1${X1}X2${X2}X38 [type=s,weight=2];"
      else
        echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X2${X2}X31 [type=s,weight=2];"
      fi
    
    
    done
  done
done

echo "}"
