#!/bin/bash

export domName=$1 
export probName=$2 
export outName=$3

#outName=${outName%.pddl}


#echo $domName $probName $outName



# ln -s "../$domName" "results/$outName.dom.pddl"
# ln -s "../$probName" "results/$outName.prob.pddl"

# if [ -f "exp_results/$outName.bound.out" ]; then
#   exit
# fi

# while [ `squeue | grep abd | wc -l` -gt 300 ]
# do
#   sleep 2
# done


# The next few loops are to make sure that mistakes discovered in problems are verified.

# if grep -q "not valid" exp6_data/$outName.FDsat.plan.INVAL_validity_PDDL; then
#   rm exp6_data/$outName.FDsat.plan.INVAL_validity_PDDL
#   rm exp6_data/$outName.FDsat.plan.INVAL_validity_PDDL_time
#   timeout 10m ./validate_FD_plan_INVAL.sh
# fi

# if grep -q "invalid" exp6_data/$outName.FDsat.plan.VAL_validity_PDDL || grep -q "execution failed" exp6_data/$outName.FDsat.plan.VAL_validity_PDDL; then
#   rm exp6_data/$outName.FDsat.plan.VAL_validity_PDDL
#   rm exp6_data/$outName.FDsat.plan.VAL_validity_PDDL_time
#   timeout 10m ./validate_FD_plan_VAL.sh
# fi

# if ! grep -q "s VERIFIED" exp6_data/$outName.FDsat.plan.ISABELLE_VALIDATOR_validity_PDDL && ! grep -q "Error parsing" exp6_data/$outName.FDsat.plan.ISABELLE_VALIDATOR_validity_PDDL; then
#   rm exp6_data/$outName.FDsat.plan.ISABELLE_VALIDATOR_validity_PDDL
#   rm exp6_data/$outName.FDsat.plan.ISABELLE_VALIDATOR_validity_PDDL_time
#   timeout 10m ./validate_FD_plan_ISABELLE_VALIDATOR.sh
# fi




# while [ `ps -e | grep validate_ | wc -l` -gt 6 ]
# do
#   sleep 1
# done

# timeout 10m ./validate_FD_plan_VAL.sh &


# while [ `ps -e | grep validate_ | wc -l` -gt 6 ]
# do
#   sleep 1
# done
# timeout 10m ./validate_FD_plan_INVAL.sh &


# while [ `ps -e | grep compute_ | wc -l` -gt 6 ]
# do
#   sleep 1
# done

# ./compute_Malgo.sh &

# while [ `ps -e | grep compute_ | wc -l` -gt 6 ]
# do
#   sleep 1
# done

# if [ $(( ( RANDOM % 10 )  + 1 )) -ge 6 ]; then
#   exit -1;
# fi

while [ `squeue | grep -v JOBID | wc -l` -gt 100 ]
do
  sleep 1
done

# sbatch --mem=8000 --time=180 ./compute_Malgo.sh
# sbatch --mem=8000 --time=180 ./compute_Nalgo.sh
# sbatch --mem=8000 --time=180 ./compute_Salgo.sh
# sbatch --mem=6000 --time=20 ./compute_Balgo.sh
sbatch --mem=4000 --time=20 ./compute_Balgo_TD.sh
sbatch --mem=4000 --time=20 ./compute_Balgo_RD.sh
sbatch --mem=4000 --time=20 ./compute_Balgo_RD.TDgt2.sh
sbatch --mem=4000 --time=20 ./compute_Balgo_RD.TDgt2.SSle50bound.sh
# sbatch --mem=4000 --time=20 ./translate.sh

# sbatch --mem=8000 --time=30 ./plan_madagascar_no_bound.sh
# planningAlgos="M_noshort Mp_noshort MpC_noshort"
# planningAlgos="M_noshort"
# boundingAlgos="Balgo.acycNalgo.SASSCCs.TD Balgo.acycNalgo.SASSCCs Balgo.acycNalgo.noSASSCCs.TD Balgo.acycNalgo.noSASSCCs"
# boundingAlgos="Balgo.acycNalgo.noSASSCCs Balgo.acycNalgo.noSASSCCs.TD"
# for boundingAlgo in $boundingAlgos; do
#   export boundingAlgo
#   for planningAlgo in $planningAlgos; do
#     export planningAlgo
#     sbatch --mem=8000 --time=30 ./plan_madagascar_with_bound.sh
#   done
# done
# sbatch --mem=8000 --time=180 ./compute_Balgo.sh
# sbatch --mem=8000 --time=180 ./compute_Balgo.sh
# sbatch --mem=8000 --time=180 ./compute_Balgo.sh


# ./analyse_results.sh

# sbatch --mem=32192 --time=180 ./plan_madagascar_with_bound.sh
# sbatch --mem=32192 --time=180 ./plan_fdownward_unsat.sh

#sbatch --mem=32192 --time=180 ./plan_madagascar_no_bound.sh
#sbatch --mem=32192 --time=180 ./plan_fdownward_sat.sh

#sbatch --mem=8000 --time=20 ./validate_FD_plan_VAL.sh

#sbatch --mem=8000 --time=60 ./validate_FD_plan_ISABELLE_VALIDATOR_no_types.sh

# sbatch --mem=32192 --time=60 ./validate_FD_plan_ISABELLE_VALIDATOR_no_types.sh
