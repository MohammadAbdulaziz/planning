#!/bin/bash
#Putting all bounds in a csv file

 

min_data_points=20;

  # # Print all bounds computed in less than 60 sec with/ w/o snapshotting

# AllAlgos="Malgo.noSASSCCs Salgo.noSASSCCs Nalgo.SASSCCs Balgo.SASSCCs Nalgo.noSASSCCs Balgo.noSASSCCs Nalgo.acycNalgo.SASSCCs Balgo.acycNalgo.SASSCCs Nalgo.acycNalgo.noSASSCCs Balgo.acycNalgo.noSASSCCs Nalgo.acycNalgo.SASSCCs.TD Balgo.acycNalgo.SASSCCs.TD Nalgo.acycNalgo.noSASSCCs.TD Balgo.acycNalgo.noSASSCCs.TD TD"

#For the AAAI paper
#AllAlgos="Nalgo.acycNalgo.SASSCCs Balgo.acycNalgo.SASSCCs Nalgo.acycNalgo.SASSCCs.TD Balgo.acycNalgo.SASSCCs.TD"

#For the JACM paper
#AllAlgos="Malgo.noSASSCCs Nalgo.noSASSCCs Malgo.noSASSCCs.noOverflow Nalgo.noSASSCCs.noOverflow Salgo.noSASSCCs Balgo.acycNalgo.noSASSCCs"

AllAlgos="Malgo.noSASSCCs.noOverflow Nalgo.noSASSCCs.noOverflow Salgo.noSASSCCs Balgo.noSASSCCs"
declare -A ALGNAMES
ALGNAMES[Malgo.noSASSCCs]="Msum<EXP,noSAS>"
ALGNAMES[Nalgo.noSASSCCs]="Nsum<EXP,noSAS>"
ALGNAMES[Malgo.noSASSCCs.noOverflow]="Msum<EXP,noSAS,noOverflow>"
ALGNAMES[Nalgo.noSASSCCs.noOverflow]="Nsum<EXP,noSAS,noOverflow>"
ALGNAMES[Salgo.noSASSCCs]="Smax<EXP,noSAS>"
ALGNAMES[Balgo.noSASSCCs]="HYB<EXP,noSAS>"
ALGNAMES[Nalgo.SASSCCs]="Nsum<EXP>"
ALGNAMES[Balgo.SASSCCs]="HYB<EXP>"
ALGNAMES[Nalgo.acycNalgo.SASSCCs]="Nsum<EXP,acycN>"
ALGNAMES[Balgo.acycNalgo.SASSCCs]="HYB<EXP,acycN>"
ALGNAMES[Nalgo.acycNalgo.noSASSCCs]="Nsum<EXP,noSAS,acycN>"
ALGNAMES[Balgo.acycNalgo.noSASSCCs]="HYB<EXP,noSAS,acycN>"
ALGNAMES[Nalgo.acycNalgo.SASSCCs.TD]="Nsum<ARB,acycN>"
ALGNAMES[Balgo.acycNalgo.SASSCCs.TD]="HYB<ARB,noSAS,acycN>"
ALGNAMES[Nalgo.acycNalgo.noSASSCCs.TD]="Nsum<ARB,noSAS,acycN>"
ALGNAMES[Balgo.acycNalgo.noSASSCCs.TD]="HYB<ARB,noSAS,acycN>"
ALGNAMES[TD]="ARB"

declare -A CUTOFF
CUTOFF[Malgo.noSASSCCs]=2000000000
CUTOFF[Nalgo.noSASSCCs]=2000000000
CUTOFF[Malgo.noSASSCCs.noOverflow]=2000000000
CUTOFF[Nalgo.noSASSCCs.noOverflow]=2000000000
CUTOFF[Salgo.noSASSCCs]=800000000
CUTOFF[Balgo.noSASSCCs]=800000000
CUTOFF[Nalgo.SASSCCs]=2000000000
CUTOFF[Balgo.SASSCCs]=800000000
CUTOFF[Nalgo.acycNalgo.SASSCCs]=2000000000
CUTOFF[Balgo.acycNalgo.SASSCCs]=800000000
CUTOFF[Nalgo.acycNalgo.noSASSCCs]=2000000000
CUTOFF[Balgo.acycNalgo.noSASSCCs]=800000000
CUTOFF[Nalgo.acycNalgo.SASSCCs.TD]=1400000000
CUTOFF[Balgo.acycNalgo.SASSCCs.TD]=400000000
CUTOFF[Nalgo.acycNalgo.noSASSCCs.TD]=1400000000
CUTOFF[Balgo.acycNalgo.noSASSCCs.TD]=400000000
CUTOFF[TD]=10000000000



exp_data=JACM_data/exp_results_10_jan_2020

# rm *.png
# rm *_assorted
# rm *_joined
# for algo in $AllAlgos; do
#   for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
#     echo $dom
#     for file in `ls ../${exp_data}/*.algo.${algo}.bound.out |  grep -i $dom`; do
#       name=${file//..\/${exp_data}/}
#       name=${name//.$algo.bound.out/}
#       if [ -s $file ]; then
#         if [ `gawk '{print $1}' $file` -gt 0 ]; then 
#           if [ `gawk '{print $1}' $file` -lt ${CUTOFF[$algo]} ]; then 
#             echo -n $name >> ${dom}_bounds_${algo}_assorted
#             echo -n " " >> ${dom}_bounds_${algo}_assorted
#             echo `gawk '{print $1}' $file` >> ${dom}_bounds_${algo}_assorted

#             echo -n $name >> ${dom}_runtimes_${algo}_assorted
#             echo -n " " >> ${dom}_runtimes_${algo}_assorted
#             echo `gawk '{print $2}' $file` >> ${dom}_runtimes_${algo}_assorted
#           fi
#         fi 

#         echo -n $name >> ${dom}_prob_dom_${algo}_assorted
#         echo -n " " >> ${dom}_prob_dom_${algo}_assorted
#         echo `gawk '{print $3}' $file` >> ${dom}_prob_dom_${algo}_assorted

#         echo -n $name >> ${dom}_atom_dom_${algo}_assorted
#         echo -n " " >> ${dom}_atom_dom_${algo}_assorted
#         echo `gawk '{print $9}' $file` >> ${dom}_atom_dom_${algo}_assorted
#       fi;
#     done 
#   done
# done

# rm experiments_table.tex


#This is for diameterBoundingJournal

# Get scientific number with correct accuracy
nat_to_sc() {
if [ -z "$1" ]; then
    echo "---------"
else
    length=3
    result=$(printf "%1.${length}e" $1)
    result="${result%[eE]*}e"$(echo "${result#*[eE]}" | sed -e 's/+//g' -e 's/^0//' -e 's/-0/-/')
    echo $result
fi
}


produce_max_min_n_table () {
AlgosInTable=$AllAlgos
rm experiments_table.tex
echo "\begin{table}">> experiments_table.tex
echo "\begin{center}">> experiments_table.tex
echo "    \begin{tabularx}{0.45\textwidth}{| l | X | X | X | X |}">> experiments_table.tex
echo "    %p{5cm} |}">> experiments_table.tex
echo "    \hline">> experiments_table.tex
echo -n "          {\tiny }">> experiments_table.tex
for algo in $AlgosInTable; do
  echo -n "       &{\tiny ${ALGNAMES[$algo]}}" >> experiments_table.tex
done
echo -n "\\\\">> experiments_table.tex
echo " " >>experiments_table.tex
for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
  echo $dom
  for algo in $AlgosInTable; do
    cat ${dom}_bounds_${algo}_assorted | awk '{print $2}' | sort -n > ${dom}_bounds_${algo}
  done
done
for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
  echo -n "{\scriptsize ">> experiments_table.tex
  echo -n  ${dom} >> experiments_table.tex
  numInsts=`ls ../${exp_data}/*.algo.Balgo.noSASSCCs.bound.out |  grep -i $dom | wc -l`      
  echo -n " ($numInsts) " >> experiments_table.tex
  echo -n "}" >> experiments_table.tex
  for algo in $AlgosInTable; do
    min_bound=$(head -n 1 ${dom}_bounds_${algo})
    max_bound=`awk '/./{line=$0} END{print line}' ${dom}_bounds_${algo}`
    echo -n "       &{\scriptsize " >> experiments_table.tex
    echo -n `nat_to_sc $min_bound` >> experiments_table.tex
    echo -n " / " >> experiments_table.tex
    echo -n `nat_to_sc $max_bound` " " >> experiments_table.tex
    echo -n \(`wc -l < ${dom}_bounds_${algo}`\) >> experiments_table.tex
    echo -n  >> experiments_table.tex
    echo -n "}" >> experiments_table.tex
  done
  echo -n "\\\\ ">> experiments_table.tex
  echo " " >> experiments_table.tex
done
}
produce_max_min_n_table

# produce_histo_table () {
# AlgosInTable="TD Nalgo.acycNalgo.SASSCCs Nalgo.acycNalgo.SASSCCs.TD Balgo.acycNalgo.SASSCCs Balgo.acycNalgo.SASSCCs.TD"
# echo "\begin{table}">> experiments_table.tex
# echo "\begin{center}">> experiments_table.tex
# echo "    \begin{tabularx}{0.45\textwidth}{| l | X | X | X | X | X |}">> experiments_table.tex
# echo "    %p{5cm} |}">> experiments_table.tex
# echo "    \hline">> experiments_table.tex
# echo -n "          {\tiny }">> experiments_table.tex
# for algo in $AlgosInTable; do
#   echo -n "       &{\tiny ${ALGNAMES[$algo]}}" >> experiments_table.tex
# done
# echo -n "\\\\">> experiments_table.tex
# echo " " >>experiments_table.tex
# for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
#   echo $dom
#   for algo in $AlgosInTable; do
#     cat ${dom}_bounds_${algo}_assorted | awk '{print $2}' | sort -n > ${dom}_bounds_${algo}
#   done
# done
# histEntries="1000 1000000 1000000000"
# for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
#   echo -n "{\tiny ">> experiments_table.tex
#   echo -n  ${dom} >> experiments_table.tex
#   echo -n "}" >> experiments_table.tex
#   for algo in $AlgosInTable; do
#     echo -n "       &{\tiny " >> experiments_table.tex
#     oldnumInsts="-1"
#     for histEnt in $histEntries; do
#       numInsts=`cat ${dom}_bounds_${algo} |  awk '($1 < '"$histEnt"')' | wc -l`      
#       if [ $numInsts -eq $oldnumInsts ]; then
#         echo -n "-" >> experiments_table.tex
#       else
#         echo -n $numInsts >> experiments_table.tex
#       fi
#       echo -n "/" >> experiments_table.tex
#       oldnumInsts=$numInsts
#     done
#     echo -n  >> experiments_table.tex
#     echo -n "}" >> experiments_table.tex
#   done
#   echo -n "\\\\ ">> experiments_table.tex
#   echo " " >> experiments_table.tex
# done
# }



# # This is used for TD in AAAI
# produce_improvement_table () {
# AlgosInTable="Nalgo.acycNalgo.SASSCCs Balgo.acycNalgo.SASSCCs"
# echo "\begin{table}">> experiments_table.tex
# echo "\begin{center}">> experiments_table.tex
# echo "    \begin{tabularx}{0.45\textwidth}{| l | X | X | X |}">> experiments_table.tex
# echo "    %p{5cm} |}">> experiments_table.tex
# echo "    \hline">> experiments_table.tex
# echo -n "          {\tiny }">> experiments_table.tex
# echo -n "       &{\tiny ${ALGNAMES[$algo]}}" >> experiments_table.tex
# for algo in $AlgosInTable; do
#   echo -n "       &{\tiny ${ALGNAMES[$algo]}}" >> experiments_table.tex
# done
# echo -n "\\\\">> experiments_table.tex
# echo " " >>experiments_table.tex
# for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
#   echo $dom
#   for algo in $AlgosInTable; do
#     cat ${dom}_bounds_${algo}_assorted | awk '{print $2}' | sort -n > ${dom}_bounds_${algo}
#   done
# done
# for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
#   echo -n "{\tiny ">> experiments_table.tex
#   echo -n  ${dom} >> experiments_table.tex
#   echo -n "}" >> experiments_table.tex
#   numInsts=`cat ${dom}_bounds_TD |  awk '($1 < 1000000000)' | wc -l`      
#   echo -n "       &{\tiny " >> experiments_table.tex
#   echo -n $numInsts >> experiments_table.tex
#   echo -n "}" >> experiments_table.tex
#   for algo in $AlgosInTable; do
#     echo -n "       &{\tiny " >> experiments_table.tex
#     ../compare_bounds.sh ${algo}.TD ${algo} ${dom} ../exp_results >> experiments_table.tex
#     echo -n "}" >> experiments_table.tex
#   done
#   echo -n "\\\\ ">> experiments_table.tex
#   echo " " >> experiments_table.tex
# done
# }

# produce_improvement_table

# produce_solved_table () {
# BoundingAlgos="Balgo.acycNalgo.SASSCCs Balgo.acycNalgo.SASSCCs.TD"
# #planningAlgos="Mp_noshort Mp_noshort.0 Mp_noshort.1 M_noshort M_noshort.0 M_noshort.1 MpC_noshort MpC_noshort.0 MpC_noshort.1"
# planningAlgos="M_noshort.0"
# for boundAlgo in $BoundingAlgos; do
#   for planAlgo in $planningAlgos; do
#     for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
#       echo $dom
#       for file in `find ../exp_results/ -iname "*$dom*.$planAlgo.plan.$boundAlgo.out"`; do
#         grep -l "PLAN FOUND:" $file;
#       done > ${planAlgo}_${boundAlgo}_${dom}_sat
#       for file in `find ../exp_results/ -iname "*$dom*.$planAlgo.plan.$boundAlgo.out"`; do
#         grep -l "PLAN NOT FOUND:" $file;
#       done > ${planAlgo}_${boundAlgo}_${dom}_unsat
#     done 
#   done
# done
# }

# produce_solved_table

gnuplot_image () {

imageName=${1//./-}.png
  gnuplot << text
  set terminal pngcairo size 1280,960 font ",16"
  set pointsize 2
  set logscale x 10
  set logscale y 10
  set lmargin 0
  set rmargin 0
  set size square
  set output "log-$imageName"
  # set key outside right height 0
  set key inside right bottom height 0
  key_words_gnuplot_var = system('./print_domains_leq_datapts.sh $2 ${min_data_points}')
  set xlabel "$3"
  show xlabel
  set ylabel "$4"
  show ylabel
  plot for [filename in key_words_gnuplot_var] filename. "$2" using 2:3 title filename, x
text

convert -trim log-$imageName log-$imageName

  gnuplot << text
  set terminal pngcairo size 1280,960 font ",16"
  set pointsize 2
  set lmargin 0
  set rmargin 0
  set size square
  set output "$imageName"
  # set key outside right height 0
  set key inside right bottom height 0
  key_words_gnuplot_var = system('./print_domains_leq_datapts.sh $2 ${min_data_points}')
  set xlabel "$3"
  show xlabel
  set ylabel "$4"
  show ylabel
  plot for [filename in key_words_gnuplot_var] filename. "$2" using 2:3 title filename, x
text

convert -trim $imageName $imageName

}

# # The following two loops generate the plots

for algo1 in $AllAlgos; do
  echo $algo1;
  for algo2 in $AllAlgos; do
    echo $algo1;
    for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
      if [[ -s ${dom}_bounds_${algo1}_assorted ]] && [[ -s ${dom}_bounds_${algo2}_assorted ]]; then
        echo "Joining bounds for"
        echo $dom
        join <(sort -k1,1 ${dom}_bounds_${algo1}_assorted) <(sort -k1,1 ${dom}_bounds_${algo2}_assorted) | tr -d "\r" | awk '$2>0' |  awk '$3>0' > ${dom}_bounds_${algo1}_${algo2}_joined
      fi
      if [[ -s ${dom}_runtimes_${algo1}_assorted ]] && [[ -s ${dom}_runtimes_${algo2}_assorted ]]; then
        echo "Joining runtimes for"
        echo $dom
        join <(sort -k1,1 ${dom}_runtimes_${algo1}_assorted) <(sort -k1,1 ${dom}_runtimes_${algo2}_assorted) | tr -d "\r" | awk '$2>0' |  awk '$3>0' > ${dom}_runtimes_${algo1}_${algo2}_joined
      fi
      if [[ -s ${dom}_atom_dom_${algo1}_assorted ]] && [[ -s ${dom}_atom_dom_${algo2}_assorted ]]; then
        echo "Joining atom dom size for"
        echo $dom
        join <(sort -k1,1 ${dom}_atom_dom_${algo1}_assorted) <(sort -k1,1 ${dom}_atom_dom_${algo2}_assorted) | tr -d "\r" | awk '$2>0' |  awk '$3>0' > ${dom}_atom_dom_${algo1}_${algo2}_joined
      fi
    done
    gnuplot_image labelled-${algo1}-vs-${algo2}-bounds _bounds_${algo1}_${algo2}_joined "Bound computed by ${ALGNAMES[$algo1]}" "Bound computed by ${ALGNAMES[$algo2]}"
    gnuplot_image labelled-${algo1}-vs-${algo2}-runtimes _runtimes_${algo1}_${algo2}_joined "Runtime of ${ALGNAMES[$algo1]} (sec)" "Runtime of ${ALGNAMES[$algo2]} (sec)"
    gnuplot_image labelled-${algo1}-vs-${algo2}-atom-dom _atom_dom_${algo1}_${algo2}_joined "Size of largest abstraction abstraction computed by ${ALGNAMES[$algo1]} (state variable)" "Size of largest abstraction computed by ${ALGNAMES[$algo2]} (state variable)"
  done
done

for algo in $AllAlgos; do
  for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
    if [[ -s ${dom}_prob_dom_${algo}_assorted ]] && [[ -s ${dom}_bounds_${algo}_assorted ]]; then
      echo "Joining prob size and bounds for"
      echo $dom
      join <(sort -k1,1 ${dom}_prob_dom_${algo}_assorted) <(sort -k1,1 ${dom}_bounds_${algo}_assorted) | tr -d "\r" | awk '$2>0' |  awk '$3>0' > ${dom}_prob_dom_bounds_${algo}_joined
    fi
    if [[ -s ${dom}_prob_dom_${algo}_assorted ]] && [[ -s ${dom}_atom_dom_${algo}_assorted ]]; then
      echo "Joining prob size and atom size for"
      echo $dom
      join <(sort -k1,1 ${dom}_prob_dom_${algo}_assorted) <(sort -k1,1 ${dom}_atom_dom_${algo}_assorted) | tr -d "\r" | awk '$2>0' |  awk '$3>0' > ${dom}_prob_dom_atom_dom_${algo}_joined
    fi
  done
  gnuplot_image labelled-${algo}-prob-dom-vs-bounds _prob_dom_bounds_${algo}_joined "Size of concrete problem (state variables)" "Bound computed by ${ALGNAMES[$algo]}"
  gnuplot_image labelled-${algo}-prob-dom-vs-atom-dom _prob_dom_atom_dom_${algo}_joined "Size of concrete problem (state variables)" "Size of largest abstraction computed by ${ALGNAMES[$algo]} (state varaibles)"
done
