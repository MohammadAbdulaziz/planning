#! /bin/bash
exp_results_dir=exp_results
dir=${PWD}
if [ -f "exp_results/$outName.sas" ]; then
  if [ ! -f "exp_results/$outName.$1.$2.bound.out" ]; then
      ID1=$RANDOM
      ID2=$RANDOM
      mkdir /tmp/tmp_${ID1}_${ID2}
      cp -rf  ../../../codeBase/planning/stripsBound/* /tmp/tmp_${ID1}_${ID2}/
      cd /tmp/tmp_${ID1}_${ID2}/
      echo $exp_results_dir
      timeout 30m ./run_sas.sh $dir/$exp_results_dir/$outName.sas -$1 -$2 | grep -v necessary | grep -v acyclic | grep -v over >$dir/$exp_results_dir/$outName.$1.$2.bound.out 2>$dir/$exp_results_dir/$outName.$1.$2.bound.err
      cd ../../../
      rm -rf /tmp/tmp_${ID1}_${ID2}
  fi
fi
