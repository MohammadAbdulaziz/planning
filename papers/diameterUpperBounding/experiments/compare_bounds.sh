#!/bin/bash

# Usage: call this script and give it two algorithm names, a third optional argument which is the domain name, a fourth optional argument which is the directory where the exp data is
# Note that all data has to first be put in exp_results using the script rename_exp_data.sh
# Example usage:
# ./compare_bounds.sh Balgo.acycNalgo.SASSCCs.TD Balgo.acycNalgo.SASSCCs [newopen] [directory_of_exp_results]
leProbs=0
halfLeProbs=0
bound1Probs=0
bound2Probs=0
for file in `find $4 -iname "*$3*.algo.$1.bound.out"`; do
  algo1Bound=`awk '{print $1}' $file`
  algo2Bound=`awk '{print $1}' ${file/$1/$2}`
  if [[ $algo1Bound -lt 0 ]]; then
    algo1Bound=1000000000
  fi
  if [[ $algo2Bound -lt 0 ]]; then
    algo2Bound=1000000000
  fi
  if [[ $algo1Bound -lt 1000000000 ]]; then
    bound1Probs=`echo "$bound1Probs + 1" | bc`
  fi;
  if [[ $algo2Bound -lt 1000000000 ]]; then
    bound2Probs=`echo "$bound2Probs + 1" | bc`
  fi;


  if [[ $algo1Bound -lt $algo2Bound && $algo2Bound -lt 1000000000 ]]; then
    # echo -n ${file/exp_results//};
    # echo -n " "
    ratio=$(echo "`awk '{print $1}' $file`/`awk '{print $1}' ${file/$1/$2}`" | bc -l)
    if (( `echo $ratio '>=' 0.5 | bc -l `)); then
      halfLeProbs=`echo "$halfLeProbs + 1" | bc`
    fi
    # echo "`awk '{print $1}' $file`/`awk '{print $1}' ${file/$1/$2}`";
    leProbs=`echo "$leProbs + 1" | bc`
  fi;
done

echo "$bound2Probs $bound1Probs $leProbs $halfLeProbs"
echo "$leProbs/$bound2Probs" | bc -l
if [ $leProbs -gt "0" ]; then
  echo "$halfLeProbs/$leProbs" | bc -l
else
  echo "-"
fi
# echo -n "$bound2Probs:$bound1Probs:$leProbs:$halfLeProbs"
