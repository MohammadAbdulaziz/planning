#!/bin/bash

mkdir exp_results/

# rm -rf exp_results_no_overflow/
# tar xzf exp_results_no_overflow.tar.gz
for file in `ls exp_results/*.Balgo.bound.out`; do cp $file ${file/Balgo./algo.Balgo.noSASSCCs.noOverflow.RD.TDgt2.SSle50}; done
#rsync -r --include='*.out' --exclude='*' exp_results_no_overflow/ exp_results/
#rm -rf exp_results_no_overflow/


# rm -rf exp_results_Nalgo_noSASSCCs/
# tar xzf exp_results_Nalgo_noSASSCCs.tar.gz
# for file in `ls exp_results_Nalgo_noSASSCCs/*.Nalgo.*.out`; do mv $file ${file/Nalgo./algo.Nalgo.noSASSCCs.}; done
# for file in `ls exp_results_Nalgo_noSASSCCs/*.Balgo.*.out`; do mv $file ${file/Balgo./algo.Balgo.noSASSCCs.}; done
# for file in `ls exp_results_Nalgo_noSASSCCs/*.Malgo.*.out`; do mv $file ${file/Malgo./algo.Malgo.noSASSCCs.}; done
# for file in `ls exp_results_Nalgo_noSASSCCs/*.Salgo.*.out`; do mv $file ${file/Salgo./algo.Salgo.noSASSCCs.}; done
# rsync -r --include='*.out' --exclude='*' exp_results_Nalgo_noSASSCCs/ exp_results/
# rm -rf exp_results_Nalgo_noSASSCCs/

# rm -rf exp_results_Nalgo_SASSCCs/
# tar xzf exp_results_Nalgo_SASSCCs.tar.gz
# for file in `ls exp_results_Nalgo_SASSCCs/*.Nalgo.*.out`; do mv $file ${file/Nalgo./algo.Nalgo.SASSCCs.}; done
# for file in `ls exp_results_Nalgo_SASSCCs/*.Balgo.*.out`; do mv $file ${file/Balgo./algo.Balgo.SASSCCs.}; done
# rsync -r --include='*.out' --exclude='*' exp_results_Nalgo_SASSCCs/ exp_results/
# rm -rf exp_results_Nalgo_SASSCCs/

# rm -rf exp_results_acycNalgo_SASSCCs/
# tar xzf exp_results_acycNalgo_SASSCCs.tar.gz
# for file in `ls exp_results_acycNalgo_SASSCCs/*.Nalgo.*.out`; do mv $file ${file/Nalgo./algo.Nalgo.acycNalgo.SASSCCs.}; done
# for file in `ls exp_results_acycNalgo_SASSCCs/*.Balgo.*.out`; do mv $file ${file/Balgo./algo.Balgo.acycNalgo.SASSCCs.}; done
# rsync -r --include='*.out' --exclude='*' exp_results_acycNalgo_SASSCCs/ exp_results/
# rm -rf exp_results_acycNalgo_SASSCCs/

# rm -rf exp_results_acycNalgo_noSASSCCs/
# tar xzf exp_results_acycNalgo_noSASSCCs.tar.gz
# for file in `ls exp_results_acycNalgo_noSASSCCs/*.Nalgo.*.out`; do mv $file ${file/Nalgo./algo.Nalgo.acycNalgo.noSASSCCs.}; done
# for file in `ls exp_results_acycNalgo_noSASSCCs/*.Balgo.*.out`; do mv $file ${file/Balgo./algo.Balgo.acycNalgo.noSASSCCs.}; done
# rsync -r --include='*.out' --exclude='*' exp_results_acycNalgo_noSASSCCs/ exp_results/
# rm -rf exp_results_acycNalgo_noSASSCCs/

# rm -rf exp_results_acycNalgo_SASSCCs_TD/
# tar xzf exp_results_acycNalgo_SASSCCs_TD.tar.gz                                      
# for file in `ls exp_results_acycNalgo_SASSCCs_TD/*.Nalgo.*.out`; do mv $file ${file/Nalgo./algo.Nalgo.acycNalgo.SASSCCs.TD.}; done
# for file in `ls exp_results_acycNalgo_SASSCCs_TD/*.Balgo.*.out`; do mv $file ${file/Balgo./algo.Balgo.acycNalgo.SASSCCs.TD.}; done
# rsync -r --include='*.out' --exclude='*' exp_results_acycNalgo_SASSCCs_TD/ exp_results/
# rm -rf exp_results_acycNalgo_SASSCCs_TD/

# rm -rf exp_results_acycNalgo_noSASSCCs_TD/
# tar xzf exp_results_acycNalgo_noSASSCCs_TD.tar.gz
# for file in `ls exp_results_acycNalgo_noSASSCCs_TD/*.Nalgo.*.out`; do mv $file ${file/Nalgo./algo.Nalgo.acycNalgo.noSASSCCs.TD.}; done
# for file in `ls exp_results_acycNalgo_noSASSCCs_TD/*.Balgo.*.out`; do mv $file ${file/Balgo./algo.Balgo.acycNalgo.noSASSCCs.TD.}; done
# rsync -r --include='*.out' --exclude='*' exp_results_acycNalgo_noSASSCCs_TD/ exp_results/
# rm -rf exp_results_acycNalgo_noSASSCCs_TD/

# rm -rf exp_results_TD/
# tar xzf exp_results_TD.tar.gz
# for file in `ls exp_results_TD/*.Nalgo.*.out`; do mv $file ${file/Nalgo./algo.TD.}; done
# rsync -r --include='*.out' --exclude='*' exp_results_TD/ exp_results/
# rm -rf exp_results_TD/
