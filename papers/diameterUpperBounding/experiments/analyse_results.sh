#!/bin/bash
#modified_outName=`echo $outName | sed 's/_aux//g'`
# modified_outName=$outName

# for solver in "M Mp MpC"; do
#   for boundingAlgo in "nobound withbound.Balgo.acycNalgo.SASSCCs.TD withbound.Balgo.acycNalgo.SASSCCs"; do
#     plannerOutFileName="exp_data/${modified_outName}.$solver.plan.$boundingAlgo.out"      
#     if [ ! -f $plannerOutFileName ]; then
#       echo $plannerOutFileName not found
#     fi    
#     totalTime=`cat $plannerOutFileName | grep "total time" | gawk '{print $3}'`
#     if grep -q "PLAN FOUND:" $plannerOutFileName; then
#       echo ${modified_outName} $totalTime >>$solver.$boundingAlgo.sat.solvedprobs
#     fi
#     if grep -q "PLAN NOT FOUND:" $plannerOutFileName;then
#       # check for instances labelled as sat and unsat by Mp
#       if ! grep -q "PLAN FOUND" exp_data/${modified_outName}.Mp.plan.nobound.out 2>/dev/null; then
#         echo ${modified_outName} $totalTime >>$solver.$boundingAlgo.unsat.solvedprobs
#       else
#        echo ${modified_outName} $totalTime >>$solver.$boundingAlgo.unsat.solvedprobs.debug
#       fi
#     fi
#   done
# done


for file in `ls exp_results/*TD.bound.out`; do
  file2=${file/algo.Balgo.noSASSCCs.noOverflow.TD./Balgo.};
  if [ -f $file2 ]; then
    x=`awk '{print $2}' $file`;
    y=`awk '{print $2}' $file2`;
    if (( $(echo "$x > $y" | bc) )); then
      echo -n $x;
      echo -n " ";
      echo -n $y;
      echo -n " ";
      echo -n $file
      echo -n " ";
      echo $file2
    fi
  fi
done
