#!/bin/bash

exp_results_dir=exp_results
dir=${PWD}

if [ ! -f "$exp_results_dir/$outName.sas" ]; then
    ID1=$RANDOM
    ID2=$RANDOM
    mkdir /tmp/tmp_${ID1}_${ID2}
    cp -rf  ../../../codeBase/* /tmp/tmp_${ID1}_${ID2}/
    cd /tmp/tmp_${ID1}_${ID2}/planning/stripsBound/

    rm *.pddl* *.sas* output
    CODE_BASE_DIR=../
    FD_DIR=$CODE_BASE_DIR/FD/
    export CODE_BASE_DIR
    export FD_DIR

    $FD_DIR/src/translate/translate.py $domName $probName > /dev/null
    $FD_DIR/src/preprocess/preprocess < output.sas > /dev/null

    cd $dir
    cp /tmp/tmp_${ID1}_${ID2}/planning/stripsBound//output $exp_results_dir/$outName.sas
    rm -rf /tmp/tmp_${ID1}_${ID2}

fi
