#!/bin/sh

cd ../../../codeBase/Madagascar/
echo $outName
#modified_outName=`echo $outName | sed 's/_aux//g'`
modified_outName="$outName"
echo $modified_outName
#boundingAlgo=Balgo.acycNalgo.SASSCCs.TD
# boundingAlgo=Balgo.acycNalgo.SASSCCs
# planningAlgo=M
boundval=`cat ../../papers/diameterUpperBounding/experiments/exp_results/$modified_outName.algo.$boundingAlgo.bound.out |  awk '{print $1}'`
echo $boundval
if echo $boundval | egrep -q '^[0-9]+$'; then #boundval is a number
  if [ $boundval -gt 0 ]; then 
    # if [ ! -f "../../papers/diameterUpperBounding/experiments/exp_results/$outName.$planningAlgo.plan.withbound.$boundingAlgo.out" ]; then
    #   ./$planningAlgo -m 100000 -F $boundval -T $boundval $domName $probName >../../papers/diameterUpperBounding/experiments/exp_results/$outName.$planningAlgo.plan.withbound.$boundingAlgo.out 2>../../papers/diameterUpperBounding/experiments/exp_results/$outName.$planningAlgo.plan.withbound.$boundingAlgo.err
    # fi
    if [ ! -f "../../papers/diameterUpperBounding/experiments/exp_results/$outName.$planningAlgo.0.plan.withbound.$boundingAlgo.out" ]; then
      ./$planningAlgo -P 0 -m 100000 -F $boundval -T $boundval $domName $probName >../../papers/diameterUpperBounding/experiments/exp_results/$outName.$planningAlgo.0.plan.withbound.$boundingAlgo.out 2>../../papers/diameterUpperBounding/experiments/exp_results/$outName.$planningAlgo.0.plan.withbound.$boundingAlgo.err
    fi
    # if [ ! -f "../../papers/diameterUpperBounding/experiments/exp_results/$outName.$planningAlgo.1.plan.withbound.$boundingAlgo.out" ]; then
    #   ./$planningAlgo -P 1 -m 100000 -F $boundval -T $boundval $domName $probName >../../papers/diameterUpperBounding/experiments/exp_results/$outName.$planningAlgo.1.plan.withbound.$boundingAlgo.out 2>../../papers/diameterUpperBounding/experiments/exp_results/$outName.$planningAlgo.1.plan.withbound.$boundingAlgo.err
    # fi
  fi
fi
