#!/bin/sh
cd ../../../codeBase/Madagascar/
if [ ! -f "../../papers/diameterUpperBounding/experiments/exp_results/$outName.M.plan.nobound.out" ]; then
  ./M -m 100000 $domName $probName >../../papers/diameterUpperBounding/experiments/exp_results/$outName.M.plan.nobound.out 2>../../papers/diameterUpperBounding/experiments/exp_results/$outName.M.plan.nobound.err
fi
