#!/bin/bash

#This generates a dot file that can be drawn using dot,
#the command is dot -Tpng <(./generateSalgoTightellProofFig1.sh) -o SalgoTightellProofFig1.png

echo "strict digraph gr {"
echo "splines=true;"
echo "sep=\"+2,2\";"
echo "graph [fontsize=10 ];"
echo "node [style=filled, fillcolor=white, shape=rectangle,ranksep=1]"

# echo "{rank=same;"

# for X1 in 0 1 2 3 4
# do
#   for X2 in 0 1 2 3 4
#   do
#     if [ "$X1" = "0" -o "$X2" = "0" ] ; then
#       echo -n "X1${X1}X2${X2} "
#    fi
#   done
# done

# echo "}"


#echo "concentrate=true"
echo "X11X21 [fillcolor=blue,margin=0,height=0,width=0.03,label=11]"
echo "X12X21 [fillcolor=red,margin=0,height=0,width=0.03,label=21]"
echo "X11X22 [fillcolor=red,margin=0,height=0,width=0.03,label=12]"
echo "X11X24 [fillcolor=red,margin=0,height=0,width=0.03,label=14]"
echo "X12X24 [fillcolor=green,margin=0,height=0,width=0.03,label=24]"

for X1 in 1 2 3
do
  for X2 in 1 2 3 4
  do
    echo "X1${X1}X2${X2} [margin=0,height=0,width=0.03,label=${X1}${X2}]"
    if [ "$X1" = "1" -a "$X2" = "1" ] ; then
      echo "X1${X1}X2${X2} -> X12X2${X2} [type=s,weight=2];"
    fi
    if [ "$X1" = "1" -a "$X2" = "2" ] ; then
      echo "X1${X1}X2${X2} -> X12X2${X2} [type=s,weight=2];"
    fi
    if [ "$X1" = "2" -a "$X2" = "2" ] ; then
      echo "X1${X1}X2${X2} -> X13X2${X2} [type=s,weight=2];"
    fi
    if [ "$X1" = "1" -a "$X2" = "3" ] ; then
      echo "X1${X1}X2${X2} -> X12X2${X2} [type=s,weight=2];"
    fi
    if [ "$X1" = "1" -a "$X2" = "4" ] ; then
      echo "X1${X1}X2${X2} -> X12X2${X2} [type=s,weight=2];"
    fi
    if [ "$X2" = "1" ] ; then
      echo "X1${X1}X2${X2} -> X11X22 [type=s,weight=2];"
      echo "X1${X1}X2${X2} -> X11X23 [type=s,weight=2];"
    fi
    if [ "$X2" = "2" ] ; then
      echo "X1${X1}X2${X2} -> X11X24 [type=s,weight=2];"
    fi
    if [ "$X2" = "3" ] ; then
      echo "X1${X1}X2${X2} -> X11X24 [type=s,weight=2];"
    fi
  done
done

echo "}"
