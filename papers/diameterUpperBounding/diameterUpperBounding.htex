\documentclass[11pt]{article}
\usepackage[titletoc,toc,title]{appendix}
\usepackage{geometry}
\geometry{
  a4paper,         % or letterpaper
  textwidth=16cm,  % llncs has 12.2cm
  textheight=25cm, % llncs has 19.3cm
  heightrounded,   % integer number of lines
  hratio=1:1,      % horizontally centered
  vratio=1:1,      % not vertically centered
}
\usepackage[singlespacing]{setspace}
\renewcommand{\baselinestretch}{1}
\usepackage[T1]{fontenc} \usepackage[linesnumbered,ruled,vlined,noalgohanging]{algorithm2e}
\usepackage{stmaryrd}
\usepackage{alltt}
\usepackage{tikz}
\usepackage{setspace}
\usetikzlibrary{arrows,positioning,decorations.markings}
\let\proof\relax
\let\endproof\relax
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\newtheorem{mydef}{Definition}
\newtheorem{mylem}{Lemma}
\newtheorem{mythm}{Theorem}
\newtheorem{myprop}{Proposition}
\newtheorem{mynthm}{Not-A-Theorem}
\newtheorem{mycor}{Corollary}
\theoremstyle{remark}
\newtheorem{myeg}{Example}
\newtheorem{mycase}{Case}
\usepackage{relsize}
\usepackage{holtexbasic}
\usepackage{alltt}
\newcommand{\holtxt}[1]{\textsf{#1}}
\renewcommand{\HOLConst}[1]{\holtxt{#1}}
\renewcommand{\HOLSymConst}[1]{\holtxt{#1}}
\renewcommand{\HOLKeyword}[1]{\texttt{#1}}
\renewcommand{\HOLTyOp}[1]{\mbox{\fontencoding{T1}\fontfamily{bch}\fontseries{m}\fontshape{it}\fontsize{9.5}{10}\selectfont #1}}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\captionsetup{compatibility=false}
\usepackage{latexsym}
\usepackage[inline]{enumitem}
\setlist[enumerate]{label=(\roman*)}
\usepackage[Euler]{upgreek}
\usepackage{color}
\usepackage{url}
%\usepackage[UKenglish]{babel}
\usepackage{hieroglf}
\pdfmapfile{+hieroglf.map}
\newcommand{\vs}{\ensuremath{\mathit{vs}}}
\tikzset{
  varnode/.style={rectangle,outer sep=0mm,draw},
  varnodenoperi/.style={rectangle,outer sep=-1mm},
  ourarrow/.style={>=stealth,thick},
  ourarc/.style={>=stealth,thick,bend right=60}
}
\usepackage{times}
\usepackage{tabularx}
\usepackage{mathtools}
\usepackage{placeins}
\usepackage{hyperref}
\usetikzlibrary{fit,shapes.geometric}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\makeatletter
\newcommand\footnoteref[1]{\protected@xdef\@thefnmark{\ref{#1}}\@footnotemark}
\newcommand\citep[1]{\cite{#1}}
\makeatother
\newcommand{\moham}[1]{Mohammad: \color{green} #1}
\newcommand{\Charles}[1]{Charles: \color{blue} #1}

%%\newcommand{\Omit}[1]{}

\title{Compositional Upper-Bounding of Digraph Topological Properties}

\author{%% Mohammad Abdulaziz\\Technical University of Munich\\
%% email:\url{mohammad.abdulaziz@in.tum.de}
%% Charles Gretton 
}
\pdfinfo{
/Title(Compositional Upper-Bounding of Digraph Topological Properties)
/Author(Mohammad Abdulaziz)
}

\begin{document}

\maketitle
\input{macros}

\begin{abstract}

  \Charles{
  In AI planning and model checking a problem is described using a succinct representation of the underlying transition system.
  That system corresponds to a directed graph ({\em digraph}), where vertices and edges model states and transitions, respectively.
  Of interest is the default situation, where for the reason of Bellman's so-called {\em curse of dimensionality} that digraph is so large it cannot be represented explicitly.
  A number of solution concepts in this setting require we first obtain, or otherwise bound a topological property of the succinctly represented graph.
Examples of properties of interest here are: \begin{enumerate*}\item the diameter (the length of the longest {\em optimal} path),  and \item  the recurrence diameter (the length of the longest {\em simple} path)\end{enumerate*}.
We develop and study {\em compositional} approaches to compute upper bounds on such topological properties.
Compositional approaches proceed by treating small abstractions of the concrete system.
Properties are calculated directly for very small abstractions, and then composed to obtain a bound on the value the the property for the concrete system.
  
%%
We present new compositional algorithms and related abstraction techniques.
These are implemented, and those implementations form the basis of a detailed empirical study. 
We also develop theoretical results that capture exactly the limitations and tightness of compositional bounding approaches.
}

\Omit{
Our results include new compositional bounding algorithms based on existing and new abstraction techniques, as well as theoretical results on the possibility of compositional bounding for different topological properties, and the optimality of our compositional algorithms.
We also provide implementations of our algorithms which we apply to standard AI~planning and verification problems, showing how the computed bounds can significantly improve the state-of-the-art for planning and verification problems.
}

\Omit{  
Due to their small size, succinct representations of digraphs arise in both practical and theoretical settings.
In applications like artificial intelligence (AI) planning and model checking, computing or upper-bounding topological properties of succinct graphs, such as the diameter (the longest shortest path) or the recurrence diameter (the length of the longest simple path), can be necessary.
Nonetheless, solving problems on succinct digraphs can be exponentially harder than for explicitly represented digraphs.
This is because, effectively, one needs to construct the explicit digraph.
%% The above two points are not core to this work, and so I would say do not belong in the abstract.
}

\Omit{
Interpreting the succinct digraph as a transition system and the explicit digraph as the system's state space, we study compositional approaches that compute upper bounds on topological properties, while alleviating the complexity of the state-space explosion.
First, (potentially exponentially) smaller state spaces of abstractions of the system are constructed.
Second, the values of topological properties of those abstractions are computed.
Third, those values are composed to bound the given system's value.
}


\Omit{
Our results include new compositional bounding algorithms based on existing and new abstraction techniques, as well as theoretical results on the possibility of compositional bounding for different topological properties, and the optimality of our compositional algorithms.
We also provide implementations of our algorithms which we apply to standard AI~planning and verification problems, showing how the computed bounds can significantly improve the state-of-the-art for planning and verification problems.
}

\end{abstract}

\input{diameterUpperBoundingintro}
\input{diameterUpperBoundinglitrev}
\input{diameterUpperBoundingResultsSummary}
\input{diameterUpperBoundingdefinitions}
\input{projCompositionBounding}
\input{traversalDiamProps}
\input{acycDepGraphBounding}
\input{sublistDiamProps}
\input{acycSspaceBounding}
\input{hybridBounding}
\input{hybridBoundingExperiments}
\input{diameterUpperBoundingconclusion}

%% \paragraph{HOL4 Notation and Availability}
%% All statements appearing with a turnstile ($\vdash$) are HOL4 theorems, automatically pretty-printed to \LaTeX{}.
%% All our HOL scripts, experimental code and data are available from \url{https://MohammadAbdulaziz@bitbucket.org/MohammadAbdulaziz/planning.git}.
\bibliographystyle{plain}
\bibliography{paper}
%\input{formalProofs}

\end{document}
