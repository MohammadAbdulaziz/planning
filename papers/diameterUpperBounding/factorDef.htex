\begin{mydef}[Factoring]
\label{def:factor}
Factored representation $\delta_1$ is a factoring of  representation $\delta_2$ iff all the following is true:
\begin{enumerate}
  \item $\dom(\delta_1) \subseteq \dom(\delta_2)$
  \item $\cardinality{\delta_1} \leq \cardinality{\delta_2}$
  \item $\forall  \act_2 \in \delta_2, \state \in \uniStates(\delta_2). \exists \act_1 \in \delta_1. $ $\exec{\state}{\act_1} = \exec{\state}{\act_2}$
  \item there is a function $f:\delta_1 \Rightarrow 2^{\delta_2}$, where
  \begin{enumerate}[label=(\alph*)]
    \item $\forall (p_1, e_1) \in \delta_1, (p_2, e_2) \in f(p_1,e_1). p_1 \subseteq p_2$,
    \item $\forall \act_1 \in \delta_1, \state \in \uniStates(\delta_2).\exists \act_2 \in f(\act_1). \exec{\state}{\act_1} = \exec{\state}{\act_2}$, and
    \item $\forall \act_1 \in \delta_1. f(\act_1) \neq \emptyset$
  \end{enumerate}
 \end{enumerate}
\end{mydef}
