#!/bin/bash

#This generates a dot file that can be drawn using dot,
 #the command is fdp -Tpng <(./generateprodTighttdAcycProofFig.sh) -o prodTighttdAcycProofFig.png

echo "strict digraph gr {"
echo "splines=true;"
echo "sep=\"+25,25\";"
echo "graph [fontsize=9 ];"
echo "node [style=filled, fillcolor=white, shape=rectangle,ranksep=0.2]"

# echo "{rank=same;"

# for X1 in 0 1 2 3 4
# do
#   for X2 in 0 1 2 3 4
#   do
#     if [ "$X1" = "0" -o "$X2" = "0" ] ; then
#       echo -n "X1${X1}X2${X2} "
#    fi
#   done
# done

# echo "}"


#echo "concentrate=true"

for X1 in 0 1 2
do
  for X2 in 0 1 2 3
  do
    for X3 in 0 1 2 
    do
      echo "X1${X1}X2${X2}X3${X3} [margin=0,height=0,width=0.03,fontsize=24,label=${X1}${X2}${X3}]"
    done
  done
done
for X1 in 0 1 2
do
  for X2 in 0 1 2 3
  do
    for X3 in 0 1 2 
    do
              if [ "$X3" = "0" ] ; then 
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X2${X2}X31 [type=s,weight=10];"
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X2${X2}X32 [type=s,weight=10];"
              else
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X2${X2}X30 [type=s,weight=10];"                
              fi
              if [ "$X2" = "0" ] ; then 
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X21X3${X3} [type=s,weight=10];"
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X22X3${X3} [type=s,weight=10];"
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X23X3${X3} [type=s,weight=10];"
              else
                echo "X1${X1}X2${X2}X3${X3} -> X1${X1}X20X3${X3} [type=s,weight=10];"
              fi
              if [ "$X1" = "0" ] ; then 
                echo "X1${X1}X2${X2}X3${X3} -> X11X2${X2}X3${X3} [type=s,weight=10];"
                echo "X1${X1}X2${X2}X3${X3} -> X12X2${X2}X3${X3} [type=s,weight=10];"
              else
                echo "X1${X1}X2${X2}X3${X3} -> X10X2${X2}X3${X3} [type=s,weight=10];"                
              fi
    done
  done
done

echo "}"
