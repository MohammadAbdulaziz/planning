\begin{figure}[t]
\centering
\begin{subfigure}[b]{0.2\textwidth}
\begin{tikzpicture}
\node (s0) at (0,0) [varnode] {\scriptsize $x_0^1$ } ;
\node (s1) at (1.25,0)   [varnode] {\scriptsize $x_1^1$ } ;
\node (s2) at (0,1.25)   [varnode] {\scriptsize $x_2^1$ } ;
\node (s3) at (0,-1.25)[varnode] {\scriptsize $x_3^1$ } ;
\node (s4) at (-1.25,0)  [varnode] {\scriptsize $x_4^1$ } ;
\draw [<->,ourarrow] (s0) -- (s1) ;
\draw [<->,ourarrow] (s0) -- (s2) ;
\draw [<->,ourarrow] (s0) -- (s3) ;
\draw [<->,ourarrow] (s0) -- (s4) ;
\end{tikzpicture}
\caption[stateDiag]{\label{fig:flower}}
\end{subfigure}
\begin{subfigure}[b]{0.2\textwidth}
\begin{tikzpicture}
\node (s0) at (0,0) [varnode] {\scriptsize $x_0^1$ } ;
\node (s1) at (1.25,0)   [varnode] {\scriptsize $x_1^1$ } ;
\node (s2) at (0,1.25)   [varnode] {\scriptsize $x_2^1$ } ;
\node (s3) at (0,-1.25)[varnode] {\scriptsize $x_3^1$ } ;
\node (s4) at (-1.25,0)  [varnode] {\scriptsize $x_4^1$ } ;
\draw [<->,ourarrow] (s0) -- (s1) ;
\draw [<->,ourarrow] (s0) -- (s2) ;
\draw [<->,ourarrow] (s0) -- (s3) ;
\draw [<->,ourarrow] (s0) -- (s4) ;
\end{tikzpicture}
\caption[stateDiag]{\label{fig:flower}}
\end{subfigure}
\begin{subfigure}[b]{0.5\textwidth}
\includegraphics[width=1\textwidth]{rdnDecompProofFig}
\caption[fig:tighteg3]{\label{fig:witnessFlowerUnion}}
\end{subfigure}

\caption{ \label{fig:rdnDecompProof} Referring to Example~\ref{eg:rdnDecompProof}: (a) is the largest connected component in $\graph(\protect\lotus^1)$. (b) shows the largest connected component in $\graph(\protect\lotus^1 \cup \protect\lotus^2)$. The states $\state_1$ and $\state_2$ are coloured blue and red respectively. The states traversed by $\as$ when executed at $\state_1$ are coloured in red.}
\end{figure}


\begin{myeg}
\label{eg:rdnDecompProof}
This example shows the previous construction for a function $f:\mathbb{N}^2\Rightarrow\mathbb{N}$, and the two numbers $n_1=2$ and $n_2=2$, where $f(n_1,n_2)=2$.
We construct two flowers, $\lotus^1$ and $\lotus^2$, each of which has $f(n_1,n_2) + 2$ petals, i.e. four petals.
Let $\state_0^1=\{\overline{u},\overline{v},\overline{w}\}$, $\state_1^1=\{\overline{u},\overline{v},w\}$, $\state_2^1=\{\overline{u},v,\overline{w}\}$, $\state_3^1=\{\overline{u},v,w\}$, and $\state_4^1=\{u,\overline{v},\overline{w}\}$.
The actions of the first flower are $\lotus^1 = \{(\state_0^1,\state_1^1), (\state_0^1,\state_2^1), (\state_0^1,\state_3^1),$ $(\state_0^1,\state_4^1), (\state_1^1,\state_0^1), (\state_2^1,\state_0^1),$ $(\state_3^1,\state_0^1), (\state_4^1,\state_0^1)\}$.
Accordingly, $\state_0^1$ is the pistil of $\lotus^1$, and $\{\state_1^1, \state_2^1, \state_3^1, \state_4^1\}$ are its petal tips.
The largest connected component of $\graph(\lotus^1)$ is shown in Figure~\ref{fig:flower}.
In this construction $\as_{i\mapsto j}^1=[(\state_i^1,\state_0^1), (\state_0^1,\state_j^1)]$.

Similarly, let $\state_0^2=\{\overline{x},\overline{y},\overline{z}\}$, $\state_1^2=\{\overline{x},\overline{y},z\}$, $\state_2^2=\{\overline{x},y,\overline{z}\}$, $\state_3^2=\{\overline{x},y,z\}$, and $\state_4^2=\{x,\overline{y},\overline{z}\}$, and $\lotus^2 = \{(\state_0^2,\state_1^2), (\state_0^2,\state_2^2), (\state_0^2,\state_3^2),$ $(\state_0^2,\state_4^2), (\state_1^2,\state_0^2), (\state_2^2,\state_0^2),$ $(\state_3^2,\state_0^2), (\state_4^2,\state_0^2)\}$.
Note that $\dom(\lotus^1)=\{u,v,w\}$ and $\dom(\lotus^2)=\{x,y,z\}$.

Consider the states $\state_1 = \state_1^1\uplus\state_1^2 = \{\overline{u},\overline{v},w, \overline{x},\overline{y},z\}$ and $\state_2 = \state_4^1\uplus\state_4^2 = \{u,\overline{v},\overline{w}, x,\overline{y},\overline{z}\}$.
The action sequence resulting from the above construction is $\as = [\as_{1\mapsto 2}^1;\as_{1\mapsto 2}^2; \as_{2\mapsto 3}^1; \as_{2\mapsto 3}^2; \as_{3\mapsto 4}^1; \as_{3\mapsto 4}^2]$ and its length is 12.
$\as$ will reach $\state_2$ if executed at $\state_1$, while traversing all distinct states.
The largest connected component of $\lotus^1 \cup \lotus^2$ and the path induced by execution of $\as$ from $\state_1$ are shown in Figure~\ref{fig:witnessFlowerUnion}.
\end{myeg}
