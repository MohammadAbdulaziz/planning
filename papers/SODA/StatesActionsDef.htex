\begin{mydef}[States and Actions]
\label{def:stateAction}
%A factoring of a digraph is defined in terms of {\em states} and {\em actions}:
A maplet, $\v \mapsto b$, maps a variable $\v$---i.e. a state-characterising proposition---to a Boolean $b$.
A state, $\state$, is a finite set of maplets.
We write $\dom(\state)$ to denote $\{\v \mid (\v \mapsto b) \in \state\}$, the domain of $\state$.
For states $\state_1$ and $\state_2$, the union, $\state_1 \uplus \state_2$, is defined as $\{\v \mapsto b \mid $ $\v \in \dom(\state_1) \cup \dom(\state_2) \wedge \ifnew\; \v \in \dom(\state_1) \;\thennew\; b = \state_1(\v) \;\elsenew\; b = \state_2(\v)\}$.
Note that the state $\state_1$ takes precedence.
%A set of states $\cal S$ is said to be a  state space over some set of variables $\vs$ iff $\forall s \in {\cal S}. \dom(s) = \vs$.
An action is a pair of states, $(p,e)$, where $p$ represents the \emph{preconditions} and $e$ represents the \emph{effects}.
For action $\act=(p,e)$, the domain of that action is $\dom(\act)\equiv\dom(p) \cup \dom(e)$.
\end{mydef}
