\begin{mydef}[Dependency]
\label{def:dep}
A variable \(\v_2\) is dependent on \(\v_1\) in \(\delta\) (written $\dep{\delta}{v_1}{v_2}$) iff one of the following statements holds:
\footnote{Our definition is equivalent to those in \cite{williams:nayak:97,knoblock:94} in the context of AI planning.}
\begin{enumerate*}
\item \(\v_1\) is the same as \(\v_2\),
\item there is $(p,e)\in \delta$ such that $\v_1 \in \dom(p)$ and $\v_2 \in \dom(e)$, or
\item there is a $(p,e)\in \delta$ such that both \(\v_1\) and \(\v_2\) are in $\dom(e)$.
\end{enumerate*}
A set of variables \(\vs_2\) is dependent on  \(\vs_1\) in \(\delta\) (written \dep{\delta}{\vs_1}{\vs_2}) iff:
\begin{enumerate*}
\item \(\vs_1\) and \(\vs_2\) are disjoint, and
\item there are $v_1\in\vs_1$ and $v_2\in\vs_2$, where \dep{\delta}{\v_1}{\v_2}.
\end{enumerate*}
\end{mydef}
