\subsection{Tight Upper Bounds}
\label{subsec:RemarksonTightness}
Before we describe our main contributions, we define our notion of a valid upper bound.
In the simplest case, a function $g$ is an upper bound on a function $f$ iff $\forall x. f(x) \leq g(x)$.
However, for an upper bound $g$ to be practically useful, it needs to take as input abstractions of inputs of $f$.
For example, some prior work on upper-bounding the diameter of graphs abstracts the graph to a pair of numbers: the degree of the graph and the maximum graph vertex degree.
%% For a function $f$, one reason for devising an upper bound $g$, is to have an ``easier'' to compute estimate of the value of $f$.
%% The lower comlexity of computing $g$ is because it takes ``abstractions'' of the objects in $\dom(f)$ as inputs, i.e. $g$ processes less information.
We formalise such abstractions as follows:
\begin{mydef}[Abstractions]
\label{def:validbound}
An abstraction is a relation $R:\alpha \times \beta  \Rightarrow \bool$ that maps every object of type $\alpha$ to its abstractions of type $\beta$, where every $x:\alpha$ has at least one abstraction $y:\beta$.
We say $y$ is an $R$ abstraction of $x$ iff $R(x,y)$ holds.
Also, we define for $R$ the function $R^*:\alpha \Rightarrow \beta$ that maps every object $x$ to one of its $R$ abstractions.
\end{mydef}
\noindent Given that notion of an abstraction, we define valid and tight upper bounds to be:%
\footnote{Definition~\ref{def:validbound} and Definition~\ref{def:tightbound} can be generalised to lower bounds, and to any lattice rather than $\mathbb{N}$.}
\begin{mydef}[Valid Upper Bound]
\label{def:validbound}
For functions $f:\alpha \Rightarrow \mathbb{N}$ and $g:\beta \Rightarrow \mathbb{N}$, and an abstraction $R:(\alpha\times \beta) \Rightarrow \bool$, $g$ is a valid upper bound on $f$ under $R$ iff for any $x:\alpha$ and $y:\beta$, $R(x, y)$ implies $f(x) \leq g(y)$.
%% \begin{alltt}
%% \HOLthm[def,>>,width=80]{tightness.valid_bound_def}
%% \end{alltt}
\end{mydef}
\begin{mydef}[Tight Upper Bound]
\label{def:tightbound}
For functions $f:\alpha \Rightarrow \mathbb{N}$ and $g:\beta \Rightarrow \mathbb{N}$, and an abstraction relation $R:(\alpha\times \beta) \Rightarrow \bool$, $g$ is a tight upper bound on $f$ under $R$ iff
\begin{enumerate*}
  \item $g$ is a valid upper bound on $f$ under $R$, and
  \item for every $y:\beta$, if $y$ is an $R$ abstraction of an $x:\alpha$, then there is an $x':\alpha$ for which $y$ is an $R$ abstraction and $f(x') = g(y)$.
\end{enumerate*}
%% \begin{alltt}
%% \HOL thm[def,>>]{tightness.tight_bound_def}
%% \end{alltt}
\end{mydef}
\begin{myprop}
\label{prop:tightsanity}
For functions $f,g$, and $h$ and the abstraction relation $R$, if $g$ is a tight upper bound on $f$ under $R$ and there is a $y$ that is an $R$ abstraction of some $x$, and $h(y)<g(y)$ then $h$ is not a valid upper bound on $f$ under $R$.
\end{myprop}
\noindent An important concept for manipulating bounds and abstractions is function-relation composition.
\begin{mydef}[Function-Relation Composition]
\label{def:funreldecomp}
For a function $f:\beta\Rightarrow \gamma$ and a relation $R:\alpha \times \beta \Rightarrow \bool$, $(f \bullet R): \alpha \times \gamma \Rightarrow \bool $ is the relation that is true for $x:\alpha$ and $z:\gamma$ iff there is a $y:\beta$, such that: 
\begin{enumerate*}
  \item $R(x,y)$ is true, and
  \item $f(y) = z$.
\end{enumerate*}
\end{mydef}
\begin{myprop}
\label{prop:valiff}
For functions $f$, $g$ and $h$, $g \circ h$ is a valid upper bound on $f$ under $R$ iff $g$ is a valid upper bound on $f$ under $h \bullet R$.
%% \begin{alltt}
%% \HOL thm[>>]{tightness.valid_compose}
%% \end{alltt}
\end{myprop}


\subsubsection{Remarks on Tightness}

Our formalisation of tightness models tightness of an upper bound $g$ on a function $f$, as a predicate relative to an abstraction relation.
Another way to look at that is by seeing it as a predicate relative to the amount of information passed to $g$, where that amount depends on how much information does the abstraction relation add or remove from the objects in the domain of $f$.

Seeing it that way, suppose that there are two versions of the same function, $g_1$ and $g_2$, where $g_1$ takes more information than $g_2$ (we formalise the mechanics of that later in this section).
Our notion of tightness formalises the following intuitions as theorems:
\begin{enumerate}
  \item If $g_1$ is a tight upper bound on some $f$ then $g_2$ is also a tight upper bound on $f$, but not necessarily the other way around.
  %% \item If two functions $g$ and $h$ are both tight upper bounds on a third function, if $g$ takes more information than $h$, then the time/space complexity of $h$ is upper-bounded by the time/space complexity of $g$.
  \item If $g_1$ is a tight upper bound on $f$ then $g_2$ is a tight upper bound on $g_1$, or in other words the bound is more ``accurate'' given more information.
  \item If $g_1$ is a tight upper bound on $f$ and $g_1$ takes the same or more information than $f$, then the time/space complexity of $f$ is upper-bounded by the time/space complexity of $g_1$.
\end{enumerate}


%% An example of the first intuition is the functions $\NalgoName$ and $\NalgoName_\mathbb{N}$.
%% Both of those functions are essentially the same expression except that $\NalgoName$ takes more information than $\Nalgo_\mathbb{N}$ (the abstraction $DH_{\delta \times \graph_\vstype}$ adds $\graph_\vstype$ to $\delta$).
We consider the cardinality of the domain of a function as the amount of information that this function takes.
Accordingly, we formalise the notion of two versions of a function, $g_1$ and $g_2$, with $g_1$ taking more information than $g_2$ as the existence of a decomposition of $g_1 = g_2 \circ g_3$, where $g_3$ is a surjective function.
Based on that, we formalise the first intuition in the following proposition:
\begin{myprop}
\label{prop:tightcomp1}
For a function $g$, and a surjective function $h$, if $g \circ h$ is a tight upper bound on $f$ under $R$, then $g$ is a tight upper bound on $f$ under $h \bullet R$.
%% \begin{alltt}
%% \HOL thm[>>]{tightness.tight_compose_1}
%% \end{alltt}
\end{myprop}
\noindent An analogous proposition, that deals with abstractions passing less or more information follows:
%% \begin{mythm}
%% \label{thm:tightleq1}
%% For a function $g$, and a surjective function $h$, if $g \circ h$ is a tight upper bound on $f$ under $R$, then for any $x$ and $y$: $(g \circ h)(y) \leq g(x)$.
%% \end{mythm}
\begin{myprop}
\label{prop:tightcomp2}
For a function $g$, and an injective function $h$, if $g$ is a tight upper bound on $f$ under $h \bullet R$ then $g \circ h$ is a tight upper bound on $f$ under $R$.
%% \begin{alltt}
%% \HOL thm[>>]{tightness.tight_compose_2}
%% \end{alltt}
\end{myprop}
\noindent The last two propositions suggest the following approach for investigating tightness of an upper bound $g$ on a function $f$ under an abstraction $R$: 
\begin{itemize}
 \item if $g$ can be decomposed into $g =$ $g_1 \circ g_2$, with $g_2$ a surjection, one may first try to prove the necessary condition: $g_1$ is a tight upper bound on $f$ under $g_2 \bullet R$, which should be ``easier'' than proving that $g$ is tight under $R$, given the more abstract (due to smaller cardinality) domain of $g_1$.

\item Similarly, if $R$ can be decomposed into $h \bullet R_1$, with $h$ an injection, one may first try to prove the necessary condition: $g \circ h$ is a tight upper bound on $f$ under $R_1$.
 %% \item For functions $g_1$, $g_2$, if for any function $g_3$ and injective function $g_4$, where $g_1 = g_3 \circ g_4$: $g_4 \circ g_2$ is not tight under $g_3 \bullet R$, then in some sense $g_2$ is ``a tightest possible bound'' on $f$ under $g_1 \bullet R$.
\end{itemize}

To formalise the second intuition, we firstly define the following notion of a function abstraction.
\begin{mydef}[Function Abstraction Relation]
\label{def:injrel}
For a function $h:\alpha \Rightarrow \beta$, we define $h^+:\alpha \times \beta \Rightarrow \bool$ to be the relation that is true for an $x:\alpha$ and $y:\beta$ iff $h(x) = y$.
Similarly, we define $h^-:\beta \times \alpha \Rightarrow \bool$ to be the relation that is true for an $x:\beta$ and $y:\alpha$ iff $h(y) = x$.
\end{mydef}
\begin{myprop}
\label{prop:tightcomp1cor}
For a function $g$, and a surjective function $h$, if $g \circ h$ is a tight upper bound on $f$ under $R$, then $g$ is a tight upper bound on $g \circ h$ under $h^+$.
\end{myprop}
\begin{myprop}
\label{prop:tightcomp2cor}
For a function $g$, and an injective function $h$, if $g$ is a tight upper bound on $f$ under $h \bullet R$ then $g \circ h$ is a tight upper bound on $g$ under $h^-$.
\end{myprop}
\noindent The last two propositions show that the more information an upper bound takes, the more accurate it is.

To formalise the third intuition we define the notion of an abstraction relation that adds information.
\begin{mydef}[Injective Relation]
\label{def:injrel}
An abstraction relation $R:\alpha \times \beta \Rightarrow \bool$ is injective iff for every $x:\alpha$, all $R$ abstractions of $x$ are not $R$ abstractions for any $x' \neq x$ .
\end{mydef}

\begin{myprop}
\label{cor:moreinfotight2}
For the injective abstraction relation $R$, and the functions $f$, $g$, if $g$ is a tight upper bound on $f$ under $R$, then
$f = g \circ R^*$.
\end{myprop}

\noindent The last proposition shows that one way of disproving tightness for a bound that takes more information than what it bounds can be through time/memory complexity arguments.
Also, if upper bounds on a function are devised for easy estimation of that function, the last proposition shows that an upper bound that takes more or equal information to the function it bounds is not useful.
