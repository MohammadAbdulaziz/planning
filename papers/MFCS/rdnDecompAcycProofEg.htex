\begin{myeg}
\label{eg:rdnDecompAcycProof}
This example shows the previous construction for a function $f:\mathbb{N}$-graph$\Rightarrow\mathbb{N}$, and the graph $\graph_\mathbb{N}$ shown in Figure~\ref{fig:graphN}, where $f(\graph_\mathbb{N})=2$.
In this graph there are three vertices $\vertexa$ (the root), $\vertexb$, and $\vertexc$, labelled by the numbers $2,3,$ and $2$, respectively.
We construct three flowers, one per vertex.
For $\vertexb$ the constructed flower is $\lotus_{4,3}^\vertexb = \{(\state_0^\vertexb, \state_4^\vertexb),(\state_4^\vertexb, \state_5^\vertexb), (\state_5^\vertexb, \state_0^\vertexb)\} \cup \bigcup\{\{(\state_0^\vertexb, \state_l^\vertexb),(\state_l^\vertexb, \state_0^\vertexb)\} \mid 1 \leq l \leq 3\}$
where the states are defined as follows $\state_0^\vertexb = \{\overline{l},\overline{m},\overline{n}\}$ (the pistil), $\state_1^\vertexb = \{\overline{l}, \overline{m},n\}$ (the first petal tip), $\state_2^\vertexb = \{\overline{l},m,\overline{n}\}$ (the second petal tip), $\state_3^\vertexb = \{\overline{l},m,n\}$ (the third petal tip), $\state_4^\vertexb = \{l,\overline{m},\overline{n}\}$, $\state_5^\vertexb = \{l,\overline{m},n\}$ (the fourth petal tip).
Note that since for the flower $\lotus_{4,3}^\vertexb$, $m=3$ (i.e. odd), the last petal has one more state in it (the petal at the bottom of Figure~\ref{fig:flower1rdnDecompAcyc}).
For $\vertexc$ the constructed flower is $\lotus_{4,2}^\vertexc = \bigcup\{\{(\state_0^\vertexc, \state_l^\vertexc), (\state_l^\vertexc, \state_0^\vertexc)\} \mid 1 \leq l\leq 4\}$,
where the states are defined as follows $\state_0^\vertexc = \{\overline{o},\overline{q}, \overline{r}\}$ (the pistil), $\state_1^\vertexc = \{\overline{o},\overline{q}, r\}$ (the first petal tip), $\state_2^\vertexc = \{\overline{o}, q, \overline{r}\}$ (the second petal tip), $\state_3^\vertexc = \{\overline{o},q , r\}$ (the third petal tip), and $\state_4^\vertexc = \{o,\overline{q}, \overline{r}\}$ (the fourth petal tip).
For $\vertexa$ the constructed flower is $\lotus_{4,2}^\vertexa = \{\{(\state_0^\vertexa, \state_1^\vertexa), (\state_l^\vertexa, \state_0^\vertexa)\} \mid 1 \leq 4\}$,
where the states are defined as follows $\state_0^\vertexa = \{\overline{a},\overline{b}, \overline{c}\}$ (the pistil), $\state_1^\vertexa = \{\overline{a},\overline{b}, c\}$ (the first petal tip), $\state_2^\vertexa = \{\overline{a}, b, \overline{c}\}$ (the second petal tip), $\state_3^\vertexa = \{\overline{a},b , c\}$ (the third petal tip), and $\state_4^\vertexa = \{a,\overline{b}, \overline{c}\}$ (the fourth petal tip).

The required witness $\delta = \lotus_{4,2}^\vertexa \cup \lotus_{4,3}^\vertexb \cup \lotus_{4,2}^\vertexc \cup \{(\state_0^\vertexa\uplus\state_0^\vertexb,\state_1^\vertexb),(\state_0^\vertexa\uplus\state_0^\vertexc,\state_1^\vertexc)\}$
where the actions $\{(\state_0^\vertexa\uplus\state_0^\vertexb,\state_1^\vertexb),(\state_0^\vertexa\uplus\state_0^\vertexc,\state_1^\vertexc)\}$ add to $\delta$ dependencies equivalent to the edges of $\graph_\mathbb{N}$, i.e. the dependencies shown in Figure~\ref{fig:graphvstyperdnDecompAcyc}.

Consider the states ${\state_1 = \state_1^\vertexa\uplus\state_1^\vertexb\uplus\state_1^\vertexc=\{\overline{a},\overline{b},c,\overline{l},\overline{m}, n,\overline{o},}$ $\overline{q},r\}$ and $\state_2 = \state_4^\vertexa\uplus\state_4^\vertexb\uplus\state_4^\vertexc = \{a,\overline{b}, \overline{c}, l, \overline{m},n, o, \overline{q},\overline{r}\}$.
Following Algorithm~\ref{alg:rdnDecompAcyc}, the resulting action sequence is $\as = \as_{1\mapsto 2}^\vertexa\cat\as_{1\mapsto 2}^\vertexb\cat$ $\as_{1\mapsto 2}^\vertexc\cat\as_{2\mapsto 3}^\vertexa\cat \as_{2\mapsto 3}^\vertexb\cat \as_{2\mapsto 3}^\vertexc\cat \as_{3\mapsto 4}^\vertexa\cat \as_{3\mapsto 4}^\vertexb\cat\as_{3\mapsto 4}^\vertexc$ and its length is 18.
$\as$ will reach $\state_2$ if executed at $\state_1$, while traversing all distinct states.
The largest connected component of $\graph(\delta)$ and the path traversed by executing $\as$ from $\state_1$ are shown in Figure~\ref{fig:rdnDecompAcyc}.
\end{myeg}
