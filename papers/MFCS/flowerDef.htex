The proof of this theorem is similar in structure to that of Theorem~\ref{thm:dnDecompAcyc}.
One difference is that instead of constructing paths for projections, we construct systems whose state spaces resemble ``flowers'' (Figure~\ref{fig:flower}).
A flower can have arbitrarily many petal tips without changing its recurrence diameter, which is crucial to construct arbitrarily long paths with all distinct states in the final construction, while keeping the recurrence diameters of projections constant.
Flowers are formally described as follows.
First let a petal $\petal_{l,m}^i$ be a loop of $\frac{m}{2}+1$ states labelled with $0$ and $\frac{m}{2}$ other numbers.
We formally define it as $\petal_{l,m}^i = \{(\state_{(l-2)c + k}^i, \state_{(l-2)c + (k + 1\bmod a)}^i) \mid 0 \leq k \leq a - 1 \}$, where $a = \lceil \frac{m}{2}\rceil + 1$ if $m$ is odd, and $a = \frac{m}{2} + 1$ otherwise, and $c = \lfloor \frac{m}{2}\rfloor + 1$.
A flower system, $\lotus_{l,m}^i$, denotes $\bigcup_{1 \leq j < k \leq l} \{(\state_{j}^i, \state_{k}^i), (\state_{k}^i, \state_{j}^i)\}$ (i.e. a clique) if $m=1$, and $\petal_{l,m}^i \cup \bigcup_{1 \leq j \leq l-2} \{(\state_{jc + k}^i, \state_{jc + (k + 1\bmod c)}^i) \mid 0 \leq k \leq c - 1 \}$ otherwise.
$\lotus_{l,m}^i$ is a factored system whose state space contains $l$ cycles (the petals).
All other petals are of length $\lfloor \frac{m}{2}\rfloor + 1$, except for the last one, if $m$ is odd its length is $\lceil \frac{m}{2}\rceil + 1$ otherwise its length is $\frac{m}{2}+1$.
\emph{All} of the petals have exactly \emph{one} state in common between them (the pistil), which is $\state_0^i$.
In each petal $j$ there is a state $t_j^i$ (the ``petal tip'') defined as $t_j^i = \state_{(l-2)c + \lceil \frac{a}{2} \rceil}^i$ for the last petal (i.e. $j = l-1$), and $t_j^i=\state_{jc + \lceil \frac{c}{2} \rceil }^i$, otherwise.
$\tip_0^i$ can be reached by exactly $\lceil \frac{a}{2} \rceil$ actions from $\state_0^i$, and $\state_0^i$ can be reached from $\tip_0^i$ by exactly $\lfloor \frac{a}{2} \rfloor$ actions.
Similarly, for $j\neq 1$, $\tip_j^i$ can be reached by exactly $\lceil \frac{c}{2} \rceil$ actions from $\state_0^i$, and $\state_0^i$ can be reached from $\tip_j^i$ by exactly $\lfloor \frac{c}{2} \rfloor$ actions.
Denoting by $\as_{a \mapsto b}^i$ the shortest action sequence that joins the petal tip $\tip_a^i$ with $\tip_b^i$, $c \leq \cardinality{\as_{a \mapsto b}^i}$ holds for $a \neq b$.
Also $\recurrenceDiam(\lotus_{l,m}^i) = m$ holds.
