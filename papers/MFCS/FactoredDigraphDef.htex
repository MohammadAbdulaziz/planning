\begin{mydef}[Factored System]
For a set of actions $\delta$ %
we write $\dom(\delta)$ to denote $\bigcup \{\dom(\act) \mid \act \in \delta\}$, the domain of $\delta$.
The set of valid states, written $\uniStates(\delta)$,  is $\{\state \mid \dom(\state) = \dom (\delta)\}$.
The set of valid action sequences, $\ases{\delta}$, is $\{\as \mid\; \listset(\as) \subseteq \delta\}$, where $\listset(l)$ s the set of members in list $l$.

$\delta$ is the factored representation of the digraph $\graph(\delta) \equiv \langle V,E,\labelfun \rangle$, where $V =\uniStates(\delta)$, $\labelfun$ is the identity function, $E \equiv \{(\state, \exec{\state}{\act})\mid \state \in \uniStates(\delta), \act \in \delta\}$.
We refer to $\graph(\delta)$ as the state space of $\delta$.
%% For states $\state$ and $\state'$, $\reachable{\state}{}{\state'}$ denotes that there is a $\as \in \ases{\delta}$ such that $\exec{\state}{\as} = \state'$.
%% Let the connected component for a state $\statea$ be $\probss{\delta,\statea} = \{\stateb \mid \exists \statec. (\reachable{\statea}{}{\statec} \wedge \reachable{\statec}{}{\stateb})\vee (\reachable{\stateb}{}{\statec} \wedge \reachable{\statec}{}{\statea}) \}$.
%% We denote the states in the largest connected component in $\graph(\delta)$ as $\probss{\delta} = \probss{\delta,\state_\max}$, where $\state_\max = \underset{x\in\uniStates(\delta)}{\argmax}\;\; \cardinality{\probss{\delta,\state}}$.
\end{mydef}
