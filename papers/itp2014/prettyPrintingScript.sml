open HolKernel Parse boolLib bossLib;

open GraphPlanTheoremTheory
open SCCTheory
open SCCGraphPlanTheoremTheory
val _ = new_theory "prettyPrinting";

val _ = overload_on ("LL'", ``problem_plan_bound``)
val _ = overload_on ("LL", ``problem_plan_bound_charles``)

(* parenthesised L *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "LL'",
                  pp_elements = [TOK "(LL'_L)", TM, TOK "(LL'_R)"]}
(* parenthesised L_\bot *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "LL",
                  pp_elements = [TOK "(LL_L)", TM, TOK "(LL_R)"]}

val planning_problem_def' = store_thm(
  "planning_problem_def'",
  ``planning_problem PROB <=>
      (!p e. (p,e) IN PROB.A ==> FDOM p SUBSET FDOM PROB.I /\
                            FDOM e SUBSET FDOM PROB.I) /\
      (FDOM PROB.G = FDOM PROB.I)``,
  SRW_TAC [][FM_planTheory.planning_problem_def,
             pairTheory.FORALL_PROD] THEN METIS_TAC[]);

fun stitch'' f s =
    save_thm(s, f (SIMP_RULE (srw_ss()) [replace_projected_stitch]
                          (DB.fetch "GraphPlanTheorem" s)))
val stitch' = stitch'' (fn x => x)

val _ = stitch''
          (CONV_RULE (STRIP_QUANT_CONV (RAND_CONV (REWR_CONV EQ_SYM_EQ))))
          "len_replace_proj_works"
val _ = stitch' "replace_proj_exec_works"
val _ = stitch''
          (CONV_RULE (STRIP_QUANT_CONV (RAND_CONV (REWR_CONV EQ_SYM_EQ))))
          "replace_proj_exec_works_diff"
val _ = stitch' "sublist_proj_imp_replace_proj_sat_precond"
val _ = stitch' "sublist_imp_replace_proj_works"

val sublist_t = rator (``sublist (foo: ('a state # 'a state) list)``)
val abs_to_setcomp = prove(
  ``(λa:'a state # 'a state. FDOM (SND a) <> {}) =
    { a | FDOM (SND a) <> {} }``,
  SIMP_TAC (srw_ss()) [pred_setTheory.EXTENSION])

val bound_child_parent_lemma_1_1 = save_thm(
  "bound_child_parent_lemma_1_1",
  bound_child_parent_lemma_1_1
    |> CONV_RULE
        (STRIP_QUANT_CONV
           (RAND_CONV
              (STRIP_QUANT_CONV
                 (markerLib.move_conj_left
                    (free_in sublist_t)))))
    |> SIMP_RULE(srw_ss()) [abs_to_setcomp])

val bound_child_parent_lemma = save_thm(
  "bound_child_parent_lemma",
  GraphPlanTheoremTheory.bound_child_parent_lemma
    |> SIMP_RULE(srw_ss()) [abs_to_setcomp])

val bound_child_parent_lemma_1 = save_thm(
  "bound_child_parent_lemma_1",
  bound_child_parent_lemma_1 |> SIMP_RULE(srw_ss()) [abs_to_setcomp])


val _ = remove_ovl_mapping "Π" {Thy = "pred_set", Name = "PROD_IMAGE"}
val _ = remove_ovl_mapping "S" {Thy = "combin", Name = "S"}
val _ = overload_on("II", ``Π.I``)
val _ = overload_on("AA", ``Π.A``)
val _ = overload_on("GG", ``Π.G``)
val _ = overload_on("DD", ``FDOM Π.I``)

val _ = overload_on("AAA", ``λs. ancestors(Π,s)``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "AAA",
                  pp_elements = [TOK "(ANC_L)", TM, TOK "(ANC_R)"]}

val _ = overload_on ("'", ``IMAGE``)
val _ = set_fixity "'" (Infixl 2000);

val dep_var_set_def = store_thm(
  "dep_var_set_def",
  ``dep_var_set (Π, cs, ps) <=>
     DISJOINT cs ps /\
     ∃c p. c ∈ cs ∧ p ∈ ps ∧ dep (Π,c,p)``,
  SRW_TAC[][dep_var_set_def] THEN METIS_TAC[]);

(* ----------------------------------------------------------------------
    domain restriction syntax
   ---------------------------------------------------------------------- *)

val prob_proj_def' = store_thm(
  "prob_proj_def'",
  ``prob_proj (PROB, vs) =
     PROB with <| A := { action_proj(a,vs) | a IN PROB.A };
                  I := DRESTRICT PROB.I vs;
                  G := DRESTRICT PROB.G vs |>``,
  SRW_TAC [][prob_proj_def, FM_planTheory.problem_component_equality,
             action_proj_def, pred_setTheory.EXTENSION]);



val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Suffix 2100,
                  term_name = "domrestrict",
                  pp_elements = [TOK "(DOMR_L)", TM, TOK "(DOMR_R)"]}

(* for basic finite maps *)
val _ = overload_on("domrestrict", ``DRESTRICT``)

(* for actions *)
val _ = overload_on("domrestrict", ``\a vs. action_proj (a, vs)``)
(* for action lists *)
val _ = overload_on("domrestrict", ``\as vs. as_proj(as, vs)``)
(* for problems *)
val _ = overload_on("domrestrict", ``\p vs. prob_proj (p, vs)``)

val _ = overload_on("DVS", ``FDOM Π.I DIFF vs``)


val _ = set_mapped_fixity { term_name = "sublist",
                            fixity = Infix(NONASSOC, 450),
                            tok = "<=<" }

val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "LENGTH",
                  pp_elements = [TOK "BarLeft|", TM, TOK "|BarRight"]}

val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "CARD",
                  pp_elements = [TOK "BarLeft|", TM, TOK "|BarRight"]}

val _ = remove_termtok { tok = "=", term_name = "="}
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Infix(NONASSOC, 450),
                  term_name = "=",
                  pp_elements = [HardSpace 1, TOK "=", BreakSpace(1,2)]}

val _ = remove_termtok { tok = "==>", term_name = "⇒"}
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Infixr 200,
                  term_name = "==>",
                  pp_elements = [HardSpace 1, TOK "⇒", BreakSpace(1,2)]}

val _ = remove_termtok { tok = "⇔", term_name = "<=>"}
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Infix(NONASSOC, 100),
                  term_name = "<=>",
                  pp_elements = [HardSpace 1, TOK "⇔", BreakSpace(1,2)]}

val _ = export_theory();
