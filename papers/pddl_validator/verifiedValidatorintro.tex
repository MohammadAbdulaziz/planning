%\clearpage 
\section{Introduction}
\label{sec:intro}



%% Three new ideas should be inserted:

%% 1) formalising semantics in Isabelle is more readable than in VAL or INVAL


%% 2) Isabelle allows for sanity checking theorems that confirm basic expected
%% properties of the formalised semantics, like soundness theorem

%% 3) It allows for the utilistation of existing theorem proving technology to prove properties about PDDL benchmarks as well as invariants, and ii) to obtain very fast verified implementations from semantic specs.

%% 4) Additionally in details, map at least one formal def to the pen-and-paper one

%% 5) Fuzz existing plans to induce mistakes

Since their earliest days, AI planning systems have had a lot of interaction with theorem provers.
First-Order Logic (FOL) theorem provers were used in~\cite{fikes1971strips,green1969application}, and, more recently, 
propositional satisfiability solvers in~\cite{kautz:selman:92,rintanen:12}.
However, most of that work was focused on using theorem provers to solve planning problems, i.e.\ to produce plans.

In this work we argue that theorem proving technology can excel in filling needs for the planning community, other than producing plans. 
% namely the formal verification of properties of planning problems and their solutions.
One obvious such need is validating whether a plan actually solves a planning problem. 
Since in many cases large transition systems underlie planning problems, and (candidate) plans can be substantially long, manual validation is infeasible.
Furthermore, the need for validation is exacerbated if the application in which those plans are used is safety-critical.
This motivates the need for automatic \emph{plan validators} that verify whether a sequence of actions indeed achieves the goal of a planning problem.
%Additionally in some applications, referred to as \fquot{mixed-initiative systems} in \cite{howey2004val}, humans produce the plans and the main task for the machine is to verify those plans.

This issue is addressed by validators for different planning formalisms, most notably VAL \cite{howey2004val}, which validates plans for problems specified in PDDL2.1~\cite{fox2003pddl2}. 
It was later extended \cite{fox2005validating} to cover processes and events.
However, VAL's C++ implementation uses many performance oriented optimisations that sometimes obfuscate the relation between the semantics of PDDL and the actual implementation, which might cause the implemented semantics to not be equivalent to the intended semantics.
One attempt to remedy this is Patrik Haslum's INVAL\footnote{https://github.com/patrikhaslum/INVAL} validator for PDDL.
It is written in Common Lisp, with the intent of closely resembling the semantics of PDDL~\cite{fox2003pddl2,thiebaux2005defense}.
This, however, comes at the cost of performance: in our experiments INVAL is substantially slower than VAL.
Moreover, although Common Lisp may be more concise than C++ in describing the semantics of PDDL, it is still a programming language.
Thus, it requires the semantics to be specified as executable programs instead of the mathematically more concise and abstract specifications which are used in pen-and-paper specification.
Again, this has the potential of introducing differences between the implemented and the intended semantics.

% 
% Previously, authors addressed the issue by developing automatic validators for different planning formalisms.
% 
% 
% 
% 
% Previously, authors addressed the issue by developing automatic validators for different planning formalisms.
% Most notably is the work in \cite{howey2004val} where the authors provided the validator VAL for planning problems specified in PDDL2.1~\cite{fox2003pddl2}.
% This was extended in \cite{fox2005validating} to cover planning problems that have processes and events triggered by those processes.
% An issue with their approach is that the implementation uses many performance oriented optimisations that sometimes obfuscate the relation between the semantics of PDDL and the actual code that does the plan validation.
% This can jeopardise the purpose of the validator: which is to guarantee the validity of plans w.r.t. a specification of PDDL.
% One trial to remedy this is by Patrik Haslum who implemented the validator INVAL in Common Lisp.%
% \footnote{https://github.com/patrikhaslum/INVAL}.
% INVAL's main feature is that: since its implementation is in Lisp which is a high-level language, then that implementation is claimed to closely resemble the semantics of PDDL described in, \eg, \cite{fox2003pddl2} and \cite{thiebaux2005defense}.
% This, nonetheless comes at the cost of performance: in our experiments INVAL is substantially slower than VAL.
% However, there is a fundamental problem with both approaches: even if the semantics were written (relatively) clearly in a high-level language like Lisp, one still trusts that those semantics are equivalent to the pen-and-paper semantics that are proved sound e.g.\ \cite{lifschitz1987semantics,pednault1989adl}, which is usually done by visual inspection.

Indeed, there are bugs in both VAL and INVAL.
There are domains, instances and buggy plans that are mistakenly accepted as correct plans by VAL.
Furthermore, there are domains, instances and plans that are all correct, but trigger non-termination for INVAL and segmentation faults for VAL, thus rendering them practically incomplete.%
\footnote{We describe the bugs we found in more detail in the experiments section.}


In this work we argue that theorem provers and formal methods technology are an excellent fit to alleviate: \begin{enumerate*} \item having to choose between clear semantics and efficient implementations, and \item having to trust the equivalence of the implemented semantics and the pen-and-paper semantics.\end{enumerate*}
%, and actually having both and establishing their equivalence using formal methods technology.
Languages used by \emph{theorem provers}, like Higher-Order Logic (HOL) \cite{HOLsystem,paulson1994isabelle} and Dependent Type Theory \cite{coquand1988calculus,norell2007towards}, allow for the use of standard mathematical constructs that are not necessarily executable, like set comprehensions.
Thus, they enable a formal specification of the semantics that is far more concise and elegant than what is possible in in any programming language (see Table~\ref{table:LOCs}).
This reduces the chance of having bugs in the specified semantics.
More importantly, theorem provers allow for proving that sanity checking properties hold for the specified semantics.
It also allows mechanising soundness theorems associated with pen-and-paper semantics.
Having those properties mechanised is substantially more reliable than mere visual inspection, and thus gives a much stronger indication on the equivalence between the formally specified and the pen-and-paper semantics.
%Additionally, mechanising soundness theorems in a theorem prover can increase the confidence in the original pen-and-paper results and may help uncovering mistakes or ambiguities in them.


\begin{table}
\begin{center}
    \begin{tabularx}{0.45\textwidth}{| l | X | X | X |}
    %p{5cm} |}
    \hline
          {\scriptsize Semantic Definition / Size}       &{\scriptsize VAL}       & {\scriptsize INVAL}       & {\scriptsize Isabelle/HOL} \\ 
          {\scriptsize Apply Action}         &{\scriptsize 75}        & {\scriptsize 23}          & {\scriptsize 3} \\
          {\scriptsize Action Enabled in State}       &{\scriptsize 67}       & {\scriptsize 69}      & {\scriptsize 15} \\ 
          {\scriptsize Well-Formed Action Instance}       &{\scriptsize 87}       & {\scriptsize 77}      & {\scriptsize 25} \\ 
          {\scriptsize Plan is Valid}       &{\scriptsize 224}       & {\scriptsize 163}      & {\scriptsize 50}\\         \hline
    \end{tabularx}
    \caption{\label{table:LOCs} Sizes in lines of code(LOC) of the specification of different definitions.
This table is only indicative, since different validators support different features.
However, whenever we could, we only count LOC contributing to the STRIPS fragment in VAL and INVAL.
E.g. in INVAL, we count no lines that apply relative or assign effects.}
\end{center}
\end{table}


Another advantage of using theorem provers for building validators is that the implementation of the validator needs not be the same as the specified semantics.
Indeed, an optimised implementation of the validator with low level performance oriented trickery can be used.
The equivalence between the implementation and the specification is formally proved within the theorem prover.
% can be formally confirmed using, for instance, the theorem prover in which the semantics were specified.
This allows for the runtimes of our validator to compete with VAL, and be much faster than INVAL, despite our semantics specification in HOL being substantially more concise than that of INVAL, let alone VAL.

% Moreover, due to recent advances in theorem proving technology e.g. \cite{paulson2010three,paulson2010three,zhan2016auto2}, formalising the semantics of a planning paradigm in a theorem prover opens the door for applications that are far beyond plan validation.
% In principle one can use that to formally (dis)prove arbitrary properties about planning domains and problems, e.g. that a plan does not exist or that two domains are equivalent, for some notion of equivalence.
% In this work we use our formalised semantics to build a framework that can be used to (almost) automatically verify that invariants hold in a planning domain.

{\bf Contributions}: \begin{enumerate*} \item (Section~\ref{sec:STRIPS}) In \emph{Isabelle/HOL}, we formalise the semantics of a propositional STRIPS like formalism for ground actions, but we add to it equality, negation, disjunctions and implication in the preconditions.
We roughly follow the semantics of STRIPS by Lifschitz~\cite{lifschitz1987semantics}, adapting it to the extra features that we support and removing from it practically irrelevant features like non-atomic effects.
\item (Section~\ref{sec:PDDL}) We define the semantics of a fragment of PDDL (i.e. the notion of action schemata and typing) on top of our formalisation of ground actions.
Our fragment includes the PDDL flags :strips, :typing, :negative-preconditions, :disjunctive-preconditions, :equality, :constants, and :action-costs.
\item (Section~\ref{sec:SOUND}) We prove theorems about the formalised semantics that serve as sanity checks.
% a prominent advantage of formalising semantics in a theorem prover, namely, proving theorems about the formalised semantics that serve as a sanity check for them.
% We do so by proving a soundness theorem that resembles the soundness theorem for STRIPS in~\cite{lifschitz1987semantics}, with the necessary adaptations to accommodate negations, disjunctions and equality.
%the first time to our knowledge, formally show how to extend the soundness results from~\cite{lifschitz1987semantics} to action schemata, types, and preconditions with disjuntions, negations, and implications.
\item (Section~\ref{sec:PDDLValidation}) We specify an optimised validator in Isabelle/HOL, and prove it correct w.r.t. our formalised semantics.
Using Isabelle's code generator~\cite{haftmann2007code,lochbihler2013light}, we extract an executable validator as Standard ML~\cite{milner1997definition} program.
It is much faster than INVAL and competitive with VAL.
To the best of our knowledge, this is the first formally verified plan validator.
% 
% 
% We implement an optimised validator in Standard ML~\cite{milner1997definition}.
% Its correctness is formally verified w.r.t. our formalised semantics using Isabelle/HOL and its code generation facilities~\cite{haftmann2007code,lochbihler2013light}.
% The validator checks the well-formedness of problems and the validity of plans.
% It is much faster than INVAL and is competitive with VAL.
% To the best of our knowledge, this is the first formally verified plan validator.
% \item (Section~\ref{sec:invariants}) We use the semantics formalised in Isabelle/HOL to construct a framework to formally (dis)prove that a domain satisfies invariant(s).
\end{enumerate*}
The Isabelle sources of our development can be downloaded at: \url{http://home.in.tum.de/~mansour/verval.html}.

\subsection{Isabelle/HOL}
Isabelle/HOL~\cite{paulson1994isabelle} is a theorem prover based on Higher-Order Logic. 
Roughly speaking, Higher-Order Logic can be seen as a combination of functional programming with logic.
Isabelle/HOL supports the extraction of the functional fragment to actual code in various languages~\cite{haftmann2007code}.

Isabelle's syntax is a variation of Standard ML combined with (almost) standard mathematical notation.
Function application is written infix, and functions are usually Curried (i.e., function $f$ applied to arguments $x_1~\ldots~x_n$ is written as $f~x_1~\ldots~x_n$ instead of the standard notation $f(x_1,~\ldots~,x_n)$).
We explain non-standard syntax in the paper where it occurs.

Isabelle is designed for trustworthiness: following the LCF approach~\cite{milner1972logic}, a small kernel implements the inference rules of the logic, and, using encapsulation features of ML, it guarantees that all theorems are actually proved by this small kernel.
% Isabelle follows the Logic for Computable Functions architecture~\cite{milner1972logic}: it is designed for trustworthiness, where a small kernel implements the inference rules of the logic, and, using encapsulation features of ML, it is guaranteed that all theorems are actually proved by this small kernel.
Around the kernel, there is a large set of tools that implement proof tactics and high-level concepts like algebraic datatypes and recursive functions.
Bugs in these tools cannot lead to inconsistent theorems being proved, but only to error messages when the kernel refuses a proof.
