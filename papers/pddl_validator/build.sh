#!/bin/bash
#projDir=~/bitBucket/planning/

pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd`
pushd "../.." > /dev/null
projDir=`pwd`
popd > /dev/null
popd > /dev/null

# projDir=/media/Data/Automated_reasoning/PhD_research/planning_bitbucket/verifiedValidator/
export TEXINPUTS=.:$projDir/papers/latexIncludes/:$projDir/papers/latexIncludes/IEEEtran/:
#export OSFONTS=$projDir/papers//
# export TEXFONTMAPS=.:$projDir/papers//:
# export TFMFONTS=.:$projDir/papers//:
# export BIBINPUTS=.:$projDir/papers//:
# export BSTINPUTS=.:$projDir/papers//:
# export MFINPUTS=.:$projDir/papers//:
# export MPINPUTS=.:$projDir/papers//:
# export VFFONTS=.:$projDir/papers//:
# export MISCFONTS=.:$projDir/papers//:
# export T1FONTS=.:$projDir/papers//:
rm verifiedValidator.pdf

echo $TEXINPUTS

# rm *.aux
# rm *.tex
# ../../../../HOL/bin/Holmake cleanAll
latexmk -f -gg -pdf  verifiedValidator.tex
