\section{Verifying a SAS+ Validator}\label{sec:SASPValidation}
We now describe our approach to construct a formally verified validator for problems in the format produced by the Translator module of the planning system Fast-Downward, which is an extension of SAS+.
One motivation for this validator is that it is simple enough to be used to explain our methodology and the different features of Isabelle/HOL that we use to construct formally verified validators.
%In this section we describe in details the process of obtaining a formally verified validator.

\subsection{Formalising the Abstract Syntax Tree of SAS+}

We start by formally defining the abstract syntax tree of the output of the Translator as described on Fast-Downward's web-page\footnote{A link will be provided in case of acceptance}.
For that, we first define the concepts that constitute the syntax tree. 
We declare those concepts as \fquot{type synonyms}, i.e. abbreviations for types. The most basic concept is that of a name, which is defined as an Isabelle/HOL string.

\noindent \isacommand{type{\isacharunderscore}synonym}\isamarkupfalse%
\ name\ {\isacharequal}\ string

A \emph{SAS+ variable} is defined as a triple made of \begin{enumerate*} \item the name of the variable, \item a \fquot{nat option} which is the axiom layer of the variable, and \item a list of names, each of which represents an assignment of that variable.\end{enumerate*}
\fquot{nat option} is a type with either no value at all (in case the variable has no axiom layer), or some value of type \fquot{nat}, a natural number.

\noindent\isacommand{type{\isacharunderscore}synonym}\isamarkupfalse\isanewline
\ \ \ ast{\isacharunderscore}variable\ {\isacharequal}\ {\isachardoublequoteopen}name\ {\isasymtimes}\ nat\ option\ {\isasymtimes}\ name\ list{\isachardoublequoteclose}

We model partial states (i.e. an assignment of a set of variables) as lists of pairs of natural numbers, representing the variable ID and the assigned value.
The variable ID is in the order of declaration of the variables.
% The first of each pair is a variable ID which is the order in which the variable was declared, and the second is the assignment.
Partial states are used to model preconditions and the goal. 

We model an effect as the following 4-tuple, whose members are \begin{enumerate*}\item the effect preconditions, \item the affected variable, \item the implicit precondition (if any), which is an assignment that the affected variable needs to have for the effect to take place, \item and the new value for the variable.\end{enumerate*}

\noindent\isacommand{type{\isacharunderscore}synonym}\isamarkupfalse\isanewline
\ \ \ ast{\isacharunderscore}effect\ {\isacharequal}\ {\isachardoublequoteopen}ast{\isacharunderscore}precond\ list\ {\isasymtimes}\ nat\ {\isasymtimes}\ nat\ option\ {\isasymtimes}\ nat{\isachardoublequoteclose}

The type of an operator is the following 4-tuple, which has the operator name, preconditions, effects and cost.

\noindent\isacommand{type{\isacharunderscore}synonym}\isamarkupfalse%
\ ast{\isacharunderscore}operator\ {\isacharequal}\isanewline
\ \ \ {\isachardoublequoteopen}name\ {\isasymtimes}\ ast{\isacharunderscore}precond\ list\ {\isasymtimes}\ ast{\isacharunderscore}effect\ list\ {\isasymtimes}\ nat{\isachardoublequoteclose}

Finally, we model a problem as follows.

\noindent\isacommand{type{\isacharunderscore}synonym}\isamarkupfalse%
\ ast{\isacharunderscore}problem\ {\isacharequal}\ {\isachardoublequoteopen}ast{\isacharunderscore}variable{\isacharunderscore}section\ {\isasymtimes}\ ast{\isacharunderscore}initial{\isacharunderscore}state\ {\isasymtimes}\ ast{\isacharunderscore}goal\ {\isasymtimes}\ ast{\isacharunderscore}operator{\isacharunderscore}section{\isachardoublequoteclose}

The variable and operator sections are lists of variables and operators, respectively.
The initial state is a list of natural numbers (assignments), whose length has to be equal to the number of state variables, and the goal is a partial state.

We now have defined the types of the objects that constitute the abstract syntax tree.
Before we specify the predicates for its well-formedness, we briefly introduce the \fquot{locale} feature of Isabelle/HOL.
% 
% Now that we have defined the types of the objects that constitute the syntax tree, we demonstrate the predicates that characterise the well-formedness of an abstract syntax tree representing a problem.
% Before we go into this, we discuss a feature of Isabelle/HOL referred to as a \fquot{locale}.

\subsubsection{Isabelle/HOL Locales}
A locale is a declared scope that fixes some identifiers and makes some assumptions that are to be shared among all the definitions and theorems 
declared within the locale.
Both, fixed identifiers and assumptions, are optional.
Locales are useful if one does not want to repeat the respective identifiers or assumptions for every definition.
% and it includes some bound variable names or assumptions that are to be shared among all the definitions and theorems declared within the locale.
% It is useful in situations where one would not want to repeat the respective variable name or a set of assumptions.
To define the predicates of well-formed variables, actions, and problems we declare the following locale.

\isacommand{locale}\isamarkupfalse%
\ ast{\isacharunderscore}problem\ {\isacharequal}\isanewline
\ \ \ \ \isakeyword{fixes}\ problem\ {\isacharcolon}{\isacharcolon}\ ast{\isacharunderscore}problem

This locale fixes the identifier \fquot{problem} and asserts that it is of type \fquot{ast{\isacharunderscore}problem}, i.e. it is an abstract syntax tree of a problem.
Also in this locale, we define the following function to access the set of state variables in a problem without needing to list \fquot{problem} as one of the arguments since the function is defined in the locale.

\isacommand{definition}\isamarkupfalse%
\ astDom\ {\isacharcolon}{\isacharcolon}\ ast{\isacharunderscore}variable{\isacharunderscore}section\ \isanewline
\ \ \ \ \isakeyword{where}\ {\isachardoublequoteopen}astDom\ {\isasymequiv}\ case\ problem\ of\ {\isacharparenleft}D{\isacharcomma}I{\isacharcomma}G{\isacharcomma}{\isasymdelta}{\isacharparenright}\ {\isasymRightarrow}\ D{\isachardoublequoteclose}

We similarly define other functions to access the initial state, the goal and the set of actions, and refer to them as \fquot{astI}, \fquot{astG} and \fquot{ast$\delta$}.

\subsubsection{Well-Formedness}

The following predicate then defines a well-formed (partial) state  w.r.t. \fquot{problem}\footnote{\fquot{map} is the high-order function that applies a function to each element of a list.}.

\isacommand{definition}\isamarkupfalse%
\ {\isachardoublequoteopen}wf{\isacharunderscore}partial{\isacharunderscore}state\ ps\ {\isasymequiv}\ \isanewline
\ \ \ \ \ \ distinct\ {\isacharparenleft}map\ fst\ ps{\isacharparenright}\ \isanewline
\ \ \ \ \ \ {\isasymand}\ {\isacharparenleft}{\isasymforall}{\isacharparenleft}x{\isacharcomma}v{\isacharparenright}\ {\isasymin}\ set\ ps{\isachardot}\ x\ {\isacharless}\ numVars\ {\isasymand}\ v\ {\isacharless}\ numVals\ x{\isacharparenright}{\isachardoublequoteclose}

For a state to be well-formed it has to: \begin{enumerate*} \item map every variable to a value only once, and \item map only valid variables to valid values for that variable.
% map a variable to a value if this value is one of the assignments of that variable.
\end{enumerate*}
Next, we define a well-formed operator as follows.

\isacommand{definition}\isamarkupfalse%
\ wf{\isacharunderscore}operator\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}ast{\isacharunderscore}operator\ {\isasymRightarrow}\ bool{\isachardoublequoteclose}\ \isanewline
\ \ \isakeyword{where}\ {\isachardoublequoteopen}wf{\isacharunderscore}operator\ {\isasymequiv}\ {\isasymlambda}{\isacharparenleft}name{\isacharcomma}\ pres{\isacharcomma}\ effs{\isacharcomma}\ cost{\isacharparenright}{\isachardot}\ \isanewline
\ \ \ \ wf{\isacharunderscore}partial{\isacharunderscore}state\ pres\ \isanewline
\ \ {\isasymand}\ distinct\ {\isacharparenleft}map\ {\isacharparenleft}{\isasymlambda}{\isacharparenleft}{\isacharunderscore}{\isacharcomma}\ x{\isacharcomma}\ {\isacharunderscore}{\isacharcomma}\ {\isacharunderscore}{\isacharparenright}{\isachardot}\ x{\isacharparenright}\ effs{\isacharparenright}\isanewline
\ \ {\isasymand}\ {\isacharparenleft}{\isasymforall}{\isacharparenleft}epres{\isacharcomma}x{\isacharcomma}vp{\isacharcomma}v{\isacharparenright}{\isasymin}set\ effs{\isachardot}\ \isanewline
\ \ \ \ \ \ wf{\isacharunderscore}partial{\isacharunderscore}state\ epres\ {\isasymand}\isanewline
\ \ \ \ \ \ x\ {\isacharless}\ numVars\ {\isasymand}\ v\ {\isacharless}\ numVals\ x\ {\isasymand}\isanewline
\ \ \ \ \ \ {\isacharparenleft}case\ vp\ of\ None\ {\isasymRightarrow}\ True\ {\isacharbar}\ Some\ v\ {\isasymRightarrow}\ v{\isacharless}numVals\ x{\isacharparenright}{\isacharparenright}{\isachardoublequoteclose}

That predicate is a lambda expression that takes an operator.
This operator is well-formed iff \begin{enumerate*} \item the preconditions are a well-formed partial state, \item every effect changes a different variable, and \item for every effect: \begin{itemize*} \item its  preconditions are well-formed, \item the effect changes a state variable, \item it changes the variable to a permissible assignment, and \item if it requires that variable to have some value as a precondition, this value needs to be a permissible value for that variable.\end{itemize*} \end{enumerate*}

Lastly, based on the above predicates, a well-formed problem is characterised as follows:

\isacommand{definition}\isamarkupfalse%
\ {\isachardoublequoteopen}well{\isacharunderscore}formed\ {\isasymequiv}\ \isanewline
\ \ \ \ \ \ length\ astI\ {\isacharequal}\ numVars\isanewline
\ \ \ \ {\isasymand}\ {\isacharparenleft}{\isasymforall}x{\isacharless}numVars{\isachardot}\ astI{\isacharbang}x\ {\isacharless}\ numVals\ x{\isacharparenright}\isanewline
\ \ \ \ {\isasymand}\ wf{\isacharunderscore}partial{\isacharunderscore}state\ astG\isanewline
\ \ \ \ {\isasymand}\ {\isacharparenleft}distinct\ {\isacharparenleft}map\ fst\ ast{\isasymdelta}{\isacharparenright}{\isacharparenright}\isanewline
\ \ \ \ {\isasymand}\ {\isacharparenleft}{\isasymforall}{\isasympi}{\isasymin}set\ ast{\isasymdelta}{\isachardot}\ wf{\isacharunderscore}operator\ {\isasympi}{\isacharparenright}{\isachardoublequoteclose}

A problem is well-formed iff \begin{enumerate*} \item  its initial state assigns \emph{all} state variables to permissible values, \item the goal is a well-formed partial state, \item all operators have distinct names, and \item every operator is well-founded.\end{enumerate*}

\subsection{Semantics of SAS+ Planning Problems}

To formalise the semantics in the language of Isabelle/HOL we define predicates and functions that describe execution.
We first define two functions that, w.r.t. the problem, return the set of valid states and the set of valid states subsuming a given state.

\noindent\isacommand{definition}\isamarkupfalse%
\noindent valid{\isacharunderscore}states\ \isakeyword{where}\isanewline
{\isachardoublequoteopen}valid{\isacharunderscore}states\ {\isasymequiv}
{\isacharbraceleft}s{\isachardot}\ dom\ s\ {\isacharequal}\ Dom\ {\isasymand}\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ {\isacharparenleft}{\isasymforall}x{\isasymin}Dom{\isachardot}\ the\ {\isacharparenleft}s\ x{\isacharparenright}\ {\isasymin}\ range{\isacharunderscore}of{\isacharunderscore}var\ x{\isacharparenright}{\isacharbraceright}{\isachardoublequoteclose}

\noindent\isacommand{definition}\isamarkupfalse%
\ subsuming{\isacharunderscore}states\ \isakeyword{where}\isanewline
{\isachardoublequoteopen}subsuming{\isacharunderscore}states ps\ {\isasymequiv}\ {\isacharbraceleft}s{\isasymin}valid{\isacharunderscore}states{\isachardot}\ ps{\isasymsubseteq}\isactrlsub ms{\isacharbraceright}{\isachardoublequoteclose}

Here, the syntax $\{x\in S.\ P\}$ denotes the subset of elements of set $S$ for which $P$ holds, and $f \subseteq_m g$ means that any argument mapped to a value by $f$ is mapped to the same value by $g$.

We now define two predicates that define when an operator and an effect are enabled in a given state, respectively.
An operator can execute in a given state iff: \begin{enumerate*} \item the operator belongs to the problem, \item the state at which the operator is executed subsumes the operator's prevail preconditions, and \item every effect's implicit precondition is subsumed by the state at which the operator is executed\footnote{\fquot{map\_of}  takes a list of pairs and returns a function that corresponds to that list.}.\end{enumerate*}

\noindent\isacommand{definition}\isamarkupfalse%
\ enabled\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}name\ {\isasymRightarrow}\ state\ {\isasymRightarrow}\ bool{\isachardoublequoteclose}\isanewline
\ \ \isakeyword{where}\ {\isachardoublequoteopen}enabled\ name\ s\ {\isasymequiv}\isanewline
\ \ \ \ case\ lookup{\isacharunderscore}operator\ name\ of\isanewline
\ \ \ \ \ \ \ Some\ {\isacharparenleft}{\isacharunderscore}{\isacharcomma}pres{\isacharcomma}effs{\isacharcomma}{\isacharunderscore}{\isacharparenright}\ {\isasymRightarrow}\ \isanewline
\ \ \ \ \ \ \ \ \ \ s{\isasymin}subsuming{\isacharunderscore}states\ {\isacharparenleft}map{\isacharunderscore}of\ pres{\isacharparenright}\isanewline
\ \ \ \ \ \ \ \ \ \ \ {\isasymand}s{\isasymin}subsuming{\isacharunderscore}states\ {\isacharparenleft}map{\isacharunderscore}of\ {\isacharparenleft}implicit{\isacharunderscore}pres\ effs{\isacharparenright}{\isacharparenright}\isanewline
\ \ \ \ \ \ {\isacharbar}\ None\ {\isasymRightarrow}\ False{\isachardoublequoteclose}

An effect on the other hand is enabled at a state iff its preconditions are subsumed by the state.

\isacommand{definition}\isamarkupfalse%
\ eff{\isacharunderscore}enabled\ \isakeyword{where}\isanewline
{\isachardoublequoteopen}eff{\isacharunderscore}enabled\ s\ {\isasymequiv}\isanewline
\ \ \ \ \ \ {\isasymlambda}{\isacharparenleft}pres{\isacharcomma}{\isacharunderscore}{\isacharcomma}{\isacharunderscore}{\isacharcomma}{\isacharunderscore}{\isacharparenright}{\isachardot}\ s{\isasymin}subsuming{\isacharunderscore}states{\isacharparenleft}map{\isacharunderscore}of\ pres{\isacharparenright}{\isachardoublequoteclose}

Now we define the function that returns the state resulting from executing an operator in a given state, assuming that the operator is enabled in that state.
The function takes an operator name.
If it exists in the given problem's list of operators then the operator is executed.
This is done by modifying every assignment of every variable in the given state to match its corresponding assignment in the operator's effects, given that the effect is enabled.
If the given operator name does not exist the function's return value is not defined\footnote{In the above definition \fquot{{\isacharplus}{\isacharplus}} takes two functions and merges them, with precedence to its right argument. 
\fquot{filter} is a high-order function that removes members that satisfy a given predicate from a list.}.


\noindent\isacommand{definition}\isamarkupfalse%
\ execute\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}name\ {\isasymRightarrow}\ state\ {\isasymRightarrow}\ state{\isachardoublequoteclose}\ \isakeyword{where}\isanewline
\ \ {\isachardoublequoteopen}execute\ name\ s\ {\isasymequiv}\ \isanewline
\ \ \ \ case\ lookup{\isacharunderscore}operator\ name\ of\isanewline
\ \ \ \ \ \ Some\ {\isacharparenleft}{\isacharunderscore}{\isacharcomma}{\isacharunderscore}{\isacharcomma}effs{\isacharcomma}{\isacharunderscore}{\isacharparenright}\ {\isasymRightarrow}\isanewline
\ \ \ \ \ \ \ \ \ \ \ s\ {\isacharplus}{\isacharplus}\ map{\isacharunderscore}of\ {\isacharparenleft}map\ {\isacharparenleft}{\isasymlambda}{\isacharparenleft}{\isacharunderscore}{\isacharcomma}x{\isacharcomma}{\isacharunderscore}{\isacharcomma}v{\isacharparenright}{\isachardot}\ {\isacharparenleft}x{\isacharcomma}v{\isacharparenright}{\isacharparenright}\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ {\isacharparenleft}filter\ {\isacharparenleft}eff{\isacharunderscore}enabled\ s{\isacharparenright}\ effs{\isacharparenright}{\isacharparenright}\isanewline
\ \ \ \ {\isacharbar}\ None\ {\isasymRightarrow}\ undefined{\isachardoublequoteclose}

Given the previous execution semantics, a valid path between two states is recursively defined as follows.
Note: every action in the path has to be enabled before executing it.

\isacommand{fun}\isamarkupfalse%
\ path{\isacharunderscore}to\ \isakeyword{where}\isanewline
\ \ \ \ \ \ {\isachardoublequoteopen}path{\isacharunderscore}to\ s\ {\isacharbrackleft}{\isacharbrackright}\ s{\isacharprime}\ {\isasymlongleftrightarrow}\ s{\isacharprime}{\isacharequal}s{\isachardoublequoteclose}\isanewline
\ \ \ \ {\isacharbar}\ {\isachardoublequoteopen}path{\isacharunderscore}to\ s\ {\isacharparenleft}{\isasympi}{\isacharhash}{\isasympi}s{\isacharparenright}\ s{\isacharprime}\ {\isasymlongleftrightarrow}\ enabled\ {\isasympi}\ s\ {\isasymand}\ path{\isacharunderscore}to\ {\isacharparenleft}execute\ {\isasympi}\ s{\isacharparenright}\ {\isasympi}s\ s{\isacharprime}{\isachardoublequoteclose}



Lastly, the following predicate specifies when is an action sequence a plan.

\isacommand{definition}\isamarkupfalse%
\ valid{\isacharunderscore}plan\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}plan\ {\isasymRightarrow}\ bool{\isachardoublequoteclose}\ \isanewline
\ \ \ \ \ \ \isakeyword{where}\ {\isachardoublequoteopen}valid{\isacharunderscore}plan\ {\isasympi}s\ {\isasymequiv}\ {\isasymexists}s{\isacharprime}{\isasymin}G{\isachardot}\ path{\isacharunderscore}to\ I\ {\isasympi}s\ s{\isacharprime}{\isachardoublequoteclose}

In the predicate above \fquot{G} is the set of goal states and \fquot{I} is a function that given a state variable returns the assignment of that variable in the initial state.

\subsubsection{Sanity Checking Lemmas}

As stated earlier in the introduction, one advantage of formally specifying the semantics of some language in a theorem prover is that one can prove sanity checking theorems for the semantics.
Those theorems are proven for more assurance that the semantics were formalised as intended.
An example sanity check on the semantics, is that the execution of an operator (sequence) from a problem on a state valid w.r.t. the problem will result in a state that is also valid w.r.t. the problem.
This is expressed formally as follows in Isabelle/HOL.

%% \isacommand{lemma}\isamarkupfalse%
%% \ I{\isacharunderscore}valid{\isacharcolon}\ {\isachardoublequoteopen}I\ {\isasymin}\ valid{\isacharunderscore}states{\isachardoublequoteclose}

\isacommand{lemma}\isamarkupfalse%
\ path{\isacharunderscore}to{\isacharunderscore}pres{\isacharunderscore}valid{\isacharcolon}\isanewline
\ \ \ \ \ \ \isakeyword{assumes}\ {\isachardoublequoteopen}s{\isasymin}valid{\isacharunderscore}states{\isachardoublequoteclose}\ {\isachardoublequoteopen}path{\isacharunderscore}to\ s\ {\isasympi}s\ s{\isacharprime}{\isachardoublequoteclose}\isanewline
\ \ \ \ \ \ \isakeyword{shows}\ {\isachardoublequoteopen}s{\isacharprime}{\isasymin}valid{\isacharunderscore}states{\isachardoublequoteclose}

\noindent Proofs of those lemmas are either basic rewriting or basic induction.

\subsection{The Validator Implementation}

The above specification concisely describes the semantics of SAS+ problems. However, it contains some elements that cannot be extracted to executable code. For example, the set comprehension and map comparison in \fquot{subsuming\_states} are not executable, as the involved domains could potentially be infinite.
% Although the predicates and functions described characterise the well-formedness and the semantics of SAS+ problems concisely, they cannot be used directly to compute the validity of plans.
% This is because many of them are defined using set comprehensions, (e.g. \fquot{subsuming\_states}), and Isabelle's code generator cannot show that these sets are finite let alone find a construction principle for them.
% Accordingly the code generator cannot generate executable versions of them.
Thus, we provide an alternative version of these specifications using more executable data structures like finite lists, and show that they are equivalent to the original specifications.

% 
% The way to go around that is to specify implementations of those predicate or functions that use data types whose constructors are well-known to the code generator, like finite lists.
% Those implementations are to be provided in Isabelle/HOL's langugage.
For example to provide a computable version of \fquot{subsuming\_states}, we first define a function to check whether a single assignment is subsumed by a state:

\noindent \isacommand{definition}\isamarkupfalse%
\ match{\isacharunderscore}pre\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}ast{\isacharunderscore}precond\ {\isasymRightarrow}\ state\ {\isasymRightarrow}\ bool{\isachardoublequoteclose}\isanewline
\ \ \ \ \isakeyword{where}\ {\isachardoublequoteopen}match{\isacharunderscore}pre\ {\isasymequiv}\ {\isasymlambda}{\isacharparenleft}x{\isacharcomma}v{\isacharparenright}\ s{\isachardot}\ s\ x\ {\isacharequal}\ Some\ v{\isachardoublequoteclose}

We then extend it to lists of assignments, and show that the extended function is equivalent to \fquot{subsuming\_states}.

\noindent \isacommand{definition}\isamarkupfalse%
\ match{\isacharunderscore}pres\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}ast{\isacharunderscore}precond\ list\ {\isasymRightarrow}\ state\ {\isasymRightarrow}\ bool{\isachardoublequoteclose}\ \isakeyword{where}\ \isanewline
\ \ \ \ {\isachardoublequoteopen}match{\isacharunderscore}pres\ pres\ s\ {\isasymequiv}\ {\isasymforall}pre{\isasymin}set\ pres{\isachardot}\ match{\isacharunderscore}pre\ pre\ s{\isachardoublequoteclose}\isanewline

\noindent \isacommand{lemma}\isamarkupfalse%
\ match{\isacharunderscore}pres{\isacharunderscore}correct{\isacharcolon}\isanewline
\ \isakeyword{assumes}\ {\isachardoublequoteopen}distinct\ {\isacharparenleft}map\ fst\ pres{\isacharparenright}{\isachardoublequoteclose}\ \ {\isachardoublequoteopen}s{\isasymin}valid{\isacharunderscore}states{\isachardoublequoteclose}\isanewline
\ \isakeyword{shows}\ {\isachardoublequoteopen}match{\isacharunderscore}pres\ pres\ s\ {\isasymlongleftrightarrow}\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ s{\isasymin}subsuming{\isacharunderscore}states\ {\isacharparenleft}map{\isacharunderscore}of\ pres{\isacharparenright}{\isachardoublequoteclose}

By replacing \fquot{subsuming\_states} with \fquot{match\_pres}, and using lists instead of sets of assignments, we obtain executable functions
\fquot{enabled$'$}, \fquot{eff\_enabled$'$}, \fquot{execute$'$}, which we prove to be equivalent to their abstract counterparts. For example, we prove:
% 
% 
% We then build on top of that computable versions of \fquot{enabled}, \fquot{eff\_enabled}, \fquot{execute} and call them \fquot{enabled$'$}, \fquot{eff\_enabled$'$}, \fquot{execute$'$}, by plugging-in \fquot{match\_pres} instead of \fquot{subsuming\_states}.
% We then prove the equivalence of the computable version to the original functions as follows.

\isacommand{lemma}\isamarkupfalse%
\ execute{\isacharprime}{\isacharunderscore}correct{\isacharcolon}\isanewline
\ \ \ \ \ \ \isakeyword{assumes}\ V{\isacharcolon}\ {\isachardoublequoteopen}s{\isasymin}valid{\isacharunderscore}states{\isachardoublequoteclose}\isanewline
\ \ \ \ \ \ \isakeyword{assumes}\ {\isachardoublequoteopen}enabled\ name\ s{\isachardoublequoteclose}\ \ \ \ \ \ \isanewline
\ \ \ \ \ \ \isakeyword{shows}\ {\isachardoublequoteopen}execute{\isacharprime}\ problem\ name\ s\ {\isacharequal}\ execute\ name\ s{\isachardoublequoteclose}

Another example of a non-executable specification is the existential quantifier in \fquot{valid\_plan}. Fortunately, as the resulting state is uniquely determined
by the initial state and the plan, it can be easily eliminated:

\isacommand{fun}\isamarkupfalse%
\ simulate{\isacharunderscore}plan{\isacharprime}\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}plan\ {\isasymRightarrow}\ state\ {\isasymRightarrow}\ state\ option{\isachardoublequoteclose} \isanewline\isakeyword{where}\isanewline
\ \ \ \ {\isachardoublequoteopen}simulate{\isacharunderscore}plan{\isacharprime}\ {\isacharbrackleft}{\isacharbrackright}\ s\ {\isacharequal}\ Some\ s{\isachardoublequoteclose}\isanewline
\ \ {\isacharbar}\ {\isachardoublequoteopen}simulate{\isacharunderscore}plan{\isacharprime}\ {\isacharparenleft}{\isasympi}{\isacharhash}{\isasympi}s{\isacharparenright}\ s\ {\isacharequal}\ {\isacharparenleft}\isanewline
\ \ \ \ \ \ if\ enabled{\isacharprime}\ {\isasympi}\ s\ then%\isanewline
\ simulate{\isacharunderscore}plan{\isacharprime}\ {\isasympi}s\ {\isacharparenleft}execute{\isacharprime}\ {\isasympi}\ s{\isacharparenright}\isanewline
\ \ \ \ \ \ else\ None{\isacharparenright}{\isachardoublequoteclose}%

\isacommand{lemma}\isamarkupfalse\ simulate{\isacharunderscore}plan{\isacharprime}{\isacharunderscore}correct{\isacharcolon}\ \isanewline
\ \ \isakeyword{assumes}\ {\isachardoublequoteopen}s{\isasymin}valid{\isacharunderscore}states{\isachardoublequoteclose}\isanewline
\ \ \isakeyword{shows}\ {\isachardoublequoteopen}simulate{\isacharunderscore}plan{\isacharprime}\ {\isasympi}s\ s\ {\isacharequal}\ Some\ s{\isacharprime}\isanewline
\ \ \ \ \ \ \ \ {\isasymlongleftrightarrow}\ path{\isacharunderscore}to\ s\ {\isasympi}s\ s{\isacharprime}{\isachardoublequoteclose}


% 
% execution is deterministic, 
% it can be easily eliminated
% 
% uncomputable predicate is that of \fquot{valid\_plan} since it has an existential quantifier that requires enumerating a set.
% To implement a computable version, we first define the following function which we show that it computes  \fquot{path\_to}.

% \isacommand{fun}\isamarkupfalse%
% \ simulate{\isacharunderscore}plan{\isacharprime}\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}plan\ {\isasymRightarrow}\ state\ {\isasymRightarrow}\ state\ option{\isachardoublequoteclose}\ \isakeyword{where}\isanewline
% \ \ \ \ {\isachardoublequoteopen}simulate{\isacharunderscore}plan{\isacharprime}\ {\isacharbrackleft}{\isacharbrackright}\ s\ {\isacharequal}\ Some\ s{\isachardoublequoteclose}\isanewline
% \ \ {\isacharbar}\ {\isachardoublequoteopen}simulate{\isacharunderscore}plan{\isacharprime}\ {\isacharparenleft}{\isasympi}{\isacharhash}{\isasympi}s{\isacharparenright}\ s\ {\isacharequal}\ {\isacharparenleft}\isanewline
% \ \ \ \ \ \ if\ enabled{\isacharprime}\ {\isasympi}\ s\ then\isanewline
% \ \ \ \ \ \ \ \ let\ s\ {\isacharequal}\ execute{\isacharprime}\ {\isasympi}\ s\ in\isanewline
% \ \ \ \ \ \ \ \ simulate{\isacharunderscore}plan{\isacharprime}\ {\isasympi}s\ s\isanewline
% \ \ \ \ \ \ else\ None{\isacharparenright}{\isachardoublequoteclose}%
% 
% \isacommand{lemma}\isamarkupfalse%
% \ simulate{\isacharunderscore}plan{\isacharprime}{\isacharunderscore}correct{\isacharcolon}\isanewline
% \ \isakeyword{assumes}\ {\isachardoublequoteopen}s{\isasymin}valid{\isacharunderscore}states{\isachardoublequoteclose}\isanewline
% \ \isakeyword{shows}\ {\isachardoublequoteopen}simulate{\isacharunderscore}plan{\isacharprime}\ problem\ {\isasympi}s\ s\ {\isacharequal}\ simulate{\isacharunderscore}plan\ {\isasympi}s\ s{\isachardoublequoteclose}

After making some more specifications executable along the same lines, we finally define our executable plan checker, and show that it is correct:

% 
% Similarly we define a computable predicate \fquot{valid\_plan$'$}, show that it is equivalent to \fquot{valid\_plan}, and then use it to construct the following computable function to validate plans.





%% \isacommand{definition}\isamarkupfalse%
%% \ check{\isacharunderscore}plan{\isacharprime}\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}ast{\isacharunderscore}problem\ {\isasymRightarrow}\ plan\ {\isasymRightarrow}\ bool{\isachardoublequoteclose}\ \isakeyword{where}\isanewline
%% \ {\isachardoublequoteopen}check{\isacharunderscore}plan{\isacharprime}\ problem\ {\isasympi}s\ {\isacharequal}\ {\isacharparenleft}\isanewline
%% \ \ case\ simulate{\isacharunderscore}plan{\isacharprime}\ problem\ {\isasympi}s\ {\isacharparenleft}initial{\isacharunderscore}state{\isacharprime}\ problem{\isacharparenright}\ of\ \isanewline
%% \ \ \ \ None\ {\isasymRightarrow}\ False\ \isanewline
%% \ \ {\isacharbar}\ Some\ s{\isacharprime}\ {\isasymRightarrow}\ match{\isacharunderscore}pres\ {\isacharparenleft}ast{\isacharunderscore}problem{\isachardot}astG\ problem{\isacharparenright}\ s{\isacharprime}{\isacharparenright}{\isachardoublequoteclose}

%% \isacommand{lemma}\isamarkupfalse%
%% \ check{\isacharunderscore}plan{\isacharprime}{\isacharunderscore}correct{\isacharcolon}\isanewline
%% \ \ \ \ \ \ {\isachardoublequoteopen}check{\isacharunderscore}plan{\isacharprime}\ problem\ {\isasympi}s\ {\isacharequal}\ check{\isacharunderscore}plan\ {\isasympi}s{\isachardoublequoteclose}


\isacommand{definition}\isamarkupfalse%
\ verify{\isacharunderscore}plan\isanewline\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}ast{\isacharunderscore}problem\ {\isasymRightarrow}\ plan\ {\isasymRightarrow}\ String{\isachardot}literal\ {\isacharplus}\ unit{\isachardoublequoteclose}\ \isakeyword{where}\isanewline
\ \ \ \ {\isachardoublequoteopen}verify{\isacharunderscore}plan\ problem\ {\isasympi}s\ {\isacharequal}\ {\isacharparenleft}\isanewline
\ \ \ \ \ \ if\ ast{\isacharunderscore}problem{\isachardot}well{\isacharunderscore}formed\ problem\ then\isanewline
\ \ \ \ \ \ \ \ if\ check{\isacharunderscore}plan{\isacharprime}\ problem\ {\isasympi}s\ then\ Inr{\isacharparenleft}{\isacharparenright}\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ else\ Inl\ {\isacharparenleft}STR\ {\isacharprime}{\isacharprime}Invalid\ plan{\isacharprime}{\isacharprime}{\isacharparenright}\isanewline
\ \ \ \ \ \ else\ Inl\ {\isacharparenleft}STR\ {\isacharprime}{\isacharprime}Problem\ not\ well\ formed{\isacharprime}{\isacharprime}{\isacharparenright}{\isacharparenright}{\isachardoublequoteclose}

% We finally prove that this function is a sound validator for the well-formedness of a problem and validity of candidate plan.

\isacommand{lemma}\isamarkupfalse%
\ verify{\isacharunderscore}plan{\isacharunderscore}correct{\isacharcolon}\isanewline
\ \ \ \ {\isachardoublequoteopen}verify{\isacharunderscore}plan\ problem\ {\isasympi}s\ {\isacharequal}\ Inr\ {\isacharparenleft}{\isacharparenright}\ {\isasymlongleftrightarrow}\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ast{\isacharunderscore}problem{\isachardot}well{\isacharunderscore}formed\ problem\ {\isasymand}\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ ast{\isacharunderscore}problem{\isachardot}valid{\isacharunderscore}plan\ problem\ {\isasympi}s{\isachardoublequoteclose}

That is, we have proved that our validator returns the value \fquot{Inr\ ()} if and only if the problem is well-formed and the plan is valid. 
Otherwise, it returns a value \fquot{Inl\ msg} containing an error message. Note that, for technical reasons, the problem is passed as explicit 
parameter here, while it was implicit via a locale before.

Furthermore, we can instruct Isabelle's code generator to use some alternative equations to translate a function, provided we prove that alternative equations to hold.
For example, the function \fquot{simulate\_plan$'$} looks up the same operator twice, once in the call to \fquot{execute$'$} and another time in \fquot{enabled$'$}.
This can be avoided by proving the following alternative representation, which will then be used by Isabelle's code generator:

% 
% 
% Furthermore, we can provide more optimised specifications of the computable functions and still show that they are equivalent to the elegant specifications.
% For example, the function \fquot{simulate\_plan$'$} looks up the same operator twice, once in the call to \fquot{execute$'$} and another time in \fquot{enabled$'$}.
% This can be avoided by proving the following alternative representation, which will then be used by Isabelle's code generator:

% 
% showing that it can be rewritten into the following code equations, that only perform one lookup, and those code equations are the ones to be generated.

\isacommand{lemma}\isamarkupfalse%
\ simulate{\isacharunderscore}plan{\isacharprime}{\isacharunderscore}code{\isacharbrackleft}code{\isacharbrackright}{\isacharcolon}\isanewline
\ \ \ \ {\isachardoublequoteopen}simulate{\isacharunderscore}plan{\isacharprime}\ problem\ {\isacharbrackleft}{\isacharbrackright}\ s\ {\isacharequal}\ Some\ s{\isachardoublequoteclose}\isanewline
\ \ \ \ {\isachardoublequoteopen}simulate{\isacharunderscore}plan{\isacharprime}\ problem\ {\isacharparenleft}{\isasympi}{\isacharhash}{\isasympi}s{\isacharparenright}\ s\ {\isacharequal}\ {\isacharparenleft}\isanewline
\ \ \ \ \ \ case\ lookup{\isacharunderscore}operator{\isacharprime}\ problem\ {\isasympi}\ of\isanewline
\ \ \ \ \ \ \ \ None\ {\isasymRightarrow}\ None\isanewline
\ \ \ \ \ \ {\isacharbar}\ Some\ {\isasympi}\ {\isasymRightarrow}\ \isanewline
\ \ \ \ \ \ \ \ \ \ if\ enabled{\isacharunderscore}opr{\isacharprime}\ {\isasympi}\ s\ then\ \isanewline
\ \ \ \ \ \ \ \ \ \ \ \ simulate{\isacharunderscore}plan{\isacharprime}\ problem\ {\isasympi}s\ {\isacharparenleft}execute{\isacharunderscore}opr{\isacharprime}\ {\isasympi}\ s{\isacharparenright}\isanewline
\ \ \ \ \ \ \ \ \ \ else\ None{\isacharparenright}{\isachardoublequoteclose}

\subsection{Parsing Problems and Obtaining an Executable}

Isabelle/HOL's code generator can generate implementations of computable functions in different languages.
We generated a Standard ML \cite{milner1997definition} implementation of the function \fquot{verify\_plan}, which is the validation function proven correct above.
This generated function expects two arguments, a planning problem and a plan.
%% Its signature is shown in Figure~\ref{fig:ParsedProbPlanType}.

%% \begin{figure}[!htb]
%% \centering
%% \begin{subfigure}[b]{0.49 \textwidth}
%% \input{ParsedProbPlanType}
%% \caption[ParsedProbPlanType]{\label{fig:ParsedProbPlanType}}
%% \end{subfigure}
%% \begin{subfigure}[b]{0.49 \textwidth}
%% \input{SASPVarParseSpec}
%% \caption[SASPVarParseSpec]{\label{fig:SASPVarParseSpec}}
%% \end{subfigure}
%% \caption{
%%  (a) shows the signature of the function \fquot{verify\_plan}.
%% (b) is the specification of the syntactic structure of a SAS+ variable in terms of parser combinators.
%% \label{fig:smlSnippets}}
%% \end{figure}


The remaining part is parsing the generated file with the problem and a plan file that has a sequence of actions.
We do that using an open source parser combinator library written in Standard ML\footnote{We would refer to its repository in case of acceptance.}, in which a description of the syntax of the language to parse is specified in terms of parsing primitives, that are themselves Standard ML functions.
%For example, the description of the syntax of a generated SAS+ variable is show in Figure~\ref{fig:SASPVarParseSpec}.
We note, however, that parsing is a trusted part of our validator, i.e.\ we have no formal proof that the parser actually recognizes the desired grammar and produces the correct abstract syntax tree. However, the parsing combinator approach allows to write concise, clean, and legible parsers, which can be easily checked manually.
