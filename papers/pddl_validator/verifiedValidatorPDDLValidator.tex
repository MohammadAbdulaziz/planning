\section{A Formally Verified Plan Validator}
\label{sec:PDDLValidation}

Using the Semantics of the fragment of PDDL defined in Section~\ref{sec:PDDL}, we concisely write down a PDDL plan validator.
Given a problem \fquot{P} and plan \fquot{{\isasympi}s}, the validator checks that the problem is well-formed and the plan is valid:

{\footnotesize\noindent
  validator\ P\ {\isasympi}s {\isasymequiv} wf{\isacharunderscore}problem\ P\ {\isasymand}\ valid{\isacharunderscore}plan\ P\ {\isasympi}s  
}

However, this specification depends on several abstract mathematical concepts that we used in our semantics,
e.g.\ sets and reflexive transitive closures. While these are well-suited for an elegant specification, they cannot be directly executed.
Thus, we prove that our abstract specification is equivalent to a concrete, algorithmic one, for which Isabelle/HOL can extract actual code.
This refinement approach gives us the best of two worlds: a concise semantics and an efficient checker that provably implements the semantics.

% A refinement approach solves this problem without obfuscating our specifications by implementation details: 
% We prove that our abstract specification is equivalent to a concrete, algorithmic specification for which Isabelle/HOL can extract actual code.
% This way, we get the best of both worlds: a concise semantics and an efficient checker that provably implements the semantics.

Our refinement proceeds in two steps.
First, we replace abstract specifications by algorithms defined on abstract mathematical types like sets.
Second, we replace the abstract types by concrete (verified) data structures like red-black trees. 
The second step is automated by the Containers Framework~\cite{lochbihler2013light}.
We exemplify some of the refinements below.

% The well-formedness criteria frequently require a uniqueness check, e.g., for action names or predicate names.
% The predicate \fquot{distinct} in Isabelle states that a list contains no duplicate elements. It is defined as
% 
% {\footnotesize\noindent
% \isacommand{fun}\isamarkupfalse%
% \ distinct\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}{\isacharprime}a\ list\ {\isasymRightarrow}\ bool{\isachardoublequoteclose}\ \isakeyword{where}\isanewline
% {\isachardoublequoteopen}distinct\ {\isacharbrackleft}{\isacharbrackright}\ {\isasymlongleftrightarrow}\ True{\isachardoublequoteclose}\ {\isacharbar}\isanewline
% {\isachardoublequoteopen}distinct\ {\isacharparenleft}x\ {\isacharhash}\ xs{\isacharparenright}\ {\isasymlongleftrightarrow}\ x\ {\isasymnotin}\ set\ xs\ {\isasymand}\ distinct\ xs{\isachardoublequoteclose}
% }
% 
% However, this definition would yield a quite inefficient algorithm. Instead, we prove
% 
% {\footnotesize\noindent
% \isacommand{lemma}\isamarkupfalse%
% \ {\isacharbrackleft}code{\isacharunderscore}unfold{\isacharbrackright}{\isacharcolon}\ {\isachardoublequoteopen}distinct\ l\ {\isacharequal}\ no{\isacharunderscore}stutter\ {\isacharparenleft}quicksort\ l{\isacharparenright}{\isachardoublequoteclose}
% }
% 
% where \fquot{no{\isacharunderscore}stutter} checks that there are no adjacent duplicate elements. 
% In practice, this algorithm is much more efficient.
% Note that the \fquot{code{\isacharunderscore}unfold} attribute instructs Isabelle's code generator to use this equation instead of the original one. 

For reflexive transitive closure, we use a (general purpose) DFS algorithm. We define

\vspace{.7ex}
{\footnotesize\noindent
\isacommand{definition}\isamarkupfalse%
\ {\isachardoublequoteopen}of{\isacharunderscore}type{\isacharunderscore}impl\ G\ oT\ T\isanewline {\isasymequiv}\ {\isacharparenleft}{\isasymforall}pt{\isasymin}set\ {\isacharparenleft}primitives\ oT{\isacharparenright}{\isachardot}\ dfs{\isacharunderscore}reachable\ G\ {\isacharparenleft}op{\isacharequal}pt{\isacharparenright}\ {\isacharparenleft}primitives\ T{\isacharparenright}{\isacharparenright}{\isachardoublequoteclose}
}

Here, the \fquot{G} parameter will be instantiated by the tabulated successor function of the subtype graph, which is only computed once and passed as extra parameter to all subsequent functions ($\lambda$-lifting). We prove


{\footnotesize\noindent
\isacommand{lemma}\isamarkupfalse%
\ of{\isacharunderscore}type{\isacharunderscore}impl{\isacharunderscore}correct{\isacharcolon}\ {\isachardoublequoteopen}of{\isacharunderscore}type{\isacharunderscore}impl\ STG\ oT\ T\ {\isasymlongleftrightarrow}\ of{\isacharunderscore}type\ oT\ T{\isachardoublequoteclose}
}

Above, \fquot{STG} is the actual tabulated subtype graph defined w.r.t.\ the implicitly fixed domain $D$. 
% Note that, for technical reasons, this lemma has the abstract part on the RHS, while the lemma for \fquot{distinct} was phrased the other way round.

Another crucial refinement summarises the enabledness check and effect application of a plan action. 
This way, the action has to be instantiated only once. Moreover, we use an error monad to report human-readable error messages:

\vspace{.7ex}
{\footnotesize\noindent
\isacommand{definition}\isamarkupfalse%
\ en{\isacharunderscore}exE{\isadigit{2}}\ \isakeyword{where}\isanewline
{\isachardoublequoteopen}en{\isacharunderscore}exE{\isadigit{2}}\ G\ mp\ {\isasymequiv}\ {\isasymlambda}{\isacharparenleft}PAction\ n\ args{\isacharparenright}\ {\isasymRightarrow}\ {\isasymlambda}M{\isachardot}\ do\ {\isacharbraceleft}\isanewline
\ \ a\ {\isasymleftarrow}\ resolve{\isacharunderscore}action{\isacharunderscore}schemaE\ n{\isacharsemicolon}\isanewline
\ \ check\ {\isacharparenleft}action{\isacharunderscore}params{\isacharunderscore}match{\isadigit{2}}\ G\ mp\ a\ args{\isacharparenright}\ {\isacharparenleft}ERRS\ {\isacharprime}{\isacharprime}Par.\ mism.{\isacharprime}{\isacharprime}{\isacharparenright}{\isacharsemicolon}\isanewline
\ \ let\ ai\ {\isacharequal}\ instantiate{\isacharunderscore}action{\isacharunderscore}schema\ a\ args{\isacharsemicolon}\isanewline
\ \ check\ {\isacharparenleft}holds\ M\ {\isacharparenleft}precondition\ ai{\isacharparenright}{\isacharparenright}\ {\isacharparenleft}ERRS\ {\isacharprime}{\isacharprime}Precondition\ not\ satisfied{\isacharprime}{\isacharprime}{\isacharparenright}{\isacharsemicolon}\isanewline
\ \ Error{\isacharunderscore}Monad{\isachardot}return\ {\isacharparenleft}apply{\isacharunderscore}effect\ {\isacharparenleft}effect\ ai{\isacharparenright}\ M{\isacharparenright}{\isacharbraceright}{\isachardoublequoteclose}%
}

Here, \fquot{G} will again be instantiated by the subtype relation, and \fquot{mp} will be instantiated by a map from object names to types.
The following lemma justifies the refinement:

\vspace{.7ex}
{\footnotesize\noindent
\isacommand{lemma}\isamarkupfalse%
\ {\isacharparenleft}\isakeyword{in}\ wf{\isacharunderscore}ast{\isacharunderscore}problem{\isacharparenright}\ en{\isacharunderscore}exE{\isadigit{2}}{\isacharunderscore}return{\isacharunderscore}iff{\isacharcolon}\isanewline
\isakeyword{assumes}\ {\isachardoublequoteopen}wm{\isacharunderscore}basic\ M{\isachardoublequoteclose}\isanewline
\isakeyword{shows}\ {\isachardoublequoteopen}en{\isacharunderscore}exE{\isadigit{2}}\ STG\ mp{\isacharunderscore}objT\ a\ M\ {\isacharequal}\ Inr\ M{\isacharprime}\isanewline
\ \ {\isasymlongleftrightarrow}\ plan{\isacharunderscore}action{\isacharunderscore}enabled\ a\ M\ {\isasymand}\ M{\isacharprime}\ {\isacharequal}\ execute{\isacharunderscore}plan{\isacharunderscore}action\ a\ M{\isachardoublequoteclose}}

That is, for a basic world model \fquot{M} and plan action \fquot{a}, the function 
\fquot{en{\isacharunderscore}exE{\isadigit{2}}\ STG\ mp{\isacharunderscore}objT\ a\ M} will return \fquot{Inr\ M'}, if and only if the action is enabled and its execution yields \fquot{M'}.
Otherwise, it returns \fquot{Inl\ msg} with a human readable error message.
Again, \fquot{STG} is the actual subtype relation, and \fquot{mp{\isacharunderscore}objT} is the actual mapping from objects to types.




% 
% 
% %TODO: Convert verbatims to proper isabelle notation, once examples are stable and fixed!
% 
% The above specification concisely describes the semantics of a fragment of PDDL problems.
% However, the cost of conciseness is that our semantic specification it has some elements that cannot be extracted to executable code, like set comprehensions.
% Using a stepwise refinement approach, we define a series of refined specifications and prove that each specification is equivalent to the previous one. 
% \peter{Can you describe what is a step-wise refinement approach, at least from high-level?}
% The refinements implement efficient algorithms for the abstractly specified operations,  (e.g.\ quicksort for element distinctness checks) replace non-executable by efficiently executable data structures, (e.g.\ red-black-trees for various maps) and add human-readable error messages.
% 
% A crucial refinement is to summarise the enabledness check and the execution of an action.
% This way, we need to resolve the action only once. Moreover, when executing actions, we have already checked that the problem is well-formed.
% Thus, the well well-formedness check for an action reduces to checking that the arguments match the formal parameter types:
% 
% {\footnotesize\noindent
% \isacommand{definition}\isamarkupfalse%
% \ {\isachardoublequoteopen}en{\isacharunderscore}exE{\isadigit{2}}\ mp\ {\isasymequiv}\ {\isasymlambda}{\isacharparenleft}PAction\ n\ args{\isacharparenright}\ {\isasymRightarrow}\ {\isasymlambda}s{\isachardot}\ do\ {\isacharbraceleft}\isanewline
% \ \ \ \ a\ {\isasymleftarrow}\ resolve{\isacharunderscore}action{\isacharunderscore}schemaE\ n{\isacharsemicolon}\isanewline
% \ \ \ \ check\ {\isacharparenleft}action{\isacharunderscore}params{\isacharunderscore}match{\isadigit{2}}\ mp\ a\ args{\isacharparenright}\ {\isacharparenleft}ERRS {\isacharprime}{\isacharprime}Args{\isacharprime}{\isacharprime}{\isacharparenright}{\isacharsemicolon}\isanewline
% \ \ \ \ let\ ai\ {\isacharequal}\ instantiate{\isacharunderscore}action{\isacharunderscore}schema\ a\ args{\isacharsemicolon}\isanewline
% \ \ \ \ check\ {\isacharparenleft}holds\ s\ {\isacharparenleft}precondition\ ai{\isacharparenright}{\isacharparenright}\ {\isacharparenleft}ERRS {\isacharprime}{\isacharprime}Precond{\isacharprime}{\isacharprime}{\isacharparenright}{\isacharsemicolon}\isanewline
% \ \ \ \ Error{\isacharunderscore}Monad{\isachardot}return\ {\isacharparenleft}apply{\isacharunderscore}effect\ {\isacharparenleft}effect\ ai{\isacharparenright}\ s{\isacharparenright}{\isacharbraceright}{\isachardoublequoteclose}
% }
% 
% This algorithm is phrased in the error monad, using a Haskell like do-notation. 
% An error monad execution will either return a result \fquot{Inr res} or an error message \fquot{Inl msg}.
% \peter{Maybe explain a little more about sum types and their constructors?}
% The \fquot{check} function checks that the first argument is true, and otherwise raises the indicated error message.
% 
% \noindent Correctness of this refinement is stated as follows:
% 
% {\footnotesize\noindent
% \isacommand{lemma}\isamarkupfalse%
% \ {\isachardoublequoteopen}{\isasymlbrakk}{\isasymforall}x{\isasymin}s{\isachardot}\ is{\isacharunderscore}Atom\ x{\isasymrbrakk}\ {\isasymLongrightarrow}\ \isanewline
% \ \ en{\isacharunderscore}exE{\isadigit{2}}\ mp{\isacharunderscore}objT\ a\ s\ {\isacharequal}\ Inr\ s{\isacharprime}\ \isanewline
% \ \ {\isasymlongleftrightarrow}\ plan{\isacharunderscore}action{\isacharunderscore}enabled\ a\ s\ {\isasymand}\ s{\isacharprime}\ {\isacharequal}\ execute{\isacharunderscore}plan{\isacharunderscore}action\ a\ s{\isachardoublequoteclose}
% }
% 
% \noindent Note that we assume here that the world model only contains atomic formulas, which we can easily prove as an invariant of our execution function.
% 
% For the data-structure refinements, we use the Containers Framework~\cite{lochbihler2013light}, which provides verified container data structures.
% For example, the \fquot{mp} parameter of \fquot{en{\isacharunderscore}exE{\isadigit{2}}} is a map from objects to their types.
% The containers framework automatically implements this map by a (verified) red-black tree.
% 
% Another refinement implements the distinctness check by sorting the list, and then checking that it contains no adjacent duplicates (stuttering).
% It is straightforward to prove the following lemma:
% 
% {\footnotesize\noindent
% {\isacommand{lemma}\isamarkupfalse\ [code{\isacharunderscore}unfold]{\isacharcolon}\ distinct\ l\ \isacharequal\ no{\isacharunderscore}stutter\ {\isacharparenleft}quicksort\ l{\isacharparenright}}}
% 
% \noindent
% This also registers this lemma as code-unfold rule, which Isabelle uses to rewrite the specifications before code generation.

Finally, we obtain a refined checker \fquot{check{\isacharunderscore}plan} and prove the following theorem that it is sound w.r.t. the semantics.

\vspace{.7ex}
{\footnotesize\noindent
\isacommand{lemma}\isamarkupfalse\ check{\isacharunderscore}plan{\isacharunderscore}return{\isacharunderscore}iff{\isacharcolon}\isanewline
\ \ {\isachardoublequoteopen}check{\isacharunderscore}plan\ P\ {\isasympi}s\ {\isacharequal}\ Inr\ {\isacharparenleft}{\isacharparenright}\ {\isasymlongleftrightarrow}\ wf{\isacharunderscore}problem\ P\ {\isasymand}\ valid{\isacharunderscore}plan\ P\ {\isasympi}s{\isachardoublequoteclose}}

For a problem $P$ and an action sequence $\pi$s, our executable plan checker \fquot{check\_plan} returns $Inr~()$ if and only if the problem and its domain are well-formed, 
and the plan is valid.\footnote{Note: the problem $P$ is now an explicit parameter.}
Otherwise, it returns $Inl~msg$ with some error message.

During the refinement steps, we prove correct many algorithms and data structures.
However, these details are irrelevant for understanding the final correctness theorem: if one believes that the right hand side of the equivalence, which only depends on the abstract semantics, correctly specifies a well-formed problem and valid plan, and believes in the soundness of Isabelle, 
then this theorem states that the left hand side (the checker) is a plan validator.

% 
% 
% To enhance the performance of our validator, we use a verified red-black tree implementation to represent the state (which is a set of atomic formulae).
% This data refinement is done automatically by Isabelle, using the Containers framework~\cite{lochbihler2013light}.
% We also apply some semi-automatic optimizations, implementing maps from various entities by red-black trees, and using an efficient element distinctness check 
% based on quicksort. Again, all these optimizations are proved correct within Isabelle.
% By doing some profiling to identify hotspots and performance leaks, we were able to reduce the runtime of our initial (list-based) validator by several orders of magnitude.
% 




%


Isabelle/HOL's code generator can generate implementations of computable functions in different languages.
We generate a Standard ML~\cite{milner1997definition} implementation of \fquot{check\_plan} and combine it
with a parser and a command line interface (CLI) to obtain an executable plan validator.
Note that the parser and CLI are trusted parts of the validator, i.e.\ there is no formal correctness proof that the parser actually recognises the desired grammar and produces the correct abstract syntax tree, nor that the CLI correctly forwards the arguments to \fquot{check\_plan} and correctly displays the result.

%However, we have put some effort into a clean and legible implementation of the parser and CLI, such that it can be verified by manual inspection.
% 
% 
% We combine our checker with a parser that we implemented in Standard ML using an open source parser combinator library \footnote{We would refer to its repository in case of acceptance.}.
% We note, however, that parsing is a trusted part of our validator, i.e.\ we have no formal proof that the parser actually recognizes the desired grammar and produces the correct abstract syntax tree.
% However, the parsing combinator approach allows for writing concise, clean, and legible parsers, which can be easily checked manually.
% Finally, we add a command line interface to obtains a fully operational plan validator.

%% Its signature is shown in Figure~\ref{fig:ParsedProbPlanType}.

%% \begin{figure}[!htb]
%% \centering
%% \begin{subfigure}[b]{0.49 \textwidth}
%% \input{ParsedProbPlanType}
%% \caption[ParsedProbPlanType]{\label{fig:ParsedProbPlanType}}
%% \end{subfigure}
%% \begin{subfigure}[b]{0.49 \textwidth}
%% \input{SASPVarParseSpec}
%% \caption[SASPVarParseSpec]{\label{fig:SASPVarParseSpec}}
%% \end{subfigure}
%% \caption{
%%  (a) shows the signature of the function \fquot{verify\_plan}.
%% (b) is the specification of the syntactic structure of a SAS+ variable in terms of parser combinators.
%% \label{fig:smlSnippets}}
%% \end{figure}


%% The remaining part is parsing the generated file with the problem and a plan file that has a sequence of actions.
%% We do that , in which a description of the syntax of the language to parse is specified in terms of parsing primitives, that are themselves Standard ML functions.
%% %For example, the description of the syntax of a generated SAS+ variable is show in Figure~\ref{fig:SASPVarParseSpec}.
%% We note, however, that parsing is a trusted part of our validator, i.e.\ we have no formal proof that the parser actually recognizes the desired grammar and produces the correct abstract syntax tree. However, the parsing combinator approach allows to write concise, clean, and legible parsers, which can be easily checked manually.


\subsection{Empirical Evaluation}
\label{sec:experiments}

Our validator currently supports the PDDL flags :strips, :typing, :negative-preconditions, :disjunctive-preconditions, :equality, :constants, and :action-costs, which are enough to validate the vast majority of the International Planning Competition benchmarks and their solutions.
We use our validator, in addition to VAL and INVAL, to validate PDDL problems and plans from previous International Planning Competitions (IPC).
We validate plans generated by Fast-Downward~\cite{Helmert06}.
Table~\ref{table:ValidatorRuntimes} shows the runtimes of our validator, VAL, and INVAL for different IPC benchmark domains, and for each domain shows how many plans were labelled as valid.
%The domains that we use are: Logistics, Rovers, Visitall, Parking, Tidybot, Hiking, Zeno, Thoughtful, Scanalyzer, Pegsol, Gripper, and Satellite.
%We also use a collection of STRIPS problems that are compilations of open Qualitative Preference Rovers (QP) benchmarks from IPC2006 by~\cite{haslum:07}, to have a total of 2084 problems.
%How many timeout do we have, ho many do they have?
There are two main goals for this experiment.
First, we investigate whether there are differences in the validation outcomes of VAL, INVAL, and our validator, and whether such differences are due to bugs.
%\footnote{Since we focus on benchmarks that support basic features, we are more confident that differences in validation outcomes are due to bugs rather than differences in the interpretation of the semantics.}
For the IPC benchmarks, observed differences were primarily due to segmentation faults by VAL, which happened with 111 instances.
Another difference is that both VAL and INVAL allow an empty list of object declarations of the form \fquot{- type} in the problem,
while our validator expects at least one object, which conforms to Kovacs' grammar. This showed up in a problem from the WoodWorking domain.

% 
% Another difference in validation outcomes is a problem from the domain WoodWorking which, unlike VAL and INVAL, our validator labelled as invalid.
% This is because VAL and INVAL allow an empty list of objects declaration of the form \fquot{- type} in the problem, while our validator would expects at least one object before the ``-'' (i.e. our validator expects \fquot{o1 o2 .. on - type}) which conforms to Kovacs' grammar.
% %% E.g., VAL and our validator (which follow Kovacs' syntax) will parse \fquot{?x?y} as two variables, while INVAL 
% %% requires a space before the second variable (\fquot{?x ?y}).

However, for non-IPC benchmarks, we found bugs in both VAL and INVAL that did not show up in the IPC benchmarks: VAL erroneously identifies distinct atoms during its precondition check.
Consider a domain with a predicate \fquot{P} and an instance of that domain with objects \fquot{OA}, \fquot{OB}, \fquot{O}, and \fquot{AOB}.
An action with precondition \fquot{(P\;\; OA\;\; OB)} will be satisfied by an atom \fquot{(P\;\; O\;\; AOB)} in the state.
Based on that, we construct examples where VAL would report an incorrect plan to be valid.
Another issue with VAL is that it sometimes reports a valid plan to be invalid, if it has an action with no preconditions.
%
INVAL does not terminate for domains with cyclic type dependencies if its type-checker is enabled.
%
% For VAL, we found that it does not properly check arguments passed to a predicates.
% In particular, if an action has the precondition \fquot{(P\;\; OA\;\; OB)} and a state only has the atom \fquot{(P\;\; O\;\; AOB)} in it, then VAL will execute the action as if its precondition is satisfied in the state, even though it is not.
% Based on that, we could construct examples where it would report an incorrect plan to be valid.
% Another issue with VAL is that it would sometimes report a valid plan to be invalid, if it has an action with no preconditions.
%% For INVAL, we found that it does not do any type checking for arguments of predicates, whether they are in the initial state, actions, or in the goal, where we .

The fact that these bugs are not detected via testing on the IPC benchmarks strengthens the argument for the use of formal verification to develop AI planning tools, especially if the main purpose of those tools is to add confidence in the correctness of  plans and planning systems, as is the case with validators.%
\footnote{We note that all of these bugs were reported to the relevant bug trackers.}


% 
% % 
% % in validation outcomes are caused by differences in the ways validators parse problems and plans.
% For instance, we observed that VAL and our validator, which follows Kovacs' PDDL syntax description, accept variable names without a preceding space and consider the \fquot{?} preceding the variable name to be a proper delimiter.
% INVAL on the other hand requires a space as a delimiter before variable names.
% %% The other issue that we observe is that for many of the QP benchmarks, both VAL and INVAL fail to validate those problems and their plans.
% %% INVAL exits without providing an informative error message, and VAL provides a parsing error that is not very informative.\footnote{We did not yet get responses from the authors of both VAL and INVAL as to what could be causing this.}
% %% On the other hand, our validator validates the well-formedness of those benchmarks and their plans.
% Other than those parsing issues, we did not find other discrepancies in the validation outcomes.

\begin{table}
\begin{center}
    \begin{tabularx}{0.45\textwidth}{| l | l | l | X |}
    %p{5cm} |}
    \hline
\input{runtime_table_entries}
         \hline
    \end{tabularx}
    \caption{\label{table:ValidatorRuntimes} A table showing the maximum and minimum runtimes of different validators on instances in different IPC domains and the number of plans labelled as valid.
The column headed by * is the one for our validator.}
\end{center}
\vspace{-4ex}
\end{table}


%% \begin{figure}[!htb]
%% \centering
%%         \includegraphics[width=0.48\textwidth]{experiments/plots/labelled-INVAL-vs-ISABELLE.png}
%%     \caption{\label{fig:invalvsisabelle}Scatter plot of the runtimes of INVAL versus our verified validator.
%% }
%% \end{figure}
%% \begin{figure}[!htb]
%% \centering
%%         \includegraphics[width=0.48\textwidth]{experiments/plots/labelled-ISABELLE-vs-VAL.png}
%%     \caption{\label{fig:isabellevsval}Scatter plot of the runtimes of our verified validator versus VAL.
%% }
%% \end{figure}

The second purpose of our experiments is comparing the performance.
Table~\ref{table:ValidatorRuntimes} shows that our validator is much faster than INVAL on all benchmarked domains.
The runtimes of our validator are even comparable to VAL.
% On the other hand, compared to VAL, the runtimes of our validator are similar, ranging from roughly twice as fast to twice as slow.
% Also, our validator is one order of magnitude worse for the Zeno, Hiking, and Gripper domains, and as bad as two orders of magnitude for the Rovers and Vvisitall domains.
This, however, is rather impressive when compared to other pieces of verified software.
For example, the verified model-checker in~\cite{brunner2016formal} is about 400 times slower than the unverified Spin model-checker~\cite{holzmann1997model}.
This performance is the result of several rounds of profiling to identify hotspots and performance leaks, and adjusting the refinements (and their correctness proofs) accordingly. 


%% This is mainly due to low-level optimisations that are considerably easier to realize in unverified software than in verified one, especially when the unverified programme is implemented in a low-level language like C or C++.

% 
% This is because the unverified programmes can take advantage of low level implementation optimisations (e.g. the verified model-checker in \cite{brunner2016formal} has a 400 slow-down factor compared to the model-checker Spin~\cite{holzmann1997model}).

%\moham{Does VAL really SEGFAULT? Report that!} % Even without using CakeML, our code is almost safe against segfaults!

% THIS SHOULD BE USED IF WE USE CAKEML: VAL seg faults in 5
