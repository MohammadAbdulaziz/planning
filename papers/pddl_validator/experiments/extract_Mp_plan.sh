#!/bin/bash

outName=$1
planName=$2

test -n "$planName" || planName=${outName%.out}.plan

if grep -q 'PLAN FOUND:' $outName; then 
  echo "Extracting plan to $planName"
  if [[ $outName == *"newopen"* ]]; then
    cat $outName | grep STEP | awk '{$1="";$2=""; print $0 }' | sed 's/(/ /g' | sed 's/) /\n/g' | sed 's/)//g' | sed 's/^[ \t]*//g' | sed 's/[ \t]*$//g' >$planName
  else
    cat $outName | grep STEP | awk '{$1="";$2=""; print $0 }' | sed 's/(/ /g' | sed 's/,/ /g' | sed 's/) /\n/g' | sed 's/)//g' | sed 's/^[ \t]*//g' | sed 's/[ \t]*$//g' >$planName
  fi
  exit 0
else
  echo "*** No plan found in $outName"
  exit 1
fi
