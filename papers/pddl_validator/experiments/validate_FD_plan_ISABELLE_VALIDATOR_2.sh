#!/bin/bash

# Validates a FD plan relative to a FD's SAS+ translation

# if [ -f "exp6_data/$3.FDsat.plan.plan" ];
# then
#   exit
# fi
echo $domName
echo $probName
echo $outName

echo 0

if [ -f "exp6_data/$outName.FDsat.plan.ISABELLE_VALIDATOR_2_validity_PDDL" ];
then
  exit
fi

echo 1

if [ -f "exp6_data/$outName.FDsat.plan.out" ];
then
  echo 2
  ./extract_FD_plan.sh  exp6_data/$outName.FDsat.plan.out > /dev/null
  echo "extracred plan"
  if [ -f "exp6_data/$outName.FDsat.plan.plan" ];
  then
    echo 3
    ID1=$RANDOM
    ID2=$RANDOM
    mkdir tmp_${ID1}_${ID2}
    cp -rf ../../../codeBase/planning/pddlParser/pddl_validate_plan tmp_${ID1}_${ID2}
    sed 's/ ([0-9]*)//g' exp6_data/$outName.FDsat.plan.plan | sed  's/^/(/' | sed 's/$/)/' >tmp_${ID1}_${ID2}/plan
    cd tmp_${ID1}_${ID2}/
    { time ./pddl_validate_plan <(tr A-Z a-z < ../$domName) <(tr A-Z a-z < ../$probName) <(tr A-Z a-z < plan) ; } > ../exp6_data/$outName.FDsat.plan.ISABELLE_VALIDATOR_2_validity_PDDL 2>../exp6_data/$outName.FDsat.plan.ISABELLE_VALIDATOR_2_validity_PDDL_time
    cd ..
    rm -rf tmp_${ID1}_${ID2}
    echo "finished with ISABELLE VALIDATOR"
    cat exp6_data/$outName.FDsat.plan.ISABELLE_VALIDATOR_2_validity_PDDL
  fi
fi
