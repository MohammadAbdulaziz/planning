#!/bin/bash
if [ ! -f "exp6_data/$outName.FDsat.plan.out" ]; then

ID1=$RANDOM
ID2=$RANDOM
mkdir tmp_${ID1}_${ID2}
cp -rf ../../../codeBase/planning/FD/ tmp_${ID1}_${ID2}/
cd tmp_${ID1}_${ID2}/FD/src/
./fast-downward.py ../../../$domName ../../../$probName --heuristic 'hlm,hff=lm_ff_syn(lm_rhw(reasonable_orders=true,lm_cost_type=one,cost_type=one))' --search 'lazy_greedy([hff,hlm],preferred=[hff,hlm])' >../../../exp6_data/$outName.FDsat.plan.out 2>../../../exp6_data/$outName.FDsat.plan.err
cd -
rm -rf tmp_${ID1}_${ID2}/
fi
