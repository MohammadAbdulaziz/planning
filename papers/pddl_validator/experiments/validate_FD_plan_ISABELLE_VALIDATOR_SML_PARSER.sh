#!/bin/bash

# Validates a FD plan relative to a FD's SAS+ translation

# if [ -f "exp6_data/$3.FDsat.plan.plan" ];
# then
#   exit
# fi
echo $domName
echo $probName
echo $outName

function fail() {
  echo $1;
  exit 1
}

BASEDIR="$PWD"
DATADIR="$BASEDIR/exp6_data"


export VALIDATOR_SUFFIX="SML_PARSER_REFACTOR"
VALIDATE="$BASEDIR/../../../codeBase/planning/pddlParser/pddl"




RES_VALIDITY_FILE="$DATADIR/$outName.FDsat.plan.ISABELLE_VALIDATOR_${VALIDATOR_SUFFIX}_validity_PDDL"
RES_TIME_FILE="$DATADIR/$outName.FDsat.plan.ISABELLE_VALIDATOR_${VALIDATOR_SUFFIX}_validity_PDDL_time"

echo 0

if [ -f "$RES_VALIDITY_FILE" -a -z "$ALWAYS_REBUILD"  ];
then
  fail "Already have results for $outName, skipping"
fi

echo 1


FD_OUT="exp6_data/$outName.FDsat.plan.out"
PLAN="exp6_data/$outName.FDsat.plan.plan"

if [ -f "$FD_OUT" ];
then
  ./extract_FD_plan.sh  "$FD_OUT" "$PLAN" > /dev/null
  
  test -f "$PLAN" || fail "No plan produced by extract_FD_plan ???"
  echo "extracted plan"
else
  fail "No such FD-output: $FD_OUT"
fi

    
WORKDIR="res/$VALIDATOR_SUFFIX/$outName"
mkdir -p "$WORKDIR"
    
cat $BASEDIR/$domName | tr A-Z a-z >$WORKDIR/domain
cat $BASEDIR/$probName | tr A-Z a-z >$WORKDIR/prob
sed 's/ ([0-9]*)//g;s/^/(/;s/$/)/' $BASEDIR/exp6_data/$outName.FDsat.plan.plan >$WORKDIR/plan

echo "Invoke validator"
cd $WORKDIR
{ /usr/bin/time -f 'real %e' $VALIDATE domain prob plan ; } >$RES_VALIDITY_FILE 2>$RES_TIME_FILE
cp $RES_VALIDITY_FILE result
cp $RES_TIME_FILE time
cd $BASEDIR    

echo "finished with ISABELLE VALIDATOR"
cat $RES_VALIDITY_FILE

