(define (problem prob)
 (:domain Dom)
 (:objects 
     O1 - T1
     O2 - T2
)
 (:init (P O1))
 (:goal
  (and
     (P O1))))
