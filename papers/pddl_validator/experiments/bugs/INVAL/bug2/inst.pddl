(define (problem prob)
 (:domain Dom)
 (:objects 
     O1 - T1
     O2 - Ta
)
 (:init 
   (and (P1 O2))
)
 (:goal
  (and
     (P1 O1))))
