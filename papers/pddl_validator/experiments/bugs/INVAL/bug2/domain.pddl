(define (domain Dom)
  (:requirements :strips :typing)
  (:types T1 Ta - object
  	  T2 - T1
          T1 - T2
  	  Tb - Ta
          Ta - Tb)
  (:predicates  (P1 ?c - T1)
                (P2 ?c - Ta))
		
  (:action A1
             :parameters (V1 - T1_1)
             :effect (P1 V1))
)