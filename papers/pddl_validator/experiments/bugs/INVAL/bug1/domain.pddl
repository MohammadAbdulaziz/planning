(define (domain Dom)
  (:requirements :strips :typing)
  (:types T1 T2 - object
  	  T1_1 - T1
          T2_1 - T2)
  (:predicates  (P1 ?c - T1_1)
                (P2 ?c - T2_1))
		
  (:action A1
             :parameters (?V1 - T1_1)
             :effect (P1 ?V1))
)