(define (problem prob)
 (:domain Dom)
 (:objects 
     O1 - T1_1
     O2 - T2_1
)
 (:init 
   (and (P1 O2))
)
 (:goal
  (and
     (P1 O1))))
