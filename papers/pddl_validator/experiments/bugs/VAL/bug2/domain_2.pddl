(define (domain barman)
  (:requirements :strips)
  (:predicates  (P1 ?c1 ?c2)
                (P2 ?c1 ?c2))
		
  (:action A1 :parameters (?V1 ?V2)
             :precondition (and (P1 ?V1 ?V2))
             :effect (and (P2 ?V1 ?V2)))
)