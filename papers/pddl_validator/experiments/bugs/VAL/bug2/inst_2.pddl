(define (problem prob)
 (:domain barman)
 (:objects 
     OA OB O AOB
)
 (:init 
   (P1 O AOB)
)
 (:goal
  (and
     (P2 OA OB))))
