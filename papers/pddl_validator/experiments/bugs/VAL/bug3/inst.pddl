(define (problem prob)
 (:domain barman)
 (:objects 
     O1 - T1
)
 (:init 
  (P O1)
)
 (:goal
  (and
     (P O1)
)))
