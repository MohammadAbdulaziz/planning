(define (domain barman)
  (:requirements :strips :typing)
  (:types T1 - object)
  (:predicates  (P ?c - T1))
		
  (:action A1
             :parameters (?c -  T1)
             ;;:precondition ()
             :effect (and (P ?c)))


 )