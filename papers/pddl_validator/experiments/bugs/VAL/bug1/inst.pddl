(define (problem prob)
 (:domain Dom)
 (:objects 
     OA OB
)
 (:init 
   (and (P1 OA OB))
)
 (:goal
  (and
     (P2 OA OB))))
