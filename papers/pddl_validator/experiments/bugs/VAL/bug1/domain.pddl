(define (domain Dom)
  (:requirements :strips :typing)
  (:predicates  (P1 ?o1 ?o2)
                (P2 ?o1 ?o2))
		
  (:action A1
             :parameters (?o1 ?o2 - object)
             :precondition (P1 ?o1 ?o2)
             :effect (P2 ?o1 ?o2))
)