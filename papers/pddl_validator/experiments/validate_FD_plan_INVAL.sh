#!/bin/bash

# Validates a FD plan using INVAL, by Haslum

# if [ -f "exp6_data/$3.FDsat.plan.plan" ];
# then
#   exit
# fi
echo $domName
echo $probName
echo $outName
#INVAL_COMM=../../../codeBase/planning/INVAL/inval -c
INVAL_COMM=../../../codeBase/planning/INVAL/inval
if [ -f "exp6_data/$outName.FDsat.plan.INVAL_validity_PDDL" ];
then
  exit
fi

echo "passed first if"

if [ -f "exp6_data/$outName.FDsat.plan.out" ];
then
  echo "passed 2nd if"
  ./extract_FD_plan.sh  exp6_data/$outName.FDsat.plan.out > /dev/null
  echo "extracred plan"
  if [ -f "exp6_data/$outName.FDsat.plan.plan" ];
  then
    echo "passed 3rd if"
    # { time ../../../codeBase/planning/INVAL/inval -c $domName $probName <(./FD_plan_to_IPC_plan.sh exp6_data/$outName.FDsat.plan.plan) ; } > exp6_data/$outName.FDsat.plan.INVAL_validity_PDDL 2>exp6_data/$outName.FDsat.plan.INVAL_validity_PDDL_time
    { time $INVAL_COMM  $domName $probName <(./FD_plan_to_IPC_plan.sh exp6_data/$outName.FDsat.plan.plan) ; } > exp6_data/$outName.FDsat.plan.INVAL_validity_PDDL 2>exp6_data/$outName.FDsat.plan.INVAL_validity_PDDL_time
    echo "finished with INVAL"
    cat exp6_data/$outName.FDsat.plan.INVAL_validity_PDDL
    # ID1=$RANDOM
    # ID2=$RANDOM
    # mkdir tmp_${ID1}_${ID2}
    # cp -rf ../../../codeBase/planning/INVAL//* tmp_${ID1}_${ID2}
    # sed 's/ ([0-9]*)//g' exp6_data/$outName.FDsat.plan.plan | sed  's/^/(/' | sed 's/$/)/' >tmp_${ID1}_${ID2}/plan
    # cd tmp_${ID1}_${ID2}/
    # { time ./inval ../$domName ../$probName plan ; } > ../exp6_data/$outName.FDsat.plan.INVAL_validity_PDDL 2>../exp6_data/$outName.FDsat.plan.INVAL_validity_PDDL_time
    # cd ..
    # rm -rf tmp_${ID1}_${ID2}
    # echo "finished with INVAL"
    # cat exp6_data/$outName.FDsat.plan.INVAL_validity_PDDL
  fi
fi
