#!/bin/bash

plan=$1
basename=${plan%.plan}
logname="$basename.vrf.log"

domain="$basename.dom.pddl"
problem="$basename.prob.pddl"

TIME=`which time`

if test -f "$logname" -a ! \( "$plan" -nt "$logname" \) && grep -q '^s VERIFIED$' "$logname"; then
  echo "Skipping $basename: already verified"
else
  date > $logname
  echo "Verifying $basename" | tee -a $logname
  echo "Checksums" >> $logname
  md5sum "$domain" "$problem" "$plan" >> $logname
  $TIME PDDL_Checker "$domain" "$problem" "$plan" >> $logname 2>&1
  grep "^s " $logname || echo "No result output by validator"
fi
