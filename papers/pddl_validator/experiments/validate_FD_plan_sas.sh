#!/bin/bash

# Validates a FD plan relative to a FD's SAS+ translation

# if [ -f "exp6_data/$3.FDsat.plan.plan" ];
# then
#   exit
# fi


if [ -f "exp6_data/$3.FDsat.plan.sas_validity" ];
then
  exit
fi

if [ -f "exp6_data/$3.FDsat.plan.out" ];
then
  ID=$RANDOM
  mkdir sas_$ID
  cd sas_$ID
  ../../../../codeBase/planning/FD/src/translate/translate.py ../$1 ../$2 > /dev/null
  cd ../
  ./extract_FD_plan.sh  exp6_data/$3.FDsat.plan.out > /dev/null
  if [ -f "exp6_data/$3.FDsat.plan.plan" ];
  then
    ./strips sas_$ID/output.sas <(sed 's/ ([0-9]*)//g' exp6_data/$3.FDsat.plan.plan) | grep -i "valid plan" > exp6_data/$3.FDsat.plan.sas_validity
    cat exp6_data/$3.FDsat.plan.sas_validity
  fi
  rm -rf sas_$ID
fi
