#!/bin/bash

outName=$1
planName=$2

test -n "$planName" || planName=${outName%.out}.plan

if grep -q 'Solution found!' $outName; then 
  echo "Extracting plan to $planName"
  egrep '^([A-Za-z][A-Za-z0-9_\,-]* +)+ *\([0-9]+\)$' $outName \
  | sed -re 's/^/\(/;s/ *\([0-9]+\)$//;s/$/\)/' >$planName
  exit 0
else
  echo "*** No plan found in $outName"
  exit 1
fi
