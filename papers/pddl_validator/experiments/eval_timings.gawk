#!/usr/bin/gawk -f


$1=="NEW" {
  newtime[$2] = $3
  newsucc[$2] = ($4=="true")
}

$1=="OLD" {
  oldtime[$2] = $3
  oldsucc[$2] = ($4=="true")
}

END {
  for (n in oldtime) {
    printf("%s %s %s %s %s", n , oldtime[n] , newtime[n] , oldsucc[n] , newsucc[n])
    
    if (oldsucc[n] && !newsucc[n]) {
      printf("*** COMPL-DEGR ***")
    }
    
    if (oldsucc[n] && oldtime[n]<newtime[n]) {
      printf("*** PERF-DEGR ***")
    }
    
    print ""
  }
}


