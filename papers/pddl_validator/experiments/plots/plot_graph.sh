#!/bin/bash
#Putting all bounds in a csv file

rm INVAL_runtimes_assorted
rm VAL_runtimes_assorted
rm ISABELLE_runtimes_assorted
rm ISABELLE_2_runtimes_assorted
rm *_INVAL_runtimes_assorted
rm *_VAL_runtimes_assorted
rm *_ISABELLE_runtimes_assorted
rm *_ISABELLE_2_runtimes_assorted
 
 

# # # This loop produces all data points in runtimes_stats files

echo "NEED TO INCLUDE FAILED PROBS (INVALID PLANS)"

for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
  echo $dom
  for file in `ls ../exp6_data/*FDsat*.INVAL*_time |  grep -i $dom | grep -v -f doms_ISABELLE_VALIDATOR_NOT_WORK`; do
    if [ -s $file ]; then
      if grep -q "plan valid!" ${file%_time} ; then
        echo -n ${file%.plan.INVAL_validity_PDDL_time}
        echo -n " "
        echo `cat $file | grep user | gawk '{print $2}' | sed 's/s//g' | sed s/m/*60+/g | bc`
      fi;
    fi;
  done | sed 's/..\/exp6_data\///g' > ${dom}_INVAL_runtimes_assorted
  cat ${dom}_INVAL_runtimes_assorted >> INVAL_runtimes_assorted
done

for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
  echo $dom
  for file in `ls ../exp6_data/*FDsat*.VAL*_time |  grep -i $dom | grep -v -f doms_ISABELLE_VALIDATOR_NOT_WORK`; do
    if [ -s $file ]; then
      if grep -q "Plan valid" ${file%_time} ; then
        echo -n ${file%.plan.VAL_validity_PDDL_time}
        echo -n " "
        echo `cat $file | grep user | gawk '{print $2}' | sed 's/s//g' | sed s/m/*60+/g | bc`
      fi
    fi;
  done | sed 's/..\/exp6_data\///g' > ${dom}_VAL_runtimes_assorted
  cat ${dom}_VAL_runtimes_assorted >> VAL_runtimes_assorted
done

for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
  echo $dom
  for file in `ls ../exp6_data/*FDsat*.ISABELLE_VALIDATOR_validity_PDDL_time |  grep -i $dom | grep -v -f doms_ISABELLE_VALIDATOR_NOT_WORK`; do
    if [ -s $file ]; then
      if ! grep -q "s ERROR: Error parsing domain" ${file%_time} ; then
        echo -n ${file%.plan.ISABELLE_VALIDATOR_validity_PDDL_time}
        echo -n " "
        echo `cat $file | grep user | gawk '{print $2}' | sed 's/s//g' | sed s/m/*60+/g | bc`
      fi
    fi;
  done | sed 's/..\/exp6_data\///g' > ${dom}_ISABELLE_runtimes_assorted
  cat ${dom}_ISABELLE_runtimes_assorted >> ISABELLE_runtimes_assorted
done

for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
  echo $dom
  for file in `ls ../exp6_data/*FDsat*.ISABELLE_VALIDATOR_2_validity_PDDL_time | grep -i $dom | grep -v -f doms_ISABELLE_VALIDATOR_NOT_WORK`; do
    if [ -s $file ]; then
      if grep -q "Valid Plan" ${file%_time} ; then
        echo -n ${file%.plan.ISABELLE_VALIDATOR_2_validity_PDDL_time}
        echo -n " "
        echo `cat $file | grep user | gawk '{print $2}' | sed 's/s//g' | sed s/m/*60+/g | bc`
      fi
    fi;
  done | sed 's/..\/exp6_data\///g' > ${dom}_ISABELLE_2_runtimes_assorted
  cat ${dom}_ISABELLE_2_runtimes_assorted >> ISABELLE_2_runtimes_assorted
done

rm INVAL_failed_doms
for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
  echo $dom
  for file in `grep -l "Valid" ../exp6_data/*.ISABELLE_VALIDATOR_2_validity_PDDL | grep -i $dom | grep -v -f doms_ISABELLE_VALIDATOR_NOT_WORK `; do
    grep -L "plan valid" ${file%.ISABELLE_VALIDATOR_2_validity_PDDL}.INVAL_validity_PDDL;
  done  >> INVAL_failed_doms
  
done

rm VAL_failed_doms
for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
  echo $dom
  for file in `grep -l "Valid" ../exp6_data/*.ISABELLE_VALIDATOR_2_validity_PDDL | grep -i $dom | grep -v -f doms_ISABELLE_VALIDATOR_NOT_WORK `; do
    grep -L "Plan valid" ${file%.ISABELLE_VALIDATOR_2_validity_PDDL}.VAL_validity_PDDL;
  done  >> VAL_failed_doms
  
done

join <(sort -k1,1 INVAL_runtimes_assorted | awk '$2>0') <(sort -k1,1 VAL_runtimes_assorted | awk '$2>0') > INVAL_VAL_runtimes_assorted
join <(sort -k1,1 INVAL_runtimes_assorted | awk '$2>0') <(sort -k1,1 ISABELLE_runtimes_assorted | awk '$2>0') > INVAL_ISABELLE_runtimes_assorted
join <(sort -k1,1 ISABELLE_runtimes_assorted | awk '$2>0') <(sort -k1,1 VAL_runtimes_assorted | awk '$2>0') > ISABELLE_VAL_runtimes_assorted
join <(sort -k1,1 INVAL_runtimes_assorted | awk '$2>0') <(sort -k1,1 ISABELLE_2_runtimes_assorted | awk '$2>0') > INVAL_ISABELLE_2_runtimes_assorted
join <(sort -k1,1 ISABELLE_2_runtimes_assorted | awk '$2>0') <(sort -k1,1 VAL_runtimes_assorted | awk '$2>0') > ISABELLE_2_VAL_runtimes_assorted
join <(sort -k1,1 ISABELLE_2_runtimes_assorted | awk '$2>0') <(sort -k1,1 ISABELLE_runtimes_assorted | awk '$2>0') > ISABELLE_2_ISABELLE_runtimes_assorted

  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set size square
  set output "INVAL-vs-VAL.png"
  set xlabel "INVAL Runtimes (sec)"
  show xlabel
  set ylabel "VAL Runtimes (sec)"
  show ylabel
  set key off
  plot 'INVAL_VAL_runtimes_assorted' using 2:3
text

  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set size square
  set output "INVAL-vs-ISABELLE.png"
  set xlabel "INVAL Runtimes (sec)"
  show xlabel
  set ylabel "Verified Validator Runtimes (sec)"
  show ylabel
  set key off
  plot 'INVAL_ISABELLE_runtimes_assorted' using 2:3
text


  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set size square
  set output "INVAL-vs-ISABELLE-2.png"
  set xlabel "INVAL Runtimes (sec)"
  show xlabel
  set ylabel "Verified Validator Runtimes (sec)"
  show ylabel
  set key off
  plot 'INVAL_ISABELLE_2_runtimes_assorted' using 2:3
text

  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set size square
  set output "ISABELLE-vs-VAL.png"
  set xlabel "Verified Validator Runtimes (sec)"
  show xlabel
  set ylabel "VAL Runtimes (sec)"
  show ylabel
  set key off
  plot 'ISABELLE_VAL_runtimes_assorted' using 2:3
text

  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set size square
  set output "ISABELLE-2-vs-VAL.png"
  set xlabel "Verified Validator Runtimes (sec)"
  show xlabel
  set ylabel "VAL Runtimes (sec)"
  show ylabel
  set key off
  plot 'ISABELLE_2_VAL_runtimes_assorted' using 2:3
text

  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set size square
  set output "ISABELLE-2-vs-ISABELLE.png"
  set xlabel "Verified Validator 2 Runtimes(sec)"
  show xlabel
  set ylabel "Verified Validator 1 Runtimes(sec)"
  show ylabel
  set key off
  plot 'ISABELLE_2_ISABELLE_runtimes_assorted' using 2:3
text

rm runtime_table.tex
echo "\begin{table}">> runtime_table.tex
echo "\begin{center}">> runtime_table.tex
echo "    \begin{tabularx}{0.45\textwidth}{| l | X | X | X |}">> runtime_table.tex
echo "    %p{5cm} |}">> runtime_table.tex
echo "    \hline">> runtime_table.tex
echo "          {\scriptsize }       &{\scriptsize VAL}       & {\scriptsize INVAL}       & {\scriptsize Isabelle/HOL} \\" >> runtime_table.tex
for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
  echo $dom
    cat ${dom}_INVAL_runtimes_assorted | awk '{print $2}' | sort -n > ${dom}_INVAL_runtimes
    cat ${dom}_VAL_runtimes_assorted  | awk '{print $2}'  | sort -n > ${dom}_VAL_runtimes
    cat ${dom}_ISABELLE_runtimes_assorted  | awk '{print $2}'  | sort -n > ${dom}_ISABELLE_runtimes
    cat ${dom}_ISABELLE_2_runtimes_assorted  | awk '{print $2}'  | sort -n > ${dom}_ISABELLE_2_runtimes
    echo -n "{\scriptsize ">> runtime_table.tex
    echo -n  ${dom} >> runtime_table.tex
    echo -n "}         &{\scriptsize ">> runtime_table.tex
    echo -n $(head -n 1 ${dom}_VAL_runtimes)>> runtime_table.tex
    echo -n "/" >> runtime_table.tex
    echo -n `awk '/./{line=$0} END{print line}' ${dom}_VAL_runtimes` >> runtime_table.tex
    echo -n \(`wc -l < ${dom}_VAL_runtimes`\) >> runtime_table.tex
    echo -n  >> runtime_table.tex
    echo -n "}        & {\scriptsize ">> runtime_table.tex
    echo -n $(head -n 1 ${dom}_INVAL_runtimes)>> runtime_table.tex
    echo -n "/" >> runtime_table.tex
    echo -n `awk '/./{line=$0} END{print line}' ${dom}_INVAL_runtimes` >> runtime_table.tex
    echo -n \(`wc -l < ${dom}_INVAL_runtimes`\) >> runtime_table.tex
    echo -n "}          & {\scriptsize " >> runtime_table.tex
    echo -n $(head -n 1 ${dom}_ISABELLE_2_runtimes)>> runtime_table.tex
    echo -n "/" >> runtime_table.tex
    echo -n `awk '/./{line=$0} END{print line}' ${dom}_ISABELLE_2_runtimes` >> runtime_table.tex
    echo -n \(`wc -l < ${dom}_ISABELLE_2_runtimes`\) >> runtime_table.tex
    echo -n "} \\\\ ">> runtime_table.tex
    echo " " >> runtime_table.tex
done

for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
  echo $dom
    join <(sort -k1,1 ${dom}_INVAL_runtimes_assorted | awk '$2>0') <(sort -k1,1 ${dom}_VAL_runtimes_assorted | awk '$2>0') > ${dom}_INVAL_VAL_runtimes_assorted
    join <(sort -k1,1 ${dom}_INVAL_runtimes_assorted | awk '$2>0') <(sort -k1,1 ${dom}_ISABELLE_runtimes_assorted | awk '$2>0') > ${dom}_INVAL_ISABELLE_runtimes_assorted
    join <(sort -k1,1 ${dom}_ISABELLE_runtimes_assorted | awk '$2>0') <(sort -k1,1 ${dom}_VAL_runtimes_assorted | awk '$2>0') > ${dom}_ISABELLE_VAL_runtimes_assorted
    join <(sort -k1,1 ${dom}_INVAL_runtimes_assorted | awk '$2>0') <(sort -k1,1 ${dom}_ISABELLE_2_runtimes_assorted | awk '$2>0') > ${dom}_INVAL_ISABELLE_2_runtimes_assorted
    join <(sort -k1,1 ${dom}_ISABELLE_2_runtimes_assorted | awk '$2>0') <(sort -k1,1 ${dom}_VAL_runtimes_assorted | awk '$2>0') > ${dom}_ISABELLE_2_VAL_runtimes_assorted
    join <(sort -k1,1 ${dom}_ISABELLE_2_runtimes_assorted | awk '$2>0') <(sort -k1,1 ${dom}_ISABELLE_runtimes_assorted | awk '$2>0') > ${dom}_ISABELLE_2_ISABELLE_runtimes_assorted
    # join <(sort -k1,1 ${dom}_INVAL_runtimes_assorted) <(sort -k1,1 ${dom}_runtimesnosnapshots_assorted) | tr -d "\r" | awk '$2>0' |  awk '$12>0' > ${dom}_runtimeswithsnapshots_nosnapshots_joined
done

min_data_points=1;

#Plotting integrated and labelled runtimes

  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set size square
  set output "labelled-INVAL-vs-VAL.png"
  set key off
  key_words_gnuplot_var = system('./print_domains_leq_datapts.sh _INVAL_VAL_runtimes_assorted ${min_data_points}')
  #set xrange [1:200]
  #set yrange [10:5000]
  set xlabel "INVAL Runtimes (sec)"
  show xlabel
  set ylabel "VAL Runtimes (sec)"
  show ylabel
  set key outside
  #plot x
  set logscale x 10
  set logscale y 10
  plot for [filename in key_words_gnuplot_var] filename. "_INVAL_VAL_runtimes_assorted" using 2:3 title filename, x
text

convert -trim labelled-INVAL-vs-VAL.png labelled-INVAL-vs-VAL.png


  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set size square
  set output "labelled-INVAL-vs-ISABELLE.png"
  set key off
  key_words_gnuplot_var = system('./print_domains_leq_datapts.sh _INVAL_ISABELLE_runtimes_assorted ${min_data_points}')
  # set xrange [1:200]
  # set yrange [10:5000]
  set xlabel "INVAL Runtimes (sec)"
  show xlabel
  set ylabel "Verified Validator Runtimes (sec)"
  show ylabel
  set key outside
  set yrange [0.001:3]
  set xrange [0.01:100]
  plot x, for [filename in key_words_gnuplot_var] filename. "_INVAL_ISABELLE_runtimes_assorted" using 2:3 title filename
text

convert -trim labelled-INVAL-vs-ISABELLE.png labelled-INVAL-vs-ISABELLE.png

  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set size square
  set output "labelled-INVAL-vs-ISABELLE-2.png"
  set key off
  key_words_gnuplot_var = system('./print_domains_leq_datapts.sh _INVAL_ISABELLE_2_runtimes_assorted ${min_data_points}')
  # set xrange [1:200]
  # set yrange [10:5000]
  set xlabel "INVAL Runtimes (sec)"
  show xlabel
  set ylabel "Verified Validator Runtimes (sec)"
  show ylabel
  set key outside
  set yrange [0.001:3]
  set xrange [0.01:100]
  plot x, for [filename in key_words_gnuplot_var] filename. "_INVAL_ISABELLE_2_runtimes_assorted" using 2:3 title filename
text

convert -trim labelled-INVAL-vs-ISABELLE-2.png labelled-INVAL-vs-ISABELLE-2.png

  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set size square
  set output "labelled-ISABELLE-vs-VAL.png"
  set key off
  key_words_gnuplot_var = system('./print_domains_leq_datapts.sh _ISABELLE_VAL_runtimes_assorted ${min_data_points}')
  # set xrange [1:200]
  # set yrange [10:5000]
  set xlabel "Verified Validator Runtimes (sec)"
  show xlabel
  set ylabel "VAL Runtimes (sec)"
  show ylabel
  set key outside
  set yrange [0.001:2]
  set xrange [0.001:3]
  plot x, for [filename in key_words_gnuplot_var] filename. "_ISABELLE_VAL_runtimes_assorted" using 2:3 title filename
text

convert -trim labelled-ISABELLE-vs-VAL.png labelled-ISABELLE-vs-VAL.png


  gnuplot << text
  set terminal pngcairo size 1280,960 font ",12"
  set logscale x 10
  set logscale y 10
  set size square
  set output "labelled-ISABELLE-2-vs-VAL.png"
  set key off
  key_words_gnuplot_var = system('./print_domains_leq_datapts.sh _ISABELLE_2_VAL_runtimes_assorted ${min_data_points}')
  # set xrange [1:200]
  # set yrange [10:5000]
  set xlabel "Verified Validator Runtimes (sec)"
  show xlabel
  set ylabel "VAL Runtimes (sec)"
  show ylabel
  set key outside
  set yrange [0.001:2]
  set xrange [0.001:3]
  plot x, for [filename in key_words_gnuplot_var] filename. "_ISABELLE_2_VAL_runtimes_assorted" using 2:3 title filename
text

convert -trim labelled-ISABELLE-2-vs-VAL.png labelled-ISABELLE-2-vs-VAL.png

# convert -trim labelled-atom-dom-withsnapshot-vs-atom-dom-nosnapshot.png labelled-atom-dom-withsnapshot-vs-atom-dom-nosnapshot.png

