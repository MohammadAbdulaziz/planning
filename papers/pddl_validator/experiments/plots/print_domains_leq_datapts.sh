#!/bin/bash
for dom in `cat dom_keywords_no_IPC_no_SATUNSAT`; do
  for file in `ls ${dom}$1`; do
    if [ `wc -l $file | gawk '{print $1}'` -gt $2 ]; then
      wc -l $file
    fi
  done
done | sort -nr -k1,1 | gawk '{print $2}' | sed 's/'"$1"'//g'
