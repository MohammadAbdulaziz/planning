 #! /bin/bash
#benchmarks_path=~/planning/planningBenchmarks/
#rm submittedProbs
benchmarks_path=../../../planningBenchmarks/
# cp ../../../codeBase/planning/stripsParser/strips .
#rm exp_results

# for problem in `ls ~/planning/papers/ITP2015/experiments/generatedHotelkeyInstancesSafe/ | grep .pddl | grep -v _aux`; do

# for problem in `ls ${benchmarks_path}/unsolvable_IPC/diagnosis/appn/ | grep .pddl | grep -v domain.pddl`; do
#     echo unsolve_diag_appn_$problem>>submittedProbs
#     ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/diagnosis/appn/domain.pddl ${benchmarks_path}/unsolvable_IPC/diagnosis/appn/$problem unsolve_diag_appn_$problem
# done

# for problem in `ls ${benchmarks_path}/unsolvable_IPC/diagnosis/appn/pstrips/ | grep domain\.pddl`; do
#     echo unsolve_diag_appn_strips_$problem>>submittedProbs
#     ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/diagnosis/appn/pstrips/$problem ${benchmarks_path}/unsolvable_IPC/diagnosis/appn/pstrips/${problem%.domain.pddl}.problem.pddl unsolve_diag_appn_strips_$problem
# done

# for problem in `ls ${benchmarks_path}/unsolvable_IPC/diagnosis/witas/ | grep .pddl | grep -v domain.pddl`; do
#     echo unsolve_diag_witas_$problem>>submittedProbs
#     ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/diagnosis/witas/domain.pddl ${benchmarks_path}/unsolvable_IPC/diagnosis/witas/$problem unsolve_diag_witas_$problem
# done

# for problem in `ls ${benchmarks_path}/unsolvable_IPC/diagnosis/witas/pstrips/ | grep domain\.pddl`; do
#     echo unsolve_diag_witas_strips_$problem>>submittedProbs
#     ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/diagnosis/witas/pstrips/$problem ${benchmarks_path}/unsolvable_IPC/diagnosis/witas/pstrips/${problem%.domain.pddl}.problem.pddl unsolve_diag_witas_strips_$problem
# done

# for problem in `ls ${benchmarks_path}/unsolvable_IPC/FINAL/diagnosis/ | grep satdom`; do
#     echo unsolve_final_diag_$problem>>submittedProbs
#     ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/FINAL/diagnosis/$problem ${benchmarks_path}/unsolvable_IPC/FINAL/diagnosis/satprob${problem#satdom} unsolve_final_diag_$problem
# done

# for problem in `ls ${benchmarks_path}/unsolvable_IPC/FINAL/diagnosis/ | grep unknown | grep dom`; do
#     echo unsolve_final_diag_$problem>>submittedProbs
#     ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/FINAL/diagnosis/$problem ${benchmarks_path}/unsolvable_IPC/FINAL/diagnosis/unknownprob${problem#unknowndom} unsolve_final_diag_$problem
# done

# for problem in `ls ${benchmarks_path}/unsolvable_IPC/FINAL/diagnosis/ | grep -v satdom | grep -v unknown | grep dom`; do
#     echo unsolve_final_diag_$problem>>submittedProbs
#     ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/FINAL/diagnosis/$problem ${benchmarks_path}/unsolvable_IPC/FINAL/diagnosis/prob${problem#dom} unsolve_final_diag_$problem
# done

# for domain in chessboard-pebbling over-tpp over-nomystery bag-gripper bottleneck tetris pegsol over-rovers sliding-tiles pegsol-row5; do
#   for problem in `ls ${benchmarks_path}/unsolvable_IPC/FINAL/${domain}/ | grep prob`; do
#       echo unsolve_final_${domain}_${problem}>>submittedProbs
#       ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/FINAL/${domain}/domain.pddl ${benchmarks_path}/unsolvable_IPC/FINAL/${domain}/${problem} unsolve_final_${domain}_${problem}
#   done
# done

# for domain in bag-transport bag-barman cave-diving; do
#   for problem in `ls ${benchmarks_path}/unsolvable_IPC/FINAL/${domain}/ | grep -iv sat | grep dom`; do
#       echo unsolve_final_${domain}_satprob${problem#dom}>>submittedProbs
#       ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/FINAL/${domain}/$problem ${benchmarks_path}/unsolvable_IPC/FINAL/bag-transport/satprob${problem#dom} unsolve_final_${domain}_satprob${problem#dom}
#       echo unsolve_final_${domain}_prob${problem#dom}>>submittedProbs
#       ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/FINAL/${domain}/$problem ${benchmarks_path}/unsolvable_IPC/FINAL/bag-transport/prob${problem#dom} unsolve_final_${domain}_prob${problem#dom}
#   done
# done

# for problem in `ls ${benchmarks_path}/unsolvable_IPC/FINAL/document-transfer/ | grep satprob`; do
#     echo unsolve_doc_transfer_$problem>>submittedProbs
#     ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/FINAL/document-transfer/domain.pddl ${benchmarks_path}/unsolvable_IPC/FINAL/document-transfer/${problem} unsolve_doc_transfer_$problem
# done

# for problem in `ls ${benchmarks_path}/unsolvable_IPC/FINAL/document-transfer/ | grep -v sat | grep prob`; do
#     echo unsolve_doc_transfer_$problem>>submittedProbs
#     ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/FINAL/document-transfer/domain.pddl ${benchmarks_path}/unsolvable_IPC/FINAL/document-transfer/${problem} unsolve_doc_transfer_$problem
# done

# for problem in `ls ${benchmarks_path}/unsolvable_IPC/FINAL/document-transfer/ | grep unknown`; do
#     echo unsolve_doc_transfer_$problem>>submittedProbs
#     ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/FINAL/document-transfer/domain.pddl ${benchmarks_path}/unsolvable_IPC/FINAL/document-transfer/${problem} unsolve_doc_transfer_$problem
# done


# for domain in unsat-tpp bottleneck unsat-tiles unsat-rovers unsat-nomystery mystery; do
#   for problem in `ls ${benchmarks_path}/unsolvable_IPC/merge-and-shrink-set/${domain}/ | grep prob`; do
#       echo unsolve_merge_shrink_set_${domain}_${problem}>>submittedProbs
#       ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/merge-and-shrink-set/${domain}/domain.pddl ${benchmarks_path}/unsolvable_IPC/merge-and-shrink-set/${domain}/${problem} unsolve_merge_shrink_set_${domain}_${problem}
#   done
# done

# for problem in `ls ${benchmarks_path}/unsolvable_IPC/merge-and-shrink-set/unsat-pegsol-strips/ | grep domain`; do
#     echo unsolve_merge_shrink_set_unsat-pegsol-strips_${problem}>>submittedProbs
#     ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/merge-and-shrink-set/unsat-pegsol-strips/${problem} ${benchmarks_path}/unsolvable_IPC/merge-and-shrink-set/unsat-pegsol-strips/${problem%-domain.pddl}.pddl unsolve_merge_shrink_set_unsat-pegsol-strips_${problem}
# done

# for problem in `ls ${benchmarks_path}/unsolvable_IPC/merge-and-shrink-set/3unsat/ | grep -v domain`; do
#     echo  unsolve_merge_shrink_set_3unsat_${problem}>>submittedProbs
#     ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/merge-and-shrink-set/3unsat/domain_${problem} ${benchmarks_path}/unsolvable_IPC/merge-and-shrink-set/3unsat/${problem} unsolve_merge_shrink_set_3unsat_${problem}
# done

# for domain in elevators; do
#   for problem in `ls ${benchmarks_path}/unsolvable_IPC/debugging/${domain}/ | grep prob`; do
#       echo unsolve_debuggin_${domain}_${problem}>>submittedProbs
#       ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/debugging/${domain}/domain.pddl ${benchmarks_path}/unsolvable_IPC/debugging/${domain}/${problem} unsolve_debuggin_${domain}_${problem}
#   done
# done


# for problem in `ls ${benchmarks_path}/unsolvable_IPC/generated/document-transfer/ | grep prob`; do
#     echo unsolve_generated_document_transfer_$problem>>submittedProbs
#     ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/generated/document-transfer/domain.pddl ${benchmarks_path}/unsolvable_IPC/generated/document-transfer/${problem} unsolve_generated_document_transfer_${problem}
# done


# for domain in transport/transport-final-unsolvable transport/transport-final-unsolvable-sat barman-unsolvable; do
#   for problem in `ls ${benchmarks_path}/unsolvable_IPC/bagged/${domain}/ | grep reformulated-domain`; do
#       domainName=`echo $domain | sed 's/\///g'`
#       echo unsolve_debuggin_${domainName}_${problem}>>submittedProbs
#       ./run_experiment.sh  ${benchmarks_path}/unsolvable_IPC/bagged/${domain}/${problem} ${benchmarks_path}/unsolvable_IPC/bagged/${domain}/reformulated${problem#reformulated-domain} unsolve_debuggin_${domainName}_${problem}
#   done
# done


######################## Everything before here is unsolvable!!!#################


# for problem in `ls ${benchmarks_path}/newopen/ | grep \.domain\.pddl`; do
#     echo newopen_${problem%.domain.pddl}>>submittedProbs
#     ./run_experiment.sh ${benchmarks_path}/newopen/$problem ${benchmarks_path}/newopen/${problem%.domain.pddl}.problem.pddl newopen_${problem%.domain.pddl}
# done


for problem in `ls ${benchmarks_path}/Tests2/Rovers/Strips/ | grep pfile`; do
    echo Test2_rover_${problem}>>submittedProbs
    ./run_experiment.sh ${benchmarks_path}/Tests2/Rovers/Strips/StripsRover.pddl ${benchmarks_path}/Tests2/Rovers/Strips/$problem Test2_rover_${problem}
done

for problem in `ls ${benchmarks_path}/Tests2/Rovers/Strips/Untyped | grep pfile`; do
    echo Test2_rover_${problem}>>submittedProbs
    ./run_experiment.sh ${benchmarks_path}/Tests2/Rovers/Strips/Untyped/StripsRover.pddl.untyped ${benchmarks_path}/Tests2/Rovers/Strips/Untyped/$problem Test2_rover_untyped_${problem}
done


for domain in `ls ${benchmarks_path}/tseq-opt/`; do
  for problem in `ls ${benchmarks_path}/tseq-opt/${domain}/problems/ | grep pfile`; do
      echo tseq-opt_satt_${problem}>>submittedProbs
      ./run_experiment.sh ${benchmarks_path}/tseq-opt/${domain}/domain/domain.pddl ${benchmarks_path}/tseq-opt/${domain}/problems/$problem tseq-opt_${domain}_${problem}
  done
done


for problem in `ls ${benchmarks_path}/Tests2/Satellite/Strips/ | grep pfile`; do
    echo Test2_Sat_${problem}>>submittedProbs
    ./run_experiment.sh ${benchmarks_path}/Tests2/Satellite/Strips/stripsSat.pddl ${benchmarks_path}/Tests2/Satellite/Strips/$problem Test2_Sat_${problem}
done

for problem in `ls ${benchmarks_path}/Tests2/Satellite/Strips/Untyped | grep pfile`; do
    echo Test2_Sat_${problem}>>submittedProbs
    ./run_experiment.sh ${benchmarks_path}/Tests2/Satellite/Strips/Untyped/stripsSat.pddl.untyped ${benchmarks_path}/Tests2/Satellite/Strips/Untyped/$problem Test2_Sat_${problem}
done


for domain in `ls ${benchmarks_path}/IPC8/seq-sat/ | grep -iv stacks`; do
  for problem in `ls ${benchmarks_path}/IPC8/seq-sat/${domain}/ | grep -v domain| grep -v \#`; do
      echo IPC8_sat_${domain}_${problem}>>submittedProbs
      ./run_experiment.sh ${benchmarks_path}/IPC8/seq-sat/${domain}/domain.pddl ${benchmarks_path}/IPC8/seq-sat/${domain}/$problem IPC8_sat_${domain}_${problem}
  done
done

for domain in `ls ${benchmarks_path}/IPC8/seq-sat/ | grep -i stacks`; do
  for problem in `ls ${benchmarks_path}/IPC8/seq-sat/${domain}/ | grep -v domain| grep -v \#`; do
      echo IPC8_sat_${domain}_${problem}>>submittedProbs
      ./run_experiment.sh ${benchmarks_path}/IPC8/seq-sat/${domain}/domain_${problem} ${benchmarks_path}/IPC8/seq-sat/${domain}/$problem IPC8_sat_${domain}_${problem}
  done
done

for domain in `ls ${benchmarks_path}/IPC8/seq-opt/ | grep -iv stacks`; do
  for problem in `ls ${benchmarks_path}/IPC8/seq-opt/${domain}/ | grep -v domain| grep -v \#`; do
      echo IPC8_opt_${domain}_${problem}>>submittedProbs
      ./run_experiment.sh ${benchmarks_path}/IPC8/seq-opt/${domain}/domain.pddl ${benchmarks_path}/IPC8/seq-opt/${domain}/$problem IPC8_opt_${domain}_${problem}
  done
done

for domain in `ls ${benchmarks_path}/IPC8/seq-opt/ | grep -i stacks`; do
  for problem in `ls ${benchmarks_path}/IPC8/seq-opt/${domain}/ | grep -v domain| grep -v \#`; do
      echo IPC8_opt_${domain}_${problem}>>submittedProbs
      ./run_experiment.sh ${benchmarks_path}/IPC8/seq-opt/${domain}/domain_${problem} ${benchmarks_path}/IPC8/seq-opt/${domain}/$problem IPC8_opt_${domain}_${problem}
  done
done

for domain in `ls ${benchmarks_path}/IPC5/ | grep -v path`; do
  for problem in `ls ${benchmarks_path}/IPC5/${domain}/Propositional | grep -v domain| grep -v \#`; do
      echo IPC5_${domain}_${problem}>>submittedProbs
      ./run_experiment.sh ${benchmarks_path}/IPC5/${domain}/Propositional/domain.pddl ${benchmarks_path}/IPC5/${domain}/Propositional/$problem IPC5_${domain}_${problem}
  done
done

for problem in `ls ${benchmarks_path}/IPC5/pathways/Propositional/ | grep -v domain | grep \.pddl  | grep -v \#`; do
      echo IPC5_pathways_${problem}>>submittedProbs
    ./run_experiment.sh ${benchmarks_path}/IPC5/pathways/Propositional/domain_${problem} ${benchmarks_path}/IPC5/${domain}/Propositional/$problem IPC5_pathways_${problem}
done

for domain in `ls ${benchmarks_path}/seq-opt/ | grep -iv stack | grep -iv printer | grep -iv barman | grep -iv floor | grep -iv parking`; do
  for problem in `ls ${benchmarks_path}/seq-opt/${domain}/problems`; do
      echo seq-opt_${domain}_${problem}>>submittedProbs
      ./run_experiment.sh ${benchmarks_path}/seq-opt/${domain}/domain/domain.pddl ${benchmarks_path}/seq-opt/${domain}/problems/$problem seq-opt_${domain}_${problem}
  done
done

for domain in `ls ${benchmarks_path}/seq-sat/ | grep -iv stack | grep -iv printer | grep -iv barman | grep -iv parking | grep -iv tidy`; do
  for problem in `ls ${benchmarks_path}/seq-sat/${domain}/problems`; do
      echo seq-sat_${domain}_${problem}>>submittedProbs
      ./run_experiment.sh ${benchmarks_path}/seq-sat/${domain}/domain/domain-woac.pddl ${benchmarks_path}/seq-sat/${domain}/problems/$problem seq-sat_${domain}_${problem}
  done
done

for problem in `ls ${benchmarks_path}/seq-opt/floortile/problems`; do
    echo seq-opt_floortile_${problem}
    ./run_experiment.sh ${benchmarks_path}/seq-opt/floortile/domain/floortile.pddl ${benchmarks_path}/seq-opt/floortile/problems/$problem seq-opt_floortile_${problem}
done

for problem in `ls ${benchmarks_path}/seq-opt/parking/problems`; do
    echo seq-opt_parking_${problem}>>submittedProbs
    ./run_experiment.sh ${benchmarks_path}/seq-opt/parking/domain/parking.pddl ${benchmarks_path}/seq-opt/parking/problems/$problem seq-opt_parking_${problem}
done


for problem in `ls ${benchmarks_path}/seq-sat/parking/problems`; do
    echo seq-sat_parking_${problem}>>submittedProbs
    ./run_experiment.sh ${benchmarks_path}/seq-sat/parking/domain/parking-woac.pddl ${benchmarks_path}/seq-sat/parking/problems/$problem seq-sat_parking_${problem}
done


for problem in `ls ${benchmarks_path}/seq-opt/parcprinter/problems`; do
  domain=${problem%.pddl}
    echo seq-opt_parcprinter_${problem}>>submittedProbs
    ./run_experiment.sh ${benchmarks_path}/seq-opt/parcprinter/domain/${domain}-domain.pddl ${benchmarks_path}/seq-opt/parcprinter/problems/$problem seq-opt_parcprinter_${problem}
done

for problem in `ls ${benchmarks_path}/seq-sat/parcprinter/problems`; do
  domain=${problem%.pddl}
    echo seq-sat_parcprinter_${problem}>>submittedProbs
    ./run_experiment.sh ${benchmarks_path}/seq-sat/parcprinter/domain/${domain}-domain-woac.pddl ${benchmarks_path}/seq-sat/parcprinter/problems/$problem seq-sat_parcprinter_${problem}
done


for problem in `ls ${benchmarks_path}/seq-opt/openstacks/problems`; do
    domain=${problem%.pddl}
    echo seq-opt_openstacks_${problem}>>submittedProbs
    ./run_experiment.sh ${benchmarks_path}/seq-opt/openstacks/domain/${domain}-domain.pddl ${benchmarks_path}/seq-opt/openstacks/problems/$problem seq-opt_openstacks_${problem}
done

for problem in `ls ${benchmarks_path}/seq-sat/openstacks/problems`; do
    domain=${problem%.pddl}
    echo seq-sat_openstacks_${problem}>>submittedProbs
    ./run_experiment.sh ${benchmarks_path}/seq-sat/openstacks/domain/${domain}-domain-woac.pddl ${benchmarks_path}/seq-sat/openstacks/problems/$problem seq-sat_openstacks_${problem}
done

for problem in `ls ${benchmarks_path}/gripper/strips/ | grep -v domain | grep -v \#`; do
    echo gripper_${problem}>>submittedProbs
    ./run_experiment.sh ${benchmarks_path}/gripper/strips/domain.pddl ${benchmarks_path}/gripper/strips/$problem gripper_${problem}
done

for problem in `ls ${benchmarks_path}/ZenoTravel/Strips// | grep -v woac | grep pfile`; do
    echo zeno_${problem}>>submittedProbs
    ./run_experiment.sh ${benchmarks_path}/ZenoTravel/Strips/zenotravelStrips.pddl ${benchmarks_path}/ZenoTravel/Strips/$problem zeno_${problem}
done

for problem in `ls ${benchmarks_path}/ZenoTravel/Strips/Untyped/ | grep -v woac | grep pfile`; do
    echo zeno_untyped_${problem}>>submittedProbs
    ./run_experiment.sh ${benchmarks_path}/ZenoTravel/Strips/Untyped/zenotravelStrips.pddl.untyped ${benchmarks_path}/ZenoTravel/Strips/Untyped//$problem zeno_untyped_${problem}
done


for domain in Track1/Typed Track1/Typed/Additional Track1/Untyped Track1/Untyped/Additional  Track2 Track2/Additional; do
  for problem in `ls ${benchmarks_path}/Logistics/${domain}/ | grep prob`; do
      domainName=`echo $domain | sed 's/\///g'`
      echo logistics_${domainName}_${problem}>>submittedProbs
      ./run_experiment.sh ${benchmarks_path}/Logistics/${domain}/domain.pddl ${benchmarks_path}//Logistics/${domain}/$problem logistics_${domainName}_${problem}
  done
done
