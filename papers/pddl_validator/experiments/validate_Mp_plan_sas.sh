#!/bin/bash

# Validates a Madagascar plan relative to a FD's SAS+ translation

# if [ -f "exp6_data/$3.FDsat.plan.plan" ];
# then
#   exit
# fi


if [ -f "exp6_data/$3.Mp.plan.nobound.sas_validity" ];
then
  exit
fi

if [ -f "exp6_data/$3.Mp.plan.nobound.out" ];
then
  ID=$RANDOM
  mkdir sas_$ID
  cd sas_$ID
  ../../../../codeBase/planning/FD/src/translate/translate.py ../$1 ../$2 > /dev/null
  cd ../
  ./extract_Mp_plan.sh  exp6_data/$3.Mp.plan.nobound.out > /dev/null
  if [ -f "exp6_data/$3.Mp.plan.nobound.plan" ];
  then
    ./strips <(sed 's/[ \t]*$//g' sas_$ID/output.sas) exp6_data/$3.Mp.plan.nobound.plan | grep -i "valid plan" > exp6_data/$3.Mp.plan.nobound.sas_validity
    cat exp6_data/$3.Mp.plan.nobound.sas_validity
  fi
  rm -rf sas_$ID
fi
