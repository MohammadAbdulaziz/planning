#!/bin/bash

# Validates a FD plan using VAL

# if [ -f "exp6_data/$3.FDsat.plan.plan" ];
# then
#   exit
# fi
echo $domName
echo $probName
echo $outName
if [ -f "exp6_data/$outName.FDsat.plan.VAL_validity_PDDL" ];
then
  exit
fi

echo 1

if [ -f "exp6_data/$outName.FDsat.plan.out" ];
then
  ./extract_FD_plan.sh  exp6_data/$outName.FDsat.plan.out > /dev/null
  echo "extracred plan"
  if [ -f "exp6_data/$outName.FDsat.plan.plan" ];
  then
    { time ../../../codeBase/planning/VAL/validate $domName $probName <(./FD_plan_to_IPC_plan.sh exp6_data/$outName.FDsat.plan.plan) ; } > exp6_data/$outName.FDsat.plan.VAL_validity_PDDL 2>exp6_data/$outName.FDsat.plan.VAL_validity_PDDL_time
    echo "finished with VAL"
    cat exp6_data/$outName.FDsat.plan.VAL_validity_PDDL
    # ID1=$RANDOM
    # ID2=$RANDOM
    # mkdir tmp_${ID1}_${ID2}
    # cp -rf ../../../codeBase/planning/VAL/* tmp_${ID1}_${ID2}
    # ./FD_plan_to_IPC_plan.sh exp6_data/$outName.FDsat.plan.plan  > tmp_${ID1}_${ID2}/plan
    # cd tmp_${ID1}_${ID2}
    # { time ./validate ../$domName ../$probName plan ; } > ../exp6_data/$outName.FDsat.plan.VAL_validity_PDDL 2>../exp6_data/$outName.FDsat.plan.VAL_validity_PDDL_time
    # cd ..
    # rm -rf tmp_${ID1}_${ID2}
    # echo "finished with VAL"
    # cat exp6_data/$outName.FDsat.plan.VAL_validity_PDDL
  fi
fi
