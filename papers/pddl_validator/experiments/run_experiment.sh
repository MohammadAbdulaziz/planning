#!/bin/bash

export domName=$1 
export probName=$2 
export outName=$3

# echo $outName
# ls $domName
# ls $probName

# echo $outName $domName $probName exp6_data/$outName.FDsat.plan.plan
# 
# 
# exit

#outName=${outName%.pddl}


#echo $domName $probName $outName



# ln -s "../$domName" "results/$outName.dom.pddl"
# ln -s "../$probName" "results/$outName.prob.pddl"

# if [ -f "exp_results/$outName.bound.out" ]; then
#   exit
# fi

# while [ `squeue | grep abd | wc -l` -gt 300 ]
# do
#   sleep 2
# done


# The next few loops are to make sure that mistakes discovered in problems are verified.

# if grep -q "not valid" exp6_data/$outName.FDsat.plan.INVAL_validity_PDDL; then
#   rm exp6_data/$outName.FDsat.plan.INVAL_validity_PDDL
#   rm exp6_data/$outName.FDsat.plan.INVAL_validity_PDDL_time
#   timeout 10m ./validate_FD_plan_INVAL.sh
# fi

# while [ `ps -e | grep validate_ | wc -l` -gt 4 ]
# do
#   sleep 1
# done

# if grep -q "invalid" exp6_data/$outName.FDsat.plan.VAL_validity_PDDL || grep -q "execution failed" exp6_data/$outName.FDsat.plan.VAL_validity_PDDL; then
#   rm exp6_data/$outName.FDsat.plan.VAL_validity_PDDL
#   rm exp6_data/$outName.FDsat.plan.VAL_validity_PDDL_time
#   timeout 10m ./validate_FD_plan_VAL.sh
# fi

# while [ `ps -e | grep validate_ | wc -l` -gt 4 ]
# do
#   sleep 1
# done

# if ! grep -q "s VERIFIED" exp6_data/$outName.FDsat.plan.ISABELLE_VALIDATOR_validity_PDDL && ! grep -q "Error parsing" exp6_data/$outName.FDsat.plan.ISABELLE_VALIDATOR_validity_PDDL; then
#   rm exp6_data/$outName.FDsat.plan.ISABELLE_VALIDATOR_validity_PDDL
#   rm exp6_data/$outName.FDsat.plan.ISABELLE_VALIDATOR_validity_PDDL_time
#   timeout 10m ./validate_FD_plan_ISABELLE_VALIDATOR.sh
# fi



# if grep -q ":types" $domName; then
#   export ALWAYS_REBUILD="TRUE"
# #   rm -f exp6_data/$outName.FDsat.plan.ISABELLE_VALIDATOR_SML_PARSER_validity_PDDL
# #   rm -f exp6_data/$outName.FDsat.plan.ISABELLE_VALIDATOR_SML_PARSER_validity_PDDL_time
#   timeout 10m ./validate_FD_plan_ISABELLE_VALIDATOR_SML_PARSER.sh
# fi


# while [ `ps -e | grep validate_ | wc -l` -gt 4 ]
# do
#   sleep 1
# done

# timeout 10m ./validate_FD_plan_VAL.sh &


# while [ `ps -e | grep validate_ | wc -l` -gt 6 ]
# do
#   sleep 1
# done
# timeout 10m ./validate_FD_plan_VAL.sh &
while [ `ps -e | grep validate_ | wc -l` -gt 6 ]
do
  sleep 1
done
timeout 10m ./validate_FD_plan_INVAL.sh &
# while [ `ps -e | grep validate_ | wc -l` -gt 6 ]
# do
#   sleep 1
# done
# timeout 10m ./validate_FD_plan_ISABELLE_VALIDATOR_2.sh &



# while [ `ps -e | grep plan_fdownward | wc -l` -gt 6 ]
# do
#   sleep 1
# done
# timeout 15m ./plan_fdownward_sat.sh &

# while [ `ps -e | grep validate_ | wc -l` -gt 6 ]
# do
#   sleep 1
# done
# timeout 10m ./validate_FD_plan_ISABELLE_VALIDATOR.sh &



# ./analyse_results.sh

# sbatch --mem=32192 --time=180 ./plan_madagascar_with_bound.sh
# sbatch --mem=32192 --time=180 ./plan_fdownward_unsat.sh

#sbatch --mem=32192 --time=180 ./plan_madagascar_no_bound.sh
#sbatch --mem=32192 --time=180 ./plan_fdownward_sat.sh

# sbatch --mem=8000 --time=20 ./validate_FD_plan_VAL.sh

#sbatch --mem=8000 --time=60 ./validate_FD_plan_ISABELLE_VALIDATOR_no_types.sh

# sbatch --mem=32192 --time=60 ./validate_FD_plan_ISABELLE_VALIDATOR_no_types.sh
