#!/bin/bash

DIR=$1
PRF=$2



for tfile in `find $DIR -name time`; do
  basedir=${tfile%/time}
  basename=`basename $basedir`
  
  if grep -q Valid $basedir/result; then valid=true; else valid=false; fi
  
  time=`gawk '$1=="real" {print $2}' $basedir/time`
  
  echo "$PRF $basename $time $valid"
  
#   echo -n $time " "
#   $valid || echo -ne "\033[1;31m(INVALID) \033[0m"
#   $valid && echo -ne "\033[1;32m(VALID)   \033[0m"
#   echo -n $basename
#   echo
done



