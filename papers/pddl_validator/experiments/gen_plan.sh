#!/bin/bash


cmdDir=${0%/*}

probName=$1
domName="domain.pddl"
outName="$probName.out"
planName="$probName.plan"

FD=~/devel/isabelle/planning/codeBase/planning/FD/src/fast-downward.py
EXTRACT_PLAN="$cmdDir/extract_FD_plan.sh"


# if [ ! -f "../../../../papers/sspaceAcyclicity/experiments/exp_results/$outName.FDsat.plan.out" ]; then

if test ! -s $planName; then

  echo "Running $baseName"
  timeout 15m $FD $domName $probName --search "lazy_greedy([ff()], preferred=[ff()])" >$outName 2>&1

  $EXTRACT_PLAN $outName $planName

else
  echo "Plan file $planName already exists"
fi
