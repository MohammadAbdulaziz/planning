#!/bin/bash

# This script run the verified validator to validate plans for problems whose types have been striped by the simplify tool of Patrik Haslum.
# This script assumes that the simplify tool does not introduce ADL constructs
sleep 3
echo $domName
echo $probName
echo $outName

if [ -f "exp6_data/$outName.FDsat.plan.ISABELLE_VALIDATOR_no_types_validity_PDDL" ];
then
  exit
fi

echo 1

if grep -iq ":adl" $domName;
then
  exit
fi

echo 2

if [ -f "exp6_data/$outName.FDsat.plan.out" ];
then
  echo 3
  ./extract_FD_plan.sh  exp6_data/$outName.FDsat.plan.out > /dev/null
  echo "extracred plan"
  if [ -f "exp6_data/$outName.FDsat.plan.plan" ];
  then
    echo 4
    ID1=$RANDOM
    ID2=$RANDOM
    mkdir tmp_${ID1}_${ID2}
    cp -rf ../../../codeBase/planning/INVAL/* tmp_${ID1}_${ID2}
    cd tmp_${ID1}_${ID2}/
    ./simplify -T ../$domName ../$probName
    # rm domain-$(basename $domName)
    # rm domain-$(basename $probName)
    sed -i 's/:ADL//g' `ls domain-$(basename $probName)`
    export domName=`pwd`/`ls domain-$(basename $probName)`
    if [[ $domName == *".pddl.pddl"* ]]; then
      export domName=${domName%.pddl}
    fi
    echo $domName
    export probName=`pwd`/`ls simplified-$(basename $probName)`
    if [[ $probName == *".pddl.pddl"* ]]; then
      export probName=${probName%.pddl}
    fi
    echo $probName
    export outName=${outName}_no_types
    #cp exp6_data/$outName.FDsat.plan.out exp6_data/${outName}_no_types.FDsat.plan.out
    cd ..
    ./plan_fdownward_sat.sh
    echo "Finished planning w/o types"
    pwd
    ./validate_FD_plan_ISABELLE_VALIDATOR.sh
    rm -rf tmp_${ID1}_${ID2}
  fi
fi
