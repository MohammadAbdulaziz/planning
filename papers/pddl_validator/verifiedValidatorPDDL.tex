\section{Beyond SAS+: Verifying a PDDL Validator}
\label{sec:PDDLValidation}

%TODO: Convert verbatims to proper isabelle notation, once examples are stable and fixed!

In this section we present a verified PDDL validator, following the same approach as for SAS+:
Based on an abstract syntax tree for PDDL, we have formalised 
an abstract semantics and refined it to an executable validator.
Our validator currently support STRIPS, equality, action-costs and plain types (i.e., no subtyping), which is enough to validate many of the International Planning Competition benchmarks and their solutions.

Like for the SAS+ semantics, we start with a straightforward and simple semantics, 
which is not obfuscated by design choices motivated by efficient execution.
In a second step, this abstract semantics is refined to an executable version, from which we extract
Standard ML code for a validator. Again, all refinement steps are proved correct within Isabelle,
such that, provided we trust Isabelle's kernel, we do not have to consider the efficient implementation in order to
believe that our checker actually implements the abstract semantics.

% As we do not need to consider executability at the specification level, 
% we can provide a straightforward and simple semantics, which is not obfuscated by design 
% choices motivated by efficient execution.
% 
% In a second step, the abstract version of the semantics is refined to an executable version, from 
% which we can extract Standard ML code for a validator. We prove our refinement correct, such that 
% we obtain an Isabelle theorem which states that the (executable) validator correctly implements the 
% abstract semantics --- provided we trust Isabelle, we do not have to consider the efficient 
% implementation of the semantics, but it is enough to consider the abstract semantics in order to believe that our checker is correct.

Our abstract syntax tree for PDDL roughly follows Kovacs' PDDL grammar~\cite{kovacscomplete}, striving for a concise encoding in HOL.
Following a similar approach that we used with the SAS+ validator, we formalise an abstract syntax for PDDL schemas, PDDL instances, and plans.\footnote{The syntax of plans is not specified in Kovacs' grammar.}

For example, we encode an action schema as follows:

\ \ \isacommand{datatype}\isamarkupfalse%
\ ast{\isacharunderscore}action{\isacharunderscore}schema\ {\isacharequal}\ Action{\isacharunderscore}Schema\isanewline
\ \ \ \ {\isacharparenleft}name{\isacharcolon}\ name{\isacharparenright}\isanewline
\ \ \ \ {\isacharparenleft}parameters{\isacharcolon}\ {\isachardoublequoteopen}{\isacharparenleft}variable\ {\isasymtimes}\ type{\isacharparenright}\ list{\isachardoublequoteclose}{\isacharparenright}\isanewline
\ \ \ \ {\isacharparenleft}precondition{\isacharcolon}\ {\isachardoublequoteopen}variable\ ast{\isacharunderscore}prop{\isachardoublequoteclose}{\isacharparenright}\isanewline
\ \ \ \ {\isacharparenleft}effect{\isacharcolon}\ {\isachardoublequoteopen}variable\ ast{\isacharunderscore}effect{\isachardoublequoteclose}{\isacharparenright}

This declares a record\footnote{Records are isomorphic to tuples as we used them for SAS+, and it is merely a matter of style which representation to use.} type \fquot{Action\_Schema} with the fields \fquot{name}, \fquot{parameters}, \fquot{precondition}, and \fquot{effect}. 

% 
% 
% 
% \begin{verbatim}
% datatype ast_action_schema = Act
%   (name: name)
%   (parameters: "(variable * type) list")
%   (precondition: "variable ast_prop")
%   (effect: "variable ast_effect")
% \end{verbatim}

% where
% \begin{verbatim}
%   datatype 'val ast_prop =
%     prop_atom "'val atom"
%   | prop_eq 'val 'val
%   | prop_not "'val ast_prop"
%   | prop_and "'val ast_prop list"
% \end{verbatim}
% Note that 'val is a type parameter here, which is set to variable in the action schema. 
% We will use the same data type, with 'val=object, when it comes to describing the semantics of instances.

Our abstract syntax specification already removes some redundancies and indirections from Kovacs' grammar.
For example, the original grammar makes a special case for an empty precondition, which can be easily mapped to an empty conjunction already by the parser.
Thus, our abstract syntax does not include this special case.
Removing purely syntactic shortcuts already during parsing makes the specification more concise and readable.

After having defined the abstract syntax tree, we define a set of well-formedness conditions for a PDDL domain, a PDDL problem, and a plan.
For example, an action schema is well-formed if the parameter names are distinct, and all preconditions and effects are well-formed w.r.t.\ the specified parameters:

\isacommand{fun}\isamarkupfalse\ wf{\isacharunderscore}action{\isacharunderscore}schema\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}ast{\isacharunderscore}action{\isacharunderscore}schema\ {\isasymRightarrow}\ bool{\isachardoublequoteclose}\ \isakeyword{where}\ \ \ \isanewline
\ \ {\isachardoublequoteopen}wf{\isacharunderscore}action{\isacharunderscore}schema\ {\isacharparenleft}Action{\isacharunderscore}Schema\ n\ par\ pre\ eff{\isacharparenright}\isanewline
\ \ {\isasymlongleftrightarrow}\ {\isacharparenleft}\ let\ tyv\ {\isacharequal}\ map{\isacharunderscore}of\ par\ in\isanewline
\ \ \ \ \ \ distinct\ {\isacharparenleft}map\ fst\ par{\isacharparenright}\isanewline
\ \ \ \ {\isasymand}\ wf{\isacharunderscore}prop\ tyv\ pre\ {\isasymand}\ wf{\isacharunderscore}effect\ tyv\ eff\ {\isacharparenright}{\isachardoublequoteclose}

% 
% \begin{verbatim}
% fun wf_action_schema where   
%   wf_action_schema (Act n par pre eff) 
% <--> (
%     let tyv = map_of par in
%       distinct (map fst par)
%     & wf_prop tyv pre
%     & wf_effect tyv eff)
% \end{verbatim}

Finally, we define what the execution of an action and a plan means: a state is a set of facts, there is an enabled function that checks whether an action is enabled in a given state, and an execute function that returns the next state induced by executing an action.
An action of a plan is simply the name of an action applied to a list of objects:

\ \isacommand{datatype}\isamarkupfalse\ plan{\isacharunderscore}action\ {\isacharequal}\ PAction\isanewline
\ \ \ {\isacharparenleft}name{\isacharcolon}\ name{\isacharparenright}\isanewline
\ \ \ {\isacharparenleft}arguments{\isacharcolon}\ {\isachardoublequoteopen}object\ list{\isachardoublequoteclose}{\isacharparenright}

% 
% \begin{verbatim}
% datatype plan_action = PAction
%   (name: name)
%   (arguments: "object list")
% \end{verbatim}

An action is enabled, if it is well-formed and the current state meets the precondition:

\isacommand{definition}\isamarkupfalse\ enabled\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}plan{\isacharunderscore}action\ {\isasymRightarrow}\ state\ {\isasymRightarrow}\ bool{\isachardoublequoteclose}\ \isakeyword{where}\isanewline
\ \ {\isachardoublequoteopen}enabled\ pa\ s\ {\isasymlongleftrightarrow}\ \isanewline
\ \ \ \ \ \ wf{\isacharunderscore}plan{\isacharunderscore}action\ pa\ \isanewline
\ \ \ \ {\isasymand}\ holds\ s\ {\isacharparenleft}precondition\ {\isacharparenleft}resolve{\isacharunderscore}instantiate\ pa{\isacharparenright}{\isacharparenright}{\isachardoublequoteclose}

% \begin{verbatim}
% definition enabled 
%   :: "plan_action => state => bool" 
% where
%   "enabled pa s <--> 
%     wf_plan_action pa 
%   & holds s (precondition (
%       resolve_instantiate pa))"
% \end{verbatim}
Here, the well-formedness check ensures that the action name and its parameters are valid, and \fquot{resolve\_instantiate} instantiates the corresponding action schema with the objects given as parameters. 
Finally, \fquot{holds} checks that the precondition of the instantiated action holds in the current state.

Based on that, we define the function execute, which applies the effect of the instantiated action to the current state, as follows.

\isacommand{definition}\isamarkupfalse\ execute\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}plan{\isacharunderscore}action\ {\isasymRightarrow}\ state\ {\isasymRightarrow}\ state{\isachardoublequoteclose}\ \isakeyword{where}\isanewline
\ {\isachardoublequoteopen}execute\ pa\ s\ {\isacharequal}\isanewline
\ \ \ \ \ \ \ \ apply{\isacharunderscore}effect\ {\isacharparenleft}effect\ {\isacharparenleft}resolve{\isacharunderscore}instantiate\ pa{\isacharparenright}{\isacharparenright}\ s{\isachardoublequoteclose}

% \begin{verbatim}
% definition execute 
%   :: plan_action => state => state 
% where
%   execute pa s = apply_effect (
%     effect (resolve_instantiate pa)) s
% \end{verbatim}
It is then straightforward to define when a plan is valid if executed from a given state, which we define as follows.

\isacommand{fun}\isamarkupfalse\ valid{\isacharunderscore}plan{\isacharunderscore}from\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}state\ {\isasymRightarrow}\ plan\ {\isasymRightarrow}\ bool{\isachardoublequoteclose}\ \isakeyword{where}\isanewline
\ \ {\isachardoublequoteopen}valid{\isacharunderscore}plan{\isacharunderscore}from\ s\ {\isacharbrackleft}{\isacharbrackright}\ {\isasymlongleftrightarrow}\ holds\ s\ {\isacharparenleft}goal\ P{\isacharparenright}{\isachardoublequoteclose}\isanewline
{\isacharbar}\ {\isachardoublequoteopen}valid{\isacharunderscore}plan{\isacharunderscore}from\ s\ {\isacharparenleft}{\isasympi}{\isacharhash}{\isasympi}s{\isacharparenright}\isanewline
\ \  {\isasymlongleftrightarrow}\ enabled\ {\isasympi}\ s\ {\isasymand}\ {\isacharparenleft}valid{\isacharunderscore}plan{\isacharunderscore}from\ {\isacharparenleft}execute\ {\isasympi}\ s{\isacharparenright}\ {\isasympi}s{\isacharparenright}{\isachardoublequoteclose}


% \begin{verbatim}
% fun valid_plan_from 
%   :: "state => plan => bool" 
% where
%   "valid_plan_from s [] 
%     <--> holds s (goal P)"
% | "valid_plan_from s (pa#pas) 
%     <--> enabled pa s 
%       & (valid_plan_from 
%           (execute pa s) pas)"
% \end{verbatim}
Finally, a valid plan is a plan that is valid from the initial state (denoted by I), which is defined as follows.

\isacommand{definition}\isamarkupfalse\ valid{\isacharunderscore}plan\ {\isacharcolon}{\isacharcolon}\ {\isachardoublequoteopen}plan\ {\isasymRightarrow}\ bool{\isachardoublequoteclose}\ \isanewline
\ \ \isakeyword{where}\ {\isachardoublequoteopen}valid{\isacharunderscore}plan\ {\isasymequiv}\ valid{\isacharunderscore}plan{\isacharunderscore}from\ I{\isachardoublequoteclose}


% \begin{verbatim}
% definition valid_plan :: "plan => bool" 
%   where "valid_plan = valid_plan_from I"
% \end{verbatim}


\subsection{Refinement}
In a next step, we re-implement parts of our definitions, with the goal to make them (more efficiently) executable, and more convenient to use.
We add human-readable error messages, and implement some operations more efficiently. For example, in order check and execute the next plan action, it is sufficient to resolve and instantiate the action once, whereas the abstract specification instantiates the action first for the enabledness check, and a second time for the application of the effect. 


We then prove that our refined checker is sound w.r.t.\ the abstract
specification:

\isacommand{lemma}\isamarkupfalse\ check{\isacharunderscore}plan{\isacharunderscore}return{\isacharunderscore}iff{\isacharcolon}\isanewline
\ \ {\isachardoublequoteopen}check{\isacharunderscore}plan\ P\ {\isasympi}s\ {\isacharequal}\ Inr\ {\isacharparenleft}{\isacharparenright}\ \isanewline{\isasymlongleftrightarrow}\ wf{\isacharunderscore}problem\ P\ {\isasymand}\ valid{\isacharunderscore}plan\ P\ {\isasympi}s{\isachardoublequoteclose}

% \begin{verbatim}
% lemma check_plan_return_iff: 
%   "check_plan P pas = Inr () 
%   <--> wf_problem P 
%      & valid_plan P pas"
% \end{verbatim}
For a problem $P$ and an action sequence $\pi$s, our executable plan checker returns $Inr~()$ iff and only iff the problem and its domain are well-formed, 
and the plan is valid.\footnote{Note that the problem $P$ is now an explicit parameter to valid\_plan.}
Otherwise, our checker returns $Inl~msg$ with some error message.

\subsubsection{Data Refinement}
To enhance the performance of our validator, we use a verified red-black tree implementation to represent the state. (which is a set of facts)
This data refinement is done automatically by Isabelle, using the Containers framework~\cite{lochbihler2013light}.
We also apply some semi-automatic optimizations, implementing maps from various entities by red-black trees, and using an efficient element distinctness check 
based on quicksort. Again, all these optimizations are proved correct within Isabelle.
By doing some profiling to identify hotspots and performance leaks, we were able to reduce the runtime of our initial (list-based) validator by several 
orders of magnitude.

Finally, we combine our checker with a parser (which is, like for SAS+, a trusted part of our PDDL validator), to obtain a fully operational validator satisfying the following specification.

\isacommand{lemma}\isamarkupfalse\ parse{\isacharunderscore}check{\isacharunderscore}dpp{\isacharunderscore}impl{\isacharunderscore}return{\isacharunderscore}iff{\isacharcolon}\isanewline
\ \ {\isachardoublequoteopen}parse{\isacharunderscore}check{\isacharunderscore}dpp{\isacharunderscore}impl\ ds\ ps\ pls\ {\isacharequal}\ Inr\ {\isacharparenleft}{\isacharparenright}\ \isanewline
\ \ {\isasymlongleftrightarrow}\ {\isacharparenleft}{\isasymexists}D\ P\ {\isasympi}s{\isachardot}\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ parse{\isacharunderscore}domain{\isacharunderscore}file\ ds\ {\isacharequal}\ Inr\ D\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ {\isasymand}\ parse{\isacharunderscore}problem{\isacharunderscore}file\ D\ ps\ {\isacharequal}\ Inr\ P\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ {\isasymand}\ parse{\isacharunderscore}plan{\isacharunderscore}file\ pls\ {\isacharequal}\ Inr\ {\isasympi}s\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ {\isasymand}\ wf{\isacharunderscore}problem\ P\ \isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ {\isasymand}\ valid{\isacharunderscore}plan\ P\ {\isasympi}s\ {\isacharparenright}{\isachardoublequoteclose}


% \begin{verbatim}
%   lemma parse_check_return_iff:
%     "parse_check ds ps pls = Inr () 
%     <--> (EX D P pas.
%       parse_domain_file ds = Inr D
%     & parse_problem_file D ps = Inr P
%     & parse_plan_file pls = Inr pas
%     & ast_problem.wf_problem P 
%     & ast_problem.valid_plan P pas)" 
% \end{verbatim}
Isabelle exports our checker to Standard ML code, and after adding a command line interface, one obtains a fully operational plan validator.






% Note that the problem $P$ has now become an explicit parameter to the functions, while it was implicit in the presentation above.








% An atom is well-formed if all its argument types are compatible with the declared types:
% \begin{verbatim}
%   fun wf_atom :: 'val atom => bool where
%     wf_atom (Atom p vs) <--> (
%       case sig p of 
%         None => False
%       | Some Ts => 
%           list_all2 is_of_type vs Ts
%     )
% \end{verbatim}








% 
% 
% removes certian some redundancies and indirections from the grammar: 
% For example, the grammar actually makes a special 
% 
% 
% 
% 
% 
% The semantics is written at an abstract level, with the intention to be simple and readable.
% 
% 
% 
% 
% (currently supporting STRIPS, equality, and plain types)
% 



