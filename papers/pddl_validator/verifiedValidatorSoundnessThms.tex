\section{Proving Properties About the Semantics: Sanity Checking Theorems}
\label{sec:SOUND}

Traditionally, validators specify semantics (usually in the form of an implementation) and trust that the specified semantics are what they are supposed to mean at face value; the only check one could do is visual inspection.
Here, we demonstrate that specifying semantics in a theorem prover allows proving sanity checking theorems within the theorem prover about the specified semantics, instead of mere visual inspection.
%Indeed, theorem provers allow proving sanity checking theorems about the semantics specification, thus ensuring that they satisfy desired basic properties.
Admittedly, the theorem statements are still trusted only visually, but it is established as a plausible premise that proving such sanity checking theorems reduces the chance of bugs remaining unnoticed, e.g. Norrish's seminal formalisation of the C-language's semantics~\cite{norrish1998c}.

One sanity checking theorem we prove in Isabelle/HOL concerns the soundness of closing the world-model.
It states that for a formula without negation, implication, nor equality, our entailment is the same as entailment without closing the model.
This shows that our ground action semantics generalise standard STRIPS without negation and equality.

Another sanity check shows that executing a well-typed plan preserves well-formedness of the world model.
Here, a world model is well-formed if it is basic, and all its predicates are declared and applied to type-compatible declared objects.
This is a sanity check for our well-formedness conditions: it proves that they are strong enough to ensure nothing odd will happen to the world model during execution.

A more involved sanity check confirms that our execution semantics of ground actions are sound, in the sense used by Lifschitz~\cite{lifschitz1987semantics}.
Lifschitz introduced the notions of abstract states and abstract actions, and derived conditions on ground actions and world models allowing their execution to be simulated by executing abstract actions on abstract states.
Since the execution semantics of abstract actions are an alternative formulation of the execution semantics of ground actions, then showing a simulation between the two semantics is a form of validation reducing the possibility of having bugs. 
%We prove a similar theorem for our semantics in Isabelle/HOL, with a few differences.
We model abstract states in Isabelle/HOL as \emph{valuations}, i.e. functions from atoms to truth values.
While Lifschitz assumed that the concept of an abstract state satisfying a formula is readily defined, we formalise that concept using the syntactic entailment operator defined in the logic of Michael and Nipkow~\cite{MichaelisN-TYPES17}, in which they use \fquot{s\;\isasymTurnstile\;\isasymphi} to denote that a valuation \fquot{s} entails a formula \fquot{\isasymphi}.
However, we extend the entailment operator of their logic to include equalities and denote this new entailment by \fquot{s\ {\isasymTurnstile}\isactrlsub =\ \isasymphi}.
Lifschitz, and we, define abstract actions as partial functions from abstract states to abstract states.
However, we model partiality of actions using the \fquot{option} type in Isabelle.

To demonstrate the simulation, we define mappings from world-models to abstract states and from ground actions to abstract actions.
A world-model \fquot{M} is mapped to an abstract state \fquot{s} if {\isasymforall}{\isasymphi}{\isasymin}close\_world\ M.\ s\ {\isasymTurnstile}\isactrlsub =\ {\isasymphi}.
Let $\alpha$ be a ground action with precondition \fquot{pre}, and effects \fquot{add} and \fquot{del}.
$\alpha$ is mapped to an abstract action $f$ if for any abstract state $s$, \fquot{s {\isasymTurnstile}\isactrlsub =\ pre} implies that there is an $s'$ s.t. $f(s) = s'$ (i.e. the abstract action is well-defined on $s$), and that for any atom \fquot{atm}
\begin{enumerate*}
  \item if \fquot{atm $\notin$ del} and \fquot{s {\isasymTurnstile}\isactrlsub =\ atm}, then \fquot{s' {\isasymTurnstile}\isactrlsub =\ atm}, and 
  \item if \fquot{atm $\notin$ add} and \fquot{s {\isasymTurnstile}\isactrlsub =\ $\neg$ atm}, then \fquot{s' {\isasymTurnstile}\isactrlsub =\ $\neg$ atm}, and
  \item if \fquot{atm $\in$ add}, then \fquot{s' {\isasymTurnstile}\isactrlsub =\ atm}w, and
  \item if \fquot{atm $\in$ del} and \fquot{atm $\notin$ add}, then \fquot{s' {\isasymTurnstile}\isactrlsub =\ $\neg$ atm}.
\end{enumerate*}
%A ground action is sound if there is an abstract action to which it is mapped. 
Those two mappings are analogous to the mappings by Lifschitz.
However, since, unlike Lifschitz, we support negations, implications and equalities in the precondition  we use an entailment operator with equality, and we add the articles ii and iii to the action mapping.
Also since we restrict ground action effects to atoms, we omit the condition stating that every non-atomic effect in \fquot{add} is a tautology from the action mapping.
Using our mappings we show the required simulation between ground actions/world models and abstract actions/states as a theorem in Isabelle/HOL equivalent to the theorem in Section~4 in Lifschitz's paper.

As a last sanity check, we show that our semantics of types and action schema instantiation do not affect soundness, i.e. we show that a well-formed instantiation of a well-formed domain can be simulated by abstract states and actions.
A minor challenge is devising concrete mappings from PDDL world models to abstract states and the other way around, as well as as from PDDL plan actions to abstract actions.

%% {\footnotesize\noindent
%% \isacommand{definition}\isamarkupfalse%
%% "state\_to\_wm\ s\ =\ \{Atom\ (predAtm\ p\ xs)\ {\isacharbar}\ p\ xs.\ s\ (p,xs)\}"
%% }

%% {\footnotesize\noindent
%% \isacommand{definition}\isamarkupfalse%
%% "wm\_to\_state\ M\ =\ ({\isasymlambda}(p,xs).\ (Atom\ (predAtm\ p\ xs))\ {\isasymin}\ M)"
%% }

%% {\footnotesize\noindent
%% \isacommand{definition}\isamarkupfalse%
%% \ "pddl\_opr\_to\_act\ g\_opr\ s\ =\isanewline
%%  (let\ M\ =\ state\_to\_wm\ s\ in\isanewline
%% \ \ \ \ if\ (wm\_to\_state\ (close\_world\ M))\ {\isasymTurnstile}\isactrlsub =\ (precondition\ g\_opr)\ then\isanewline
%% \ \ \ \ \ \ Some\ (wm\_to\_state\ (apply\_effect\ (effect\ g\_opr)\ M))\isanewline
%% \ \ \ \ else\ None)"}

