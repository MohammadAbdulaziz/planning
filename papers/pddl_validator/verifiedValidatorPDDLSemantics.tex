\section{Mechanising the Semantics of Ground Actions}
\label{sec:STRIPS}
In this section, we formalise a semantics of ground actions, i.e. STRIPS extended with disjunction, negation, and equality in the preconditions and goals.
We follow the definitions by Lifschtiz~\cite{lifschitz1987semantics}, whenever possible.
In Section~\ref{sec:PDDL}, we use this formalisation as a basis for our PDDL semantics.

% 
% 
% We describe our formalisation of the semantics of ground actions and their execution in the theorem prover Isabelle/HOL. 
% We first formalise an abstract syntax for ground actions, where we effectively formalise STRIPS extended with disjunction, negation and equality.
% Then based on that we define the semantics, where we follow the definitions in Lifschtiz~\cite{lifschitz1987semantics}, whenever possible.

\subsection{Formalising the Abstract Syntax}

First, we define basic concepts like atoms, predicates, and formulae.
These concepts are also used for defining PDDL semantics, so we follow Kovacs'~\cite{kovacscomplete} PDDL grammar where appropriate.
In Isabelle/HOL, Kovacs' abstract syntax can elegantly be modelled as algebraic data types.
E.g. consider the following abstract syntax rules from Kovacs'.

{\footnotesize
\begin{verbatim}
<name> ::= <letter> <any char>*
<predicate> ::= <name>
<atomic formula (t)> ::= (<predicate> t*)
<atomic formula (t)> ::= (= t t)
\end{verbatim}
}

Those rules are modelled as follows in Isabelle/HOL as follows.

{\footnotesize\noindent
\isacommand{type\_synonym}\isamarkupfalse%
\ name\ =\ string
}

{\footnotesize\noindent
\isacommand{datatype}\isamarkupfalse%
\ predicate\ =\ Pred\ (name:\ name)
}

{\footnotesize\noindent
\isacommand{datatype}\isamarkupfalse%
\ {\isacharprime}t\ atom\ =\ predAtm\ (predicate:\ predicate)\ (arguments:\ "{\isacharprime}t\ list")\isanewline
\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ {\isacharbar}\ Eq\ (lhs:\ {\isacharprime}t)\ (rhs:\ {\isacharprime}t)%
}

That is, our abstract syntax does not detail the structure of names, but models them as strings. 
For predicates, we use a datatype with a single constructor \fquot{Pred}, which contains a \fquot{name}. The Isabelle notation \fquot{name:} 
defines a selector function, i.e. \fquot{name\ p} is the name of predicate \fquot{p}. For atomic formulae, which we call atoms here, we use a datatype
with two constructors, one per rule in the grammar. Moreover, the parameter \fquot{t} in Kovacs' grammar is modelled as a type parameter \fquot{{\isacharprime}t}. 
E.g. \fquot{object atom} is the type of ground atoms, and \fquot{term atom} is the type of atoms over terms.

To model formulae, we use an existing Isabelle/HOL formalisation of propositional logic by Michael and Nipkow~\cite{MichaelisN-TYPES17}. 
Formulae are parameterised over atoms, and consist of the standard connectives $\wedge, \vee, \rightarrow, \neg$ and $\bot$ (i.e. falsum).
For example, the type \fquot{object atom formula} represents ground formulae.



An effect is a pair of sets of formulae to be added and deleted.
We restrict the formulae to predicate atoms only, since non-atomic effects are practically irrelevant. 

\vspace{.7ex}
{\footnotesize\noindent
\isacommand{datatype}\isamarkupfalse%
\ {\isacharprime}t\ effect\ =\ Effect\isanewline
\ \ (adds:\ "{\isacharprime}t\ atom\ formula\ list")\isanewline
\ \ (dels:\ "{\isacharprime}t\ atom\ formula\ list")%
}

The restriction on the add and delete formulae to predicate atoms is modelled by a well-formedness condition.
A ground action (a.k.a. operator) consists of a precondition and an effect. 

\vspace{.7ex}
{\footnotesize\noindent
\isacommand{datatype}\isamarkupfalse%
\ ground\_action\ =\ Ground\_Action\isanewline
\ \ (precondition:\ "object\ atom\ formula")\isanewline
\ \ (effect:\ "object\ ast\_effect")%
}

\subsection{The Semantics of STRIPS + Negation and Equality}

We define a world model to be a set of ground formulae. 

{\footnotesize\noindent
\ \ \isacommand{type\_synonym}\isamarkupfalse%
\ world\_model\ =\ "object\ atom\ formula\ set"%
}

States resulting from action execution are ``basic' world models containing only predicate atoms instead of arbitrary formulae.


{\footnotesize\noindent
\ \ \isacommand{definition}\isamarkupfalse%
\ "wm\_basic\ M\ {\isasymequiv}\ {\isasymforall}a{\isasymin}M.\ is\_predAtom\ a"
}

To define entailment, the basic world model is closed by adding the negations of all predicates not in the basic model, as well as all equalities and inequalities:

\vspace{.7ex}
{\footnotesize\noindent
\isacommand{definition}\isamarkupfalse%
\ close\_world\ ::\ "world\_model\ {\isasymRightarrow}\ world\_model"\ \isakeyword{where}\isanewline
"close\_world\ M\ =\isanewline
\ \ M\ {\isasymunion}\ \{\isactrlbold {\isasymnot}(Atom\ (predAtm\ p\ as))\ {\isacharbar}\ p\ as.\ Atom\ (predAtm\ p\ as)\ {\isasymnotin}\ M\}\isanewline
\ \ {\isasymunion}\ \{Atom\ (Eq\ a\ a)\ {\isacharbar}\ a.\ True\}\ {\isasymunion}\ \{\isactrlbold {\isasymnot}(Atom\ (Eq\ a\ b))\ {\isacharbar}\ a\ b.\ a{\isasymnoteq}b\}"
}

We write \fquot{M\ \isactrlsup c{\isasymTTurnstile}\isactrlsub =\ {\isasymphi}} for \fquot{close\_world\ M\ {\isasymTTurnstile}\ {\isasymphi}}, where {\isasymTTurnstile} is propositional logic entailment from Michael and Nipkow~\cite{MichaelisN-TYPES17}. 

An effect is applied to a basic world model by first removing the delete-predicates, and then adding the add-predicates. 

\vspace{.7ex}
{\footnotesize\noindent
\isacommand{fun}\isamarkupfalse%
\ apply\_effect\ ::\ "object\ ast\_effect\ {\isasymRightarrow}\ world\_model\ {\isasymRightarrow}\ world\_model"\isanewline
\isakeyword{where}\ "apply\_effect\ (Effect\ a\ d)\ s\ =\ (s\ -\ set\ d)\ {\isasymunion}\ (set\ a)"%
}

Note that \isacommand{fun} in Isabelle/HOL allows pattern matching and (terminating) recursive definitions.

A valid ground action sequence $\alpha_1\ldots \alpha_n$ connecting an initial world model $M=M_1$ and a world model $M_{n+1}=M'$ is a sequence of actions 
such that there are intermediate models $M_i$, each $M_i$ entails the precondition of $\alpha_i$ (the action is enabled), and $M_{i+1}$ is obtained from
$M_i$ by applying the effect of $\alpha_i$.
In Isabelle/HOL, this is most elegantly modelled as a recursive function, leaving implicit the intermediate models:

\vspace{.7ex}
{\footnotesize\noindent
\isacommand{fun}\isamarkupfalse%
\ ground\_action\_path\isanewline
\ \ ::\ "world\_model\ {\isasymRightarrow}\ ground\_action\ list\ {\isasymRightarrow}\ world\_model\ {\isasymRightarrow}\ bool"\isanewline
\isakeyword{where}\isanewline
\ \ "ground\_action\_path\ M\ {\isacharbrackleft}{\isacharbrackright}\ M{\isacharprime}\ {\isasymlongleftrightarrow}\ (M\ =\ M{\isacharprime})"\isanewline
\isacharbar \ "ground\_action\_path\ M\ ({\isasymalpha}{\isacharhash}{\isasymalpha}s)\ M{\isacharprime}\ {\isasymlongleftrightarrow}\ M\ \isactrlsup c{\isasymTTurnstile}\isactrlsub =\ precondition\ {\isasymalpha}\isanewline
\ \ {\isasymand}\ (ground\_action\_path\ (apply\_effect\ (effect\ {\isasymalpha})\ M)\ {\isasymalpha}s\ M{\isacharprime})"
}


% 
% To model the semantics of formula wrt.\ negation and equality, we close a basic world model by adding 
% 
% 
% 
% We define the semantics of propositional STRIPS on top of the abstract syntax defined above.
% We formalise what it means to execute an operator in a given world model \fquot{M} as follows:
% 
% {\footnotesize\noindent
% \isacommand{fun}\isamarkupfalse%
% \ apply\_effect\ \isakeyword{where}\ "apply\_effect\ (Effect\ (adds)\ (dels))\ M =\isanewline (M\ -\ (set\ dels))\ {\isasymunion}\ ((set\ adds))"
% }
% 
% \noindent Note: here we use \isacommand{fun}, which, unlike \isacommand{definition}, supports pattern matching and (terminating) recursive definitions.
% Note the resemblance of the above definition with Equation~2 in~\cite{lifschitz1987semantics}.
% 
% % , and not \isacommand{definition}, as we do structural induction on \fquot{ast\_effect}'s record components.
% To define the notion of a valid plan, we first formalise what it means for a precondition to hold in a world model or a state.
% Since preconditions are formulae we use the logical concepts of entailment to formalise that.
% Michaelis and Nipkow define the semantic and syntactic entailment operators {\isasymTTurnstile} and $\vdash$, respectively, in their formalisation of propositional logic, as follows.
% 
% entail\_def
% 
% sem\_entail\_def
% 
% In words, $\vdash$ is equivalent to the notion of a valuation (i.e. a logical state) satisfying a formula, and
% the {\isasymTTurnstile} operator denotes that a set of formulae (i.e. a world-model, except we restrict world-models to sets of atomic formulae\peter{Is this true}) entails a formula.
% %(equivalent to the $\vdash$ operator in~\cite{lifschitz1987semantics}) and  .
% Based on those concepts, we define a valid plan as follows.
% 
% \peter{Refresh}
% {\footnotesize\noindent
% \isacommand{fun}\isamarkupfalse%
% \ ast\_action\_inst\_path\ \isakeyword{where}\isanewline
% \ \ "ast\_action\_inst\_path\ M\ {\isacharbrackleft}{\isacharbrackright}\ M{\isacharprime}\ {\isasymlongleftrightarrow}\ (M\ =\ M{\isacharprime})"\isanewline
% {\isacharbar}\ "ast\_action\_inst\_path\ M\ ({\isasymalpha}{\isacharhash}{\isasymalpha}s)\ M{\isacharprime}\ {\isasymlongleftrightarrow}\ M\ {\isasymTTurnstile}\ precondition\ {\isasymalpha}\ \isanewline
% \ \ {\isasymand}\ (ast\_action\_inst\_path\ (apply\_effect\ (effect\ {\isasymalpha})\ M)\ {\isasymalpha}s\ M{\isacharprime})"
% }
% 
% 
% In the above definition \fquot{close\_world M} denotes the world-model \fquot{M $\cup\;\{\neg a.\; a \notin M\}\; \cup\; \{a = a.\; a = a \}$}, which is world-model \fquot{M} but closed to include the negations of all predicate atoms that are not specified in \fquot{M} and equalities of all structurally equal objects.
% This closing of the world-model is to enable negations, implications and equalities in the preconditions of the ground actions.
% Note how this deviates from standard STRIPS semantics where negations and equalities are not supported (e.g. Eq.~3 in~\cite{lifschitz1987semantics}) where \fquot{M\ {\isasymTTurnstile}\ precondition\ {\isasymalpha}} is used instead for the enabledness check of an operator.
% 
% %% Note STRIPS system accepting a plan (Eq.~3 in~\cite{lifschitz1987semantics}) as follows.
% 
% %% Our definition uses structural recursion over the list of operations, which is more natural in the context 
% %% of HOL than the index-based recursive definition used in~\cite{lifschitz1987semantics}. 
% %% Thus, only the initial and last world models \fquot{M} and \fquot{M'} (equivalent to $M_0$ and $M_N$ in~\cite{lifschitz1987semantics}) are explicitly specified, and intermediate models only occur implicitly in the recursion.

\section{Formalised Semantics of a PDDL Fragment}
\label{sec:PDDL}
%TODO: Convert verbatims to proper isabelle notation, once examples are stable and fixed!

We now specify semantics of (a fragment of) PDDL, by adding action schemata and types to the semantics of ground actions. 
An action schema (sometimes simply called action) is parameterised over object-valued variables. Substituting these variables for actual objects yields a ground action.
Moreover, a hierarchical type system restricts the applicability of predicates to objects.
We first formalise the abstract syntax of PDDL following Kovacs' grammar, and then define its semantics.

\subsection{Abstract Syntax}

A term is an object or a variable.

{\footnotesize\noindent
\isacommand{datatype}\isamarkupfalse%
\ "term"\ =\ VAR\ variable\ {\isacharbar}\ CONST\ object
}

A type is a list of type names (Either-type).

{\footnotesize\noindent
\isacommand{datatype}\isamarkupfalse%
\ type\ =\ Either\ (primitives:\ "name\ list")%
}

An action schema has a name and a list of typed parameters, as well as a precondition and an effect, which 
are defined over terms (instead of objects for ground actions).

\vspace{.7ex}
{\footnotesize\noindent
\isacommand{datatype}\isamarkupfalse%
\ ast\_action\_schema\ =\ Action\_Schema\isanewline
\ \ (name:\ name)\isanewline
\ \ (parameters:\ "(variable\ {\isasymtimes}\ type)\ list")\isanewline
\ \ (precondition:\ "term\ atom\ formula")\isanewline
\ \ (effect:\ "term\ ast\_effect")%
}

Finally, an action of a plan is a name (referencing an action schema) and a list of (argument) objects:

{\footnotesize\noindent
\isacommand{datatype}\isamarkupfalse\ plan\_action\ =\ PAction\ (name:\ name)\ (args:\ "object\ list")
}

Similarly, we model the remaining abstract syntax, based on Kovacs' grammar\footnote{We sometimes use slightly different names than Kovacs.}.
We only display the top-level entities for a PDDL domain and instance here:

\vspace{1ex}\noindent
\begin{minipage}{.49\columnwidth}
{\footnotesize\noindent
\isacommand{datatype}\isamarkupfalse%
\ ast\_domain\ =\ Domain\isanewline
\ \ (types:\ "(name\ {\isasymtimes}\ name)\ list")\isanewline
\ \ (predicates:\ "predicate\_decl\ list")\isanewline
\ \ (consts:\ "(object\ {\isasymtimes}\ type)\ list")\isanewline
\ \ (actions:\ "ast\_action\_schema\ list")
}
\end{minipage}
\begin{minipage}{.49\columnwidth}
{\footnotesize\noindent
\isacommand{datatype}\isamarkupfalse%
\ ast\_problem\ =\ Problem\isanewline
\ \ (domain:\ ast\_domain)\isanewline
\ \ (objects:\ "(object\ {\isasymtimes}\ type)\ list")\isanewline
\ \ (init:\ "object\ atom\ formula\ list")\isanewline
\ \ (goal:\ "object\ atom\ formula")
}
\end{minipage}
\vspace{1ex}

A domain consists of a list of type declarations, which are pairs of declared types and supertypes, as well as predicate declarations, constant declarations, and action schemas.
A problem refers to a domain, and additionally contains object declarations, as well as an initial state and a goal.
A well-formedness condition will restrict the formulae of the initial state to be only predicate atoms\footnote{Kovacs grammar also allows negated atoms and equalities here, which, however, is meaningless under our closed world assumption.}. 



\subsection{Well-Formedness}
The next step towards specification of a PDDL semantics is to define well-formedness criteria.
For example, we require that action names are unique and that all predicates occurring in formulae or effects have been declared
and are applied to type-compatible terms.

While many of the well-formedness conditions are straightforward, some leave room for ambiguities that have to be resolved in a formal specification.
For example, Kovacs' grammar formally allows for types being declared with an Either-supertype.
The semantics of this is not clear; different validators seem to have different interpretations of what this means, and thus, we do not support this feature.
In our semantics of typing each type declaration \fquot{tn -- tn'} introduces an edge $tn \leftarrow tn'$ in a subtype relation, where $tn$ and $tn'$ are type names.
A term of (Either-) type $oT$ can be substituted at a parameter position 
expecting type $T$ iff every type name in $oT$ is reachable from some type name in $T$ via the following subtype relation.

\vspace{.7ex}
{\footnotesize\noindent
\isacommand{definition}\isamarkupfalse%
\ of\_type\ ::\ "type\ {\isasymRightarrow}\ type\ {\isasymRightarrow}\ bool"\ \isakeyword{where}\isanewline
"of\_type\ oT\ T\ {\isasymequiv}\ set\ (primitives\ oT)\ {\isasymsubseteq}\ subtype\_rel\isactrlsup *\ {\isacharbackquote}{\isacharbackquote}\ set\ (primitives\ T)"%
}

Here, \fquot* denotes reflexive transitive closure, and \fquot{{\isacharbackquote}{\isacharbackquote}} is the image of a set under a relation.




% xxx, ctd here
% 
% 
% 
% We now give an overview of our formalised abstract syntax and semantics for PDDL, defined on top of the previous semantics for ground actions.
% We discuss extending the those ground action semantics to have action schemas and types.
% 
% We first formalise an abstract syntax for PDDL domains, PDDL problems, and plans, following Kovacs' grammar.\footnote{The syntax of plans is not specified in Kovacs' grammar.}
% %The PDDL concepts of \fquot{variable} and \fquot{object} are all formalised as type synonyms of \fquot{name}, i.e. as strings.
% ------
% 
% \peter{Update!}
% A PDDL \fquot{type} is modelled as a list of primitive type names, ``object'' being interpreted specially.
% 
% ------
% 
% To model action schemas we first formalise the concept of a PDDL ``term'', which we restrict to objects/constants and variables.
% 
% term\_def
% 
% Actions are then defined as the following record datatype:
% 
% ------
% 
% \peter{Update!}
% {\footnotesize\noindent
% \isacommand{datatype}\isamarkupfalse%
% \ ast\_action\_schema\ =\ Action\_Schema\isanewline
% \ \ \ \ (name:\ name)\isanewline
% \ \ \ \ (parameters:\ "(variable\ {\isasymtimes}\ type)\ list")\isanewline
% \ \ \ \ (precondition:\ "(variable\ atom)\ formula")\isanewline
% \ \ \ \ (effect:\ "(variable\ atom)\ formula\ ast\_effect") 
% }
% 
% ------
% 
% This record has four components: \begin{enumerate*} \item the action name, \item the parameters of the action, which is a list of (term $\times$ type) pairs, where a term can be either a variable or a constant, \item the precondition of the action, which is a propositional formula, and \item the effect, which is an instantiation of \fquot{ast\_effect} with propositional formulae.
% \end{enumerate*}
% 
% An action of a plan is a name (referencing an action schema) and a list of (argument) objects:
% 
% {\footnotesize\noindent
% \isacommand{datatype}\isamarkupfalse\ plan\_action\ =\ PAction\ (name:\ name)\ (args:\ "object\ list")
% }
% 
% %Other elements of the abstract PDDL syntax are modelled similarly, but omitted here due to space constraints.
% 
% We similarly model other elements of the abstract syntax of PDDL domains and problems as datatype records, in the obvious manner.
% E.g., a predicate declaration is modelled as a record with the predicate name and a list of types that represents the types of its arguments.
% % However, we will not list all of the datatypes due to space limits.
% A domain is modelled as a record with three components: a list of types, a list of predicate declarations and a list of action schemas.
% A PDDL problem instance is modelled with a record that has: \begin{enumerate*} \item a domain, \item a list of (object x type) pairs, \item a list of formulae representing the initial world model, and \item one formula representing the goal.\end{enumerate*}
% Lastly, a plan action is modelled as a record with the action name and a list of objects, that instantiate the corresponding action schema.
% 
% We note that our abstract syntax specification removes some redundancies from Kovacs' grammar.
% For example, Kovacs' grammar makes a special case for an empty precondition, but we do not include this special case since it can be easily mapped to the formula $\neg \bot$ by the parser.
% Generally speaking, removing purely syntactic shortcuts during parsing makes our specification more concise and readable.
% 
% \subsection{Well-Formedness}
% 
% After specifying the abstract syntax, we specify the semantics of different features of PDDL, utilising the semantics of ground actions specified above.
% However, the main challenge is to formalise the semantics of proper instantiation and well-typing.
% This is dealt with by imposing well-formedness conditions on PDDL domains, problems and plans.
% %% Additionally, those well-formedness conditions ensure that the STRIPS soundness theorem extends to PDDL problem instances.
% %% The soundness theorem states that a STRIPS model would be sound if all non-atomic formulae in the initial world model and in the add effects are satisfied in all states of the world. 
% The first well-formedness conditions we deal with are for world-models and action effects (i.e. \fquot{ast\_effect}).
% World-models  are restricted to sets of well-formed atomic formulae, and effects are restricted to only have atoms in its add and delete lists.%
% \footnote{This is to avoid the soundness complications unveiled by Lifschitz~\cite{lifschitz1987semantics}, who showed that any non-atomic formulae in the effects or in a world-model needs to be a tautology.}
% % @Mohammad: I commented this sentence out, as I do not understand it, and it obfuscates the reference of the next sentence.
% 
% \peter{Update!}
% {\footnotesize\noindent
% \isacommand{definition}\isamarkupfalse%
% \ "wf\_world\_model\ M\ =\ ({\isasymforall}f{\isasymin}M.\ \ wf\_fmla\_atom\ f)"
% 
% \noindent
% \isacommand{fun}\isamarkupfalse%
% \ "wf\_effect\ (Effect\ adds\ dels)\ {\isasymlongleftrightarrow}\ \isanewline
% \ \ \ \ ({\isasymforall}ae{\isasymin}set\ adds.\ wf\_fmla\_atom\ ae)\ \isanewline
% {\isasymand}\ ({\isasymforall}de{\isasymin}set\ dels.\ \ wf\_fmla\_atom\ de)"
% }
% % @Mohammad: I dropped an objT here, making it inconsistent with the Isabelle formalisation on purpose! We also hide the fact that the concepts above have an implicit val_ty parameter!
% 
% A well-formed predicate atom (i.e. \fquot{predAtm}) requires a predicate with the same name to be declared in the domain, and that the arguments of the predicate match the types in the declaration.
% 
% %% {\footnotesize\noindent
% %% \ \ \ \ \ \ \isacommand{fun}\isamarkupfalse%
% %% \ wf\_atom\ ::\ "{\isacharprime}val\ atom\ {\isasymRightarrow}\ bool"\ \ \isakeyword{where}\isanewline
% %% \ \ \ \ \ \ \ \ "wf\_atom\ in\_atom\ =\ \isanewline
% %% \ \ \ \ \ \ \ \ \ \ (case\ in\_atom\ of\isanewline
% %% \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ (predAtm\ p\ vs)\ {\isasymRightarrow}\ (\isanewline
% %% \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ case\ sig\ p\ of\ \isanewline
% %% \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ None\ {\isasymRightarrow}\ False\isanewline
% %% \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ {\isacharbar}\ Some\ Ts\ {\isasymRightarrow}\ list\_all{\isadigit{2}}\ is\_of\_type\ vs\ Ts)\isanewline
% %% \ \ \ \ \ \ \ \ \ \ \ \ \ \ {\isacharbar}\ (Eq\ v{\isadigit{1}}\ v{\isadigit{2}})\ {\isasymRightarrow}\ ty\_val\ v{\isadigit{1}}{\isasymnoteq}None\ {\isasymand}\ ty\_val\ v{\isadigit{2}}{\isasymnoteq}None)"
% %% }
% 
% %% In the definition above, 
% 
% Preconditions and goal descriptions must be well-formed formulae defined as follows:
% 
% {\footnotesize\noindent
% \ \ \ \ \ \isacommand{fun}\isamarkupfalse%
% \ wf\_fmla\ \isakeyword{where}\isanewline
% \ \ \ \ \ "wf\_fmla\ (Atom\ a)\ {\isasymlongleftrightarrow}\ wf\_atom\ a"\isanewline
% \ \ \ \ {\isacharbar}\ "wf\_fmla\ ({\isasymphi}{\isadigit{1}}\ \isactrlbold {\isasymand}\ {\isasymphi}{\isadigit{2}})\ {\isasymlongleftrightarrow}\ (wf\_fmla\ {\isasymphi}{\isadigit{1}}\ {\isasymand}\ wf\_fmla\ {\isasymphi}{\isadigit{2}})"\ \ \isanewline
% \ \ \ \ {\isacharbar}\ "wf\_fmla\ ({\isasymphi}{\isadigit{1}}\ \isactrlbold {\isasymor}\ {\isasymphi}{\isadigit{2}})\ {\isasymlongleftrightarrow}\ (wf\_fmla\ {\isasymphi}{\isadigit{1}}\ {\isasymand}\ wf\_fmla\ {\isasymphi}{\isadigit{2}})"\ \ \isanewline
% \ \ \ \ {\isacharbar}\ "wf\_fmla\ (\isactrlbold {\isasymnot}{\isasymbottom})\ {\isasymlongleftrightarrow}\ True"\isanewline
% \ \ \ \ {\isacharbar}\ "wf\_fmla\ \_\ {\isasymlongleftrightarrow}\ False"
% }
% 
% 
% Well-formedness of the other syntax elements is defined in the obvious way.
% For example, an action schema is well-formed iff its parameters have different names, its precondition is a well-formed formula and its effect is well-formed.
% We do not list these definitions due to lack of space, however.
% 
% \peter{Add the type checking description?}
% \peter{May be add some other well-formedness defs?}

\subsection{Execution Semantics}

Finally, we define the semantics of plans within our fragment of PDDL: by instantiating the action schema referenced by a plan action we obtain a ground action, which is then applied to the world model using the execution semantics defined in Section~\ref{sec:STRIPS}.
Additionally, we check that each plan action is well-formed, i.e. that its arguments' types can be substituted for the action schema's parameter types. 
Note that we assume an implicitly fixed PDDL instance $P$ with domain $D$ for the following definitions.


\vspace{.7ex}
{\footnotesize\noindent
\isacommand{definition}\isamarkupfalse%
\ plan\_action\_path\isanewline
::\ "world\_model\ {\isasymRightarrow}\ plan\_action\ list\ {\isasymRightarrow}\ world\_model\ {\isasymRightarrow}\ bool"\ \isakeyword{where}\isanewline
"plan\_action\_path\ M\ {\isasympi}s\ M{\isacharprime}\ =\isanewline
\ \ \ \ (({\isasymforall}{\isasympi}\ {\isasymin}\ set\ {\isasympi}s.\ wf\_plan\_action\ {\isasympi})\isanewline
\ \ {\isasymand}\ ground\_action\_path\ M\ (map\ resolve\_instantiate\ {\isasympi}s)\ M{\isacharprime})"%
}

In this definition, we first ensure that every plan action is well-formed. Then, we resolve and instantiate the actions of the plan, 
obtaining a sequence of ground actions, which is checked against the basic semantics for ground actions.

With the above definition, it is straightforward to define a valid plan as a plan that transforms the initial world model \fquot{I} to a world model \fquot{M'} that entails the problem's goal:

\vspace{.7ex}
{\footnotesize\noindent
\isacommand{definition}\isamarkupfalse%
\ valid\_plan\ ::\ "plan\ {\isasymRightarrow}\ bool"\ \isakeyword{where}\isanewline
"valid\_plan\ {\isasympi}s\ {\isasymequiv}\ {\isasymexists}M{\isacharprime}.\ plan\_action\_path\ I\ {\isasympi}s\ M{\isacharprime}\ {\isasymand}\ M{\isacharprime}\ \isactrlsup c{\isasymTTurnstile}\isactrlsub =\ (goal\ P)"
}

% Finally, we define what the execution of an action and a plan means for PDDL.
% Except for taking care of well-formedness and instantiation, execution semantics for PDDL are exactly like ground actions.
% E.g., a plan action is enabled in a world-model iff it is well-formed and the corresponding instantiated action schema is enabled in the given world-model.
% This is formally stated as follows, for world-model \fquot{M} and plan action \fquot{$\pi$}. 
% 
% \peter{Update!}
% {\footnotesize\noindent
% \isacommand{definition}\isamarkupfalse
% \ "plan\_action\_enabled\ $\pi$\ M\ {\isasymlongleftrightarrow}\isanewline
% wf\_plan\_action\ $\pi$\ \ {\isasymand}\ M\ {\isasymTTurnstile}\ precondition\ (resolve\_instantiate\ $\pi$)"
% }
% 
% Here, the well-formedness check ensures that \fquot{$\pi$} instantiates a declared action schema with objects of the right types, and \fquot{resolve\_instantiate} instantiates the corresponding action schema with the objects given as parameters. 
% Similarly we extend the ground action concepts of action execution of a path \fquot{$\pi$s} between two world-models.
% 
% \peter{Update!}
% {\footnotesize\noindent
% \isacommand{definition}\isamarkupfalse%
% \ "execute\_plan\_action\ $\pi$\ M\ =\isanewline
% \ (apply\_effect\ (effect\ (resolve\_instantiate\ $\pi$))\ M)"
% 
% 
% \peter{Update!}
% \noindent\isacommand{definition}\isamarkupfalse%
% \ "plan\_action\_path\ M\ {\isasympi}s\ M{\isacharprime}\ =\ \isanewline
% \ \ \ \ (({\isasymforall}{\isasympi}\ {\isasymin}\ set\ {\isasympi}s.\ wf\_plan\_action\ {\isasympi})\ \isanewline
% {\isasymand}\ ast\_action\_inst\_path\ M\ (map\ resolve\_instantiate\ {\isasympi}s)\ M{\isacharprime})"%
% }
% 
% It is then straightforward to define, for a problem \fquot{P}, when a plan \fquot{$\pi$s} is valid if executed from a given state:
% 
% \peter{Update!}
% {\footnotesize\noindent
% \isacommand{definition}\isamarkupfalse%
% \ "valid\_plan\_from\ M\ {\isasympi}s\ =\isanewline
%  ({\isasymexists}M{\isacharprime}.\ plan\_action\_path\ M\ {\isasympi}s\ M{\isacharprime}\ {\isasymand}\ M{\isacharprime}\ {\isasymTTurnstile}\ (goal\ P))"
% }
% 
% Finally, a plan is valid if it is valid from the initial state \fquot{I}:
% 
% {\footnotesize\noindent
% \isacommand{definition}\ "valid\_plan\ {\isasymequiv}\ valid\_plan\_from\ I"
% }
