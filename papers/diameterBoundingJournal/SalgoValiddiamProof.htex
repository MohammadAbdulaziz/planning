\begin{mylem}
\label{lem:Sworks}
For any $\delta$ and $\vs$ where $\proj{\delta}{\vs}$ is acyclic, $\state \in \uniStates(\delta)$, and $\as \in \ases{\delta}$, there is $\as'$ such that $\exec{\state}{\as} = \exec{\state}{\as'}$ and $|\as'| \leq \Sbrace{d}(\proj{\state}{\vs},\vs,\delta)$.%
\footnote{In the rest of this proof we omit the $\vs$ and/or $\delta$ arguments from $\deriv{}{}{}$ and $\Sname$ as they do not change.}
\end{mylem}
\begin{proof}
The proof is by induction on $\derivabbrev{\state}{\as}{\vs}$.
The base case where $\derivabbrev{\state}{\as}{\vs} = []$ is trivial.
In the step case we have that $\derivabbrev{\state}{\as}{\vs} = \state'::\sll$ and the induction hypothesis: for any $\state_\IndHyp \in \uniStates(\delta)$, and $\as_\IndHyp \in \ases{\delta}$ if $\derivabbrev{\state_\IndHyp}{\as_\IndHyp}{\vs} = \sll$ then there is $\as{_\IndHyp}'$ where $\exec{\state_\IndHyp}{\as_\IndHyp} = \exec{\state_\IndHyp}{\as{_\IndHyp}'}$ and $|\as{_\IndHyp}'| \leq \Sbrace{d}(\proj{\state_\IndHyp}{\vs})$.

Since $\derivabbrev{\state}{\as}{\vs} = \state'::\sll$, we have $\as_1,\act$ and $\as_2$ satisfying the conclusions of Proposition~\ref{prop:vsDerivCons}.
Based on conclusion i, ii, and iii of Proposition~\ref{prop:vsDerivCons} and Proposition~\ref{prop:vsDerivCat} we have $\derivabbrev{\state'}{\as_2}{\vs} = \sll$.
Accordingly, letting $\state_\IndHyp$, and $ \as_\IndHyp$ from the inductive hypothesis be $\state'$, and $\as_2$, respectively, there is $\as_2'$ such that $\exec{\state'}{\as_2} = \exec{\state}{\as_2'}$ and $|\as_2'| \leq \Sbrace{d}(\proj{\state'}{\vs})$.$^\dagger$

From conclusion ii of Proposition~\ref{prop:vsDerivCons} and conclusion ii of Proposition~\ref{prop:vsConstdiam} there is $\as_1'$ where $\exec{\state}{\as_1} = \exec{\state}{\as_1'}$ and $|\as_1'| \leq d(\snapshot{\delta}{\proj{\state}{\vs}})$.
Letting $\as' = \as_1'\#\act::\as_2'$, from conclusions iii and iv of Proposition~\ref{prop:vsDerivCons} and $\dagger$ we have $\exec{\state}{\as} = \exec{\state}{\as'}$ and $|\as'| \leq d(\snapshot{\delta}{\proj{\state}{\vs}}) + 1 + \Sbrace{d}(\proj{\state'}{\vs})$.$\ddagger$

Lastly, from conclusion i of Proposition~\ref{prop:vsConstdiam} and conclusion ii of Proposition~\ref{prop:vsDerivCons} we have $\proj{\state}{\vs} = \proj{\exec{\state}{\as_1}}{\vs} = \proj{\exec{\state}{\as_1'}}{\vs}$ and accordingly $\exec{\proj{\state}{\vs}}{\proj{\act}{\vs}} = \proj{\state'}{\vs}$.
Based on that we have $\proj{\state'}{\vs} \in \succstates{\proj{\state}{\vs}}{\proj{\delta}{\vs}}$.
Then from Proposition~\ref{prop:SalgoLeqSalgoChild} and $\ddagger$ we have $|\as'| \leq \Sbrace{d}(\proj{\state}{\vs})$.
\end{proof}

\noindent Theorem~\ref{thm:SalgoValiddiam} follows from Lemma~\ref{lem:Sworks} and Definitions~\ref{def:diamrd}~and~\ref{def:salgo}.
