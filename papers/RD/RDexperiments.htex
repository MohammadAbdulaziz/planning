\section{Further Experiments}
\begin{figure*}[h]
\centering
\begin{minipage}[b]{0.24\textwidth}
\centering
       \includegraphics[width=1\textwidth,height=0.8\textwidth]{log-labelled-Balgo-RD-vs-Balgo-TD_ARB-bounds.png}
\end{minipage}
\begin{minipage}[b]{0.24\textwidth}
\centering
        \includegraphics[width=1\textwidth,height=0.8\textwidth]{log-labelled-Balgo-TD_ARB-vs-Balgo-RD-runtimes.png}
\end{minipage}
\begin{minipage}[b]{0.24\textwidth}
        \includegraphics[width=1\textwidth,height=0.8\textwidth]{log-labelled-Balgo-RD-TDgt2-vs-Balgo-RD-runtimes.png}
\end{minipage}
\begin{minipage}[b]{0.24\textwidth}
        \includegraphics[width=1\textwidth,height=0.8\textwidth]{log-labelled-Balgo-RD-TDgt2-vs-Balgo-RD-TDgt2-SSle50bound-bounds.png}
\end{minipage}
\begin{minipage}[b]{0.24\textwidth}
\centering
        \includegraphics[width=1\textwidth,height=0.8\textwidth]{log-labelled-Balgo-RD-TDgt2-SSle50bound-vs-Balgo-RD-TDgt2-runtimes.png}
\end{minipage}
\begin{minipage}[b]{0.24\textwidth}
\centering
       \includegraphics[width=1\textwidth,height=0.8\textwidth]{log-labelled-Balgo-RD-TDgt2-SSle50bound-vs-Balgo-TD_ARB-bounds.png}
\end{minipage}
\begin{minipage}[b]{0.24\textwidth}
\centering
\includegraphics[width=1\textwidth,height=0.8\textwidth]{log-labelled-Mp_noshort-0-plan-withbound-Balgo-RD-TDgt2-vs-Mp_noshort-0-plan-withbound-Balgo-TD_ARB-planning-runtimes.png}
\end{minipage}
\begin{minipage}[b]{0.24\textwidth}
\centering
\includegraphics[width=1\textwidth,height=0.8\textwidth]{log-labelled-Mp_noshort-0-plan-withbound-Balgo-RD-TDgt2-vs-Mp_noshort-0-plan-withbound-Balgo-TD_ARB-total-runtimes.png}
\end{minipage}
\vspace{-1.5ex}
     \caption{\label{fig:boundcompareTDvsRD}Different scatter-plots comparing the different bounding algorithms in terms of the quality of their bounds, the needed bound computation time, and the planning time using those bounds as a horizon for {\sc Mp}.}
\vspace{-3ex}
\end{figure*}
\input{boundsTable}


Note that, although Encoding~\ref{enc:rdnew} is more efficient than Encoding~\ref{enc:rdBiere}, the number of problems that were successfully bounded is less when using $\recurrenceDiam$ as a base case function compared to $\traversalDiam$, since $\recurrenceDiam$ can take exponentially longer to compute than $\traversalDiam$.
Figure~\ref{fig:boundcompareTDvsRD} shows that running time difference for problems successfully bounded using both base case functions.
% but is most extreme in Elevators, Zeno, Hiking, WoodWorking and Storage.
Also Table~\ref{table:boundsDoms} shows this in terms of the numbers of problems bounded within 20 minutes, when using $\recurrenceDiam$ vs.\ $\traversalDiam$.

One thing we observe during our experiments is that many of the base case systems have traversal diameters that are 1 or 2.
We exploit that to improve the running time by devising a base case function that will only invoke the expensive computation of $\recurrenceDiam$ in case $\traversalDiam$ is greater than 2.
\renewcommand{\thennew}{\mbox{\upshape \textsf{thn}}}
\renewcommand{\elsenew}{\mbox{\upshape \textsf{els}}}
\begin{mydef}
$\basecasefun_1(\delta) = \ifnew\;2 < \traversalDiam(\delta)\;\thennew\;\recurrenceDiam(\delta)\;\elsenew\;\traversalDiam(\delta)$.
\end{mydef}
Limiting the computation of $\recurrenceDiam$ as in the base case function $\basecasefun_1$ significantly reduces the bound computation time.
This leads to to more problems being successfully bounded as shown in col.4 of Table~\ref{table:boundsDoms}, compared to when $\recurrenceDiam$ is used.
The substantially improved running time is shown in Figure~\ref{fig:boundcompareTDvsRD}.
We also observe that bounds computed using $\recurrenceDiam$ as a base case function are exactly the same as those computed using $\basecasefun_1$, for all problems on which they terminate.
Thus, this improvement in running time does not come at the cost of looser bounds.
Indeed, we conjecture the following.
\begin{myconj}
\label{conj:tdeqrd112}
For any factored transition system $\delta$, if $\traversalDiam(\delta)\in\{1,2\}$, then $\traversalDiam(\delta)=\recurrenceDiam(\delta)$.
\end{myconj}
%% Because of Conjecture~\ref{conj:tdeqrd112}, the value of $\basecasefun_1$ is the same as $\recurrenceDiam$.

%% Figure~\ref{fig:boundcompareRDvsB1} shows that bounds computed using $\basecasefun_1$ are the same as those computed when $\recurrenceDiam$ is used, and that bound computation using $\basecasefun_1$ as a base case function takes significantly less time than when $\recurrenceDiam$ is used.

Note, however, that the number of problems successfully bounded when using $\basecasefun_1$ as a base case function is still less than the number of problems bounded using $\traversalDiam$.
This is because the large computation cost of $\recurrenceDiam$ on the base cases on which it is invoked is still much more than the cost of computing $\traversalDiam$.
Another technique to improve the bound computation time is to limit the computation of $\recurrenceDiam$ to problems whose state spaces' sizes are bound by a constant.
This is done with the following base case function.
\begin{mydef}
$\basecasefun_2(\delta) = \ifnew 50<\expbound(\delta)\;\thennew\;\basecasefun_1(\delta)\;\elsenew\;\traversalDiam(\delta)$.
\end{mydef}
We set 50 as an upper limit on the state space size as more than 95\% of abstractions whose $\recurrenceDiam$ was successfully computed had values less than 50. %by recording the size of the largest state space for which the function $\basecasefun_1$ could successfully terminate.

As shown in col. 5 of Table~\ref{table:boundsDoms}, the number of problems that are successfully bounded within 20 minutes when $\basecasefun_2$ is used as a base case function is substantially more than those when $\basecasefun_1$ is used, especially in the domains where $\recurrenceDiam$ and $\basecasefun_1$ were less successful than $\traversalDiam$.
%This can be seen in 
However, the bounds computed when $\basecasefun_2$ is used are sometimes worse than those computed when $\basecasefun_1$ is used, like in the case of TPP.
Figure~\ref{fig:boundcompareTDvsRD} shows this bound degradation and bound computation time improvement for the problems on which both methods terminate.
Nonetheless, the bounds computed using $\basecasefun_2$ are still much better than $\traversalDiam$, as shown in Figure~\ref{fig:boundcompareTDvsRD}.
This is because there are abstractions whose recurrence diameter is computable within the timeout and whose state spaces have more than 50 states.
For those abstractions, $\traversalDiam$ computed instead of $\recurrenceDiam$, when $\basecasefun_2$ is used.
An interesting problem is adjusting the threshold in $\basecasefun_2$ to maximise the number of abstractions whose recurrence diameter can be computed within the timeout.
We do not fully explore this problem here.
\vspace{-2.5ex}

\paragraph{Using the bounds for SAT-based planning}
\input{solvedTables}

Table~\ref{table:solvedDoms} shows that the coverage of {\sc Mp} increases if we use, as horizons, the bounds computed with base case functions involving $\recurrenceDiam$, compared to when using $\traversalDiam$ as a base case function.
An exception is Zeno, where the better bound computation time using $\traversalDiam$ is decisive.
In addition to coverage, makes sense to take a look into how the exact running times compare as many problems are solved using any of the bounds.
The two plots at the bottom right of Figure~\ref{fig:boundcompareTDvsRD} show the time needed for computing a plan when using $\basecasefun_1$ vs.\ $\traversalDiam$ as base case functions for problems on which both methods succeed.
One plot shows that the planning time (excluding bound computation) using the tighter bounds is much smaller almost always, which is to be expected.
The other shows planning time including bound computation.
Interestingly, although the bound computation time is more when using $\basecasefun_1$ than $\traversalDiam$, tighter bounds always payoff for problems that need more than 5 seconds to solve.
\footnote{We omit a similar plot comparing $\basecasefun_2$ and $\traversalDiam$ due to space limits.}
Thus, time spent computing better bounds before planning is almost always well-spent.
%% Also Figures~\ref{fig:plantimeA}-\ref{fig:plantimeC} show the running time of the planner when different bounds are used as horizon, for problems where planning succeeds with the two compared bounds. 
%% It clearly confirms that computing tighter upper bounds mainly pays-off, despite the fact that computing those tighter bounds can take longer.

%% Also, using $\TDbound$ as a base case for $\Balgo$ allows {\sc Mp} to prove the unsolvability of an additional 4 problems from the domain \domain{newopen} and 52 problems from the domain \domain{HotelKey}, that it could not solve when $\expbound$ is used as a base case function.
%% w







%% \begin{figure}[t]
%% \centering
%% \begin{minipage}[b]{0.45\textwidth}
%% \centering
%%         \includegraphics[width=1\textwidth,height=0.6\textwidth]{log-labelled-M-seq-plan-nobound-vs-Mp_noshort-0-plan-withbound-Balgo-TD_ARB-planning-runtimes.png}
%% \end{minipage}
%% \begin{minipage}[b]{0.45\textwidth}
%% \centering
%%         \includegraphics[width=1\textwidth,height=0.6\textwidth]{log-labelled-M-seq-plan-nobound-vs-Mp_noshort-0-plan-withbound-Balgo-RD-TDgt2-planning-runtimes.png}
%% \end{minipage}
%% \begin{minipage}[b]{0.45\textwidth}
%% \centering
%%         \includegraphics[width=1\textwidth,height=0.6\textwidth]{log-labelled-M-seq-plan-nobound-vs-Mp_noshort-0-plan-withbound-Balgo-RD-TDgt2-SSle50bound-planning-runtimes.png}
%% \end{minipage}
%%      \caption{\label{fig:plantimeC}Comparison of planning running times when using $\traversalDiam$, $\basecasefun_1$, and $\basecasefun_2$ as base case functions for bounding vs planning without a bound.}
%% \end{figure}
