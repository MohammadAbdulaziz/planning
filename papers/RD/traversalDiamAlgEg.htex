\begin{myeg}
\label{eg:traversalDiamAlg}
Consider the projection $\proj{\delta}{\vs_1}$ of $\delta$ from Example~\ref{eg:proj} as input to Algorithm~\ref{alg:traversalDiam}.
The first step in Algorithm~\ref{alg:traversalDiam} is to compute the SCCs of the state space $\graph(\delta)$.
$\graph(\delta)$ is shown in Figure~\ref{fig:projSspace1}, and it has three strongly connected components, thus $SCC := \{\{\overline{\vara\varb},\overline\vara\varb\},\{\vara\overline\varb\},\{\vara\varb\}\}$.
Those connected components induce the quotient of the state space shown in Figure~\ref{fig:projSspaceQuot}.
Next, the algorithm computes $\Salgo{\cardfun}(\graph(\delta)/SCC)$.
We have $\Sbrace{\cardfun}(\{\vara\overline\varb\})= \cardfun(\{\vara\overline\varb\}) = 0$, and $\Sbrace{\cardfun}(\{\vara\varb\})= \cardfun(\{\vara\varb\}) = 0$.
$\Sbrace{\cardfun}(\{\overline{\vara\varb},\overline\vara\varb\}) = \cardfun{\{\overline{\vara\varb},\overline\vara\varb\}} + \max\{\Sbrace{\cardfun}(\{\vara\varb\}),\Sbrace{\cardfun}(\{\vara\overline\varb\})\} + 1 = 1 + 0 + 1 = 2$.
Thus, $\Salgo{\cardfun}(\graph(\delta)/SCC) = 2$, which is the traversal diameter of the state space of $\proj{\delta}{\vs_1}$ and the value returned by Algorithm~\ref{alg:traversalDiam}.
\end{myeg}
