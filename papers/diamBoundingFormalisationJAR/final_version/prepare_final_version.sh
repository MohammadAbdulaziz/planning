#!/bin/bash
cd ../
../../HOL/bin/Holmake
cd final_version/
#rm sources/*.tex
rm *.png
projDir=/media/Data/Automated_reasoning/PhD_research/planning_bitbucket/planning
cp $projDir/papers/latexIncludes/* .
cp $projDir/papers/latexIncludes/*/* .
cp $projDir/papers/sspaceAcyclicity/*.tex .
cp $projDir/papers/SODA/*.tex .
cp $projDir/thesis/*.tex .
cp $projDir/thesis/theoryGraph.png .
cp $projDir/papers/diameterUpperBounding/*.tex .
#cp $projDir/papers/diameterUpperBounding/*.png .
cp $projDir/papers/diamBoundingFormalisationJAR/*.tex .
cp $projDir/papers/diamBoundingFormalisationJAR/*.eps .
#Flatten
perl latexpand/latexpand diamBoundingFormalisationJAR.tex > out
rm *.tex
rm *~
mv out paper.tex
mkdir sources
./aaai_script.sh paper.tex
cp sources/*.sty .
rm -rf sources
rm *.txt
latexmk -f -gg -pdf paper.tex
pdftotext paper.pdf
pdftotext ../diamBoundingFormalisationJAR.pdf
DIFF=$(diff ../diamBoundingFormalisationJAR.txt paper.txt) 
if [ "$DIFF" != "" ] 
then
    echo "The pdf is not exactly as it should be. Check diff.txt."
    echo $DIFF > diff.txt
fi
rm paper.pdf
pdflatex paper.tex
bibtex paper
pdflatex paper.tex
pdflatex paper.tex
rm *.txt
rm *.pdf
rm *.log
rm testOnly
rm eshell
rm __tmp*
rm aaai17.sty aaai.bst aaai.dot aaai.sty *.out IEEEtran.cls
rm *.idf *.mf *.log *.fd *.tfm *pk *.idx *.def *.out
rm *.pfb *.clo *.eps JAR_edit_report* *.ins flatten *.map *.dtx *.fdb_latexmk *.afm *.fls *.xml *.gf *diff* *.dtx README *.blg *. *.aux 

