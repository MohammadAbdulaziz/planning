\section{Concluding Remarks}
\label{sec:conclsion}

With this work we publish the details of the formal verification work behind a fruitful collaboration between the disciplines of AI~planning and mechanised mathematics.
In concluding, it is worth revisiting key results and observations made during our formalisation efforts, and the motivation for mechanisation.
From the point of view of the interactive theorem-proving community, it is gratifying to be able to find and fix errors in the modern research literature.
The insights which led us to develop the sublist diameter, and the top-down algorithm, followed preliminary attempts to formalise results by Rintanen and Gretton in~\cite{Rintanen:Gretton:2013}.
Those efforts, reported in~\cite{abdulaziz2015verified}, uncovered an error in their theoretical claims, where they incorrectly state that the diameter can be compositionally bounded using abstractions induced by acyclicity in the dependency graph.
Importantly, that error never shows up during experimentation, where compositional bounding using the diameter yields admissible results on all of thousands of diverse problems from planning benchmarks.
Nonetheless such an error cannot be tolerated in safety critical applications.
This kind of elusive error makes a strong case for the utility of mechanical verification, which identifies and helps eliminate mistakes before they migrate into production systems.

It is vital that we give AI researchers assurances that their algorithms, theory and systems are correct. 
For AI planning systems to be deployed in safety critical applications and for autonomous exploration of space, they must not only be efficient, and provably conservative in their resource consumption, but also correct. 
The upper-bounding algorithms like the ones we formalise here underpin fixed-horizon planning; indeed, they provide the fixed horizon past which an algorithm need not search.
If an autonomous vehicle exploring outer space implemented diameter-based compositional bounding suggested by \RG to do its plan-search, that system could incorrectly conclude that no plan exists.

Our experience mechanising results in AI planning allows us to provide insights regarding the scalability of formalising AI~planning algorithms.
To formalise the compositional algorithms we developed a library of HOL4 proof scripts that is around 14k lines long, including comments.
The general organisation of the library is shown in Figure~\ref{fig:HOL4Lib} and the sizes and descriptions of important theories is in Table~\ref{table:theories}.
The first algorithm that we formalised was $\NalgoName$, and to do so we developed around 10k lines of proof script.
That script was developed in approximately six months.
Our subsequent formalisation of $\SalgoName$ and $\Balgo$ required an additional 2k lines of proof scripts each.
Each of those algorithms took around two and half weeks to formalise.
This productivity improvement follows because when we formalised $\NalgoName$, we developed the majority of the needed formal background theory.
That theory is leveraged in our formalisation of other algorithms on factored transition systems.

We made a number of observations in our efforts that we believe provide insight into how HOL4 can be improved.
The feature of HOL4 that we would cite as the most positive, is the ability to quickly modify existing tactics, or add new tactics, since the entire system is completely implemented in SML.
Also, automation tactics in general are reasonable.
Nonetheless, we think that other aspects of automation can still be improved. 
Tasks which we found cumbersome and to which more automation could be helpful include: \begin{enumerate*}\item searching for theorems in the library, \item the generation of termination conditions, and \item deriving the function form of relations for which uniqueness properties exist.\end{enumerate*}
Another more general issue that we faced is the absence of a mechanism akin to type classes in Isabelle/HOL, which could have allowed us to reduce the repetition of theorem hypotheses.

Future work can leverage our work on propositionally factored systems in more general settings, such as for systems in which the codomains of states are not necessarily Boolean, finite or even countable.
This raises the possibility of applying and extending our algorithms and related proof scripts to hybrid systems.
In particular, we developed a large library describing factored transition systems.
Much of the theory in our library applies to factored systems that are not {\em propositionally} factored,  and therefore that theory can be used for verifying algorithms on hybrid systems.
Here, an interesting challenge would be extending the theory we developed to be capable of representing actions whose preconditions and effects are functions in state variables, versus assignments to state variables.

In terms of formally verifying AI~planning algorithms, we have only scratched the surface.
For instance, when a tight bound for a planning problem is known, one effective technique for finding a plan is to reduce that problem to SAT~\citep{rintanen:12}.
%%
Proposed reductions are constructive, in the sense that a plan can be constructed in linear time from a satisfying assignment to a formula. 
A key recent advance in the setting of planning-via-SAT has been the development of compact SAT-representations of planning problems. 
Such representations facilitate more efficient search~\citep{rintanen:12,robinson09}.
In future work, we would like to verify the correctness of both the reductions to SAT, and the algorithms that subsequently construct plans from a satisfying assignment.

\input{theoriesTable}
\begin{figure}[!htb]
        \includegraphics[width=\textwidth]{theoryGraph}
        \caption{\label{fig:HOL4Lib} The organisation of the different theories concerning factored transition systems. An edge from one theory to another indicates the dependence of the latter on the former.}
\end{figure}

