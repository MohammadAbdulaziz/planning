open HolKernel Parse boolLib bossLib;

open acyclicityTheory
open CNFTheory
open HO_arith_utilsTheory
open SCCMainsystemAbstractionTheory
open SCCTheory
open SCCsystemAbstractionTheory
open actionSeqProcessTheory
open acycSspaceTheory
open acyclicityTheory
open arith_utilsTheory
open boundingAlgorithmsTheory
open depGraphVtxCutTheory
open dependencyTheory
open factoredSystemTheory
open fmap_utilsTheory
open invariantStateSpaceTheory
open invariantsPlusOneTheory
open invariantsTheory
open list_utilsTheory
open parentChildStructureTheory
open acycDepGraphTheory
open parentTwoChildrenStructureTheory
open partitioningTheory
open prodValidtdTheory
open rel_utilsTheory
open set_utilsTheory
open stateSpaceProductTheory
open sublistTheory
open systemAbstractionTheory
open tightnessTheory
open topologicalPropsTheory
open utilsTheory
open planningProblemTheory
open instantiationTheory

open lcsymtacs
val _ = new_theory "prettyPrinting";

val as_proj_def = store_thm(
  "as_proj_def",
  ``(as_proj ([], vs) = []) /\
    (as_proj ((p,e) :: as, vs) = if FDOM (DRESTRICT e vs) ≠ ∅ then (DRESTRICT p vs, DRESTRICT e vs) :: as_proj(as,vs)
                                 else as_proj(as,vs))``,
  simp[systemAbstractionTheory.as_proj_def, systemAbstractionTheory.action_proj_def]);

val state_succ_def = store_thm(
  "state_succ_def",
  ``state_succ s (p,e) = if (SUBMAP) p s then FUNION e s else s``,
  simp[state_succ_def]);

val top_sorted_def = store_thm(
  "top_sorted_def",
  ``(top_sorted (PROB, []) = T) /\
    (top_sorted (PROB, vs::lvs) ⇔
       (∀vs'. MEM vs' lvs ⇒ ¬dep_var_set(PROB,vs',vs)) /\ top_sorted (PROB, lvs))``,
  simp[top_sorted_def] >> Cases_on `top_sorted(PROB,lvs)` >> simp[] >> pop_assum  kall_tac >>
  `∀lvs a. FOLDL (\acc vs'. acc /\ ~dep_var_set(PROB, vs', vs)) a lvs ⇔
           a /\ ∀vs'. MEM vs' lvs ⇒ ¬dep_var_set(PROB, vs', vs)` suffices_by metis_tac[] >>
  Induct_on `lvs` >> simp[] >> metis_tac[]);

val top_sorted_curried_def = store_thm(
  "top_sorted_curried_def",
  ``(top_sorted_curried PROB [] = T) /\
    ((top_sorted_curried PROB (vs::lvs)) ⇔
       ((∀vs'. MEM vs' lvs ⇒ ¬dep_var_set(PROB,vs',vs)) /\ top_sorted_curried PROB lvs))``,
  simp[top_sorted_curried_def] >> Cases_on `top_sorted_curried PROB lvs` >> simp[] >> pop_assum  kall_tac >>
  `∀lvs a. FOLDL (\acc vs'. acc /\ ~dep_var_set(PROB, vs', vs)) a lvs ⇔
           a /\ ∀vs'. MEM vs' lvs ⇒ ¬dep_var_set(PROB, vs', vs)` suffices_by metis_tac[] >>
  Induct_on `lvs` >> simp[] >> metis_tac[]);

val _ = ParseExtras.tight_equality()
val _ = remove_termtok { tok = "=", term_name = "="}
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Infix(NONASSOC, 450),
                  term_name = "=",
                  pp_elements = [HardSpace 1, TOK "=", BreakSpace(1,2)]}

(* Pretty => *)
val _ = remove_termtok { tok = "==>", term_name = "⇒"}
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Infixr 200,
                  term_name = "==>",
                  pp_elements = [HardSpace 1, TOK "⇒", BreakSpace(1,2)]}

(* Pretty <=> *)
val _ = remove_termtok { tok = "⇔", term_name = "<=>"}
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Infix(NONASSOC, 100),
                  term_name = "<=>",
                  pp_elements = [HardSpace 1, TOK "⇔", BreakSpace(1,2)]}

(* force parentheses around l *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "problem_plan_bound",
                  pp_elements = [TOK "(ellLP)", TM, TOK "(ellRP)"]}

(* force parentheses around td*)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "td",
                  pp_elements = [TOK "(tdLP)", TM, TOK "(tdRP)"]}


(* force parentheses around valid_states *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "valid_states",
                  pp_elements = [TOK "(validstatesLP)", TM, TOK "(validstatesRP)"]}

val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "PLS",
                  pp_elements = [TOK "(PLSLP)", TM, TOK "(PLSRP)"]}



val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "problem_plan_bound_charles",
                  pp_elements = [TOK "(problem_plan_bound_charlesLP)", TM, TOK "(problem_plan_bound_charlesRP)"]}

(* force parentheses around valid_plans *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "valid_plans",
                  pp_elements = [TOK "(validplansLP)", TM, TOK "(validplansRP)"]}

(* force parentheses around rd *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "RD",
                  pp_elements = [TOK "(RDLP)", TM, TOK "(RDRP)"]}

(* force parentheses around l *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "problem_plan_bound",
                  pp_elements = [TOK "(ellLP)", TM, TOK "(ellRP)"]}

(* squish n closer to its argument *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT,0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Prefix 900,
                  term_name = "n",
                  pp_elements = [TOK "n"]}

(* squish N closer to its argument *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT,0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Prefix 900,
                  term_name = "N",
                  pp_elements = [TOK "N"]}

(* squish exec_plan closer to its argument *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT,0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Prefix 900,
                  term_name = "exec_plan",
                  pp_elements = [TOK "exec_plan"]}

(* various projections/domain-restrictions *)
val _ = overload_on ("ppPROBPROJ", ``\pi vs. prob_proj( pi, vs)``)
val _ = overload_on ("ppPROBPROJ", ``\fm vs. DRESTRICT fm vs``)
val _ = overload_on ("ppPROBPROJ", ``\ss vs. ss_proj ss vs``)
val _ = overload_on ("ppPROBPROJ", ``\as vs. as_proj( as, vs)``)
val _ = overload_on ("ppPROBPROJ", ``\a vs. action_proj( a, vs)``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Suffix 2100,
                  term_name = "ppPROBPROJ",
                  pp_elements = [TOK "(projarrowSTART)", TM,
                                 TOK "(projarrowEND)"]}
(* Snapshotting *)
val _ = overload_on ("ppPROBSNAPSHOT", ``\pi s. snapshot pi s``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Suffix 2100,
                  term_name = "ppPROBSNAPSHOT",
                  pp_elements = [TOK "(snapshotarrowSTART)", TM,
                                 TOK "(snapshotarrowEND)"]}



val _ = overload_on("PICOMPL", ``λvs. (prob_dom PROB) DIFF vs``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "PICOMPL",
                  pp_elements = [TOK "(PICOMPLLP)", TM, TOK "(PICOMPLRP)"]}

(* infix sublist *)
val _ = set_mapped_fixity { term_name = "sublist",
                            fixity = Infix(NONASSOC, 450),
                            tok = "<=<" }

(*
(* Convert IMAGE f *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Suffix 1900,
                  term_name = "IMAGE",
                  pp_elements = [TOK "(IMAGE1)", TM, TOK "(IMAGE2)"]}
*)

(* LENGTH *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "LENGTH",
                  pp_elements = [TOK "BarLeft|", TM, TOK "|BarRight"]}

(* CARD *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "CARD",
                  pp_elements = [TOK "BarLeft|", TM, TOK "|BarRight"]}


val TRUTH = save_thm("TRUTH", TRUTH)

(* alternative presentations of definitions *)
val dep_def = store_thm(
  "dep_def",
  ``dep (PROB, v1, v2) ⇔
      (∃p e. (p,e) ∈ PROB ∧
            (v1 ∈ FDOM p ∧ v2 ∈ FDOM e ∨
             v1 ∈ FDOM e ∧ v2 ∈ FDOM e)) ∨ v1 = v2``,
  simp[dependencyTheory.dep_def, pairTheory.EXISTS_PROD]);

(*val prob_proj_def = store_thm(
  "prob_proj_def",
  ``prob_proj (PROB, vs) = PROB with <| A := { (DRESTRICT p vs, DRESTRICT e vs) | (p,e) ∈ PROB.A } ;
                                        G := DRESTRICT PROB.G vs ;
                                        I := DRESTRICT PROB.I vs |>``,
                                                       simp[systemAbstractionTheory.prob_proj_def, EXTENSION, pairTheory.EXISTS_PROD]);
*)
(*
*)

(* print dep better *)
val _ = overload_on ("deparrow", ``\v1 v2. dep(PROB, v1, v2)``)
val _ = overload_on ("deparrow", ``\vs1 vs2. dep_var_set(PROB, vs1, vs2)``)
val _ = overload_on ("notdeparrow", ``\v1 v2. ¬dep(PROB, v1, v2)``)
val _ = overload_on ("notdeparrow", ``\v1 v2. ¬dep_var_set(PROB, v1, v2)``)

val _ = set_fixity "deparrow" (Infix(NONASSOC, 450))
val _ = set_fixity "notdeparrow" (Infix(NONASSOC, 450))

(* print N_gen *)
val _ = overload_on ("Ngenb", ``\b. N_gen b PROB lvs``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT,0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "Ngenb",
                  pp_elements = [TOK "(NgenSTART)", TM, TOK "(NgenEND)"]}

val _ = overload_on ("Ncurriedgenb", ``(N_gen_curried b PROB lvs)``)

val _ = overload_on ("Ncurriedgenb", ``(N_gen_curried b (PROB:'a problem) lvs)``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT,0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "Ncurriedgenb",
                  pp_elements = [TOK "(NcurriedgenbSTART)", TM, TOK "(NcurriedgenbEND)"]}

val _ = overload_on ("Ncurriedgenell", ``(N_gen_curried problem_plan_bound PROB lvs)``)

val _ = overload_on ("Ncurriedgenell", ``(N_gen_curried (problem_plan_bound:'a problem->num) (PROB:'a problem) lvs)``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT,0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "Ncurriedgenell",
                  pp_elements = [TOK "(NcurriedgenellSTART)", TM, TOK "(NcurriedgenellEND)"]}

val _ = overload_on ("NcurriedgenHyb", ``(N_gen_curried (hybrid_algo_gen f1 f2) PROB lvs)``)

val _ = overload_on ("NcurriedgenHyb", ``(N_gen_curried ((hybrid_algo_gen f1 f2):'a problem->num) (PROB:'a problem) lvs)``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT,0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "NcurriedgenHyb",
                  pp_elements = [TOK "(NcurriedgenHybSTART)", TM, TOK "(NcurriedgenHybEND)"]}



val _ = overload_on ("Sgenb", ``(S_gen b vs lss PROB)``)

val _ = overload_on ("Sgenb", ``(S_gen b vs lss (PROB:'a problem))``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT,0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "Sgenb",
                  pp_elements = [TOK "(SgenSTART)", TM, TOK "(SgenEND)"]}

val _ = overload_on ("Sgenell", ``(S_gen problem_plan_bound vs lss PROB)``)
val _ = overload_on ("Sgenell", ``(S_gen problem_plan_bound vs lss (PROB:'a problem))``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT,0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "Sgenell",
                  pp_elements = [TOK "(SgenellSTART)", TM, TOK "(SgenellEND)"]}


val _ = overload_on ("SgenHyb", ``(S_gen (hybrid_algo_gen f1 f2) vs lss PROB)``)

val _ = overload_on ("SgenHyb", ``(S_gen (hybrid_algo_gen f1 f2) vs lss (PROB:'a problem))``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT,0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "SgenHyb",
                  pp_elements = [TOK "(SgenHybSTART)", TM, TOK "(SgenHybEND)"]}


val _ = overload_on ("Ncurried", ``N_curried PROB lvs``)
val _ = overload_on ("Ncurried", ``N_curried (PROB:'a problem) lvs``)

(*Force parentheses around b*)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "b",
                  pp_elements = [TOK "(baseLP)", TM, TOK "(baseRP)"]}

val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "children",
                  pp_elements = [TOK "(childrenLP)", TM, TOK "(childrenRP)"]}

(*Force parentheses around FDOM*)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "FDOM",
                  pp_elements = [TOK "(FDOMLP)", TM, TOK "(FDOMRP)"]}

(*Force parentheses around prob_dom*)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "prob_dom",
                  pp_elements = [TOK "(prob_domLP)", TM, TOK "(prob_domRP)"]}

(*Force parentheses around planning_prob_dom*)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "planning_prob_dom",
                  pp_elements = [TOK "(prob_domLP)", TM, TOK "(prob_domRP)"]}

(*Force parentheses around needed_vars*)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "needed_vars",
                  pp_elements = [TOK "(needed_varsLP)", TM, TOK "(needed_varsRP)"]}

(*Force parentheses around needed_asses*)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "needed_asses",
                  pp_elements = [TOK "(needed_assesLP)", TM, TOK "(needed_assesRP)"]}

(*Force parentheses around Ncurried*)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "Ncurried",
                  pp_elements = [TOK "(NcurriedLP)", TM, TOK "(NcurriedRP)"]}


val _ = overload_on ("topsortedcurried", ``top_sorted_curried (PROB: α problem)``)

val _ = overload_on ("childrencurried", ``children_curried PROB lvs``)
val _ = overload_on ("childrencurried", ``children_curried (PROB:'a problem) lvs``)
val _ = overload_on ("childrencurried", ``children_curried (PROB:(α |-> β) # (α |-> β) -> bool) lvs``)
(*Force parentheses around children_curried*)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "childrencurried",
                  pp_elements = [TOK "(childrencurriedLP)", TM, TOK "(childrencurriedRP)"]}


(* handling replace projected *)
val _ = overload_on ("replproj", ``\as1 vs as2. replace_projected ([], as1, as2, vs)``)
val _ = overload_on ("replproj", ``replace_projected ([], as1, as2, vs)``)

val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Infixl 500,
                  term_name = "replproj",
                  pp_elements = [TOK "(RPROJ1)", TM, 
                                 TOK "(RPROJ2)"]}

val _ = overload_on ("replproj", ``stitch``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Infixl 500,
                  term_name = "replproj",
                  pp_elements = [TOK "(RSTTCH1)", TM, 
                                 TOK "(RSTTCH2)"]}

(*Handling precede*)

val _ = set_mapped_fixity { term_name = "precede",
                            fixity = Infix(NONASSOC, 450),
                            tok = "precs" }

(*Handling stateSpaceProduct*)

val _ = set_mapped_fixity { term_name = "stateSpaceProduct",
                            fixity = Infix(NONASSOC, 450),
                            tok = "ssprod" }

val _ = set_mapped_fixity { term_name = "planning_prob_union",
                            fixity = Infix(NONASSOC, 450),
                            tok = "pprob_union" }

val _ = set_mapped_fixity { term_name = "subprob",
                            fixity = Infix(NONASSOC, 450),
                            tok = "subprobb" }

val _ = set_mapped_fixity { term_name = "plan",
                            fixity = Infix(NONASSOC, 450),
                            tok = "solved_by" }

val _ = set_mapped_fixity { term_name = "fun_INV",
                            fixity = Suffix 1900,
                            tok = "INV" }

(*Different images*)

val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "IMAGE",
                  pp_elements = [TOK "(SSIMG1)", TM, TOK "(SSIMG2)", TM, TOK "(SSIMG3)"]}

(*val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "MAP",
                  pp_elements = [TOK "(SSMAP1)", TM, TOK "(SSMAP2)", TM, TOK "(SSMAP3)"]}*)

val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "state_image",
                  pp_elements = [TOK "(STATEIMG1)", TM, TOK "(STATEIMG2)", TM, TOK "(STATEIMG3)"]}

val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "action_image",
                  pp_elements = [TOK "(ACTIMG1)", TM, TOK "(ACTIMG2)", TM, TOK "(ACTIMG3)"]}

val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "system_image",
                  pp_elements = [TOK "(SYSIMG1)", TM, TOK "(SYSIMG2)", TM, TOK "(SYSIMG3)"]}

val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "as_image",
                  pp_elements = [TOK "(ASIMG1)", TM, TOK "(ASIMG2)", TM, TOK "(ASIMG3)"]}

val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "planning_prob_image",
                  pp_elements = [TOK "(PROBIMG1)", TM, TOK "(PROBIMG2)", TM, TOK "(PROBIMG3)"]}

val _ = export_theory();
