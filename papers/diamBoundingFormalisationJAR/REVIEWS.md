
# How this Works

<<<<<<< HEAD
Below, I've included both referee reports as sections.

The purpose of this file is to track progress towards addressing all reviewer feedback.

For simple bits of feedback, it's just a [0/1] prefix to the block of feedback. For larger blocks, I've proposed the following.

## Appearing above a block of feedback:

* FEEDBACK-NR - no response
* FEEDBACK-RR - required response

The first item is so we can "cover off" blocks of feedback that do not require any action on our part. For example, when a reviewer summarizes the material in our manuscript.

## Appearing below a block of feedback:

* Subheading "ADDRESSED" is numbered "[n]", and a 1 follows the colon ':' iff an RR commend is addressed in the paper
=======
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479

#Report 1:

## FEEDBACK-NR

This article describes a HOL4 formalization of algorithms for computing upper
bounds of graph diameters, which is useful for sound bounded model checking
and, apparently, AI planning.

## FEEDBACK-NR

The submission is based on two conference submissions: one at ITP and one at a
specialized AI planning venue, reflecting the originality of the authors'
work, regardless of formalisation.

## FEEDBACK-RR

My first question is: Where's the formalization? Although the call for papers
did not mention this explicitly, it is customary in our community to make the
sources available, ideally to the end readers and, failing that, at least to
the reviewers. To quote from the ITP 2017 CfP: "Submissions are expected to be
accompanied by verifiable evidence of a suitable implementation, such as the
source files of a formalization for the proof assistant used."

## ADDRESSED [1]: 0

## FEEDBACK-NR

The introduction and conclusion nicely capture the strengths of this paper.
Most of the authors work (~ 10k LOC) takes the form of libraries, and the
specific algorithm verifications were relatively easy to perform once the
library was in place. The motivation is convincing: The authors found bugs in
the literature and improved on it as they formalized it. When I see
counterintuitive phrases like "longest shortest path", I'm glad this is all
formalized.

The paper is generally easy to follow, largely due to the helpful diagrams
(except for the pointless Fig. 1) and the examples (cf. pp. 10, 12, 22). I
must confess I trust the authors for the main lemmas and theorems -- my domain
of expertise is theorem proving, not bounded model checking. The results look
plausible, and the main definitions are relatively simple and understandable
(despite the nested mins and maxes).

I have many suggestions to make below, listed in order of decreasing severity.
I recommend "accept with minor revisions".

<<<<<<< HEAD
Main comments:

=======

Main comments:


>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
## FEEDBACK-RR

Given that all formalization papers are essentially case studies in the use of
a proof assistant, I found the article lacking. This was discussed at the ITP
2010 and 2016 business meetings, among others. To quote the 2010 meeting:

"J Moore said that he's happy to see application papers accepted, except
they often teach him relatively little. So he would like to see application
papers conclude with 3 features of the interactive theorem proving
environment that the author really liked and 3 features (or non-features)
that almost made the author quit."

https://www.cs.utexas.edu/~kaufmann/itp-2010/business-meeting/minutes.html

In a few places, we get a glimse of the formalization (e.g. LOC counts, which
are useful information), but overall, there's little HOL4 except in the
definitions, and one gets a sense at most that notational overloading is used
(through the footnotes).

## ADDRESSED: SEE ITEM [1]

## FEEDBACK-RR

The relation to the previous work [2,3] is not entirely clear. At 4:30-31, the
authors claim to "extend" [2,3], but this earlier research (esp. [3])
contributed not only formalizations but also new algorithms. Are the new
algorithms also claimed as contributions here (in journal form)? If not,
"extend" is perhaps not the right word.

## ADDRESSED [2]: 0

* cg: I do not see the algorithms as being new claims for the JAR submission, rather it provided the first detailed publication of the mechanisation and related formalisation in HOL4. If we are all in agreement, I can volunteer to add materials to address this above reviewer comment.


## FEEDBACK-RR

Many of the HOL definitions given are trivial reformulations of the
mathematical concepts (e.g. HOL4 Definition 2 vs. Definition 3). I'm not sure
I see the point behind this verbosity. In one occasion, the relation between
HOL and non-HOL was quite confusing: Suddenly, a HOL theorem is used to prove
a non-HOL theorem on p. 24, in a strange reversal of the usual order (informal
to formal).


## ADDRESSED [3]: 0

* cg: Please commit the PDF submission to the repo., so we can all easily respond to these comments.

* cg: We need to talk about a resolution to the confusion caused by the "HOL Lemma" and "Lemma" numbering. In the manuscript I am looking at the proof sketch for "Lemma 1" under the statement of "HOL Lemma 2" is problematic. 

## FEEDBACK-RR

I found it hard to keep the numbers apart in my mind. E.g. "HOL Lemma 2" is
not the same as "Lemma 2". Sharing the counter across all environments, which
can be achieved by passing "[envcountsame]" to the "svjour4" class, is usually
an improvement, and it also helps the reader who needs to look things up.
(E.g. if I look for Lemma 16, I know it will be somewhere between Foo 15 and
Bar 17, and I can easily use binary search to locate it. Try finding Lemma 2
with the current scheme.)

## ADDRESSED: SEE ITEM [3]

## FEEDBACK-RR

Most related work is given without author names. E.g. on p. 6, we read "In a
seminal paper, [4] devised". This is as bad as these things get. To quote
Larry Paulson:

"Many people write their related work section in a wholly passive mockery of
English, never mentioning the names of their colleagues: 'Parmesan flavour
was introduced in [1]. In the seminal work of [13], stilton flavour became
possible for the first time.' It's discourteous and reads badly. Reference
numbers are not names, so why do you keep forcing your reader to look them
up in the bibliography? The direct way is also clearer: 'Jones et al. [1]
introduced Parmesan flavour, and Brown's seminal work [13] made stilton
flavour possible...' This especially helps if Jones and Brown are your
referees."

https://www.cl.cam.ac.uk/~lp15/Pages/Scream.html

My work, like that of dozens of researchers, is cited in this discourteous
fashion. Strangely enough, Peter Dankelmann et al. are named by name at 6:46,
including his first name (an incredible luxury). Two comments: (1) This just
looks inconsistent; (2) I'm jealous.

The paper starts rather slowly. On p. 12, we are still looking at projections.
There might be ways to shorten this a bit, without losing the reader.

Small-model theorems, e.g., Pnueli et al. "The Small Model Property: How Small
Can It Be?", appear to be related. Maybe they should be cited as well?

## ADDRESSED [4]: 0

* cg: well, hardly a 'major' comment, but still needs to be addressed. 

<<<<<<< HEAD
## Minor comments:

[0] \usepackage{mathptmx} instead of "times" will give you also Times math
italics---i.e., $i$ and \emph{i} will look the same.

[0] There are lots of plural possives throughout. I don't think any of them are
=======
Minor comments:

\usepackage{mathptmx} instead of "times" will give you also Times math
italics---i.e., $i$ and \emph{i} will look the same.

There are lots of plural possives throughout. I don't think any of them are
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
wrong per se, but they're normally used very occasionally in modern English.
There are good reasons to want to avoid "of", though, but sometimes both might
be avoidable (e.g. "the contractions' states" -> "contracted states"?).

<<<<<<< HEAD
* cg: I can address the above, so please let me know if you want me to...

[0] 2:46: "only [known] feasible": Insert "known" or something to that extent. The
statement is currently too strong to be defensible (or a reference is missing).

[0] 3:44 etc.: Vertex contraction is an important concept in the paper. A brief
definition would be useful.

[0] 4:47: It's not clear whether you just verify the correctness of the algorithms
or their bounds (or whether the two are the same here).

[0] 5:44: Figure 1 is very low quality, with big pixels and horizontally squashed
=======
2:46: "only [known] feasible": Insert "known" or something to that extent. The
statement is currently too strong to be defensible (or a reference is missing).

3:44 etc.: Vertex contraction is an important concept in the paper. A brief
definition would be useful.

4:47: It's not clear whether you just verify the correctness of the algorithms
or their bounds (or whether the two are the same here).

5:44: Figure 1 is very low quality, with big pixels and horizontally squashed
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
text. Please redo using a vectorial program (and perhaps indicate the sizes of
the modules, rounded to the nearest 100 LOC?). Or omit entirely: Since the
text never refers to the theories, the figure is not informative.

<<<<<<< HEAD
[0] 7:42: Sections with a single subsection look odd. Why not have two
=======
7:42: Sections with a single subsection look odd. Why not have two
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
subsections: one with related work about algorithms and one about
formalisations? Also, the heading "Formalisation Related Work" is hard to
parse. Is it meant as "Formalisation-Related Work" (= "work related to
formalisation") or as "Formalisation Related-Work" (= "related work that takes
the form of formalisations")?

<<<<<<< HEAD
[0] 8:15: What does "docketed with ... versus ..." mean?

[0] 9:6: "finite maps": Are those "states"?

[0] 9:11: Footnote 2 seems spurious. Why refer to STRIPS and SMV, when your
=======
8:15: What does "docketed with ... versus ..." mean?

9:6: "finite maps": Are those "states"?

9:11: Footnote 2 seems spurious. Why refer to STRIPS and SMV, when your
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
Definition 2 also talks about Boolean? In general, I would recommend taking a
critical look at all your footnotes and either inline them or eliminate them.
If you decide to keep footnote 2, make sure to use a smaller font for "bool".
(And why "STRIPS by [28]" instead of "STRIPS [28]" etc.? Several occurrences.)

<<<<<<< HEAD
[0] 9:21: The convention that lists are in bold is quite subtle. It would be
=======
9:21: The convention that lists are in bold is quite subtle. It would be
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
worthwhile to introduce it. Incidentally, I'm a bit puzzled that sets are
sometimes named with s's, like lists in ML (e.g. "ss"), whereas lists have
this bold notation, which I've seen nowhere before.

<<<<<<< HEAD
[0] 9:23: "ex" is not defined (it becomes so one definition later). Does it stand
for "execute"? When I see "ex", my first thought is of the "exists"
quantifier.

[0] 12:21-22: The "dropping" behavior for actions with no effects means that
projecting on the universe is not always the identity. This looks somewhat
inelegant to me.

[0] 13:39: "G_{VS}": What happened to the dependency on \delta? It looks like a
typo.

[0] 13:13-18: I found this passage hard to read, esp. that "sat-pre" is not
=======
9:23: "ex" is not defined (it becomes so one definition later). Does it stand
for "execute"? When I see "ex", my first thought is of the "exists"
quantifier.

12:21-22: The "dropping" behavior for actions with no effects means that
projecting on the universe is not always the identity. This looks somewhat
inelegant to me.

13:39: "G_{VS}": What happened to the dependency on \delta? It looks like a
typo.

13:13-18: I found this passage hard to read, esp. that "sat-pre" is not
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
defined yet at this point. Suggestion: State clearly that "ex(...) = (ex
...)..." doesn't hold unconditionally. Explain the counterexample. Then
explain informally what the precondition is and why it rules out the
counterexample and all its family.

<<<<<<< HEAD
[0] 13:30-35: I find it a bit inelegant and inconsistent that the lifting to
=======
13:30-35: I find it a bit inelegant and inconsistent that the lifting to
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
singleton sets doesn't coincide to the unlifted version. We have x -> x for
all x, but {x} -> {x} for no x. This is about as anomalous as the situation at
12:21-22.

<<<<<<< HEAD
[0] 14:1-7: Shouldn't there be three loopy edges x -> x, y -> y, and z -> z in
figure (a), given Def. 7(i)?

[0] 18:6: The algorithm looks very generic -- i.e., not restricted to weights. Why
the label "Weightiest path", and what does "l" stand for in "wlp"?
Incidentally, WLP often abbreviates "weakest liberal precondition".

[0] 22:33: What is a "scattered sublist"? A subsequence?

[0] 25:37: "[13] prove": In a formalization paper, one would expect that proving
on paper, proving formally, and getting a counterexample using a model finder
would be described using different verbs, not just as "prove".

[0] 32:36 vs. 33:36: The two "definitions" have the same tag ("The Hybrid
=======
14:1-7: Shouldn't there be three loopy edges x -> x, y -> y, and z -> z in
figure (a), given Def. 7(i)?

18:6: The algorithm looks very generic -- i.e., not restricted to weights. Why
the label "Weightiest path", and what does "l" stand for in "wlp"?
Incidentally, WLP often abbreviates "weakest liberal precondition".

22:33: What is a "scattered sublist"? A subsequence?

25:37: "[13] prove": In a formalization paper, one would expect that proving
on paper, proving formally, and getting a counterexample using a model finder
would be described using different verbs, not just as "prove".

32:36 vs. 33:36: The two "definitions" have the same tag ("The Hybrid
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
Algorithm"). I have the impression the second one is a characteristic lemma
and should be called "HOL Lemma".


<<<<<<< HEAD
## Editorial comments:

[0] 1:26: "not practically not possible": One "not" too much.

[0] 1:39: ", that can": The combination "comma + that-clause" is almost always wrong
=======
Editorial comments:

1:26: "not practically not possible": One "not" too much.

1:39: ", that can": The combination "comma + that-clause" is almost always wrong
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
(unless the comma is the second of a pair). It should be either "that can" (if
the clause is restrictive) or ", which can" (if the clause is parenthetic).
Some authors also write "which" for "that", but the other direction is just
wrong. Several occurrences throughout the manuscript (e.g. 8:11).

<<<<<<< HEAD
[0] 1:19, 2:10, etc.: Lower-case "Artificial Intelligence".

[0] 1:19 vs. 2:22: "SAT based" vs. "SAT-based". More occurrences; grep.

[0] 2:33: "[55, 4]": \usepackage{cite} will reorder these into the more appealing
"[4, 55]".

[0] 2:42: "abstraction[s]' state": Missing s.

[0] 3:6 etc.: "upper bound the diameter". An adjective ("upper") cannot modify a
=======
1:19, 2:10, etc.: Lower-case "Artificial Intelligence".

1:19 vs. 2:22: "SAT based" vs. "SAT-based". More occurrences; grep.

2:33: "[55, 4]": \usepackage{cite} will reorder these into the more appealing
"[4, 55]".

2:42: "abstraction[s]' state": Missing s.

3:6 etc.: "upper bound the diameter". An adjective ("upper") cannot modify a
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
verb ("bound"). Write as one, if necessary ("to upper-bound"), or drop the
"upper", or rephrase (e.g., "compute an upper bound"). Several occurrences,
starting with the article's title. Same with "lower" (e.g., 6:40).

<<<<<<< HEAD
[0] 4:7: My dictionary (Oxford Am.E.) only records the transitive (i.e.,
with-object) use of "execute". "Run"?

[0] 4:38 etc.: "formalistation": One t too many. Grep.

[0] 4:44: "combines both the top-down": Drop "both". It is redundant with
"combines".

[0] 5:5 etc.: "text book" -> "textbook". Even "textbookish" is recorded in my
dictionary.

[0] 6:11: "discuss concluding remarks": Sounds very meta. "Offer"? "Present"?
Or "close with some"?

[0] 6:45: "starting at 2000": Change "at" to "in"?

[0] 2:31 vs. 7:13: "state-space" vs. "state space". I would personally write
"state-space explosion" but "the state space", but here we see the opposite.

[0] 25:29 vs. 25:35 vs. 31:27: "Hotel Key Protocol" vs. "hotel key protocol vs.
"Hotel-Key protocol". I vote for lower-case.

[0] 7:4: The space after "see for example" is too big.

[0] 8:12: Is => really the right arrow for a function? I thought this was an
Isabelle-specific notation.

[0] 8:20: "were" -> "are"?

[0] 9:2: "characterizing" -> "characterising".

[0] 10:37: Spurious space at the beginning of the line.

[0] 11:22 etc.: "Note that": This can be killed in almost all cases (including
here) without loss.

[0] 11:23: Deemphasise "entirely" (which you are surely not defining here).

[0] 11:35: "exist in the theorem above". Which theorem above? I only see
definitions.

[0] 11:37: ", however" -> "; however". Comma splice. See
https://en.wikipedia.org/wiki/Comma_splice

[0] 11:43: "we give a brief background". Maybe it's just me, but this sounds as
odd to me as "discuss concluding remarks".

[0] 12:25, 12:38, 12:47: Add missing colons.

[0] 12:43: "a st s" -> "a set of states s"? And I'm a bit confused between "s",
"\delta", and "ss". Are those variables of the same type? If not, how does
this work w.r.t. the definition of the (|...|) operator?

[0] 13:34: "all of the following statements hold:" Kill 6 words + colon. And
lower-case "There".

[0] 13:39: "a a": Typo.

[0] 14:14 and 14:21: Remove colons. See point 23 on p. 4 of Knuth et al.:
http://tex.loria.fr/typographie/mathwriting.pdf

[0] 14:21: Remove the spurious space before the footnote call.

[0] 15:8 vs. 15:14: Remove one or the other sentences that say the same thing.

[0] 15:12: Remove spurious comma.

[0] 15:15: "since its state space is the clique ...": Maybe remind us why this
accounts for a diameter of 1. I guess it's because from any node, one has a
path of length two to any other node, and 2 - 1 = 1?

[0] 16:34-35: Left-align the RHSs.

[0] 17:39: "Top-down" -> "Top-Down". Cf. heading 4.2.2.

[0] 18:28: "big" -> "large"?

[0] 19:5: Kill first comma on line, and move the footnote call to after the final
stop (as required by English conventions).

[0] 19:44: Missing indentation (\parindent).

[0] 20:1 vs. 20:10: Inconsistent indentation. 20:10 looks better.

[0] 21:33: "is both:" Drop colon, which I find positively confusing. I first read
=======
4:7: My dictionary (Oxford Am.E.) only records the transitive (i.e.,
with-object) use of "execute". "Run"?

4:38 etc.: "formalistation": One t too many. Grep.

4:44: "combines both the top-down": Drop "both". It is redundant with
"combines".

5:5 etc.: "text book" -> "textbook". Even "textbookish" is recorded in my
dictionary.

6:11: "discuss concluding remarks": Sounds very meta. "Offer"? "Present"?
Or "close with some"?

6:45: "starting at 2000": Change "at" to "in"?

2:31 vs. 7:13: "state-space" vs. "state space". I would personally write
"state-space explosion" but "the state space", but here we see the opposite.

25:29 vs. 25:35 vs. 31:27: "Hotel Key Protocol" vs. "hotel key protocol vs.
"Hotel-Key protocol". I vote for lower-case.

7:4: The space after "see for example" is too big.

8:12: Is => really the right arrow for a function? I thought this was an
Isabelle-specific notation.

8:20: "were" -> "are"?

9:2: "characterizing" -> "characterising".

10:37: Spurious space at the beginning of the line.

11:22 etc.: "Note that": This can be killed in almost all cases (including
here) without loss.

11:23: Deemphasise "entirely" (which you are surely not defining here).

11:35: "exist in the theorem above". Which theorem above? I only see
definitions.

11:37: ", however" -> "; however". Comma splice. See
https://en.wikipedia.org/wiki/Comma_splice

11:43: "we give a brief background". Maybe it's just me, but this sounds as
odd to me as "discuss concluding remarks".

12:25, 12:38, 12:47: Add missing colons.

12:43: "a st s" -> "a set of states s"? And I'm a bit confused between "s",
"\delta", and "ss". Are those variables of the same type? If not, how does
this work w.r.t. the definition of the (|...|) operator?

13:34: "all of the following statements hold:" Kill 6 words + colon. And
lower-case "There".

13:39: "a a": Typo.

14:14 and 14:21: Remove colons. See point 23 on p. 4 of Knuth et al.:
http://tex.loria.fr/typographie/mathwriting.pdf

14:21: Remove the spurious space before the footnote call.

15:8 vs. 15:14: Remove one or the other sentences that say the same thing.

15:12: Remove spurious comma.

15:15: "since its state space is the clique ...": Maybe remind us why this
accounts for a diameter of 1. I guess it's because from any node, one has a
path of length two to any other node, and 2 - 1 = 1?

16:34-35: Left-align the RHSs.

17:39: "Top-down" -> "Top-Down". Cf. heading 4.2.2.

18:28: "big" -> "large"?

19:5: Kill first comma on line, and move the footnote call to after the final
stop (as required by English conventions).

19:44: Missing indentation (\parindent).

20:1 vs. 20:10: Inconsistent indentation. 20:10 looks better.

21:33: "is both:" Drop colon, which I find positively confusing. I first read
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
"is both", stopped, and wondered, "both what?" The following example
illustrates the miscue: "Some guests are thirsty. Others are hungry. I am
both: I want drinks and food."

<<<<<<< HEAD
[0] 22:40: "formalised theorem" -> "formalised lemma"?

[0] 23:37: "a constructive proof" -> "constructive".

[0] 25:9: Too big space close to the end of the line.

[0] 27:37: Add a \; space after the quantifier period (.). Or rephrase without
using logical symbols.

[0] 28:27: Missing colon.

[0] 29:19: The closing parenthesis looks lonely at the beginning of the line.

[0] 29:33: Missing final stop (before the footnote call).

[0] 31:9: "=" goes into the margin. Rephrase.

[0] 32:5: The Pi symbol is gigantic. It looks more reasonable at 32:14.

[0] 32:14: Margin problem. \allowbreak should help.

[0] 34:29: "Their" has no clear grammatical antecedent ([45] is a paper, not a
group of authors).

[0] 34:37: Unitalicize "important". Maybe replace with a stronger word, e.g.
"crucial", "vital", "essential".

[0] 34:45: Too big space before "to do".

[0] 34:48: "An additional point we would like to state is that" -> "Moreover,".
=======
22:40: "formalised theorem" -> "formalised lemma"?

23:37: "a constructive proof" -> "constructive".

25:9: Too big space close to the end of the line.

27:37: Add a \; space after the quantifier period (.). Or rephrase without
using logical symbols.

28:27: Missing colon.

29:19: The closing parenthesis looks lonely at the beginning of the line.

29:33: Missing final stop (before the footnote call).

31:9: "=" goes into the margin. Rephrase.

32:5: The Pi symbol is gigantic. It looks more reasonable at 32:14.

32:14: Margin problem. \allowbreak should help.

34:29: "Their" has no clear grammatical antecedent ([45] is a paper, not a
group of authors).

34:37: Unitalicize "important". Maybe replace with a stronger word, e.g.
"crucial", "vital", "essential".

34:45: Too big space before "to do".

34:48: "An additional point we would like to state is that" -> "Moreover,".
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
Beyond removing padding, my proposal has the advantage that it promotes the
rest of the sentence from a subclause to a main clause, thereby emphasizing
it. Similar horrible padding at 35:4 and 35:12. Yesterday, I reviewed a
35-page manuscript with about five "Note that"s or "It is important to note
that"s per page on average. Please save me.

<<<<<<< HEAD
[0] 35:15: "We note however that the" -> "Interestingly, the" or "Remarkably,
the" or just "The".

[0] 35:15: 14000 -> 14\,000 (ISO) or 14,000 (English)? Same elsewhere. (Some
authors start at 4 digits, but not for dates; others start at 5.)


# Report #2: "lots of minor modifications"
=======
35:15: "We note however that the" -> "Interestingly, the" or "Remarkably,
the" or just "The".

35:15: 14000 -> 14\,000 (ISO) or 14,000 (English)? Same elsewhere. (Some
authors start at 4 digits, but not for dates; others start at 5.)


Report #2:
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479

The article by Abdulaziz, Norrish and Gretton
adresses the problem to compute upper bounds for the so-called diameter
of transition systems that are indirectly represented via composition operators.
This is a relevant problem for the completeness
of planning algorithms and bounded model checkers. According to
the authors, a proven correct combination of two formerly analysed algorithms
has been experimentally shown to produce tighter bounds than existing solutions.

The article presents clearly novel and significant work worth to be
put into archivable
journal format, and I recommend acceptance without hesitation. What
pours a little
luke water into the wine: there is a fairly large number of awkward
and distracting sentences
in the paper that need to be addressed for the final publication.
So my verdict: lots of minor modifications.

Minor issues:
<<<<<<< HEAD

## FEEDBACK-RR

=======
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
Lets start with the abstract:
I found the first two sentences distracting and repetitive (2 times AI
planning or BMC;
an application which is then not further substantiated in the paper,
what would be
highly interesting by the way ...).
<<<<<<< HEAD

=======
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
What I would expect is s th like:
"The *diameter* of a transition system system is the longest shortest
path between any two vertices.
Its value is of key importance for the completeness of AI planning
algorithms or bounded model checkers.
Since the explicit representation of transition systems is usually
infeasible for realistic systems,
the question of constructing upper bounds of the diameter from
structured representations
of transition systems raises continuous interest in research communities.
In this paper, we present … "

Abstract: … is not practically not possible … awkward sentence.

Abstract: recurrence diameter - not really well explained

<<<<<<< HEAD
## ADDRESSED [5]: 0


=======
>>>>>>> 96ac30cdbe69c517f3e09713b5be3ffe80f7a479
pp 3: AI algorithms in general need a lot of time/memory and in many
applications … bizarre argument.

pp 3: The first introduction of "factored representation" with a hint
to the hotel room example (which is then
later properly explained) is not really helpful here; I suggest to
look for an alternative explanation
more focused to the idea to be put forward.

pp 4: recurrence diameter : the longest simple path. I am puzzled
here: is this different of a shortest
path ? Explain better the difference or use uniform terminology.

pp 6: the concept of *recurrence* diameter (which has been mentioned
only in the abstract briefly;
since this concept has not been introduced in more detail, this para
is not really understandable.

pp 7: change: It was also formalised in Isabelle/HOL[43], where Paulson used ...

pp 8 : The transition / motivation of Definition 1 to HOL4 Definition
1 (DAG) is not clear to me:
a digraph is a very different beast than a DAG.
As in other cases the transition between a conceptual textbook
definition and its
formalisation in HOL4 is just juxtaposed (for ex.: Def 3 and HOL Def
2), the reader is a bit
puzzled with HOL4 Def 1.

pp 8: I can imagine what EVERY means, but I would prefer to have it explained…

pp 9: Note *that* the state x1 takes precedence …

pp 10 : Note again that this finite map …

pp 11 : (and other topological properties — which ones ?

Fig 2/3 is actually very helpful for the understanding of Example 1 - 3.

pp 13 : … can have their preconditions satisfied after projection;
the projection would therefore be incorrect presentation. (skip
subsequent sentence).

pp 13: where it can be used obtain -> which can be used to obtain very
useful projections

By the way, why "useful" ?

pp 13: which are described by the *dependency graph", a concept
defined in the following: …

pp 14: although quite speaking names, I would prefer an explication
for DISJOINT and ALL-DISTINCT …

pp 15: And finally the long awaited definition for recurrence diameter …

In the para starting with : "Two common features of … ", it should be
made explicit that you refer to the
"lifted dependency graph" mentioned in Fig 4. Otherwise Dag.

pp 16: Theorem 1 … This is formally proven in HOL4, I suppose ? An
outline of the proof would be desirable.
I acknowledge that the subsequent gives some ideas how this proof
works, but I would prefer in this
journal article either an outline or a ref to a TR or something …

The part over the formalisation is better formulated.

pp 31: the comparison to Madagascar is interesting, but are there also
interesting implementations from the BMC world ?
Overall, the application side looks pretty based towards AI and leaves
little room for justifying the referred application BMC.

pp 34: Again, strong bias towards AI and little substance to BMC.

On the other hand, it is *important* in practice to give the planning
community … This sounds a bit ridiculous.
I d say the world can perfectly survive without ;-) (seriously:
awkward sentence …)
