#!/bin/bash

echo "strict digraph gr {"
echo "node [style=filled, fillcolor=white]"
echo "S00S10S20 [fillcolor=red]"
echo "S00S10S20 -> S01S10S20 [style=dashed]"
echo "S01S10S20 [fillcolor=red]"
echo "S01S10S20 -> S02S10S20 [style=dashed]"
echo "S02S10S20 [fillcolor=red]"
echo "S02S10S20 -> S02S11S20 [style=dashed]"
echo "S02S11S20 [fillcolor=red]"
echo "S02S11S20 -> S00S11S20 [style=dashed]"
echo "S00S11S20 [fillcolor=red]"
echo "S00S11S20 -> S01S11S20 [style=dashed]"
echo "S01S11S20 [fillcolor=red]"
echo "S01S11S20 -> S01S12S20 [style=dashed]"
echo "S01S12S20 [fillcolor=red]"
echo "S01S12S20 -> S02S12S20 [style=dashed]"
echo "S02S12S20 [fillcolor=red]"
echo "S02S12S20 -> S00S12S20 [style=dashed]"
echo "S00S12S20 [fillcolor=red]"
echo "S00S12S20 -> S00S13S20 [style=dashed]"
echo "S00S13S20 [fillcolor=red]"
echo "S00S13S20 -> S01S13S20 [style=dashed]"
echo "S01S13S20 [fillcolor=red]"
echo "S01S13S20 -> S02S13S20 [style=dashed]"
echo "S02S13S20 [fillcolor=red]"
echo "S02S13S20 -> S02S13S21 [style=dashed]"
echo "S02S13S21 [fillcolor=red]"
echo "S02S13S21 -> S00S13S21 [style=dashed]"
echo "S00S13S21 [fillcolor=red]"
echo "S00S13S21 -> S01S13S21 [style=dashed]"
echo "S01S13S21 [fillcolor=red]"
echo "S01S13S21 -> S01S13S22 [style=dashed]"
echo "S01S13S22 [fillcolor=red]"
echo "S01S13S22 -> S02S13S22 [style=dashed]"
echo "S02S13S22 [fillcolor=red]"
echo "S02S13S22 -> S00S13S22 [style=dashed]"
echo "S00S13S22 [fillcolor=red]"
for S0 in 0 1 2
do
  for S1 in 0 1 2 3
  do
    for S2 in 0 1 2
    do
      for S0_ in 0 1 2
      do
        for S1_ in 0 1 2 3
        do
          for S2_ in 0 1 2
          do
            if [ "$S0" != "$S0_" -o "$S1" != "$S1_" -o "$S2" != "$S2_" ] ; then
              if [ \( "$S0" = 0 \) -a "$S1" = "$S1_" -a "$S2" = "$S2_" ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S01S1${S1_}S2${S2_} [type=s];"
              fi
              if [ \( "$S0" = 1 \) -a "$S1" = "$S1_" -a "$S2" = "$S2_" ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S02S1${S1_}S2${S2_} [type=s];"
              fi

              if [ \( "$S0" = 2 \) -a "$S1" = "0" -a "$S0" = "$S0_" -a "$S2" = "$S2_" ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S0${S0_}S11S2${S2_} [type=s];"
              fi

              if [ \( "$S0" = 2 \) -a "$S1" = "$S1_" -a "$S2" = "$S2_" ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S00S1${S1_}S2${S2_} [type=s];"
              fi
              if [ \( "$S0" = 1 \) -a "$S1" = "1" -a "$S0" = "$S0_" -a "$S2" = "$S2_" ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S0${S0_}S12S2${S2_} [type=s];"
              fi
              if [ \( "$S0" = 0 \) -a "$S1" = "2" -a "$S0" = "$S0_" -a "$S2" = "$S2_" ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S0${S0_}S13S2${S2_} [type=s];"
              fi
              if [ "$S0" = 2  -a "$S2" = "0" -a "$S0" = "$S0_" -a "$S1" = "$S1_" ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S0${S0_}S1${S1_}S21 [type=s];"
              fi

              if [ "$S0" = 1  -a "$S2" = "1" -a "$S0" = "$S0_" -a "$S1" = "$S1_" ] ; then 
                echo "S0${S0}S1${S1}S2${S2} -> S0${S0_}S1${S1_}S22 [type=s];"
              fi
            fi
          done
        done
      done
    done
  done
done

echo "}"
