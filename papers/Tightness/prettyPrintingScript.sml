open HolKernel Parse boolLib bossLib;

open ParentChildrenBoundTheory;
open arithmeticTheory;
open pred_setTheory;
open rich_listTheory;
open listTheory;
open relationTheory;
open finite_mapTheory;
open utilsTheory;
open FM_planTheory
open sublistTheory;
open SCCTheory;
open planning_problem_diameter_boundTheory;
open ChildParentBoundTheory;
open SCCGraphPlanTheoremTheory;
open SCCMainGraphPlanTheoremTheory;
open VertexCutTheoremTheory;
open ChildParentBoundTheory;
open BoundingAlgorithmsTheory;
open GraphPlanTheoremTheory;
open tightnessTheory;

open lcsymtacs
val _ = new_theory "prettyPrinting";

val as_proj_def = store_thm(
  "as_proj_def",
  ``(as_proj ([], vs) = []) /\
    (as_proj ((p,e) :: as, vs) = if FDOM (DRESTRICT e vs) ≠ ∅ then (DRESTRICT p vs, DRESTRICT e vs) :: as_proj(as,vs)
                                 else as_proj(as,vs))``,
  simp[GraphPlanTheoremTheory.as_proj_def, GraphPlanTheoremTheory.action_proj_def]);

val state_succ_def = store_thm(
  "state_succ_def",
  ``state_succ s (p,e) = if (SUBMAP) p s then FUNION e s else s``,
  simp[state_succ_def]);

val top_sorted_def = store_thm(
  "top_sorted_def",
  ``(top_sorted (PROB, []) = T) /\
    (top_sorted (PROB, vs::lvs) ⇔
       (∀vs'. MEM vs' lvs ⇒ ¬dep_var_set(PROB,vs',vs)) /\ top_sorted (PROB, lvs))``,
  simp[top_sorted_def] >> Cases_on `top_sorted(PROB,lvs)` >> simp[] >> pop_assum  kall_tac >>
  `∀lvs a. FOLDL (\acc vs'. acc /\ ~dep_var_set(PROB, vs', vs)) a lvs ⇔
           a /\ ∀vs'. MEM vs' lvs ⇒ ¬dep_var_set(PROB, vs', vs)` suffices_by metis_tac[] >>
  Induct_on `lvs` >> simp[] >> metis_tac[]);

val top_sorted_curried_def = store_thm(
  "top_sorted_curried_def",
  ``(top_sorted_curried PROB [] = T) /\
    ((top_sorted_curried PROB (vs::lvs)) ⇔
       ((∀vs'. MEM vs' lvs ⇒ ¬dep_var_set(PROB,vs',vs)) /\ top_sorted_curried PROB lvs))``,
  simp[top_sorted_curried_def] >> Cases_on `top_sorted_curried PROB lvs` >> simp[] >> pop_assum  kall_tac >>
  `∀lvs a. FOLDL (\acc vs'. acc /\ ~dep_var_set(PROB, vs', vs)) a lvs ⇔
           a /\ ∀vs'. MEM vs' lvs ⇒ ¬dep_var_set(PROB, vs', vs)` suffices_by metis_tac[] >>
  Induct_on `lvs` >> simp[] >> metis_tac[]);

val _ = ParseExtras.tight_equality()
val _ = remove_termtok { tok = "=", term_name = "="}
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Infix(NONASSOC, 450),
                  term_name = "=",
                  pp_elements = [HardSpace 1, TOK "=", BreakSpace(1,2)]}

(* Pretty => *)
val _ = remove_termtok { tok = "==>", term_name = "⇒"}
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Infixr 200,
                  term_name = "==>",
                  pp_elements = [HardSpace 1, TOK "⇒", BreakSpace(1,2)]}

(* Pretty <=> *)
val _ = remove_termtok { tok = "⇔", term_name = "<=>"}
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Infix(NONASSOC, 100),
                  term_name = "<=>",
                  pp_elements = [HardSpace 1, TOK "⇔", BreakSpace(1,2)]}

(* force parentheses around l *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "problem_plan_bound",
                  pp_elements = [TOK "(ellLP)", TM, TOK "(ellRP)"]}

(* force parentheses around valid_states *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "valid_states",
                  pp_elements = [TOK "(validstatesLP)", TM, TOK "(validstatesRP)"]}

val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "PLS",
                  pp_elements = [TOK "(PLSLP)", TM, TOK "(PLSRP)"]}



val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "problem_plan_bound_charles",
                  pp_elements = [TOK "(problem_plan_bound_charlesLP)", TM, TOK "(problem_plan_bound_charlesRP)"]}

(* force parentheses around valid_plans *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "valid_plans",
                  pp_elements = [TOK "(validplansLP)", TM, TOK "(validplansRP)"]}

(* force parentheses around rd *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "RD",
                  pp_elements = [TOK "(RDLP)", TM, TOK "(RDRP)"]}

(* force parentheses around l *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "problem_plan_bound",
                  pp_elements = [TOK "(ellLP)", TM, TOK "(ellRP)"]}

(* squish n closer to its argument *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT,0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Prefix 900,
                  term_name = "n",
                  pp_elements = [TOK "n"]}

(* squish N closer to its argument *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT,0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Prefix 900,
                  term_name = "N",
                  pp_elements = [TOK "N"]}

(* squish exec_plan closer to its argument *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT,0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Prefix 900,
                  term_name = "exec_plan",
                  pp_elements = [TOK "exec_plan"]}

(* various projections/domain-restrictions *)
val _ = overload_on ("ppPROBPROJ", ``\pi vs. prob_proj(pi,vs)``)
val _ = overload_on ("ppPROBPROJ", ``\fm vs. DRESTRICT fm vs``)
val _ = overload_on ("ppPROBPROJ", ``\as vs. as_proj( as, vs)``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Suffix 2100,
                  term_name = "ppPROBPROJ",
                  pp_elements = [TOK "(projarrowSTART)", TM,
                                 TOK "(projarrowEND)"]}

val _ = overload_on("PICOMPL", ``λvs. (prob_dom PROB) DIFF vs``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "PICOMPL",
                  pp_elements = [TOK "(PICOMPLLP)", TM, TOK "(PICOMPLRP)"]}

(* infix sublist *)
val _ = set_mapped_fixity { term_name = "sublist",
                            fixity = Infix(NONASSOC, 450),
                            tok = "<=<" }

(*
(* Convert IMAGE f *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Suffix 1900,
                  term_name = "IMAGE",
                  pp_elements = [TOK "(IMAGE1)", TM, TOK "(IMAGE2)"]}
*)

(* LENGTH *)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "LENGTH",
                  pp_elements = [TOK "BarLeft|", TM, TOK "|BarRight"]}


val TRUTH = save_thm("TRUTH", TRUTH)

(* alternative presentations of definitions *)
val dep_def = store_thm(
  "dep_def",
  ``dep (PROB, v1, v2) ⇔
      (∃p e. (p,e) ∈ PROB ∧
            (v1 ∈ FDOM p ∧ v2 ∈ FDOM e ∨
             v1 ∈ FDOM e ∧ v2 ∈ FDOM e)) ∨ v1 = v2``,
  simp[dependencyTheory.dep_def, pairTheory.EXISTS_PROD]);

(*val prob_proj_def = store_thm(
  "prob_proj_def",
  ``prob_proj (PROB, vs) = PROB with <| A := { (DRESTRICT p vs, DRESTRICT e vs) | (p,e) ∈ PROB.A } ;
                                        G := DRESTRICT PROB.G vs ;
                                        I := DRESTRICT PROB.I vs |>``,
                                                       simp[GraphPlanTheoremTheory.prob_proj_def, EXTENSION, pairTheory.EXISTS_PROD]);
*)
(*
*)

(* print dep better *)
val _ = overload_on ("deparrow", ``\v1 v2. dep(PROB, v1, v2)``)
val _ = overload_on ("deparrow", ``\vs1 vs2. dep_var_set(PROB, vs1, vs2)``)
val _ = overload_on ("notdeparrow", ``\v1 v2. ¬dep(PROB, v1, v2)``)
val _ = overload_on ("notdeparrow", ``\v1 v2. ¬dep_var_set(PROB, v1, v2)``)

val _ = set_fixity "deparrow" (Infix(NONASSOC, 450))
val _ = set_fixity "notdeparrow" (Infix(NONASSOC, 450))

(* print N_gen *)
val _ = overload_on ("Ngenb", ``\b. N_gen b PROB lvs``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT,0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "Ngenb",
                  pp_elements = [TOK "(NgenSTART)", TM, TOK "(NgenEND)"]}

val _ = overload_on ("Ncurriedgenb", ``(N_gen_curried b PROB lvs)``)

val _ = overload_on ("Ncurriedgenb", ``(N_gen_curried b (PROB:'a problem) lvs)``)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT,0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "Ncurriedgenb",
                  pp_elements = [TOK "(NcurriedgenSTART)", TM, TOK "(NcurriedgenEND)"]}



val _ = overload_on ("Ncurried", ``N_curried PROB lvs``)
val _ = overload_on ("Ncurried", ``N_curried (PROB:'a problem) lvs``)

(*Force parentheses around b*)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "b",
                  pp_elements = [TOK "(baseLP)", TM, TOK "(baseRP)"]}

val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "children",
                  pp_elements = [TOK "(childrenLP)", TM, TOK "(childrenRP)"]}

(*Force parentheses around FDOM*)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "FDOM",
                  pp_elements = [TOK "(FDOMLP)", TM, TOK "(FDOMRP)"]}

(*Force parentheses around prob_dom*)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "prob_dom",
                  pp_elements = [TOK "(prob_domLP)", TM, TOK "(prob_domRP)"]}


(*Force parentheses around Ncurried*)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "Ncurried",
                  pp_elements = [TOK "(NcurriedLP)", TM, TOK "(NcurriedRP)"]}


val _ = overload_on ("topsortedcurried", ``top_sorted_curried (PROB: α problem)``)

val _ = overload_on ("childrencurried", ``children_curried PROB lvs``)
val _ = overload_on ("childrencurried", ``children_curried (PROB:'a problem) lvs``)
val _ = overload_on ("childrencurried", ``children_curried (PROB:(α |-> β) # (α |-> β) -> bool) lvs``)
(*Force parentheses around children_curried*)
val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Closefix,
                  term_name = "childrencurried",
                  pp_elements = [TOK "(childrencurriedLP)", TM, TOK "(childrencurriedRP)"]}


(* handling replace projected *)
val _ = overload_on ("replproj", ``\as1 vs as2. replace_projected ([], as1, as2, vs)``)

val _ = add_rule {block_style = (AroundEachPhrase, (PP.CONSISTENT, 0)),
                  paren_style = OnlyIfNecessary,
                  fixity = Infixl 500,
                  term_name = "replproj",
                  pp_elements = [TOK "(RPROJ1)", TM, 
                                 TOK "(RPROJ2)"]}


val _ = export_theory();
