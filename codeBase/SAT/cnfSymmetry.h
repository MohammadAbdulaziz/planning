#ifndef CNF_SYMM_H
#define CNF_SYMM_H

#include "cnfFormula.h"

#define MAX_CLAUSE_ORBIT_SIZE MAX_CLAUSE_NUMBER
#define MAX_CLAUSE_ORBIT_COUNT MAX_CLAUSE_NUMBER

#define MAX_LIT_ORBIT_SIZE MAX_DOMAIN_SIZE
#define MAX_LIT_ORBIT_COUNT MAX_DOMAIN_SIZE
#define MAX_CONFLICTING_LITS_COUNT MAX_DOMAIN_SIZE


class cnfSymmetry{
 public:
  cnfFormula phi;
  cnfFormula* quotPhi;
  cnfSymmetry(const cnfFormula phiIn): phi (phiIn){;}
  cnfSymmetry(){
    unsigned int i = 0;
    // initialising all conflicting lits to zeros
    for(i = 0; i < phi.domSize ; i++)
      {
        numConflictingLits[2 * i] = 0;
        numConflictingLits[2 * i + 1] = 0;
       }
     numClauseOrbit = 0;
     numLitOrbit = 0;
  }
  ~cnfSymmetry(){
    delete quotPhi;
  }
  unsigned int clauseOrbits[MAX_CLAUSE_ORBIT_COUNT][MAX_CLAUSE_ORBIT_SIZE] ;
  unsigned int clauseOrbitSizes[MAX_CLAUSE_ORBIT_COUNT];
  unsigned int numClauseOrbit;
  unsigned int clauseToOrbitMap[MAX_CLAUSE_NUMBER];
  unsigned int litOrbits[MAX_LIT_ORBIT_COUNT][MAX_LIT_ORBIT_SIZE];
  unsigned int litOrbitSizes[MAX_LIT_ORBIT_COUNT];
  unsigned int numLitOrbit;
  unsigned int litToOrbitMap[2 * MAX_DOMAIN_SIZE]; //This is only suitable to boolean constants. If the domain size is arbitrary, it will not work.
  unsigned int quotientVarNameNormalsiation[MAX_DOMAIN_SIZE + 1];
  unsigned int colouringConstraints[2 * MAX_DOMAIN_SIZE][MAX_CONFLICTING_LITS_COUNT];
  unsigned int numConflictingLits[2 * MAX_DOMAIN_SIZE];

  unsigned int colours[MAX_DOMAIN_SIZE][MAX_DOMAIN_SIZE];
  unsigned int colourSize[MAX_DOMAIN_SIZE];
  unsigned int nColours;


  void PrintcnfFormulaDescriptionGraph();
  void colourLiteralSymmetry(FILE* naut);
  void initColours();
  void getColours();
  void getColoursIncremental();
  void getColoursAssOrbit();
  void getOrbits();
  std::vector<std::pair<int, int> > getOrbitColouringGraph(unsigned int orbitIndex);
  std::vector<std::pair<int, int> > getOrbitColouringGraphVarSymm(unsigned int orbitIndex);
  double symmDetTime;
  void constructUniformModel();
  void solveBySolvingQuotient();
};

#endif
