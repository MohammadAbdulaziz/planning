#include "cnfSymmetry.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string.h>

//#define debug

// void cnfSymmetry::colourLiteralSymmetry(FILE* naut)
//   {
//     //one colour for positive and negative literals
//     int j;
//     for(j = 0 ; j < (int)phi.domSize - 1; j ++)
//       {
//         fprintf(naut, "%d,", (int)phi.clausesSize + 2 * j);        
//         fprintf(naut, "%d,", (int)phi.clausesSize + 2 * j + 1);        
//       }
//     fprintf(naut, "%d,", (int)phi.clausesSize + 2 * j);        
//     fprintf(naut, "%d", (int)phi.clausesSize + 2 * j + 1);        
//   }


void cnfSymmetry::colourLiteralSymmetry(FILE* naut)
  {

    for(unsigned int i = 0 ; i < nColours ; i ++)
      {
        unsigned int j;
        for(j = 0 ; j < colourSize[i] - 1; j ++)
          {
            fprintf(naut, "%d,", (int)phi.clausesSize + colours[i][j] - 2);
          }
        fprintf(naut, "%d", (int)phi.clausesSize + colours[i][j] - 2);
        if(i < nColours - 1)
          fprintf(naut, "|");
      }
  }

void cnfSymmetry::PrintcnfFormulaDescriptionGraph()
  {
    std::cout << "Number of clauses = " << phi.clausesSize << std::endl;
    std::cout << "Number of variables = " << phi.domSize << std::endl;
    FILE* naut = fopen("nauty_in", "w");
    fprintf(naut, "At\r\n\r\n-a\r\n-m\r\nn=%d g\r\n", phi.clausesSize + 2*phi.domSize);
    std::cout << "Printing clause edges"<< std::endl;
    //Printing action edges
    for(int i = 0; i < (int) phi.clausesSize ; i++)
      {
        fprintf(naut, "%d: ", i);
        //Printing clause positive lits edges: they must only be connected to even var nodes (positive literals)
        for(int j = 0; j < (int)phi.clauses[i].posLitsCount ; j++)
          {
            fprintf(naut, "%d ",  (int)phi.clausesSize + 2 * (phi.clauses[i]._posLits[j] - 1));
	  }
        //Printing clause negative lits edges: they must only be connected to odd var nodes (negative literals)
        for(int j = 0; j < (int)phi.clauses[i].negLitsCount ; j++)
          {
            fprintf(naut, "%d ",  (int)phi.clausesSize + 2 * (phi.clauses[i]._negLits[j] - 1) + 1);
	  }
        fprintf(naut, ";\r\n");
        fflush(stdout);
      }
    std::cout << "Printing variable edges (only to variables)"<< std::endl;
    int i = 0;
    for(i = 0; i < (int)phi.domSize - 1 ; i++)
      {
        fprintf(naut, "%d:%d;\r\n", (int)phi.clausesSize + 2 * i, (int)phi.clausesSize + 2 * i + 1);
      }
    fprintf(naut, "%d:%d\r\n", (int)phi.clausesSize + 2 * i, (int)phi.clausesSize + 2 * i + 1);
    //printing colouring edges 


    fprintf(naut, ".\r\n");
    
    //one colour for clauses
    fprintf(naut, "f=[");
    for(i = 0; i < (int)phi.clausesSize - 1 ; i++)
      fprintf(naut, "%d,", i);
    fprintf(naut, "%d|", i);
    colourLiteralSymmetry(naut);
    fprintf(naut, "] x o\r\n"); 
    fclose(naut);
  }


void cnfSymmetry::initColours()
{
  nColours = 2;
  colourSize[0] = phi.domSize;
  colourSize[1] = phi.domSize;
  for(unsigned int i = 0 ; i < phi.domSize ; i++)
    {
      colours[0][i] = 2 * i + 2;
      colours[1][i] = 2 * i + 2 + 1;
    }
}


std::vector<std::pair<int, int> > cnfSymmetry::getOrbitColouringGraph(unsigned int orbitIndex)
  {

    std::vector<std::pair<int, int> > orbitColouringConstraints;
    //Constraint between literal l1 and l2 should be added if: a) l1 = not l2 b) l1 and l2 are in the same clause
    printf("Debugging colour graph generation for orbit %d\r\n", orbitIndex);
    for(unsigned int i = 0 ; i < litOrbitSizes[orbitIndex] ; i++)
      {
        printf("litOrbits[orbitIndex][i]=%d\r\n", litOrbits[orbitIndex][i]);
        printf("phi.clausesStatingNegLit[litOrbits[orbitIndex][i]/2]=%d\r\n", phi.clausesStatingNegLit[litOrbits[orbitIndex][i]/2]);
        for(unsigned int j = i + 1 ; j < litOrbitSizes[orbitIndex] ; j++)
          {
            printf("litOrbits[orbitIndex][j]=%d\r\n", litOrbits[orbitIndex][j]);
            if(litOrbits[orbitIndex][i]/2 == litOrbits[orbitIndex][j]/2)
              {
                orbitColouringConstraints.push_back(std::pair<int,int>(litOrbits[orbitIndex][i],litOrbits[orbitIndex][j]));
              }
            else if(litOrbits[orbitIndex][i]%2 == 0 && litOrbits[orbitIndex][j]%2 == 0
                    && !interEmpty(phi.posLit_to_clause[litOrbits[orbitIndex][i]/2], phi.clausesStatingPosLit[litOrbits[orbitIndex][i]/2],
                                   phi.posLit_to_clause[litOrbits[orbitIndex][j]/2], phi.clausesStatingPosLit[litOrbits[orbitIndex][j]/2]))
              {
                orbitColouringConstraints.push_back(std::pair<int,int>(litOrbits[orbitIndex][i],litOrbits[orbitIndex][j]));
              }
            else if(litOrbits[orbitIndex][i]%2 == 0 && litOrbits[orbitIndex][j]%2 != 0
                    && ! interEmpty(phi.posLit_to_clause[litOrbits[orbitIndex][i]/2], phi.clausesStatingPosLit[litOrbits[orbitIndex][i]/2],
                                    phi.negLit_to_clause[litOrbits[orbitIndex][j]/2], phi.clausesStatingNegLit[litOrbits[orbitIndex][j]/2]))
              {
                orbitColouringConstraints.push_back(std::pair<int,int>(litOrbits[orbitIndex][i],litOrbits[orbitIndex][j]));
              }
            else if(litOrbits[orbitIndex][i]%2 != 0 && litOrbits[orbitIndex][j]%2 == 0
                    && ! interEmpty(phi.negLit_to_clause[litOrbits[orbitIndex][i]/2], phi.clausesStatingNegLit[litOrbits[orbitIndex][i]/2],
                                    phi.posLit_to_clause[litOrbits[orbitIndex][j]/2], phi.clausesStatingPosLit[litOrbits[orbitIndex][j]/2]))
              {
                orbitColouringConstraints.push_back(std::pair<int,int>(litOrbits[orbitIndex][i],litOrbits[orbitIndex][j]));
              }
            else if(litOrbits[orbitIndex][i]%2 != 0 && litOrbits[orbitIndex][j]%2 != 0
                    && ! interEmpty(phi.negLit_to_clause[litOrbits[orbitIndex][i]/2], phi.clausesStatingNegLit[litOrbits[orbitIndex][i]/2],
                                    phi.negLit_to_clause[litOrbits[orbitIndex][j]/2], phi.clausesStatingNegLit[litOrbits[orbitIndex][j]/2]))
              {
                orbitColouringConstraints.push_back(std::pair<int,int>(litOrbits[orbitIndex][i],litOrbits[orbitIndex][j]));
              }
          }
      }
    printf("\r\n");
    return orbitColouringConstraints;
  }


std::vector<std::pair<int, int> > cnfSymmetry::getOrbitColouringGraphVarSymm(unsigned int orbitIndex)
  {
    std::vector<std::pair<int, int> > orbitColouringConstraints;
    //Constraint between literal l1 and l2 should be added if: a) l1 = not l2 b) l1 and l2 are in the same clause
    printf("Debugging colour graph generation for orbit %d\r\n", orbitIndex);
    for(unsigned int i = 0 ; i < litOrbitSizes[orbitIndex] ; i++)
      {
        printf("litOrbits[orbitIndex][i]=%d\r\n", litOrbits[orbitIndex][i]);
        printf("phi.clausesStatingNegLit[litOrbits[orbitIndex][i]/2]=%d\r\n", phi.clausesStatingNegLit[litOrbits[orbitIndex][i]/2]);
        for(unsigned int j = i + 1 ; j < litOrbitSizes[orbitIndex] ; j++)
          {
            printf("litOrbits[orbitIndex][j]=%d\r\n", litOrbits[orbitIndex][j]);
            if(litOrbits[orbitIndex][i]/2 == litOrbits[orbitIndex][j]/2)
              {
                orbitColouringConstraints.push_back(std::pair<int,int>(litOrbits[orbitIndex][i],litOrbits[orbitIndex][j]));
              }
            else if(  (!interEmpty(phi.posLit_to_clause[litOrbits[orbitIndex][i]/2], phi.clausesStatingPosLit[litOrbits[orbitIndex][i]/2],
                                   phi.posLit_to_clause[litOrbits[orbitIndex][j]/2], phi.clausesStatingPosLit[litOrbits[orbitIndex][j]/2]))
                    ||(!interEmpty(phi.posLit_to_clause[litOrbits[orbitIndex][i]/2], phi.clausesStatingPosLit[litOrbits[orbitIndex][i]/2],
                                   phi.negLit_to_clause[litOrbits[orbitIndex][j]/2], phi.clausesStatingNegLit[litOrbits[orbitIndex][j]/2]))
                    ||(!interEmpty(phi.negLit_to_clause[litOrbits[orbitIndex][i]/2], phi.clausesStatingNegLit[litOrbits[orbitIndex][i]/2],
                                   phi.posLit_to_clause[litOrbits[orbitIndex][j]/2], phi.clausesStatingPosLit[litOrbits[orbitIndex][j]/2]))
                    ||(!interEmpty(phi.negLit_to_clause[litOrbits[orbitIndex][i]/2], phi.clausesStatingNegLit[litOrbits[orbitIndex][i]/2],
                                   phi.negLit_to_clause[litOrbits[orbitIndex][j]/2], phi.clausesStatingNegLit[litOrbits[orbitIndex][j]/2])))
              {
                orbitColouringConstraints.push_back(std::pair<int,int>(litOrbits[orbitIndex][i],litOrbits[orbitIndex][j]));
              }
          }
      }
    printf("\r\n");
    return orbitColouringConstraints;
  }

void cnfSymmetry::getColours()
  {
    nColours = 0;
    double smtColouringTime;
    std::set<int> smtColouringVertices;
    std::vector<std::pair<int, int> > smtColouringConstraints;
    for(unsigned int i = 0 ; i < numLitOrbit ; i++)
      {
        smtColouringVertices.clear();
        printf("Colouring orbit %d of size %d\r\n", i, litOrbitSizes[i]);
        smtColouringConstraints = getOrbitColouringGraph(i);
        for(unsigned int j = 0 ; j < litOrbitSizes[i] ; j++)
          {
            smtColouringVertices.insert(litOrbits[i][j]);
          }
        std::vector< std::set<int> > smtColouring;
        int minColours = 1;
        for(unsigned int j = minColours ; j < litOrbitSizes[i]; j ++)
          {
            printf("smtColouringVertices.size()=%d\r\n", (int)smtColouringVertices.size());
            printf("Trying to colour with %d colours\r\n", j);
            smtColouring =  smtColour(smtColouringVertices, smtColouringConstraints, j, smtColouringTime);
            if(smtColouring.size() != 0)
              {
                printf("Successfully coloured with %d colours\r\n", (int)smtColouring.size());
                for(unsigned int k = nColours; k < nColours + smtColouring.size() ; k++)
                  {
                    printf("Colour %d = ", k);
                    colourSize[k] = smtColouring[k - nColours].size();
                    std::set<int>::iterator colour_iter = smtColouring[k - nColours].begin();
                    for(unsigned int l = 0 ; l < smtColouring[k - nColours].size() ; l++)
                      {
                        colours[k][l] = *colour_iter;
                        colour_iter++;
                        printf("%d ", colours[k][l]);
		      }
                    printf("\r\n");
                  }
                nColours += smtColouring.size();
                break;
              }
          }        
      }
  }


void cnfSymmetry::getColoursIncremental()
  {
  }

void cnfSymmetry::getColoursAssOrbit()
  {
    nColours = 0;
    double smtColouringTime;
    std::set<int> smtColouringVertices;
    std::vector<std::pair<int, int> > smtColouringConstraints;
    for(unsigned int i = 0 ; i < numLitOrbit ; i++)
      {
        smtColouringVertices.clear();
        printf("Colouring orbit %d of size %d\r\n", i, litOrbitSizes[i]);
        smtColouringConstraints = getOrbitColouringGraphVarSymm(i);
        for(unsigned int j = 0 ; j < litOrbitSizes[i] ; j++)
          {
            smtColouringVertices.insert(litOrbits[i][j]);
          }
        std::vector< std::set<int> > smtColouring;
        int minColours = 1;//This has a lower bound more than 1
        for(unsigned int j = minColours ; j < litOrbitSizes[i]; j ++)
          {
            printf("smtColouringVertices.size()=%d\r\n", (int)smtColouringVertices.size());
            printf("Trying to colour with %d colours\r\n", j);
            smtColouring =  smtColour(smtColouringVertices, smtColouringConstraints, j, smtColouringTime);
            if(smtColouring.size() != 0)
              {
                printf("Successfully coloured with %d colours\r\n", (int)smtColouring.size());
                for(unsigned int k = nColours; k < nColours + smtColouring.size() ; k++)
                  {
                    printf("Colour %d = ", k);
                    colourSize[k] = smtColouring[k - nColours].size();
                    std::set<int>::iterator colour_iter = smtColouring[k - nColours].begin();
                    for(unsigned int l = 0 ; l < smtColouring[k - nColours].size() ; l++)
                      {
                        colours[k][l] = *colour_iter;
                        colour_iter++;
                        printf("%d ", colours[k][l]);
		      }
                    printf("\r\n");
                  }
                nColours += smtColouring.size();
                break;
              }
          }        
      }
    numLitOrbit = nColours;
    printf("Splitted orbits are:\r\n");
    for(unsigned int i = 0 ; i < numLitOrbit ; i++)
      {
        printf("Orbit %d: ", i);
        litOrbitSizes[i] = colourSize[i];
        for(unsigned int j = 0 ; j < litOrbitSizes[i] ; j++)
          {
            litOrbits[i][j] = colours[i][j];
            printf("%d ", litOrbits[i][j]);
          }
        printf("\r\n");
      }    
  }

void cnfSymmetry::getOrbits()
  {
    system("$NAUTY_DIR/dreadnaut < nauty_in > nauty_out");
    FILE* naut = fopen("nauty_out", "r");
    char token[100];
    fscanf(naut, "%s", token);
    char time_token[100];
    token[0] = '\0';
    while(strcmp(token,"seconds")!=0)
      {
        strcpy(time_token, token);
        fscanf(naut, "%s", token);
      }
    symmDetTime = strtod(time_token, NULL);
    numLitOrbit = -1;
    numClauseOrbit = -1;
    std::set<int> current_orbit;
    char orbitType = 0; //0 init 1 lit orbit 2 clause orbit
    while(fscanf(naut, "%s", token)!=EOF)
      {
#ifdef debug
        printf("Number of clause orbits = %d\r\n", numClauseOrbit + 1); 
#endif
        if(token[0]!='(')//size of an orbit token
          {
            if(strstr(token,":")!=NULL)
              {
                char*subtoken;
                subtoken = strtok(token,":");
                unsigned int low_range = strtol(subtoken, NULL,10);
                subtoken = strtok(NULL,":");
                unsigned int high_range = strtol(subtoken, NULL,10);
#ifdef debug
                printf("Orbit %d:%d\r\n", low_range, high_range);
#endif
                if(orbitType == 0 && low_range <= phi.clausesSize - 1 && high_range <= phi.clausesSize - 1)//clause orbit
                  {
                    orbitType = 2;
                    numClauseOrbit++;
                    clauseOrbitSizes[numClauseOrbit] = 0;
                    for(unsigned int i = low_range ; i <= high_range; i++)
                      {
                        clauseOrbits[numClauseOrbit][clauseOrbitSizes[numClauseOrbit]] = i;
                        clauseOrbitSizes[numClauseOrbit]++;
                        clauseToOrbitMap[i] = numClauseOrbit;
                      }
                  }
                else if(orbitType == 2 && low_range <= phi.clausesSize - 1 && high_range <= phi.clausesSize - 1)
                  {
                    for(unsigned int i = low_range ; i <= high_range; i++)
                      {
                        clauseOrbits[numClauseOrbit][clauseOrbitSizes[numClauseOrbit]] = i;
                        clauseOrbitSizes[numClauseOrbit]++;
                        clauseToOrbitMap[i] = numClauseOrbit;
                      }
                  }
                else if(orbitType == 0 && low_range > phi.clausesSize - 1 && high_range > phi.clausesSize - 1)//lit orbit
                  {
                    orbitType = 1;
                    numLitOrbit++;
                    litOrbitSizes[numLitOrbit] = 0;
                    for(unsigned int i = low_range ; i <= high_range; i++)
                      {
                        litOrbits[numLitOrbit][litOrbitSizes[numLitOrbit]] = i - phi.clausesSize + 2;
                        litOrbitSizes[numLitOrbit]++;
                        litToOrbitMap[i - phi.clausesSize + 2] = numLitOrbit;
                      }
                  }
                else if(orbitType == 1 && low_range > phi.clausesSize - 1 && high_range > phi.clausesSize - 1)
                  {
                    for(unsigned int i = low_range ; i <= high_range; i++)
                      {
                        litOrbits[numLitOrbit][litOrbitSizes[numLitOrbit]] = i - phi.clausesSize + 2;
                        litOrbitSizes[numLitOrbit]++;
                        litToOrbitMap[i - phi.clausesSize + 2] = numLitOrbit;
                      }
                  }
                else
                  {
                    printf("Orbit problem, exiting!!\r\n");
                    exit(-1);
                  }               
                //Add all the range to the current orbit
              }
            else//We are only taking even orbits because odd ones are isomorphic to them
              {
                unsigned int vtx_id = strtol(token, NULL,10);
                //printf("vtx_id %d\r\n", vtx_id);
                //Add this vtx to the current orbit
                if(orbitType == 0  && vtx_id <= phi.clausesSize - 1)//clause orbit
                  {
                    orbitType = 2;
                    numClauseOrbit++;
                    clauseOrbitSizes[numClauseOrbit] = 0;
                    clauseOrbits[numClauseOrbit][clauseOrbitSizes[numClauseOrbit]] = vtx_id;
                    clauseOrbitSizes[numClauseOrbit]++;
                    clauseToOrbitMap[vtx_id] = numClauseOrbit;
                  }
                else if(orbitType == 2  && vtx_id <= phi.clausesSize - 1)//clause orbit
                  {
                    clauseOrbits[numClauseOrbit][clauseOrbitSizes[numClauseOrbit]] = vtx_id;
                    clauseOrbitSizes[numClauseOrbit]++;
                    clauseToOrbitMap[vtx_id] = numClauseOrbit;
                  }
                else if(orbitType == 0  && vtx_id > phi.clausesSize - 1)//lit orbit
                  {
                    orbitType = 1;
                    numLitOrbit++;
                    litOrbitSizes[numLitOrbit] = 0;
                    litOrbits[numLitOrbit][litOrbitSizes[numLitOrbit]] = vtx_id - phi.clausesSize + 2;
                    litOrbitSizes[numLitOrbit]++;
                    litToOrbitMap[vtx_id - phi.clausesSize] = numLitOrbit;
                  }
                else if(orbitType == 1  && vtx_id > phi.clausesSize - 1)//lit orbit
                  {
                    litOrbits[numLitOrbit][litOrbitSizes[numLitOrbit]] = vtx_id - phi.clausesSize + 2;
                    litOrbitSizes[numLitOrbit]++;
                    litToOrbitMap[vtx_id - phi.clausesSize] = numLitOrbit;
                  }
                else
                  {
                    printf("Orbit problem, exiting!!\r\n");
                    exit(-1);
                  }
                //printf("%d ", vtx_id);
              }
          }
        if(strstr(token,";") !=NULL)//end of current orbit
          {
            orbitType = 0;
          }
      }
    numClauseOrbit++;
    printf("Number of clause orbits = %d\r\n", numClauseOrbit);
    numLitOrbit++;
    printf("Number of variable orbits = %d\r\n", numLitOrbit);
    fclose(naut);
  }

void cnfSymmetry::constructUniformModel()
{
#define quotModel quotPhi->model
#define currentPosLitOrbitIndex quotModel._posLits[i] - 1
  phi.model.posLitsCount = 0;
  phi.model.negLitsCount = 0;
  printf("Computed model is ");
  for(unsigned int i = 0 ; i < quotModel.posLitsCount ; i++)
    {
      for(unsigned int j = 0 ; j < litOrbitSizes[quotientVarNameNormalsiation[currentPosLitOrbitIndex + 1] - 1] ; j++)
        {
          if(litOrbits[quotientVarNameNormalsiation[currentPosLitOrbitIndex + 1] - 1][j]%2 == 0)//positive literal
            {
              phi.model._posLits[phi.model.posLitsCount] = litOrbits[quotientVarNameNormalsiation[currentPosLitOrbitIndex + 1] - 1][j]/2;
              printf("%d ", phi.model._posLits[phi.model.posLitsCount]);
              phi.model.posLitsCount++;
            }
          else //negative literal
            {
              phi.model._negLits[phi.model.negLitsCount] = litOrbits[quotientVarNameNormalsiation[currentPosLitOrbitIndex + 1] - 1][j]/2;
              printf("-%d ", phi.model._negLits[phi.model.negLitsCount]);
              phi.model.negLitsCount++;
            }
        }
    }
#define currentNegLitOrbitIndex quotModel._negLits[i] - 1
  for(unsigned int i = 0 ; i < quotModel.negLitsCount ; i++)
    {
      for(unsigned int j = 0 ; j < litOrbitSizes[quotientVarNameNormalsiation[currentNegLitOrbitIndex + 1] - 1] ; j++)
        {
          if(litOrbits[quotientVarNameNormalsiation[currentNegLitOrbitIndex + 1] - 1][j]%2 != 0)//negative literal was negative in the quotient model
            {
              phi.model._posLits[phi.model.posLitsCount] = litOrbits[quotientVarNameNormalsiation[currentNegLitOrbitIndex + 1] - 1][j]/2;
              printf("%d ", phi.model._posLits[phi.model.posLitsCount]);
              phi.model.posLitsCount++;
            }
          else //positive literal
            {
              phi.model._negLits[phi.model.negLitsCount] = litOrbits[quotientVarNameNormalsiation[currentNegLitOrbitIndex + 1] - 1][j]/2;
              printf("-%d ", phi.model._negLits[phi.model.negLitsCount]);
              phi.model.negLitsCount++;
            }
        }
    }
  printf("\r\n");
  phi.SAT = 1; //Assuming that this is a model for phi, whihc is to be verified
}


void cnfSymmetry::solveBySolvingQuotient()
{
  // quotPhi = phi.getQuotient(this->litOrbits, this->numLitOrbit,  this->litOrbitSizes,
  //  			    this->clauseOrbits, this->numClauseOrbit,  this->clauseOrbitSizes, quotientVarNameNormalsiation);
  quotPhi = phi.getQuotientArbitrary(this->litOrbits, this->numLitOrbit,  this->litOrbitSizes, quotientVarNameNormalsiation);
  if(quotPhi == NULL)
    {
      printf("Quotient formuala undefined!!\r\n");
      return;
    }
  quotPhi->writeFormulaDIMACS();
  quotPhi->searchModel();
  if(quotPhi->SAT == 1)
    {  
      printf("Validating quotient model...\r\n");
      quotPhi->validateModel();
      constructUniformModel();
      printf("Validating uniform model...\r\n");
      phi.validateModel();
    }
}
