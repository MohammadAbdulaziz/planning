#ifndef CNF_VAR_SYMM_H
#define CNF_VAR_SYMM_H

#include "cnfFormula.h"

#define MAX_CLAUSE_ORBIT_SIZE MAX_CLAUSE_NUMBER/10
#define MAX_CLAUSE_ORBIT_COUNT MAX_CLAUSE_NUMBER/10

#define MAX_VAR_ORBIT_SIZE MAX_DOMAIN_SIZE/10
#define MAX_VAR_ORBIT_COUNT MAX_DOMAIN_SIZE/10
#define MAX_CONFLICTING_VAR_COUNT MAX_DOMAIN_SIZE/10


class cnfVarSymmetry{
 public:
  cnfFormula phi;
  cnfFormula* quotPhi;
  cnfVarSymmetry(const cnfFormula phiIn): phi (phiIn){;}
  cnfVarSymmetry(){
    unsigned int i = 0;
    // initialising all conflicting vars to zeros
    for(i = 0; i < phi.domSize ; i++)
      {
        numConflictingVars[2 * i] = 0;
        numConflictingVars[2 * i + 1] = 0;
       }
     numClauseOrbit = 0;
     numVarOrbit = 0;
  }
  ~cnfVarSymmetry(){
    delete quotPhi;
  }
  unsigned int clauseOrbits[MAX_CLAUSE_ORBIT_COUNT][MAX_CLAUSE_ORBIT_SIZE] ;
  unsigned int clauseOrbitSizes[MAX_CLAUSE_ORBIT_COUNT];
  unsigned int numClauseOrbit;
  unsigned int clauseToOrbitMap[MAX_CLAUSE_NUMBER];
  unsigned int varOrbits[MAX_VAR_ORBIT_COUNT][MAX_VAR_ORBIT_SIZE];
  unsigned int varOrbitSizes[MAX_VAR_ORBIT_COUNT];
  unsigned int numVarOrbit;
  unsigned int varToOrbitMap[MAX_DOMAIN_SIZE]; //This is only suitable to boolean constants. If the domain size is arbitrary, it will not work.
  unsigned int quotientVarNameNormalsiation[MAX_DOMAIN_SIZE + 1];
  unsigned int colouringConstraints[MAX_DOMAIN_SIZE][MAX_CONFLICTING_VAR_COUNT];
  unsigned int numConflictingVars[MAX_DOMAIN_SIZE];

  unsigned int colours[MAX_DOMAIN_SIZE][MAX_DOMAIN_SIZE];
  unsigned int colourSize[MAX_DOMAIN_SIZE];
  unsigned int nColours;


  void PrintcnfFormulaDescriptionGraph();
  void colourVarSymmetry(FILE* naut);
  void initColours();
  void getColours();
  void getColoursIncremental();
  void getColoursAssOrbit();
  void getOrbits();
  std::vector<std::pair<int, int> > getOrbitColouringGraph(unsigned int orbitIndex);
  std::vector<std::pair<int, int> > getOrbitColouringGraphVarSymm(unsigned int orbitIndex);
  double symmDetTime;
  void constructUniformModel();
  void solveBySolvingQuotient();
  void mergeColours();
};

#endif
