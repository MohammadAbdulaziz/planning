#include "cnfVarSymmetry.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string.h>

//#define debug

void cnfVarSymmetry::colourVarSymmetry(FILE* naut)
  {

    for(unsigned int i = 0 ; i < nColours ; i ++)
      {
        unsigned int j;
        for(j = 0 ; j < colourSize[i] - 1; j ++)
          {
            fprintf(naut, "%d,", (int)phi.clausesSize + 2 * colours[i][j] - 2);
          }
        fprintf(naut, "%d", (int)phi.clausesSize + 2 * colours[i][j] - 2);
        fprintf(naut, "|\r\n");
      }
    for(unsigned int i = 0 ; i < nColours ; i ++)
      {
        unsigned int j;
        for(j = 0 ; j < colourSize[i] - 1; j ++)
          {
            fprintf(naut, "%d,", (int)phi.clausesSize + 2 * colours[i][j] - 2 + 1);
          }
        fprintf(naut, "%d", (int)phi.clausesSize + 2 * colours[i][j] - 2 + 1);
        if(i < nColours - 1)
          fprintf(naut, "|\r\n");
      }
  }

void cnfVarSymmetry::PrintcnfFormulaDescriptionGraph()
  {
    std::cout << "Number of clauses = " << phi.clausesSize << std::endl;
    std::cout << "Number of variables = " << phi.domSize << std::endl;
    FILE* naut = fopen("nauty_in", "w");
    fprintf(naut, "At\r\n\r\n-a\r\n-m\r\nn=%d g\r\n", phi.clausesSize + 2*phi.domSize);
    std::cout << "Printing clause edges"<< std::endl;
    //Printing action edges
    for(int i = 0; i < (int) phi.clausesSize ; i++)
      {
        fprintf(naut, "%d: ", i);
        //Printing clause positive lits edges: they must only be connected to even var nodes (positive literals)
        for(int j = 0; j < (int)phi.clauses[i].posLitsCount ; j++)
          {
            fprintf(naut, "%d ",  (int)phi.clausesSize + 2 * (phi.clauses[i]._posLits[j] - 1));
	  }
        //Printing clause negative lits edges: they must only be connected to odd var nodes (negative literals)
        for(int j = 0; j < (int)phi.clauses[i].negLitsCount ; j++)
          {
            fprintf(naut, "%d ",  (int)phi.clausesSize + 2 * (phi.clauses[i]._negLits[j] - 1) + 1);
	  }
        fprintf(naut, ";\r\n");
        fflush(stdout);
      }
    std::cout << "Printing variable edges (only to variables)"<< std::endl;
    int i = 0;
    for(i = 0; i < (int)phi.domSize - 1 ; i++)
      {
        fprintf(naut, "%d:%d;\r\n", (int)phi.clausesSize + 2 * i, (int)phi.clausesSize + 2 * i + 1);
      }
    fprintf(naut, "%d:%d\r\n", (int)phi.clausesSize + 2 * i, (int)phi.clausesSize + 2 * i + 1);
    //printing colouring edges 
    fprintf(naut, ".\r\n");
    
    //one colour for clauses
    fprintf(naut, "f=[");
    for(i = 0; i < (int)phi.clausesSize - 1 ; i++)
      fprintf(naut, "%d,", i);
    fprintf(naut, "%d|\r\n", i);
    colourVarSymmetry(naut);
    fprintf(naut, "] x o\r\n"); 
    fclose(naut);
  }


void cnfVarSymmetry::initColours()
{
  nColours = 1;
  colourSize[0] = phi.domSize;
  for(unsigned int i = 0 ; i < phi.domSize ; i++)
    {
      colours[0][i] = i + 1;
    }
}



std::vector<std::pair<int, int> > cnfVarSymmetry::getOrbitColouringGraphVarSymm(unsigned int orbitIndex)
  {
    std::vector<std::pair<int, int> > orbitColouringConstraints;
    //Constraint between literal l1 and l2 should be added if: a) l1 = not l2 b) l1 and l2 are in the same clause
#ifdef debug
    printf("Debugging colour graph generation for orbit %d\r\n", orbitIndex);
#endif
    for(unsigned int i = 0 ; i < varOrbitSizes[orbitIndex] ; i++)
      {
#ifdef debug
        printf("varOrbits[orbitIndex][i]=%d\r\n", varOrbits[orbitIndex][i]);
        printf("phi.clausesStatingNegLit[varOrbits[orbitIndex][i]]=%d\r\n", phi.clausesStatingNegLit[varOrbits[orbitIndex][i]]);
#endif
        for(unsigned int j = i + 1 ; j < varOrbitSizes[orbitIndex] ; j++)
          {
#ifdef debug
            printf("litOrbits[orbitIndex][j]=%d\r\n", varOrbits[orbitIndex][j]);
#endif
            if((!interEmpty(phi.posLit_to_clause[varOrbits[orbitIndex][i]], phi.clausesStatingPosLit[varOrbits[orbitIndex][i]],
                            phi.posLit_to_clause[varOrbits[orbitIndex][j]], phi.clausesStatingPosLit[varOrbits[orbitIndex][j]]))
                ||(!interEmpty(phi.posLit_to_clause[varOrbits[orbitIndex][i]], phi.clausesStatingPosLit[varOrbits[orbitIndex][i]],
                               phi.negLit_to_clause[varOrbits[orbitIndex][j]], phi.clausesStatingNegLit[varOrbits[orbitIndex][j]]))
                ||(!interEmpty(phi.negLit_to_clause[varOrbits[orbitIndex][i]], phi.clausesStatingNegLit[varOrbits[orbitIndex][i]],
                               phi.posLit_to_clause[varOrbits[orbitIndex][j]], phi.clausesStatingPosLit[varOrbits[orbitIndex][j]]))
                ||(!interEmpty(phi.negLit_to_clause[varOrbits[orbitIndex][i]], phi.clausesStatingNegLit[varOrbits[orbitIndex][i]],
                               phi.negLit_to_clause[varOrbits[orbitIndex][j]], phi.clausesStatingNegLit[varOrbits[orbitIndex][j]])))
              {
                orbitColouringConstraints.push_back(std::pair<int,int>(varOrbits[orbitIndex][i],varOrbits[orbitIndex][j]));
#ifdef debug
                printf("(%d,%d)\r\n", varOrbits[orbitIndex][i], varOrbits[orbitIndex][j]);
#endif
              }
          }
      }
#ifdef debug
    printf("\r\n");
#endif
    return orbitColouringConstraints;
  }

void cnfVarSymmetry::getColours()
  {
    nColours = 0;
    double smtColouringTime;
    std::set<int> smtColouringVertices;
    std::vector<std::pair<int, int> > smtColouringConstraints;
    for(unsigned int i = 0 ; i < numVarOrbit ; i++)
      {
        smtColouringVertices.clear();
#ifdef debug
        printf("Colouring orbit %d of size %d\r\n", i, varOrbitSizes[i]);
#endif
        smtColouringConstraints = getOrbitColouringGraphVarSymm(i);
        for(unsigned int j = 0 ; j < varOrbitSizes[i] ; j++)
          {
            smtColouringVertices.insert(varOrbits[i][j]);
          }
        std::vector< std::set<int> > smtColouring;
        int minColours = 1;
        for(unsigned int j = minColours ; j < varOrbitSizes[i]; j ++)
          {
#ifdef debug
            printf("smtColouringVertices.size()=%d\r\n", (int)smtColouringVertices.size());
            printf("Trying to colour with %d colours\r\n", j);
#endif
            smtColouring =  smtColour(smtColouringVertices, smtColouringConstraints, j, smtColouringTime);
            if(smtColouring.size() != 0)
              {
#ifdef debug
                printf("Successfully coloured with %d colours\r\n", (int)smtColouring.size());
#endif
                for(unsigned int k = nColours; k < nColours + smtColouring.size() ; k++)
                  {
#ifdef debug
                    printf("Colour %d = ", k);
#endif
                    colourSize[k] = smtColouring[k - nColours].size();
                    std::set<int>::iterator colour_iter = smtColouring[k - nColours].begin();
                    for(unsigned int l = 0 ; l < smtColouring[k - nColours].size() ; l++)
                      {
                        colours[k][l] = *colour_iter;
                        colour_iter++;
#ifdef debug
                        printf("%d ", colours[k][l]);
#endif
		      }
#ifdef debug
                    printf("\r\n");
#endif
                  }
                nColours += smtColouring.size();
                break;
              }
          }        
      }
  }

void cnfVarSymmetry::mergeColours()
  {
    unsigned int smallestColuor = 0;
    for(unsigned int i = 0 ; i < nColours ; i++)
      {
        if(colourSize[i] > colourSize[smallestColuor])
          smallestColuor = i;
      }
    printf("Smallest colur is %d of size %d\r\n", smallestColuor, colourSize[smallestColuor]);
    unsigned int k = 0;
    if(smallestColuor != 0)
      {
         for(unsigned int i = 0 ; i < nColours ; i++)
          {
            for(unsigned int j = 0 ; j < colourSize[i] ; j++)
              {
                if(i != smallestColuor)
                  {
                    colours[0][k] = colours[i][j];
                    k++;
                  }
              }
          }
        for(unsigned int j = 0 ; j < colourSize[smallestColuor] ; j++)
          {
            colours[1][j] = colours[smallestColuor][j];
          }
        colourSize[0] = k;
        colourSize[1] = colourSize[smallestColuor];
      }
    else
      {
         for(unsigned int i = 0 ; i < nColours ; i++)
          {
            for(unsigned int j = 0 ; j < colourSize[i] ; j++)
              {
                if(i != smallestColuor)
                  {
                    colours[1][k] = colours[i][j];
                    k++;
                  }
              }
          }
        colourSize[1] = k;
      }
    nColours = 2;
  }


void cnfVarSymmetry::getColoursIncremental()
  {
    printf("Incrementally splitting orbits..\r\n");
    printf("Num Orbits = %d\r\n", numVarOrbit);
    getColours();
    unsigned int prevOrbits = numVarOrbit;
    mergeColours();
    PrintcnfFormulaDescriptionGraph();
    getOrbits();
    printf("Num Orbits = %d\r\n", numVarOrbit);
    while(prevOrbits != numVarOrbit)
      {
        prevOrbits = numVarOrbit;
        getColours();
        mergeColours();
        PrintcnfFormulaDescriptionGraph();
        getOrbits();
        printf("Num Orbits = %d\r\n", numVarOrbit);
      }
    getColoursAssOrbit();
  }

void cnfVarSymmetry::getColoursAssOrbit()
  {
    nColours = 0;
    double smtColouringTime;
    std::set<int> smtColouringVertices;
    std::vector<std::pair<int, int> > smtColouringConstraints;
    for(unsigned int i = 0 ; i < numVarOrbit ; i++)
      {
        smtColouringVertices.clear();
#ifdef debug
        printf("Colouring orbit %d of size %d\r\n", i, varOrbitSizes[i]);
#endif
        smtColouringConstraints = getOrbitColouringGraphVarSymm(i);
        for(unsigned int j = 0 ; j < varOrbitSizes[i] ; j++)
          {
            smtColouringVertices.insert(varOrbits[i][j]);
          }
        std::vector< std::set<int> > smtColouring;
        int minColours = 1;//This has a lower bound more than 1
        for(unsigned int j = minColours ; j < varOrbitSizes[i]; j ++)
          {
#ifdef debug
            printf("smtColouringVertices.size()=%d\r\n", (int)smtColouringVertices.size());
            printf("Trying to colour with %d colours\r\n", j);
#endif
            smtColouring =  smtColour(smtColouringVertices, smtColouringConstraints, j, smtColouringTime);
            if(smtColouring.size() != 0)
              {
#ifdef debug
                printf("Successfully coloured with %d colours\r\n", (int)smtColouring.size());
#endif
                for(unsigned int k = nColours; k < nColours + smtColouring.size() ; k++)
                  {
#ifdef debug
                    printf("Colour %d = ", k);
#endif
                    colourSize[k] = smtColouring[k - nColours].size();
                    std::set<int>::iterator colour_iter = smtColouring[k - nColours].begin();
                    for(unsigned int l = 0 ; l < smtColouring[k - nColours].size() ; l++)
                      {
                        colours[k][l] = *colour_iter;
                        colour_iter++;
#ifdef debug
                        printf("%d ", colours[k][l]);
#endif
		      }
#ifdef debug
                    printf("\r\n");
#endif
                  }
                nColours += smtColouring.size();
                break;
              }
          }        
      }
    numVarOrbit = nColours;
    //#ifdef debug
    printf("Splitted orbits are:\r\n");
    //#endif
    for(unsigned int i = 0 ; i < numVarOrbit ; i++)
      {
	//#ifdef debug
        printf("Orbit %d(size %d): ", i, colourSize[i]);
	//#endif
        varOrbitSizes[i] = colourSize[i];
        for(unsigned int j = 0 ; j < varOrbitSizes[i] ; j++)
          {
            varOrbits[i][j] = colours[i][j];
	    //#ifdef debug
            printf("%d ", varOrbits[i][j]);
	    //#endif
          }
	//#ifdef debug
        printf("\r\n");
	//#endif
      }    
  }

void cnfVarSymmetry::getOrbits()
  {
    system("$NAUTY_DIR/dreadnaut < nauty_in > nauty_out");
    FILE* naut = fopen("nauty_out", "r");
    char token[100];
    fscanf(naut, "%s", token);
    char time_token[100];
    token[0] = '\0';
    while(strcmp(token,"seconds")!=0)
      {
        strcpy(time_token, token);
        fscanf(naut, "%s", token);
      }
    symmDetTime = strtod(time_token, NULL);
    numVarOrbit = -1;
    numClauseOrbit = -1;
    std::set<int> current_orbit;
    char orbitType = 0; //0 init 1 lit orbit 2 clause orbit
    while(fscanf(naut, "%s", token)!=EOF)
      {
#ifdef debug
        printf("Number of clause orbits = %d\r\n", numClauseOrbit + 1); 
#endif
        if(token[0]!='(')//size of an orbit token
          {
            if(strstr(token,":")!=NULL)
              {
                char*subtoken;
                subtoken = strtok(token,":");
                unsigned int low_range = strtol(subtoken, NULL,10);
                subtoken = strtok(NULL,":");
                unsigned int high_range = strtol(subtoken, NULL,10);
#ifdef debug
                printf("Orbit %d:%d\r\n", low_range, high_range);
#endif
                if(orbitType == 0 && low_range <= phi.clausesSize - 1 && high_range <= phi.clausesSize - 1)//clause orbit
                  {
                    orbitType = 2;
                    numClauseOrbit++;
                    clauseOrbitSizes[numClauseOrbit] = 0;
                    for(unsigned int i = low_range ; i <= high_range; i++)
                      {
                        clauseOrbits[numClauseOrbit][clauseOrbitSizes[numClauseOrbit]] = i;
                        clauseOrbitSizes[numClauseOrbit]++;
                        clauseToOrbitMap[i] = numClauseOrbit;
                      }
                  }
                else if(orbitType == 2 && low_range <= phi.clausesSize - 1 && high_range <= phi.clausesSize - 1)
                  {
                    for(unsigned int i = low_range ; i <= high_range; i++)
                      {
                        clauseOrbits[numClauseOrbit][clauseOrbitSizes[numClauseOrbit]] = i;
                        clauseOrbitSizes[numClauseOrbit]++;
                        clauseToOrbitMap[i] = numClauseOrbit;
                      }
                  }
                else if(orbitType == 0 && low_range > phi.clausesSize - 1 && high_range > phi.clausesSize - 1
                        && (low_range - (phi.clausesSize))%2 == 0 && (high_range - (phi.clausesSize))%2 == 0)
                //var orbit, checking only even orbits
                  {
                    orbitType = 1;
                    numVarOrbit++;
                    varOrbitSizes[numVarOrbit] = 0;
                    for(unsigned int i = low_range ; i <= high_range; i++)
                      {
                        varOrbits[numVarOrbit][varOrbitSizes[numVarOrbit]] = (i - phi.clausesSize)/2 + 1;
                        varOrbitSizes[numVarOrbit]++;
                        varToOrbitMap[(i - phi.clausesSize)/2 + 1] = numVarOrbit;
                      }
                  }
                else if(orbitType == 1 && low_range > phi.clausesSize - 1 && high_range > phi.clausesSize - 1
                        && (low_range - (phi.clausesSize))%2 == 0)
                  {
                    for(unsigned int i = low_range ; i <= high_range; i++)
                      {
                        varOrbits[numVarOrbit][varOrbitSizes[numVarOrbit]] = (i - phi.clausesSize)/2 + 1;
                        varOrbitSizes[numVarOrbit]++;
                        varToOrbitMap[(i - phi.clausesSize)/2 + 1] = numVarOrbit;
                      }
                  }
                // else
                //   {
                //     printf("Orbit problem, exiting!!\r\n");
                //     exit(-1);
                //   }               
                //Add all the range to the current orbit
              }
            else//We are only taking even orbits because odd ones are isomorphic to them
              {
                unsigned int vtx_id = strtol(token, NULL,10);
                //printf("vtx_id %d\r\n", vtx_id);
                //Add this vtx to the current orbit
                if(orbitType == 0  && vtx_id <= phi.clausesSize - 1)//clause orbit
                  {
                    orbitType = 2;
                    numClauseOrbit++;
                    clauseOrbitSizes[numClauseOrbit] = 0;
                    clauseOrbits[numClauseOrbit][clauseOrbitSizes[numClauseOrbit]] = vtx_id;
                    clauseOrbitSizes[numClauseOrbit]++;
                    clauseToOrbitMap[vtx_id] = numClauseOrbit;
                  }
                else if(orbitType == 2  && vtx_id <= phi.clausesSize - 1)//clause orbit
                  {
                    clauseOrbits[numClauseOrbit][clauseOrbitSizes[numClauseOrbit]] = vtx_id;
                    clauseOrbitSizes[numClauseOrbit]++;
                    clauseToOrbitMap[vtx_id] = numClauseOrbit;
                  }
                else if(orbitType == 0  && vtx_id > phi.clausesSize - 1
                        && (vtx_id - (phi.clausesSize))%2 == 0)//var orbit
                  {
                    orbitType = 1;
                    numVarOrbit++;
                    varOrbitSizes[numVarOrbit] = 0;
                    varOrbits[numVarOrbit][varOrbitSizes[numVarOrbit]] = (vtx_id - phi.clausesSize + 2)/2;
                    varOrbitSizes[numVarOrbit]++;
                    varToOrbitMap[(vtx_id - phi.clausesSize)/2] = numVarOrbit;
                  }
                else if(orbitType == 1  && vtx_id > phi.clausesSize - 1
                        && (vtx_id - (phi.clausesSize))%2 == 0)//var orbit
                  {
                    varOrbits[numVarOrbit][varOrbitSizes[numVarOrbit]] = (vtx_id - phi.clausesSize + 2)/2;
                    varOrbitSizes[numVarOrbit]++;
                    varToOrbitMap[(vtx_id - phi.clausesSize)/2] = numVarOrbit;
                  }
                // else
                //   {
                //     printf("Orbit problem, exiting!!\r\n");
                //     exit(-1);
                //   }
                //printf("%d ", vtx_id);
              }
          }
        if(strstr(token,";") !=NULL)//end of current orbit
          {
            orbitType = 0;
          }
      }
    numClauseOrbit++;
    printf("Number of clause orbits = %d\r\n", numClauseOrbit);
    numVarOrbit++;
    printf("Number of variable orbits = %d\r\n", numVarOrbit);
    fclose(naut);
  }

void cnfVarSymmetry::constructUniformModel()
{
#define quotModel quotPhi->model
#define currentPosVarOrbitIndex quotModel._posLits[i] - 1
  phi.model.posLitsCount = 0;
  phi.model.negLitsCount = 0;
  printf("Computed model is ");
  for(unsigned int i = 0 ; i < quotModel.posLitsCount ; i++)
    {
      for(unsigned int j = 0 ; j < varOrbitSizes[quotientVarNameNormalsiation[currentPosVarOrbitIndex + 1] - 1] ; j++)
        {
          phi.model._posLits[phi.model.posLitsCount] = varOrbits[quotientVarNameNormalsiation[currentPosVarOrbitIndex + 1] - 1][j];
          printf("%d ", phi.model._posLits[phi.model.posLitsCount]);
          phi.model.posLitsCount++;
        }
    }
#define currentNegLitOrbitIndex quotModel._negLits[i] - 1
  for(unsigned int i = 0 ; i < quotModel.negLitsCount ; i++)
    {
      for(unsigned int j = 0 ; j < varOrbitSizes[quotientVarNameNormalsiation[currentNegLitOrbitIndex + 1] - 1] ; j++)
        {
          phi.model._negLits[phi.model.negLitsCount] = varOrbits[quotientVarNameNormalsiation[currentNegLitOrbitIndex + 1] - 1][j];
          printf("-%d ", phi.model._negLits[phi.model.negLitsCount]);
          phi.model.negLitsCount++;
        }
    }
  printf("\r\n");
  phi.SAT = 1; //Assuming that this is a model for phi, whihc is to be verified
}


void cnfVarSymmetry::solveBySolvingQuotient()
{
  // quotPhi = phi.getQuotient(this->litOrbits, this->numLitOrbit,  this->litOrbitSizes,
  //  			    this->clauseOrbits, this->numClauseOrbit,  this->clauseOrbitSizes, quotientVarNameNormalsiation);
  quotPhi = phi.getQuotientArbitraryVarSymm(this->varOrbits, this->numVarOrbit,  this->varOrbitSizes, quotientVarNameNormalsiation);
  if(quotPhi == NULL)
    {
      printf("Quotient formuala undefined!!\r\n");
      return;
    }
  quotPhi->writeFormulaDIMACS();
  quotPhi->searchModel();
  if(quotPhi->SAT == 1)
    {  
      printf("Validating quotient model...\r\n");
      quotPhi->validateModel();
      constructUniformModel();
      printf("Validating uniform model...\r\n");
      phi.validateModel();
    }
}
