#ifndef CNF_PROB
#define CNF_PROB
#include <set>
#include "../utils/util.h"
#include "../utils/proposition.h"

#define dynamic

#define MAX_CLAUSE_SIZE 100
#define MAX_CLAUSE_NUMBER 10000
#define MAX_DOMAIN_SIZE 10000


class GroundClauseType{
 public:
  Proposition _posLits[MAX_CLAUSE_SIZE];
  unsigned int posLitsCount;
  Proposition _negLits[MAX_CLAUSE_SIZE];
  unsigned int negLitsCount;
//bool operator==(const GroundClauseType& rhs) const {return ;}
  GroundClauseType(){}
};

class cnfFormula{
 public:
  char* name;
  unsigned int domSize;
  unsigned int domain[MAX_DOMAIN_SIZE];
  unsigned int clausesSize;
  GroundClauseType clauses[MAX_CLAUSE_NUMBER]; //An array of clauuses
  bool SAT;
  GroundClauseType model;
  // Solution stats
  unsigned int genStates;
  unsigned int expanded;
  double searchTime;
  double translationTime;

  unsigned int negLit_to_clause[MAX_DOMAIN_SIZE][MAX_CLAUSE_NUMBER];
  unsigned int clausesStatingNegLit[MAX_DOMAIN_SIZE];//Number of clauses stating the neg lit
  unsigned int posLit_to_clause[MAX_DOMAIN_SIZE][MAX_CLAUSE_NUMBER];
  unsigned int clausesStatingPosLit[MAX_DOMAIN_SIZE];//Number of clauses stating the pos lit

  cnfFormula(){;}
  void processComment(FILE* cnfFile);
  void processClause(FILE* cnfFile, unsigned int clause);
  cnfFormula(FILE* cnfFile);
  cnfFormula(const cnfFormula& phiIn){}
  int getClauseIndex(GroundClauseType clause);
  void writeFormulaDIMACS();
  void searchModel();
  void validateModel();
  cnfFormula* getQuotient(unsigned int litPartition[][MAX_DOMAIN_SIZE], unsigned int &litPartitionSize, unsigned int litEquivalenceClassCounts[],
                          unsigned int clausePartition[][MAX_CLAUSE_NUMBER], unsigned int &clausePartitionSize, unsigned int clauseEquivalenceClassCounts[],
                          unsigned int *quotientVarNameNormalsiation);
  cnfFormula* getQuotientArbitrary(unsigned int litPartition[][MAX_DOMAIN_SIZE], unsigned int &litPartitionSize, unsigned int litEquivalenceClassCounts[],
				   unsigned int *quotientVarNameNormalsiation);

  cnfFormula* getQuotientArbitraryVarSymm(unsigned int litPartition[][MAX_DOMAIN_SIZE/10], unsigned int &litPartitionSize, unsigned int litEquivalenceClassCounts[],
				   unsigned int *quotientVarNameNormalsiation);

  // Partition of the literals of the this formula. It has to satisfy if x and not y \in p, then \exists p'. not x and y \in p'.

};

std::set<int> domClause(GroundClauseType clause);
std::set<int> getClausesDomain(std::vector<GroundClauseType> clauses);
GroundClauseType imageModel(GroundClauseType clause,
                           std::map<int, int> instantiation);
#endif
