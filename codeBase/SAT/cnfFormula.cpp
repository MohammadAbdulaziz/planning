#include "cnfFormula.h"
#include <stdlib.h>
#include <string.h>

//#define debug

void cnfFormula::processComment(FILE* cnfFile)
{
  char initToken;
  initToken = fgetc(cnfFile);
#ifdef debug
  printf("Processing comment\nCurrent comment:");
#endif
  while(initToken != '\n')
    {
#ifdef debug
      printf("%c", initToken);
#endif
      initToken = fgetc(cnfFile);
    }  
}

void cnfFormula::processClause(FILE* cnfFile, unsigned int clause)
{
#ifdef debug
  printf("Processing clause %d..\r\n", clause);
#endif
  char token [100];
  clauses[clause].posLitsCount = 0;
  clauses[clause].negLitsCount = 0;
  unsigned int currentPosLitsCount = 0;
  unsigned int currentNegLitsCount = 0;
  while(fscanf(cnfFile, "%s", token) != EOF)
    {
#ifdef debug
      printf("%s ", token);
#endif
      if(token[0] == '0') //end of clause
        break;
      int litBuffer = 0;
      litBuffer = strtol(token, NULL, 10);
      if(litBuffer > 0)
        {  
          clauses[clause]._posLits[currentPosLitsCount] = abs(litBuffer);
          posLit_to_clause[abs(litBuffer)][clausesStatingPosLit[abs(litBuffer)]] = clause;
          clausesStatingPosLit[abs(litBuffer)]++;
          currentPosLitsCount++;
        }
      else if(litBuffer < 0)
        {
          clauses[clause]._negLits[currentNegLitsCount] = abs(litBuffer);
          negLit_to_clause[abs(litBuffer)][clausesStatingNegLit[abs(litBuffer)]] = clause;
          clausesStatingNegLit[abs(litBuffer)]++;
          currentNegLitsCount++;
        }
    }
#ifdef debug
      printf("\r\n");
#endif
  clauses[clause].posLitsCount = currentPosLitsCount;
  clauses[clause].negLitsCount = currentNegLitsCount;
}


cnfFormula::cnfFormula(FILE* cnfFile)
{

  char preambleProcessed = 0;
  char token[100];
  unsigned int processedClausesSize = 0;
  while(fscanf(cnfFile, "%s", token) != EOF)
    {
      if(token[0] == 'c')
        processComment(cnfFile);
      else if(token[0] == 'p')
        {
#ifdef debug
          printf("Processing preamble..\r\n");
#endif
          if(preambleProcessed) //No two preambles allowed
            {
              printf("Two preambles found, exiting!!! FILE:%s LINE:%d", __FILE__, __LINE__);
              exit(1);
            }
          preambleProcessed = 1;
          fscanf(cnfFile, "%s", token);
          if(strcmp(token,"cnf") != 0)// Wrong preamble
            exit(1);
          fscanf(cnfFile, "%s", token);
          domSize = strtol(token, NULL, 10);
          for(unsigned int i = 0 ;  i < domSize ; i++)
            {
              clausesStatingNegLit[i] = 0;
              clausesStatingPosLit[i] = 0;
            }
          fscanf(cnfFile, "%s", token);
          clausesSize = strtol(token, NULL, 10);
        }
      else
        {
          if(!preambleProcessed) //No Clauses before preamble
            {
              printf("Clauses before preamble, exiting!!! FILE:%s LINE:%d\r\n", __FILE__, __LINE__);
              exit(1);
            }
#ifdef debug
          printf("Will start to process clause %d...\r\n", processedClausesSize);
#endif
	  fseek(cnfFile, -(strlen(token)), SEEK_CUR);
          processClause(cnfFile, processedClausesSize);
          processedClausesSize++;
        }
    }
  if(processedClausesSize != clausesSize)
    {
      printf("Number of clauses in the file is not like the number in the preaamble, exiting!!!, FILE:%s LINE:%d", __FILE__, __LINE__);
      exit(1);
    }
}

void cnfFormula::writeFormulaDIMACS()
{
  FILE* phiFile = fopen ("phiFile.cnf", "w");
  fprintf(phiFile, "p cnf %d %d\r\n", domSize, clausesSize);
  unsigned int i = 0 ; 
  for(; i < clausesSize ; i++)
    {
      unsigned int j = 0;
      for( ; j < clauses[i].negLitsCount ; j++)
        fprintf(phiFile, "-%d ", clauses[i]._negLits[j]);
      j = 0;
      for( ; j < clauses[i].posLitsCount ; j++)
        fprintf(phiFile, "%d ", clauses[i]._posLits[j]);
      fprintf(phiFile, "0\r\n");
    }
  fclose(phiFile);
}

void cnfFormula::searchModel()
{
  system("$MINISAT_PATH/minisat/build/release/bin/minisat phiFile.cnf phiModel");
  FILE*phiModel = fopen("phiModel", "r");
  char token[100];
  fscanf(phiModel, "%s", token);
  if(strcmp(token, "UNSAT") == 0)
    {
      SAT = 0;
      return;
    }
  else
    {
      SAT = 1;
    }
  unsigned int parsedNegLits = 0;
  unsigned int parsedPosLits = 0;
  unsigned int parsedLits = 0;
  while(fscanf(phiModel, "%s", token) != EOF)
    {
#ifdef debug
      //printf("%s\n", token);
#endif
      if(strtol(token, NULL, 10) < 0)
        {
          model._negLits[parsedNegLits] = ++parsedLits;
          parsedNegLits++;
        }
      else if(strtol(token, NULL, 10) > 0)
        {
          model._posLits[parsedPosLits] = ++parsedLits;
          parsedPosLits++;
        }
      else
        {
          break;
        }
    }
  model.negLitsCount = parsedNegLits;
  model.posLitsCount = parsedPosLits;
#ifdef debug
  printf("Size of negative literals in the model %d\n", model.negLitsCount);
  printf("Size of positive literals in the model %d\n", model.posLitsCount);
#endif
  fclose(phiModel);
  if(parsedLits != domSize)
    {
      printf("Error parsing model, parsed lits = %d, domain size = %d, exiting!!!! FILE:%s LINE:%d\n", parsedLits, domSize, __FILE__, __LINE__);
      exit(1);
    }
}

void cnfFormula::validateModel()
{
  if(SAT == 0)
    {
      printf("The formula is UNSAT!!\r\n");
      return;
    }
  unsigned int i = 0;
  for (i = 0; i < clausesSize ; i++)
    {
      char clauseSAT = 0;
#ifdef debug
      printf("Checking if the model satisfies the clause %d which has\n", i);
      printf("%d neg lits\n", clauses[i].negLitsCount);
      printf("%d pos lits\n", clauses[i].posLitsCount);
      printf("The clause in details: ");
      for(unsigned int j = 0 ; j < clauses[i].negLitsCount ; j++)
        {
          printf("-%d ", clauses[i]._negLits[j]);
        }
      for(unsigned int j = 0 ; j < clauses[i].posLitsCount ; j++)
        {
          printf("%d ", clauses[i]._posLits[j]);
        }
      printf("\r\n");
#endif
      unsigned int j = 0;
      for(j = 0 ; j < clauses[i].negLitsCount ; j++)
        {
          unsigned int k = 0;
          for(k = 0 ; k < model.negLitsCount ; k++)
            {
              if(clauses[i]._negLits[j] == model._negLits[k])
                {
#ifdef debug
                  printf("The model satisfies clause %d with the negative literal %d \n", i, model._negLits[k]);
#endif
                  clauseSAT = 1;
                  break;
                }
            }
          if(clauseSAT)
            break;
        }
      if(clauseSAT)
        continue;
      unsigned m = 0;
      for( m = 0; m < clauses[i].posLitsCount ; m++)
        {
          unsigned int k = 0;
          for(k = 0 ; k < model.posLitsCount ; k++)
            {
              if(clauses[i]._posLits[m] == model._posLits[k])
                {
#ifdef debug
                  printf("The model satisfies clause %d with the positive literal %d \n", i, model._posLits[k]);
#endif
                  clauseSAT = 1;
                  break;
                }
            }
          if(clauseSAT)
            break;
        }
      if(!clauseSAT)
        {
          printf("Invalid model, exiting!!!!!!\r\n");
          exit(1);
        }
#ifdef debug
      printf("The model satisfies clause %d\n", i);
#endif
    }
}


cnfFormula* cnfFormula::getQuotient(unsigned int litPartition[][MAX_DOMAIN_SIZE], unsigned int &litPartitionSize, unsigned int litEquivalenceClassCounts[],
                                    unsigned int clausePartition[][MAX_CLAUSE_NUMBER], unsigned int &clausePartitionSize, unsigned int clauseEquivalenceClassCounts[],
                                    unsigned int *quotientVarNameNormalsiation)
{
  cnfFormula* phiQuotientPtr = new cnfFormula;
#define  phiQuotient (*phiQuotientPtr)
  // The goal is to derive a function O from the set of literals L defined on \dom(phi) where:
  // - For every variable v in \dom(phi), either O(v) |-> r and O(not v) |-> \not r  or O(v) |-> \not r and O(not v) |-> r.
  char checkedVars[domSize];
  int varToLiteral[domSize];
  unsigned int i = 0 ;
  printf("domSize=%d\r\n",domSize);
  for(; i < domSize; i++)
    {
      checkedVars[i] = 0;
      varToLiteral[i] = 0;
    }
  unsigned int currentProcessedPartition = 0;
  printf("litPartitionSize=%d\r\n", litPartitionSize);
  for(i = 0; i < litPartitionSize ; i++)
    {
#ifdef debug
      printf("i=%d\r\n",i);
      printf("litEquivalenceClassCounts[i]=%d\r\n",litEquivalenceClassCounts[i]);
#endif
      char partitionProcessed = 0;
      for(unsigned int j = 0; j < litEquivalenceClassCounts[i] ; j++)
        {
#ifdef debug
          printf("j=%d\r\n",j);
          printf("litPartition[i][j]=%d\r\n",litPartition[i][j]);
          printf("litPartition[i][j]/2=%d\r\n",litPartition[i][j]/2);          
          printf("checkedVars[partition[i][j]/2]=%d\r\n", checkedVars[litPartition[i][j]/2 - 1]);
#endif
          if(checkedVars[litPartition[i][j]/2 - 1] == 0)
            {
#ifdef debug
              printf("Literal of unchecked var..\r\n");          
#endif
              partitionProcessed = 1;
              if(litPartition[i][j]%2 == 0)
                {  
                  varToLiteral[litPartition[i][j]/2 - 1] = currentProcessedPartition + 1;
                }
              else 
                {
                  varToLiteral[litPartition[i][j]/2 - 1] = - (currentProcessedPartition + 1);
                }
#ifdef debug
              printf("Var %d mapped to literal %d\r\n", litPartition[i][j]/2, varToLiteral[litPartition[i][j]/2 - 1]);
#endif
              checkedVars[litPartition[i][j]/2 - 1] = 1;
              partitionProcessed = 1;
            }
        }
      if(partitionProcessed)
        {
          currentProcessedPartition++;
          quotientVarNameNormalsiation[currentProcessedPartition] = i + 1;
        }
    }
    if(currentProcessedPartition != litPartitionSize/2)
    {
      printf("Problem comptuing Q*, processed %d lit partitions, exiting!!!\r\nHint: may be variables and their negations should not be in the same orbit\r\n", currentProcessedPartition);
      return NULL;
    }
#ifdef debug
  printf("Computed Q* successfully, computing phi/P*\r\n");
#endif
  phiQuotient.domSize = currentProcessedPartition;
  phiQuotient.clausesSize = clausePartitionSize;
#define currentClause this-> clauses[clausePartition[i][0]]
#define quotientClause phiQuotient.clauses[i]
#define currentQuotClausePosLitCount phiQuotient.clauses[i].posLitsCount
#define currentQuotClauseNegLitCount phiQuotient.clauses[i].negLitsCount
  for(i = 0 ; i < clausePartitionSize; i++)
    {
#ifdef debug
      printf("Current quotient clause is %d\r\n", i);
      printf("It has %d pos lits\r\n", currentClause.posLitsCount);
      printf("It has %d neg lits\r\n", currentClause.negLitsCount);
#endif
      currentQuotClausePosLitCount = 0;
      currentQuotClauseNegLitCount = 0;
      for(unsigned int j = 0 ; j < currentClause.posLitsCount; j++)
        {
          if(varToLiteral[currentClause._posLits[j] - 1] > 0)
            {
              phiQuotient.clauses[i]._posLits[currentQuotClausePosLitCount] = varToLiteral[currentClause._posLits[j] - 1];
              currentQuotClausePosLitCount++;
            }
          else if(varToLiteral[currentClause._posLits[j] - 1] < 0)
            {
              phiQuotient.clauses[i]._negLits[currentQuotClauseNegLitCount] = abs(varToLiteral[currentClause._posLits[j] - 1]);
              currentQuotClausePosLitCount++;
            }
          else
            {
              printf("Problem creating quotient, exiting!!!\r\n" );
              exit(1);
            }
        }
      for(unsigned int j = 0 ; j < currentClause.negLitsCount; j++)
        {
          if(varToLiteral[currentClause._negLits[j] - 1] < 0)
            {
#ifdef debug
              printf("varToLiteral[currentClause._negLits[j]]=%d\r\n", varToLiteral[currentClause._negLits[j] - 1]);
#endif
              phiQuotient.clauses[i]._posLits[currentQuotClausePosLitCount] = varToLiteral[currentClause._negLits[j] - 1];
              currentQuotClausePosLitCount++;
            }
          else if(varToLiteral[currentClause._negLits[j] - 1] > 0)
            {
              phiQuotient.clauses[i]._negLits[currentQuotClauseNegLitCount] = abs(varToLiteral[currentClause._negLits[j] - 1]);
#ifdef debug
              printf("phiQuotient.clauses[i]._negLits[currentQuotClauseNegLitCount]=%d\r\n", phiQuotient.clauses[i]._negLits[currentQuotClauseNegLitCount]);
#endif
              currentQuotClauseNegLitCount++;
            }
          else
            {
              printf("Problem creating quotient, exiting!!!\r\n" );
              exit(1);
            }
        }
    }
  return phiQuotientPtr;
}


cnfFormula* cnfFormula::getQuotientArbitrary(unsigned int litPartition[][MAX_DOMAIN_SIZE], unsigned int &litPartitionSize, unsigned int litEquivalenceClassCounts[],
                                             unsigned int *quotientVarNameNormalsiation)
{
  cnfFormula* phiQuotientPtr = new cnfFormula;
#define  phiQuotient (*phiQuotientPtr)
  // The goal is to derive a function O from the set of literals L defined on \dom(phi) where:
  // - For every variable v in \dom(phi), either O(v) |-> r and O(not v) |-> \not r  or O(v) |-> \not r and O(not v) |-> r.
  char checkedVars[domSize];
  int varToLiteral[domSize];
  unsigned int i = 0 ;
  printf("domSize=%d\r\n",domSize);
  for(; i < domSize; i++)
    {
      checkedVars[i] = 0;
      varToLiteral[i] = 0;
    }
  unsigned int currentProcessedPartition = 0;
  printf("litPartitionSize=%d\r\n", litPartitionSize);
  for(i = 0; i < litPartitionSize ; i++)
    {
#ifdef debug
      printf("i=%d\r\n",i);
      printf("litEquivalenceClassCounts[i]=%d\r\n",litEquivalenceClassCounts[i]);
#endif
      char partitionProcessed;
      partitionProcessed = 0;
      for(unsigned int j = 0; j < litEquivalenceClassCounts[i] ; j++)
        {
#ifdef debug
          printf("j=%d\r\n",j);
          printf("litPartition[i][j]=%d\r\n",litPartition[i][j]);
          printf("litPartition[i][j]/2=%d\r\n",litPartition[i][j]/2);          
          printf("checkedVars[partition[i][j]/2]=%d\r\n", checkedVars[litPartition[i][j]/2 - 1]);
#endif
          if(checkedVars[litPartition[i][j]/2 - 1] == 0)
            {
#ifdef debug
              printf("Literal of unchecked var..\r\n");          
#endif
              partitionProcessed = 1;
              if(litPartition[i][j]%2 == 0)
                {  
                  varToLiteral[litPartition[i][j]/2 - 1] = currentProcessedPartition + 1;
                }
              else 
                {
                  varToLiteral[litPartition[i][j]/2 - 1] = - (currentProcessedPartition + 1);
                }
#ifdef debug
              printf("Var %d mapped to literal %d\r\n", litPartition[i][j]/2, varToLiteral[litPartition[i][j]/2 - 1]);
#endif
              checkedVars[litPartition[i][j]/2 - 1] = 1;
              partitionProcessed = 1;
            }
        }
      if(partitionProcessed)
        {
          currentProcessedPartition++;
          quotientVarNameNormalsiation[currentProcessedPartition] = i + 1;
        }
    }
    if(currentProcessedPartition != litPartitionSize/2)
    {
      printf("Problem comptuing Q*, processed %d lit partitions, exiting!!!\r\nHint: may be variables and their negations should not be in the same orbit\r\n", currentProcessedPartition);
      return NULL;
    }
#ifdef debug
  printf("Computed Q* successfully, computing phi/P*\r\n");
#endif
  phiQuotient.domSize = currentProcessedPartition;
  phiQuotient.clausesSize = clausesSize;
#define currentClause this-> clauses[i]
#define quotientClause phiQuotient.clauses[i]
#define currentQuotClausePosLitCount phiQuotient.clauses[i].posLitsCount
#define currentQuotClauseNegLitCount phiQuotient.clauses[i].negLitsCount
  for(i = 0 ; i < clausesSize; i++)
    {
#ifdef debug
      printf("Current quotient clause is %d\r\n", i);
      printf("It has %d pos lits\r\n", currentClause.posLitsCount);
      printf("It has %d neg lits\r\n", currentClause.negLitsCount);
#endif
      currentQuotClausePosLitCount = 0;
      currentQuotClauseNegLitCount = 0;
      for(unsigned int j = 0 ; j < currentClause.posLitsCount; j++)
        {
          if(varToLiteral[currentClause._posLits[j] - 1] > 0)
            {
              phiQuotient.clauses[i]._posLits[currentQuotClausePosLitCount] = varToLiteral[currentClause._posLits[j] - 1];
              currentQuotClausePosLitCount++;
            }
          else if(varToLiteral[currentClause._posLits[j] - 1] < 0)
            {
              phiQuotient.clauses[i]._negLits[currentQuotClauseNegLitCount] = abs(varToLiteral[currentClause._posLits[j] - 1]);
              currentQuotClausePosLitCount++;
            }
          else
            {
              printf("Problem creating quotient, exiting!!!\r\n" );
              exit(1);
            }
        }
      for(unsigned int j = 0 ; j < currentClause.negLitsCount; j++)
        {
          if(varToLiteral[currentClause._negLits[j] - 1] < 0)
            {
#ifdef debug
              printf("varToLiteral[currentClause._negLits[j]]=%d\r\n", varToLiteral[currentClause._negLits[j] - 1]);
#endif
              phiQuotient.clauses[i]._posLits[currentQuotClausePosLitCount] = varToLiteral[currentClause._negLits[j] - 1];
              currentQuotClausePosLitCount++;
            }
          else if(varToLiteral[currentClause._negLits[j] - 1] > 0)
            {
              phiQuotient.clauses[i]._negLits[currentQuotClauseNegLitCount] = abs(varToLiteral[currentClause._negLits[j] - 1]);
#ifdef debug
              printf("phiQuotient.clauses[i]._negLits[currentQuotClauseNegLitCount]=%d\r\n", phiQuotient.clauses[i]._negLits[currentQuotClauseNegLitCount]);
#endif
              currentQuotClauseNegLitCount++;
            }
          else
            {
              printf("Problem creating quotient, exiting!!!\r\n" );
              exit(1);
            }
        }
    }
  return phiQuotientPtr;
}



cnfFormula* cnfFormula::getQuotientArbitraryVarSymm(unsigned int varPartition[][MAX_DOMAIN_SIZE/10], unsigned int &varPartitionSize, unsigned int varEquivalenceClassCounts[],
                                                    unsigned int *quotientVarNameNormalsiation)
{
  cnfFormula* phiQuotientPtr = new cnfFormula;
#define  phiQuotient (*phiQuotientPtr)
  // The goal is to derive a function O from the set of literals L defined on \dom(phi) where:
  // - For every variable v in \dom(phi), either O(v) |-> r and O(not v) |-> \not r  or O(v) |-> \not r and O(not v) |-> r.
  char checkedVars[domSize];
  int varToVarOrbit[domSize];
  unsigned int i = 0 ;
  printf("domSize=%d\r\n",domSize);
  for(; i < domSize; i++)
    {
      checkedVars[i] = 0;
      varToVarOrbit[i] = 0;
    }
  unsigned int currentProcessedPartition = 0;
  printf("varPartitionSize=%d\r\n", varPartitionSize);
  for(i = 0; i < varPartitionSize ; i++)
    {
#ifdef debug
      printf("i=%d\r\n",i);
      printf("varEquivalenceClassCounts[i]=%d\r\n",varEquivalenceClassCounts[i]);
#endif
      char partitionProcessed;
      partitionProcessed = 0;
      for(unsigned int j = 0; j < varEquivalenceClassCounts[i] ; j++)
        {
#ifdef debug
          printf("j=%d\r\n",j);
          printf("varPartition[i][j]=%d\r\n",varPartition[i][j]);
          printf("checkedVars[partition[i][j]]=%d\r\n", checkedVars[varPartition[i][j] - 1]);
#endif
          if(checkedVars[varPartition[i][j] - 1] == 0)
            {
#ifdef debug
              printf("Unchecked var..\r\n");          
#endif
              partitionProcessed = 1;
              varToVarOrbit[varPartition[i][j] - 1] = currentProcessedPartition + 1;
#ifdef debug
              printf("Var %d mapped to variable %d\r\n", varPartition[i][j], varToVarOrbit[varPartition[i][j] - 1]);
#endif
              checkedVars[varPartition[i][j] - 1] = 1;
              partitionProcessed = 1;
            }
        }
      if(partitionProcessed)
        {
          currentProcessedPartition++;
          quotientVarNameNormalsiation[currentProcessedPartition] = i + 1;
        }
    }
    if(currentProcessedPartition != varPartitionSize)
    {
      printf("Problem comptuing Q*, currentProcessedPartition=%d varPartitionSize=%d, file: %s line:%d\r\n", currentProcessedPartition, varPartitionSize, __FILE__, __LINE__);
      return NULL;
    }
#ifdef debug
  printf("Computed Q* successfully, computing phi/P*\r\n");
#endif
  phiQuotient.domSize = currentProcessedPartition;
  phiQuotient.clausesSize = clausesSize;
#define currentClause this-> clauses[i]
#define quotientClause phiQuotient.clauses[i]
#define currentQuotClausePosLitCount phiQuotient.clauses[i].posLitsCount
#define currentQuotClauseNegLitCount phiQuotient.clauses[i].negLitsCount
  for(i = 0 ; i < clausesSize; i++)
    {
#ifdef debug
      printf("Current quotient clause is %d\r\n", i);
      printf("It has %d pos lits\r\n", currentClause.posLitsCount);
      printf("It has %d neg lits\r\n", currentClause.negLitsCount);
#endif
      currentQuotClausePosLitCount = 0;
      currentQuotClauseNegLitCount = 0;
      for(unsigned int j = 0 ; j < currentClause.posLitsCount; j++)
        {
          phiQuotient.clauses[i]._posLits[currentQuotClausePosLitCount] = varToVarOrbit[currentClause._posLits[j] - 1];
          currentQuotClausePosLitCount++;
        }
      for(unsigned int j = 0 ; j < currentClause.negLitsCount; j++)
        {
          phiQuotient.clauses[i]._negLits[currentQuotClauseNegLitCount] = varToVarOrbit[currentClause._negLits[j] - 1];
#ifdef debug
          printf("phiQuotient.clauses[i]._negLits[currentQuotClauseNegLitCount]=%d\r\n", phiQuotient.clauses[i]._negLits[currentQuotClauseNegLitCount]);
#endif
          currentQuotClauseNegLitCount++;
        }
    }
  return phiQuotientPtr;
}
