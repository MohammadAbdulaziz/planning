#include "stripsProblem.h"
#include <stdlib.h>
#include <string.h>

#include <src/preprocess/state.h>
#include <src/preprocess/mutex_group.h>
#include <src/preprocess/axiom.h>
#include <src/preprocess/operator.h>
#include <src/preprocess/variable.h>
#include <src/preprocess/successor_generator.h>
#include <src/preprocess/causal_graph.h>

class State;
class MutexGroup;
class Operator;
class Axiom;

void removeNoneOfTheAbove();

GroundActionType::GroundActionType(Operator& opn,
                                   std::map<const Variable*, int>& sas_var_to_id,
                                   std::map<std::pair<int, int>, Proposition >& variables)
{
  for(size_t i = 0 ; i < opn.get_prevail().size() ; i++)
    {
      if (variables[std::pair<int,int> (sas_var_to_id[opn.get_prevail()[i].var],
					opn.get_prevail()[i].prev)] < 0)
        {
          printf("Action has var with negative id, exiting!!!");
          exit(-1);
        }
      preconditions.Trues.push_back(variables[std::pair<int,int> (sas_var_to_id[opn.get_prevail()[i].var],
                                opn.get_prevail()[i].prev)]);
    }
  for(size_t i = 0 ; i < opn.get_pre_post().size() ; i++)
    {
      if(opn.get_pre_post()[i].is_conditional_effect)
        {
          printf("Conditional effect, exiting!!\r\n");
          exit(1);
        }
      if(opn.get_pre_post()[i].pre != -1)
        {
          if(variables[std::pair<int,int> (sas_var_to_id[opn.get_pre_post()[i].var],
                                    opn.get_pre_post()[i].pre)] < 0)
            {
              printf("Action has var with negative id, exiting!!!");
              exit(-1);
            }
          preconditions.Trues.push_back(variables[std::pair<int,int> (sas_var_to_id[opn.get_pre_post()[i].var],
                                    opn.get_pre_post()[i].pre)]);
          effects.Falses.push_back(variables[std::pair<int,int> (sas_var_to_id[opn.get_pre_post()[i].var],
                                   opn.get_pre_post()[i].pre)]);
        }
      else
        {          
          for(int j = 0 ; j < opn.get_pre_post()[i].var->get_range() ; j ++)
            {
              if(opn.get_pre_post()[i].post != j)
                {
                  effects.Falses.push_back(variables[std::pair<int,int> (sas_var_to_id[opn.get_pre_post()[i].var], j)]);
                }
            }
        }
      if(variables[std::pair<int,int> (sas_var_to_id[opn.get_pre_post()[i].var],
                               opn.get_pre_post()[i].post)] < 0)
       {
         printf("Action has var with negative id, exiting!!!");
         exit(-1);
       }
      effects.Trues.push_back(variables[std::pair<int,int> (sas_var_to_id[opn.get_pre_post()[i].var],
                               opn.get_pre_post()[i].post)]);
    }
}

//TODO: replace this with debug?
//#define verbose

StripsProblem::StripsProblem(std::vector<Variable *> vars, State init, std::vector<std::pair<Variable *, int> > goals, std::vector<Operator> operators, vector<MutexGroup> mtxs, char* name, vector<Axiom> axioms, bool SASSCCs)
{
  CausalGraph cg(vars, operators, axioms, goals);
  const std::vector<Variable *> &ordering = cg.get_variable_ordering();

  std::map<const Variable*, int> sas_var_to_id;
  for(size_t i = 0; i < ordering.size() ; i++)
    sas_var_to_id[ordering[i]] = i;
  int currentVarID = 0;
  for(size_t i = 0; i < ordering.size() ; i++)
    for(size_t j = 0; j < (size_t)(ordering[i]->get_range()) ; j++)
      {
        variables [std::pair<int, int>(i, j)] = currentVarID;
        sasVarMap[i].push_back(currentVarID);
        sasAss[currentVarID] = std::pair<int, int>(i, j);
        currentVarID++;
      }
  for(size_t i = 0; i < sasVarMap.size(); i++)
    initSASVarSizes[i] = sasVarMap[i].size();
  std::vector<int> emptyActionIndices;

  //-->>>> mutexes;

  for (size_t i = 0; i < mtxs.size(); i++)
    {
      std::vector<int> currentmtxs;
      for(size_t j = 0; j < mtxs[i].facts.size(); j++)
        {
          const Variable *var = mtxs[i].facts[j].first;
          int value = mtxs[i].facts[j].second;
          currentmtxs.push_back(variables[std::pair<int, int>(sas_var_to_id[var], value)]);
        }
      mutexes.push_back(currentmtxs);
      // printf("Current mutex group:\r\n");
      // printVector(currentmtxs);
      currentmtxs.clear();      
    }

  for(int i = 0 ; i < (int) variables.size() ; i++)
    var_to_action.push_back(emptyActionIndices);

  domSize = (int)variables.size();
#ifdef verbose
  printf("Number of vars is: %d\r\n", (int) variables.size());
#endif
  for(size_t i = 0; i < ordering.size() ; i++)
    initial_state_vars.insert(variables[std::pair<int,int> (sas_var_to_id[ordering[i]], init[ordering[i]])]);

  for(size_t i = 0; i < goals.size() ; i++)
    this->goals.insert(variables[std::pair<int,int> (sas_var_to_id[(goals[i].first)], goals[i].second)]);
#ifdef verbose
  printf("Number of goals is: %d\r\n", (int) goals.size());
#endif

  FILE* actionIDFile = fopen("ActionIDToName","w");
  for(size_t i = 0 ; i < operators.size() ; i ++)
    {
      fprintf(actionIDFile, "%d %s \r\n", (int) i, operators[i].get_name().c_str());
      GroundActionType tempAction(operators[i], sas_var_to_id, variables);
      actions.push_back(tempAction);
      std::set<int> domain = domAction(tempAction);
      std::set<int>::iterator domIter = domain.begin();
      for(int j = 0; j < (int)domain.size() ; j++)
        {
          var_to_action[*domIter].push_back(i); 
          domIter++;
        }
    }
  fclose(actionIDFile);
  this->name = (char*)malloc(strlen(name) + 1);
  strcpy(this->name, name);
  if(variables.size() != 0)
    {
      this->onlySASSCCs = SASSCCs;
      initDependencyGraph();
      initStateGraphs(ordering, init, goals, operators, mtxs, name, axioms);
    }

  if(sasVarMap.size() != stateGraphs.size())
    {
      printf("Problem in constructing transition graphs, exiting!!!\r\n");
      exit(-1);
    }
  for(size_t i = 0; i < stateGraphs.size() ; i++)
    {
      if(sasVarMap[i].size() != num_vertices(stateGraphs[i]))
        {
          printf("Problem in constructing transition graphs, exiting!!!\r\n");
          exit(-1);
        }
    }
}

int StripsProblem::getActionIndex(GroundActionType action)
{
  for(size_t j = 0; j < actions.size() ; j++)
    {
#ifdef verbose
      printf("Comparing to action %d\r\n", j);
#endif
      if(action == actions[j])
	{
#ifdef verbose
          action.print();
          actions[j].print();
#endif
	  return j;
	}
    }
  printf("Action not found!!!!\r\n");
  return -1;
}

void StripsProblem::writePorblemPDDL()
{
  char* domName = (char*)malloc(strlen(name) + strlen("dom.pddl")+ 1);
  domName = strcpy(domName, "dom");
  domName = strcat(domName, name);
  domName = strcat(domName, ".pddl");
  FILE* domain = fopen(domName, "w");
  free(domName);
  fprintf(domain, "(define (domain quotient)\r\n");
  fprintf(domain, "(:predicates ");
  for(int i = 0 ; i < (int)domSize ; i ++)
    {
      fprintf(domain, "(v%d) ", i);
    }
  fprintf(domain, ")\r\n");
  for(size_t i = 0 ; i < actions.size() ; i ++)
    {
      fprintf(domain, "(:action a%d\r\n", (int) i);
      fprintf(domain, ":precondition (and ");
      for(size_t j = 0 ; j < actions[i].preconditions.Trues.size() ; j ++)
        {
          fprintf(domain, "(v%d) ", actions[i].preconditions.Trues[j]);
        }  
      fprintf(domain, ")\r\n");
      fprintf(domain, ":effect (and ");
      for(size_t j = 0 ; j < actions[i].effects.Trues.size() ; j ++)
        {
          fprintf(domain, "(v%d) ", actions[i].effects.Trues[j]);
        }  
      for(size_t j = 0 ; j < actions[i].effects.Falses.size() ; j ++)
        {
          fprintf(domain, "(not (v%d)) ", actions[i].effects.Falses[j]);
        }  
      fprintf(domain, ")\r\n");
      fprintf(domain, ")\r\n");
    }
  fprintf(domain, "\r\n)");
  fclose(domain);
  char* factName = (char*)malloc(strlen(name) + strlen("fact.pddl")+ 1);
  factName = strcpy(factName, "fact");
  factName = strcat(factName, name);
  factName = strcat(factName, ".pddl");
  FILE* facts = fopen(factName, "w");
  free(factName);
  fprintf(facts, "(define (problem quotientProb)\r\n");
  fprintf(facts, "(:domain quotient)\r\n");
  fprintf(facts, "(:init ");
  std::set<Proposition >::iterator init_iter = initial_state_vars.begin();
  for(size_t i = 0 ; i < initial_state_vars.size() ; i ++)
    {
      fprintf(facts, "(v%d) ", *init_iter);
      init_iter++;
    }  
  fprintf(facts, ")\r\n");
  fprintf(facts, "(:goal (and ");
  std::set<Proposition >::iterator goal_iter = goals.begin();
  for(size_t i = 0 ; i < goals.size() ; i ++)
    {
      fprintf(facts, "(v%d) ", *goal_iter);
      goal_iter++;
    }
  // std::set<int >::iterator common_iter = commResources.begin();
  // for(size_t i = 0 ; i < commResources.size() ; i ++)
  //   {
  //     Proposition tempProp;
  //     tempProp = *common_iter;
  //     if(initial_state_vars.find(tempProp) != initial_state_vars.end())
  //       fprintf(facts, "(v%d) ", *common_iter);
  //     //This is removed because there are no negative literals required as preconds
  //     /* else  */
  //     /*   fprintf(facts, "(not (v%d)) ", *common_iter); */
  //     common_iter++;
  //   }
  fprintf(facts, ")))");
  fclose(facts);
}

void StripsProblem::searchPlan()
{
  plan.clear();
  char*command = (char*)malloc(strlen("$FF_DIR/ff -p ./ -o dom.pddl -f fact.pddl | grep \": A\"> FFplan") + 2 * strlen(name) + 1 ); 
  sprintf(command, "$FF_DIR/ff -p ./ -o dom%s.pddl -f fact%s.pddl | grep \": A\"> FFplan", name, name);
  if(system(command) == -1)
    {
      printf("System command failed, exiting!!\r\n");
      exit(-1);
    }
  free(command);
  FILE* planFile = fopen("FFplan", "r");
  char token[100];
  while(fscanf(planFile, "%s", token) != EOF)
    {
      if(strcmp(token, "step") == 0)
        break;
    }
  if(fscanf(planFile, "%s", token) == EOF)
    {
      FILE* successFile = fopen("successFile","a");
      fprintf(successFile, "%d\r\n", 6);
      fclose(successFile);
      printf("The problem was not solved, exiting!!\r\n");
      exit(1);
    }
  while(fscanf(planFile, "%s", token) != EOF)
    {
      int currentAction;
      if(strstr(token,"A") != NULL)
        {
          currentAction = strtol(token + 1, NULL, 10);//skipping the "A"
          printf("Current action is %d\r\n", currentAction);
          plan.push_back(currentAction);
        }
    }
  fclose(planFile);
}

void StripsProblem::searchPlan2()
{
  plan.clear();
  char*command = (char*)malloc(strlen("timeout 1800 $FD_DIR/src/fast-downward.py dom.pddl fact.pddl --heuristic 'hlm,hff=lm_ff_syn(lm_rhw(reasonable_orders=true,lm_cost_type=one,cost_type=one))' --search 'lazy_greedy([hff,hlm],preferred=[hff,hlm])'> FDplan") + 2 * strlen(name) + 1 ); 
  sprintf(command, "timeout 1800 $FD_DIR/src/fast-downward.py dom%s.pddl fact%s.pddl --heuristic 'hlm,hff=lm_ff_syn(lm_rhw(reasonable_orders=true,lm_cost_type=one,cost_type=one))' --search 'lazy_greedy([hff,hlm],preferred=[hff,hlm])'> FDplan", name, name);
  if(system(command) == -1)
    {
      printf("System command failed, exiting!!\r\n");
      exit(-1);
    }
  free(command);
  FILE* planFile = fopen("FDplan", "r");
  char token[100];
  char token_old[100];
  while(fscanf(planFile, "%s", token) != EOF)
    {
      if(strcmp(token, "Solution") == 0)
        if(fscanf(planFile, "%s", token) != EOF)
          if(strcmp(token, "found!")==0)
            break;
      if(strcmp(token, "Done!") == 0)
        if(fscanf(planFile, "%s", token) != EOF)
          translationTime = strtod(token + 1, NULL);
    }
  if(fscanf(planFile, "%s", token) == EOF)
    {
      FILE* successFile = fopen("successFile","a");
      fprintf(successFile, "%d\r\n", 6);
      fclose(successFile);
      printf("The problem was not solved\r\n");
      genStates = -1;
      expanded = -1;
      searchTime = -1;
      return;
    }
  fscanf(planFile, "%s", token_old);
  while(fscanf(planFile, "%s", token) != EOF)
    {
      int currentAction;
      if(strcmp(token,"(1)") == 0)
        {
          currentAction = strtol(token_old + 1, NULL, 10);//skipping the "a"
          //printf("Current action is %d\r\n", currentAction);
          plan.push_back(currentAction);
        }
      strcpy(token_old, token);
      if(strcmp(token,"Generated") == 0)
        {
          fscanf(planFile, "%s", token);
          genStates = strtol(token, NULL, 10);
          //printf("Generated states = %d\r\n", genStates);
        }
      if(strcmp(token,"Expanded") == 0)
        {
          fscanf(planFile, "%s", token);
          expanded = strtol(token, NULL, 10);
          //printf("Expanded states = %d\r\n", expanded);
        }
      if(strcmp(token,"Search") == 0)
        {
          fscanf(planFile, "%s", token);
          if(strcmp(token,"time:") == 0)
            {
              fscanf(planFile, "%s", token);
              sscanf(token, "%lf", &searchTime);
              //printf("Search time = %f %s\r\n", searchTime, token);
            }
        }
    }
  fclose(planFile);
}


void StripsProblem::validatePlan()
{
  std::set<Proposition > state;
  state = initial_state_vars;
#ifdef verbose
  printf("Initial state\r\n");
  std::set<Proposition >::iterator state_iter2 = state.begin();
  for(int i = 0 ; i < state.size() ; i++)  
    {

      cout << *state_iter2 << ' ';
      state_iter2++;
    }
  cout << endl;
#endif
  for(size_t i = 0 ; i < plan.size() ; i++)
    {
      if(subset<Proposition>(actions[plan[i]].preconditions.Trues, state))
        {
          //printf("Executing action %d\r\n", plan[i]);
          state = setMinus(state, actions[plan[i]].effects.Falses);
#ifdef verbose
          //printf("After del effects\r\n");
          std::set<Proposition >::iterator state_iter = state.begin();
          for(int i = 0 ; i < state.size() ; i++)  
            {
              cout << *state_iter << ' ';
              state_iter++;
            }
          cout << endl;
#endif
          state = customUnion(state, actions[plan[i]].effects.Trues);
#ifdef verbose
          //printf("After add effects\r\n");
          state_iter = state.begin();
          for(int i = 0 ; i < state.size() ; i++)  
            {
              cout << *state_iter << ' ';
              state_iter++;
            }
          cout << endl;
#endif
        }
      //else
        //printf("Unable to Execute action %d\r\n", plan[i]);
    }
  if(subset(goals, state))
    {
      printf("Plan is valid and it is of length %d\r\n", (int) plan.size());
      FILE* successFile = fopen("successFile","a");
      fprintf(successFile, "%d\r\n", 8);
      fclose(successFile);
    }
  else
    {
      printf("Plan is invalid!!!!!!\r\n");
      FILE* successFile = fopen("successFile","a");
      fprintf(successFile, "%d\r\n", 9);
      fclose(successFile);
    }
  // cout<< "Current state is ";
  // std::set<Proposition >::iterator state_iter = state.begin();
  // for(size_t i = 0 ; i < state.size() ; i++)  
  //   {
  //     cout << *state_iter << ' ';
  //     state_iter++;
  //   }
  // cout << endl;
  // cout << "Goal is ";
  // std::set<Proposition >::iterator goal_iter = goals.begin();
  // for(size_t i = 0 ; i < goals.size() ; i++)  
  //   {
  //     cout << *goal_iter << " ";
  //     goal_iter++;
  //   }
  // cout <<endl;
}


std::set<int> precondAction(GroundActionType action)
{
    std::set<int> dom;
    for(int j = 0; j < (int)action.preconditions.Trues.size() ; j++)
      {
        dom.insert(action.preconditions.Trues[j]);
      }
    return dom;
}


std::set<int> getActionsPreconditions(std::vector<GroundActionType >  actions)
{
  std::set<int> preconds;
  for(int i = 0 ; i < (int)actions.size() ; i++)
    {
      preconds = customUnion(preconds, precondAction(actions[i]));
    }
  return preconds;
}

std::set<int> domAction(GroundActionType action)
{
    std::set<int> dom;
    for(int j = 0; j < (int)action.preconditions.Trues.size() ; j++)
      {
        dom.insert(action.preconditions.Trues[j]);
      }
    for(int j = 0; j < (int)action.effects.Trues.size() ; j++)
      {
        dom.insert(action.effects.Trues[j]);
      }
    for(int j = 0; j < (int)action.effects.Falses.size() ; j++)
      {
        dom.insert(action.effects.Falses[j]);
      }
    return dom;
}

std::set<int> getActionsDomain(std::vector<GroundActionType> actions)
{
    std::set<int> actionsDom;
    for(int j = 0; j < (int)actions.size() ; j++)
      {
        actionsDom = customUnion(actionsDom, domAction(actions[j]));
      }
    return actionsDom;
}

GroundActionType GetBestAction
         (std::set<int> primaryCover,
          std::set<int> secondaryCover,
          std::vector<GroundActionType> actionSet)
  {
    int bestActionIndex = -1;
    int primaryCoverCount = -1;
    for(int i = 0 ; i < (int)actionSet.size(); i++)
      {
        int currentPrimaryCoverCount = customInter(primaryCover, domAction(actionSet[i])).size();
        /* printf("Current action has %d primary cover vars and %d secondary cover vars\r\n", */
        /*        customInter(primaryCover, domAction(actionSet[i])).size(), */
        /*        customInter(secondaryCover, domAction(actionSet[i])).size()); */
        if(currentPrimaryCoverCount > primaryCoverCount)
          {
            bestActionIndex = i;
            primaryCoverCount = currentPrimaryCoverCount;
          }
      }
    int secondaryCoverCount = -1;
    for(int i = 0 ; i < (int)actionSet.size(); i++)
      {
        /* printf("Current action has %d primary cover vars and %d secondary cover vars\r\n", */
        /*        customInter(primaryCover, domAction(actionSet[i])).size(), */
        /*        customInter(secondaryCover, domAction(actionSet[i])).size()); */
        int currentPrimaryCoverCount = customInter(primaryCover, domAction(actionSet[i])).size();
        int currentSecondaryCoverCount = customInter(secondaryCover, domAction(actionSet[i])).size();
        if(currentSecondaryCoverCount > secondaryCoverCount
           && currentPrimaryCoverCount >= primaryCoverCount)
          {
            bestActionIndex = i;
            secondaryCoverCount = currentSecondaryCoverCount;
            primaryCoverCount = currentPrimaryCoverCount;
          }
      }
    return actionSet[bestActionIndex];
  }

GroundActionType GroundActionType::image(std::map<int, int> instantiation)
{
  GroundActionType tempAction;
  for(int j = 0; j < (int)(*this).preconditions.Trues.size() ; j++)
    {
      if(instantiation.find((*this).preconditions.Trues[j]) != instantiation.end())
        tempAction.preconditions.Trues.push_back(instantiation[(*this).preconditions.Trues[j]]);
      else
        {
          FILE* successFile = fopen("successFile","a");
          fprintf(successFile, "%d\r\n", 7);
          fclose(successFile);
          printf("Corrupt instantiation, exiting!!!\r\n");
          exit(1);
        }
    }
  for(int j = 0; j < (int)(*this).effects.Trues.size() ; j++)
    {
      if(instantiation.find((*this).effects.Trues[j]) != instantiation.end())
        tempAction.effects.Trues.push_back(instantiation[(*this).effects.Trues[j]]);
      else
        {
          FILE* successFile = fopen("successFile","a");
          fprintf(successFile, "%d\r\n", 7);
          fclose(successFile);
          printf("Corrupt instantiation, exiting!!!\r\n");
          exit(1);
        }
    }
  for(int j = 0; j < (int)(*this).effects.Falses.size() ; j++)
    {
      if(instantiation.find((*this).effects.Falses[j]) != instantiation.end())
        tempAction.effects.Falses.push_back(instantiation[(*this).effects.Falses[j]]);
      else
        {
          FILE* successFile = fopen("successFile","a");
          fprintf(successFile, "%d\r\n", 7);
          fclose(successFile);
          printf("Corrupt instantiation, exiting!!!\r\n");
          exit(1);
        }
    }
  return tempAction;  
}

int GetBestActionPriorityQueue(std::vector<int> actionSet,
                  std::vector<std::set<int> > primaryScoreQueues,
                  std::vector<std::set<int> > secondaryScoreQueues)
  {
    std::set<int> primaryBestActionSet;
    int i = -1;
    for(i = (int)primaryScoreQueues.size() - 1 ; i >= 0  ; i--)
      {
        if(primaryScoreQueues[i].size() != 0)
          {
            primaryBestActionSet = customInter(primaryScoreQueues[i], actionSet);
            if(primaryBestActionSet.size() != 0)
              {
                break;
              }
          }
      }
    int j = -1;
    std::set<int> secondaryBestActionSet;
    for(j = (int)secondaryScoreQueues.size() - 1 ; j >= 0  ; j--)
      {
        if(secondaryScoreQueues[j].size() != 0)
          {
            secondaryBestActionSet = customInter(primaryBestActionSet, secondaryScoreQueues[j]);
            if(secondaryBestActionSet.size() != 0)
              {
                break;
              }
          }
      }
    return *(secondaryBestActionSet.begin());
  }


void StripsProblem::initDependencyGraph()
  {
#ifdef verbose
    printf("Initialising dep graph\r\n");
#endif
    std::vector<int> empty;
    for(size_t i = 0; i < variables.size(); i++)
      {
        varChildren.push_back(empty);
        varParents.push_back(empty);
      }
    dependencyGraph = DigraphT(variables.size());
    //printf("Initialised dependency graph with %d vertices and %d edges\r\n", (int)num_vertices(dependencyGraph), (int)num_edges(dependencyGraph));
    boost::graph_traits<DigraphT>::vertex_descriptor u, v;
    for(size_t i = 0; i < actions.size() ; i++)
      {
        GroundActionType currentAction = actions[i];
        for(size_t j = 0; j < actions[i].preconditions.Trues.size(); j++)
          {
            int parent = actions[i].preconditions.Trues[j];
            u = vertex(parent, dependencyGraph);
            for(size_t k = 0; k < actions[i].effects.Trues.size(); k++)
              {
                int child = actions[i].effects.Trues[k];
                varChildren[parent].push_back(child);
                varParents[child].push_back(parent);
                v = vertex(child, dependencyGraph);
                add_edge(u,v,dependencyGraph);
              }
            for(size_t k = 0; k < actions[i].effects.Falses.size(); k++)
              {
                int child = actions[i].effects.Falses[k];
                varChildren[parent].push_back(child);
                varParents[child].push_back(parent);
                v = vertex(child, dependencyGraph);
                add_edge(u,v,dependencyGraph);              
	      }
	  }
        for(size_t j = 0; j < actions[i].effects.Trues.size(); j++)
          {
            int true_eff_j = actions[i].effects.Trues[j];
            u = vertex(true_eff_j, dependencyGraph);
            for(size_t k = j + 1; k < actions[i].effects.Trues.size(); k++)
              {
                int true_eff_k = actions[i].effects.Trues[k];
                varChildren[true_eff_j].push_back(true_eff_k);
                varParents[true_eff_j].push_back(true_eff_k);
                varParents[true_eff_k].push_back(true_eff_j);
                varChildren[true_eff_k].push_back(true_eff_j);
                v = vertex(true_eff_k, dependencyGraph);
                add_edge(u,v,dependencyGraph);
                add_edge(v,u,dependencyGraph);
              }
            for(size_t k = 0; k < actions[i].effects.Falses.size(); k++)
              {
                int false_eff_k = actions[i].effects.Falses[k];
                varParents[true_eff_j].push_back(false_eff_k);
                varChildren[false_eff_k].push_back(true_eff_j);
                varParents[false_eff_k].push_back(true_eff_j);
                varChildren[true_eff_j].push_back(false_eff_k);
                v = vertex(false_eff_k, dependencyGraph);
                add_edge(u,v,dependencyGraph);              
                add_edge(v,u,dependencyGraph);
	      }
	  }
        for(size_t j = 0; j < actions[i].effects.Falses.size(); j++)
          {
            int false_eff_j = actions[i].effects.Falses[j];
            u = vertex(false_eff_j, dependencyGraph);
            for(size_t k = j + 1; k < actions[i].effects.Falses.size(); k++)
              {
                int false_eff_k = actions[i].effects.Falses[k];
                varParents[false_eff_j].push_back(false_eff_k);
                varChildren[false_eff_k].push_back(false_eff_j);
                varParents[false_eff_k].push_back(false_eff_j);
                varChildren[false_eff_j].push_back(false_eff_k);
                v = vertex(false_eff_k, dependencyGraph);
                add_edge(u,v,dependencyGraph);
                add_edge(v,u,dependencyGraph);
	      }
	  }
      }
    if(onlySASSCCs)
      {
        for(size_t i = 0; i < sasVarMap.size() ; i++)
          {
#ifdef verbose
            printf("Adding edges to make SAS+ var %d asses a clique\r\n", i);
#endif            
            for(size_t j = 0; j < sasVarMap[i].size() ; j++)
              {
                int sas_ass_j = sasVarMap[i][j];
                for(size_t k = j+1; k < sasVarMap[i].size() ; k++)
                  {
                    int sas_ass_k = sasVarMap[i][k];
#ifdef verbose
                    printf("Adding edges between assignments %d (STRIPS var: %d) and %d (STRIPS var: %d)\r\n", (int)j, sas_ass_j, (int)k, sas_ass_k);
#endif
                    varParents[sas_ass_j].push_back(sas_ass_k);
                    varChildren[sas_ass_k].push_back(sas_ass_j);
                    varParents[sas_ass_k].push_back(sas_ass_j);
                    varChildren[sas_ass_j].push_back(sas_ass_k);
                    u = vertex(sas_ass_j, dependencyGraph);
                    v = vertex(sas_ass_k, dependencyGraph);
                    add_edge(u,v,dependencyGraph);
                    add_edge(v,u,dependencyGraph);
                  }
              }
          }
      }
#ifdef verbose
        printf("Sanity checking the dependency graph\r\n");
#endif    
    for(size_t i = 0; i < variables.size(); i++)
      {
#ifdef verbose
        printf("Processing the children of vertex %d\r\n", (int) i);
#endif    
        for(size_t j = 0; j < varChildren[i].size(); j++)
          {
#ifdef verbose
            printf("Processing child %d of vertex %d\r\n", varChildren[i][j], (int) i);
#endif    
            if(vectorFind((int) i, varParents[varChildren[i][j]]) == -1)
              {
                printf("Corrupt dependency graph: child %d of vertex %d does not have it as a parent, exiting\r\n!!", varChildren[i][j], (int) i);
                exit(-1);
              }
          }
#ifdef verbose
        printf("Processing the parents of vertex %d\r\n", (int) i);
#endif    
        for(size_t j = 0; j < varParents[i].size(); j++)
          {
#ifdef verbose
            printf("Processing parent %d of vertex %d\r\n", varParents[i][j], (int) i);
#endif    
            if(vectorFind((int) i, varChildren[varParents[i][j]]) == -1)
              {
                printf("Corrupt dependency graph: parent %d of vertex %d does not have it as a child, exiting\r\n!!", varParents[i][j], (int) i);
                exit(-1);
              }
          }
      }
    
#ifdef verbose
    printf("Dependency graph constructed with %d vertices and %d edges\r\n", (int)num_vertices(dependencyGraph), (int)num_edges(dependencyGraph));
#endif
    //std::ofstream dotfile ("dependencyGraph.dot");
    //boost::write_graphviz (dotfile, dependencyGraph);//, llw);
    //dotfile.close();
  }

void StripsProblem::initStateGraphs(const std::vector<Variable *> &variables, State init, std::vector<std::pair<Variable *, int> > goals,  std::vector<Operator> operators, vector<MutexGroup> mutexes, char* name, vector<Axiom> axioms)
  {
    std::vector<DomainTransitionGraph> transition_graphs;
    DigraphT emptyStateGraph;
    boost::graph_traits<DigraphT>::vertex_descriptor u, v;
    build_DTGs(variables, operators, axioms, transition_graphs);
#ifdef verbose
    printf("Computed %d transition graphs\r\n", (int)transition_graphs.size());
#endif
    for(size_t i = 0; i < transition_graphs.size(); i++)
      {
        size_t curSASVarTransGrSize = transition_graphs[i].vertices.size();
        if(curSASVarTransGrSize != sasVarMap[i].size())
          {
            printf("Transition graphs ids do not match var set ids, exiting!!!\r\n");
            exit(-1);
          }
        stateGraphs.push_back(emptyStateGraph);
        stateGraphs[i] = DigraphT(curSASVarTransGrSize);
        std::vector<std::set <int> > tempAss2DArray;
        sasAssChildren.push_back(tempAss2DArray);
        for(size_t j = 0 ; j < curSASVarTransGrSize; j++)
          {
            u = vertex(j, stateGraphs[i]);
            std::set <int> tempAss1DArray;
            sasAssChildren[i].push_back(tempAss1DArray);
            for(size_t k = 0 ; k < transition_graphs[i].vertices[j].size(); k++)
              {
                const DomainTransitionGraph::Transition &trans = transition_graphs[i].vertices[j][k];
                v = vertex(trans.target, stateGraphs[i]);
                add_edge(u,v,stateGraphs[i]);
                sasAssChildren[i][j].insert(trans.target);
              }
          }
        if(curSASVarTransGrSize != (size_t)num_vertices(stateGraphs[i]))
          {
            printf("Transition graph size does not match constructed state var set ids, exiting!!!\r\n");
            exit(-1);
          }
      }
  }

bool StripsProblem::isStateGraphAcyclic(int varsetID)
  {
    std::vector<int>vertex_to_component = std::vector<int>(num_vertices(this->stateGraphs[varsetID]));
    int num = 
         strong_components(this->stateGraphs[varsetID], 
                          make_iterator_property_map(
                          vertex_to_component.begin(),
                          get(boost::vertex_index,
                              this->stateGraphs[varsetID]),
                          vertex_to_component[0]));
    return (num == (int)num_vertices(stateGraphs[varsetID]));
  }

bool StripsProblem::isStateSpaceAcyclic()
  {
    bool acyc = true;
    size_t nacyc = this->stateGraphs.size();
    for(size_t i = 0 ; i < this->stateGraphs.size(); i++)
      {
        // if((!subset(sasVarMap[i], this->dom())) && (customInter(sasVarMap[i], this->dom()).size() != 0))
        //   {
        //     nacyc--;
        //     acyc = false;
        //   }          
        if((!isStateGraphAcyclic(i)) && ((customInter(this->dom(), sasVarMap[i])).size() != 0))
          {
#ifdef verbose
            printf("SASVar %d is not acyclic.\r\n", (int)i);
#endif
            nacyc--;
            acyc = false;
          }
      }

#ifdef verbose
    printf("Acyclic SAS+ Vars are %d out of %d.\r\n", (int)nacyc, (int)stateGraphs.size());
#endif
    return acyc;
  }

GroundActionType GroundActionType::project(std::vector<Proposition> vs)
  {
    GroundActionType projAct;
    projAct.preconditions.Trues = customInter(this->preconditions.Trues, vs);
    projAct.effects.Falses = customInter(this->effects.Falses, vs);
    projAct.effects.Trues = customInter(this->effects.Trues, vs);
    return projAct;
  }

StripsProblem StripsProblem::projectFD(std::vector<Proposition> vs)
  {
    StripsProblem tempProb;
    tempProb.name = (char*)malloc(strlen("Proj") + 1);
    strcpy(tempProb.name, "Proj");
    //TODO: this is not correct in general, you can project on a super set!
    tempProb.domSize = vs.size();
    tempProb.goals = customInter(this->goals, vs);
    tempProb.initial_state_vars = customInter(this->initial_state_vars, vs);
    for(size_t i = 0; i < this->actions.size(); i++)
      tempProb.actions.push_back(this->actions[i].project(vs));
    tempProb.writePorblemPDDL();
    char*command = (char*)malloc(strlen("timeout 1800 $FD_DIR/src/translate/translate.py domProj.pddl factProj.pddl") + 1 ); 
    sprintf(command, "timeout 1800 $FD_DIR/src/translate/translate.py domProj.pddl factProj.pddl");
    if(system(command) == -1)
      {
        printf("System command failed, exiting!!\r\n");
        exit(-1);
      }
    free(command);
    ifstream output;
    output.open("output.sas");
    bool metric;
    vector<Variable> internal_variables;
    vector<Variable *> variables;
    vector<MutexGroup> mutexes;
    State initial_state;
    vector<std::pair<Variable *, int> > goals;
    vector<Operator> operators;
    vector<Axiom> axioms;
    read_preprocessed_problem_description(output, metric, internal_variables, variables, mutexes, initial_state, goals, operators, axioms);
    output.close();
    char*name = (char*)"Prob";
    StripsProblem projProb(variables, initial_state, goals, operators, mutexes, name, axioms, onlySASSCCs);
    return projProb;
  }

//Note:  the mutexes of the projection are not valid
// the transition graphs are only valid for SAS variables whose propositions are all contained in the projection
StripsProblem StripsProblem::project(std::vector<Proposition> vs)
  {
    //printf("Projection size = %d\r\n", (int)vs.size());
    StripsProblem tempProb;
    tempProb.name = (char*)malloc(strlen("Proj") + 1);
    strcpy(tempProb.name, "Proj");
    std::vector<std::pair<int, int> > sasVs = imageVector(sasAss, vs);
    tempProb.variables = keyToIndexMap(mapDomFilter(variables, vectorToSet(sasVs)));

    std::map<Proposition, Proposition> varIdAdjust = valueToIndexMap(mapDomFilter(variables, vectorToSet(sasVs)));
    for(size_t i = 0 ; i < sasVarMap.size() ; i ++)
      {
        tempProb.sasVarMap[i] = customInter(this->sasVarMap[i], vs);
        tempProb.sasVarMap[i] = imageVector(varIdAdjust, tempProb.sasVarMap[i]);
      }
    tempProb.initSASVarSizes = this->initSASVarSizes;
    tempProb.sasAss = mapInverse(tempProb.variables);
          
    // Note: Mutexes carry on after projection, as long as the size of the projected mutex is greater than 1
    for(size_t i = 0; i < this->mutexes.size(); i++)
      {
        std::vector<Proposition> proj_mutex = customInter(this->mutexes[i], vs);
        if (proj_mutex.size() > 1)
          {
#ifdef verbose
            printf("Adding this mutex to projection\n");
            printVector(proj_mutex);
#endif
            tempProb.mutexes.push_back(imageVector(varIdAdjust, proj_mutex));
          }
      }
    tempProb.domSize = tempProb.variables.size();
    tempProb.goals = imageSet(varIdAdjust, customInter(this->goals, vs));
    tempProb.initial_state_vars = imageSet(varIdAdjust, customInter(this->initial_state_vars, vs));
    for(size_t i = 0; i < this->actions.size(); i++)
      {
        //actionsProj = ;
        if((this->actions[i].project(vs)).effects.dom().size() != 0)
          tempProb.actions.push_back((this->actions[i].project(vs)).image(varIdAdjust));
      }
    for(size_t i = 0; i < tempProb.variables.size(); i++)
      tempProb.var_to_action.push_back(this->var_to_action[mapInverse(varIdAdjust)[i]]);

#ifdef verbose
    printf("Projected problem dom size = %d, actions num = %d\r\n", (int)tempProb.variables.size(), (int) tempProb.actions.size());
#endif
    tempProb.onlySASSCCs = this -> onlySASSCCs;
    tempProb.initDependencyGraph();
    tempProb.stateGraphs = this -> stateGraphs;
    tempProb.sasAssChildren = this -> sasAssChildren;
    tempProb.sasAssParents = this -> sasAssParents;
    return tempProb;
  }

std::vector<Proposition> PropState::dom()
  {
    return customUnion(Trues, Falses);
  }

PropState PropState::inter(PropState other)
  {
    PropState emptyState;
    emptyState.Falses = customInter(this->Trues, other.Falses);
    emptyState.Trues = customInter(this->Trues, other.Trues);
    return emptyState;
  }

//Note: the transition graphs and the mutexes of the returned problem are not valid
//Note: the transition graphs are valid if the option noSASSCCs is used when invoking the programme
StripsProblem StripsProblem::snapshot(PropState s)
  {
    //TODO: mutexes here could be improved. E.g. if we have a mutex {x, y} and we take a snapshot on the state {x -> true}, then {y -> false} should hold every where in the snapshot and thus we can remove all actions violating {y -> false}.

    StripsProblem tempProb(*this);
    //Remove actions that violate the input state
    tempProb.actions.clear();
    for(size_t i = 0 ; i < this -> actions.size() ; i++)
      {
        if(customInter(this->actions[i].effects.dom(), s.dom()).size() ==  this->actions[i].effects.inter(s).dom().size() &&
	   customInter(this->actions[i].preconditions.dom(), s.dom()).size() ==  this->actions[i].preconditions.inter(s).dom().size())
          {
#ifdef verbose
            cout << "Adding this action to snapshot:\n";
            this->actions[i].print();
#endif
            tempProb.actions.push_back(this->actions[i]);
          }
	else
          {
#ifdef verbose
            cout << "NOT adding this action to snapshot:\n";
            this->actions[i].print();
#endif
          }

      }
    //Project on the domain of the problem minus the domain of the input state
    StripsProblem snapProb(tempProb.project(setToVector(setMinus(coDom(variables),s.dom()))));
#ifdef verbose
    printf("Snapshot has %d out of %d actions. The snapshot was done on the state:\r\n", (int)snapProb.actions.size(), (int) this->actions.size());
    s.print();
    printf("Original domain is:\r\n");
#endif
    return snapProb;
  }

StripsProblem StripsProblem::snapshot(std::pair<int,int> sasVarAssignment)
  {
    //Construct propstate equiv of the input sasState
    PropState state(sasVarAssignment, variables, sasVarMap);
#ifdef verbose
    cout << "Computing snapshot on assignment: " << endl;
    printMaplet(sasVarAssignment);
    cout << "Equivalent state is: " << endl;
    state.print();
#endif
    //Compute the snapshot on the Propstate
    StripsProblem snapProb(this->snapshot(state));
    //Copy all the transition graphs to new problem,
    //except for the one associated with the input sas var
    //TODO: transition graphs should not be copied like that!!!
    // Only temporary
    return snapProb;
  }
