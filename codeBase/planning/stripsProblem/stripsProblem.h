#ifndef STRIPS_PROB
#define STRIPS_PROB

#include <src/preprocess/helper_functions.h>
#include <set>
#include <src/preprocess/operator.h>
#include "../../utils/util.h"
#include "../../utils/proposition.h"

#include <src/preprocess/state.h>
#include <src/preprocess/mutex_group.h>
#include <src/preprocess/axiom.h>
#include <src/preprocess/operator.h>
#include <src/preprocess/variable.h>
#include <src/preprocess/successor_generator.h>
#include <src/preprocess/domain_transition_graph.h>

#include <boost/graph/graphviz.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/config.hpp>
#include "../digraphs/digraph.h"

class State;
class MutexGroup;
class Operator;
class Axiom;

class PropState{
 public:
  std::vector<Proposition > Trues;
  std::vector<Proposition > Falses;
  bool operator==(const PropState& rhs) const {return vectorToSet(Trues) == vectorToSet(rhs.Trues) && vectorToSet(Falses) == vectorToSet(rhs.Falses);}
  std::vector<Proposition> dom();
  void print()
    {
      printf("True vars:\r\n");
      printVector(Trues);
      printf("False vars\r\n");
      printVector(Falses);
    }
  PropState(){}
  PropState(std::pair<int,int> sasVarAssignment,
            std::map<std::pair<int, int>, Proposition > variables,
            std::map<int, std::vector<Proposition> > sasVarMap)
    {
      int sasVar = sasVarAssignment.first;
      std::set<Proposition> singleton;
      singleton.insert(variables[sasVarAssignment]);
      Falses = setMinus(sasVarMap[sasVar], singleton);
      Trues.push_back(variables[sasVarAssignment]);
    }
  PropState inter(PropState other);
};

class GroundActionType{
 public:
  PropState preconditions;
  PropState effects;
  /* std::vector<Proposition > preconditions.Trues; */
  /* std::vector<Proposition > effects.Trues; */
  /* std::vector<Proposition > effects.Falses; */
  bool operator==(const GroundActionType& rhs) const {return vectorToSet(preconditions.Trues) == vectorToSet(rhs.preconditions.Trues) && vectorToSet(effects.Trues) == vectorToSet(rhs.effects.Trues) && vectorToSet(effects.Falses) == vectorToSet(rhs.effects.Falses);}
  GroundActionType(Operator&, std::map<const Variable*, int>&, std::map<std::pair<int, int>, Proposition >&);
  void print()
    {
      printf("preconditions:\r\n");
      printVector(preconditions.Trues);
      printf("Pos Effects\r\n");
      printVector(effects.Trues);
      printf("Neg Effects\r\n");
      printVector(effects.Falses);
    }
  GroundActionType(){}
  GroundActionType project(std::vector<Proposition> vs);
  GroundActionType image(std::map<int, int> instantiation);
};

class StripsProblem{
 public:
  char* name;
  std::map<std::pair<int, int>, Proposition > variables;//a map from sas asses to strips variables
  std::map<int, std::vector<Proposition> > sasVarMap;//sas var id to STRIPS propositions
  std::map<int, int> initSASVarSizes;
  std::map<Proposition, std::pair<int, int> > sasAss;//STRIPS propositions to SAS+ assignments
  std::vector<std::vector<Proposition> > mutexes;
  int domSize;
  std::set<Proposition > goals;
  std::set<Proposition > initial_state_vars;
  std::vector<GroundActionType> actions;//TODO: this should be a set of actions, now that I have defined an orderning of actions
  //Solution information
  std::vector<int> plan;
  int genStates;
  int expanded;
  double searchTime;
  double translationTime;

  std::vector<std::vector<int> > var_to_action;
  StripsProblem(){
    name = NULL;
  }
  StripsProblem(const std::vector<Variable *> variables, State init, std::vector<std::pair<Variable *, int> > goals,  std::vector<Operator> operators, vector<MutexGroup> mutexes, char* name, vector<Axiom> axioms, bool = false);
  StripsProblem(const StripsProblem& prob)
    {
      name = (char*)malloc(strlen(prob.name) + 1);
      strcpy(name, prob.name);
      variables = prob.variables;
      domSize = variables.size();
      sasVarMap = prob.sasVarMap;
      sasAss = prob.sasAss;
      mutexes = prob.mutexes;
      goals = prob.goals;
      initial_state_vars = prob.initial_state_vars;
      actions = prob.actions;
      plan = prob.plan;
      var_to_action = prob.var_to_action;
      onlySASSCCs = prob.onlySASSCCs;
      initDependencyGraph();
      initSASVarSizes = prob.initSASVarSizes;
      stateGraphs = prob.stateGraphs;
      sasAssChildren = prob.sasAssChildren;
      sasAssParents = prob.sasAssParents;
      // if graph is copied a segfault is generated when computing SCCs
      //copy_graph(prob.dependencyGraph, dependencyGraph);
    }
  ~StripsProblem()
    {
      if (name != NULL)
        free(name);
    }
  int getActionIndex(GroundActionType action);
  void writePorblemPDDL();
  void searchPlan();
  void searchPlan2();
  void validatePlan();
  StripsProblem projectFD(std::vector<Proposition> vs);
  StripsProblem project(std::vector<Proposition> vs);
  StripsProblem snapshot(PropState s);
  StripsProblem snapshot(std::pair<int,int> sasVarAssignment);
  std::set<Proposition> dom() {  return coDom(variables);  }
  //---------Dependency and state graphs
  bool onlySASSCCs;//Dependency graph SCCs can only contain whole SAS+ varaibles
  DigraphT dependencyGraph;
  //redundant for easy access of dependency graph children and parents
  std::vector<std::vector<int> > varChildren;
  std::vector<std::vector<int> > varParents;
  void initDependencyGraph();

  std::vector<DigraphT> stateGraphs;
  //redundant for easy access of transition graphs children
  std::vector<std::vector<std::set<int> > > sasAssChildren;
  std::vector<std::vector<std::set<int> > > sasAssParents;
  void initStateGraphs(const std::vector<Variable *> &variables, State init, std::vector<std::pair<Variable *, int> > goals,  std::vector<Operator> operators, vector<MutexGroup> mutexes, char* name, vector<Axiom> axioms);
  bool isStateGraphAcyclic(int varsetID);
  bool isStateSpaceAcyclic();
};

std::set<int> precondAction(GroundActionType action);
std::set<int> getActionsPreconditions(std::vector<GroundActionType >  actions);
std::set<int> domAction(GroundActionType action);
std::set<int> getActionsDomain(std::vector<GroundActionType> actions);
GroundActionType GetBestAction
         (std::set<int> primaryCover,
          std::set<int> secondaryCover,
          std::vector<GroundActionType> actionSet);
int GetBestActionPriorityQueue(std::vector<int> actionSet,
                  std::vector<std::set<int> > primaryScoreQueues,
                  std::vector<std::set<int> > secondaryScoreQueues);
GroundActionType imageAction(GroundActionType action,
                             std::map<int, int> instantiation);
#endif
