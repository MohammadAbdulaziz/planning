  (* This file contains the code generating the Isabelle theory file that has theorems and proofs for the validity of domain invariants. *)
  open PDDL
  val IsabelleStringImplode = implode;
  val IsabelleStringExplode = explode;
  val SMLCharImplode = String.implode;
  val SMLCharExplode = String.explode;

  fun stringListToStringList ss = (String.concatWith ", ") (map stringToString ss)

  fun pddlVarToString (v:PDDL_VAR) = "Var " ^ stringToString (pddl_var_name v)

  fun pddlObjConsToString (oc:PDDL_OBJ_CONS) = "Obj " ^ stringToString (pddl_obj_name oc)

  fun pddlTermToString term = 
    case term of VAR_TERM v => "term.VAR( " ^ pddlVarToString v ^ " )"
             | OBJ_CONS_TERM oc => "term.CONS( " ^ pddlObjConsToString oc ^ " )"

  fun pddlVarTermToString term = 
    case term of VAR_TERM v => pddlVarToString v
             | _ => exit_fail ("Var expected, but obejct found: pddlVarTermToString " ^ (pddlObjConsTermToString term))

  and pddlObjConsTermToString term = 
    case term of OBJ_CONS_TERM oc => pddlObjConsToString oc
             | _ => exit_fail ("Object expected, but variable found: pddlObjConsTermToString " ^ (pddlVarTermToString term))

  fun pddlTypeToString (type_ :PDDL_TYPE) = "Either([" ^ stringListToStringList (map pddl_prim_type_name type_) ^ "])";

  fun mk_str_pair x y = "(" ^ x ^ "," ^ y ^ ")"

  fun pddlTypedListXTypesConv typedList cat_fn mk_pair_fn obj_v_conv_fun type_conv_fun =
    let
      fun wrap_var_with_type t = (fn v => mk_pair_fn (obj_v_conv_fun v) (type_conv_fun t))
    in
      cat_fn (map (fn (vars, type_) => (map (wrap_var_with_type type_) vars)) typedList)
    end

  fun type_str_cat_fun (l:string list list) = (String.concatWith ", ") (map (String.concatWith ", ") l)

  fun pddlTypedListVarsTypesToString (typedList :PDDL_VAR PDDL_TYPED_LIST) =
      "[" ^ (pddlTypedListXTypesConv typedList type_str_cat_fun mk_str_pair pddlVarToString pddlTypeToString) ^ "]"

  fun pddlTypedListObjsConsTypesToString (typedList :PDDL_OBJ_CONS PDDL_TYPED_LIST) =
      "[" ^ (pddlTypedListXTypesConv typedList type_str_cat_fun mk_str_pair pddlObjConsToString pddlTypeToString) ^ "]"

  fun pddlTypedListTypesToString (typedList :'a PDDL_TYPED_LIST) =
            "[" ^ (String.concatWith ", ")
                            (map (fn (vars, type_) =>
                                     ((String.concatWith ", ")
                                           (map (fn _ =>
                                                    (pddlTypeToString type_)) vars)))
                                 typedList) ^ "]";
  fun extractFlatTypedListString (typedList :PDDL_PRIM_TYPE PDDL_TYPED_LIST) = 
            (String.concatWith ", ") (map (stringListToStringList o (map pddl_prim_type_name) o fst) typedList);


  fun extractFlatTypedList cat_fn str_fn mk_pair_fn (typedList :PDDL_PRIM_TYPE PDDL_TYPED_LIST) = let
    fun sng_typ [t] = str_fn (pddl_prim_type_name t)
      | sng_typ _ = exit_fail "Either-types not supported as supertypes"
  in
    cat_fn (map (fn (ts, supt) => map (fn t => mk_pair_fn (str_fn (pddl_prim_type_name t)) (sng_typ supt)) ts) typedList)
  end

  fun extractFlatTypedListString typedList =
                 extractFlatTypedList ((String.concatWith ", ") o (map (String.concatWith ", ")))
                                      stringToString mk_str_pair typedList

  fun pddlTypesDefToString (typesDefOPT :PDDL_TYPES_DEF) =
                   case typesDefOPT of
                        SOME typesDef =>
                             ("definition \"dom_types = [" ^ (extractFlatTypedListString typesDef) ^ "]\"\n")
                      | _ => "definition \"dom_types = []\"\n"

  fun pddlConstsDefToString (constsDefOPT :PDDL_CONSTS_DEF) =
                   case constsDefOPT of
                        SOME constsDef =>
                             ("definition \"dom_consts = [" ^ (pddlTypedListTypesToString constsDef) ^ "]\"\n")
                      | _ => "definition \"dom_consts = []\"\n"

  fun pddlPredToString (pred, args) = "PredDecl ( Pred (" ^ stringToString (pddl_pred_name pred)  ^ ")) (" ^ (pddlTypedListTypesToString args) ^ ")"

  fun pddlPredDefToString pred_defOPT =
                   case pred_defOPT of
                        SOME pred_def =>
                              "definition \"dom_predicates = [(" ^ ((String.concatWith "),\n (") (map pddlPredToString pred_def)) ^ ")]\"\n"
                        | _ => "definition \"dom_predicates=[]\"\n"

  fun pddlEqToSrtingTerm (term1, term2) = "atom.Eq (" ^ pddlTermToString term1 ^ ") ("  ^ pddlTermToString term2 ^ ")"

  fun pddlEqToStringObj (term1, term2) = "atom.Eq (" ^ pddlObjConsTermToString term1 ^ ") ("  ^ pddlObjConsTermToString term2 ^ ")"

  fun pddlAtomToStringTerm atm =
   case atm of PredAtm (Pred pred_name, terms_list) =>
      "predAtm (Pred (" ^ stringToString (IsabelleStringImplode pred_name) ^ "))([" ^ (String.concatWith "," (map pddlTermToString terms_list)) ^ "])"
              | (PDDL_Checker_Exported.Eqa eqtm) => pddlEqToSrtingTerm eqtm

  fun pddlAtomToStringObj atm =
   case atm of (PredAtm (Pred pred_name, terms_list)) =>
      "predAtm (Pred (" ^ stringToString (IsabelleStringImplode pred_name) ^ "))([" ^ (String.concatWith "," (map pddlObjConsTermToString terms_list)) ^ "])"
              | (PDDL_Checker_Exported.Eqa eqtm) => pddlEqToStringObj eqtm

  fun pddlFormulaToASTPropString atom_fn phi =
      case phi of Prop_atom(atom: PDDL_TERM PDDL_ATOM) => ("formula.Atom (" ^ atom_fn atom ^ ")")
                 | Prop_not(prop: PDDL_TERM PDDL_PROP) => ("formula.Not (" ^ (pddlFormulaToASTPropString atom_fn) prop ^ ")")
                 | Prop_and(propList: PDDL_TERM PDDL_PROP list) => ("BigAnd [" ^ (String.concatWith ", " (List.filter (fn x => size x > 0 ) (map (pddlFormulaToASTPropString atom_fn) propList))) ^ "]")
                 | Prop_or(propList: PDDL_TERM PDDL_PROP list) => ("BigOr [" ^ (String.concatWith ", " (List.filter (fn x => size x > 0 ) (map (pddlFormulaToASTPropString atom_fn) propList))) ^ "]")
                 | _ => ""

  fun pddlFormulaToASTPropStringTerm phi = pddlFormulaToASTPropString pddlAtomToStringTerm phi

  fun pddlFormulaToASTPropStringObj phi = pddlFormulaToASTPropString pddlAtomToStringObj phi



  fun pddlPreGDToString PreGD =
      case PreGD of SOME (prop: PDDL_TERM PDDL_PROP) => pddlFormulaToASTPropStringTerm prop
                 | _ => ""

  fun addEffToString atom = "(" ^ pddlAtomToStringTerm atom ^ ")"

  fun delEffToString atom = "(" ^ pddlAtomToStringTerm atom ^ ")"

  fun isProp_atom fmla = case fmla of Prop_atom(atom) => true | _ => false
  fun isNegProp_atom fmla = case fmla of Prop_not(Prop_atom(atom)) => true | _ => false

  fun nonEmptyLiteralsToString lits = (List.filter (fn x => size x > 0 ) (* The reason for this filtering is that fluent terms in
                                                                            formulae are transformed to empty strings*)
                                                   (map (pddlPropToASTEffString false) lits))

  and pddlPropToASTEffString one_eff (Prop: PDDL_TERM PDDL_PROP) =
      case Prop of Prop_atom atom =>
                      (let val eff_atom = (pddlFormulaToASTPropStringTerm (Prop_atom (atom))) in
                           if one_eff then "[" ^ eff_atom ^ "][]" else eff_atom
                       end)
                 | Prop_not(Prop_atom atom) =>
                      (let val eff_atom = (pddlFormulaToASTPropStringTerm (Prop_atom (atom))) in 
                           if one_eff then "[][" ^ eff_atom ^ "]" else eff_atom
                       end)
                 | Prop_and propList
                     => (let val adds = (List.filter isProp_atom propList);
                             val dels = (List.filter isNegProp_atom propList);
                         in
                          "[" ^ String.concatWith ", " (nonEmptyLiteralsToString adds) ^ "]" ^
                          "[" ^ String.concatWith ", " (nonEmptyLiteralsToString dels) ^ "]"
                         end)
                 | _ => ""

  fun pddlCEffectToString CEff =
      case CEff of SOME (prop: PDDL_TERM PDDL_PROP) => "Effect " ^ (pddlPropToASTEffString true prop)
                 | _ => "Effect [] []"

  fun actDefBodyPreToString pre = case pre of SOME (u, pre: PDDL_PRE_GD) => pddlPreGDToString pre
                                            | _ => ""

  fun actDefBodyEffToString eff = case eff of SOME (u, eff: C_EFFECT) => pddlCEffectToString eff
                                            | _ => ""

  fun pddlActDefBodyToString (pre, eff) = "(" ^ (actDefBodyPreToString pre) ^ ")\n(" ^ (actDefBodyEffToString eff) ^ ")"

  fun pddlIsabelleActName actName = SMLCharImplode (map (fn c => if c = #"-" then #"_" else c) (SMLCharExplode actName))

  fun args_n n sep = case n of 0 => ""
                       | 1 => "arg" ^ Int.toString n ^ (args_n (n - 1) sep)
                       | _ => "arg" ^ Int.toString n ^ (SMLCharImplode [sep]) ^ (args_n (n - 1) sep)

  fun get_action_args_num args = (foldr (op +) 0 (map (length o fst) args))

  fun prove_invariant_for_action (actName, args, invariant_name) =
"lemma " ^ pddlIsabelleActName invariant_name ^ "_invariant_" ^ pddlIsabelleActName actName ^ ":\n\
  \assumes \"list_all2 (ast_problem.is_obj_of_type PI) args (map snd (parameters  " ^ pddlIsabelleActName actName ^ "))\"\n\
\  shows \"ast_problem.invariant_act " ^ pddlIsabelleActName invariant_name ^ " (ast_domain.instantiate_action_schema  " ^ pddlIsabelleActName actName ^ " args)\"\n\
\  unfolding ast_problem.invariant_act_def ast_domain.execute_ground_action_def\n\
\proof(safe)\n\
\  fix s assume ass: \"" ^ pddlIsabelleActName invariant_name ^ " s\" \" s \\<^sup>c\\<TTurnstile>\\<^sub>= precondition (ast_domain.instantiate_action_schema  " ^ pddlIsabelleActName actName ^ " args)\"\n\
\  obtain " ^ args_n (get_action_args_num args) #" " ^ " where args_" ^ Int.toString (get_action_args_num args) ^ ": \"args = [" ^ args_n (get_action_args_num args) #"," ^ "]\"\n\
\    using assms by (auto simp: list_all2_Cons2 " ^ pddlIsabelleActName actName ^ "_def)\n\
\  show \"" ^ pddlIsabelleActName invariant_name ^ "(ast_domain.apply_effect (effect (ast_domain.instantiate_action_schema  " ^ pddlIsabelleActName actName ^ " args)) s)\"\n\
\    using ass\n\
\    unfolding " ^ pddlIsabelleActName invariant_name ^ "_def  " ^ pddlIsabelleActName actName ^ "_def\n\
\    by(auto simp add: args_" ^ Int.toString (get_action_args_num args) ^ " ast_domain.instantiate_action_schema.simps ast_domain.apply_effect.simps entailment_def ast_problem.holds_def valuation_def Let_def ast_domain.subst_term.simps)\n\
\qed\n"

  fun pddlActToString (actName, (args, defBody)) =
      "definition \"" ^ pddlIsabelleActName actName ^
      " = Action_Schema(''" ^ actName ^ "'')\n " ^ pddlTypedListVarsTypesToString args ^
      "\n" ^ pddlActDefBodyToString defBody ^ "\"\n\n"

  fun pddlActionsDefToString (actsDef : PDDL_ACTION list) =
                    (String.concatWith "\n\n") (map pddlActToString actsDef) ^
                    "\ndefinition \"dom_actions = [" ^
                       (String.concatWith ", ") (map (pddlIsabelleActName o fst) actsDef) ^
                     "]\"\n\n"

  type CONSTRAINT_BODY = PDDL_TERM PDDL_PROP

  type CONSTRAINT = string * (PDDL_VAR list * (CONSTRAINT_BODY))

  fun vars_terms_to_obj_terms var = 
    case var of VAR_TERM (PDDL_VAR v) => OBJ_CONS_TERM (PDDL_OBJ_CONS v)
             | _ => var;

  (* Note that when we convert the constraint to string we transform all the varaibles that occur in it to consts.
     This is because relation holds in Isabelle is defined only for objects. It should also logically OK because
     that way we are proving the constraints for arbitrary constants which is the same as for universally quantified
     variables.*)
  fun constraintToString ((constraintName, (vars, constraintBody)): CONSTRAINT) =
       "definition \"" ^ pddlIsabelleActName constraintName ^ " = (%s. (ast_problem.holds s (" ^
       (pddlFormulaToASTPropStringObj (pddl_prop_map vars_terms_to_obj_terms constraintBody)) ^ ")))\"\n"


  type CONSTRAINTS_DEF = CONSTRAINT list

  fun constraintsDefToString constraints = (String.concatWith "\n\n") (map constraintToString constraints)

fun prove_action_names_distinct actsDef =
"lemma distinct_act_names:\
\      shows \"distinct (map ast_action_schema.name (dom_actions))\"\n\
\  unfolding dom_types_def dom_predicates_def dom_actions_def ast_domain.wf_domain_def\n" ^
               (String.concatWith " ") (map ((fn name => pddlIsabelleActName name ^ "_def") o fst) actsDef)
            (* begin_cut_def continue_cut_def end_cut_def begin_transpose_splice_def continue_splice_def end_splice_def begin_transverse_splice_def begin_inverse_splice_def begin_inverse_splice_special_case_def continue_inverse_splice_A_def continue_inverse_splice_B_def end_inverse_splice_A_def end_inverse_splice_B_def reset_1_def*)
 ^ "\n\
\  by (auto simp add: ast_domain.wf_predicate_decl.simps ast_domain.wf_type.simps\n\
\                      ast_domain.wf_action_schema.simps ast_domain.wf_effect.simps\n\
\                     ast_domain.wf_atom.simps ast_domain.sig_def\n\
\                    ast_domain.is_of_type_def ast_domain.of_type_def)\n\
\\n\
\definition \"prob_dom = Domain dom_types dom_predicates dom_consts dom_actions\"\n\n"


fun prove_invariant_for_plans actions_def constraint =
"lemma " ^ pddlIsabelleActName (fst constraint) ^ "_for_prob_dom:\n\
\  assumes \"PI = (Problem (prob_dom) (obj) (I) (G))\"\n\
\  shows \"ast_problem.invariant_wf_plan_act_seq PI " ^ pddlIsabelleActName (fst constraint) ^ " as\"\n\
\  apply(rule ast_problem.invariant_for_plan_act_seq)\n\
\  apply(rule ast_problem.invariant_action_insts_imp_invariant_plan_actions[where ?Q = "^ pddlIsabelleActName (fst constraint) ^ "])\n\
\  using ast_problem.invariant_for_plan_act_seq assms distinct_act_names\n" ^
((String.concatWith " ") (map (fn (actionName, _) => (pddlIsabelleActName (fst constraint) ^ "_invariant_" ^ (pddlIsabelleActName actionName))) actions_def)) ^ "\n\
\    unfolding prob_dom_def dom_actions_def ast_problem.action_params_match_def\n" ^
((String.concatWith " ") (map (fn (actionName, _) => ((pddlIsabelleActName actionName) ^ "_def")) actions_def)) ^ "\n\
\  by auto\n"

  fun prove_invariants actions_def constraints_def =
             "\n\n\nnotation (output) Atom  (\"_\" 55) and predAtm  (\"_\" 55) and Pred  (\"_\" 55) and Obj  (\"_\" 55)\n\n\n" ^
             ((String.concatWith "\n\n")
                  (map (fn constraint =>
                         ((String.concatWith "\n\n")
                            (map (fn (actName, (args, body)) =>
                                   prove_invariant_for_action(actName, args, fst constraint))
                                        actions_def)) ^
                           prove_invariant_for_plans actions_def constraint) constraints_def))

  fun pddlDomToString (reqs:PDDL_REQUIRE_DEF,
                         (types_def,
                            (consts_def,
                               (pred_def,
                                   (fun_def,
                                       (actions_def,
                                          constraints_def))))))
                      = "theory prob_defs\n" ^
                        "imports Main PDDL_STRIPS_Checker invariant_verification\n begin\n" ^
                        (pddlTypesDefToString types_def) ^
                        (pddlConstsDefToString consts_def) ^
                        (pddlPredDefToString pred_def) ^
                        (pddlActionsDefToString actions_def) ^
                        prove_action_names_distinct actions_def ^
                        (constraintsDefToString constraints_def) ^
                        (prove_invariants actions_def constraints_def) ^ "\nend\n"

  fun objDefToString (objs:PDDL_OBJ_DEF) = "definition \"objs = " ^ pddlTypedListObjsConsTypesToString objs ^ "\"\n"

  fun initElToString (init_el:PDDL_INIT_EL) = pddlFormulaToASTPropStringObj (pddl_prop_map OBJ_CONS_TERM init_el)

  fun isntFluent x = (case x of Fluent => false | _ => true)

  fun isntTautology x = (case x of Not Bot => false | _ => true)

  fun pddlInitToStringWithObjEqs (init:PDDL_INIT) (objs) =
      "definition \"init_state = [" ^ String.concatWith ", " (map initElToString (List.filter isntFluent init)) ^ ", " ^
           String.concatWith ", "  (map (fn obj => "formula.Atom (" ^ (pddlEqToStringObj (obj, obj)) ^ ") ") objs) ^  "]\"\n"

  fun pddlInitToString (init:PDDL_INIT) (objs) =
      "definition \"init_state = [" ^ String.concatWith ", " (map initElToString (List.filter isntFluent init)) ^  "]\"\n"

  fun pddlGoalToString (goal:PDDL_GOAL) = "definition \"goals = " ^ pddlFormulaToASTPropStringObj goal ^ "\"\n"

  fun pddlProbToString (reqs:PDDL_REQUIRE_DEF,
                          (objs:PDDL_OBJ_DEF,
                              (init:PDDL_INIT,
                                (goal_form:PDDL_GOAL,
                                   metric)))) =
                                    objDefToString objs ^ "\n" ^
                                    (pddlInitToString init (List.concat (map #1 objs))) ^ "\n" ^
                                    pddlGoalToString goal_form ^ "\n"


  fun planActionToString paction = "PAction ''" ^ (pddl_plan_action_name paction) ^ "'' [" ^ (String.concatWith ", " (map pddlObjConsToString (pddl_plan_action_args paction))) ^ "]\r\n"

  fun planToString plan = "definition \"plan = [" ^ (String.concatWith ", " (map planActionToString plan)) ^ "]\"\n"

fun readFile file =
let
    fun next_String input = (TextIO.inputAll input)
    val stream = TextIO.openIn file
in
    next_String stream
end

fun writeFile file content =
    let val fd = TextIO.openOut file
        val _ = TextIO.output (fd, content) handle e => (TextIO.closeOut fd; raise e)
        val _ = TextIO.closeOut fd
    in () end

fun parse_wrapper parser file =
  case (CharParser.parseString parser (readFile file)) of
    Sum.INR x => x
  | Sum.INL err => exit_fail err

val parse_pddl_dom = parse_wrapper PDDL.domain
val parse_pddl_prob = parse_wrapper PDDL.problem
val parse_pddl_plan = parse_wrapper PDDL.plan


fun do_generate_invar_check_scripts dom_file = let
  val parsedDom = parse_pddl_dom dom_file
in
  writeFile "prob_defs.thy" (pddlDomToString parsedDom)
end


val args = CommandLine.arguments()

fun print_help () = (
  println("c Usage: " ^ CommandLine.name() ^ "<domain>")
)

val _ = case args of
  [d] => do_generate_invar_check_scripts d
| _ => (
    println("Invalid command line arguments");
    print_help ();
    exit_fail ""
  )

val _ = OS.Process.exit(OS.Process.success)
