#ifndef STRIPS_BOUND
#define STRIPS_BOUND
#include "../stripsProblem/stripsProblem.h"
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include "../stripsSymmetry/stripsSymmetry.h"

#define SALGO 1
#define BALGO 2
#define MALGO 3
#define NALGO 4

#define TD_ARB 1
#define RD 2
#define RD_TDgt2 3
#define RD_TDgt2_SSle50bound 4

class StripsBound{
  int computeDependencyGraphSCCs();
  void computeSCCAncestors();
  void computeQuotientDependencyGraph(); //i.e. Lifted depency graph over the sccs
  void initSnapshotOrbits();
  void initSnapshotOrbits(int SASVarID);
 public:
  StripsProblem problem;

  std::vector<int>varToSCC;
  std::vector<std::vector<int> > dependencyGraphSCCs;
  std::vector<std::set<int> > dependencyGraphSCCsChildren;
  std::vector<std::set<int> > dependencyGraphSCCsAncestors;
  std::vector<std::set<int> > dependencyGraphSCCsParents;
  std::map<std::pair<int,int>, int> SASASS_to_Sval;
  
  /* std::vector<std::vector<int> > SASASS_to_snapshotOrbit; */
  /* std::vector<std::vector <std::set<int> > > snapshotOrbits; */
  /* std::vector<std::map<int, int> > snapshotOrbit_to_Sval; */
  std::vector<int> SASASS_to_snapshotOrbit;
  std::vector <std::set<int> > snapshotOrbits;
  std::map<int, int> snapshotOrbit_to_ellval;
 
  std::map<int, int> SCC_to_Nval;
  std::map<int, int> SCC_to_Mval;
  int DAGsToAnalyse;//Number of SAS+ DAGs to analyse
  int algorithm;//The bounding algorithm to be used
  int hardest_basecase_fun;//The bounding algorithm to be used
  StripsBound(){}
  StripsBound(const StripsProblem& problemIn, int DAGs, int alg, int bcase):problem (problemIn)
    {  
      /* problem.goals.clear(); */
      /* problem.initial_state_vars.clear(); */
      algorithm = alg;
      DAGsToAnalyse = DAGs;
      hardest_basecase_fun = bcase;
    }
  StripsBound(const std::vector<Variable *> variables, State init, std::vector<std::pair<Variable *, int> > goals,
              std::vector<Operator> operators, vector<MutexGroup> mutexes,
              char* name, vector<Axiom> axioms, int DAGs, bool onlySASSCCs, int alg, int bcase)
    : problem (variables, init, goals, operators, mutexes, name, axioms, onlySASSCCs)
    {
      /* problem.goals.clear(); */
      /* problem.initial_state_vars.clear(); */
      algorithm = alg;
      DAGsToAnalyse = DAGs;
      hardest_basecase_fun = bcase;      
    }
  long long int getSCCSASBound(std::set<int> SCC);
  long long int N(int SCC);
  long long int acycN(int SCC);
  long long int SumN();
  long long int M(int SCC);
  long long int SumM();
  long long int S(int SASVarID, int ass);
  long long int maxS(int SASVarID);
  long long int computeBound(); // The argument is the bounding algorithm to use
  long long int sasBound();
  long long int getRDBound(long long int ubound);
  bool isRD(long long int d);
  long long int getSCCTDBound(std::set<int> SCC);
  long long int mutexBound();
  long long int actionSetBound();
  StripsBound projectFD(std::vector<Proposition> vs);
  StripsBound project(std::vector<Proposition> vs);
  StripsBound snapshot(int sasVarID, int ass);
};
#endif
