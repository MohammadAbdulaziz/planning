#!/bin/bash
rm -rf /tmp/planBound
mkdir /tmp/planBound
mkdir /tmp/planBound/src
mkdir /tmp/planBound/bin
cd ../../
make clean
cd -
cp -rf ../../FD /tmp/planBound/src/
rm -rf /tmp/planBound/src/stripsBound/utils
rm -rf /tmp/planBound/src/FD/src/build_all
rm -rf /tmp/planBound/src/FD/src/translate/
rm -rf /tmp/planBound/src/FD/src/fast-downward.py
rm -rf /tmp/planBound/src/FD/src/driver/
rm -rf /tmp/planBound/src/FD/src/cleanup
rm -rf /tmp/planBound/src/FD/src/search/
rm -rf /tmp/planBound/src/FD/src/preprocess/*
cp -rf ../../FD/src/preprocess/helper_functions.cc /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/variable.cc /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/mutex_group.cc /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/operator.cc /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/axiom.cc /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/state.cc /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/domain_transition_graph.cc /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/successor_generator.cc /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/causal_graph.cc /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/scc.cc /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/max_dag.cc /tmp/planBound/src/FD/src/preprocess/

cp -rf ../../FD/src/preprocess/helper_functions.h /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/variable.h /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/mutex_group.h /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/operator.h /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/axiom.h /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/state.h /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/domain_transition_graph.h /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/successor_generator.h /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/causal_graph.h /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/scc.h /tmp/planBound/src/FD/src/preprocess/
cp -rf ../../FD/src/preprocess/max_dag.h /tmp/planBound/src/FD/src/preprocess/

cp -rf ../../stripsProblem /tmp/planBound/src/
cp -rf ../../stripsBound /tmp/planBound/src/
cp -rf ../../stripsSymmetry /tmp/planBound/src/
cp -rf ../../digraphs /tmp/planBound/src/
cp -rf ../../../utils /tmp/planBound/
cp -rf makefile /tmp/planBound/src/
cp -rf README /tmp/planBound/
cp -rf run.sh /tmp/planBound/bin
rm -rf /tmp/planBound/src/*/*.gch
rm -rf /tmp/planBound/src/*/a.out


cd /tmp
rm bound.zip
zip -r bound.zip planBound/
