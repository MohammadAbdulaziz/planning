#/bin/bash
#CODE_BASE_DIR=/media/Data/Automated_reasoning/PhD_research/planning_bitbucket/planning/codeBase/planning/
CODE_BASE_DIR=./
FD_DIR=$CODE_BASE_DIR/FD/
FF_DIR=$CODE_BASE_DIR/FF/
NAUTY_DIR=$CODE_BASE_DIR/nauty25r9/
Z3_DIR=$CODE_BASE_DIR/z3/
export CODE_BASE_DIR
export FD_DIR
export FF_DIR
export NAUTY_DIR
export Z3_DIR

if [[ $1 == "" ]]; then
  echo "Usage: ./run_sas.sh problem.sas -{Malgo | Nalgo | Salgo | Balgo} -{TD_ARB | -RD | RD.TDgt2 | RD.TDgt2.SSle50bound}"
fi

if [[ $2 == "" ]]; then
  echo "Usage: ./run_sas.sh problem.sas -{Malgo | Nalgo | Salgo | Balgo} -{TD_ARB | -RD | RD.TDgt2 | RD.TDgt2.SSle50bound}"
fi

if [[ $3 == "" ]]; then
  echo "Usage: ./run_sas.sh problem.sas -{Malgo | Nalgo | Salgo | Balgo} -{TD_ARB | -RD | RD.TDgt2 | RD.TDgt2.SSle50bound}"
fi

# noSASSCCs = no two assignments of a sinlge SAS+ var will be in different dependency graph SCC, i.e. every SAS+ variable is contained in one SCC

# valgrind  --main-stacksize=83886080 --leak-check=full $CODE_BASE_DIR/stripsBound/bound 1000 -noSASSCCs $2 $3 < $1

$CODE_BASE_DIR/bound 1000 -noSASSCCs $2 $3 < $1
