#include "stripsBound.h"
#include <limits>
#include <time.h>
//#define debug

long long int StripsBound::actionSetBound()
  {
    if(problem.actions.size() == 1)
      return 1;
    else return -1;
  }

long long int StripsBound::mutexBound()
  { 
    unsigned int vars_in_mutex = 0;
    for(size_t i = 0; i < problem.mutexes.size(); i++)
      {
        vars_in_mutex = vars_in_mutex + problem.mutexes[i].size();
      }
    if(vars_in_mutex < problem.variables.size())
      {
#ifdef debug
        printf("Mutexes don't include every variable!!\r\n");
#endif
        return -1;
      }
    long long int bound = 1;
    for(size_t i = 0; i < problem.mutexes.size(); i++)
      {
        //printf("current mutex group size %d\r\n", (int)problem.mutexes[i].size());
        bound = bound * problem.mutexes[i].size();
        if(bound <= 0)
          {
#ifdef debug
            printf("Mutex bound overflow !!!!\r\n");
#endif
            return -1;
          }
      }
#ifdef debug
    printf("mutex bound = %lld\r\n", bound);
#endif
    return bound;
  }

int StripsBound::computeDependencyGraphSCCs()
  {
    // printf("dep graph vertices: %d\r\n", (int)num_vertices(problem.dependencyGraph));
    // printf("dep graph edges: %d\r\n", (int)num_edges(problem.dependencyGraph));
    std::vector<int>vertex_to_component = std::vector<int>(num_vertices(problem.dependencyGraph));
    int num = 
         strong_components(problem.dependencyGraph, 
                           make_iterator_property_map(
                           vertex_to_component.begin(),
                           get(boost::vertex_index,
                               problem.dependencyGraph),
                           vertex_to_component[0]));
    varToSCC = vertex_to_component;
    std::vector<int> empty;
    for(int i = 0 ; i < num ; i ++)
      dependencyGraphSCCs.push_back(empty);
#ifdef debug
    printf("Dependency graph has %d strongly connected components\r\n", num);
#endif
    for(size_t i = 0; i < problem.variables.size(); i++)
      {
#ifdef debug
        printf("vertex_to_component[%d] = %d\r\n", (int)i, vertex_to_component[i]);
#endif
        dependencyGraphSCCs[varToSCC[i]].push_back(i);
      }
    return num;
  }

void StripsBound::computeQuotientDependencyGraph()
  {
#ifdef debug
    printf("Computing (dependency Graph/SCCs)\r\n");
#endif
    std::set<int> empty;
    for(size_t i = 0 ; i < dependencyGraphSCCs.size() ; i++)
      {
        dependencyGraphSCCsChildren.push_back(empty);
        dependencyGraphSCCsParents.push_back(empty);
      }
    for(size_t i = 0; i < problem.variables.size(); i++)
      {
#ifdef debug
        printf("Processing the SCC %d of vertex %d\r\n", varToSCC[i], (int) i);
#endif
        for(size_t j = 0; j < problem.varChildren[i].size(); j++)
          {
#ifdef debug
            printf("Processing child %d of vertex %d, whose SCC is %d\r\n", problem.varChildren[i][j], (int) i, varToSCC[problem.varChildren[i][j]]);
#endif
            if(varToSCC[problem.varChildren[i][j]] != varToSCC[i])
              //Lifted dep graph has edges between different SCCs only
              dependencyGraphSCCsChildren[varToSCC[i]].insert(varToSCC[problem.varChildren[i][j]]);
          }
        for(size_t j = 0; j < problem.varParents[i].size(); j++)
          {
#ifdef debug
            printf("Processing parent %d of vertex %d, whose SCC is %d\r\n", problem.varParents[i][j], (int) i, varToSCC[problem.varParents[i][j]]);
#endif
            if(varToSCC[problem.varParents[i][j]] != varToSCC[i])
              dependencyGraphSCCsParents[varToSCC[i]].insert(varToSCC[problem.varParents[i][j]]);
          }
      }
    int edges = 0;
    for(size_t i = 0 ; i < dependencyGraphSCCsChildren.size(); i++)
      {  
        edges = edges + dependencyGraphSCCsChildren[i].size();
      }
#ifdef debug
    printf("Sanity checking the lifted dep graph\r\n");
#endif
    int neg_edges = 0;
    for(size_t i = 0 ; i < dependencyGraphSCCsParents.size(); i++)
      {  
        neg_edges = neg_edges + dependencyGraphSCCsParents[i].size();
      }
#ifdef debug
    printf("The lifted dep graph has %d pos edges and %d neg edges\r\n", edges, neg_edges);
#endif
    if( ! (edges == neg_edges))
      {
        printf("Corrupt lifted dependency graph, exiting!!!\r\n");
        exit(-1);
      }
    for(size_t i = 0 ; i < dependencyGraphSCCs.size() ; i ++)
      {
        for(std::set<int>::iterator currentChild =  dependencyGraphSCCsChildren[i].begin(); currentChild != dependencyGraphSCCsChildren[i].end() ; currentChild++)
          {
            if(dependencyGraphSCCsParents[*currentChild].find((int) i) == dependencyGraphSCCsParents[*currentChild].end())
              {
                printf("Corrupt lifted dependency graph: SCC %d has %d as a child but not the other way around, exiting!!!\r\n", (int) i, *currentChild);
                exit(-1);
              }
          } 
        for(std::set<int>::iterator currentParent =  dependencyGraphSCCsParents[i].begin(); currentParent != dependencyGraphSCCsParents[i].end() ; currentParent++)
          {
            if( dependencyGraphSCCsChildren[*currentParent].find((int) i) == dependencyGraphSCCsChildren[*currentParent].end())
              {
                printf("Corrupt lifted dependency graph: SCC %d has %d as a parent but not the other way around, exiting!!!\r\n", (int) i, *currentParent);
                exit(-1);
              }
          }
     }
#ifdef debug
    printf("Dependency Graph/SCCs has %d vertices and %d edges\r\n", (int)dependencyGraphSCCs.size(), edges);
#endif
  }

long long int StripsBound::sasBound()
  {
    long long int bound = 1;
    std::map<Proposition, std::vector<int> >::iterator sasiter = problem.sasVarMap.begin();
    for(size_t i = 0; i < problem.sasVarMap.size(); i++)
      {
#ifdef debug
        printf("SAS var %d size = %d\r\n", (sasiter->first), (int)(sasiter->second).size());
#endif
        //TODO: the following if condition should be removed.
        if((sasiter->second).size() != 0)
          {
            //printVector((sasiter->second));
            bound = bound * ((sasiter->second).size() + 1);
          }
        if(bound <= 0)
          {
#ifdef debug
            printf("SAS+ bound overflow\r\n");
#endif
            return -1;
          }
        sasiter++;
      }
#ifdef debug
    printf("SAS+ bound = %lld\r\n", bound);
#endif
    return bound;
  }

long long int StripsBound::getSCCTDBound(std::set<int> SCC)
  {
    //printSet(SCC);
    std::map <int,int> varNums;
    for(std::set<int>::iterator i =  SCC.begin(); i != SCC.end() ; i++)
      {
        varNums[problem.sasAss[*i].first]++;
      }
    long long int SCCCard = 1;    
    for (std::map<int,int>::iterator it=varNums.begin(); it!=varNums.end(); ++it)
      {
#ifdef debug
        cout << "The SCC has " << it->second << " variables belonging to SAS+ variable" << it->first << " " << problem.initSASVarSizes[it->first] << endl;
        if(this->problem.isStateGraphAcyclic(it->first))
          cout << it->first << " acyclic" << std::endl;
        cout << (computeTD(this->problem.stateGraphs[it->first]) + 1) << "/" << it->second << std::endl;
#endif
        if(it->second == (int)problem.initSASVarSizes[it->first])
          {
#ifdef debug
            cout << "The SCC includes all the members of the SAS variable " << it->first << endl;
#endif
            SCCCard *= (computeTD(this->problem.stateGraphs[it->first]) + 1);
          }
        else
          {
#ifdef debug
            cout << "The SCC does not include all the members of the SAS variable " << it->first << endl;
#endif
            //SCCCard *= (it->second);
            SCCCard *= (it->second + 1);
          }
        if(SCCCard <= 0)
          {
#ifdef debug
            printf("TD bound overflow error\r\n");
#endif
            return -1;
          }
        //check whether it is a full SAS variable or not
      }
#ifdef debug
    cout << "Current SCC has variables from " << varNums.size() << " SAS+ variables, and its computed TD bound is " << SCCCard << endl;
#endif
    return SCCCard - 1;
  }

long long int StripsBound::getSCCSASBound(std::set<int> SCC)
  {
    //printSet(SCC);
    std::map <int,int> varNums;
    for(std::set<int>::iterator i =  SCC.begin(); i != SCC.end() ; i++)
      {
         varNums[problem.sasAss[*i].first]++;
      }
    long long int SCCCard = 1;    
    for (std::map<int,int>::iterator it=varNums.begin(); it!=varNums.end(); ++it)
      {
#ifdef debug
        cout << "This SCC has " << it->second << " variables belonging to SAS+ variable" << it->first << " " << problem.initSASVarSizes[it->first] << endl;
#endif
        if(it->second == (int)problem.initSASVarSizes[it->first])
          {
#ifdef debug
            cout << "This includes all the members of the SAS variable\r\n";
#endif
            SCCCard *= it->second;
          }
        else
          {
            //SCCCard *= (it->second);
            SCCCard *= (it->second + 1);
          }
        if(SCCCard <= 0)
          {
#ifdef debug
            printf("SAS+ bound overflow error\r\n");
#endif
            return -1;
          }
        //check whether it is a full SAS variable or not
      }
#ifdef debug
    cout << "Current SCC has variables from " << varNums.size() << " SAS+ variables, and its computed SAS+ bound is " << SCCCard << endl;
#endif
    return SCCCard;
  }

long long int StripsBound::N(int SCCid)
  {
#ifdef debug
    printf("New call to N, SCCid = %d\r\n", SCCid);
#endif    
    if(SCC_to_Nval.find(SCCid) != SCC_to_Nval.end())
      {
#ifdef debug
        printf("N for SCC %d already computed\r\n", SCCid);
#endif
        return SCC_to_Nval[SCCid];
      }
    StripsBound projProb = this->project(dependencyGraphSCCs[SCCid]);
    long long int l = projProb.computeBound();
#ifdef debug
    printf("New call to N, SCCid = %d, l = %lld \r\n", SCCid, l);
#endif
    if(l < 0)
      {
        cout << "N overflow error" << endl;
        return -1;
      }
   long long int childrenBound = 0;
   for(std::set<int>::iterator i = dependencyGraphSCCsChildren[SCCid].begin(); i != dependencyGraphSCCsChildren[SCCid].end() ; i++)
     {
       childrenBound += N(*i);
     }
#ifdef debug
   printf("Children bound is %lld\r\n", childrenBound);
#endif
   // TODO: This next line overflows in the problem: unsolve_merge_shrink_set_mystery_prob12.pddl.bound.out
   // Need to have all bounds and bounding functions long long int
   long long int bound = ((long long int)l)*((long long int)(childrenBound + 1));
   if (((long long int)((int)bound)) < bound)
     {
        cout << "N overflow error" << endl;
        return -1;
     }
   SCC_to_Nval[SCCid] = bound;
#ifdef debug
   printf("New call to N, SCCid = %d, l = %lld, N = %lld\r\n", SCCid, l, bound);
#endif
   return bound ;
  }

long long int StripsBound::acycN(int SCCid)
  {
#ifdef debug
    printf("New call to acycN, SCCid = %d\r\n", SCCid);
#endif    
    if(SCC_to_Nval.find(SCCid) != SCC_to_Nval.end())
      {
#ifdef debug
        printf("acycN for SCC %d already computed\r\n", SCCid);
#endif
        return SCC_to_Nval[SCCid];
      }
    StripsBound projProb = this->project(dependencyGraphSCCs[SCCid]);
    long long int l = projProb.computeBound();
#ifdef debug
    printf("New call to acycN, SCCid = %d, l = %lld \r\n", SCCid, l);
#endif
    if(l < 0)
      {
        cout << "acycN overflow error" << endl;
        return -1;
      }
   long long int childrenBound = 0;
   for(std::set<int>::iterator i = dependencyGraphSCCsChildren[SCCid].begin(); i != dependencyGraphSCCsChildren[SCCid].end() ; i++)
     {
       childrenBound += acycN(*i);
     }
#ifdef debug
   printf("Children bound is %lld\r\n", childrenBound);
#endif
   // TODO: This next line overflows in the problem: unsolve_merge_shrink_set_mystery_prob12.pddl.bound.out
   // Need to have all bounds and bounding functions long long int
   long long int bound = 0; 
   if (projProb.problem.isStateSpaceAcyclic())
     {
#ifdef debug
       printf("State space of projection on SCCid %d is acyclic, its bound is %lld, its childrens' bound is %lld!\r\n", SCCid, l, childrenBound);
#endif
       bound = l;
     }
   else
     {
       bound = l*(childrenBound + 1);
     }
   SCC_to_Nval[SCCid] = bound;
#ifdef debug
   printf("New call to acycN, SCCid = %d, l = %lld, acycN = %lld\r\n", SCCid, l, bound);
#endif
   return bound ;
  }

long long int StripsBound::SumN()
  {
    //TODO: Only one call to N
    long long int bound = 0;
    for(size_t i = 0 ; i < dependencyGraphSCCs.size() ; i++)
      {
        //long long int tempBound = N(i);
        long long int tempBound = acycN(i);
        if(tempBound < 0)
         {
#ifdef debug
           cout << "Nalgo bound overflow!!" << endl;
#endif
           return -1;
         }
        bound += tempBound;
        if(bound < 0)
         {
#ifdef debug
           cout << "Nalgo bound overflow!!" << bound << endl;
#endif
           return -1;
         }
#ifdef debug
        //cout << "SCC: " << i << " Bound sum " << bound << " N(SCC) " << tempBound << " size(component) " << dependencyGraphSCCs[i].size() << endl;
        cout << "SCC: " << i << " Bound sum " << bound << " acycN(SCC) " << tempBound << " size(component) " << dependencyGraphSCCs[i].size() << endl;
#endif
      }
    return bound;
  }

long long int StripsBound::M(int SCCid)
  {
#ifdef debug
    printf("New call to M, SCCid = %d\r\n", SCCid);
#endif    
    if(SCC_to_Mval.find(SCCid) != SCC_to_Mval.end())
      {
#ifdef debug
        printf("M for SCC %d already computed\r\n", SCCid);
#endif
        return SCC_to_Mval[SCCid];
      }
    StripsBound projProb = this->project(dependencyGraphSCCs[SCCid]);
    long long int l = projProb.computeBound();
#ifdef debug
    printf("New call to M, SCCid = %d, l = %lld \r\n", SCCid, l);
#endif
    if(l < 0)
      {
        cout << "M overflow error" << endl;
        return -1;
      }
   long long int ancestorsBound = 0;
   for(std::set<int>::iterator i = dependencyGraphSCCsAncestors[SCCid].begin(); i != dependencyGraphSCCsAncestors[SCCid].end() ; i++)
     {
       ancestorsBound += M(*i);
     }
#ifdef debug
   printf("Ancestors bound is %lld\r\n", ancestorsBound);
#endif
   // TODO: This next line overflows in the problem: unsolve_merge_shrink_set_mystery_prob12.pddl.bound.out
   // Need to have all bounds and bounding functions long long int
   long long int bound = l + (1 + l)*ancestorsBound;
   if (((long long int)((int)bound)) < bound)
     {
        cout << "M overflow error" << endl;
        return -1;
     }
   SCC_to_Mval[SCCid] = bound;
#ifdef debug
   printf("New call to M, SCCid = %d, l = %lld, M = %lld\r\n", SCCid, l, bound);
#endif
   return bound ;
  }

void StripsBound::computeSCCAncestors()
  {
#ifdef debug
    printf("Starting computing ancestors of all SCCs (%d) with %d parent entries \r\n", (int) dependencyGraphSCCs.size(), (int) dependencyGraphSCCsParents.size());
#endif
    for(size_t SCCid = 0; SCCid < dependencyGraphSCCs.size(); SCCid ++)
      {
        std::set<int> empty;
        dependencyGraphSCCsAncestors.push_back(empty);
#ifdef debug
        printf("Added an empty ancestors set for SCC %d\r\n", (int) SCCid);
#endif
      }
    for(size_t SCCid = 0; SCCid < dependencyGraphSCCs.size(); SCCid ++)
      {
#ifdef debug
        printf("Processing SCC %d which has %d parents which are\r\n", (int) SCCid, (int) dependencyGraphSCCsParents[SCCid].size());
        printSet(dependencyGraphSCCsParents[SCCid]);
#endif
        std::set<int> SCCsToProcess;
        SCCsToProcess.clear();
        SCCsToProcess = dependencyGraphSCCsParents[SCCid];
#ifdef debug
        printf("SCCsToProcess = %d\r\n", (int) SCCsToProcess.size());
#endif
        std::set<int>::iterator i = SCCsToProcess.begin();
        while( i != SCCsToProcess.end())
          {
	    std::set<int> currentParents = dependencyGraphSCCsParents[*i];
#ifdef debug
            printf("Processing SCC %d which has %d parents which are\r\n", *i, (int) currentParents.size());
            printSet(currentParents);
#endif
            dependencyGraphSCCsAncestors[SCCid].insert(*i);
#ifdef debug
            printf("Inserted it in the ancestors\r\n");
#endif
            SCCsToProcess.erase(i);
#ifdef debug
            printf("Removd it from the SCCs to process\r\n");
            printf("Adding its parents to the SCCs to process which are now %d\r\n", (int) SCCsToProcess.size());
#endif
            SCCsToProcess = customUnion(SCCsToProcess, currentParents);
#ifdef debug
            printf("Added its parents to the SCCs to process which are now %d\r\n", (int) SCCsToProcess.size());
#endif
            i = SCCsToProcess.begin();
          }
      }
#ifdef debug
    printf("Finished computing ancestors of SCCs\r\n");
    for(size_t i = 0 ; i < dependencyGraphSCCs.size() ; i++)
      {
        cout << "SCC: " << i << " has " << (int) dependencyGraphSCCsAncestors[i].size() << " ancestors and " << (int) dependencyGraphSCCsChildren[i].size() << " children. " << endl;
      }
#endif    
  }

long long int StripsBound::SumM()
  {
    // Compute ancestors for every SCC
    computeSCCAncestors();
    long long int bound = 0;
    for(size_t i = 0 ; i < dependencyGraphSCCs.size() ; i++)
      {
        long long int tempBound = M(i);
        if(tempBound < 0)
         {
#ifdef debug
           cout << "Malgo bound overflow!!" << endl;
#endif
           return -1;
         }
        bound += tempBound;
        if(bound < 0)
         {
#ifdef debug
           cout << "Malgo bound overflow!!" << bound << endl;
#endif
           return -1;
         }
#ifdef debug
        cout << "SCC: " << i << " Bound sum " << bound << " M(SCC) " << tempBound << " size(component) " << dependencyGraphSCCs[i].size() << endl;
#endif
      }
    return bound;
  }


//#define SNAP_SHOT_SYMM_BREAK
long long int StripsBound::S(int SASVarID, int ass)
  {
    if(SASASS_to_Sval.find(std::pair<int,int>(SASVarID, ass)) != SASASS_to_Sval.end())
      {
#ifdef debug
        printf("S for SASvar %d ass %d already computed\r\n", SASVarID, ass);
#endif
        return SASASS_to_Sval[std::pair<int,int>(SASVarID, ass)];
      }
#ifdef debug
    printf("Computing S for SASvar %d ass %d\r\n", SASVarID, ass);
#endif
    fflush(stdout);
    StripsBound snapShotProb = this->snapshot(SASVarID, ass);
    long long int l = -1;
#ifdef SNAP_SHOT_SYMM_BREAK
    if(snapshotOrbit_to_ellval.find(SASASS_to_snapshotOrbit[ass]) != snapshotOrbit_to_ellval.end())
      {
#ifdef debug
        printf("l for snapshot orbit of SASvar %d ass %d already computed\r\n", SASVarID, ass);
#endif
        l = snapshotOrbit_to_ellval[SASASS_to_snapshotOrbit[ass]];
      }
    else
      {
#endif
#ifdef debug
        printf("snapshot size %d vs %d variables\r\n",(int)snapShotProb.problem.variables.size(), (int)this->problem.variables.size());
        printf("SASVar %d has %d asses in the snapshot vs %d in the original\r\n", SASVarID, (int)snapShotProb.problem.sasVarMap[SASVarID].size(), (int)this->problem.sasVarMap[SASVarID].size());
        cout << "Calling computeBound\n";
#endif
        l = snapShotProb.computeBound();
#ifdef debug
        cout << "Finished computeBound\n";
#endif
#ifdef SNAP_SHOT_SYMM_BREAK
        snapshotOrbit_to_ellval[SASASS_to_snapshotOrbit[ass]] = l;
       }
#endif
#ifdef debug
    printf("New call to S, SASVarId = %d, AssID = %d, l = %lld \r\n", SASVarID, ass, l);
#endif
    
    if(l < 0)
      {
        cout << "S overflow error" << endl;
        return -1;
      }
#ifdef debug
    printSet(problem.sasAssChildren[SASVarID][ass]);
#endif
    long long int childrenBound = -1;
    for(std::set<int>::iterator 
          i = problem.sasAssChildren[SASVarID][ass].begin();
        i != problem.sasAssChildren[SASVarID][ass].end() ; i++)
      {
#ifdef debug
        printf("SASvar %d, ass %d has children \r\n", SASVarID, ass);
#endif
        long long int tempBound = S(SASVarID, *i);
        if(tempBound < 0)
          {
           cout << "S overflow error" << endl;
           return -1;
          }
        if (tempBound > childrenBound)
          childrenBound = tempBound;
      }
    long long int bound = -1;
    if(problem.sasAssChildren[SASVarID][ass].size() != 0)
       bound = l + 1 + childrenBound;
    else 
      //TODO: bound = l;
      bound = l;
    SASASS_to_Sval[std::pair<int,int>(SASVarID, ass)] = bound;
    //snapshotOrbit_to_ellval[SASASS_to_snapshotOrbit[ass]] = bound;
    return bound;
  }

long long int StripsBound::maxS(int SASVarID)
  {
#ifdef SNAP_SHOT_SYMM_BREAK
    initSnapshotOrbits(SASVarID);
#endif
#ifdef debug
    printf("computing MaxS for SASvar %d\r\n", SASVarID);
#endif
    long long int bound = -1;
    for(size_t i = 0 ; i < problem.sasVarMap[SASVarID].size() ; i++)
      {
        long long int tempBound = S(SASVarID, i);
        if(tempBound < 0)
         {
           cout << "Salgo bound overflow!!" << endl;
           return -1;
         }
        if(tempBound > bound)
           bound = tempBound;
#ifdef debug
        cout << "<" << SASVarID << "," << i << ">"<< " Bound sum " << bound << " S(SASVarID, ass) " << tempBound << endl;
#endif
      }
#ifdef SNAP_SHOT_SYMM_BREAK
    for(size_t ass = 0 ; ass < problem.sasVarMap[SASVarID].size() ; ass++)
      {
        snapshotOrbits.clear();
        SASASS_to_snapshotOrbit.clear();
      }
    snapshotOrbit_to_ellval.clear();
#endif
    return bound;
  }

extern int max_atom_size;
extern int search_size;
extern int max_atom_dom_size;
extern int max_atom_actions_size;

long long int StripsBound::computeBound()
  {
#ifdef debug
    cout << "this->problem.variables.size() = " << this->problem.variables.size() << endl;
    cout << "this->problem.actions.size() = " << this->problem.actions.size() << endl;
#endif
    if(this->problem.variables.size() == 0 || this->problem.actions.size() == 0)
      {
	return 0;
      }
    search_size ++;
    long long int bound = std::numeric_limits<int>::max();
    long long int tempBound = mutexBound();
#ifdef debug
    printf("mutexBound = %lld\r\n", tempBound);
#endif
    long long int sspaceSizeEst = getSCCSASBound(bigUnion(coDom(problem.sasVarMap)));
    tempBound = sspaceSizeEst;
#ifdef debug
    printf("SCCSASBound = %lld\r\n", tempBound);
#endif
    if(tempBound != -1 && tempBound < bound)
      {
        bound = tempBound;
      }
    tempBound = actionSetBound();
#ifdef debug
    printf("actionSetBound = %lld\r\n", tempBound);
#endif
    if(tempBound != -1 && tempBound < bound)
      {
        bound = tempBound;
      }
#ifdef debug
    long long int TDBoundtemp = getSCCTDBound(bigUnion(coDom(problem.sasVarMap)));
    tempBound = TDBoundtemp;
    printf("getSCCTDBound = %lld\r\n", tempBound);
#else
    tempBound = getSCCTDBound(bigUnion(coDom(problem.sasVarMap)));
#endif
    if(tempBound != -1 && tempBound < bound)
      {
        bound = tempBound;
      }
    //Computing Sbound for state graphs that are DAGs
    bool decomposed = false;
    if(algorithm == MALGO)
      {
        //Computing SCCs of dependency graph and Nbound if there is more than one SCC
        if(computeDependencyGraphSCCs() > 1)
          {
            decomposed = true;
            computeQuotientDependencyGraph();
            tempBound = SumM();
          }
        if(tempBound != -1 && tempBound < bound)
          {
            bound = tempBound;
          }
      }
    if(algorithm == NALGO || algorithm == BALGO)
      {
        //Computing SCCs of dependency graph and Nbound if there is more than one SCC
        if(computeDependencyGraphSCCs() > 1)
          {
            decomposed = true;
            computeQuotientDependencyGraph();
            tempBound = SumN();
          }
        if(tempBound != -1 && tempBound < bound)
          {
            bound = tempBound;
          }
      }
    if(algorithm == SALGO || algorithm == BALGO)
      {
        if(DAGsToAnalyse > 0 && !decomposed)
          {
#ifdef debug
            printf("DAGsToAnalyse = %d\r\n",DAGsToAnalyse);
#endif
            int varStateDAGs = 0;
            for(size_t i = 0 ; i < problem.stateGraphs.size(); i++)
              {        
                if( problem.isStateGraphAcyclic(i) &&
                    //TODO: this condition should be removed and SAS vars should be removed with snapshotting
                    problem.sasVarMap[i].size() == (size_t)problem.initSASVarSizes[i])
                  {
#ifdef debug
                    printf("MaxS taken\r\n");
#endif
                    varStateDAGs++;
                    tempBound = maxS(i);
                    if(tempBound != -1 && tempBound < bound)
                      {
                        decomposed = true;
                        bound = tempBound;
                        break;
                      }
                  }
              }
#ifdef debug
            printf("Processed varsetStateDAGs = %d/%d\r\n", varStateDAGs, (int)problem.stateGraphs.size());
#endif
          }
      }

    if(tempBound < std::numeric_limits<int>::max() && 0 <= tempBound)
      {
        if ((!decomposed && tempBound > 2 && sspaceSizeEst <= 50 && hardest_basecase_fun == RD_TDgt2_SSle50bound) ||
            (!decomposed && tempBound > 2 && hardest_basecase_fun == RD_TDgt2) ||
            (!decomposed && hardest_basecase_fun == RD)) // && sspaceSizeEst <= 1000 && 0 <= sspaceSizeEst)
          {
#ifdef debug
            printf("Computing RD for undecomposed problem with %d states\n", sspaceSizeEst);
#endif
            long long int RDBound = 0;//This bound is the on the number of states considered in the longest distinct path, not the number of action, hence the plus 1.
            RDBound = tempBound + 1;
#ifdef debug
            printf("RDUpperbound = %lld\n", RDBound);
            long long int RDBoundtemp = getRDBound(RDBound);
            tempBound = RDBoundtemp;
            printf("RD = %lld\r\n", tempBound);
#else
            tempBound = getRDBound(RDBound);
#endif
            if (tempBound == 0)
              {
                printf("RD is 0, probably an error. Exiting!!!\n");
                problem.writePorblemPDDL();
                exit(-1);
              }
            if(tempBound != -1 && tempBound < bound)
              {
                bound = tempBound;
              }
          }
      }
    //Note: The following is just to compute statistics
    if(!decomposed)
      {
        if(max_atom_dom_size < (int)this->problem.variables.size())
          {
            max_atom_dom_size = this->problem.variables.size();
            free(this->problem.name);
	    this->problem.name = (char*)malloc(strlen("_max_vars") + 1);
	    this->problem.name = strcpy(this->problem.name, "_max_vars");
            this->problem.writePorblemPDDL();
          }
        if(max_atom_actions_size < (int)this->problem.actions.size())
          {
            free(this->problem.name);
	    this->problem.name = (char*)malloc(strlen("_max_actions") + 1);
	    this->problem.name = strcpy(this->problem.name, "_max_actions");
            this->problem.writePorblemPDDL();
            max_atom_actions_size = this->problem.actions.size();
          }
      }
#ifdef debug
    cout << "SCC bound is " << sspaceSizeEst << " TD bound is " << TDBoundtemp << " comuteBound returns " << bound << std::endl;
#endif
    if(bound == std::numeric_limits<int>::max())
      return -1;
    else
      return bound;
  }

//Projection using FD
StripsBound StripsBound::projectFD(std::vector<Proposition> vs)
  {
    StripsProblem tempProb;
    tempProb.name = (char*)malloc(strlen("Proj") + 1);
    strcpy(tempProb.name, "Proj");
    if(system("rm domProj.pddl factProj.pddl output.sas") == -1)
      {
        printf("System command failed, exiting!!\r\n");
        exit(-1);
      }
    tempProb.domSize = problem.domSize;
    tempProb.goals = customInter(problem.goals, vs);
    tempProb.initial_state_vars = customInter(problem.initial_state_vars, vs);
    for(size_t i = 0; i < problem.actions.size(); i++)
      tempProb.actions.push_back(problem.actions[i].project(vs));
    tempProb.writePorblemPDDL();
    char*command = (char*)malloc(strlen("timeout 1800 $FD_DIR/src/translate/translate.py domProj.pddl factProj.pddl>/dev/null") + 1 ); 
    sprintf(command, "timeout 1800 $FD_DIR/src/translate/translate.py domProj.pddl factProj.pddl>/dev/null");
    if(system(command) == -1)
      {
        printf("System command failed, exiting!!\r\n");
        exit(-1);
      }
    free(command);
    ifstream output;
    output.open("output.sas");
    bool metric;
    vector<Variable> internal_variables;
    vector<Variable *> variables;
    vector<MutexGroup> mutexes;
    State initial_state;
    vector<std::pair<Variable *, int> > goals;
    vector<Operator> operators;
    vector<Axiom> axioms;
    read_preprocessed_problem_description(output, metric, internal_variables, variables, mutexes, initial_state, goals, operators, axioms);
    output.close();
    char*name = (char*)"Prob";
    StripsBound projBoundingTool(variables, initial_state, goals, operators, mutexes, name, axioms, DAGsToAnalyse, problem.onlySASSCCs, this->algorithm, this->hardest_basecase_fun);
    return projBoundingTool;
  }

//Custom projection
StripsBound StripsBound::project(std::vector<Proposition> vs)
  {
    StripsBound projBoundingTool(this->problem.project(vs), DAGsToAnalyse, this->algorithm, this->hardest_basecase_fun);
    return projBoundingTool;
  }

StripsBound StripsBound::snapshot(int sasVarID, int ass)
  {
    StripsBound snapshotBoundingTool(this->problem.snapshot(std::pair<int, int>(sasVarID, ass)), DAGsToAnalyse - 1, this->algorithm, this->hardest_basecase_fun);
    return snapshotBoundingTool;
  }

extern double symmetry_compuation_time;

void StripsBound::initSnapshotOrbits(int SASVarID)
  {
    clock_t start, end;
    start = clock();
    std::vector<int> empty_vector;
    std::vector<std::set<int> > empty_vector_of_sets;
    for(size_t ass = 0 ; ass < problem.sasVarMap[SASVarID].size() ; ass++)
      {
        std::set<int> singletonOrbit;
        singletonOrbit.insert(ass);
        snapshotOrbits.push_back(singletonOrbit);
        singletonOrbit.clear();
        SASASS_to_snapshotOrbit.push_back(ass);
      }
#ifdef debug
    printf("Computing isomorphic asses for SAS var %d whose size %d\r\n", SASVarID, (int) problem.sasVarMap[SASVarID].size());
#endif
    int oldNumOrbits = coDom(vectorToMap(SASASS_to_snapshotOrbit)).size();
    while(1)
      {
        int newNumOrbits = -1;
        if(oldNumOrbits == newNumOrbits)
          break;
        else
          oldNumOrbits = newNumOrbits;
        for(size_t i = 0 ; i < snapshotOrbits.size() ; i++)
          {
            if(snapshotOrbits[i].size() != 0)
              {
                int ass_i_snapshot = *(snapshotOrbits[i].begin());
                StripsSymmetry symmTool((this->snapshot(SASVarID, ass_i_snapshot)).problem);
                symmTool.initColours();
                for(size_t j = i + 1; j < snapshotOrbits.size() ; j++)
                  {
                    if(snapshotOrbits[j].size() != 0)
                      {
                        int ass_j_snapshot = *(snapshotOrbits[i].begin());
                        if(symmTool.isomorphic((this->snapshot(SASVarID, ass_j_snapshot)).problem))
                          {
#ifdef debug
                            printf("Ass %d and %d are isomorphic\r\n", ass_i_snapshot, ass_j_snapshot);
#endif
                            // (this->snapshot(SASVarID, (int)i)).problem.writePorblemPDDL();
                            // system("cp domProj.pddl domProj1.pddl");
                            // system("cp factProj.pddl factProj1.pddl");
                            // (this->snapshot(SASVarID, (int)j)).problem.writePorblemPDDL();
                            // i's orbit = union of i's and j's orbit
                            int i_orbit_id = SASASS_to_snapshotOrbit[ass_i_snapshot];
                            int j_orbit_id = SASASS_to_snapshotOrbit[ass_j_snapshot];
                            snapshotOrbits[i_orbit_id] = customUnion(snapshotOrbits[i_orbit_id], snapshotOrbits[j_orbit_id]);
                            //j's orbit is the merged orbit
                            SASASS_to_snapshotOrbit[ass_j_snapshot] = i_orbit_id;
                            snapshotOrbits[j].clear();
                          }
                        else
                          {
#ifdef debug
                            printf("Ass %d and %d are NOT isomorphic\r\n", (int)i, (int)j);
#endif
                          }
    		      }
                  }
              }
          }
        newNumOrbits = coDom(vectorToMap(SASASS_to_snapshotOrbit)).size();
      }
    end = clock();
    symmetry_compuation_time += ((double)(end - start))/CLOCKS_PER_SEC;

    // for(size_t i = 0 ; i < problem.sasVarMap[SASVarID].size() ; i++)
    //   {
    //     fprintf(file, "Snapshot orbit of ass %d is %d\r\n", i, SASASS_to_snapshotOrbit[i]);
    //     printSet(snapshotOrbits[SASASS_to_snapshotOrbit[i]]);
    //   }
  }

void mutex_vars(std::vector<Proposition> vars, unsigned int t, FILE* file)
  {
    fprintf(file, "\n;;Mutex constraints\n");
    std::vector<int>::iterator var_iter2 = vars.begin();
    for(size_t i = 0; i < vars.size(); i++)
      {
	std::vector<int>::iterator var_iter3 = var_iter2 + 1;
	for(size_t j = i + 1; j < vars.size(); j++)
	  {
	    fprintf(file, "(assert (or (not v%d_%d) (not v%d_%d)))\n", (int)*var_iter2, (int)t, (int)*var_iter3, (int)t);
	    var_iter3++;
	  }
	var_iter2++;
      }
  }

void eq_states(std::set<Proposition> vars, unsigned int t1, unsigned int t2, FILE* file)
  {
    fprintf(file, "\n;;States equal\n");
    std::set<Proposition>::iterator var_iter = vars.begin();
    fprintf(file, "(and true\n");
    for(size_t j = 0; j < vars.size(); j++)
      {
        fprintf(file, " (= v%d_%d v%d_%d)", *var_iter, (int)t1, *var_iter, (int)t2);
        var_iter++;
      }
    fprintf(file, ")\n");
  }

void frame_axiom(std::set<Proposition> vars, unsigned int t, FILE* file)
  {
    fprintf(file, "\n;;Frame axioms\n");
    eq_states(vars, t, t + 1, file);
    fprintf(file, "\n");
  }

void props_hold(std::set<Proposition> vars, unsigned int t, FILE* file)
  {
    fprintf(file, "\n;;Those props hold:\n");
    std::set<Proposition>::iterator var_iter = vars.begin();
    fprintf(file, "(and true");
    for(size_t j = 0; j < vars.size(); j++)
      {
        fprintf(file, " v%d_%d", *var_iter, (int)t);
        var_iter++;
      }
    fprintf(file, ")\n");
  }

void props_hold_not(std::set<Proposition> vars, unsigned int t, FILE* file)
  {
    fprintf(file, "\n;;Those props do not hold:\n");
    std::set<Proposition>::iterator var_iter = vars.begin();
    fprintf(file, "(and true");
    for(size_t j = 0; j < vars.size(); j++)
      {
        fprintf(file, " (not v%d_%d)", *var_iter, (int)t);
        var_iter++;
      }
    fprintf(file, ")\n");
  }

#include <fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>

bool StripsBound::isRD(long long int d)
  {
    mkfifo("isRDfile.smt2", 0666);
    pid_t pid = fork();
    if(pid == 0)
      {
        system("yices-smt2 --interactive < isRDfile.smt2 1> is_rd_out 2>is_rd_err");
        exit(0);
      }      
    int fifo_fd = open("isRDfile.smt2", O_WRONLY);
    if (fifo_fd == -1)
      {
        printf("Could not open FIFO 1, exiting!!!\n");
        exit(-1);
      }
    FILE *isRDfile;
    if((isRDfile = fdopen(fifo_fd, "w")) == NULL)
    {
      printf("Could not open FIFO 2, exiting!!!\n");
      exit(-1);
    }
    fprintf(isRDfile, "(set-logic QF_UF)\n");    

    for(size_t t = 0; t <= d; t++)
      {
	for(size_t i = 0; i < problem.variables.size(); i++)
	  {
	    fprintf(isRDfile, "(declare-const v%d_%d Bool)\n", (int)i, (int)t);
	  }

	std::map<Proposition, std::vector<int> >::iterator sasiter = problem.sasVarMap.begin();
	for(size_t i = 0; i < problem.sasVarMap.size(); i++)
	  {
            if(sasiter->second.size() != 0)
              {
                std::vector<int>::iterator assiter = sasiter->second.begin();
                fprintf(isRDfile, "(assert (or");
                for(size_t i = 0; i < sasiter->second.size(); i++)
                  {
                    fprintf(isRDfile, " v%d_%d", *assiter, (int)t);
                    assiter++;
                  }
                fprintf(isRDfile, "))\n");
                mutex_vars(sasiter->second, t, isRDfile);
              }
#ifdef debug
            else
              {
                printf("SAS var %d is not in this abstraction\n", sasiter->first);
              }
#endif
            sasiter++;
          }
	for(size_t i = 0; i < problem.mutexes.size(); i++)
	  {
	    mutex_vars(problem.mutexes[i], t, isRDfile);
	  }
      }
    fprintf(isRDfile, ";;Action propositions\n\n");
    for(size_t t = 0; t < d; t++)
      {
        std::vector<GroundActionType>::iterator actiter = problem.actions.begin();
        std::vector<Proposition> action_smt_vars;
        std::vector<GroundActionType> added_actions; // This is to avoid encoding more than one action in the smt encoding. This is because project can lead to multiple copies of the same action
        for(size_t i = 0; i < problem.actions.size(); i++)
         {
           if (vectorFind(*actiter, added_actions) == -1)
             {
               int action_var_id = (int) (i + problem.dom().size());
               action_smt_vars.push_back(action_var_id);
               fprintf(isRDfile, "(declare-const v%d_%d Bool)\n", (int) action_var_id, (int)t);
               fprintf(isRDfile, "(assert (= v%d_%d", (int) action_var_id, (int)t);
               std::set<Proposition> effects = vectorToSet(customUnion(actiter->effects.Falses, actiter->effects.Trues));
               std::set<Proposition> untouched_vars = setMinus(problem.dom(), effects);
               fprintf(isRDfile, "(and");
               fprintf(isRDfile, "\n;;Untouched vars stay the same\n");
               frame_axiom(untouched_vars, t, isRDfile);
               // fprintf(isRDfile, "(ite");
               fprintf(isRDfile, ";;Preconditions");
               props_hold(vectorToSet(actiter->preconditions.Trues), t, isRDfile);
               fprintf(isRDfile, "(and ");
               fprintf(isRDfile, ";;Effects");
               props_hold(vectorToSet(actiter->effects.Trues), t+1, isRDfile);
               props_hold_not(vectorToSet(actiter->effects.Falses), t+1, isRDfile);
               fprintf(isRDfile, ")");
               // frame_axiom(effects, t, isRDfile);
               // fprintf(isRDfile, ")");
               fprintf(isRDfile, ")))\n");
               added_actions.push_back(*actiter);
             }
           actiter++;
         }
        mutex_vars(action_smt_vars, t, isRDfile);
        fprintf(isRDfile, "(declare-const T%d_%d Bool)\n", (int)t, (int)(t + 1));
        fprintf(isRDfile, "(assert (= T%d_%d (or", (int)t, (int)(t + 1));
        actiter = problem.actions.begin();
        for(size_t i = 0; i < action_smt_vars.size(); i++)
         {
           fprintf(isRDfile, " v%d_%d", action_smt_vars[i] , (int)t);
         }
        fprintf(isRDfile, ")))\n");
      }

    fprintf(isRDfile, ";;States from 0 to d are distinct\n\n");

    fprintf(isRDfile, "(assert (and");
    for(size_t t1 = 0; t1 <= d; t1++)
      {
        for(size_t t2 = t1 + 1; t2 <= d; t2++)
          {
            fprintf(isRDfile, "(not");
            eq_states(problem.dom(), t1, t2, isRDfile);
            fprintf(isRDfile, ")\n");
          }
      }
    fprintf(isRDfile, "))\n");

    // fprintf(isRDfile, ";;The last state occurs at least once\n\n");
    // fprintf(isRDfile, "(assert (or");
    // for(size_t t = 0; t <= d; t++)
    //   {
    //     eq_states(problem.dom(), t, d + 1, isRDfile);
    //   }
    // fprintf(isRDfile, "))\n");

    fprintf(isRDfile, ";;Every step has a transition\n\n");

    fprintf(isRDfile, "(assert (and");
    for(size_t t = 0; t < d; t++)
      {
        fprintf(isRDfile, " T%d_%d", (int)t, (int)t+1);
      }
    fprintf(isRDfile, "))\n");
    fprintf(isRDfile, "(check-sat)\n(get-model)\n");
    fclose(isRDfile);
    int status = -1;
    pid = wait(&status);
    system("rm isRDfile.smt2");
    FILE*rdOut = fopen("is_rd_out","r");
    char token[100];
    while(fscanf(rdOut, "%s", token) != EOF)
      {
        if(strstr(token, "unsat") != NULL)
          {
#ifdef debug
            cout << "RD lt" << d << endl;
#endif
            fclose(rdOut);
            return false;
          }
        if(strstr(token, "sat") != NULL)
          {
#ifdef debug
            cout << "RD geq" << d << endl;
#endif
            fclose(rdOut);
            return true;
          }
      }
    printf("Error: RD computation failed, exiting!!!\nErrorYices output is:\r\n");
    system("cat is_rd_out");
    system("cat is_rd_err");
    exit(-1);
  }

long long int StripsBound::getRDBound(long long int ubound)
  {
    long long int d = 0;
    for(d = 1; d <= ubound; d++)
      {
#ifdef debug
        printf("Current d: %lld\n", d);
#endif
        // if (d == 11)
        //   break;
        if (!isRD(d))
          {
            //There is not a simple path whose length is d+1
            break;
          }
      }
    //Number of actions in the longest distinct path
    return  d - 1;
  }
