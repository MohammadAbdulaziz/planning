#/bin/sh
rm *.pddl* *.sas* output
#CODE_BASE_DIR=/media/Data/Automated_reasoning/PhD_research/planning_bitbucket/planning/codeBase/planning/
CODE_BASE_DIR=../
FD_DIR=$CODE_BASE_DIR/FD/
FF_DIR=$CODE_BASE_DIR/FF/
NAUTY_DIR=$CODE_BASE_DIR/nauty25r9/
Z3_DIR=$CODE_BASE_DIR/z3/
export CODE_BASE_DIR
export FD_DIR
export FF_DIR
export NAUTY_DIR
export Z3_DIR

if [[ $1 = "" ]]; then
  echo "Usage: ./run.sh domain problem -{Malgo | Nalgo | Salgo | Balgo} -{TD_ARB | -RD | RD.TDgt2 | RD.TDgt2.SSle50bound}"
fi

if [[ $2 = "" ]]; then
  echo "Usage: ./run.sh domain problem -{Malgo | Nalgo | Salgo | Balgo} -{TD_ARB | -RD | RD.TDgt2 | RD.TDgt2.SSle50bound}"
fi

if [[ $3 = "" ]]; then
  echo "Usage: ./run.sh domain problem -{Malgo | Nalgo | Salgo | Balgo} -{TD_ARB | -RD | RD.TDgt2 | RD.TDgt2.SSle50bound}"
fi

if [[ $4 = "" ]]; then
  echo "Usage: ./run.sh domain problem -{Malgo | Nalgo | Salgo | Balgo} -{TD_ARB | -RD | RD.TDgt2 | RD.TDgt2.SSle50bound}"
fi

# noSASSCCs = no two assignments of a sinlge SAS+ var will be in different dependency graph SCC, i.e. every SAS+ variable is contained in one SCC

$FD_DIR/src/translate/translate.py $1 $2 > /dev/null
#$FD_DIR/src/translate/translate.py domProb.pddl factProb.pddl > /dev/null
$FD_DIR/src/preprocess/preprocess < output.sas > /dev/null
#valgrind --db-attach=yes --main-stacksize=83886080 --leak-check=full ./a.out 10 -noSASSCCs $3 < output
# if [[ $3 = Malgo ]]; then
#     Nalgo=`$CODE_BASE_DIR/stripsBound/a.out 1000 -noSASSCCs -Nalgo < output`
#     Malgo=`$CODE_BASE_DIR/stripsBound/a.out 1000 -noSASSCCs $3 < output`
# else
#     Nalgo=`$CODE_BASE_DIR/stripsBound/a.out 1000 -noSASSCCs $3 < output`
# fi

$CODE_BASE_DIR/stripsBound/bound 1000 -noSASSCCs $3 $4 < output
