#include "../../utils/util.h"
#include <src/preprocess/helper_functions.h>
#include "stripsBound.h"

#define experiments

int max_atom_dom_size = 0;
int max_atom_actions_size = 0;
int search_size = 0;
int number_state_vars = 0;
int number_actions = 0;
int DAGs = 0;
int SAS_VARs = 0;
int DAG_size = 1;
double bound_computation_time = 0;
double symmetry_compuation_time = 0;

void printDAGProportion(StripsProblem problem)
  {
    for(size_t i = 0 ; i < problem.stateGraphs.size(); i++)
      {        
        if( problem.isStateGraphAcyclic(i))
          {
            DAGs++;
            //printf("DAG %d size is %d\r\n", i, num_vertices(problem.stateGraphs[i]));
            DAG_size = DAG_size * num_vertices(problem.stateGraphs[i]);
          }
      }
    number_state_vars = problem.variables.size();
    number_actions = problem.actions.size();
    SAS_VARs = (int)problem.stateGraphs.size();
  }

int assign_algorithm(char* arg)
  {
    if(strcmp(arg, "-Nalgo") == 0)
      {
        return NALGO;
      }
    else if(strcmp(arg, "-Salgo") == 0)
      {
        return SALGO;
      }
    else if(strcmp(arg, "-Balgo") == 0)
      {
        return BALGO;
      }
    else if(strcmp(arg, "-Malgo") == 0)
      {
        return MALGO;
      }
    return -1;
  }

int assign_hardest_basecase_fun(char* arg)
  {
    if(strcmp(arg, "-TD_ARB") == 0)
      {
        return TD_ARB;
      }
    else if(strcmp(arg, "-RD") == 0)
      {
        return RD;
      }
    else if(strcmp(arg, "-RD.TDgt2") == 0)
      {
        return RD_TDgt2;
      }
    else if(strcmp(arg, "-RD.TDgt2.SSle50bound") == 0)
      {
        return RD_TDgt2_SSle50bound;
      }
    return -1;
  }

bool assign_SCC(char* arg)
  {
    if(strcmp(arg, "-noSASSCCs") == 0)
      {
        return true;
      }
    if(strcmp(arg, "-SASSCCs") == 0)
      {
        return false;
      }
    return false;
  }

int main(int argc, char** argv)
  {
    bool metric;
    vector<Variable> internal_variables;
    vector<Variable *> variables;
    vector<MutexGroup> mutexes;
    State initial_state;
    vector<std::pair<Variable *, int> > goals;
    vector<Operator> operators;
    vector<Axiom> axioms;
    clock_t start, end;
    start = clock();
    read_preprocessed_problem_description(cin,
                                          metric,
 					  internal_variables,
    					  variables,
					  mutexes,
					  initial_state,
					  goals,
					  operators,
					  axioms);
    char*name = (char*)"Prob";


    // std::vector<DomainTransitionGraph> transition_graphs;
    // CausalGraph cg(variables, operators, axioms, goals);
    // const std::vector<Variable *> &ordering = cg.get_variable_ordering();
    // build_DTGs(ordering, operators, axioms, transition_graphs);
    // printf("Computed %d transition graphs\r\n", (int)transition_graphs.size());


    //StripsProblem prob(variables, initial_state, goals, operators, mutexes, name, axioms);
    //prob.writePorblemPDDL();
    // printf("num goals = %d\r\n", (int)goals.size());
    // StripsProblem prob(variables, initial_state, goals, operators, mutexes, name, axioms, true);
    // prob.writePorblemPDDL();
    // exit(-1);
    bool noSASSCCs = false;
    int algorithm = -1;
    int basecase_fun = -1;
    for( int i = 2; i < argc; i++)
      {
        if(algorithm == -1)
          {
            algorithm = assign_algorithm(argv[i]);
          }
        if(basecase_fun == -1)
          {
            basecase_fun = assign_hardest_basecase_fun(argv[i]);
          }
        if(noSASSCCs == false)
          {
            noSASSCCs = assign_SCC(argv[i]);
          }
      }
    StripsBound boundingTool(variables, initial_state, goals, operators, mutexes, name, axioms, strtol(argv[1],NULL,10), noSASSCCs, algorithm, basecase_fun);
    // printf("%lld\n", boundingTool.getRDBound(6));
    // exit(-1);
    printDAGProportion(boundingTool.problem);
#ifdef experiments
    printf("%lld ", boundingTool.computeBound());
#else
    printf("Bound = %lld\r\n", boundingTool.computeBound());
#endif
    end = clock();
    bound_computation_time = ((double)(end - start))/CLOCKS_PER_SEC;
#ifdef experiments
    printf("%f ", bound_computation_time);
    //printf("%f ", symmetry_computation_time);
    printf("%d ", number_state_vars);
    printf("%d ", number_actions);
    printf("%d %d ", DAGs, SAS_VARs);
    if(DAG_size > 0)
      printf("%d ", DAG_size);
    else
      printf("%d ", std::numeric_limits<int>::max());
    printf("%d ", search_size);
    printf("%d ", max_atom_dom_size);
    printf("%d \r\n", max_atom_actions_size);
#else
    printf("bound_computation_time = %f\r\n", bound_computation_time);
    //printf("symmetry_computation_time = %f ", symmetry_computation_time);
    printf("number_state_vars = %d\r\n", number_state_vars);
    printf("number_actions = %d\r\n", number_actions);
    printf("DAGs/SAS_VARs = %d/%d\r\n", DAGs, SAS_VARs);
    if(DAG_size > 0)
      printf("SAS_DAGs_sspace_size = %d\r\n", DAG_size);
    else
      printf("SAS_DAGs_sspace_size = %d\r\n", std::numeric_limits<int>::max());
    printf("search_size = %d\r\n", search_size);
    printf("max_atom_dom_size = %d\r\n", max_atom_dom_size);
    printf("max_atom_actions_size = %d\r\n", max_atom_actions_size);
#endif
    //StripsBound boundingTool((const StripsProblem&) prob);

    // //StripsBound boundingTool(variables, initial_state, goals, operators, mutexes, name, axioms);
  }

