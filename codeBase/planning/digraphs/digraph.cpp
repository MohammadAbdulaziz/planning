#include "digraph.h"
#include "../../utils/util.h"
#include <tuple>
//#define debug

void printDigraph(DigraphT digraph)
  {

    DigraphT::edge_iterator eit, eend;
    std::tie(eit, eend) = boost::edges(digraph);
    auto printEdge = std::ostream_iterator<DigraphT::edge_descriptor>{std::cout, "\n"};
    std::copy(eit, eend, printEdge);
  }

template<typename typeName> void printDigraph(typeName digraph)
  {
    QuotientDigraphT::edge_iterator eit, eend;
    std::tie(eit, eend) = boost::edges(digraph);
#ifdef debug
    std::cout << "Edges are:\r\n";
    auto printEdge = std::ostream_iterator<QuotientDigraphT::edge_descriptor>{std::cout, "\n"};
    std::copy(eit, eend, printEdge);
#endif
  }

QuotientDigraphT computeQuotientDigraph(std::vector<int>vertex_to_component, DigraphT digraph)
  {
#ifdef debug
    printf("Computing (Graph/Components)\r\n");
#endif
    size_t n_components = (vectorToSet(vertex_to_component)).size();
#ifdef debug
    printf("#Components=%d\r\n", (int) n_components);
#endif
    boost::graph_traits<QuotientDigraphT>::vertex_descriptor v;
    QuotientDigraphT quotDigraph;
    std::vector<int> emptyVec;
    std::vector <std::vector <int> > components(n_components);
    for(size_t i = 0; i < vertex_to_component.size(); i++)
      {
        components[vertex_to_component[i]].push_back(i);
      }
    for(size_t i = 0 ; i < n_components ; i ++)
      {
        v = boost::add_vertex(quotDigraph);
        boost::property_map<QuotientDigraphT, component_t>::type
              vtxLabel = get(component_t(), quotDigraph);
        vtxLabel[v] = components[i];
#ifdef debug
        std::cout << "Component " << i << " members are:\r\n";
        std::copy(vtxLabel[v].begin(), vtxLabel[v].end(), std::ostream_iterator<int>{std::cout, "\n"});
#endif
      }
    for(size_t i = 0 ; i < num_vertices(digraph) ; i ++)
      {
        DigraphT::out_edge_iterator eit, eend;
        std::tie(eit, eend) = boost::out_edges(i, digraph);
        auto addQuotientEdge = [&quotDigraph, digraph, vertex_to_component, i](DigraphT::edge_descriptor it)
                 {
                   size_t j = boost::target(it, digraph);
                   if(vertex_to_component[j] != vertex_to_component[i])
                   //Lifted dep graph has edges between different SCCs only
                     boost::add_edge(vertex_to_component[i], vertex_to_component[j], quotDigraph);
                 };
        std::for_each(eit, eend, addQuotientEdge);
      }
#ifdef debug
    printf("Quotient graph constructed with %d vertices and %d edges\r\n", (int)num_vertices(quotDigraph), (int)num_edges(quotDigraph));
#endif
    return quotDigraph;
  }

int S(std::vector<int> &Ss, boost::graph_traits<QuotientDigraphT>::vertex_descriptor v, QuotientDigraphT quotDigraph)
  {
    boost::property_map<QuotientDigraphT, component_t>::type
          vtxLabel = get(component_t(), quotDigraph);
    DigraphT::out_edge_iterator eit, eend;
    std::tie(eit, eend) = boost::out_edges(v, quotDigraph);
    if (eit == eend)
       return vtxLabel[v].size() - 1;
#ifdef debug
    std::cout << "Computing S for SCC " << v << std::endl;
#endif
    if(Ss[v] != -100)
      {
#ifdef debug
    std::cout << "S for SCC " << v << " already computed" << std::endl;
#endif
        return Ss[v];
      }  
    auto computeSForChild = [quotDigraph, &Ss](DigraphT::edge_descriptor it)
             {
               boost::graph_traits<QuotientDigraphT>::vertex_descriptor u = boost::target(it, quotDigraph);
               return S(Ss, u, quotDigraph);
             };
    std::vector<int> childrenComponentSizes(std::distance(eit, eend));
    std::transform(eit, eend, childrenComponentSizes.begin(), computeSForChild);
#ifdef debug
    std::cout << "Children component sizes for vtx " << v << " are: ";
    std::copy(childrenComponentSizes.begin(), childrenComponentSizes.end(), std::ostream_iterator<int>{std::cout, "\n"});
#endif
    Ss[v] = vtxLabel[v].size() + (*std::max_element(childrenComponentSizes.begin(), childrenComponentSizes.end()));
    return Ss[v];
  }

int computeTD(DigraphT digraph)
  {
    std::vector<int>vertex_to_component = std::vector<int>(num_vertices(digraph));
    int num = 
         strong_components(digraph, 
                          make_iterator_property_map(
                          vertex_to_component.begin(),
                          get(boost::vertex_index,
                              digraph),
                          vertex_to_component[0]));
    QuotientDigraphT quotDigraph = computeQuotientDigraph(vertex_to_component, digraph);
#ifdef debug
    printDigraph(digraph);
    printDigraph(quotDigraph);
#endif
    QuotientDigraphT::vertex_iterator vit, vend;
    std::tie(vit, vend) = boost::vertices(quotDigraph);
    std::vector<int> Ss(num_vertices(quotDigraph), -100);
    auto computeSForVtx = [quotDigraph, &Ss](QuotientDigraphT::vertex_descriptor v)
             {
               return S(Ss, v, quotDigraph);
             };
    std::transform(vit, vend, Ss.begin(), computeSForVtx);
    int Smax = (*std::max_element(Ss.begin(), Ss.end()));
#ifdef debug
    std::cout << "Smax = " << Smax << std::endl;    
#endif
    return Smax;
  }


// int main()
//   {
//     DigraphT digraph;
//     boost::add_edge((size_t)0, (size_t)1, digraph);
//     boost::add_edge((size_t)1, (size_t)2, digraph);
//     boost::add_edge((size_t)2, (size_t)0, digraph);
//     boost::add_edge((size_t)2, (size_t)3, digraph);
//     std::vector<int>vertex_to_component = std::vector<int>(num_vertices(digraph));
//     int num = 
//          strong_components(digraph, 
//                           make_iterator_property_map(
//                           vertex_to_component.begin(),
//                           get(boost::vertex_index,
//                               digraph),
//                           vertex_to_component[0]));
//     QuotientDigraphT quotDigraph = computeQuotientDigraph(vertex_to_component, digraph);
//     std::cout << computeTD(digraph);
//   }
