#ifndef DIGRAPH_H
#define DIGRAPH_H

#include <boost/graph/graphviz.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/config.hpp>
#include <boost/graph/labeled_graph.hpp>

  typedef boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS > DigraphT;
  struct component_t { typedef boost::vertex_property_tag kind; };
  typedef boost::property<component_t, std::vector<int> > QuotientGraphLabelProp;
  typedef boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS, QuotientGraphLabelProp> QuotientDigraphT;
  /* typedef boost::labeled_graph<DigraphT, std::vector<int>> QuotientDigraphT; */
  QuotientDigraphT computeQuotientDigraph(std::vector<int>vertex_to_component, DigraphT digraph);
  int computeTD(DigraphT digraph);

#endif
