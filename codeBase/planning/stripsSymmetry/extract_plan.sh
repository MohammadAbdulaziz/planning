#!/bin/bash

IFS=$'\r\n'
found=0
while read line; do
  if [[ "$line" == *"Concrete problem plan:"* ]]; then
      found=1
  fi
  if [[ "$found" == "1" ]]; then
      if [[ "$line" == *"Current"* ]]; then
	  act_id=`echo $line | gawk '{print $4}'`
          act=`grep -w $act_id ActionIDToName | gawk '{$1=""; print $0}'`
          echo \($act\)
      fi
  fi
done < $1
