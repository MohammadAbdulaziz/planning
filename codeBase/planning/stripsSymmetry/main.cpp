#include "../../utils/util.h"
#include <src/preprocess/helper_functions.h>
#include "stripsSymmetry.h"

#include <src/preprocess/state.h>
#include <src/preprocess/mutex_group.h>
#include <src/preprocess/axiom.h>
#include <src/preprocess/operator.h>
#include <src/preprocess/variable.h>
#include <src/preprocess/successor_generator.h>
#include <src/preprocess/causal_graph.h>

class State;
class MutexGroup;
class Operator;
class Axiom;

void planByInstantiatingQuotientPlan(StripsSymmetry& symTool)
{
  std::set< std::map<int,int> >::iterator instSetIter = symTool.instantiationSet.begin();
  std::vector<int> quotientPlan = symTool.quotientProblem.plan;
  symTool.problem.plan.clear();
  for(int i = 0 ; i < (int)symTool.instantiationSet.size() ; i++)
    {
      for(int j = 0 ; j < (int)quotientPlan.size() ; j++)
        {
           
          int currentAction = symTool.problem.getActionIndex(symTool.quotientProblem.actions[quotientPlan[j]].image(*instSetIter));
          //printf("Quotient action ID: %d\r\n", symTool.quotientProblem.getActionIndex(symTool.quotientProblem.actions[quotientPlan[j]]));
          if(currentAction == -1)
            {
	      //#ifdef debug
              printf("Exiting!!\r\n");
              exit(-1);
	      //printSet(symTool.action_orbits[846]);
	      //#endif
            }
          symTool.problem.plan.push_back(currentAction);
          printf("Current action is %d\r\n", currentAction);
        }
      instSetIter++;
    }
}

void testInstantiation(StripsSymmetry& symmTool)
{
  std::map<int,int> tempInst;
  std::set<int> actionOrbits;
  for(int i = 0 ; i < (int)symmTool.action_orbits.size() ; i++)
    actionOrbits.insert(i);
  //tempInst = symmTool.GetInstantiationsRecurcisve(tempInst, actionOrbits);
  //symmTool.getSortedActionOrbits();
  std::vector<int> tempOrbit;
  for(int i = 0 ; i < (int)symmTool.action_orbits.size() ; i++)
    symmTool.sorted_action_orbits.push_back(tempOrbit);

  while(symmTool.primaryCover.size() != 0)
    {
      printf("Primary cover size: %d\r\n", (int)symmTool.primaryCover.size());
      tempInst.clear();
      //printf("Covering var %d from orbit %d\r\n", *symmTool.primaryCover.begin(), symmTool.var_to_orbit_map[*symmTool.primaryCover.begin()]);
      tempInst[symmTool.var_to_orbit_map[*symmTool.primaryCover.begin()]] = *symmTool.primaryCover.begin();
      //symmTool.primaryCover.erase(symmTool.primaryCover.begin());
      tempInst = symmTool.GetInstantiationsRecurcisveWithSortedActions(tempInst, actionOrbits);
      //printf("xx:%d\r\n",(int)tempInst.size());//customInter(symmTool.primaryCover,
      //coDom(tempInst)).size());
      symmTool.primaryCover = setMinus(symmTool.primaryCover, coDom(tempInst));
      // cout << "inst(1) = " << tempInst[1] << endl;
      // cout << "Covered vars: ";
      // cout << coDom(tempInst);
      // cout << endl;
      //printf("xx:%d\r\n",(int)coDom(tempInst).size());
    }

  if(tempInst.size() == 0)
    {
      printf("No instantiations possible!!!!\r\n");
    }
  else
    {
      printf("Instantiation is possible of size %d!!!!\r\n", (int)tempInst.size());//(int)symmTool.var_orbits.size());
      if(subset(customInter(symmTool.primaryCover, getActionsDomain(symmTool.problem.actions)), coDom(tempInst)))
        {
          printf("The witness instantiation is a covering instantiation\r\n");
          symmTool.instantiationSet.insert(tempInst);
        }
      else
        printf("The witness instantiation does not cover %d variables from the primary cover!!\r\n",
                 (int)setMinus(customInter(symmTool.primaryCover,
                                           getActionsDomain(symmTool.problem.actions)), coDom(tempInst)).size());
    }
}

void testInstantiation2(StripsSymmetry symmTool)
{
  std::map<int,int> tempInst;
  std::set<int> actionOrbits;
  for(int i = 0 ; i < (int)symmTool.action_orbits.size() ; i++)
    actionOrbits.insert(i);
  symmTool.GetAllInstantiationsRecurcisve(tempInst, actionOrbits);
  printf("Set of all instantiations is <= %d\r\n", (int)symmTool.allInstantiations.size());
}



int main()
{
  bool metric;
  vector<Variable> internal_variables;
  vector<Variable *> variables;
  vector<MutexGroup> mutexes;
  State initial_state;
  vector<std::pair<Variable *, int> > goals;
  vector<Operator> operators;
  vector<Axiom> axioms;
  read_preprocessed_problem_description(cin,
                                        metric,
					internal_variables,
					variables,
					mutexes,
					initial_state,
					goals,
					operators,
					axioms);
  char*name = (char*)"Prob";
  StripsProblem prob(variables, initial_state, goals, operators, mutexes, name, axioms);

  prob.writePorblemPDDL();
  //prob.searchPlan2(); // This searches with FD
  //prob.searchPlan();  // This searches with FF

  prob.validatePlan();

  StripsSymmetry symmTool(prob);

  symmTool.initColours();
  symmTool.PrintStripsProblemDescriptionGraph();
  symmTool.getOrbits();

  symmTool.getSameActionConstraints();
  symmTool.getOrbitsColouringGraphs();
  symmTool.colourOrbitsVars();
  
  if(symmTool.varColours.size() != symmTool.var_orbits.size())
    {
      symmTool.PrintStripsProblemDescriptionGraph();
      symmTool.getOrbits();
    }

  symmTool.GetQuotientProblem();
  //testing instantiation conflicts  
  // symmTool.getPairwisCoflictingActionOrbits();
  //testInstantiation(symmTool);  
  //testInstantiation2(symmTool);
  //symmTool.GetInstantiations();
  // symmTool.GetCoveringInstantiationsRecursive();
  symmTool.GetCoveringInstantiationsSMT();

  if(symmTool.instantiationSet.size() == 0)
    symmTool.GetInstantiationsPriorityQueue();


  symmTool.getCommonResources();

  std::set<int >::iterator common_iter = symmTool.commonResourcesOrbits.begin();
  for(size_t i = 0 ; i < symmTool.commonResourcesOrbits.size() ; i ++)
    {
      Proposition tempProp;
      tempProp = *common_iter;
      if(symmTool.quotientProblem.initial_state_vars.find(tempProp) != symmTool.quotientProblem.initial_state_vars.end())
        {
          printf("Adding orbit %d to the quotient goal\r\n", tempProp);
          symmTool.quotientProblem.goals.insert(tempProp);          
        }
      //This is removed because there are no negative literals required as preconds
      /* else  */
      /*   fprintf(facts, "(not (v%d)) ", *common_iter); */
      common_iter++;
    }

  symmTool.quotientProblem.writePorblemPDDL();
  printf("Quotient problem plan:\r\n");
  symmTool.quotientProblem.searchPlan();
  //symmTool.quotientProblem.searchPlan2();
  symmTool.quotientProblem.validatePlan();
  FILE* expresult = fopen("stats", "a");
  fprintf(expresult, "%d %d %d %d %d %d %d %d %d %d %d %f %f %f %f %f %f %f\r\n",
  	 (int)prob.actions.size(),
  	 (int)symmTool.quotientProblem.actions.size(),
  	 (int)prob.variables.size(),
  	 (int)symmTool.var_orbits.size(),
  	 prob.genStates, 
  	 symmTool.quotientProblem.genStates,
  	 prob.expanded, 
  	 symmTool.quotientProblem.expanded,
  	 (int)prob.plan.size(),
  	 (int)symmTool.instantiationSet.size(),
  	 (int)symmTool.quotientProblem.plan.size(),
  	 prob.searchTime,
  	 prob.translationTime,
  	 symmTool.quotientProblem.searchTime,
  	 symmTool.quotientProblem.translationTime,
  	 symmTool.symmDetTime,
  	 symmTool.colouringTime,
  	 symmTool.instFindTime);
  fclose(expresult);
  printf("Concrete problem plan:\r\n");
  planByInstantiatingQuotientPlan(symmTool);
  symmTool.problem.validatePlan();
}
