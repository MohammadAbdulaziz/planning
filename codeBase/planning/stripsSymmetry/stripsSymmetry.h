#include "../stripsProblem/stripsProblem.h"
#include <stdlib.h>
#include <time.h>
#include <sys/stat.h>

#ifndef STRIPS_SYMM
#define STRIPS_SYMM

class StripsSymmetry{
 public:
  StripsProblem problem, quotientProblem;

  std::vector<std::pair<int, int> > varColouringGraph;//A set representing the relation R of guranteed instantiation
  std::vector<std::vector<std::pair<int, int> > >orbitsColouringGraphs;//The relation R within orbits
  std::vector< std::set<int> > varColours;//A partition of vars showing what can be permuted with what
  std::vector< std::set<int> > action_orbits;
  std::vector< std::vector<int> > sorted_action_orbits;//based on primary cover
  std::vector< std::set<int> > var_orbits;
  std::map<int, int> action_to_orbit_map;
  std::map<int, int> var_to_orbit_map;
  std::set< std::map<int,int> > instantiationSet;
  std::set< std::map<int,int> > allInstantiations;
  std::set<int> commonResourcesOrbits;
  std::set<int> commonVarOrbits;
  std::vector<int> action_to_primary_cover_score;
  std::vector<int> action_to_secondary_cover_score;

  std::vector<std::set<int> > primary_cover_score_to_actions;

  std::vector<std::set<int> > secondary_cover_score_to_actions;

  std::set<int> primaryCover;
  std::set<int> secondaryCover;


  StripsSymmetry(const StripsProblem& problemIn):problem (problemIn){ 
    std::set< Proposition >::iterator goal_iter = problem.goals.begin();
    for(int i = 0 ; i < (int)problem.goals.size() ; i++)
      {
        primaryCover.insert(*goal_iter);
        goal_iter++;
      }
    printf("Initial size of primary cover %d.\r\n", (int)primaryCover.size());
    /* std::cout << primaryCover; */
    /* std::cout << std::endl; */
    for(int i = 0 ; i < (int)problem.variables.size() ; i++)
      secondaryCover.insert(i);
    printf("Initial size of secondary cover %d.\r\n", (int)secondaryCover.size());
    printf("Number of variable orbits is %d\r\n", (int)var_orbits.size());
    InitPrimaryScoreStructure();
    InitSecondaryScoreStructure();
}
  void PrintStripsProblemDescriptionGraph();
  void GetQuotientProblem();
  void getOrbits();


  void getSameActionConstraints();
  void colourVars();
  void initColours();
  void getOrbitsColouringGraphs();
  void colourOrbitsVars();
  void getCommonResources();
  void GetInstantiations();
  std::vector<GroundActionType> GetConsistentActions
    (std::map<int,int> currentInst,
     std::map<int, int> var_to_orbit_map,
     std::map<int, GroundActionType> actionSet);
  std::vector<int> GetConsistentActions2
         (std::map<int,int> currentInst,
          std::map<int, int> var_to_orbit_map,
          std::map<int, GroundActionType> actionSet);
  std::set<int> GetConsistentActions3
         (std::map<int,int> currentInst,
          std::map<int, int> var_to_orbit_map,
          std::set<int> actionSet,
          std::vector<GroundActionType> actions);
  bool ActionConsistentActionOrbits
	  (GroundActionType action,
	   std::vector<GroundActionType> actionSet,
	   std::set<int> actionOrbits);
  bool ActionConsistentAction
	  (GroundActionType action1,
	   GroundActionType action2);
  bool instantiationSanityCheck();
  bool orbitsSanityCheck();
  //This shouldnt be here
  void planByInstantiatingQuotientPlan();
  int AugmentInstantiation
         (std::map<int,int> &currentInst,
          GroundActionType action,
          std::map<int, int> var_to_orbit_map);
  bool ActionConsistentInstantiation
         (GroundActionType action,
          std::map<int,int> currentInst,
          std::map<int, int> var_to_orbit_map);
  std::map<int,int> GetInstantiationsRecurcisve(std::map<int,int> currentInst, 
                                                std::set<int> remainingActionOrbits);
  void GetAllInstantiationsRecurcisve(std::map<int,int> currentInst, std::set<int> remainingActionOrbits);
  std::vector<GroundActionType> GetConsistentActions
         (std::map<int,int> currentInst,
          std::map<int, int> var_to_orbit_map,
          std::set<int> actionSet,
          std::vector<GroundActionType> actions);
  std::vector<std::pair<int, int> > getPairwisCoflictingActionOrbits();
  void GetInstantiationsPriorityQueue();
  void GetInstantiationsPriorityQueue2();
  void InitPrimaryScoreStructure();
  void updatePrimaryScoreStructure(std::set<int> dom);
  void InitSecondaryScoreStructure();
  void updateSecondaryScoreStructure(std::set<int> dom);
  int GetBestConsistentActionPriorityQueue(std::set<int> actionSet,
			       std::map<int,int> currentInst,
                               std::vector<std::set<int> > primaryScoreQueues,
                               std::vector<std::set<int> > secondaryScoreQueues);
  void getSortedActionOrbits();//sorted according to primary cover score
  void sortOrbit(int i);
  std::map<int,int> GetInstantiationsRecurcisveWithSortedActions(std::map<int,int> currentInst,
                                                                 std::set<int> remainingActionOrbits);
  void GetCoveringInstantiationsRecursive();
  double symmDetTime;//time to compute the automorphism group
  double colouringTime;//time to compute the colouring
  double instFindTime;//time to find the instantiations
  std::map<int,int> GetInstantiationSMT(std::map<int,int> currentInst, std::set<int> remainingActionOrbits, double &inst_time);
  void GetCoveringInstantiationsSMT();
  bool isomorphic(StripsProblem other);
};
#endif
