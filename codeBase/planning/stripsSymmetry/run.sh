#/bin/sh
rm *.pddl* *.sas* 
CODE_BASE_DIR=../
#CODE_BASE_DIR=~/bitBucket/planning/codeBase/planning/
FD_DIR=$CODE_BASE_DIR/FD/
FF_DIR=$CODE_BASE_DIR/FF/
NAUTY_DIR=$CODE_BASE_DIR/nauty25r9/
Z3_DIR=$CODE_BASE_DIR/z3/
export CODE_BASE_DIR
export FD_DIR
export FF_DIR
export NAUTY_DIR
export Z3_DIR
$FD_DIR/src/translate/translate.py $1 $2 >/dev/null
#$FD_DIR/src/preprocess/preprocess.py < output.sas >/dev/null
./extract_plan.sh <(./planWithQuotient < output.sas)
