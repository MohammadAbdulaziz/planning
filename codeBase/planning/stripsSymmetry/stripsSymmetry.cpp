#include "stripsSymmetry.h"
#include <iostream>
#include <sstream>
#include <string.h>

void StripsSymmetry::PrintStripsProblemDescriptionGraph()
  {
    std::cout << "Number of actions = " << problem.actions.size() << std::endl;
    std::cout << "Number of variables = " << problem.variables.size()  << std::endl;
    FILE* naut = fopen("nauty_in", "w");
    fprintf(naut, "At\r\n\r\n-a\r\n-m\r\nn=%d g\r\n", 2*(int)problem.actions.size() + 2*(int)problem.variables.size() + 2);
    std::cout << "Printing action edges"<< std::endl;
    //Printing action edges
    for(int i = 0; i < (int)problem.actions.size() ; i++)
      {
        //fprintf(naut, "%d\r\n", i);
        fprintf(naut, "%d:%d ", 2*i, 2*i + 1);
        //Printing action precondition edges: they must only be connected to even var nodes (positive literals)
        for(int j = 0; j < (int)problem.actions[i].preconditions.Trues.size() ; j++)
          {
            fprintf(naut, "%d ",  2 * (int)problem.actions.size() + 2 * problem.actions[i].preconditions.Trues[j]);
	  }
        fprintf(naut, ";\r\n");
        //Printing action add effect edges: they must only be connected to even var nodes (positive literals)
        fprintf(naut, "%d:", 2*i + 1);
        for(int j = 0; j < (int)problem.actions[i].effects.Trues.size() ; j++)
          {
            fprintf(naut, "%d ", 2 * (int)problem.actions.size() + 2 * problem.actions[i].effects.Trues[j]);
	  }
        //Printing action del effect edges: they must only be connected to odd var nodes (neg literals)
        for(int j = 0; j < (int)problem.actions[i].effects.Falses.size() ; j++)
          {
            fprintf(naut, "%d ", 2 * (int)problem.actions.size() + 2 * problem.actions[i].effects.Falses[j] + 1);
	  }
        fprintf(naut, ";\r\n");
        fflush(stdout);
      }
    std::cout << "Printing variable edges (only to variables)"<< std::endl;
    int i = 0;
    for(i = 0; i < (int)problem.variables.size() - 1 ; i++)
      {
        fprintf(naut, "%d:%d;\r\n", 2 * (int)problem.actions.size() + 2 * i, 2 * (int)problem.actions.size() + 2 * i + 1);
      }
    fprintf(naut, "%d:%d;\r\n", 2 * (int)problem.actions.size() + 2 * i, 2 * (int)problem.actions.size() + 2 * i + 1);
    std::cout << "Printing initial state edges"<< std::endl;
    fprintf(naut, "%d:", 2 * (int)problem.actions.size() + 2 * (int)problem.variables.size());
    std::set< Proposition >::iterator init_iter = problem.initial_state_vars.begin();
    for(i = 0; i < (int)problem.initial_state_vars.size(); i++)
      {
        fprintf(naut, "%d ", 2 * (int)problem.actions.size() + 2 * (*init_iter));
        init_iter++;
      }
    fprintf(naut, ";\r\n");
    std::cout << "Printing goal edges"<< std::endl;
    fprintf(naut, "%d:", 2 * (int)problem.actions.size() + 2 * (int)problem.variables.size() + 1);
    std::set< Proposition >::iterator goals_iter = problem.goals.begin();
    for(i = 0; i < (int)problem.goals.size(); i++)
      {
        fprintf(naut, "%d ", 2 * (int)problem.actions.size() + 2 * (*goals_iter));
        goals_iter++;
      }
    fprintf(naut, ".\r\n");
    fprintf(naut, "f=[");
    for(i = 0; i < 2 * (int)problem.actions.size() - 2 ; i = i + 2)
      fprintf(naut, "%d,", i);
    fprintf(naut, "%d|", i);
    for(i = 1; i < 2 * (int)problem.actions.size() - 1 ; i = i + 2)
      fprintf(naut, "%d,", i);
    fprintf(naut, "%d|", i);

    // for(i = 2 * problem.actions.size() ; i < 2 * problem.actions.size() + 2 * problem.variables.size() - 2 ; i = i + 2)
    //   fprintf(naut, "%d,", i);
    // fprintf(naut, "%d|", i);
    // for(i = 2 * problem.actions.size() + 1; i < 2 * problem.actions.size() + 2 * problem.variables.size() - 1 ; i = i + 2)
    //   fprintf(naut, "%d,", i);
    // fprintf(naut, "%d", i);

    for(i = 0 ; i < (int)varColours.size() - 1 ; i ++)
      {
    	std::set<int>::iterator colour_iter = varColours[i].begin();
        for(int j = 0 ; j < (int)varColours[i].size() - 1; j ++)
          {
            fprintf(naut, "%d,", 2 * (int)problem.actions.size() + 2*(*colour_iter));        
            colour_iter ++;
          }
        fprintf(naut, "%d| ", 2 * (int)problem.actions.size() + 2*(*colour_iter));
        colour_iter = varColours[i].begin();
        for(int j = 0 ; j < (int)varColours[i].size() - 1  ; j ++)
          {
            fprintf(naut, "%d,", 2 * (int)problem.actions.size() + 1 + 2*(*colour_iter));        
            colour_iter ++;
          }
        fprintf(naut, "%d| ", 2 * (int)problem.actions.size() + 1 + 2*(*colour_iter));
      }
    std::set<int>::iterator colour_iter = varColours[i].begin();
    for(int j = 0 ; j < (int)varColours[i].size() - 1; j ++)
      {
        fprintf(naut, "%d,", 2 * (int)problem.actions.size() + 2*(*colour_iter));        
        colour_iter ++;
      }
    fprintf(naut, "%d| ", 2 * (int)problem.actions.size() + 2*(*colour_iter));
    colour_iter = varColours[i].begin();
    for(int j = 0 ; j < (int)varColours[i].size() - 1  ; j ++)
      {
        fprintf(naut, "%d,", 2 * (int)problem.actions.size() + 1 + 2*(*colour_iter));        
        colour_iter ++;
      }
    fprintf(naut, "%d", 2 * (int)problem.actions.size() + 1 + 2*(*colour_iter));

    fprintf(naut, "] x o\r\n"); 
    fclose(naut);
  }


void StripsSymmetry::GetQuotientProblem()
  {
    char* tempName = (char*)"quotient";
    quotientProblem.name = (char*)malloc(strlen(tempName) + 1);
    strcpy(quotientProblem.name, tempName);
    std::set< int > addedActionQuotients;//to make sure the same quotient action is not added
    //vectorFind(i, adjacentCliqueSetsWithCut[0])
    for(int i = 0; i < (int)action_orbits.size() ; i++)
      {
        //printf("Adding quotient action %d to quotient problem\r\n", i);
        GroundActionType currentAction;
        for(int j = 0; j < (int)problem.actions[*(action_orbits[i].begin())].preconditions.Trues.size() ; j++)
        //We dont check whether the same orbit has been added before or not because if this happens no instantiations would be existing
          {
            currentAction.preconditions.Trues.push_back(var_to_orbit_map[problem.actions[*(action_orbits[i].begin())].preconditions.Trues[j]]);
          }
        for(int j = 0; j < (int)problem.actions[*(action_orbits[i].begin())].effects.Trues.size() ; j++)
          {
            currentAction.effects.Trues.push_back(var_to_orbit_map[problem.actions[*(action_orbits[i].begin())].effects.Trues[j]]);
          }
        for(int j = 0; j < (int)problem.actions[*(action_orbits[i].begin())].effects.Falses.size() ; j++)
          {
            currentAction.effects.Falses.push_back(var_to_orbit_map[problem.actions[*(action_orbits[i].begin())].effects.Falses[j]]);
          }
        quotientProblem.actions.push_back(currentAction);
        currentAction.preconditions.Trues.clear();
        currentAction.effects.Trues.clear();
        currentAction.effects.Falses.clear();
      }
    printf("Number of actions in the problem = %d\r\n", (int)problem.actions.size());
    printf("Number of action orbits added to the quotient problem = %d\r\n", (int)quotientProblem.actions.size());
    if(action_orbits.size() != quotientProblem.actions.size())
      {
	printf("Not all action orbits added to quotient problem, exit!!");
        exit(-1);
      }
    std::set< Proposition >::iterator init_iter = problem.initial_state_vars.begin();
    for(int i = 0; i < (int)problem.initial_state_vars.size(); i++)
      {
#ifdef debug
        printf("Adding orbit %d to which var %d belongs to the initial state of the quotient problem\r\n", var_to_orbit_map[2 * (init_iter->id)], (init_iter->id));
#endif
        quotientProblem.initial_state_vars.insert(var_to_orbit_map[(*init_iter)]);
        init_iter++;
      }
    printf("Number of var in the problem initial state = %d\r\n", (int)problem.initial_state_vars.size());
    printf("Number of var orbits added to the quotient problem initial state = %d\r\n", (int)quotientProblem.initial_state_vars.size());
    std::set< Proposition >::iterator goal_iter = problem.goals.begin();
    for(int i = 0; i < (int)problem.goals.size(); i++)
      {
#ifdef debug
        printf("Adding orbit %d to which var %d belongs to the goal state of the quotient problem\r\n", var_to_orbit_map[2 * (*goal_iter)], (*goal_iter));
#endif
        quotientProblem.goals.insert(var_to_orbit_map[(*goal_iter)]);
        goal_iter++;
      }
    
    printf("Number of var in the problem goal state = %d\r\n", (int)problem.goals.size());
    printf("Number of var orbits added to the quotient problem goal state = %d\r\n", (int)quotientProblem.goals.size());
    if(quotientProblem.actions.size() == problem.actions.size())
      {
        printf("No symmetries found, exiting!!\r\n");
        FILE* successFile = fopen("successFile","a");
        fprintf(successFile, "%d\r\n", -1);
        fclose(successFile);
        exit(1);
      }
    quotientProblem.domSize = (int)var_orbits.size() ; 
  }

void StripsSymmetry::getOrbits()
  {
    if(system("$NAUTY_DIR/dreadnaut <nauty_in > nauty_out") == -1)
      {
        printf("System command failed, exiting!!\r\n");
        exit(-1);
      }
    FILE* naut = fopen("nauty_out", "r");
    char token[100];
    fscanf(naut, "%s", token);
    char time_token[100];
    token[0] = '\0';
    while(strcmp(token,"seconds")!=0)
      {
        strcpy(time_token, token);
        fscanf(naut, "%s", token);
      }
    symmDetTime = strtod(time_token, NULL);
    action_orbits.clear();
    var_orbits.clear();
    std::set<int> current_orbit;
    char orbitType = 0; //0 init 1 var orbit 2 action orbit
    while(fscanf(naut, "%s", token)!=EOF)
      {
        if(token[0]!='(')//size of an orbit token
          {
            if(strstr(token,":")!=NULL)//This should never happen because consecutive vertices are always in a different colour
              {
                printf("Wrong orbit, exiting!!");
                exit(-1);
                char*subtoken;
                subtoken = strtok(token,":");
                printf("%s ", subtoken);
                int low_range = strtol(subtoken, NULL,10);
                subtoken = strtok(NULL,":");
                printf("%s ", subtoken);
                int high_range = strtol(subtoken, NULL,10);
                printf("%d:%d", low_range, high_range);
                //Add all the range to the current orbit
              }
            else//We are only taking even orbits because odd ones are isomorphic to them
              {
                int vtx_id = strtol(token, NULL,10);
                //Add this vtx to the current orbit
                if(orbitType == 0 && vtx_id <= 2*(int)problem.actions.size() - 1 && vtx_id % 2 == 0)
                  {
                    //printf("New action orbit\r\n");
                    orbitType = 2;
                    current_orbit.insert(vtx_id/2);
                    action_to_orbit_map[vtx_id/2] = action_orbits.size();
                  }
                else if(orbitType == 2 && vtx_id <= 2*(int)problem.actions.size() - 1 && vtx_id % 2 == 0)
                  {
                    current_orbit.insert(vtx_id/2);
                    action_to_orbit_map[vtx_id/2] = (int)action_orbits.size();
                  }
                else if(orbitType == 0 && vtx_id >= 2*(int)problem.actions.size() && vtx_id <= 2*(int)problem.actions.size() + 2*(int)problem.variables.size() - 1 && vtx_id % 2 == 0)
                  {
                    //printf("New var orbit\r\n");
                    orbitType = 1;
                    current_orbit.insert((vtx_id - 2 * problem.actions.size())/2);
                    var_to_orbit_map[(vtx_id - 2 * problem.actions.size())/2] = var_orbits.size();
                  }
                else if(orbitType == 1 && vtx_id >= 2*(int)problem.actions.size() && vtx_id <= 2*(int)problem.actions.size() + 2*(int)problem.variables.size() - 1 && vtx_id % 2 == 0)
                  {
                    current_orbit.insert((vtx_id - 2 * (int)problem.actions.size())/2);
                    var_to_orbit_map[(vtx_id - 2 * (int)problem.actions.size())/2] = (int)var_orbits.size();
                  }
                else if(vtx_id == 2*(int)problem.actions.size() + 2*(int)problem.variables.size() + 1 || vtx_id == 2*(int)problem.actions.size() + 2*(int)problem.variables.size())
                  {
                    //printf("Skipping initial state node and goal state node.\r\n");
                  }
                else if(vtx_id%2 != 0)
                  {
                    //printf("Skipping odd node.\r\n");
                  }
                else
                  {
                    printf("Orbit problem, exiting!!\r\n");
                    exit(-1);
                  }
                //printf("%d ", vtx_id);
              }
          }
        if(strstr(token,";") !=NULL)//end of current orbit
          {
            //printf("\nNext orbit\r\n");             
            //Store current orbit
            if(orbitType == 1)
              var_orbits.push_back(current_orbit);              
            else if(orbitType == 2)
              action_orbits.push_back(current_orbit);
            //Init new orbit
            current_orbit.clear();
            orbitType = 0;
          }
      }
    printf("Number of action orbits = %d\r\n", (int)action_orbits.size());
    printf("Number of variable orbits = %d\r\n", (int)var_orbits.size());
    fclose(naut);
  }

void StripsSymmetry::getSameActionConstraints()
{
  for(int i = 0 ; i < (int)problem.actions.size() ; i++)
    {
      for(int j = 0; j < (int)problem.actions[i].preconditions.Trues.size() ; j++)
        {
          for(int k = j + 1; k < (int)problem.actions[i].preconditions.Trues.size() ; k++)
            {
              varColouringGraph.push_back(std::pair<int,int> (problem.actions[i].preconditions.Trues[j], problem.actions[i].preconditions.Trues[k]));
            }
          for(int k = 0; k < (int)problem.actions[i].effects.Trues.size() ; k++)
            {
              varColouringGraph.push_back(std::pair<int,int> (problem.actions[i].preconditions.Trues[j], problem.actions[i].effects.Trues[k]));
            }
          for(int k = 0; k < (int)problem.actions[i].effects.Falses.size() ; k++)
            {
              varColouringGraph.push_back(std::pair<int,int>(problem.actions[i].preconditions.Trues[j], problem.actions[i].effects.Falses[k]));
            }
        }
      for(int j = 0; j < (int)problem.actions[i].effects.Trues.size() ; j++)
        {
          for(int k = j + 1; k < (int)problem.actions[i].effects.Trues.size() ; k++)
            {
              varColouringGraph.push_back(std::pair<int,int>(problem.actions[i].effects.Trues[j], problem.actions[i].effects.Trues[k]));
            }
          for(int k = 0; k < (int)problem.actions[i].effects.Falses.size() ; k++)
            {
              varColouringGraph.push_back(std::pair<int,int>(problem.actions[i].effects.Trues[j], problem.actions[i].effects.Falses[k]));
            }
        }
      for(int j = 0; j < (int)problem.actions[i].effects.Falses.size() ; j++)
        {
          for(int k = j + 1; k < (int)problem.actions[i].effects.Falses.size() ; k++)
            {
              varColouringGraph.push_back(std::pair<int,int>(problem.actions[i].effects.Falses[j], problem.actions[i].effects.Falses[k]));
            }
        } 
    }
  for(int i = 0 ; i < (int)problem.variables.size() ; i++)
    {
      for(int j = i + 1 ; j < (int)problem.variables.size() ; j++)
        {
          if(var_to_orbit_map[i] != var_to_orbit_map[j])
            varColouringGraph.push_back(std::pair<int,int>(i,j));
        }
    }
}

void StripsSymmetry::colourVars()
{
    for(int i = var_orbits.size() ; i < (int)problem.variables.size() ; i++)
    {
      printf("Trying to colour with %d colours\n", i);
      varColours = smtColour(problem.variables.size(), varColouringGraph, i);
      if(varColours.size() != 0)
        {
          printf("Found colouring with %d colours\n", i);
          break;
        }
    }
}

void StripsSymmetry::initColours()
{
  std::set<int> initColour;
  for(int i = 0 ; i < (int)problem.variables.size() ; i++)
    {
      initColour.insert(i);
    }
  varColours.push_back(initColour);
}

void StripsSymmetry::getOrbitsColouringGraphs()
{
  std::vector<std::pair<int, int> >currentOrbitColouringGraph;
  for(int i = 0 ; i < (int)var_orbits.size() ; i ++)
    {
      orbitsColouringGraphs.push_back(currentOrbitColouringGraph);
    }
  for(int j = 0 ;  j < (int)varColouringGraph.size() ; j++)
    {
      int orbitFst, orbitSnd;
      orbitFst = var_to_orbit_map[varColouringGraph[j].first];
      orbitSnd = var_to_orbit_map[varColouringGraph[j].second];
      if(orbitFst == orbitSnd && varColouringGraph[j].first != varColouringGraph[j].second)
        {
          if(vectorFind(std::pair<int,int> (varColouringGraph[j].first, varColouringGraph[j].second),
                        orbitsColouringGraphs[orbitFst])  == -1 &&
             vectorFind(std::pair<int,int> (varColouringGraph[j].second, varColouringGraph[j].first),
                        orbitsColouringGraphs[orbitFst])  == -1)
            {  
              orbitsColouringGraphs[orbitFst].
                          push_back(std::pair<int,int> (varColouringGraph[j].first, varColouringGraph[j].second));
              cout << "adding (" << varColouringGraph[j].first << "," << varColouringGraph[j].second << ")" << std::endl;
            }
        }
    }
}

void StripsSymmetry::colourOrbitsVars()
  {
    std::vector<std::set<int> > currentOrbitColouring;
    varColours.clear();
    colouringTime = 0.0;
    for(int j = 0 ; j < (int)var_orbits.size() ; j++)
      {
        if((int)var_orbits[j].size() > 1 && orbitsColouringGraphs[j].size() != 0)
	  {
           for(int i = 1 ; i <= (int)var_orbits[j].size() ; i++)
             {
               printf("Trying to colour orbit %d (%d constraints) with %d colours\n", j, (int)orbitsColouringGraphs[j].size(), i);
               std::cout << var_orbits[j];
               std::cout << std::endl;
               double current_orbit_colouring_time;
               currentOrbitColouring = smtColour(var_orbits[j], orbitsColouringGraphs[j], i, current_orbit_colouring_time);
               if(currentOrbitColouring.size() != 0)
                 {
                   printf("Found colouring of orbit %d with %d colours\n", j, i);
                   varColours = vectorConcat(varColours, currentOrbitColouring);
                   colouringTime += current_orbit_colouring_time;
                   break;
                 }
              }
            if(currentOrbitColouring.size() == 0)
              {
                printf("Failed to colour orbit %d (of size %d), exiting!!!\r\n", j, (int)var_orbits[j].size());
                exit(1);
              }
	  }
        else
          {
	    std::vector<std::set<int> > temp;
            temp.push_back(var_orbits[j]);
            varColours = vectorConcat(varColours, temp);
          }
      }
  }


void StripsSymmetry::getCommonResources()
{
//std::set< std::map<int,int> >
  std::set< std::map<int,int> >::iterator instSetIter = instantiationSet.begin();
  std::vector<std::set<int> > instSetCodomains;
  for(int i = 0 ; i < (int)instantiationSet.size() ; i++)
    {
      std::set<int> currentCodom;
      currentCodom.clear();
      std::map<int,int>::const_iterator instIter = (*instSetIter).begin();
      for(int j = 0 ; j < (int)(*instSetIter).size() ; j++)
        {
          currentCodom.insert(instIter->second);
          instIter++;
        }
      instSetCodomains.push_back(currentCodom);
      instSetIter++;
    }
  std::set<int> bigUnionOfPairWiseInter;
  for(int i = 0 ; i < (int)instSetCodomains.size() ; i++)
    {
      for(int j = i + 1 ; j < (int)instSetCodomains.size() ; j++)
        {
          //printf("Getting inter of instantiation %d and %d\r\n", i, j);
          bigUnionOfPairWiseInter = customUnion(bigUnionOfPairWiseInter, customInter(instSetCodomains[i], instSetCodomains[j]));
        }
    }  
  printf("The number of common resources is %d and the size of common resource orbits is %d\r\n", (int)bigUnionOfPairWiseInter.size(), (int)getVarsOrbits(bigUnionOfPairWiseInter, var_to_orbit_map).size());
  std::set<int>::iterator bigUnionOfPairWiseInter_iter = bigUnionOfPairWiseInter.begin();
  printf("Common resources are ");
  for(int i = 0 ; i < (int)bigUnionOfPairWiseInter.size() ; i++)
    {
      //printf("%d /*(%s=%d)*/ belonging to orbit %d of size %d\r\n",
      // printf("%d belonging to orbit %d of size %d\r\n",
      //        *bigUnionOfPairWiseInter_iter,
      //        // getFluentNameAndAss(*bigUnionOfPairWiseInter_iter).first,
      //        // getFluentNameAndAss(*bigUnionOfPairWiseInter_iter).second,
      //        var_to_orbit_map[*bigUnionOfPairWiseInter_iter],
      //        (int)var_orbits[(int)var_to_orbit_map[*bigUnionOfPairWiseInter_iter]].size());
      bigUnionOfPairWiseInter_iter++;
    }
  commonVarOrbits = getVarsOrbits(bigUnionOfPairWiseInter, var_to_orbit_map);
  commonResourcesOrbits = getVarsOrbits(bigUnionOfPairWiseInter, var_to_orbit_map);
  std::cout << "Common resource orbits before removing non-preconditions are ";
  std::cout << commonResourcesOrbits;
  std::cout << std::endl;
  commonResourcesOrbits = customInter(commonResourcesOrbits, getActionsPreconditions(quotientProblem.actions));
  //commonResourcesOrbits.clear();
  std::cout << "Common resource orbits after removing non-preconditions are ";
  std::cout << commonResourcesOrbits;
  // TODO: Last step is remove the common resources no provided by the initial state
  std::cout << std::endl;
#ifdef debug
  instSetIter = instantiationSet.begin();
  std::set<int>::iterator commonResourceIter = commonResourcesOrbits.begin();
  for(int i = 0 ; i < (int)instantiationSet.size() ; i++)
    {
      commonResourceIter = commonResourcesOrbits.begin();
      for(int j = 0 ; j < (int)commonResourcesOrbits.size() ; j++)
        {
          if(((std::map<int,int>)*instSetIter).find(*commonResourceIter) == ((std::map<int,int>)*instSetIter).end())
            {
              FILE* successFile = fopen("successFile","a");
              fprintf(successFile, "%d\r\n", 5);
              fclose(successFile);
              printf("Corrupted instantiation found, exiting!!!\r\n");
              exit(-1);
	    }
          printf("Instantiation %d maps orbit %d to var %d\r\n", i, *commonResourceIter, ((std::map<int,int>)*instSetIter)[*commonResourceIter]);
          commonResourceIter++;
        }  
      instSetIter++;
    }
#endif
  printf("\r\n");
}


void StripsSymmetry::GetInstantiations()
  {
    instantiationSet.clear();    
    std::set<int> primaryCover;
    std::set< Proposition >::iterator goal_iter = problem.goals.begin();
    for(int i = 0 ; i < (int)problem.goals.size() ; i++)
      {
        primaryCover.insert(*goal_iter);
        goal_iter++;
      }
    printf("Initial size of primary cover %d.\r\n", (int)primaryCover.size());
    /* std::cout << primaryCover; */
    /* std::cout << std::endl; */
    std::set<int> secondaryCover;
    for(int i = 0 ; i < (int)problem.variables.size() ; i++)
      secondaryCover.insert(i);
    printf("Initial size of secondary cover %d.\r\n", (int)secondaryCover.size());
    printf("Number of variable orbits is %d\r\n", (int)var_orbits.size());
    /* std::cout << secondaryCover; */
    /* std::cout << std::endl; */
    printf("The actions do not cover %d variables from the secondary cover\r\n", (int)setMinus(secondaryCover, getActionsDomain(problem.actions)).size());
    std::set<int> var_orbits_not_to_cover = getVarsOrbits(setMinus(secondaryCover, getActionsDomain(problem.actions)), var_to_orbit_map);
    if((customInter(bigUnion(vectorFilter(var_orbits, var_orbits_not_to_cover)),
                    getActionsDomain(problem.actions))).size() != 0
       || action_orbits.size() != quotientProblem.actions.size())
      {
        FILE* successFile = fopen("successFile","a");
        fprintf(successFile, "%d\r\n", 1);
        fclose(successFile);
        printf("Problem in symmteries, exiting!!!\r\n");
        exit(1);
      }
    instantiationSanityCheck();
    std::set<int> action_orbits_set;
    for(int i = 0 ; i < (int)action_orbits.size() ; i++)
      action_orbits_set.insert(i);
    std::map<int,int> currentInst;
    currentInst.clear();
    //for(int i = 0 ; i < quotientProblem.actions.size() ; i ++)
    while(!primaryCover.empty()){
      currentInst.clear();
      std::set<int> remaining_action_orbits = action_orbits_set;
      std::map<int, GroundActionType> remainingActions;
      remainingActions = vectorToMap(problem.actions);
      while(currentInst.size() < (var_orbits.size() - var_orbits_not_to_cover.size()))
	// || remaining_action_orbits.size() != 0)
        {
          //1-Get the set of actions that are consistent with the current instantiation and within themselves
          //If it is empty, then no instantiations can be found
          printf("Size of remaining actions = %d\r\n", (int)remainingActions.size());
          std::vector<GroundActionType> consistentActions
              = GetConsistentActions
                   (currentInst,
                    var_to_orbit_map,
                    remainingActions);
          //#ifdef debug
          printf("\r\nNumber of consistent actions is %d\r\n", (int)consistentActions.size());
          //#endif
          if(consistentActions.size() == 0)
            {
              FILE* successFile = fopen("successFile","a");
              fprintf(successFile, "%d\r\n", 2);
              fclose(successFile);
              printf("No instantiations possible 1, exiting!!!\r\n");
              exit(1);
            }
          //2-Get the action that has the largest number of members of primary cover variables.
          //Break ties with number of secondary cover variables
          GroundActionType bestAction = GetBestAction
                                             (primaryCover,
                                              secondaryCover,
                                              consistentActions);        
          //#ifdef debug
          printf("Size of best action domain is %d.\r\n", (int)domAction(bestAction).size());
          printf("It has %d primary cover variables in its domain.\r\n", (int)customInter(primaryCover, domAction(bestAction)).size());
          printf("It has %d secondary cover variables in its domain.\r\n", (int)customInter(secondaryCover, domAction(bestAction)).size());
          int bestActionIndex = problem.getActionIndex(bestAction);
          int bestActionOrbit = action_to_orbit_map[bestActionIndex];
          printf("It belongs to orbit %d.\r\n", bestActionOrbit);           
          remaining_action_orbits.
             erase(remaining_action_orbits.find(bestActionOrbit));
          remainingActions = mapErase(remainingActions, action_orbits[bestActionOrbit]);
          printf("Number of remaining action orbits to instantiate %d.\r\n", (int)remaining_action_orbits.size());
          //#endif
          //3-Add the mapping from the orbits to the variables stated in the best action. 
          //Check whether the orbit is already mapped to a different variable
          if(AugmentInstantiation(currentInst,
                                  bestAction,
                                  var_to_orbit_map) == -1)
            {
              FILE* successFile = fopen("successFile","a");
              fprintf(successFile, "%d\r\n", 3);
              fclose(successFile);
              printf("No instantiations possible 2, exiting!!!");
              exit(1);
            }
          //#ifdef debug
          printf("Size of current instantiation domain is %d\r\n", (int)currentInst.size());
          //#endif
          //4-remove instantiated variables from the primary and the secondary cover
          primaryCover = setMinus(primaryCover, domAction(bestAction));
          //#ifdef debug
          printf("Size of primary cover is %d\r\n", (int)primaryCover.size());
          //#endif            
          secondaryCover = setMinus(secondaryCover, domAction(bestAction));
          //#ifdef debug
          printf("Size of secondary cover is %d\r\n", (int)secondaryCover.size());
          //#endif            
        }
      //#ifdef debug
      printf("Instantiation %d completed with domain size %d\r\n", (int)instantiationSet.size(), (int)currentInst.size());
      //#endif                
      //5-Add current instantiation to the set of intantiations and clear the current instantiation
      instantiationSet.insert(currentInst);
    }
    if(!primaryCover.empty())
      {
        FILE* successFile = fopen("successFile","a");
        fprintf(successFile, "%d\r\n", 4);
        fclose(successFile);
        printf("Instantiation failed, exiting!!!\r\n");
        exit(1);
      }
    printf("Size of instantiations is %d\r\n", (int)instantiationSet.size());
  }

void StripsSymmetry::InitPrimaryScoreStructure()
  {
    std::set<int> tempSet;
    for(int i = 0 ; i <= (int)primaryCover.size() ; i++)
      primary_cover_score_to_actions.push_back(tempSet);
    for(int i = 0 ; i < (int)problem.actions.size() ; i++)
      {
        int initScore = (int)(customInter(primaryCover, domAction(problem.actions[i]))).size();
        action_to_primary_cover_score.push_back(initScore);
        primary_cover_score_to_actions[initScore].insert(i);
      }
  }

void StripsSymmetry::updatePrimaryScoreStructure(std::set<int> dom)//, std::set<int> remainingActions)
  {
    std::vector<int> affectedActions;
    std::set<int>::iterator domIter = dom.begin();
    for(int i = 0; i < (int)dom.size() ; i++)
      {
        affectedActions = customUnion(affectedActions, problem.var_to_action[*domIter]);
        domIter++;
      }
    for(int i = 0 ; i < (int)affectedActions.size() ; i++)
      {
        GroundActionType affectedAction = problem.actions[affectedActions[i]];
        int scoreDec = (int) (customInter(customInter(dom, domAction(affectedAction)), primaryCover)).size();
        int oldScore = action_to_primary_cover_score[affectedActions[i]];
        primary_cover_score_to_actions[oldScore].erase(affectedActions[i]);
        int newScore = oldScore-scoreDec;
        primary_cover_score_to_actions[newScore].insert(affectedActions[i]);
        action_to_primary_cover_score[affectedActions[i]] = newScore;
      }
    primaryCover = setMinus(primaryCover, dom);
  }

void StripsSymmetry::InitSecondaryScoreStructure()
  {
    std::set<int> tempSet;
    for(int i = 0 ; i <= (int)secondaryCover.size() ; i++)
      secondary_cover_score_to_actions.push_back(tempSet);
    for(int i = 0 ; i < (int)problem.actions.size() ; i++)
      {
        int initScore = (int)(customInter(secondaryCover, domAction(problem.actions[i]))).size();
        action_to_secondary_cover_score.push_back(initScore);
        secondary_cover_score_to_actions[initScore].insert(i);
      }
  }

void StripsSymmetry::updateSecondaryScoreStructure(std::set<int> dom)
  {
    std::vector<int> affectedActions;
    std::set<int>::iterator domIter = dom.begin();
    for(int i = 0; i < (int)dom.size() ; i++)
      {
        affectedActions = customUnion(affectedActions, problem.var_to_action[*domIter]);
        domIter++;
      }
    for(int i = 0 ; i < (int)affectedActions.size() ; i++)
      {
        GroundActionType affectedAction = problem.actions[affectedActions[i]];
        int scoreDec = (int) (customInter(customInter(dom, domAction(affectedAction)), secondaryCover)).size();
        int oldScore = action_to_secondary_cover_score[affectedActions[i]];
        secondary_cover_score_to_actions[oldScore].erase(affectedActions[i]);
        int newScore = oldScore-scoreDec;
        secondary_cover_score_to_actions[newScore].insert(affectedActions[i]);
        action_to_secondary_cover_score[affectedActions[i]] = newScore;
      }
    secondaryCover = setMinus(secondaryCover, dom);
  }



void StripsSymmetry::GetInstantiationsPriorityQueue()
  {
    instantiationSet.clear();
    /* std::cout << secondaryCover; */
    /* std::cout << std::endl; */
    printf("The actions do not cover %d variables from the secondary cover\r\n", (int)setMinus(secondaryCover, getActionsDomain(problem.actions)).size());
    std::set<int> var_orbits_not_to_cover = getVarsOrbits(setMinus(secondaryCover, getActionsDomain(problem.actions)), var_to_orbit_map);
    if((customInter(bigUnion(vectorFilter (var_orbits, var_orbits_not_to_cover)),
                    getActionsDomain(problem.actions))).size() != 0
       || action_orbits.size() != quotientProblem.actions.size())
      {
        FILE* successFile = fopen("successFile","a");
        fprintf(successFile, "%d\r\n", 1);
        fclose(successFile);
        printf("Problem in symmteries, exiting!!!\r\n");
        exit(1);
      }
    instantiationSanityCheck();
    std::set<int> action_orbits_set;
    for(int i = 0 ; i < (int)action_orbits.size() ; i++)
      action_orbits_set.insert(i);
    std::map<int,int> currentInst;
    currentInst.clear();
    //for(int i = 0 ; i < quotientProblem.actions.size() ; i ++)
    while(!primaryCover.empty()){
      currentInst.clear();
      std::set<int> remaining_action_orbits = action_orbits_set;
      std::map<int, GroundActionType> remainingActions;
      remainingActions = vectorToMap(problem.actions);
      while(currentInst.size() < (var_orbits.size() - var_orbits_not_to_cover.size())
	    || remaining_action_orbits.size() != 0)
        {
          //1-Get the set of actions that are consistent with the current instantiation and within themselves
          //If it is empty, then no instantiations can be found
          printf("Size of remaining actions = %d\r\n", (int)remainingActions.size());
          std::vector<int> consistentActions
             = GetConsistentActions2
                  (currentInst,
                   var_to_orbit_map,
                   remainingActions);
          //#ifdef debug
          printf("\r\nNumber of consistent actions is %d\r\n", (int)consistentActions.size());
          //#endif
          if(consistentActions.size() == 0)
            {
              FILE* successFile = fopen("successFile","a");
              fprintf(successFile, "%d\r\n", 2);
              fclose(successFile);
              printf("No instantiations possible 1, exiting!!!\r\n");
              exit(1);
            }
          //2-Get the action that has the largest number of members of primary cover variables.
          //Break ties with number of secondary cover variables
          int bestAction = GetBestActionPriorityQueue(consistentActions,
                                         primary_cover_score_to_actions,
                                         secondary_cover_score_to_actions);
          //#ifdef debug
          printf("Size of best action domain is %d.\r\n", (int)domAction(problem.actions[bestAction]).size());
          printf("It has %d primary cover variables in its domain.\r\n", (int)customInter(primaryCover, domAction(problem.actions[bestAction])).size());
          printf("It has %d secondary cover variables in its domain.\r\n", (int)customInter(secondaryCover, domAction(problem.actions[bestAction])).size());
          int bestActionIndex = bestAction;
          int bestActionOrbit = action_to_orbit_map[bestActionIndex];
          printf("It belongs to orbit %d.\r\n", bestActionOrbit);           
          remaining_action_orbits.
             erase(remaining_action_orbits.find(bestActionOrbit));
          remainingActions = mapErase(remainingActions, action_orbits[bestActionOrbit]);
          printf("Number of remaining action orbits to instantiate %d.\r\n", (int)remaining_action_orbits.size());
          //#endif
          //3-Add the mapping from the orbits to the variables stated in the best action. 
          //Check whether the orbit is already mapped to a different variable
          if(AugmentInstantiation(currentInst,
                                  problem.actions[bestAction],
                                  var_to_orbit_map) == -1)
            {
              FILE* successFile = fopen("successFile","a");
              fprintf(successFile, "%d\r\n", 3);
              fclose(successFile);
              printf("No instantiations possible 2, exiting!!!");
              exit(1);
            }
          //#ifdef debug
          printf("Size of current instantiation domain is %d\r\n", (int)currentInst.size());
          //#endif
          //4-remove instantiated variables from the primary and the secondary cover
          updatePrimaryScoreStructure(domAction(problem.actions[bestAction]));
          //primaryCover = setMinus(primaryCover, domAction(problem.actions[bestAction]));
          //#ifdef debug
          printf("Size of primary cover is %d\r\n", (int)primaryCover.size());
          //#endif            
          updateSecondaryScoreStructure(domAction(problem.actions[bestAction]));
          //secondaryCover = setMinus(secondaryCover, domAction(problem.actions[bestAction]));
          //#ifdef debug
          printf("Size of secondary cover is %d\r\n", (int)secondaryCover.size());
          //#endif            
        }
      //#ifdef debug
      printf("Instantiation %d completed with domain size %d\r\n", (int)instantiationSet.size(), (int)currentInst.size());
      //#endif                
      //5-Add current instantiation to the set of intantiations and clear the current instantiation
      instantiationSet.insert(currentInst);
    }
    if(!primaryCover.empty())
      {
        FILE* successFile = fopen("successFile","a");
        fprintf(successFile, "%d\r\n", 4);
        fclose(successFile);
        printf("Instantiation failed, exiting!!!\r\n");
        exit(1);
      }
    printf("Size of instantiations is %d\r\n", (int)instantiationSet.size());
  }


void StripsSymmetry::GetInstantiationsPriorityQueue2()
  {
    instantiationSet.clear();
    /* std::cout << secondaryCover; */
    /* std::cout << std::endl; */
    printf("The actions do not cover %d variables from the secondary cover\r\n", (int)setMinus(secondaryCover, getActionsDomain(problem.actions)).size());
    std::set<int> var_orbits_not_to_cover = getVarsOrbits(setMinus(secondaryCover, getActionsDomain(problem.actions)), var_to_orbit_map);
    if((customInter(bigUnion(vectorFilter (var_orbits, var_orbits_not_to_cover)),
                    getActionsDomain(problem.actions))).size() != 0)
       //|| action_orbits.size() != quotientProblem.actions.size())
      {
        FILE* successFile = fopen("successFile","a");
        fprintf(successFile, "%d\r\n", 1);
        fclose(successFile);
        printf("Problem in symmteries, exiting!!!\r\n");
        exit(1);
      }
    instantiationSanityCheck();
    std::set<int> action_orbits_set;
    for(int i = 0 ; i < (int)action_orbits.size() ; i++)
      action_orbits_set.insert(i);
    std::map<int,int> currentInst;
    currentInst.clear();
    InitPrimaryScoreStructure();
    //for(int i = 0 ; i < quotientProblem.actions.size() ; i ++)
    while(!primaryCover.empty()){
      currentInst.clear();
      std::set<int> remaining_action_orbits = action_orbits_set;
      //std::map<int, GroundActionType> remainingActions;
      //remainingActions = vectorToMap(problem.actions);
      std::set<int> remainingActions;
      remainingActions = bigUnion(action_orbits);
      while(currentInst.size() < (var_orbits.size() - var_orbits_not_to_cover.size())
	    || remaining_action_orbits.size() != 0)
        {
          //1-Get the set of actions that are consistent with the current instantiation and within themselves
          //If it is empty, then no instantiations can be found
          printf("Size of remaining actions = %d\r\n", (int)remainingActions.size());
          //2-Get the action that has the largest number of members of primary cover variables.
          //Break ties with number of secondary cover variables
          int bestAction = GetBestConsistentActionPriorityQueue(remainingActions,
                                                                currentInst,
                                                                primary_cover_score_to_actions,
                                                                secondary_cover_score_to_actions);
          if(bestAction == -1)
            {
              FILE* successFile = fopen("successFile","a");
              fprintf(successFile, "%d\r\n", 2);
              fclose(successFile);
              printf("No instantiations possible 1, exiting!!!\r\n");
              exit(1);
            }
          //#ifdef debug
          printf("Size of best action domain is %d.\r\n", (int)domAction(problem.actions[bestAction]).size());
          printf("It has %d primary cover variables in its domain.\r\n", (int)customInter(primaryCover, domAction(problem.actions[bestAction])).size());
          printf("It has %d secondary cover variables in its domain.\r\n", (int)customInter(secondaryCover, domAction(problem.actions[bestAction])).size());
          int bestActionIndex = bestAction;
          int bestActionOrbit = action_to_orbit_map[bestActionIndex];
          printf("It belongs to orbit %d.\r\n", bestActionOrbit);
          remaining_action_orbits.
             erase(remaining_action_orbits.find(bestActionOrbit));
          //remainingActions = mapErase(remainingActions, action_orbits[bestActionOrbit]);
          remainingActions = setMinus(remainingActions, action_orbits[bestActionOrbit]);
          printf("Number of remaining action orbits to instantiate %d.\r\n", (int)remaining_action_orbits.size());
          //#endif
          //3-Add the mapping from the orbits to the variables stated in the best action. 
          //Check whether the orbit is already mapped to a different variable
          if(AugmentInstantiation(currentInst,
                                  problem.actions[bestAction],
                                  var_to_orbit_map) == -1)
            {
              FILE* successFile = fopen("successFile","a");
              fprintf(successFile, "%d\r\n", 3);
              fclose(successFile);
              printf("No instantiations possible 2, exiting!!!");
              exit(1);
            }
          //#ifdef debug
          printf("Size of current instantiation domain is %d\r\n", (int)currentInst.size());
          //#endif
          //4-remove instantiated variables from the primary and the secondary cover
          updatePrimaryScoreStructure(domAction(problem.actions[bestAction]));
          //primaryCover = setMinus(primaryCover, domAction(problem.actions[bestAction]));
          //#ifdef debug
          printf("Size of primary cover is %d\r\n", (int)primaryCover.size());
          //#endif            
          updateSecondaryScoreStructure(domAction(problem.actions[bestAction]));
          //secondaryCover = setMinus(secondaryCover, domAction(problem.actions[bestAction]));
          //#ifdef debug
          printf("Size of secondary cover is %d\r\n", (int)secondaryCover.size());
          //#endif            
        }
      //#ifdef debug
      printf("Instantiation %d completed with domain size %d\r\n", (int)instantiationSet.size(), (int)currentInst.size());
      //#endif                
      //5-Add current instantiation to the set of intantiations and clear the current instantiation
      instantiationSet.insert(currentInst);
    }
    if(!primaryCover.empty())
      {
        FILE* successFile = fopen("successFile","a");
        fprintf(successFile, "%d\r\n", 4);
        fclose(successFile);
        printf("Instantiation failed, exiting!!!\r\n");
        exit(1);
      }
    printf("Size of instantiations is %d\r\n", (int)instantiationSet.size());
  }


std::vector<GroundActionType> StripsSymmetry::GetConsistentActions
         (std::map<int,int> currentInst,
          std::map<int, int> var_to_orbit_map,
          std::map<int, GroundActionType> actionSet)
  {
#ifdef verobse
    printf("Getting consistent actions\r\n");
#endif
    std::vector<GroundActionType> consistentActions;
    std::map<int,int> tempInst;
    std::map<int, GroundActionType>::iterator actSetIter = actionSet.begin();
    for(int i = 0 ; i < (int)actionSet.size() ; i++)
      {
        tempInst = currentInst;
        if(ActionConsistentInstantiation(actionSet[actSetIter->first], tempInst, var_to_orbit_map))
          consistentActions.push_back(actionSet[actSetIter->first]);
        actSetIter++;
      }
    return consistentActions;
  }

std::vector<int> StripsSymmetry::GetConsistentActions2
         (std::map<int,int> currentInst,
          std::map<int, int> var_to_orbit_map,
          std::map<int, GroundActionType> actionSet)
  {
#ifdef verobse
    printf("Getting consistent actions\r\n");
#endif
    std::vector<int> consistentActions;
    std::map<int,int> tempInst;
    std::map<int, GroundActionType>::iterator actSetIter = actionSet.begin();
    for(int i = 0 ; i < (int)actionSet.size() ; i++)
      {
        tempInst = currentInst;
        if(ActionConsistentInstantiation(actionSet[actSetIter->first], tempInst, var_to_orbit_map))
          consistentActions.push_back(actSetIter->first);
        actSetIter++;
      }
    return consistentActions;
  }

std::vector<GroundActionType> StripsSymmetry::GetConsistentActions
         (std::map<int,int> currentInst,
          std::map<int, int> var_to_orbit_map,
          std::set<int> actionSet,
          std::vector<GroundActionType> actions)
  {
#ifdef verobse
    printf("Getting consistent actions\r\n");
#endif
    std::vector<GroundActionType> consistentActions;
    std::map<int,int> tempInst;
    std::set<int>::iterator actSetIter = actionSet.begin();
    for(int i = 0 ; i < (int)actionSet.size() ; i++)
      {
        tempInst = currentInst;
        if(ActionConsistentInstantiation(actions[*actSetIter], tempInst, var_to_orbit_map))
          consistentActions.push_back(actions[*actSetIter]);
        actSetIter++;
      }
    return consistentActions;
  }

std::set<int> StripsSymmetry::GetConsistentActions3
         (std::map<int,int> currentInst,
          std::map<int, int> var_to_orbit_map,
          std::set<int> actionSet,
          std::vector<GroundActionType> actions)
  {
#ifdef verobse
    printf("Getting consistent actions\r\n");
#endif
    std::set<int> consistentActions;
    std::map<int,int> tempInst;
    std::set<int>::iterator actSetIter = actionSet.begin();
    for(int i = 0 ; i < (int)actionSet.size() ; i++)
      {
        tempInst = currentInst;
        if(ActionConsistentInstantiation(actions[*actSetIter], tempInst, var_to_orbit_map))
          consistentActions.insert(*actSetIter);
        actSetIter++;
      }
    return consistentActions;
  }


bool StripsSymmetry::ActionConsistentActionOrbits
          (GroundActionType action,
	   std::vector<GroundActionType> actionSet,
	   std::set<int> actionOrbits)
  {
    for(int i = 0 ; i < (int)actionSet.size() ; i++)
      {
        if(ActionConsistentAction(action, actionSet[i]))
          {
	    if(actionOrbits.find(action_to_orbit_map[problem.getActionIndex(actionSet[i])]) != actionOrbits.end())
              actionOrbits.erase(actionOrbits.find(action_to_orbit_map[problem.getActionIndex(actionSet[i])]));
          }
      }
    return actionOrbits.empty();
  }

bool StripsSymmetry::ActionConsistentAction
         (GroundActionType action1,
          GroundActionType action2)
  {
    std::map<int,int> tempInst;
    AugmentInstantiation(tempInst, action1, var_to_orbit_map);
    return ActionConsistentInstantiation(action2, tempInst, var_to_orbit_map);
  }

bool StripsSymmetry::instantiationSanityCheck()
{
  int count = 0;
  for(int i = 0; i < (int)problem.actions.size() ; i++)
    {
      //printf("Action %d has %d variables which belong to %d orbits\r\n", i, domAction(actions[i]).size(), getVarsOrbits(domAction(actions[i]), var_to_orbit_map).size());
      if(domAction(problem.actions[i]).size() != getVarsOrbits(domAction(problem.actions[i]), var_to_orbit_map).size())
        {
          printf("Action %d has %d variables belonging to %d orbits!!!\r\n", i, (int)domAction(problem.actions[i]).size(), (int)getVarsOrbits(domAction(problem.actions[i]), var_to_orbit_map).size());
          printf("Sanity check failed, no instantiations will work (counter e.g.6 applies to this problem), exit!!!\r\n");
          FILE* successFile = fopen("successFile","a");
          fprintf(successFile, "%d\r\n", 0);
          fclose(successFile);
          count++;
          exit(1);
        }
    }
  printf("%d\n", count);
  return count == 0;
}

bool StripsSymmetry::orbitsSanityCheck()
{
  int count = 0;
  for(int i = 0; i < (int)var_orbits.size() ; i++)
    {
      //printf("Action %d has %d variables which belong to %d orbits\r\n", i, domAction(actions[i]).size(), getVarsOrbits(domAction(actions[i]), var_to_orbit_map).size());
      if(var_orbits[i].size() == 0)
        {
          printf("Var orbit %d is empty!!!\r\n", i);
          printf("Orbits sanity check failed, exit!!!\r\n");
          FILE* successFile = fopen("successFile","a");
          fprintf(successFile, "%d\r\n", 0);
          fclose(successFile);
          count++;
          exit(1);
        }
    }

  for(int i = 0; i < (int)action_orbits.size() ; i++)
    {
      //printf("Action %d has %d variables which belong to %d orbits\r\n", i, domAction(actions[i]).size(), getVarsOrbits(domAction(actions[i]), var_to_orbit_map).size());
      if(action_orbits[i].size() == 0)
        {
          printf("Action orbit %d is empty!!!\r\n", i);
          printf("Orbits sanity check failed, exit!!!\r\n");
          FILE* successFile = fopen("successFile","a");
          fprintf(successFile, "%d\r\n", 0);
          fclose(successFile);
          count++;
          exit(1);
        }
    }
  printf("%d\n", count);
  return count == 0;
}

bool StripsSymmetry::ActionConsistentInstantiation
         (GroundActionType action,
          std::map<int,int> currentInst,
          std::map<int, int> var_to_orbit_map)
  {
    return AugmentInstantiation(currentInst, action,var_to_orbit_map) != -1;
  }

int StripsSymmetry::AugmentInstantiation
         (std::map<int,int> &currentInst,
          GroundActionType action,
          std::map<int, int> var_to_orbit_map)
  {
#ifdef debug
    printf("Size of current instantiation is %d\r\n", (int)currentInst.size());
    printf("Size of current action preconds is %d\r\n", (int)action.preconditions.Trues.size());
#endif
    bool consistent = true;
    for(int j = 0; j < (int)action.preconditions.Trues.size() ; j++)
      {
        if(currentInst.find(var_to_orbit_map[action.preconditions.Trues[j]]) != currentInst.end())
          {
            if(currentInst[var_to_orbit_map[action.preconditions.Trues[j]]] != action.preconditions.Trues[j])
              {
                consistent = false;
                break;
                //cout << "Orbit "<< var_to_orbit_map[action.preconditions.Trues[j]] << " is assigned to " << currentInst[var_to_orbit_map[action.preconditions.Trues[j]]] << " not to " << action.preconditions.Trues[j] << std::endl;
              }
          }
	else
          currentInst[var_to_orbit_map[action.preconditions.Trues[j]]] = action.preconditions.Trues[j];
      }
    for(int j = 0; j < (int)action.effects.Trues.size() ; j++)
      {
        if(currentInst.find(var_to_orbit_map[action.effects.Trues[j]]) != currentInst.end())
          {
            if(currentInst[var_to_orbit_map[action.effects.Trues[j]]] != action.effects.Trues[j])
              {
                consistent = false;
                break;
                //cout << "Orbit "<< var_to_orbit_map[action.effects.Trues[j]] << " is assigned to " << currentInst[var_to_orbit_map[action.effects.Trues[j]]] << " not to " << action.effects.Trues[j] << std::endl;
              }
          }
	else
          currentInst[var_to_orbit_map[action.effects.Trues[j]]] = action.effects.Trues[j];
      }
    for(int j = 0; j < (int)action.effects.Falses.size() ; j++)
      {
        if(currentInst.find(var_to_orbit_map[action.effects.Falses[j]]) != currentInst.end())
          {
            if(currentInst[var_to_orbit_map[action.effects.Falses[j]]] != action.effects.Falses[j])
              {
                consistent = false;
                break;
                //cout << "Orbit "<< var_to_orbit_map[action.effects.Falses[j]] << " is assigned to " << currentInst[var_to_orbit_map[action.effects.Falses[j]]] << " not to " << action.effects.Falses[j] << std::endl;
              }
          }
	else
          currentInst[var_to_orbit_map[action.effects.Falses[j]]] = action.effects.Falses[j];
      }
    if(!consistent)
      return -1;
    return 0;
  }

std::map<int,int> StripsSymmetry::GetInstantiationsRecurcisve(std::map<int,int> currentInst, std::set<int> remainingActionOrbits)
  {
    std::map<int,int> retInst;
    int currentOrbit = *(remainingActionOrbits.begin());
    std::vector<GroundActionType> consistentActionsInThisOrbit = GetConsistentActions(currentInst, var_to_orbit_map, action_orbits[currentOrbit], problem.actions);
    if(consistentActionsInThisOrbit.size() == 0)
      return retInst;
    remainingActionOrbits.erase(remainingActionOrbits.begin());
    //Sort them according to their coverage
    for(int i = 0 ; i < (int)consistentActionsInThisOrbit.size() ; i ++)
      {
        //check whether all the orbits have at least one action consistent with the current one
          //if not continue
        //Try to instantiate with current action
        retInst = currentInst; 
        int instResult = AugmentInstantiation(retInst, consistentActionsInThisOrbit[i], var_to_orbit_map);
        if(instResult == -1)
          continue;
        if(remainingActionOrbits.size() == 0)
          {
            printf("%d\r\n",(int)retInst.size());
            return retInst;
          }
        retInst = GetInstantiationsRecurcisve(retInst, remainingActionOrbits);
        if(retInst.size() != 0)
          {
            //printf("%d\r\n",(int)retInst.size());
            return retInst;
          }
      }
    std::map<int,int> emptyMap;
    return emptyMap;
  }



std::vector<std::pair<int, int> > StripsSymmetry::getPairwisCoflictingActionOrbits()
{
  std::vector<std::pair<int, int> > retVec;
  for(int i = 0 ; i < (int)action_orbits.size() ; i++ )
    for(int j = i + 1 ; j < (int)action_orbits.size() ; j++ )
      {
        std::map<int,int> currentInst;
        AugmentInstantiation(currentInst, problem.actions[*(action_orbits[i].begin())], var_to_orbit_map);
        if(GetConsistentActions(currentInst, var_to_orbit_map, action_orbits[j], problem.actions).size() == 0)
          {
            printf("Action orbits %d %d are conflicting\r\n", i, j);
            printf("Orbit %d is ", i);
	    std::cout<<action_orbits[i];
	    std::cout<<std::endl;
            printf("Orbit %d is ", j);
	    std::cout<<action_orbits[j];
	    std::cout<<std::endl;
            retVec.push_back(std::pair<int,int> (i,j));
          }
      }
  return retVec;
}

void StripsSymmetry::GetAllInstantiationsRecurcisve(std::map<int,int> currentInst, std::set<int> remainingActionOrbits)
  {
    std::map<int,int> retInst;
    if(remainingActionOrbits.size() == 0)
      {
        allInstantiations.insert(currentInst);
        return;
      }
    int currentOrbit = *(remainingActionOrbits.begin());
    std::vector<GroundActionType> consistentActionsInThisOrbit = GetConsistentActions(currentInst, var_to_orbit_map, action_orbits[currentOrbit], problem.actions);
    if(consistentActionsInThisOrbit.size() == 0)
      {
        return;
      }
    remainingActionOrbits.erase(remainingActionOrbits.begin());
    //Sort them according to their coverage
    for(int i = 0 ; i < (int)consistentActionsInThisOrbit.size() ; i ++)
      {
        //check whether all the orbits have at least one action consistent with the current one
          //if not continue
        //Try to instantiate with current action
        retInst = currentInst; 
        AugmentInstantiation(retInst, consistentActionsInThisOrbit[i], var_to_orbit_map);
        GetAllInstantiationsRecurcisve(retInst, remainingActionOrbits);
      }
    std::map<int,int> emptyMap;
/*  &&
           ActionConsistentActionOrbits(actionSet[i], actionSet, actionOrbits)*/
  }

int StripsSymmetry::GetBestConsistentActionPriorityQueue(std::set<int> actionSet,
			       std::map<int,int> currentInst,
                               std::vector<std::set<int> > primaryScoreQueues,
                               std::vector<std::set<int> > secondaryScoreQueues)
  {
    std::set<int> primaryBestActionSet;
    int i = -1;
    for(i = (int)primaryScoreQueues.size() - 1 ; i >= 0  ; i--)
      {
        if(primaryScoreQueues[i].size() != 0)
          {
            primaryBestActionSet = customInter(primaryScoreQueues[i], actionSet);
            primaryScoreQueues[i] = primaryBestActionSet;
            if(primaryBestActionSet.size() != 0)
              {
                primaryBestActionSet = GetConsistentActions3(currentInst, var_to_orbit_map, primaryBestActionSet, problem.actions);
                primaryScoreQueues[i] = primaryBestActionSet;
                if(primaryBestActionSet.size() != 0)
                 {                   
                   break;
                 }
              }
          }
      }
    int j = -1;
    std::set<int> secondaryBestActionSet;
    for(j = (int)secondaryScoreQueues.size() - 1 ; j >= 0  ; j--)
      {
        if(secondaryScoreQueues[j].size() != 0)
          {
            secondaryBestActionSet = customInter(secondaryScoreQueues[j], actionSet);
            if(secondaryBestActionSet.size() != 0)
              {
                secondaryBestActionSet = GetConsistentActions3(currentInst, var_to_orbit_map, secondaryBestActionSet, problem.actions);
                if(secondaryBestActionSet.size() != 0)
                 {                   
                   break;
                 }
              }
          }
      }
    return *(secondaryBestActionSet.begin());
  }

void StripsSymmetry::getSortedActionOrbits()
{
  std::vector<int> tempOrbit;
  for(int i = 0 ; i < (int)action_orbits.size() ; i++)
    sorted_action_orbits.push_back(tempOrbit);


  for(int i = 0 ; i < (int)action_orbits.size() ; i++)
    {
      std::set<int> currentOrbit = action_orbits[i];
      while(currentOrbit.size() != 0)
        {
          int max = -1;
          std::set<int>::iterator orbitsIter;
          orbitsIter = currentOrbit.begin();
          std::set<int>::iterator maxIter;
          maxIter = currentOrbit.end();
          for(int j = 0 ; j < (int)currentOrbit.size() ; j++)
            {
              if((int)customInter(domAction(problem.actions[*orbitsIter]), primaryCover).size() > max)
                {
                  max = (int)customInter(domAction(problem.actions[*orbitsIter]), primaryCover).size();
                  maxIter = orbitsIter;
                }
              orbitsIter++;
            }
          sorted_action_orbits[i].push_back(*maxIter);
          currentOrbit.erase(maxIter);
       }
    }
  printf("Finished sorting action orbits\r\n");
}

void StripsSymmetry::sortOrbit(int i)
{
  sorted_action_orbits[i].clear();
  std::set<int> currentOrbit = action_orbits[i];
  //printf("Sorting orbit %d:", i);
  while(currentOrbit.size() != 0)
    {
      int max = -1;
      std::set<int>::iterator orbitsIter;
      orbitsIter = currentOrbit.begin();
      std::set<int>::iterator maxIter;
      maxIter = currentOrbit.end();
      for(int j = 0 ; j < (int)currentOrbit.size() ; j++)
        {
          if((int)customInter(domAction(problem.actions[*orbitsIter]), primaryCover).size() > max)
            {
              max = (int)customInter(domAction(problem.actions[*orbitsIter]), primaryCover).size();
              maxIter = orbitsIter;
            }
          orbitsIter++;
        }
      //printf("act: %d sc: %d ", *maxIter, max);
      sorted_action_orbits[i].push_back(*maxIter);
      currentOrbit.erase(maxIter);
   }
  //printf("\r\n");
  //printf("Finished sorting the action orbit\r\n");
}

std::map<int,int> StripsSymmetry::GetInstantiationsRecurcisveWithSortedActions(std::map<int,int> currentInst, std::set<int> remainingActionOrbits)
  {
    std::map<int,int> retInst;
    int currentOrbit = *(remainingActionOrbits.begin());
    //printf("%d\r\n", (int)currentOrbit);
    sortOrbit(currentOrbit);
    std::set<int> consistentActionsInThisOrbit =
                     GetConsistentActions3(currentInst,
                                          var_to_orbit_map,
                                          action_orbits[currentOrbit],
                                          problem.actions);
    //printf("There are %d consistent actions in the orbit of size %d, with sorted vector of size %d\r\n", (int)consistentActionsInThisOrbit.size(), (int)action_orbits[currentOrbit].size(), (int)sorted_action_orbits[currentOrbit].size());
    if(consistentActionsInThisOrbit.size() == 0)
      {
        return retInst;
      }
    remainingActionOrbits.erase(remainingActionOrbits.begin());
    for(int i = 0 ; i < (int)sorted_action_orbits[currentOrbit].size() ; i ++)
      {
        //printf("xx\r\n");
        //printf("%d--%d\r\n", (int)sorted_action_orbits[currentOrbit].size(), (int)consistentActionsInThisOrbit.size());
        //printf("%d--%d\r\n", (int)sorted_action_orbits[currentOrbit][0], (int)(*action_orbits[currentOrbit].begin()));//(*consistentActionsInThisOrbit.begin()));
        if(consistentActionsInThisOrbit.find(sorted_action_orbits[currentOrbit][i]) !=
                         consistentActionsInThisOrbit.end())
          {
            //printf("Action: %d\r\n", (int)sorted_action_orbits[currentOrbit][i]);//customInter(primaryCover,
	    //domAction(problem.actions[sorted_action_orbits[currentOrbit][i]])).size());
            retInst = currentInst; 
            int instResult = AugmentInstantiation(retInst,
                                                  problem.actions[sorted_action_orbits[currentOrbit][i]],
                                                  var_to_orbit_map);
            if(instResult == -1)
              {
                continue;
              }
            if(remainingActionOrbits.size() == 0)
              {
                //printf("%d\r\n", (int)customInter(primaryCover,coDom(retInst)).size());
                return retInst;
              }
            retInst = GetInstantiationsRecurcisveWithSortedActions(retInst, remainingActionOrbits);
            if(retInst.size() != 0)
              {
                return retInst;
              }
          }
      }
    std::map<int,int> emptyMap;
    return emptyMap;
  }


void StripsSymmetry::GetCoveringInstantiationsRecursive()
{
  struct timespec Start, End;
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &Start);
  //getSortedActionOrbits();
  instantiationSet.clear();    
  std::map<int,int> tempInst;
  std::vector<int> tempOrbit;
  for(int i = 0 ; i < (int)action_orbits.size() ; i++)
    sorted_action_orbits.push_back(tempOrbit);
  std::set<int> actionOrbits;
  std::map<int,int> fixedInstPart;//A part of the instantiation that will not change
  for(int i = 0 ; i < (int)action_orbits.size() ; i++)
    {
      if(action_orbits[i].size() == 1)
        {
          int instResult = AugmentInstantiation(fixedInstPart,
                                                problem.actions[*action_orbits[i].begin()],
                                                var_to_orbit_map);
          if(instResult == -1)
            {
              printf("No covering instantiations, exiting!!\r\n");
              exit(0);
            }
        }
      else
        actionOrbits.insert(i);
    }  
  while(primaryCover.size() != 0)
    {
      printf("Primary cover size: %d\r\n", (int)primaryCover.size());
      tempInst.clear();
      tempInst = fixedInstPart;//Funion with the fixed part
      tempInst[var_to_orbit_map[*primaryCover.begin()]] = *primaryCover.begin();
      tempInst = GetInstantiationsRecurcisveWithSortedActions(tempInst, actionOrbits);
      if(tempInst.size() == 0)
        {
          printf("No covering instantiations, exiting!!\r\n");
          exit(0);
        }
      primaryCover = setMinus(primaryCover, coDom(tempInst));
      instantiationSet.insert(tempInst);
      //printf("xx:%d\r\n",(int)tempInst.size());//customInter(symmTool.primaryCover,
      //coDom(tempInst)).size());
    }
  printf("Covering instantiation set size is: %d\r\n", (int)instantiationSet.size());
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &End);
  instFindTime = getTimeSec(Start, End);
}


std::map<int,int> StripsSymmetry::GetInstantiationSMT(std::map<int,int> currentInst, std::set<int> remainingActionOrbits, double &inst_time)
  {
    FILE*instFile = fopen("instFind.smt2", "w");
    fprintf(instFile, "(set-logic QF_UF)\n(set-option :produce-models true)\n(declare-sort U 0)\n");
    for(int i = 0 ; i < (int)var_orbits.size() ; i ++)
      {
        fprintf(instFile, "(declare-fun o%d () U)\n", i);
        std::set<int>::iterator orbitIter = var_orbits[i].begin();
        for(int j = 0 ; j < (int)var_orbits[i].size() ; j ++)
          {
            fprintf(instFile, "(declare-fun v%d () U)\n", *orbitIter);
            orbitIter++;
          }
        fprintf(instFile, "(assert (or ");
        orbitIter = var_orbits[i].begin();
        for(int j = 0 ; j < (int)var_orbits[i].size() ; j ++)
          {
            fprintf(instFile, "(= o%d v%d) ", i, *orbitIter);
            orbitIter++;
          }
        fprintf(instFile, "))\n");
      }
    fprintf(instFile, "(assert (and ");
    std::map<int,int>::iterator instIter = currentInst.begin();
    for(int i = 0 ; i < (int)currentInst.size() ; i++)
      {
        fprintf(instFile, "(= o%d v%d) ", instIter->first, instIter->second);
        instIter++;
      }
    fprintf(instFile, "))\n");
    std::set<int>::iterator actionOrbitsIter = remainingActionOrbits.begin();
    for(int i = 0 ; i < (int)remainingActionOrbits.size() ; i ++)
      {
        fprintf(instFile, "(assert (or ");
        std::set<int>::iterator currentOrbitIter =  action_orbits[*actionOrbitsIter].begin();
        for(int j = 0 ; j < (int)action_orbits[*actionOrbitsIter].size() ; j++)
          {
            GroundActionType action = problem.actions[*currentOrbitIter];
            fprintf(instFile, "(and ");
            for(int j = 0; j < (int)action.preconditions.Trues.size() ; j++)
              {
                fprintf(instFile, "(");
                fprintf(instFile, "= o%d v%d", var_to_orbit_map[action.preconditions.Trues[j]], action.preconditions.Trues[j]);
                fprintf(instFile, ")");
              }
            for(int j = 0; j < (int)action.effects.Trues.size() ; j++)
              {
                fprintf(instFile, "(");
                fprintf(instFile, "= o%d v%d", var_to_orbit_map[action.effects.Trues[j]], action.effects.Trues[j]);
                fprintf(instFile, ")");
              }
            for(int j = 0; j < (int)action.effects.Falses.size() ; j++)
              {
                fprintf(instFile, "(");
                fprintf(instFile, "= o%d v%d", var_to_orbit_map[action.effects.Falses[j]], action.effects.Falses[j]);
                fprintf(instFile, ")");
              }
            currentOrbitIter++;
            fprintf(instFile, ")");
          }
        actionOrbitsIter++;
        fprintf(instFile, "))\n");
      }
    fprintf(instFile, "(assert ( distinct ");
    for(int i = 0 ; i < (int)problem.variables.size() ; i ++)
      {
        fprintf(instFile, "v%d ", i);
      }
    fprintf(instFile, "))\n");
    
    fprintf(instFile, "(check-sat)\n(get-value ( ");
    for(int i = 0 ; i < (int)var_orbits.size() ; i ++)
      {
        fprintf(instFile, "o%d ", i);
      }
    for(int i = 0 ; i < (int)problem.variables.size() ; i ++)
      {
        fprintf(instFile, "v%d ", i);
      }
    fprintf(instFile,"))\n");
    fclose(instFile);
    if(system("$Z3_DIR/bin/z3 -smt2 -in -st < instFind.smt2 >inst_out") == -1)
      {
        printf("System command failed, exiting!!\r\n");
        exit(-1);
      }
    std::map<int,int> retInst;
    instFile = fopen("inst_out", "r");
    char token[100];
    fscanf(instFile, "%s", token);
    if(strcmp(token, "unsat") == 0)
      return retInst;
    std::vector<int> orbitVals;
    for(int i = 0 ; i < (int)var_orbits.size() ; i++)
      {
        orbitVals.push_back(-1);
      }
    std::vector<int> varVals;
    for(int i = 0 ; i < (int)problem.variables.size() ; i++)
      {
        varVals.push_back(-1);
      }
    while(fscanf(instFile, "%s", token) != EOF)
      {
        //printf("%s\r\n", token);
        if(strstr(token, "(o"))
          {
            int orbit_id = strtol(strstr(token, "(o")+2, NULL, 10);
            fscanf(instFile, "%s", token);
            int value = strtol(token + 6, NULL, 10);
            orbitVals[orbit_id] = value;
            //printf("Orbit %d has value %d\n", orbit_id, value);
          }
        if(strstr(token, "(v"))
          {
            int var_id = strtol(strstr(token, "(v")+2, NULL, 10);
            fscanf(instFile, "%s", token);
            int value = strtol(token + 6, NULL, 10);
            varVals[value] = var_id;
          }
        if(strcmp(":time",token) == 0)
          {
            fscanf(instFile, "%s", token);
            inst_time = strtod(token, NULL);
          }
      }
    fclose(instFile);
    for(int i = 0 ; i < (int)var_orbits.size() ; i++)
      {
        retInst[i] = varVals[orbitVals[i]];
      }
    return retInst;
  }

void StripsSymmetry::GetCoveringInstantiationsSMT()
{
  //getSortedActionOrbits();
  instantiationSet.clear();    
  std::map<int,int> tempInst;
  std::vector<int> tempOrbit;
  for(int i = 0 ; i < (int)action_orbits.size() ; i++)
    sorted_action_orbits.push_back(tempOrbit);
  std::set<int> actionOrbits;
  std::map<int,int> fixedInstPart;//A part of the instantiation that will not change
  for(int i = 0 ; i < (int)action_orbits.size() ; i++)
    {
      if(action_orbits[i].size() == 1)
        {
          int instResult = AugmentInstantiation(fixedInstPart,
                                                problem.actions[*action_orbits[i].begin()],
                                                var_to_orbit_map);
          if(instResult == -1)
            {
              printf("No covering instantiations, exiting!!\r\n");
              exit(0);
            }
        }
      else
        actionOrbits.insert(i);
    }
  double current_instFindTime = -1;
  while(primaryCover.size() != 0)
    {
      printf("Primary cover size: %d\r\n", (int)primaryCover.size());
      tempInst.clear();
      tempInst = fixedInstPart;//Funion with the fixed part
      tempInst[var_to_orbit_map[*primaryCover.begin()]] = *primaryCover.begin();
      tempInst = GetInstantiationSMT(tempInst, actionOrbits, current_instFindTime);
      if(tempInst.size() == 0)
        {
          printf("No covering instantiations, exiting!!\r\n");
          exit(0);
        }
     instFindTime += current_instFindTime;
      primaryCover = setMinus(primaryCover, coDom(tempInst));
      instantiationSet.insert(tempInst);
      //printf("xx:%d\r\n",(int)tempInst.size());//customInter(symmTool.primaryCover,
      //coDom(tempInst)).size());
    }
  printf("Covering instantiation set size is: %d\r\n", (int)instantiationSet.size());
}

bool StripsSymmetry::isomorphic(StripsProblem other)
  {
    this->PrintStripsProblemDescriptionGraph();
    if(system("echo At > nauty_in_1") == -1)
      {
        printf("System command failed, exiting!!\r\n");
        exit(-1);
      }
    if(system("echo -a >> nauty_in_1") == -1)
      {
        printf("System command failed, exiting!!\r\n");
        exit(-1);
      }
    if(system("echo -m >> nauty_in_1") == -1)
      {
        printf("System command failed, exiting!!\r\n");
        exit(-1);
      }
    if(system("cat nauty_in | grep -v At | grep -v a | grep -v m | grep -v [,] >> nauty_in_1") == -1)
      {
        printf("System command failed, exiting!!\r\n");
        exit(-1);
      }
    if(system("echo \"c x @\" >> nauty_in_1") == -1)
      {
        printf("System command failed, exiting!!\r\n");
        exit(-1);
      }
    StripsSymmetry symmTool(other);
    symmTool.initColours();
    symmTool.PrintStripsProblemDescriptionGraph();
    if(system("cat nauty_in | grep -v At | grep -v a | grep -v m |grep -v [,] >> nauty_in_1") == -1)
      {
        printf("System command failed, exiting!!\r\n");
        exit(-1);
      }
    if(system("echo \"x #\" >> nauty_in_1") == -1)
      {
        printf("System command failed, exiting!!\r\n");
        exit(-1);
      }
    if(system("$NAUTY_DIR/dreadnaut < nauty_in_1 > nauty_out") == -1)
      {
        printf("System command failed, exiting!!\r\n");
        exit(-1);
      }
    if(system("cat nauty_out | grep -i identical > ident") == -1)
      {
        printf("System command failed, exiting!!\r\n");
        exit(-1);
      }
    if(system("cat nauty_out | grep -i isomorphic > isom") == -1)
      {
        printf("System command failed, exiting!!\r\n");
        exit(-1);
      }
    struct stat ident; 
    struct stat isom;
    if (stat("ident", &ident) == 0)
      {
        if (ident.st_size != 0)
          return true;
      }
    if (stat("isom", &isom) == 0)
      {
        if (isom.st_size != 0)
          return true;
      }
    return false;
  }
