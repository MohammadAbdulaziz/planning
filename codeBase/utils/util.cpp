#include "util.h"
#include <utility>
#include <set>
#include <vector>
#include <map>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <time.h>

std::vector< std::set<int> > smtColour(int nVertices, std::vector<std::pair<int, int> > constraints, int nColours)
{
  FILE*colouringFile = fopen("colouring.smt2", "w");
  fprintf(colouringFile, "(set-logic QF_UF)\n(set-option :produce-models true)\n(declare-sort U 0)\n");
  for(int i = 0 ; i < nColours ; i ++)
    {
      fprintf(colouringFile, "(declare-fun col%d () U)\n", i);
    }
  fprintf(colouringFile, "(assert (distinct ");
  for(int i = 0 ; i < nColours ; i ++)
    {
      fprintf(colouringFile, "col%d ", i);
    }
  fprintf(colouringFile, "))\n");
  for(int i = 0 ; i < nVertices ; i ++)
    {
      fprintf(colouringFile, "(declare-fun vtx%d () U)\n", i);
      fprintf(colouringFile, "(assert (or ");
      for(int j = 0 ; j < nColours ; j ++)
        {
          fprintf(colouringFile, "(= vtx%d col%d)", i,j);
        }
      fprintf(colouringFile, "))\n");
    }
  for(int i = 0 ; i < (int) constraints.size() ; i ++)
    {
      if(constraints[i].first != constraints[i].second)
        fprintf(colouringFile, "(assert (distinct vtx%d vtx%d))\n", constraints[i].first, constraints[i].second);
    }
  fprintf(colouringFile, "(check-sat)\n(get-value ( ");
  for(int i = 0 ; i < nVertices ; i ++)
    {
      fprintf(colouringFile, "vtx%d ", i);
    }
  fprintf(colouringFile,"))");
  fclose(colouringFile);  
  system("$Z3_DIR/bin/z3 -smt2 -in -st < colouring.smt2 > colouring_out");
  FILE*colouringOut = fopen("colouring_out","r");
  char token[100];
  std::vector< std::set<int> > colouring;
  while(fscanf(colouringOut, "%s", token) != EOF)
    {
      if(strstr(token, "unsat") != NULL || strstr(token, "unknown") != NULL)
        {
          return colouring;
        }
      else if(strstr(token, "sat") != NULL)
        {
	  std::set<int> tempSet;
          for(int i = 0 ; i < nColours ; i++)
            colouring.push_back(tempSet);
        }
      if(strstr(token, "vtx"))
        {
          int vtx_id = strtol(token + 4, NULL, 10);
          fscanf(colouringOut, "%s", token);
          int colour_id = strtol(token + 6, NULL, 10);
          colouring[colour_id].insert(vtx_id);
          printf("Node %d has colour %d\n", vtx_id, colour_id);
        }      
    }
  return colouring;
  fclose(colouringOut);
}

std::vector< std::set<int> > smtColour(std::set<int> vertices, std::vector<std::pair<int, int> > constraints, int nColours, double& colournigTime)
{
  FILE*colouringFile = fopen("colouring.smt2", "w");
  fprintf(colouringFile, "(set-logic QF_UF)\n(set-option :produce-models true)\n(declare-sort U 0)\n");
  for(int i = 0 ; i < nColours ; i ++)
    {
      fprintf(colouringFile, "(declare-fun col%d () U)\n", i);
    }
  fprintf(colouringFile, "(assert (distinct ");
  for(int i = 0 ; i < nColours ; i ++)
    {
      fprintf(colouringFile, "col%d ", i);
    }
  fprintf(colouringFile, "))\n");
  std::set<int>::iterator vertIter= vertices.begin();
  for(int i = 0 ; i < (int)vertices.size() ; i ++)
    {
      fprintf(colouringFile, "(declare-fun vtx%d () U)\n", *vertIter);
      fprintf(colouringFile, "(assert (or ");
      for(int j = 0 ; j < nColours ; j ++)
        {
          fprintf(colouringFile, "(= vtx%d col%d)", *vertIter,j);
        }
      fprintf(colouringFile, "))\n");
      vertIter++;
    }
  for(int i = 0 ; i < (int)constraints.size() ; i ++)
    {
      if(constraints[i].first != constraints[i].second)
        fprintf(colouringFile, "(assert (distinct vtx%d vtx%d))\n", constraints[i].first, constraints[i].second);
    }
  fprintf(colouringFile, "(check-sat)\n(get-value ( ");
  vertIter= vertices.begin();
  for(int i = 0 ; i < (int)vertices.size() ; i ++)
    {
      fprintf(colouringFile, "vtx%d ", *vertIter);
      vertIter++;
    }
  fprintf(colouringFile,"))");
  fclose(colouringFile);  
  system("$Z3_DIR/bin/z3 -smt2 -in -st -T:20 < colouring.smt2 > colouring_out");
  FILE*colouringOut = fopen("colouring_out","r");
  char token[100];
  std::vector< std::set<int> > colouring;
  double colouring_time = -1.0;
  while(fscanf(colouringOut, "%s", token) != EOF)
    {
      if(strstr(token, "unsat") != NULL || strstr(token, "unknown") != NULL)
        {
          return colouring;
        }
      else if(strstr(token, "sat") != NULL)
        {
	  std::set<int> tempSet;
          for(int i = 0 ; i < nColours ; i++)
            colouring.push_back(tempSet);
        }
      if(strstr(token, "vtx"))
        {
          //printf("%s\r\n", token);
          int vtx_id = strtol(strstr(token, "vtx") + 3, NULL, 10);
          //printf("%d\r\n", vtx_id);
          fscanf(colouringOut, "%s", token);
          int colour_id = strtol(token + 6, NULL, 10);
          colouring[colour_id].insert(vtx_id);
          //printf("Node %d has colour %d\n", vtx_id, colour_id);
        }
      if(strcmp(":time",token) == 0)
        {
          fscanf(colouringOut, "%s", token);
          colouring_time = strtod(token, NULL);
        }
    }
 fclose(colouringOut);
 colournigTime = colouring_time;
 return colouring; 
}

std::set<int> customInter(std::set<int> s, std::set<int> t)
  {
    std::set<int> interSet;
    for( std::set< int> ::iterator it = t.begin() ; it != t.end() ; ++it) 
      {
        if(s.find(*it) != s.end())
          {
            interSet.insert(*it);
          }
      }
    /* cout << "Set 1 is "; */
    /* cout << s ; */
    /* cout << endl << "Set 2 is " ; */
    /* cout << t;  */
    /* cout << endl; */
    /* cout << "The intersection is "; */
    /* cout << interSet; */
    /* cout << endl; */
    return interSet;
  }

std::vector<int> customInter(std::vector<int> s, std::vector<int> t)
  {
    std::vector<int> interVec;
    for( std::vector<int> ::iterator it = t.begin() ; it != t.end() ; ++it) 
      {
        if(vectorFind(*it, s) != -1)
          {
            interVec.push_back(*it);
          }
      }
    // cout << "Set 1 is ";
    // cout << s ;
    // cout << endl << "Set 2 is " ;
    // cout << t; 
    // cout << endl;
    // cout << "The intersection is ";
    // cout << interSet;
    // cout << endl;
    return interVec;
  }

std::set<int> customInter(std::set<int> s, std::vector<int > t)
  {
    std::set<int> interSet;
    for( std::vector<int >::iterator it = t.begin() ; it != t.end() ; ++it) 
      {
        if(s.find(*it) != s.end())
          {
            interSet.insert(*it);
          }
      }
    // cout << "Set 1 is ";
    // cout << s ;
    // cout << endl << "Set 2 is " ;
    // cout << t; 
    // cout << endl;
    // cout << "The intersection is ";
    // cout << interSet;
    // cout << endl;
    return interSet;
  }

std::set<int> getVarsOrbits(std::set<int> vars, std::map<int,int> var_to_orbit_map)
{
    std::set<int> varOrbits;
    std::set<int>::iterator var_iter = vars.begin();
    for(int j = 0; j < (int)vars.size() ; j++)
      {
        //printf("Var %d is in orbit %d\r\n", *var_iter, var_to_orbit_map[*var_iter]);
        varOrbits.insert(var_to_orbit_map[*var_iter]);
        var_iter++;
      }
#ifdef debug
    var_iter = vars.begin();
    for(int i = 0; i < vars.size() ; i++)
      {
        std::set<int>::iterator var_iter2 = vars.begin();
        for(int j = 0; j < vars.size() ; j++)
          {
            if(i != j && var_to_orbit_map[*var_iter] == var_to_orbit_map[*var_iter2])
              {
                printf("Var %d and %d are in the same orbit!!", *var_iter, *var_iter2);
              }
            var_iter2++;
          }
        var_iter++;
      }
#endif
    return varOrbits;
}

void operator<<(std::ostream& f, std::set<int> s)
  {
    std::set<int>::iterator s_itr = s.begin();
    for(int i = 0 ; i < (int)s.size() ; i++)
      {
        f << *s_itr << " ";
        s_itr++;
      }
  }

std::set<int> bigUnion(std::vector< std::set<int> > s)
{
    std::set<int> bigUnionRes;
    for(int j = 0; j < (int)s.size() ; j++)
      {
        bigUnionRes = customUnion(bigUnionRes, s[j]);
      }
    return bigUnionRes;
}

std::set<int> bigUnion(std::set< std::set<int> > s)
{
    std::set<int> bigUnionRes;
    std::set< std::set<int> >::iterator s_iter = s.begin();
    for(int j = 0; j < (int)s.size() ; j++)
      {
        bigUnionRes = customUnion(bigUnionRes, *s_iter);
        s_iter++;
      }
    return bigUnionRes;
}

double getTimeSec(clock_t begin, clock_t end)
{
  return (end - begin)/CLOCKS_PER_SEC;
}

double getTimeSec (timespec begin, timespec end)
{
  return (end.tv_sec - begin.tv_sec) + (end.tv_nsec - begin.tv_nsec)/1000000000.0;
}


char interEmpty(unsigned int arr1[], unsigned int size1, unsigned int arr2[],  unsigned int size2)
{
  // printf("Size1=%d\r\n", size1);
  // printf("Size2=%d\r\n", size2);
  // for(unsigned int i = 0; i < size1 ; i++)
  //   {
  //     printf("arr1[i] = %d ", arr1[i]);
  //   }
  // printf("\r\n");
  // for(unsigned int j = 0; j < size2 ; j++)
  //   {
  //     printf("arr2[j] = %d ", arr2[j]);
  //   }
  // printf("\r\n");
  for(unsigned int i = 0; i < size1 ; i++)
    {
      for(unsigned int j = 0; j < size2 ; j++)
        {
          if(arr1[i] == arr2[j])
            return 0;
        }
    }
  return 1;
}
