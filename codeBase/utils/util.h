#ifndef UTIL_H
#define UTIL_H

#include <utility>
#include <set>
#include <vector>
#include <map>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

std::pair<const char*, int> getFluentNameAndAss(int varId);
template<typename typeName> bool subset(std::set<typeName> s, std::set<typeName> t)
{
  std::set<int>::iterator s_iter = s.begin();
  for(size_t i = 0 ; i < s.size() ; i++)
    {
      if(t.find(*s_iter) == t.end())
        return false;
      s_iter++;
    }
  return true;
}


template<typename typeName> bool subset(std::vector<typeName> s, std::set<typeName> t)
{
  for(size_t i = 0 ; i < s.size() ; i++)
    {
      if(t.find(s[i]) == t.end())
        return false;
    }
  return true;
}

template<typename typeName> std::set<typeName> setMinus(std::set<typeName> s, std::set<typeName> t)
{
    std::set<int> diff;
    std::set<int>::iterator s_iter= s.begin();
    for(int j = 0; j < (int)s.size() ; j++)
      {
        if(t.find(*s_iter) == t.end())
          {
            diff.insert(*s_iter);
          }
        s_iter++;
      }
    return diff;
}

template<typename typeName> std::vector<typeName> setMinus(std::vector<typeName> s, std::set<typeName> t)
{
    std::vector<typeName> diff;
    for(int j = 0; j < (int)s.size() ; j++)
      {
        if(t.find(s[j]) == t.end())
          {
            diff.push_back(s[j]);
          }
      }
    return diff;
}

template <typename typeName> int vectorFind(typeName obj, std::vector<typeName> vec)
{
  //cout << "Trying to find " << obj << endl;
  int index = -1;
  for(size_t i = 0 ; i < vec.size() ; i++)
    {
      if(vec[i] == obj)
        {
          //cout << "Required object found at index " << i << endl;
          index = i;
          return index;
        }
    }
  return index;
}

template<typename typeName> std::set<typeName > setMinus(std::set<typeName> s, std::vector<typeName> t)
{
    std::set<typeName> diff;
    std::set<int>::iterator s_iter = s.begin();
    for(size_t j = 0; j < s.size() ; j++)
      {
	if(vectorFind(*s_iter, t) == -1)
          {
            diff.insert(*s_iter);
          }
        s_iter++;
      }
    return diff;
}


template <typename typeName> std::set<typeName> customUnion(std::set<typeName> s, std::set<typeName> t)
  {
    for( typename std::set<typeName> ::iterator it = t.begin() ; it != t.end() ; ++it) 
      {
        s.insert(*it);
      }
    return s;
  }

template <typename typeName> std::set<typeName> customUnion(std::set<typeName> s, std::vector<typeName> t)
  {
    for(size_t i = 0 ; i < t.size() ; i++) 
      {
        s.insert(t[i]);
      }
    return s;
  }

template <typename typeName> std::vector<typeName> customUnion(std::vector<typeName> s, std::vector<typeName> t)
  {
    std::set<typeName> tempSet;
    for(size_t i = 0 ; i < t.size() ; i++) 
      {
        tempSet.insert(t[i]);
      }
    for(size_t i = 0 ; i < s.size() ; i++) 
      {
        tempSet.insert(s[i]);
      }
    std::vector<typeName> retVec;
    for( typename std::set<typeName> ::iterator it = tempSet.begin() ; it != tempSet.end() ; ++it) 
      {
        retVec.push_back(*it);
      }
    return retVec;
  }

template <typename typeName> std::vector<typeName>  vectorFilter(std::vector<typeName> vect, std::vector<int> predicate)
  {
    std::vector<typeName> retVector;
    for(int i = 0 ; i < (int)vect.size() ; i++)
      {
        if(vectorFind<int>(i, predicate) != -1)
          retVector.push_back(vect[i]);
      }
    return retVector;
  }


template <typename typeName> std::vector<typeName> vectorFilter(std::vector<typeName> vect, std::set<int> predicate)
  {
#ifdef verbose
    printf("Fitlering vector\r\n");
#endif
    std::vector<typeName> retVector;
    for(int i = 0 ; i < (int)vect.size() ; i++)
      {
        if(predicate.find(i) != predicate.end())
          retVector.push_back(vect[i]);
      }
    return retVector;
  }

template <typename typeName1, typename typeName2> std::map<typeName2, typeName1> mapInverse(std::map<typeName1, typeName2> fn)
  {
    std::map<typeName2, typeName1> invFn;
    typename std::map<typeName1, typeName2>::iterator iter = fn.begin();
    for(int i = 0 ; i < (int)fn.size() ; i++)
      {
        invFn[(iter)->second] = (iter)->first;
        iter++;
      }
    return invFn;
  }


template <typename typeName1, typename typeName2> std::map<typeName1, typeName2> mapDomFilter(std::map<typeName1, typeName2> mapIn, std::set<typeName1> predicate)
  {
#ifdef verbose
    printf("Fitlering map\r\n");
#endif
    std::map<typeName1, typeName2> retMap;
    typename std::set<typeName1>::iterator predIter = predicate.begin();
    for(int i = 0 ; i < (int)predicate.size() ; i++)
      {
        if(mapIn.find(*predIter) != mapIn.end())
          retMap[*predIter] = mapIn[*predIter];
        predIter++;
      }
    return retMap;
  }

template<typename typeName> std::map<int, typeName> vectorToMap(std::vector<typeName> vect)
{
 std::map<int, typeName> retMap;
 for(int i = 0 ; i < (int)vect.size() ; i++)
   {
     retMap[i] = vect[i];
   }
 return retMap;
}

template<typename typeName2> std::set<typeName2> vectorToSet(std::vector<typeName2> vect)
{
 std::set<typeName2> retSet;
 for(int i = 0 ; i < (int)vect.size() ; i++)
   {
     retSet.insert(vect[i]);
   }
 return retSet;
}

template<typename typeName1, typename typeName2> std::map<typeName1, typeName2> 
mapErase(std::map<typeName1, typeName2> mapIn, std::set<typeName1> eraseSet)
{
  typename std::set<typeName1>::iterator eraseSetIter = eraseSet.begin();
  for(int i = 0 ; i < (int)eraseSet.size() ; i++)
    {
      mapIn.erase(*eraseSetIter);
      eraseSetIter++;
    }
  return mapIn;
}

template<typename typeName> std::vector<typeName> vectorConcat(std::vector<typeName> vec1, std::vector<typeName> vec2)
{
  std::vector<typeName> retVec = vec1;
  for(int i = 0 ; i < (int)vec2.size() ; i ++)
    retVec.push_back(vec2[i]);
  return retVec;
}

template<typename typeName> std::set<typeName> bigUnion(std::vector< std::set<typeName> > s)
{
    std::set<typeName> bigUnionRes;
    for(int j = 0; j < (int)s.size() ; j++)
      {
        bigUnionRes = customUnion(bigUnionRes, s[j]);
      }
    return bigUnionRes;
}

template<typename typeName> std::set<typeName> bigUnion(std::set< std::set<typeName> > s)
{
    std::set<typeName> bigUnionRes;
    typename std::set< std::set<typeName> >::iterator s_iter = s.begin();
    for(int j = 0; j < (int)s.size() ; j++)
      {
        bigUnionRes = customUnion(bigUnionRes, *s_iter);
        s_iter++;
      }
    return bigUnionRes;
}

template<typename typeName> std::set<typeName> bigUnion(std::set< std::vector<typeName> > S)
{
    typename std::set< std::vector<typeName> >::iterator S_iter = S.begin();
    std::set<std::set<typeName> > tempS;
    for(int j = 0; j < (int)S.size() ; j++)
      {
        tempS.insert(vectorToSet(*S_iter));
        S_iter++;
      }
    return bigUnion(tempS);
}


std::vector< std::set<int> > smtColour(int nVertices, std::vector<std::pair<int, int> > constraints, int nColours);
/* std::vector< std::set<int> > smtColour(std::set<int> vertices, std::vector<std::pair<int, int> > constraints, int nColours); */
std::vector< std::set<int> > smtColour(std::set<int> vertices, std::vector<std::pair<int, int> > constraints, int nColours, double& colournigTime);
std::set<int> customInter(std::set<int> s, std::set<int> t);
std::vector<int> customInter(std::vector<int> s, std::vector<int> t);
std::set<int> customInter(std::set<int> s, std::vector<int > t);
std::set<int> getVarsOrbits(std::set<int> vars, std::map<int,int> var_to_orbit_map);
void operator<<(std::ostream& f, std::set<int> s);
std::set<int> bigUnion(std::vector< std::set<int> > s);
std::set<int> bigUnion(std::set< std::set<int> > s);

template<typename typeName1, typename typeName2> std::set<typeName2> coDom(std::map<typeName1, typeName2> fn)
{
  std::set<typeName2> codom;
  typename std::map<typeName1, typeName2>::iterator fnIter = fn.begin();
  for(int i = 0 ; i < (int)fn.size() ; i++)
    {
      codom.insert(fnIter->second);
      fnIter++;
    }
  return codom;
}

template<typename typeName1, typename typeName2> std::set<typeName1> dom(std::map<typeName1, typeName2> fn)
{
  std::set<typeName1> dom;
  typename std::map<typeName1, typeName2>::iterator fnIter = fn.begin();
  for(int i = 0 ; i < (int)fn.size() ; i++)
    {
      dom.insert(fnIter->first);
      fnIter++;
    }
  return dom;
}

template<typename typeName1, typename typeName2> void printMaplet(std::pair<typeName1, typeName2> p)
{
  std::cout<< p.first << "->" << p.second << std::endl;
}

template<typename typeName1, typename typeName2> void printMap(std::map<typeName1, typeName2> fn)
{
  typename std::map<typeName1, typeName2>::iterator fnIter = fn.begin();
  for(int i = 0 ; i < (int)fn.size() ; i++)
    {
      printMaplet(*fnIter);
      /* std::cout<< fnIter->first << "->" << fnIter->second << std::endl; */
      fnIter++;
    }
}

template<typename typeName1, typename typeName2> std::map<typeName1, int> keyToIndexMap(std::map<typeName1, typeName2> fn)
{
  std::map<typeName1, int> keyToIndex;
  int count = 0;
  for(typename std::map<typeName1, typeName2>::iterator i =  fn.begin();
      i != fn.end(); i++)
    {
      keyToIndex[i->first] = count;
      count++;
    }
  return keyToIndex;
}

template<typename typeName1, typename typeName2> std::map<typeName2, int> valueToIndexMap(std::map<typeName1, typeName2> fn)
{
  std::map<typeName2, int> valToIndex;
  int count = 0;
  for(typename std::map<typeName1, typeName2>::iterator i =  fn.begin();
      i != fn.end(); i++)
    {
      valToIndex[i->second] = count;
      count++;
    }
  return valToIndex;
}


template<typename typeName> void printSet(std::set<typeName> s)
{
  typename std::set<typeName>::iterator sIter = s.begin();
  for(int i = 0 ; i < (int)s.size() ; i++)
    {
      std::cout<< *sIter << std::endl;
      sIter++;
    }
}

template<typename typeName> void printVector(std::vector<typeName> vec)
{
  typename std::vector<typeName>::iterator vecIter = vec.begin();
  for(int i = 0 ; i < (int)vec.size() ; i++)
    {
      std::cout<< *vecIter << std::endl;
      vecIter++;
    }
}

template<typename typeName1, typename typeName2> std::vector<typeName2> imageVector(std::map<typeName1,typeName2> f, std::vector<typeName1> vin)
{
  std::vector<typeName2> vout;
  for(int j = 0; j < (int)vin.size() ; j++)
    {
      if(f.find(vin[j]) != f.end())
        vout.push_back(f[vin[j]]);
      else
        {
          printf("Corrupt function, exiting!!!\r\n");
          exit(1);
        }
    }
  return vout;
}


template<typename typeName> std::vector<typeName> setToVector(std::set<typeName> sin)
{
  std::vector<typeName> vout;
  typename std::set<typeName>::iterator s_iter = sin.begin();
  for(size_t i = 0 ; i < sin.size() ; i++)
    {
      vout.push_back(*s_iter);
      s_iter++;
    }
  return vout;
}

template<typename typeName1, typename typeName2> std::set<typeName2> imageSet(std::map<typeName1,typeName2> f, std::set<typeName1> sin)
{
  return vectorToSet(imageVector(f, setToVector(sin)));
}

template<typename typeName1> std::set<typeName1> customInter(std::set<typeName1> s, std::set<typeName1> t)
  {
    std::set<typeName1> interSet;
    for( typename std::set<typeName1> ::iterator it = t.begin() ; it != t.end() ; ++it) 
      {
        if(s.find(*it) != s.end())
          {
            interSet.insert(*it);
          }
      }
    /* cout << "Set 1 is "; */
    /* cout << s ; */
    /* cout << endl << "Set 2 is " ; */
    /* cout << t;  */
    /* cout << endl; */
    /* cout << "The intersection is "; */
    /* cout << interSet; */
    /* cout << endl; */
    return interSet;
  }

template<typename typeName1> std::vector<typeName1> customInter(std::vector<typeName1> s, std::vector<typeName1> t)
  {
    std::vector<typeName1> interVec;
    for( typename std::vector<typeName1> ::iterator it = t.begin() ; it != t.end() ; ++it) 
      {
        if(vectorFind(*it, s) != -1)
          {
            interVec.push_back(*it);
          }
      }
    // cout << "Set 1 is ";
    // cout << s ;
    // cout << endl << "Set 2 is " ;
    // cout << t; 
    // cout << endl;
    // cout << "The intersection is ";
    // cout << interSet;
    // cout << endl;
    return interVec;
  }

template<typename typeName1> std::set<typeName1> customInter(std::set<typeName1> s, std::vector<typeName1> t)
  {
    std::set<typeName1> interSet;
    for( typename std::vector<typeName1>::iterator it = t.begin() ; it != t.end() ; ++it) 
      {
        if(s.find(*it) != s.end())
          {
            interSet.insert(*it);
          }
      }
    // cout << "Set 1 is ";
    // cout << s ;
    // cout << endl << "Set 2 is " ;
    // cout << t; 
    // cout << endl;
    // cout << "The intersection is ";
    // cout << interSet;
    // cout << endl;
    return interSet;
  }


double getTimeSec(clock_t begin, clock_t end);
double getTimeSec (timespec begin, timespec end);

char interEmpty(unsigned int arr1[], unsigned int size1, unsigned int arr2[],  unsigned int size2);

#endif
