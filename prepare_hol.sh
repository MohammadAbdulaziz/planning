#!/bin/bash

# #This script assumes that you have polyml
rm -rf HOL/
git clone https://github.com/HOL-Theorem-Prover/HOL
cd HOL
git checkout b6750c75a1260f3eba15073dfb32b9daf8da4efa
poly < tools/smart-configure.sml
bin/build
cd ..
chmod +x *.sh thesis/*.sh papers/*/*.sh
./clean_hol.sh
