#!/bin/bash
cd hol_sources/
../HOL/bin/Holmake cleanAll
cd ../thesis
../HOL/bin/Holmake cleanAll
cd ../
cd papers
for dir in `ls -d */`; do cd  $dir ; ../../HOL/bin/Holmake cleanAll; cd ../; done
