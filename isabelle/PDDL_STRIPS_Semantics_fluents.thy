section \<open>PDDL and STRIPS Semantics\<close>
theory PDDL_STRIPS_Semantics_fluents
imports
  "Propositional_Proof_Systems.Formulas"
  "Propositional_Proof_Systems.Sema"
  "Propositional_Proof_Systems.Consistency"
  "Automatic_Refinement.Misc"
  "Automatic_Refinement.Refine_Util"
  Option_Monad_Add
begin
no_notation insert ("_ \<triangleright> _" [56,55] 55)

subsection \<open>Utility Functions\<close>
definition "index_by f l \<equiv> map_of (map (\<lambda>x. (f x,x)) l)"

lemma index_by_eq_Some_eq[simp]:
  assumes "distinct (map f l)"
  shows "index_by f l n = Some x \<longleftrightarrow> (x\<in>set l \<and> f x = n)"
  unfolding index_by_def
  using assms
  by (auto simp: o_def)

lemma index_by_eq_SomeD:
  shows "index_by f l n = Some x \<Longrightarrow> (x\<in>set l \<and> f x = n)"
  unfolding index_by_def
  by (auto dest: map_of_SomeD)


lemma lookup_zip_idx_eq:
  assumes "length params = length args"
  assumes "i<length args"
  assumes "distinct params"
  assumes "k = params ! i"
  shows "map_of (zip params args) k = Some (args ! i)"
  using assms
  by (auto simp: in_set_conv_nth)

lemma rtrancl_image_idem[simp]: "R\<^sup>* `` R\<^sup>* `` s = R\<^sup>* `` s"
  by (metis relcomp_Image rtrancl_idemp_self_comp)


subsection \<open>Abstract Syntax\<close>

subsubsection \<open>Generic Entities\<close>
type_synonym name = String.literal

datatype predicate = Pred (name: name)
datatype "fun_name" = Fun_Name (name: name)

text \<open>Some of the AST entities are defined over a polymorphic \<open>'val\<close> type,
  which gets either instantiated by variables (for domains)
  or objects (for problems).
\<close>

text \<open>An atom is either a predicate with arguments, or an equality statement.\<close>
(* TODO: atomic_formula in Kovacs *)
datatype 'val atom = predAtm (predicate: predicate) (arguments: "'val list")
                     | Eq (lhs: 'val) (rhs: 'val)

text \<open>A type is a list of primitive type names.
  To model a primitive type, we use a singleton list.\<close>
datatype type = Either (primitives: "name list")


text \<open>Variables are identified by their names.\<close>
datatype variable = varname: Var name
text \<open>Objects and constants are identified by their names\<close>
datatype object = Obj (name: name)

datatype varobj = VAR variable | CONST object
hide_const (open) VAR CONST \<comment> \<open>Refer to constructors by qualified names only\<close>

datatype 'val "term" =
  ENT 'val
| FUN "fun_name" "'val term list"

datatype 'val assignment = ASSIGN fun_name "'val list" "'val option"

text \<open>An effect contains a list of values to be added, and a list of values
  to be removed.\<close>
datatype 'val ast_effect = Effect
  (adds: "(predicate \<times> 'val list) list")
  (dels: "(predicate \<times> 'val list) list")
  (assigns: "'val assignment list")






subsubsection \<open>Domains\<close>

text \<open>An action schema has a name, a typed parameter list, a precondition,
  and an effect.\<close>
datatype ast_action_schema = Action_Schema
  (name: name)
  (parameters: "(variable \<times> type) list")
  (precondition: "varobj term atom formula")
  (effect: "varobj term ast_effect")

text \<open>A predicate declaration contains the predicate's name and its
  argument types.\<close>
datatype predicate_decl = PredDecl
  (pred: predicate)
  (argTs: "type list")

text \<open>A domain contains the declarations of primitive types, predicates,
  and action schemas.\<close>
datatype ast_domain = Domain
  (types: "(name \<times> name) list") \<comment> \<open> \<open>(type, supertype)\<close> declarations. \<close>
  (predicates: "predicate_decl list")
  ("consts": "(object \<times> type) list")
  ("functions": "(fun_name \<times> (type list \<times> type)) list")
  (actions: "ast_action_schema list")

subsubsection \<open>Problems\<close>


text \<open>A fact is a predicate applied to objects.\<close>
type_synonym fact = "predicate \<times> object list"

datatype init_el =
  is_atom: ATOM predicate "object list"
| is_funval: FUNVAL fun_name "object list" object


text \<open>A problem consists of a domain, a list of objects,
  a description of the initial state, and a description of the goal state. \<close>
datatype ast_problem = Problem
  (domain: ast_domain)
  (objects: "(object \<times> type) list")
  (init: "init_el list")
  (goal: "object term atom formula")


subsubsection \<open>Plans\<close>
datatype plan_action = PAction
  (name: name)
  (arguments: "object list")

type_synonym plan = "plan_action list"

subsubsection \<open>Ground Actions\<close>
text \<open>The following datatype represents an action scheme that has been
  instantiated by replacing the arguments with concrete objects,
  also called ground action.
\<close>
datatype ast_action_inst = Action_Inst
  (precondition: "object term atom formula")
  (effect: "object term ast_effect")



subsection \<open>Closed-World Assumption, Equality, and Negation\<close>
  text \<open>Discriminator for atomic predicate formulas.\<close>
  fun is_predAtom where
    "is_predAtom (Atom (predAtm _ _)) = True" | "is_predAtom _ = False"


  text \<open>The basic world model is a set of ground predicates. 
    To form a world model, we will add a fluent valuation.
  \<close>
  type_synonym basic_world_model = "(predicate \<times> object list) set"

  text \<open>A valuation extracted from the atoms of the world model\<close>
  definition valuation :: "basic_world_model \<Rightarrow> object atom valuation"
    where "valuation M \<equiv> \<lambda>predAtm p xs \<Rightarrow> (p, xs) \<in> M | Eq a b \<Rightarrow> a=b"

  text \<open>Augment a world model by adding negated versions of all atoms
    not contained in it, as well as interpretations of equality.\<close>
  definition "close_basic_world M = (
      {Atom (predAtm p as) | p as. (p,as)\<in>M }
    \<union> {\<^bold>\<not>(Atom (predAtm p as)) | p as. (p,as)\<notin>M })
    \<union> {Atom (Eq a a) | a. True}
    \<union> {\<^bold>\<not>(Atom (Eq a b)) | a b. a\<noteq>b}"

  (*
  lemma
    close_basic_world_extensive: "M \<subseteq> close_basic_world M" and
    close_basic_world_idem[simp]: "close_basic_world (close_basic_world M) = close_basic_world M"
    by (auto simp: close_basic_world_def)
  *)

  lemma in_close_basic_world_conv:
    "\<phi> \<in> close_basic_world M \<longleftrightarrow> (
        (\<exists>p as. \<phi>=(Atom (predAtm p as)) \<and> (p,as)\<in>M)
      \<or> (\<exists>p as. \<phi>=\<^bold>\<not>(Atom (predAtm p as)) \<and> (p,as)\<notin>M)
      \<or> (\<exists>a. \<phi>=Atom (Eq a a))
      \<or> (\<exists>a b. \<phi>=\<^bold>\<not>(Atom (Eq a b)) \<and> a\<noteq>b)
    )"
    by (auto simp: close_basic_world_def)

  lemma valuation_aux_1:
    fixes M :: basic_world_model and \<phi> :: "object atom formula"
    defines "C \<equiv> close_basic_world M"
    assumes A: "\<forall>\<phi>\<in>C. \<A> \<Turnstile> \<phi>"
    shows "\<A> = valuation M"
    using A unfolding C_def
    apply -
    apply (auto simp: in_close_basic_world_conv valuation_def Ball_def intro!: ext split: atom.split)
    apply (metis formula_semantics.simps(1) formula_semantics.simps(3))
    apply (metis formula_semantics.simps(1) formula_semantics.simps(3))
    by (metis atom.collapse(2) formula_semantics.simps(1) is_predAtm_def)



  lemma valuation_aux_2:
    shows "(\<forall>G\<in>close_basic_world M. valuation M \<Turnstile> G)"
    by (force simp: in_close_basic_world_conv valuation_def elim: is_predAtom.elims)

  lemma val_imp_close_basic_world: "valuation M \<Turnstile> \<phi> \<Longrightarrow> close_basic_world M \<TTurnstile> \<phi>"
    unfolding entailment_def
    using valuation_aux_1
    by blast

  lemma close_basic_world_imp_val:
    "close_basic_world M \<TTurnstile> \<phi> \<Longrightarrow> valuation M \<Turnstile> \<phi>"
    unfolding entailment_def using valuation_aux_2 by blast

  text \<open>Main theorem of this section:
    If a world model \<open>M\<close> contains only atoms, its induced valuation
    satisfies a formula \<open>\<phi>\<close> if and only if the closure of \<open>M\<close> entails \<open>\<phi>\<close>.

    Note that there are no syntactic restrictions on \<open>\<phi>\<close>,
    in particular, \<open>\<phi>\<close> may contain negation.
  \<close>
  theorem valuation_iff_close_basic_world:
    shows "valuation M \<Turnstile> \<phi> \<longleftrightarrow> close_basic_world M \<TTurnstile> \<phi>"
    using val_imp_close_basic_world close_basic_world_imp_val by blast



subsection \<open>STRIPS Semantics\<close>


type_synonym fluent_valuation = "fun_name \<times> object list \<rightharpoonup> object"

type_synonym world_model = "fluent_valuation \<times> basic_world_model"

(* Note: Goes from set of valid predicates to set of valid atomic 
  formulas (including negations and equalities)
*)
definition close_world :: "world_model \<Rightarrow> fluent_valuation \<times> object atom formula set" where
  "close_world = apsnd close_basic_world"


context
  fixes F :: fluent_valuation
begin

(* Transform terms to objects *)
fun feval_term :: "object term \<rightharpoonup> object" where
  "feval_term (ENT x) = Some x"
| "feval_term (FUN f xs) = do {
    xs \<leftarrow> omap feval_term xs;
    F (f,xs)
  }"

(* Transform effects, assignments, atoms, and formula from terms to objects *)
definition "feval_ast_effect \<equiv> omap_dt set_ast_effect map_ast_effect feval_term"
(*definition "feval_assignment \<equiv> omap_dt set_assignment map_assignment feval_term"*)
definition "feval_atom \<equiv> omap_dt set_atom map_atom feval_term"
definition "feval_atom_formula \<equiv> omap_dt formula.atoms map_formula feval_atom"

end

(* TODO: Must not silently be false on error! *)
fun f_entailment (*("(_ \<TTurnstile>\<^sub>f/ _)" [53,53] 53)*) where
  "f_entailment (F,P) \<phi> = do {
    \<phi> \<leftarrow> feval_atom_formula F \<phi>;
    Some (close_basic_world P \<TTurnstile> \<phi>)
  }"

definition "f_check_entails s \<phi> \<equiv> do { b \<leftarrow> f_entailment s \<phi>; oassert b }"  

text \<open>For this section, we fix a domain \<open>D\<close>, using Isabelle's
  locale mechanism.\<close>
locale ast_domain =
  fixes D :: ast_domain
begin

  fun apply_basic_assignment where
    "apply_basic_assignment (ASSIGN f xs y) F = F((f,xs):=y)"

  text \<open>It seems to be agreed upon that, in case of a contradictory effect,
    addition overrides deletion. We model this behaviour by first executing
    the deletions, and then the additions.

    TODO: The assignments are currently applied independently of additions and deletions
    \<close>
  fun apply_basic_effect
    :: "object ast_effect \<Rightarrow> world_model \<Rightarrow> world_model"
  where
     "apply_basic_effect (Effect a d ass) (F,P) = (fold apply_basic_assignment ass F, (P - (set d)) \<union> (set a))"

  text \<open>Execute an action instance\<close>
  fun execute_ast_action_inst
    :: "ast_action_inst \<Rightarrow> world_model \<rightharpoonup> world_model"
  where
    "execute_ast_action_inst (Action_Inst pre eff) (F,P) = do {
      f_check_entails (F,P) pre;  \<comment> \<open>Ensure that precondition matches\<close>
      \<^cancel>\<open> TODO: define apply-effect function! \<close>
      eff \<leftarrow> feval_ast_effect F eff; \<comment> \<open>Evaluate functions in effect\<close>
      Some (apply_basic_effect eff (F,P)) \<comment> \<open>Apply resulting effect (which only contains objects now)\<close>
    }"

  text \<open>Predicate to model that the given list of action instances is
    executable, and transforms an initial world model \<open>M\<close> into a final
    model \<open>M'\<close>.

    Note that this definition over the list structure is more convenient in HOL
    than to explicitly define an indexed sequence \<open>M\<^sub>0\<dots>M\<^sub>N\<close> of intermediate world
     models, as done in [Lif87].
  \<close>
  fun execute_ast_action_inst_path :: "ast_action_inst list \<Rightarrow> world_model \<rightharpoonup> world_model" where
    "execute_ast_action_inst_path [] M = Some M"
  | "execute_ast_action_inst_path (\<alpha>#\<alpha>s) M = do {
      M \<leftarrow> execute_ast_action_inst \<alpha> M;
      execute_ast_action_inst_path \<alpha>s M
    }"

end \<comment> \<open>Context of \<open>ast_domain\<close>\<close>



subsection \<open>Well-Formedness of PDDL\<close>

(* Well-formedness *)

fun ty_varobj where
  "ty_varobj varT objT (varobj.VAR v) = varT v"
| "ty_varobj varT objT (varobj.CONST c) = objT c"


lemma ty_varobj_mono: "varT \<subseteq>\<^sub>m varT' \<Longrightarrow> objT \<subseteq>\<^sub>m objT' \<Longrightarrow>
  ty_varobj varT objT \<subseteq>\<^sub>m ty_varobj varT' objT'"
  apply (rule map_leI)
  subgoal for x v
    apply (cases x)
    apply (auto dest: map_leD)
    done
  done


context ast_domain begin

  text \<open>The signature is a partial function that maps the predicates
    of the domain to lists of argument types.\<close>
  definition sig :: "predicate \<rightharpoonup> type list" where
    "sig \<equiv> map_of (map (\<lambda>PredDecl p n \<Rightarrow> (p,n)) (predicates D))"

  definition fsig :: "fun_name \<rightharpoonup> (type list \<times> type)" where
    "fsig \<equiv> map_of (functions D)"


  fun subtype_edge where
    "subtype_edge (ty,superty) = (superty,ty)"

  definition "subtype_rel \<equiv> set (map subtype_edge (types D))"

  definition of_type :: "type \<Rightarrow> type \<Rightarrow> bool" where
    "of_type oT T \<equiv> set (primitives oT) \<subseteq> subtype_rel\<^sup>* `` set (primitives T)"
  text \<open>This checks that every primitive on the LHS is contained in or a
    subtype of a primitive on the RHS\<close>


  text \<open>For the next few definitions, we fix a partial function that maps
    a polymorphic entity type @{typ "'e"} to types. An entity can be
    instantiated by variables or objects later.\<close>
  context
    fixes ty_ent :: "'ent \<rightharpoonup> type"  \<comment> \<open>Entity's type, None if invalid\<close>
  begin

    text \<open>Typecheck term, and return its type\<close>
    fun ty_term :: "'ent term \<rightharpoonup> type" where
      "ty_term (term.ENT x) = ty_ent x"
    | "ty_term (term.FUN f xs) = do {
        (Ts,T) \<leftarrow> fsig f;
        vTs \<leftarrow> omap ty_term xs;
        oassert (list_all2 of_type vTs Ts);
        Some T
       }"

    fun wf_pred_atom :: "predicate \<times> 'ent list \<Rightarrow> bool" where
      "wf_pred_atom (p,vs) \<longleftrightarrow> do {
        Ts \<leftarrow> sig p;
        vTs \<leftarrow> omap ty_ent vs;
        oassert (list_all2 of_type vTs Ts)
      } \<noteq> None"

    text \<open>Predicate-atoms are well-formed if their arguments match the
      signature, equalities are well-formed if the arguments are valid
      objects (have a type).

      TODO: We could check that types may actually overlap
    \<close>
    fun wf_atom :: "'ent atom \<Rightarrow> bool" where
      "wf_atom (predAtm p vs) \<longleftrightarrow> wf_pred_atom (p,vs)"
    | "wf_atom (Eq a b) \<longleftrightarrow> ty_ent a \<noteq> None \<and> ty_ent b \<noteq> None"


    text \<open>A formula is well-formed if it consists of valid atoms.
    \<close>
    fun wf_fmla :: "'ent atom formula \<Rightarrow> bool" where
      "wf_fmla (Atom a) \<longleftrightarrow> wf_atom a"
    | "wf_fmla (\<bottom>) \<longleftrightarrow> True"
    | "wf_fmla (\<phi>1 \<^bold>\<and> \<phi>2) \<longleftrightarrow> (wf_fmla \<phi>1 \<and> wf_fmla \<phi>2)"
    | "wf_fmla (\<phi>1 \<^bold>\<or> \<phi>2) \<longleftrightarrow> (wf_fmla \<phi>1 \<and> wf_fmla \<phi>2)"
    | "wf_fmla (\<^bold>\<not>\<phi>) \<longleftrightarrow> wf_fmla \<phi>"
    | "wf_fmla (\<phi>1 \<^bold>\<rightarrow> \<phi>2) \<longleftrightarrow> (wf_fmla \<phi>1 \<and> wf_fmla \<phi>2)"

    lemma "wf_fmla \<phi> = (\<forall>a\<in>atoms \<phi>. wf_atom a)"
      by (induction \<phi>) auto

    text \<open>Special case for a well-formed atomic predicate formula\<close>
    fun wf_fmla_atom where
      "wf_fmla_atom (Atom (predAtm a vs)) \<longleftrightarrow> wf_pred_atom (a,vs)"
    | "wf_fmla_atom _ \<longleftrightarrow> False"

    lemma wf_fmla_atom_alt: "wf_fmla_atom \<phi> \<longleftrightarrow> is_predAtom \<phi> \<and> wf_fmla \<phi>"
      by (cases \<phi> rule: wf_fmla_atom.cases) auto

    definition "wf_funval f xs y \<equiv> do {
        (Ts,T) \<leftarrow> fsig f;
        vTs \<leftarrow> omap ty_ent xs;
        oassert (list_all2 of_type vTs Ts);
        case y of
          None \<Rightarrow> Some ()
        | Some y \<Rightarrow> do {
            yT \<leftarrow> ty_ent y;
            oassert (of_type yT T)
          }
      } \<noteq> None"  
      
    fun wf_assignment where
      "wf_assignment (ASSIGN f xs y) \<longleftrightarrow> wf_funval f xs y"

    text \<open>An effect is well-formed if the added and removed formulas
      are atomic\<close>
    fun wf_effect where
      "wf_effect (Effect a d ass) \<longleftrightarrow>
          (\<forall>ae\<in>set a. wf_pred_atom ae)
        \<and> (\<forall>de\<in>set d.  wf_pred_atom de)
        \<and> (\<forall>ae\<in>set ass. wf_assignment ae)"

  end \<comment> \<open>Context fixing \<open>ty_ent\<close>\<close>

  lemma ty_term_mono: "entT \<subseteq>\<^sub>m entT' \<Longrightarrow> ty_term entT \<subseteq>\<^sub>m ty_term entT'"
    apply (rule map_leI)
    subgoal for t T
    proof (induction t arbitrary: T rule: ty_term.induct)
      case (1 x)
      then show ?case by (auto dest: map_leD split: Option.bind_splits)
    next
      case (2 f xs)
      from "2.prems" obtain xsT rT where [simp]: "fsig f = Some (xsT,rT)"
        by (auto split: Option.bind_splits)

      note IH = "2.IH"[OF this refl _ \<open>entT \<subseteq>\<^sub>m entT'\<close>]

      have "\<forall>ys. omap (ty_term entT) xs = Some ys \<longrightarrow>  omap (ty_term entT') xs = Some ys"
        using IH by (induction xs) (auto dest: map_leD split: Option.bind_splits)
      with "2.prems" show ?case by (auto dest: map_leD split: Option.bind_splits)
    qed
    done




  definition constT :: "object \<rightharpoonup> type" where
    "constT \<equiv> map_of (consts D)"

  text \<open>An action schema is well-formed if the parameter names are distinct,
    and the precondition and effect is well-formed wrt.\ the parameters.
  \<close>
  fun wf_action_schema :: "ast_action_schema \<Rightarrow> bool" where
    "wf_action_schema (Action_Schema n params pre eff) \<longleftrightarrow> (
      let
        tyt = ty_term (ty_varobj (map_of params) constT)
      in
        distinct (map fst params)
      \<and> wf_fmla tyt pre
      \<and> wf_effect tyt eff)"

  text \<open>A type is well-formed if it consists only of declared primitive types,
     and the type object.\<close>
  fun wf_type where
    "wf_type (Either Ts) \<longleftrightarrow> set Ts \<subseteq> insert STR ''object'' (fst`set (types D))"

  text \<open>A predicate is well-formed if its argument types are well-formed.\<close>
  fun wf_predicate_decl where
    "wf_predicate_decl (PredDecl p Ts) \<longleftrightarrow> (\<forall>T\<in>set Ts. wf_type T)"

  text \<open>The types declaration is well-formed, if all supertypes are declared types (or object)\<close>
  definition "wf_types \<equiv> snd`set (types D) \<subseteq> insert STR ''object'' (fst`set (types D))"

  fun wf_fun_sig where
    "wf_fun_sig (Ts,T) \<longleftrightarrow> (\<forall>T\<in>set Ts. wf_type T) \<and> wf_type T"


  text \<open>A domain is well-formed if
    \<^item> there are no duplicate declared predicate names,
    \<^item> all declared predicates are well-formed,
    \<^item> there are no duplicate action names,
    \<^item> and all declared actions are well-formed
    \<close>
  definition wf_domain :: "bool" where
    "wf_domain \<equiv>
      wf_types
    \<and> distinct (map (predicate_decl.pred) (predicates D))
    \<and> (\<forall>p\<in>set (predicates D). wf_predicate_decl p)
    \<and> distinct (map fst (consts D))  \<comment>\<open>Required by current semantics that assigns each object a unique type\<close>
    \<and> (\<forall>(n,T)\<in>set (consts D). wf_type T)
    \<and> distinct (map fst (functions D))
    \<and> (\<forall>(n,sg)\<in>set (functions D). wf_fun_sig sg)
    \<and> distinct (map ast_action_schema.name (actions D))
    \<and> (\<forall>a\<in>set (actions D). wf_action_schema a)
    "

end \<comment> \<open>locale \<open>ast_domain\<close>\<close>

value "map_of [(1::int,1::int),(1::int,2::int)] 1"


text \<open>We fix a problem, and also include the definitions for the domain
  of this problem.\<close>
locale ast_problem = ast_domain "domain P"
  for P :: ast_problem
begin
  text \<open>We refer to the problem domain as \<open>D\<close>\<close>
  abbreviation "D \<equiv> ast_problem.domain P"

  definition objT :: "object \<rightharpoonup> type" where
    "objT \<equiv> map_of (objects P) ++ constT"

  lemma objT_alt: "objT = map_of (consts D @ objects P)"
    unfolding objT_def constT_def
    apply (clarsimp)
    done

  definition "fun_inits \<equiv> map (\<lambda>FUNVAL f xs y \<Rightarrow> ((f,xs),y)) (filter is_funval (init P))"


  definition wf_fact :: "fact \<Rightarrow> bool" where
    "wf_fact = wf_pred_atom objT"

  fun wf_init_el where
    "wf_init_el (ATOM p xs) = wf_fact (p,xs)"
  | "wf_init_el (FUNVAL f xs y) = wf_funval objT f xs (Some y)"

    
  definition wf_basic_world_model :: "basic_world_model \<Rightarrow> bool" where
    "wf_basic_world_model M \<equiv> \<forall>f\<in>M. wf_pred_atom objT f"
    
  definition wf_fluent_valuation :: "fluent_valuation \<Rightarrow> bool" where
    "wf_fluent_valuation fv \<equiv> \<forall>f xs y. fv (f,xs) = Some y \<longrightarrow> wf_funval objT f xs (Some y)"
    
  definition wf_world_model :: "world_model \<Rightarrow> bool" where
    "wf_world_model \<equiv> \<lambda>(F,M). wf_fluent_valuation F \<and> wf_basic_world_model M"

  definition wf_problem where
    "wf_problem \<equiv>
      wf_domain
    \<and> distinct (map fst (objects P) @ map fst (consts D)) \<comment>\<open>Required by current semantics that assigns each object a unique type\<close>
    \<and> (\<forall>(n,T)\<in>set (objects P). wf_type T)
    \<and> (\<forall>ie\<in>set (init P). wf_init_el ie)
    \<and> (distinct (map fst fun_inits))
    \<and> wf_fmla (ty_term objT) (goal P)
    "

  abbreviation "wf_effect_inst \<equiv> wf_effect (ty_term objT)"


end \<comment> \<open>locale \<open>ast_problem\<close>\<close>

text \<open>Locale to express a well-formed domain\<close>
locale wf_ast_domain = ast_domain +
  assumes wf_domain: wf_domain

text \<open>Locale to express a well-formed problem\<close>
locale wf_ast_problem = ast_problem P for P +
  assumes wf_problem: wf_problem
begin
  sublocale wf_ast_domain "domain P"
    apply unfold_locales
    using wf_problem
    unfolding wf_problem_def by simp

end \<comment> \<open>locale \<open>wf_ast_problem\<close>\<close>

subsection \<open>PDDL Semantics\<close>


context ast_domain begin

  definition resolve_action_schema :: "name \<rightharpoonup> ast_action_schema" where
    "resolve_action_schema n = index_by ast_action_schema.name (actions D) n"

  fun subst_varobj where
    "subst_varobj psubst (varobj.VAR x) = psubst x"
  | "subst_varobj psubst (varobj.CONST c) = c"

  text \<open>To instantiate an action schema, we first compute a substitution from
    parameters to objects, and then apply this substitution to the
    precondition and effect. The substitution is applied via the \<open>map_xxx\<close>
    functions generated by the datatype package.
    \<close>
  fun instantiate_action_schema
    :: "ast_action_schema \<Rightarrow> object list \<Rightarrow> ast_action_inst"
  where
    "instantiate_action_schema (Action_Schema n params pre eff) args = (let
        psubst = (the o (map_of (zip (map fst params) args)));
        tsubst = map_term (subst_varobj psubst);
        pre_inst = (map_formula o map_atom) tsubst pre;
        eff_inst = map_ast_effect tsubst eff
      in
        Action_Inst pre_inst eff_inst
      )"

end \<comment> \<open>Context of \<open>ast_domain\<close>\<close>


context ast_problem begin

  text \<open>Initial model\<close>
  definition I :: "world_model" where
    "I \<equiv> (map_of fun_inits, {(p,xs). ATOM p xs \<in> set (init P)})"


  text \<open>Resolve a plan action and instantiate the referenced action schema.\<close>
  fun resolve_instantiate :: "plan_action \<Rightarrow> ast_action_inst" where
    "resolve_instantiate (PAction n args) =
      instantiate_action_schema
        (the (resolve_action_schema n))
        args" (* TODO: Avoid the underdefinedness, and move to option monad! *)

  text \<open>HOL encoding of matching an action's formal parameters against an
    argument list.
  \<close>
  definition "action_params_match a args \<equiv> do {
    vTs \<leftarrow> omap objT args;
    let Ts = map snd (parameters a);
    oassert (list_all2 of_type vTs Ts)
  } \<noteq> None"

  text \<open>At this point, we can define well-formedness of a plan action:
    The action must refer to a declared action schema and the arguments must
    be compatible with the formal parameters' types.
  \<close>
  fun wf_plan_action :: "plan_action \<Rightarrow> bool" where
    "wf_plan_action (PAction n args) = (
      case resolve_action_schema n of
        None \<Rightarrow> False
      | Some a \<Rightarrow>
          \<comment>\<open>Objects are valid and match parameter types\<close>
          action_params_match a args
          \<comment>\<open>Effect is valid\<close>
        \<and> wf_effect_inst (effect (instantiate_action_schema a args))
        )"
  text \<open>
    TODO: The second conjunct is redundant, as instantiating a well formed
      action with valid objects yields a valid effect.
  \<close>



  text \<open>A sequence of plan actions form a path, if they are well-formed and
    their instantiations form a path.\<close>
  definition execute_plan_action_path
    :: "plan \<Rightarrow> world_model \<rightharpoonup> world_model"
  where
    "execute_plan_action_path \<pi>s M = do {
      oassert (\<forall>\<pi> \<in> set \<pi>s. wf_plan_action \<pi>);
      let \<pi>s = map resolve_instantiate \<pi>s;
      execute_ast_action_inst_path \<pi>s M
    }"

  text \<open>Stepwise characterization\<close>  
  lemma execute_plan_action_path_Nil: "execute_plan_action_path [] M = Some M"
    unfolding execute_plan_action_path_def
    by (auto split: Option.bind_split)

  definition "execute_plan_action \<pi> M \<equiv> do {
      oassert (wf_plan_action \<pi>);
      let \<pi> = resolve_instantiate \<pi>;
      execute_ast_action_inst \<pi> M
    }"  

        
  lemma execute_plan_action_path_Cons: "execute_plan_action_path (\<pi>#\<pi>s) M = do {
      M \<leftarrow> execute_plan_action \<pi> M;
      execute_plan_action_path \<pi>s M
    }"  
    unfolding execute_plan_action_path_def execute_plan_action_def
    by (auto split: Option.bind_split)
      
  lemmas execute_plan_action_path_simps = execute_plan_action_path_Nil execute_plan_action_path_Cons
    
    
  text \<open>A plan is valid wrt.\ a given initial model, if it forms a path to a
    goal model \<close>
  definition valid_plan_from :: "plan \<Rightarrow> world_model \<Rightarrow> bool" where
    "valid_plan_from \<pi>s M \<equiv> do {
      M' \<leftarrow> execute_plan_action_path \<pi>s M;
      f_check_entails M' (goal P)
    } \<noteq> None"

  (* Implementation note: resolve and instantiate already done inside
      enabledness check, redundancy! *)

  text \<open>Finally, a plan is valid if it is valid wrt.\ the initial world
    model @{const I}\<close>
  definition valid_plan :: "plan \<Rightarrow> bool"
    where "valid_plan \<pi>s \<equiv> valid_plan_from \<pi>s I"

end \<comment> \<open>Context of \<open>ast_problem\<close>\<close>



subsection \<open>Preservation of Well-Formedness\<close>

subsubsection \<open>Well-Formed Action Instances\<close>
text \<open>The goal of this section is to establish that well-formedness of
  world models is preserved by execution of well-formed plan actions.
\<close>

definition "optord P a b \<equiv> case (a,b) of (None,_) \<Rightarrow> True | (Some a, Some b) \<Rightarrow> P b a | _ \<Rightarrow> False"

lemma optord_simps[simp]:
  "optord P None x"
  "optord P (Some a) (Some b) \<longleftrightarrow> P b a"
  "\<not>optord P (Some a) None"
  unfolding optord_def by auto

lemma omap_mono':
  assumes "\<And>x. x\<in>set xs \<Longrightarrow> optord P (f x) (f' x)"
  shows "optord (list_all2 P) (omap f xs) (omap f' xs)"
  using assms
  apply (induction xs)
  by (force split: Option.bind_splits option.splits simp: optord_def)+

lemma omap_mono'':
  assumes "\<And>x. optord P (f x) (f' x)"
  shows "optord (list_all2 P) (omap f xs) (omap f' xs)"
  using assms
  apply (induction xs)
  by (force split: Option.bind_splits option.splits simp: optord_def)+

locale optord_loc =
  fixes P
  assumes P_refl: "P x x"
  assumes P_trans: "P x y \<Longrightarrow> P y z \<Longrightarrow> P x z"
begin
  lemma optord_refl[simp]: "optord P x x"
    by (auto simp: optord_def P_refl split: option.splits)

  lemma optord_trans[trans]: "optord P x y \<Longrightarrow> optord P y z \<Longrightarrow> optord P x z"
    by (auto simp: optord_def split: option.splits dest: P_trans)

end

context ast_domain begin
  lemma of_type_refl[simp, intro!]: "of_type T T"
    unfolding of_type_def by auto

  lemma of_type_trans[trans]:
    "of_type T1 T2 \<Longrightarrow> of_type T2 T3 \<Longrightarrow> of_type T1 T3"
    unfolding of_type_def
    by clarsimp (metis (no_types, hide_lams)
      Image_mono contra_subsetD order_refl rtrancl_image_idem)

  sublocale optord_loc of_type
    apply unfold_locales
    apply simp
    by (rule of_type_trans)

end


context ast_problem begin

  text \<open>As plan actions are executed by first instantiating them, and then
    executing the action instance, it is natural to define a well-formedness
    concept for action instances.\<close>

  fun wf_action_inst :: "ast_action_inst \<Rightarrow> bool" where
    "wf_action_inst (Action_Inst pre eff) \<longleftrightarrow> (
        wf_fmla (ty_term objT) pre
      \<and> wf_effect (ty_term objT) eff
      )
    "

  text \<open>We first prove that instantiating a well-formed action schema will yield
    a well-formed action instance.

    We begin with some auxiliary lemmas before the actual theorem.
  \<close>


  (*
  lemma is_of_type_map_ofE:
    assumes "is_of_type (map_of params) x T"
    obtains i xT where "i<length params" "params!i = (x,xT)" "of_type xT T"
    using assms
    unfolding is_of_type_def
    by (auto split: option.splits dest!: map_of_SomeD simp: in_set_conv_nth)
  *)


  lemma omap_leD:
    assumes SS: "f \<subseteq>\<^sub>m f'"
    assumes "omap f xs = Some v"
    shows "omap f' xs = Some v"
    using assms(2)
    apply (induction xs arbitrary: v)
    apply (auto split: Option.bind_splits dest: map_leD[OF SS])
    done

  lemma omap_map: "omap f (map g xs) = omap (f o g) xs"
    by (induction xs) auto


  lemma wf_atom_mono:
    assumes SS: "tys \<subseteq>\<^sub>m tys'"
    assumes WF: "wf_atom tys a"
    shows "wf_atom tys' a"
  proof -
    from WF show ?thesis
      apply (cases a)
      apply (auto split: option.splits Option.bind_splits dest: omap_leD[OF SS] map_leD[OF SS])
      done
  qed

  lemma wf_fmla_atom_mono:
    assumes SS: "tys \<subseteq>\<^sub>m tys'"
    assumes WF: "wf_fmla_atom tys a"
    shows "wf_fmla_atom tys' a"
  proof -
    from WF show ?thesis
      apply (cases a rule: wf_fmla_atom.cases)
      apply (auto split: option.splits Option.bind_splits dest: omap_leD[OF SS] map_leD[OF SS])
      done
  qed


  lemma constT_ss_objT: "constT \<subseteq>\<^sub>m objT"
    unfolding constT_def objT_def
    apply rule
    by (auto simp: map_add_def split: option.split)

  lemma tyt_constT_ss_objT:
    "ty_term (ty_varobj Q constT) \<subseteq>\<^sub>m ty_term (ty_varobj Q objT)"
    apply (rule ty_term_mono)
    apply (rule ty_varobj_mono[OF map_le_refl constT_ss_objT])
    done

  lemma wf_atom_constT_imp_objT: "wf_atom (ty_term (ty_varobj Q constT)) a \<Longrightarrow> wf_atom (ty_term (ty_varobj Q objT)) a"
    by (erule wf_atom_mono[OF tyt_constT_ss_objT])

  lemma wf_fmla_atom_constT_imp_objT: "wf_fmla_atom (ty_term (ty_varobj Q constT)) a \<Longrightarrow> wf_fmla_atom (ty_term (ty_varobj Q objT)) a"
    by (erule wf_fmla_atom_mono[OF tyt_constT_ss_objT])

  lemma wf_fmla_constT_imp_objT: "wf_fmla (ty_term (ty_varobj Q constT)) \<phi> \<Longrightarrow> wf_fmla (ty_term (ty_varobj Q objT)) \<phi>"
    by (induction \<phi>) (auto simp: wf_atom_constT_imp_objT)
    
  context
    fixes Q and f :: "variable \<Rightarrow> object"
    assumes INST: "optord of_type (Q v) (objT (f v))"
  begin


    (*
    lemma is_of_type_var_conv: "is_of_type (ty_term Q objT) (term.VAR x) T  \<longleftrightarrow> is_of_type Q x T"
      unfolding is_of_type_def by (auto)

    lemma is_of_type_const_conv: "is_of_type (ty_term Q objT) (term.CONST x) T  \<longleftrightarrow> is_of_type objT x T"
      unfolding is_of_type_def
      by (auto split: option.split)

    lemma INST': "is_of_type (ty_term Q objT) x T \<Longrightarrow> is_of_type objT (subst_term f x) T"
      apply (cases x) using INST apply (auto simp: is_of_type_var_conv is_of_type_const_conv)
      done
    *)


    lemma inst_varobj: "optord of_type (ty_varobj Q objT x) (objT (subst_varobj f x))"
      by (cases x) (auto simp: INST)

    lemma inst_ty_term: "optord of_type (ty_term (ty_varobj Q objT) t) (ty_term objT (map_term (subst_varobj f) t))"
    proof (induction t rule: ty_term.induct)
      case (1 x)
      then show ?case by (auto simp: inst_varobj)
    next
      case (2 fx xs)
      then show ?case
        using omap_mono'[of xs of_type "ty_term (ty_varobj Q objT)" "(ty_term objT \<circ>\<circ> map_term) (subst_varobj f)"]
        apply (auto split: option.splits Option.bind_splits simp: omap_map)
        by (smt ast_domain.of_type_trans list_all2_trans)

    qed


    (*
    lemma wf_inst_eq_aux: "Q x = Some T \<Longrightarrow> objT (f x) \<noteq> None"
      using INST[of x T]
      by (auto split: option.splits)
    *)

    (*
    lemma wf_inst_eq_aux': "ty_term (ty_varobj Q objT) x = Some T \<Longrightarrow> objT (map_term (subst_varobj f) x) \<noteq> None"
      by (cases x) (auto simp: wf_inst_eq_aux)
    *)

    (*
    lemma wf_inst_atom:
      assumes "wf_atom (ty_term Q constT) a"
      shows "wf_atom objT (map_atom (subst_term f) a)"
    proof -
      have X1: "list_all2 (is_of_type objT) (map (subst_term f) xs) Ts" if
        "list_all2 (is_of_type (ty_term Q objT)) xs Ts" for xs Ts
        using that
        apply induction
        using INST'
        by auto
      then show ?thesis
        using assms[THEN wf_atom_constT_imp_objT] wf_inst_eq_aux'
        by (cases a; auto split: option.splits)

    qed

    lemma wf_inst_formula_atom:
      assumes "wf_fmla_atom (ty_term Q constT) a"
      shows "wf_fmla_atom objT ((map_formula o map_atom o subst_term) f a)"
      using assms[THEN wf_fmla_atom_constT_imp_objT] wf_inst_atom
      apply (cases a rule: wf_fmla_atom.cases; auto split: option.splits)
      by (simp add: INST' list.rel_map(1) list_all2_mono)
    *)


    lemma wf_inst_effect:
      assumes "wf_effect (ty_term (ty_varobj Q objT)) \<phi>"
      shows "wf_effect (ty_term objT) ((map_ast_effect (map_term (subst_varobj f))) \<phi>)"
      using assms
      proof (induction \<phi>)
        case (Effect x1a x2a)
        then show ?case
          thm inst_ty_term
          using omap_mono''[of _ "ty_term (ty_varobj Q objT)" "\<lambda>t. ty_term objT (map_term (subst_varobj f) t)", OF inst_ty_term]
          apply (auto split: Option.bind_splits simp: omap_map comp_def)
          apply (auto split: Option.bind_splits)
          apply (drule (1) bspec)
          apply (auto split: Option.bind_splits)
          apply (metis optord_simps(3))
          apply (drule (1) bspec)
          apply (auto split: Option.bind_splits)
          apply (smt P_trans list_all2_trans optord_simps(2))
          apply (drule (1) bspec)
          apply (auto split: Option.bind_splits)
          apply (metis optord_simps(3))
          apply (drule (1) bspec)
          apply (auto split: Option.bind_splits)
          apply (smt P_trans list_all2_trans optord_simps(2))
          apply (drule (1) bspec)
          apply (case_tac x)
          apply (auto split: Option.bind_splits option.splits simp: omap_map comp_def wf_funval_def)
          apply (metis optord_simps(3))
          apply (smt P_trans list_all2_trans optord_simps(2))
          apply (metis optord_simps(3))
          apply (smt P_trans list_all2_trans optord_simps(2))
          apply (metis inst_ty_term optord_simps(3))
          apply (metis optord_simps(3))
          apply (smt P_trans list_all2_trans optord_simps(2))
          by (metis ast_domain.of_type_trans inst_ty_term optord_simps(2))
      qed


    lemma wf_inst_atom:
      "wf_atom (ty_term (ty_varobj Q objT)) a \<Longrightarrow> wf_atom (ty_term objT) (map_atom (map_term (subst_varobj f)) a)"
      apply (cases a)
      using omap_mono''[of _ "ty_term (ty_varobj Q objT)" "\<lambda>t. ty_term objT (map_term (subst_varobj f) t)", OF inst_ty_term]
      apply (auto split: Option.bind_splits simp: omap_map comp_def)
      apply (metis optord_simps(3))
      apply (smt P_trans list_all2_trans optord_simps(2))
      apply (metis inst_ty_term not_None_eq optord_simps(3))
      apply (metis inst_ty_term not_None_eq optord_simps(3))
      done
    (*TODO: Clean up the two proofs above!
      option_ordD, + transitivity*)


    lemma wf_inst_formula:
      assumes "wf_fmla (ty_term (ty_varobj Q objT)) \<phi>"
      shows "wf_fmla (ty_term objT) (map_formula (map_atom (map_term (subst_varobj f))) \<phi>)"
      using assms
      apply (induction \<phi>)
      apply (auto simp: wf_inst_atom)
      done



  end

  context 
    fixes T T' :: "'a \<rightharpoonup> type"
    assumes SM: "T\<subseteq>\<^sub>mT'"
  begin
  
    lemma wf_pred_atom_mono: "wf_pred_atom T a \<Longrightarrow> wf_pred_atom T' a"
      apply (cases a)
      by (auto split: Option.bind_splits simp: omap_leD[OF SM])
  
    lemma wf_assignment_mono: "wf_assignment T a \<Longrightarrow> wf_assignment T' a"  
      apply (cases a)
      by (auto split: Option.bind_splits option.splits simp: omap_leD[OF SM] map_leD[OF SM] wf_funval_def)
  
    lemma wf_effect_mono: "wf_effect T e \<Longrightarrow> wf_effect T' e"
      apply (cases e)
      by (simp add: wf_pred_atom_mono wf_assignment_mono)

    thm ty_term_mono ty_varobj_mono     
    
    find_theorems ty_varobj name: mono
    
                 
  end
  
  
  text \<open>Instantiating a well-formed action schema with compatible arguments
    will yield a well-formed action instance.
  \<close>
  theorem wf_instantiate_action_schema:
    assumes "action_params_match a args"
    assumes "wf_action_schema a"
    shows "wf_action_inst (instantiate_action_schema a args)"
  proof (cases a)
    case [simp]: (Action_Schema name params pre eff)

    from assms(2) have DP: "distinct (map fst params)" by (auto simp: Let_def)
    
    have INST: "optord of_type (map_of params x) (objT (the (map_of (zip (map fst params) args) x)))"
      for x
      using assms(1) DP
      apply (auto 
        simp: optord_def action_params_match_def in_set_conv_decomp
        split: Option.bind_splits option.split)
      apply (auto dest!: map_of_Some_split simp: list_all2_append2 list_all2_Cons2)
      done
    
    have 1: "ty_term (ty_varobj (map_of params) constT) \<subseteq>\<^sub>m ty_term (ty_varobj (map_of params) objT)"  
      by (simp add: ast_domain.ty_term_mono ty_varobj_mono constT_ss_objT)
    
    show ?thesis
      using assms(2)
      apply (auto simp: Let_def comp_def)
      subgoal
        apply (rule wf_inst_formula[rotated])
        apply (erule wf_fmla_constT_imp_objT)
        by (rule INST)
      subgoal
        apply (rule wf_inst_effect)
        apply (rule INST)
        apply (rule wf_effect_mono)
        apply (rule 1)
        .
      done
      
  qed
        
end \<comment> \<open>Context of \<open>ast_problem\<close>\<close>



subsubsection \<open>Preservation\<close>

(*
context ast_problem begin

  text \<open>We start by defining two shorthands for enabledness and execution of
    a plan action.\<close>

  text \<open>Shorthand for enabled plan action: It is well-formed, and the
    precondition holds for its instance.\<close>
  definition plan_action_enabled :: "plan_action \<Rightarrow> world_model \<Rightarrow> bool" where
    "plan_action_enabled \<pi> M
      \<longleftrightarrow> wf_plan_action \<pi> \<and> M \<TTurnstile>\<^sub>f precondition (resolve_instantiate \<pi>)"

  text \<open>Shorthand for executing a plan action: Resolve, instantiate, and
    apply effect\<close>
  definition execute_plan_action :: "plan_action \<Rightarrow> world_model \<Rightarrow> world_model"
    where "execute_plan_action \<pi> M
      = (apply_basic_effect (the (feval_ast_effect (fst M) (effect (resolve_instantiate \<pi>)))) M)"

  xxx: effect evaluation can go wrong, even for well-formed actions, b/c undefined fluents
      
  text \<open>The @{const plan_action_path} predicate can be decomposed naturally
    using these shorthands: \<close>
  lemma execute_plan_action_path_Nil[simp]: "execute_plan_action_path [] M = Some M' \<longleftrightarrow> M'=M"
    by (auto simp: execute_plan_action_path_def split: Option.bind_splits)

  thm execute_ast_action_inst.simps  
  lemma execute_ast_action_inst_conv: "execute_ast_action_inst a M = Some M' \<longleftrightarrow> (\<exists>eff. 
      feval_ast_effect (fst M) (effect a) = Some eff
    \<and> M \<TTurnstile>\<^sub>f precondition a
    \<and> M' = apply_basic_effect eff M
      )"  
    apply (cases a; cases M)
    by (auto split: Option.bind_split)

  lemma execute_ast_action_inst_conv_None: "execute_ast_action_inst a M = None 
    \<longleftrightarrow> (feval_ast_effect (fst M) (effect a) = None) \<or> \<not>(M \<TTurnstile>\<^sub>f precondition a)"  
    apply (cases a; cases M)
    by (auto split: Option.bind_split)
    
        
  lemma eq_None_conv: "v=None \<longleftrightarrow> (\<nexists>x. v = Some x)" by simp 
    
  lemma execute_plan_action_path_Cons[simp]:
    "execute_plan_action_path (\<pi>#\<pi>s) M = Some M' \<longleftrightarrow>
      plan_action_enabled \<pi> M
    \<and> execute_plan_action_path \<pi>s (execute_plan_action \<pi> M) = Some M'"
    subgoal
      supply [simp del] = wf_plan_action.simps
      apply (cases \<pi>)
      apply (auto simp: execute_plan_action_path_def execute_plan_action_def split: option.splits Option.bind_splits)
      apply (auto simp: plan_action_enabled_def execute_ast_action_inst_conv execute_ast_action_inst_conv_None)
      
      done
      
    find_theorems "Option.bind _ _ = Some _"
    
    oops
    apply (auto
      simp: execute_plan_action_path_def execute_plan_action_def
            (*execute_ast_action_inst_def*) plan_action_enabled_def
      split: Option.bind_splits option.splits           
            )
    apply (case_tac x2a; auto simp: Let_def)


end \<comment> \<open>Context of \<open>ast_problem\<close>\<close>

*)

fun eval_assignment where
  "eval_assignment f (ASSIGN n args v) = do {
    args \<leftarrow> omap f args;
    v \<leftarrow> omap_option f v;
    Some (ASSIGN n args v)
  }"

lemma omap_effect_alt:
  shows "omap_dt set_ast_effect map_ast_effect f e = (case e of (Effect ad de as) \<Rightarrow> do {
    ad \<leftarrow> omap (omap_prod Some (omap f)) ad;
    de \<leftarrow> omap (omap_prod Some (omap f)) de;
    as \<leftarrow> omap (eval_assignment f) as;
    Some (Effect ad de as)
  })"  
  apply (cases e)
  apply (auto simp: omap_dt_def in_set_conv_decomp split: Option.bind_splits)
  
  apply (case_tac xa; auto simp: in_set_conv_decomp split: Option.bind_splits)
  apply (auto simp: omap_alt omap_alt_None in_set_conv_decomp split: Option.bind_splits) [3]
  apply (case_tac x; auto simp: omap_alt omap_alt_None in_set_conv_decomp omap_option_conv split: Option.bind_splits)
  apply (auto simp: omap_alt omap_alt_None in_set_conv_decomp split: Option.bind_splits) [3]
  apply (case_tac x; auto simp: omap_alt omap_alt_None in_set_conv_decomp omap_option_eq split: Option.bind_splits option.splits)
  done



context wf_ast_problem begin

  lemma wf_fluents_I: "wf_fluent_valuation (map_of fun_inits)"
    using wf_problem
    unfolding wf_problem_def
    by (auto simp: wf_fluent_valuation_def fun_inits_def dest!: map_of_SomeD split: init_el.splits)
    
  lemma wf_basic_I: "wf_basic_world_model {(p, xs). ATOM p xs \<in> set (init P)}"
    using wf_problem
    unfolding wf_problem_def wf_basic_world_model_def
    by (auto simp: wf_fact_def)
    
  text \<open>The initial world model is well-formed\<close>
  lemma wf_I: "wf_world_model I"
    unfolding wf_world_model_def I_def
    using wf_basic_I wf_fluents_I 
    by auto

  text \<open>Application of a well-formed effect preserves well-formedness
    of the model\<close>
  thm execute_plan_action_path_Cons  
  term execute_plan_action_path  
    
  
  term wf_action_inst
  
  (* TODO: Move to domain *)
  lemma wf_resolve_action_schema: "resolve_action_schema n = Some a \<Longrightarrow> wf_action_schema a"
    using wf_domain unfolding wf_domain_def
    by (auto simp: resolve_action_schema_def)
  
  lemma wf_apply_basic_assignment:
    assumes "wf_assignment objT a"  
    assumes "wf_fluent_valuation fv"
    shows "wf_fluent_valuation (apply_basic_assignment a fv)"
    using assms
    by (cases a) (auto simp: wf_fluent_valuation_def)
    
  lemma wf_apply_basic_assignments:
    "\<lbrakk>\<forall>a\<in>set as. wf_assignment objT a; wf_fluent_valuation fv\<rbrakk> \<Longrightarrow> wf_fluent_valuation (fold apply_basic_assignment as fv)"
    apply (induction as arbitrary: fv)
    apply (auto simp: wf_apply_basic_assignment)
    done
    
    
    
  lemma wf_apply_basic_effect:
    assumes "wf_effect objT e"
    assumes "wf_world_model s"
    shows "wf_world_model (apply_basic_effect e s)"
    using assms wf_problem
    unfolding wf_world_model_def wf_basic_world_model_def wf_problem_def wf_domain_def
    apply (cases e) 
    apply (auto split: formula.splits prod.splits simp: wf_apply_basic_assignments)
    done
    
    
  lemma wf_feval_term:
    assumes "wf_fluent_valuation fv"
    assumes "feval_term fv t = Some x"  
    assumes "ty_term objT t = Some T"
    shows "\<exists>T'. objT x = Some T' \<and> of_type T' T"
    using assms
    apply (cases t; auto split: Option.bind_splits)
    unfolding wf_fluent_valuation_def wf_funval_def
    apply (fastforce split: Option.bind_splits)
    done
    
  lemma wf_feval_terms:  
    assumes "wf_fluent_valuation fv"
    assumes "omap (feval_term fv) ts = Some xs"  
    assumes "omap (ty_term objT) ts = Some Ts"
    shows "\<exists>Ts'. omap objT xs = Some Ts' \<and> list_all2 of_type Ts' Ts"
    using assms
    apply (induction ts arbitrary: xs Ts)
    apply simp
    apply (fastforce split: Option.bind_splits dest: wf_feval_term)
    done
  
    
    
  lemma of_type_list_trans: "\<lbrakk>list_all2 of_type as bs; list_all2 of_type bs cs\<rbrakk> \<Longrightarrow> list_all2 of_type as cs"  
    using list_all2_trans[of of_type of_type of_type] of_type_trans
    by blast
    
  lemma wf_feval_pred_atom:
    "\<lbrakk>wf_fluent_valuation fv; omap (feval_term fv) ba = Some b; wf_pred_atom (ty_term objT) (a, ba)\<rbrakk>
       \<Longrightarrow> wf_pred_atom objT (a, b)"  
    apply (auto split: Option.bind_splits dest: wf_feval_terms)   
    apply (drule (2) wf_feval_terms) 
    apply (auto dest: of_type_list_trans)
    done
    
    
  lemma wf_feval_assignment:  
    assumes "wf_fluent_valuation fv"
    assumes "eval_assignment (feval_term fv) xb = Some xc"
    assumes "wf_assignment (ty_term objT) xb"
    shows "wf_assignment objT xc"
    supply [iff del] = not_None_eq
    using assms(2,3)
    supply DRS = wf_feval_terms[OF assms(1)]
    supply DR = wf_feval_term[OF assms(1)]
    apply (cases xb; auto split: Option.bind_splits option.splits simp: wf_funval_def)
    apply (fastforce dest: DRS of_type_list_trans)
    apply (fastforce dest: DRS of_type_list_trans)
    apply (fastforce dest: DRS of_type_list_trans)
    apply (fastforce dest: DRS of_type_list_trans)
    apply (fastforce dest: DR)
    apply (fastforce dest: DRS of_type_list_trans)
    apply (fastforce dest: DRS of_type_list_trans)
    using DR of_type_trans by fastforce
    
    
  lemma wf_feval_effect:
    assumes "wf_fluent_valuation fv"
    assumes "wf_effect_inst ei"
    assumes "feval_ast_effect fv ei = Some e"  
    shows "wf_effect objT e"
    using assms
    supply [simp del] = wf_pred_atom.simps
    apply (cases ei)
    by (auto simp: feval_ast_effect_def omap_effect_alt in_set_conv_decomp wf_feval_pred_atom wf_feval_assignment split: Option.bind_splits)
    
    
  lemma wf_execute_ast_action_inst:
    assumes "wf_world_model s"  
    assumes "wf_action_inst a"
    assumes "execute_ast_action_inst a s = Some s'"
    shows "wf_world_model s'"
    using assms
    apply (cases a; cases s)
    apply (auto split: Option.bind_splits simp: )
    apply (rule wf_apply_basic_effect)
    apply (auto simp: wf_world_model_def wf_feval_effect)
    done
    
  
  lemma wf_execute_plan_action:
    assumes "wf_world_model s"
    assumes "execute_plan_action \<pi> s = Some s'"
    shows "wf_world_model s'"
    using assms
    supply wf_action_inst.simps[simp del]
    apply (cases \<pi>) unfolding execute_plan_action_def
    apply (auto split: Option.bind_splits option.splits)
    apply (drule wf_instantiate_action_schema)
    apply (erule wf_resolve_action_schema)
    apply (simp add: wf_execute_ast_action_inst)
    done
  

  text \<open>Execution of a plan preserves well-formedness\<close>
  corollary wf_plan_action_path:
    "\<lbrakk>wf_world_model s; execute_plan_action_path \<pi>s s = Some s'\<rbrakk> \<Longrightarrow> wf_world_model s'"
    by (induction \<pi>s arbitrary: s) 
      (auto intro: wf_execute_plan_action simp: execute_plan_action_path_simps split: Option.bind_splits)


end \<comment> \<open>Context of \<open>wf_ast_problem\<close>\<close>

end \<comment> \<open>Theory\<close>
