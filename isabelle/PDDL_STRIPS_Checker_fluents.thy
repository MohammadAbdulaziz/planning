section \<open>Executable PDDL Checker\<close>
theory PDDL_STRIPS_Checker_fluents
imports
  PDDL_STRIPS_Semantics_fluents

  Error_Monad_Add
  (*"HOL.String"*)

  (*"HOL-Library.Code_Char"     TODO: This might lead to performance loss! CHECK! *)
  "HOL-Library.Code_Target_Nat"

  "HOL-Library.While_Combinator"

  "Containers.Containers"
begin

subsection \<open>Generic DFS Reachability Checker\<close>
text \<open>Used for subtype checks\<close>

definition "E_of_succ succ \<equiv> { (u,v). v\<in>set (succ u) }"
lemma succ_as_E: "set (succ x) = E_of_succ succ `` {x}"
  unfolding E_of_succ_def by auto

context
  fixes succ :: "'a \<Rightarrow> 'a list"
begin

  private abbreviation (input) "E \<equiv> E_of_succ succ"


definition "dfs_reachable D w \<equiv>
  let (V,w,brk) = while (\<lambda>(V,w,brk). \<not>brk \<and> w\<noteq>[]) (\<lambda>(V,w,_).
    case w of v#w \<Rightarrow>
    if D v then (V,v#w,True)
    else if v\<in>V then (V,w,False)
    else
      let V = insert v V in
      let w = succ v @ w in
      (V,w,False)
    ) ({},w,False)
  in brk"


context
  fixes w\<^sub>0 :: "'a list"
  assumes finite_dfs_reachable[simp, intro!]: "finite (E\<^sup>* `` set w\<^sub>0)"
begin

  private abbreviation (input) "W\<^sub>0 \<equiv> set w\<^sub>0"

definition "dfs_reachable_invar D V W brk \<longleftrightarrow>
    W\<^sub>0 \<subseteq> W \<union> V
  \<and> W \<union> V \<subseteq> E\<^sup>* `` W\<^sub>0
  \<and> E``V \<subseteq> W \<union> V
  \<and> Collect D \<inter> V = {}
  \<and> (brk \<longrightarrow> Collect D \<inter> E\<^sup>* `` W\<^sub>0 \<noteq> {})"

lemma card_decreases: "
   \<lbrakk>finite V; y \<notin> V; dfs_reachable_invar D V (Set.insert y W) brk \<rbrakk>
   \<Longrightarrow> card (E\<^sup>* `` W\<^sub>0 - Set.insert y V) < card (E\<^sup>* `` W\<^sub>0 - V)"
  apply (rule psubset_card_mono)
  apply (auto simp: dfs_reachable_invar_def)
  done

lemma all_neq_Cons_is_Nil[simp]: (* Odd term remaining in goal \<dots> *)
  "(\<forall>y ys. x2 \<noteq> y # ys) \<longleftrightarrow> x2 = []" by (cases x2) auto

lemma dfs_reachable_correct: "dfs_reachable D w\<^sub>0 \<longleftrightarrow> Collect D \<inter> E\<^sup>* `` set w\<^sub>0 \<noteq> {}"
  unfolding dfs_reachable_def
  apply (rule while_rule[where
    P="\<lambda>(V,w,brk). dfs_reachable_invar D V (set w) brk \<and> finite V"
    and r="measure (\<lambda>V. card (E\<^sup>* `` (set w\<^sub>0) - V)) <*lex*> measure length <*lex*> measure (\<lambda>True\<Rightarrow>0 | False\<Rightarrow>1)"
    ])
  subgoal by (auto simp: dfs_reachable_invar_def)
  subgoal
    apply (auto simp: neq_Nil_conv succ_as_E[of succ] split: if_splits)
    by (auto simp: dfs_reachable_invar_def Image_iff intro: rtrancl.rtrancl_into_rtrancl)
  subgoal by (fastforce simp: dfs_reachable_invar_def dest: Image_closed_trancl)
  subgoal by blast
  subgoal by (auto simp: neq_Nil_conv card_decreases)
  done

end

definition "tab_succ l \<equiv> Mapping.lookup_default [] (fold (\<lambda>(u,v). Mapping.map_default u [] (Cons v)) l Mapping.empty)"


lemma Some_eq_map_option [iff]: "(Some y = map_option f xo) = (\<exists>z. xo = Some z \<and> f z = y)"
  by (auto simp add: map_option_case split: option.split)


lemma tab_succ_correct: "E_of_succ (tab_succ l) = set l"
proof -
  have "set (Mapping.lookup_default [] (fold (\<lambda>(u,v). Mapping.map_default u [] (Cons v)) l m) u) = set l `` {u} \<union> set (Mapping.lookup_default [] m u)"
    for m u
    apply (induction l arbitrary: m)
    by (auto
      simp: Mapping.lookup_default_def Mapping.map_default_def Mapping.default_def
      simp: lookup_map_entry' lookup_update' keys_is_none_rep Option.is_none_def
      split: if_splits
    )
  from this[where m=Mapping.empty] show ?thesis
    by (auto simp: E_of_succ_def tab_succ_def lookup_default_empty)
qed

end

lemma finite_imp_finite_dfs_reachable:
  "\<lbrakk>finite E; finite S\<rbrakk> \<Longrightarrow> finite (E\<^sup>*``S)"
  apply (rule finite_subset[where B="S \<union> (Relation.Domain E \<union> Relation.Range E)"])
  apply (auto simp: intro: finite_Domain finite_Range elim: rtranclE)
  done

lemma dfs_reachable_tab_succ_correct: "dfs_reachable (tab_succ l) D vs\<^sub>0 \<longleftrightarrow> Collect D \<inter> (set l)\<^sup>*``set vs\<^sub>0 \<noteq> {}"
  apply (subst dfs_reachable_correct)
  by (simp_all add: tab_succ_correct finite_imp_finite_dfs_reachable)



subsection \<open>Implementation Refinements\<close>

subsubsection \<open>Of-Type\<close>

definition "of_type_impl G oT T \<equiv> (\<forall>pt\<in>set (primitives oT). dfs_reachable G ((=) pt) (primitives T))"

term ty_varobj

fun ty_varobj' where
  "ty_varobj' varT objT (varobj.VAR v) = varT v"
| "ty_varobj' varT objT (varobj.CONST c) = Mapping.lookup objT c"

lemma ty_varobj'_correct_aux: "ty_varobj' varT objT t = ty_varobj varT (Mapping.lookup objT) t"
  by (cases t) auto

lemma ty_varobj'_correct[simp]: "ty_varobj' varT objT = ty_varobj varT (Mapping.lookup objT)"
  using ty_varobj'_correct_aux by auto

context ast_domain begin

  definition "of_type1 pt T \<longleftrightarrow> pt \<in> subtype_rel\<^sup>* `` set (primitives T)"

  lemma of_type_refine1: "of_type oT T \<longleftrightarrow> (\<forall>pt\<in>set (primitives oT). of_type1 pt T)"
    unfolding of_type_def of_type1_def by auto

  definition "STG \<equiv> (tab_succ (map subtype_edge (types D)))"

  lemma subtype_rel_impl: "subtype_rel = E_of_succ (tab_succ (map subtype_edge (types D)))"
    by (simp add: tab_succ_correct subtype_rel_def)

  lemma of_type1_impl: "of_type1 pt T \<longleftrightarrow> dfs_reachable (tab_succ (map subtype_edge (types D))) ((=)pt) (primitives T)"
    by (simp add: subtype_rel_impl of_type1_def dfs_reachable_tab_succ_correct tab_succ_correct)

  lemma of_type_impl_correct: "of_type_impl STG oT T \<longleftrightarrow> of_type oT T"
    unfolding of_type1_impl STG_def of_type_impl_def of_type_refine1 ..

  lemmas [simp] = of_type_impl_correct[abs_def]  
    
  definition mp_constT :: "(object, type) mapping" where
    "mp_constT = Mapping.of_alist (consts D)"

  lemma mp_constT_correct[simp]: "Mapping.lookup mp_constT = constT"
    unfolding mp_constT_def constT_def
    by transfer (simp add: Map_To_Mapping.map_apply_def)






  text \<open>Lifting the subtype-graph through wf-checker\<close>
  context
    fixes ty_ent :: "'ent \<rightharpoonup> type"  \<comment> \<open>Entity's type, None if invalid\<close>
  begin

    (*
    definition "is_of_type' stg v T \<longleftrightarrow> (
      case ty_ent v of
        Some vT \<Rightarrow> of_type_impl stg vT T
      | None \<Rightarrow> False)"

    lemma is_of_type'_correct: "is_of_type' STG v T = is_of_type ty_ent v T"
      unfolding is_of_type'_def is_of_type_def of_type_impl_correct ..
    *)

    fun wf_pred_atom' where
      "wf_pred_atom' stg (p,vs) \<longleftrightarrow> do {
        Ts \<leftarrow> sig p;
        vTs \<leftarrow> omap ty_ent vs;
        oassert (list_all2 (of_type_impl stg) vTs Ts)
      } \<noteq> None"

    lemma wf_pred_atom'_correct[simp]: "wf_pred_atom' STG pvs = wf_pred_atom ty_ent pvs"
      apply (cases pvs) 
      apply (auto simp: split:Option.bind_split)
      done

    fun wf_atom' :: "_ \<Rightarrow> 'ent atom \<Rightarrow> bool" where
      "wf_atom' stg (atom.predAtm p vs) \<longleftrightarrow> wf_pred_atom' stg (p,vs)"
    | "wf_atom' stg (atom.Eq a b) = (ty_ent a \<noteq> None \<and> ty_ent b \<noteq> None)"

    lemma wf_atom'_correct: "wf_atom' STG a = wf_atom ty_ent a"
      apply (cases a)
      by (auto simp del: wf_pred_atom.simps wf_pred_atom'.simps)
      
    fun wf_fmla' :: "_ \<Rightarrow> ('ent atom) formula \<Rightarrow> bool" where
      "wf_fmla' stg (Atom a) \<longleftrightarrow> wf_atom' stg a"
    | "wf_fmla' stg \<bottom> \<longleftrightarrow> True"
    | "wf_fmla' stg (\<phi>1 \<^bold>\<and> \<phi>2) \<longleftrightarrow> (wf_fmla' stg \<phi>1 \<and> wf_fmla' stg \<phi>2)"
    | "wf_fmla' stg (\<phi>1 \<^bold>\<or> \<phi>2) \<longleftrightarrow> (wf_fmla' stg \<phi>1 \<and> wf_fmla' stg \<phi>2)"
    | "wf_fmla' stg (\<phi>1 \<^bold>\<rightarrow> \<phi>2) \<longleftrightarrow> (wf_fmla' stg \<phi>1 \<and> wf_fmla' stg \<phi>2)"
    | "wf_fmla' stg (\<^bold>\<not>\<phi>) \<longleftrightarrow> wf_fmla' stg \<phi>"

    lemma wf_fmla'_correct[abs_def,simp]: "wf_fmla' STG \<phi> \<longleftrightarrow> wf_fmla ty_ent \<phi>"
      by (induction \<phi> rule: wf_fmla.induct) (auto simp: wf_atom'_correct)

    fun wf_fmla_atom1' where
      "wf_fmla_atom1' stg (Atom (predAtm p vs)) \<longleftrightarrow> wf_pred_atom' stg (p,vs)"
    | "wf_fmla_atom1' stg _ \<longleftrightarrow> False"

    lemma wf_fmla_atom1'_correct[abs_def, simp]: "wf_fmla_atom1' STG \<phi> = wf_fmla_atom ty_ent \<phi>"
      by (cases \<phi> rule: wf_fmla_atom.cases) (auto
        simp: wf_atom'_correct split: option.splits)

    definition "wf_funval' stg f xs y \<equiv> do {
        (Ts,T) \<leftarrow> fsig f;
        vTs \<leftarrow> omap ty_ent xs;
        oassert (list_all2 (of_type_impl stg) vTs Ts);
        case y of
          None \<Rightarrow> Some ()
        | Some y \<Rightarrow> do {
            yT \<leftarrow> ty_ent y;
            oassert (of_type_impl stg yT T)
          }
      } \<noteq> None"  
      
    lemma wf_funval'_correct[abs_def, simp]: "wf_funval' STG f xs y = wf_funval ty_ent f xs y"  
      unfolding wf_funval_def wf_funval'_def of_type_impl_correct 
      by simp
      
    fun wf_assignment' where
      "wf_assignment' stg (ASSIGN f xs y) \<longleftrightarrow> wf_funval' stg f xs y"

    lemma wf_assignment'_correct[abs_def, simp]: "wf_assignment' STG a = wf_assignment ty_ent a"
      by (cases a; simp)
        
        
    fun wf_effect' where
      "wf_effect' stg (Effect a d ass) \<longleftrightarrow>
          (\<forall>ae\<in>set a. wf_pred_atom' stg ae)
        \<and> (\<forall>de\<in>set d.  wf_pred_atom' stg de)
        \<and> (\<forall>asse\<in>set ass. wf_assignment' stg asse)
        "

    lemma wf_effect'_correct[abs_def, simp]: "wf_effect' STG e = wf_effect ty_ent e"
      by (cases e) (auto simp:  )  

      
    fun ty_term' where
      "ty_term' stg (term.ENT x) = ty_ent x"
    | "ty_term' stg (term.FUN f xs) = do {
        (Ts,T) \<leftarrow> fsig f;
        vTs \<leftarrow> omap (ty_term' stg) xs;
        oassert (list_all2 (of_type_impl stg) vTs Ts);
        Some T
      }"

    lemma ty_term'_correct[abs_def, simp]: "ty_term' STG t = ty_term ty_ent t"         
    proof (induction t)
      case (ENT x)
      then show ?case by auto
    next
      case (FUN f args)
      hence "omap (ty_term' STG) args = omap (ty_term ty_ent) args"
        using omap_cong by blast
      then show ?case by (auto)
    qed
      
      
  end \<comment> \<open>Context fixing \<open>ty_ent\<close>\<close>

  fun wf_action_schema' :: "_ \<Rightarrow> _ \<Rightarrow> ast_action_schema \<Rightarrow> bool" where
    "wf_action_schema' stg conT (Action_Schema n params pre eff) \<longleftrightarrow> (
      let
        tvo = ty_varobj' (map_of params) conT;
        tyv = ty_term' tvo stg 
      in
        distinct (map fst params)
      \<and> wf_fmla' tyv stg pre
      \<and> wf_effect' tyv stg eff)"

  lemma wf_action_schema'_correct[abs_def, simp]: "wf_action_schema' STG mp_constT s = wf_action_schema s"
    apply (cases s) 
    apply (auto simp:)
    done

  definition wf_domain' :: "_ \<Rightarrow> _ \<Rightarrow> bool" where
    "wf_domain' stg conT \<equiv>
      wf_types
    \<and> distinct (map (predicate_decl.pred) (predicates D))
    \<and> (\<forall>p\<in>set (predicates D). wf_predicate_decl p)
    \<and> distinct (map fst (consts D))
    \<and> (\<forall>(n,T)\<in>set (consts D). wf_type T)
    \<and> distinct (map fst (functions D))
    \<and> (\<forall>(n,sg)\<in>set (functions D). wf_fun_sig sg)
    \<and> distinct (map ast_action_schema.name (actions D))
    \<and> (\<forall>a\<in>set (actions D). wf_action_schema' stg conT a)
    "

  lemma wf_domain'_correct[abs_def, simp]: "wf_domain' STG mp_constT = wf_domain"
    unfolding wf_domain_def wf_domain'_def
    by (auto simp:)


end \<comment> \<open>Context of \<open>ast_domain\<close>\<close>


subsubsection \<open>Application of Effects\<close>

definition "set_remove_insert s sd sa 
  \<equiv> fold (\<lambda>x s. Set.insert x s) sa (fold (\<lambda>x s. Set.remove x s) sd s)"

lemma set_insert_remove_correct: "set_remove_insert s sd sa = s - set sd \<union> set sa"
  by (simp add: List.minus_set_fold set_remove_insert_def sup_commute union_set_fold)
  

type_synonym fluent_valuation_impl = "(fun_name \<times> object list, object) mapping"  
type_synonym world_model_impl = "fluent_valuation_impl \<times> basic_world_model"
  
definition wm_\<alpha> :: "world_model_impl \<Rightarrow> world_model" where 
  "wm_\<alpha> \<equiv> map_prod Mapping.lookup id"

fun apply_basic_assignment_impl where
  "apply_basic_assignment_impl (ASSIGN f xs y) F = (
    case y of Some y \<Rightarrow> Mapping.update (f,xs) y F | None \<Rightarrow> Mapping.delete (f,xs) F)"
  
  
context ast_domain begin
  text \<open>We implement the application of an effect by explicit iteration over
    the additions and deletions\<close>

  lemma apply_basic_assignment_impl_refine[simp]: 
    "Mapping.lookup (apply_basic_assignment_impl a F) = apply_basic_assignment a (Mapping.lookup F)"    
    apply (cases a) 
    apply (auto split: option.splits simp: Mapping.delete.rep_eq Mapping.lookup.rep_eq Mapping.update.rep_eq)
    done

  lemma apply_basic_assignments_impl_refine[simp]: 
    "Mapping.lookup (fold apply_basic_assignment_impl xs F) = fold apply_basic_assignment xs (Mapping.lookup F)"      
    by (induction xs arbitrary: F) auto  
    
        
  fun apply_basic_effect_impl
    :: "object ast_effect \<Rightarrow> world_model_impl \<Rightarrow> world_model_impl"
  where
    "apply_basic_effect_impl (Effect a d ass) (F,P)
      = (fold apply_basic_assignment_impl ass F, set_remove_insert P d a)"

  lemma apply_basic_effect_impl_refine[simp]:    
    "wm_\<alpha> (apply_basic_effect_impl e s) = apply_basic_effect e (wm_\<alpha> s)"
    by (cases e; cases s; auto simp: wm_\<alpha>_def set_insert_remove_correct)


end \<comment> \<open>Context of \<open>ast_domain\<close>\<close>

subsubsection \<open>Well-Formedness\<close>

context ast_problem begin

  text \<open> We start by defining a mapping from objects to types. The container
    framework will generate efficient, red-black tree based code for that
    later. \<close>

  type_synonym objT = "(object, type) mapping"

  definition mp_objT :: "(object, type) mapping" where
    "mp_objT = Mapping.of_alist (consts D @ objects P)"

  lemma mp_objT_correct[simp]: "Mapping.lookup mp_objT = objT"
    unfolding mp_objT_def objT_alt
    by transfer (simp add: Map_To_Mapping.map_apply_def)

  text \<open>We refine the typecheck to use the mapping\<close>

  (*
  definition "is_obj_of_type_impl stg mp n T = (
    case Mapping.lookup mp n of None \<Rightarrow> False | Some oT \<Rightarrow> of_type_impl stg oT T
  )"

  lemma is_obj_of_type_impl_correct[simp]:
    "is_obj_of_type_impl STG mp_objT = is_obj_of_type"
    apply (intro ext)
    apply (auto simp: is_obj_of_type_impl_def is_obj_of_type_def of_type_impl_correct split: option.split)
    done
  *)

  text \<open>We refine the well-formedness checks to use the mapping\<close>

  definition wf_fact' :: "objT \<Rightarrow> _ \<Rightarrow> fact \<Rightarrow> bool"
    where
    "wf_fact' ot stg \<equiv> wf_pred_atom' (Mapping.lookup ot) stg"

  lemma wf_fact'_correct[simp]: "wf_fact' mp_objT STG = wf_fact"
    by (auto simp: wf_fact'_def wf_fact_def wf_pred_atom'_correct[abs_def])


  definition "wf_fmla_atom2' mp stg f
    = (case f of formula.Atom (predAtm p vs) \<Rightarrow> (wf_fact' mp stg (p,vs)) | _ \<Rightarrow> False)"

  lemma wf_fmla_atom2'_correct[abs_def, simp]:
    "wf_fmla_atom2' mp_objT STG \<phi> = wf_fmla_atom objT \<phi>"
    apply (cases \<phi> rule: wf_fmla_atom.cases)
    by (auto simp: wf_fmla_atom2'_def wf_fact_def split: option.splits)

    
  fun wf_init_el' where
    "wf_init_el' ot stg (ATOM p xs) = wf_fact' ot stg (p,xs)"
  | "wf_init_el' ot stg (FUNVAL f xs y) = wf_funval' (Mapping.lookup ot) stg f xs (Some y)"
    
  lemma init_el_correct[abs_def, simp]: "wf_init_el' mp_objT STG e = wf_init_el e"
    supply [simp del] = wf_fmla_atom.simps wf_pred_atom.simps
    by (cases e) auto
  
    
  definition "wf_problem' stg conT mp \<equiv>
      wf_domain' stg conT
    \<and> distinct (map fst (objects P) @ map fst (consts D))
    \<and> (\<forall>(n,T)\<in>set (objects P). wf_type T)
    \<and> (distinct (map fst fun_inits))
    \<and> (\<forall>f\<in>set (init P). wf_init_el' mp stg f)
    \<and> wf_fmla' (ty_term' (Mapping.lookup mp) stg) stg (goal P)"

  lemma wf_problem'_correct[simp]:
    "wf_problem' STG mp_constT mp_objT = wf_problem"
    unfolding wf_problem_def wf_problem'_def wf_world_model_def
    by (auto simp: )


  text \<open>Instantiating actions will yield well-founded effects.
    Corollary of @{thm wf_instantiate_action_schema}.\<close>
  lemma wf_effect_inst_weak:
    fixes a args
    defines "ai \<equiv> instantiate_action_schema a args"
    assumes A: "action_params_match a args"
      "wf_action_schema a"
    shows "wf_effect_inst (effect ai)"
    using wf_instantiate_action_schema[OF A] unfolding ai_def[symmetric]
    by (cases ai) (auto simp: )


end \<comment> \<open>Context of \<open>ast_problem\<close>\<close>


context wf_ast_domain begin
  text \<open>Resolving an action yields a well-founded action schema.\<close>
  (* TODO: This must be implicitly proved when showing that plan execution
    preserves wf. Try to remove this redundancy!*)
  lemma resolve_action_wf:
    assumes "resolve_action_schema n = Some a"
    shows "wf_action_schema a"
  proof -
    from wf_domain have
      X1: "distinct (map ast_action_schema.name (actions D))"
      and X2: "\<forall>a\<in>set (actions D). wf_action_schema a"
      unfolding wf_domain_def by auto

    show ?thesis
      using assms unfolding resolve_action_schema_def
      by (auto simp add: index_by_eq_Some_eq[OF X1] X2)
  qed

end \<comment> \<open>Context of \<open>ast_domain\<close>\<close>


subsubsection \<open>Execution of Plan Actions\<close>

lemma omap_atom_alt: "omap_dt set_atom map_atom f a = (case a of 
    predAtm p args \<Rightarrow> do { args \<leftarrow> omap f args; Some (predAtm p args)}
  | atom.Eq l r \<Rightarrow> do { l\<leftarrow>f l; r\<leftarrow>f r; Some (atom.Eq l r) }
  )"
  apply (cases a)
  apply (auto simp: omap_dt_def omap_alt split: Option.bind_splits)
  done

context fixes f :: "'a \<rightharpoonup> 'b" begin  
  fun omap_formula' where
    "omap_formula' (Atom a) = do {a\<leftarrow>f a; Some (Atom a)}"  
  | "omap_formula' Bot = Some Bot"  
  | "omap_formula' (Not \<phi>) = do {\<phi>\<leftarrow>omap_formula' \<phi>; Some (Not \<phi>)}"
  | "omap_formula' (And \<phi>\<^sub>1 \<phi>\<^sub>2) = do {\<phi>\<^sub>1\<leftarrow>omap_formula' \<phi>\<^sub>1; \<phi>\<^sub>2\<leftarrow>omap_formula' \<phi>\<^sub>2; Some (And \<phi>\<^sub>1 \<phi>\<^sub>2)}"
  | "omap_formula' (Or \<phi>\<^sub>1 \<phi>\<^sub>2) = do {\<phi>\<^sub>1\<leftarrow>omap_formula' \<phi>\<^sub>1; \<phi>\<^sub>2\<leftarrow>omap_formula' \<phi>\<^sub>2; Some (Or \<phi>\<^sub>1 \<phi>\<^sub>2)}"
  | "omap_formula' (Imp \<phi>\<^sub>1 \<phi>\<^sub>2) = do {\<phi>\<^sub>1\<leftarrow>omap_formula' \<phi>\<^sub>1; \<phi>\<^sub>2\<leftarrow>omap_formula' \<phi>\<^sub>2; Some (Imp \<phi>\<^sub>1 \<phi>\<^sub>2)}"


  lemma omap_formula_alt[abs_def]: "omap_dt formula.atoms map_formula f \<phi> = omap_formula' \<phi>"
    apply (rule sym)
    apply (induction \<phi>)
    apply (auto simp: omap_dt_def split: Option.bind_splits)
    done
    
end  


context fixes F :: fluent_valuation_impl begin

definition [simp]: "feval_term' \<equiv> feval_term (Mapping.lookup F)"

(* Transform effects, assignments, atoms, and formula from terms to objects *)
definition "feval_ast_effect' \<equiv> omap_dt set_ast_effect map_ast_effect feval_term'"
definition "feval_atom' \<equiv> omap_dt set_atom map_atom feval_term'"
definition "feval_atom_formula' \<equiv> omap_dt formula.atoms map_formula feval_atom'"

lemmas [code] = feval_ast_effect'_def[unfolded omap_effect_alt]
lemmas [code] = feval_atom'_def[unfolded omap_atom_alt]
lemmas [code] = feval_atom_formula'_def[unfolded omap_formula_alt]

end  
  
lemma feval_ast_effect'_correct[simp]: "feval_ast_effect' F = feval_ast_effect (Mapping.lookup F)"
  unfolding feval_ast_effect'_def feval_ast_effect_def by simp

lemma feval_atom'_correct[simp]: "feval_atom' F = feval_atom (Mapping.lookup F)"
  unfolding feval_atom'_def feval_atom_def by simp

lemma feval_atom_formula'_correct[simp]: "feval_atom_formula' F = feval_atom_formula (Mapping.lookup F)"
  unfolding feval_atom_formula'_def feval_atom_formula_def by simp
  
  
text \<open>We will perform two refinement steps, to summarize redundant operations\<close>


definition "index_by_impl f l \<equiv> Mapping.of_alist (map (\<lambda>x. (f x,x)) l)"

lemma index_by_impl_correct[simp]: "Mapping.lookup (index_by_impl f xs) = index_by f xs"
  unfolding index_by_def index_by_impl_def
  by (auto simp: Mapping.lookup_of_alist)


text \<open>We first lift action schema lookup into the error monad. \<close>
context ast_domain begin
  definition "resolve_action_schemaE asm n \<equiv>
    lift_opt
      (Mapping.lookup asm n)
      (ERR (shows STR ''No such action schema '' o shows n))"
      
  term resolve_action_schema    
  definition "mk_asm_map \<equiv> index_by_impl ast_action_schema.name (actions D)"    
      
  lemma resolve_action_schemaE_correct[simp]: 
    "resolve_action_schemaE mk_asm_map n = Inr x \<longleftrightarrow> resolve_action_schema n = Some x"    
    unfolding resolve_action_schemaE_def resolve_action_schema_def
    by (auto simp: lift_opt_return_iff mk_asm_map_def) 

  lemma resolve_action_schemaE_correct'[simp]: "resolve_action_schemaE mk_asm_map n = Inl x \<Longrightarrow> resolve_action_schema n = None"  
    by (metis option.discI option.expand option.sel resolve_action_schemaE_correct sum.simps(4))
      
end \<comment> \<open>Context of \<open>ast_domain\<close>\<close>


text \<open>We define a function to determine whether a formula holds in
  a world model\<close>
definition "holds M F \<equiv> (valuation M) \<Turnstile> F"

text \<open>Justification of this function\<close>

lemma holds_correct[simp]:
  shows "holds s F \<longleftrightarrow> close_basic_world s \<TTurnstile> F"
  unfolding holds_def using valuation_iff_close_basic_world
  by blast



definition "f_check_entails_impl \<equiv> \<lambda>(F,P) \<phi>. do {
  \<phi> \<leftarrow> feval_atom_formula' F \<phi>;
  oassert (holds P \<phi>)
}"
    
lemma f_check_entails_impl_correct[abs_def,simp]: "f_check_entails_impl s \<phi> = f_check_entails (wm_\<alpha> s) \<phi>"
  unfolding f_check_entails_impl_def f_check_entails_def wm_\<alpha>_def 
  by (auto split: Option.bind_splits prod.splits)

  
(* TODO: Move *)  
lemma lift_opt_Inl: "lift_opt x e = Inl e' \<longleftrightarrow> e'=e \<and> x=None"
  by (auto simp: lift_opt_def split: option.splits)
  

context ast_problem begin


    
  fun execute_ast_action_inst'
    :: "ast_action_inst \<Rightarrow> world_model_impl \<rightharpoonup> world_model_impl"
  where
    "execute_ast_action_inst' (Action_Inst pre eff) s = do {
      f_check_entails_impl s pre;
      eff \<leftarrow> feval_ast_effect' (fst s) eff;
      let s = apply_basic_effect_impl eff s;
      Some s
    }"

  lemma execute_ast_action_inst'_correct[simp]: 
    "execute_ast_action_inst' a s = Some s' \<Longrightarrow> execute_ast_action_inst a (wm_\<alpha> s) = Some (wm_\<alpha> s')"
    apply (cases a; cases s)
    apply (auto split: Option.bind_splits (*simp: wm_\<alpha>_def*))
    apply (auto simp: wm_\<alpha>_def)
    done

  lemma execute_ast_action_inst'_correct2: 
    "execute_ast_action_inst a (wm_\<alpha> s) = Some sa' \<Longrightarrow> (\<exists>s'. sa' = wm_\<alpha> s' \<and> execute_ast_action_inst' a s = Some s')"
    apply (cases a; cases s)
    apply (auto split: Option.bind_splits)
    apply (auto split: Option.bind_splits simp: wm_\<alpha>_def) [3]
    by (metis apfst_conv apfst_def ast_domain.apply_basic_effect_impl_refine old.prod.exhaust wm_\<alpha>_def)
    
        
  lemma execute_ast_action_inst'_correct'[simp]: 
    "execute_ast_action_inst' a s = None \<longleftrightarrow> execute_ast_action_inst a (wm_\<alpha> s) = None"
    apply (cases a; cases s)
    apply (auto split: Option.bind_splits (*simp: wm_\<alpha>_def*))
    apply (auto simp: wm_\<alpha>_def)
    done

    
  definition "check_action_params_match mp stg a args \<equiv> do {
    vTs \<leftarrow> omap (Mapping.lookup mp) args;
    let Ts = map snd (parameters a);
    oassert (list_all2 (of_type_impl stg) vTs Ts)
  }"
    
  lemma (in wf_ast_problem) check_action_params_match_correct[simp]:
    "check_action_params_match mp_objT STG a args = Some () \<longleftrightarrow> action_params_match a args"
    unfolding check_action_params_match_def action_params_match_def
    by (auto split: Option.bind_splits)
    
    
  definition "execute_plan_actionE mp stg asm pact M \<equiv> do {
    as \<leftarrow> resolve_action_schemaE asm (plan_action.name pact);
    lift_opt (check_action_params_match mp stg as (plan_action.arguments pact)) (ERRS STR ''Parameter mismatch'');
    let a = instantiate_action_schema as (plan_action.arguments pact);
    lift_opt (execute_ast_action_inst' a M) (ERRS STR ''Execute action instance error'')
  }"  
  
  lemma (in wf_ast_problem) execute_plan_actionE_correct[simp]:
    "execute_plan_actionE mp_objT STG mk_asm_map pact M = Inr r \<Longrightarrow> execute_plan_action pact (wm_\<alpha> M) = Some (wm_\<alpha> r)"
    unfolding execute_plan_actionE_def execute_plan_action_def 
    apply (case_tac [!] pact)
    apply (auto 
      simp: lift_opt_return_iff wf_effect_inst_weak wf_resolve_action_schema 
      split: Option.bind_splits error_monad_bind_splits option.splits)
    done    

  lemma (in wf_ast_problem) execute_plan_actionE_correct'[simp]:
    "execute_plan_actionE mp_objT STG mk_asm_map pact M = Inl e \<Longrightarrow> execute_plan_action pact (wm_\<alpha> M) = None"
    unfolding execute_plan_actionE_def execute_plan_action_def 
    apply (case_tac pact)
    apply (auto 
      simp: lift_opt_return_iff lift_opt_Inl wf_effect_inst_weak wf_resolve_action_schema check_return_iff
      split: Option.bind_splits error_monad_bind_splits option.splits)
    done  
    
  thm execute_plan_action_path_simps  
    
  fun execute_plan_action_pathE where
    "execute_plan_action_pathE mp stg asm [] s = Inr s"
  | "execute_plan_action_pathE mp stg asm (\<pi>#\<pi>s) s = execute_plan_actionE mp stg asm \<pi> s \<bind> execute_plan_action_pathE mp stg asm \<pi>s"   
  
  lemma (in wf_ast_problem) execute_plan_action_pathE_correct[simp]:
    "execute_plan_action_pathE mp_objT STG mk_asm_map \<pi>s s = Inr s' \<Longrightarrow> execute_plan_action_path \<pi>s (wm_\<alpha> s) = Some (wm_\<alpha> s')"
    apply (induction \<pi>s arbitrary: s)
    apply (auto simp: execute_plan_action_path_simps split: error_monad_bind_splits Option.bind_splits)
    by fastforce
  
  lemma (in wf_ast_problem) execute_plan_action_pathE_correct'[simp]:
    "execute_plan_action_pathE mp_objT STG mk_asm_map \<pi>s s = Inl e \<Longrightarrow> execute_plan_action_path \<pi>s (wm_\<alpha> s) = None"
    apply (induction \<pi>s arbitrary: s)
    apply (auto simp: execute_plan_action_path_simps split: error_monad_bind_splits Option.bind_splits)
    by fastforce
    
      
  term f_check_entails_impl  
      
  definition "valid_plan_fromE mp stg asm \<pi>s s \<equiv> do {
      s' \<leftarrow> execute_plan_action_pathE mp stg asm \<pi>s s;
      lift_opt (f_check_entails_impl s' (goal P)) (ERRS STR ''Final state doesn't entail goal'')
    }"
    
  lemma (in wf_ast_problem) valid_plan_from_correct[simp]: 
    "valid_plan_fromE mp_objT STG mk_asm_map \<pi>s s = Inr () \<longleftrightarrow> valid_plan_from \<pi>s (wm_\<alpha> s)"  
    unfolding valid_plan_fromE_def valid_plan_from_def
    by (auto split: Option.bind_splits error_monad_bind_splits simp: lift_opt_return_iff)

    
  term I
  thm I_def  

  definition "atom_inits \<equiv> map (\<lambda>ATOM p xs \<Rightarrow> (p,xs)) (filter is_atom (init P))"
      
  lemma I_alt: "I = (map_of fun_inits, set atom_inits)"
    unfolding I_def atom_inits_def
    by (force split: init_el.splits)
    
  definition "I_impl \<equiv> (Mapping.of_alist fun_inits, set atom_inits)"
  
  lemma I_impl_correct[simp]: "wm_\<alpha> I_impl = I"
    unfolding I_impl_def I_alt wm_\<alpha>_def
    by (auto simp: lookup_of_alist)

  definition valid_planE
    where "valid_planE mp stg asm \<pi>s \<equiv> valid_plan_fromE mp stg asm \<pi>s I_impl"
    
  theorem (in wf_ast_problem) valid_planE_correct[return_iff]: "isOK (valid_planE mp_objT STG mk_asm_map \<pi>s) \<longleftrightarrow> valid_plan \<pi>s"  
    unfolding valid_plan_def valid_planE_def
    by auto
    


end \<comment> \<open>Context of \<open>ast_problem\<close>\<close>

subsection \<open>Executable Plan Checker\<close>
text \<open>We obtain the main plan checker by combining the well-formedness check
  and executability check. \<close>

(* TODO: Move *)  
lemma isOK_unit[simp]: "m = Inr (x::unit) \<longleftrightarrow> isOK m" by auto

(* TODO: Move *)  
definition "prepend_err_msg msg e \<equiv> \<lambda>_::unit. shows msg o shows STR '': '' o e ()"


definition "check_all_list P l msg msgf \<equiv>
  forallM (\<lambda>x. check (P x) (\<lambda>_::unit. shows msg o shows STR '': '' o msgf x) ) l <+? snd"

lemma check_all_list_return_iff[return_iff]: "isOK (check_all_list P l msg msgf) \<longleftrightarrow> (\<forall>x\<in>set l. P x)"
  unfolding check_all_list_def
  by (induction l) (auto)

definition "check_wf_types D \<equiv> do {
  check_all_list (\<lambda>(_,t). t=STR ''object'' \<or> t\<in>fst`set (types D)) (types D) STR ''Undeclared supertype'' (shows o snd)
}"

lemma check_wf_types_return_iff[return_iff]: "isOK (check_wf_types D) \<longleftrightarrow> ast_domain.wf_types D"
  unfolding ast_domain.wf_types_def check_wf_types_def
  by (force simp: return_iff)


context ast_domain begin  
  
definition "check_wf_domain stg conT \<equiv> do {
  check_wf_types D;
  check (distinct (map (predicate_decl.pred) (predicates D))) (ERRS STR ''Duplicate predicate declaration'');
  check_all_list (wf_predicate_decl) (predicates D) STR ''Malformed predicate declaration'' (shows o predicate.name o predicate_decl.pred);
  check (distinct (map fst (consts D))) (ERRS STR ''Duplicate constant declaration'');
  check (\<forall>(n,T)\<in>set (consts D). wf_type T) (ERRS STR ''Malformed type'');
  check (distinct (map fst (functions D))) (ERRS STR ''Duplicate function declaration'');
  check_all_list (wf_fun_sig o snd) (functions D) STR ''Malformed function signature'' (shows o fun_name.name o fst);
  check (distinct (map ast_action_schema.name (actions D))  ) (ERRS STR ''Duplicate action name'');
  check_all_list (wf_action_schema' stg conT) (actions D) STR ''Malformed action'' (shows o ast_action_schema.name)
}"  
    

lemma check_wf_domain_return_iff[return_iff]:
  "isOK (check_wf_domain stg conT) \<longleftrightarrow> wf_domain' stg conT"
  unfolding check_wf_domain_def wf_domain'_def
  by (auto simp: return_iff simp del: wf_fun_sig.simps)


end

context ast_problem begin
term wf_problem

end

context ast_problem begin

definition "check_wf_problem stg conT mp \<equiv> do {
  check_wf_domain stg conT <+? prepend_err_msg STR ''Domain not well-formed'';
  check (distinct (map fst (objects P) @ map fst (consts D))) (ERRS STR ''Duplicate object declaration'');
  check_all_list (wf_type o snd) (objects P) STR ''Malformed type of object'' (shows o object.name o fst);
  check (distinct (map fst fun_inits)) (ERRS STR ''Duplicate function initialization'');
  check_all_list (wf_init_el' mp stg) (init P) STR ''Malformed initializer'' (\<lambda>_. shows STR ''TODO: Add initalizer info here'');
  check (wf_fmla' (ty_term' (Mapping.lookup mp) stg) stg (goal P)) (ERRS STR ''Malformed goal formula'')
}"

lemma check_wf_problem_correct[return_iff]: "isOK (check_wf_problem stg conT mp) \<longleftrightarrow> wf_problem' stg conT mp"
  unfolding check_wf_problem_def wf_problem'_def
  by (auto simp: return_iff)


end


definition "check_plan P \<pi>s \<equiv> do {
  let D = ast_problem.domain P;
  let stg=ast_domain.STG D;
  let conT = ast_domain.mp_constT D;
  let mp = ast_problem.mp_objT P;
  let asm = ast_domain.mk_asm_map D;
  ast_problem.check_wf_problem P stg conT mp;
  ast_problem.valid_planE P mp stg asm \<pi>s
} <+? (\<lambda>e. String.implode (e () ''''))"

text \<open>Correctness theorem of the plan checker: It returns @{term "Inr ()"}
  if and only if the problem is well-formed and the plan is valid.
\<close>
theorem check_plan_return_iff[return_iff]: "check_plan P \<pi>s = Inr ()
  \<longleftrightarrow> ast_problem.wf_problem P \<and> ast_problem.valid_plan P \<pi>s"
proof -
  interpret ast_problem P .
  show ?thesis 
  proof (cases "wf_problem")
    case True
    then interpret wf_ast_problem P by unfold_locales
    show ?thesis
      unfolding check_plan_def
      by (auto simp: return_iff)
  next
    case False
    then show ?thesis
      unfolding check_plan_def
      by (auto simp: return_iff)
  qed        
qed
        



subsection \<open>Code Setup\<close>

text \<open>In this section, we set up the code generator to generate verified
  code for our plan checker.\<close>

subsubsection \<open>Code Equations\<close>
text \<open>We first register the code equations for the functions of the checker.
  Note that we not necessarily register the original code equations, but also
  optimized ones.
\<close>

lemmas wf_domain_code =
  ast_domain.sig_def
  ast_domain.wf_types_def
  ast_domain.wf_type.simps
  ast_domain.wf_predicate_decl.simps
  ast_domain.STG_def
  (*of_type_impl_def*)
  ast_domain.wf_atom'.simps
  ast_domain.wf_pred_atom'.simps
  ast_domain.wf_fmla'.simps
  ast_domain.wf_fmla_atom1'.simps
  ast_domain.wf_effect'.simps
  ast_domain.wf_action_schema'.simps
  ast_domain.wf_domain'_def
  ast_domain.mp_constT_def
(*
  ast_domain.wf_domain_def
  ast_domain.wf_action_schema.simps
  ast_domain.wf_effect.simps
  ast_domain.wf_fmla.simps
  ast_domain.wf_atom.simps
  ast_domain.is_of_type_def
  ast_domain.of_type_code
*)

declare wf_domain_code[code]

lemmas wf_problem_code =
  ast_problem.wf_problem'_def
  ast_problem.wf_fact'_def
  (*ast_problem.objT_def*)
  
  (*ast_problem.wf_object_def*)
  ast_problem.wf_fact_def
  ast_problem.wf_plan_action.simps
  (*ast_problem.wf_effect_inst_weak.simps*)

  ast_domain.subtype_edge.simps
  
  ast_problem.check_wf_problem_def

  ast_domain.check_wf_domain_def
  ast_problem.wf_init_el'.simps
  ast_domain.ty_term'.simps
    
  ast_domain.wf_assignment'.simps 
  ast_domain.wf_fun_sig.simps 
  ast_domain.wf_funval'_def 
  ast_domain.fsig_def  

declare wf_problem_code[code]

lemmas check_code =
  ast_problem.valid_planE_def
  ast_problem.valid_plan_fromE_def
  
  ast_problem.resolve_instantiate.simps
  ast_domain.resolve_action_schema_def
  ast_domain.resolve_action_schemaE_def
  ast_problem.I_def
  ast_domain.instantiate_action_schema.simps
  
  (*ast_domain.apply_effect_exec'.simps*)
  
  (*ast_domain.apply_atomic.simps*)
  
  ast_problem.mp_objT_def
  
  ast_problem.wf_fmla_atom2'_def
  valuation_def
  
  ast_problem.fun_inits_def
  ast_problem.execute_plan_action_pathE.simps
  ast_domain.mk_asm_map_def 
  ast_problem.I_impl_def 
  
  ast_problem.execute_plan_actionE_def
  ast_problem.atom_inits_def
  
  ast_problem.check_action_params_match_def
  ast_problem.execute_ast_action_inst'.simps
  ast_domain.subst_varobj.simps
  
  ast_domain.apply_basic_effect_impl.simps  
  
declare check_code[code]

subsubsection \<open>Setup for Containers Framework\<close>

derive (eq) ceq predicate atom object formula
derive ccompare predicate atom object formula fun_name
derive (rbt) set_impl atom formula predicate

derive (rbt) mapping_impl object fun_name

derive linorder predicate object atom "object atom formula"

subsubsection \<open>More Efficient Distinctness Check for Linorders\<close>
(* TODO: Can probably be optimized even more. *)
fun no_stutter :: "'a list \<Rightarrow> bool" where
  "no_stutter [] = True"
| "no_stutter [_] = True"
| "no_stutter (a#b#l) = (a\<noteq>b \<and> no_stutter (b#l))"

lemma sorted_no_stutter_eq_distinct: "sorted l \<Longrightarrow> no_stutter l \<longleftrightarrow> distinct l"
  apply (induction l rule: no_stutter.induct)
  apply (auto simp: )
  done

definition distinct_ds :: "'a::linorder list \<Rightarrow> bool"
  where "distinct_ds l \<equiv> no_stutter (quicksort l)"

lemma [code_unfold]: "distinct = distinct_ds"
  apply (intro ext)
  unfolding distinct_ds_def
  apply (auto simp: sorted_no_stutter_eq_distinct)
  done

subsubsection \<open>Code Generation\<close>

(* TODO/FIXME: Code_Char was removed from Isabelle-2018! 
  Check for performance regression of generated code!
*)

typ "init_el"

export_code
  check_plan
  nat_of_integer integer_of_nat Inl Inr
  predAtm Eq predicate Pred Either Var Obj PredDecl BigAnd BigOr
  formula.Not formula.Bot Effect ast_action_schema.Action_Schema
  map_atom Domain Problem PAction
  varobj.VAR varobj.CONST
  "term.ENT" "term.FUN"
  init_el.ATOM init_el.FUNVAL
  (*term.CONST term.VAR (* I want to export the entire type, but I can only export the constructor because term is already an isabelle keyword. *)*)
  String.explode String.implode
  in SML
  module_name PDDL_Checker_Exported
  file "code/PDDL_STRIPS_Checker_Fluents_Exported.sml"

(*export_code ast_domain.apply_effect_exec in SML module_name ast_domain*)
(* Usage example from within Isabelle *)
(*
ML_val \<open>
  let
    val prefix="/home/lammich/devel/isabelle/planning/papers/pddl_validator/experiments/results/"

    fun check name =
      (name,@{code parse_check_dpp_impl}
        (file_to_string (prefix ^ name ^ ".dom.pddl"))
        (file_to_string (prefix ^ name ^ ".prob.pddl"))
        (file_to_string (prefix ^ name ^ ".plan")))

  in
    (*check "IPC5_rovers_p03"*)
    check "Test2_rover_untyped_pfile07"
  end
\<close>
*)
end \<comment> \<open>Theory\<close>
