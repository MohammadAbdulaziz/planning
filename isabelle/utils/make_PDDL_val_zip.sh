#!/bin/bash

rm PDDL_val.tgz
cp PDDL_ver_val_README README
cp PDDL_VAL_ROOT ROOT

FILES=`ls -d1 README ../../bin/validate_pddl_plan.sh ../Error_Monad_Add.thy ../Lifschitz_Consistency.thy ../PDDL_STRIPS_Checker.thy ../PDDL_STRIPS_Semantics.thy ../Paper_Texgen.thy ../document/root.tex ROOT ../../isabelle/code/PDDL_STRIPS_Checker_Exported.sml ../../codeBase/planning/pddlParser/{pddl_refactor.sml,pddl_validate_plan.sml,pddl_validate_plan.mlb} ../../examples/{d-1-2.pddl,ged3-itt_no_invariants.pddl,plan} ../../isabelle/output/*.pdf ../../codeBase/planning/SMLParsecomb`

tar -czf PDDL_val.tgz --transform 's|^|PDDL_val/|'  $FILES

cp PDDL_val.tgz /tmp/
rm README
rm ROOT
