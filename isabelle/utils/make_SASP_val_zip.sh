#!/bin/bash

rm SASP_val.tgz
cp SASP_ver_val_README README
cp SASP_VAL_ROOT ROOT

FILES=`ls -d1 README ../SASP_Checker.thy ../SASP_Semantics.thy ../Paper_Texgen.thy ../document/root.tex ROOT ../../isabelle/code/SASP_Checker_Exported.sml ../../codeBase/planning/stripsParser/{strips.sml,strips.mlb} ../../examples/{gripper.sas,gripper_sas_plan} ../../isabelle/output/*.pdf ../../codeBase/planning/SMLParsecomb`

tar -czf SASP_val.tgz --transform 's|^|SASP_val/|'  $FILES

cp SASP_val.tgz /tmp/
rm README
rm ROOT
