#!/bin/bash

FILES=`ls -d1 README.md ../../bin/* Error_Monad_Add.thy ../Lifschitz_Consistency.thy ../PDDL_STRIPS_Checker.thy ../PDDL_STRIPS_Semantics.thy ../Paper_Texgen.thy ../document/root.tex ../invariant_verification.thy ../ROOT ../build.sh ../code/PDDL_STRIPS_Checker_Exported.sml ../../codeBase/planning/pddlParser/{*.sml,*.mlb} ../../examples/* ../../isabelle/output/*.pdf ../../codeBase/planning/SMLParsecomb`


tar -czf verval.tgz --transform 's|^|verval/|'  $FILES
