(** Taken and adapted from
  Julius Michaelis and Tobias Nipkow: $AFP/Propositional_Proof_Systems
*)
theory Logic_Foundation
imports Main "HOL-Library.Countable"
begin

section \<open>Syntax\<close>

type_synonym name = string

datatype predicate = Pred (name: name)
text \<open>Some of the AST entities are defined over a polymorphic \<open>'val\<close> type,
  which gets either instantiated by variables (for domains)
  or objects (for problems).
\<close>

datatype ('val) formula =
    Atom (predicate: predicate) (arguments: "'val list")
  | Equal 'val 'val                      (infix "\<^bold>=" 70)
  | Bot                              ("\<bottom>")
  | Not "'val formula"                 ("\<^bold>\<not>")
  | And "'val formula" "'val formula"    (infix "\<^bold>\<and>" 68)
  | Or "'val formula" "'val formula"     (infix "\<^bold>\<or>" 68)
  | Imp "'val formula" "'val formula"    (infixr "\<^bold>\<rightarrow>" 68)


subsection\<open>Derived Connectives\<close>

definition Top ("\<top>") where
"\<top> \<equiv> \<bottom> \<^bold>\<rightarrow> \<bottom>"

text\<open>Formulas are countable if their values are, and @{method countable_datatype} is really helpful with that.\<close>
instance predicate :: countable by countable_datatype
instance formula :: (countable) countable by countable_datatype


section \<open>Semantics\<close>


type_synonym 'val valuation = "(predicate \<times> 'val list) \<Rightarrow> bool"
text\<open>The implicit statement here is that an assignment or valuation is always defined on all atoms (because HOL is a total logic).
Thus, there are no unsuitable assignments.
\<close>

text \<open>We fix the interpretation of equality in a locale\<close>
locale sema =
  fixes equal :: "'val \<Rightarrow> 'val \<Rightarrow> bool"

  assumes eq_refl[simp]: "equal x x"
  assumes eq_sym: "equal x y \<Longrightarrow> equal y x"
  assumes eq_trans[trans]: "equal x y \<Longrightarrow> equal y z \<Longrightarrow> equal x z"
begin


primrec formula_semantics :: "'val valuation \<Rightarrow> 'val formula \<Rightarrow> bool" (infix "\<Turnstile>" 51) where
"\<A> \<Turnstile> Atom p as = \<A> (p,as)" |
"_ \<Turnstile> Equal a b = equal a b" |
"_ \<Turnstile> \<bottom> = False" |
"\<A> \<Turnstile> Not F = (\<not> \<A> \<Turnstile> F)" |
"\<A> \<Turnstile> And F G = (\<A> \<Turnstile> F \<and> \<A> \<Turnstile> G)" |
"\<A> \<Turnstile> Or F G = (\<A> \<Turnstile> F \<or> \<A> \<Turnstile> G)" |
"\<A> \<Turnstile> Imp F G = (\<A> \<Turnstile> F \<longrightarrow> \<A> \<Turnstile> G)"

abbreviation valid ("\<Turnstile> _" 51) where
"\<Turnstile> F \<equiv> \<forall>A. A \<Turnstile> F"


definition entailment :: "'val formula set \<Rightarrow> 'val formula \<Rightarrow> bool" ("(_ \<TTurnstile>/ _)" (* \TTurnstile *) [53,53] 53) where
"\<Gamma> \<TTurnstile> F \<equiv> (\<forall>\<A>. ((\<forall>G \<in> \<Gamma>. \<A> \<Turnstile> G) \<longrightarrow> (\<A> \<Turnstile> F)))"
text\<open>We write entailment differently than semantics (\<open>\<Turnstile>\<close> vs. \<open>\<TTurnstile>\<close>).
For humans, it is usually pretty clear what is meant in a specific situation,
but it often needs to be decided from context that Isabelle/HOL does not have.\<close>

text\<open>Some helpers for the derived connectives\<close>
lemma top_semantics[simp,intro!]: "A \<Turnstile> \<top>" unfolding Top_def by simp

end

end
