#!/bin/bash
while IFS='' read -r line || [[ -n "$line" ]]; do
 old=`echo $line | cut -d " " -f 1`;
 new=`echo $line | cut -d " " -f 2`;
 echo $line
 # echo ${old}_${new}
 sed -i 's/'"$old"'/'"$new"'/g' *.thy
 old=$(sed 's/_/\\\\_/g' <<<$old)
 new=$(sed 's/_/\\\\_/g' <<<$new)
 echo ${old}_${new}
 sed -i 's/'"$old"'/'"$new"'/g' *.thy

 # cd ../thesis
 # sed -i 's/'"$old"'/'"$new"'/g' prettyPrintingScript.sml
 # sed -i 's/'"$old"'/'"$new"'/g' *.htex
 # cd ../papers
 # for dir in `echo "ITP2015/ MFCS/  Tightness/ diameterUpperBounding/  sspaceAcyclicity/ LICS/ SODA/  diamBoundingFormalisationJAR/  itp2014/"`; do
 #   # echo $dir
 #   sed -i 's/'"$old"'/'"$new"'/g'  ${dir}/prettyPrintingScript.sml
 #   sed -i 's/'"$old"'/'"$new"'/g' ${dir}/*.htex
 # done
done < usedNames

