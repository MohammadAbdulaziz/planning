;; rover::roverprob7126, problem open #3690
;; selected constraints: {e12, e13, sb223}
;; value = 90.9305, penalty = 2830.9883
(define (domain rover)
 (:predicates (at,rover0,waypoint0) (at,rover0,waypoint1) (at,rover0,waypoint6) (at,rover0,waypoint7) (at,rover0,waypoint3) (at,rover0,waypoint4) (at,rover0,waypoint5) (at,rover0,waypoint2) (at,rover1,waypoint0) (at,rover1,waypoint3) (at,rover1,waypoint6) (at,rover1,waypoint1) (at,rover1,waypoint7) (at,rover1,waypoint2) (at,rover1,waypoint4) (at,rover1,waypoint5) (at,rover2,waypoint0) (at,rover2,waypoint3) (at,rover2,waypoint4) (at,rover2,waypoint1) (at,rover2,waypoint2) (at,rover2,waypoint5) (at,rover2,waypoint6) (at,rover2,waypoint7) (at,rover3,waypoint0) (at,rover3,waypoint4) (at,rover3,waypoint7) (at,rover3,waypoint1) (at,rover3,waypoint2) (at,rover3,waypoint3) (at,rover3,waypoint6) (at,rover3,waypoint5) (at_soil_sample,waypoint0) (empty,rover0store) (full,rover0store) (have_soil_analysis,rover0,waypoint0) (at_soil_sample,waypoint1) (have_soil_analysis,rover0,waypoint1) (at_soil_sample,waypoint3) (have_soil_analysis,rover0,waypoint3) (at_soil_sample,waypoint6) (have_soil_analysis,rover0,waypoint6) (empty,rover1store) (full,rover1store) (have_soil_analysis,rover1,waypoint0) (have_soil_analysis,rover1,waypoint1) (have_soil_analysis,rover1,waypoint3) (have_soil_analysis,rover1,waypoint6) (empty,rover2store) (full,rover2store) (have_soil_analysis,rover2,waypoint0) (have_soil_analysis,rover2,waypoint1) (have_soil_analysis,rover2,waypoint3) (have_soil_analysis,rover2,waypoint6) (empty,rover3store) (full,rover3store) (have_soil_analysis,rover3,waypoint0) (have_soil_analysis,rover3,waypoint1) (have_soil_analysis,rover3,waypoint3) (have_soil_analysis,rover3,waypoint6) (at_rock_sample,waypoint0) (have_rock_analysis,rover1,waypoint0) (at_rock_sample,waypoint1) (have_rock_analysis,rover1,waypoint1) (at_rock_sample,waypoint2) (have_rock_analysis,rover1,waypoint2) (at_rock_sample,waypoint4) (have_rock_analysis,rover1,waypoint4) (at_rock_sample,waypoint6) (have_rock_analysis,rover1,waypoint6) (at_rock_sample,waypoint7) (have_rock_analysis,rover1,waypoint7) (calibrated,camera3,rover0) (calibrated,camera0,rover1) (calibrated,camera1,rover2) (calibrated,camera2,rover3) (have_image,rover0,objective0,colour) (have_image,rover0,objective1,colour) (have_image,rover0,objective2,colour) (have_image,rover1,objective0,high_res) (have_image,rover1,objective0,low_res) (have_image,rover1,objective1,high_res) (have_image,rover1,objective1,low_res) (have_image,rover1,objective2,high_res) (have_image,rover1,objective2,low_res) (have_image,rover2,objective0,colour) (have_image,rover2,objective0,high_res) (have_image,rover2,objective1,colour) (have_image,rover2,objective1,high_res) (have_image,rover2,objective2,colour) (have_image,rover2,objective2,high_res) (have_image,rover3,objective0,high_res) (have_image,rover3,objective1,high_res) (have_image,rover3,objective2,high_res) (communicated_soil_data,waypoint0) (communicated_soil_data,waypoint1) (communicated_soil_data,waypoint3) (communicated_soil_data,waypoint6) (communicated_rock_data,waypoint0) (communicated_rock_data,waypoint1) (communicated_rock_data,waypoint2) (communicated_rock_data,waypoint4) (communicated_rock_data,waypoint6) (communicated_rock_data,waypoint7) (communicated_image_data,objective0,colour) (communicated_image_data,objective0,high_res) (communicated_image_data,objective0,low_res) (communicated_image_data,objective1,colour) (communicated_image_data,objective1,high_res) (communicated_image_data,objective1,low_res) (communicated_image_data,objective2,colour) (communicated_image_data,objective2,high_res) (communicated_image_data,objective2,low_res) (ok_e12) (ok_e13) (safe_sb223))
 (:action navigate,rover0,waypoint0,waypoint1
  :precondition (at,rover0,waypoint0)
  :effect (and (at,rover0,waypoint1) (not (at,rover0,waypoint0))))
 (:action navigate,rover0,waypoint0,waypoint6
  :precondition (at,rover0,waypoint0)
  :effect (and (at,rover0,waypoint6) (not (at,rover0,waypoint0))))
 (:action navigate,rover0,waypoint0,waypoint7
  :precondition (at,rover0,waypoint0)
  :effect (and (at,rover0,waypoint7) (not (at,rover0,waypoint0))))
 (:action navigate,rover0,waypoint1,waypoint0
  :precondition (at,rover0,waypoint1)
  :effect (and (at,rover0,waypoint0) (not (at,rover0,waypoint1))))
 (:action navigate,rover0,waypoint1,waypoint3
  :precondition (at,rover0,waypoint1)
  :effect (and (at,rover0,waypoint3) (not (at,rover0,waypoint1))))
 (:action navigate,rover0,waypoint1,waypoint4
  :precondition (at,rover0,waypoint1)
  :effect (and (at,rover0,waypoint4) (not (at,rover0,waypoint1))))
 (:action navigate,rover0,waypoint1,waypoint5
  :precondition (at,rover0,waypoint1)
  :effect (and (at,rover0,waypoint5) (not (at,rover0,waypoint1))))
 (:action navigate,rover0,waypoint2,waypoint3
  :precondition (at,rover0,waypoint2)
  :effect (and (at,rover0,waypoint3) (not (at,rover0,waypoint2))))
 (:action navigate,rover0,waypoint3,waypoint1
  :precondition (at,rover0,waypoint3)
  :effect (and (at,rover0,waypoint1) (not (at,rover0,waypoint3))))
 (:action navigate,rover0,waypoint3,waypoint2
  :precondition (at,rover0,waypoint3)
  :effect (and (at,rover0,waypoint2) (not (at,rover0,waypoint3))))
 (:action navigate,rover0,waypoint4,waypoint1
  :precondition (at,rover0,waypoint4)
  :effect (and (at,rover0,waypoint1) (not (at,rover0,waypoint4))))
 (:action navigate,rover0,waypoint5,waypoint1
  :precondition (at,rover0,waypoint5)
  :effect (and (at,rover0,waypoint1) (not (at,rover0,waypoint5))))
 (:action navigate,rover0,waypoint6,waypoint0
  :precondition (at,rover0,waypoint6)
  :effect (and (at,rover0,waypoint0) (not (at,rover0,waypoint6))))
 (:action navigate,rover0,waypoint7,waypoint0
  :precondition (at,rover0,waypoint7)
  :effect (and (at,rover0,waypoint0) (not (at,rover0,waypoint7))))
 (:action navigate,rover1,waypoint0,waypoint3
  :precondition (at,rover1,waypoint0)
  :effect (and (at,rover1,waypoint3) (not (at,rover1,waypoint0))))
 (:action navigate,rover1,waypoint0,waypoint6
  :precondition (at,rover1,waypoint0)
  :effect (and (at,rover1,waypoint6) (not (at,rover1,waypoint0))))
 (:action navigate,rover1,waypoint1,waypoint7
  :precondition (at,rover1,waypoint1)
  :effect (and (at,rover1,waypoint7) (not (at,rover1,waypoint1))))
 (:action navigate,rover1,waypoint2,waypoint7
  :precondition (at,rover1,waypoint2)
  :effect (and (at,rover1,waypoint7) (not (at,rover1,waypoint2))))
 (:action navigate,rover1,waypoint3,waypoint0
  :precondition (at,rover1,waypoint3)
  :effect (and (at,rover1,waypoint0) (not (at,rover1,waypoint3))))
 (:action navigate,rover1,waypoint3,waypoint7
  :precondition (at,rover1,waypoint3)
  :effect (and (at,rover1,waypoint7) (not (at,rover1,waypoint3))))
 (:action navigate,rover1,waypoint4,waypoint6
  :precondition (at,rover1,waypoint4)
  :effect (and (at,rover1,waypoint6) (not (at,rover1,waypoint4))))
 (:action navigate,rover1,waypoint5,waypoint6
  :precondition (at,rover1,waypoint5)
  :effect (and (at,rover1,waypoint6) (not (at,rover1,waypoint5))))
 (:action navigate,rover1,waypoint6,waypoint0
  :precondition (at,rover1,waypoint6)
  :effect (and (at,rover1,waypoint0) (not (at,rover1,waypoint6))))
 (:action navigate,rover1,waypoint6,waypoint4
  :precondition (at,rover1,waypoint6)
  :effect (and (at,rover1,waypoint4) (not (at,rover1,waypoint6))))
 (:action navigate,rover1,waypoint6,waypoint5
  :precondition (at,rover1,waypoint6)
  :effect (and (at,rover1,waypoint5) (not (at,rover1,waypoint6))))
 (:action navigate,rover1,waypoint7,waypoint1
  :precondition (at,rover1,waypoint7)
  :effect (and (at,rover1,waypoint1) (not (at,rover1,waypoint7))))
 (:action navigate,rover1,waypoint7,waypoint2
  :precondition (at,rover1,waypoint7)
  :effect (and (at,rover1,waypoint2) (not (at,rover1,waypoint7))))
 (:action navigate,rover1,waypoint7,waypoint3
  :precondition (at,rover1,waypoint7)
  :effect (and (at,rover1,waypoint3) (not (at,rover1,waypoint7))))
 (:action navigate,rover2,waypoint0,waypoint3
  :precondition (at,rover2,waypoint0)
  :effect (and (at,rover2,waypoint3) (not (at,rover2,waypoint0))))
 (:action navigate,rover2,waypoint0,waypoint4
  :precondition (at,rover2,waypoint0)
  :effect (and (at,rover2,waypoint4) (not (at,rover2,waypoint0))))
 (:action navigate,rover2,waypoint1,waypoint3
  :precondition (at,rover2,waypoint1)
  :effect (and (at,rover2,waypoint3) (not (at,rover2,waypoint1))))
 (:action navigate,rover2,waypoint2,waypoint3
  :precondition (at,rover2,waypoint2)
  :effect (and (at,rover2,waypoint3) (not (at,rover2,waypoint2))))
 (:action navigate,rover2,waypoint3,waypoint0
  :precondition (at,rover2,waypoint3)
  :effect (and (at,rover2,waypoint0) (not (at,rover2,waypoint3))))
 (:action navigate,rover2,waypoint3,waypoint1
  :precondition (at,rover2,waypoint3)
  :effect (and (at,rover2,waypoint1) (not (at,rover2,waypoint3))))
 (:action navigate,rover2,waypoint3,waypoint2
  :precondition (at,rover2,waypoint3)
  :effect (and (at,rover2,waypoint2) (not (at,rover2,waypoint3))))
 (:action navigate,rover2,waypoint3,waypoint5
  :precondition (at,rover2,waypoint3)
  :effect (and (at,rover2,waypoint5) (not (at,rover2,waypoint3))))
 (:action navigate,rover2,waypoint3,waypoint6
  :precondition (at,rover2,waypoint3)
  :effect (and (at,rover2,waypoint6) (not (at,rover2,waypoint3))))
 (:action navigate,rover2,waypoint3,waypoint7
  :precondition (at,rover2,waypoint3)
  :effect (and (at,rover2,waypoint7) (not (at,rover2,waypoint3))))
 (:action navigate,rover2,waypoint4,waypoint0
  :precondition (at,rover2,waypoint4)
  :effect (and (at,rover2,waypoint0) (not (at,rover2,waypoint4))))
 (:action navigate,rover2,waypoint5,waypoint3
  :precondition (at,rover2,waypoint5)
  :effect (and (at,rover2,waypoint3) (not (at,rover2,waypoint5))))
 (:action navigate,rover2,waypoint6,waypoint3
  :precondition (at,rover2,waypoint6)
  :effect (and (at,rover2,waypoint3) (not (at,rover2,waypoint6))))
 (:action navigate,rover2,waypoint7,waypoint3
  :precondition (at,rover2,waypoint7)
  :effect (and (at,rover2,waypoint3) (not (at,rover2,waypoint7))))
 (:action navigate,rover3,waypoint0,waypoint4
  :precondition (at,rover3,waypoint0)
  :effect (and (at,rover3,waypoint4) (not (at,rover3,waypoint0))))
 (:action navigate,rover3,waypoint0,waypoint7
  :precondition (at,rover3,waypoint0)
  :effect (and (at,rover3,waypoint7) (not (at,rover3,waypoint0))))
 (:action navigate,rover3,waypoint1,waypoint2
  :precondition (at,rover3,waypoint1)
  :effect (and (at,rover3,waypoint2) (not (at,rover3,waypoint1))))
 (:action navigate,rover3,waypoint1,waypoint7
  :precondition (at,rover3,waypoint1)
  :effect (and (at,rover3,waypoint7) (not (at,rover3,waypoint1))))
 (:action navigate,rover3,waypoint2,waypoint1
  :precondition (at,rover3,waypoint2)
  :effect (and (at,rover3,waypoint1) (not (at,rover3,waypoint2))))
 (:action navigate,rover3,waypoint3,waypoint6
  :precondition (at,rover3,waypoint3)
  :effect (and (at,rover3,waypoint6) (not (at,rover3,waypoint3))))
 (:action navigate,rover3,waypoint3,waypoint7
  :precondition (at,rover3,waypoint3)
  :effect (and (at,rover3,waypoint7) (not (at,rover3,waypoint3))))
 (:action navigate,rover3,waypoint4,waypoint0
  :precondition (at,rover3,waypoint4)
  :effect (and (at,rover3,waypoint0) (not (at,rover3,waypoint4))))
 (:action navigate,rover3,waypoint5,waypoint7
  :precondition (at,rover3,waypoint5)
  :effect (and (at,rover3,waypoint7) (not (at,rover3,waypoint5))))
 (:action navigate,rover3,waypoint6,waypoint3
  :precondition (at,rover3,waypoint6)
  :effect (and (at,rover3,waypoint3) (not (at,rover3,waypoint6))))
 (:action navigate,rover3,waypoint7,waypoint0
  :precondition (at,rover3,waypoint7)
  :effect (and (at,rover3,waypoint0) (not (at,rover3,waypoint7))))
 (:action navigate,rover3,waypoint7,waypoint1
  :precondition (at,rover3,waypoint7)
  :effect (and (at,rover3,waypoint1) (not (at,rover3,waypoint7))))
 (:action navigate,rover3,waypoint7,waypoint3
  :precondition (at,rover3,waypoint7)
  :effect (and (at,rover3,waypoint3) (not (at,rover3,waypoint7))))
 (:action navigate,rover3,waypoint7,waypoint5
  :precondition (at,rover3,waypoint7)
  :effect (and (at,rover3,waypoint5) (not (at,rover3,waypoint7))))
 (:action sample_soil,rover0,rover0store,waypoint0
  :precondition (and (at,rover0,waypoint0) (at_soil_sample,waypoint0) (empty,rover0store))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint0) (not (at_soil_sample,waypoint0)) (not (empty,rover0store))))
 (:action sample_soil,rover0,rover0store,waypoint1
  :precondition (and (at,rover0,waypoint1) (empty,rover0store) (at_soil_sample,waypoint1))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint1) (not (empty,rover0store)) (not (at_soil_sample,waypoint1))))
 (:action sample_soil,rover0,rover0store,waypoint3
  :precondition (and (at,rover0,waypoint3) (empty,rover0store) (at_soil_sample,waypoint3))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint3) (not (empty,rover0store)) (not (at_soil_sample,waypoint3))))
 (:action sample_soil,rover0,rover0store,waypoint6
  :precondition (and (at,rover0,waypoint6) (empty,rover0store) (at_soil_sample,waypoint6))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint6) (not (empty,rover0store)) (not (at_soil_sample,waypoint6))))
 (:action sample_soil,rover1,rover1store,waypoint0
  :precondition (and (at,rover1,waypoint0) (at_soil_sample,waypoint0) (empty,rover1store))
  :effect (and (full,rover1store) (have_soil_analysis,rover1,waypoint0) (not (at_soil_sample,waypoint0)) (not (empty,rover1store))))
 (:action sample_soil,rover1,rover1store,waypoint1
  :precondition (and (at,rover1,waypoint1) (at_soil_sample,waypoint1) (empty,rover1store))
  :effect (and (full,rover1store) (have_soil_analysis,rover1,waypoint1) (not (at_soil_sample,waypoint1)) (not (empty,rover1store))))
 (:action sample_soil,rover1,rover1store,waypoint3
  :precondition (and (at,rover1,waypoint3) (at_soil_sample,waypoint3) (empty,rover1store))
  :effect (and (full,rover1store) (have_soil_analysis,rover1,waypoint3) (ok_e13) (not (at_soil_sample,waypoint3)) (not (empty,rover1store))))
 (:action sample_soil,rover1,rover1store,waypoint6
  :precondition (and (at,rover1,waypoint6) (at_soil_sample,waypoint6) (empty,rover1store))
  :effect (and (full,rover1store) (have_soil_analysis,rover1,waypoint6) (ok_e12) (not (at_soil_sample,waypoint6)) (not (empty,rover1store))))
 (:action sample_soil,rover2,rover2store,waypoint0
  :precondition (and (at,rover2,waypoint0) (at_soil_sample,waypoint0) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint0) (safe_sb223) (not (at_soil_sample,waypoint0)) (not (empty,rover2store))))
 (:action sample_soil,rover2,rover2store,waypoint1
  :precondition (and (at,rover2,waypoint1) (at_soil_sample,waypoint1) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint1) (safe_sb223) (not (at_soil_sample,waypoint1)) (not (empty,rover2store))))
 (:action sample_soil,rover2,rover2store,waypoint3
  :precondition (and (at,rover2,waypoint3) (at_soil_sample,waypoint3) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint3) (safe_sb223) (not (at_soil_sample,waypoint3)) (not (empty,rover2store))))
 (:action sample_soil,rover2,rover2store,waypoint6
  :precondition (and (at,rover2,waypoint6) (at_soil_sample,waypoint6) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint6) (safe_sb223) (not (at_soil_sample,waypoint6)) (not (empty,rover2store))))
 (:action sample_soil,rover3,rover3store,waypoint0
  :precondition (and (at,rover3,waypoint0) (at_soil_sample,waypoint0) (empty,rover3store))
  :effect (and (full,rover3store) (have_soil_analysis,rover3,waypoint0) (not (at_soil_sample,waypoint0)) (not (empty,rover3store))))
 (:action sample_soil,rover3,rover3store,waypoint1
  :precondition (and (at,rover3,waypoint1) (at_soil_sample,waypoint1) (empty,rover3store))
  :effect (and (full,rover3store) (have_soil_analysis,rover3,waypoint1) (not (at_soil_sample,waypoint1)) (not (empty,rover3store))))
 (:action sample_soil,rover3,rover3store,waypoint3
  :precondition (and (at,rover3,waypoint3) (at_soil_sample,waypoint3) (empty,rover3store))
  :effect (and (full,rover3store) (have_soil_analysis,rover3,waypoint3) (not (at_soil_sample,waypoint3)) (not (empty,rover3store))))
 (:action sample_soil,rover3,rover3store,waypoint6
  :precondition (and (at,rover3,waypoint6) (at_soil_sample,waypoint6) (empty,rover3store))
  :effect (and (full,rover3store) (have_soil_analysis,rover3,waypoint6) (not (at_soil_sample,waypoint6)) (not (empty,rover3store))))
 (:action sample_rock,rover1,rover1store,waypoint0
  :precondition (and (at,rover1,waypoint0) (empty,rover1store) (at_rock_sample,waypoint0) (safe_sb223))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint0) (not (empty,rover1store)) (not (at_rock_sample,waypoint0))))
 (:action sample_rock,rover1,rover1store,waypoint1
  :precondition (and (at,rover1,waypoint1) (empty,rover1store) (at_rock_sample,waypoint1))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint1) (not (empty,rover1store)) (not (at_rock_sample,waypoint1))))
 (:action sample_rock,rover1,rover1store,waypoint2
  :precondition (and (at,rover1,waypoint2) (empty,rover1store) (at_rock_sample,waypoint2))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint2) (not (empty,rover1store)) (not (at_rock_sample,waypoint2))))
 (:action sample_rock,rover1,rover1store,waypoint4
  :precondition (and (at,rover1,waypoint4) (empty,rover1store) (at_rock_sample,waypoint4))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint4) (not (empty,rover1store)) (not (at_rock_sample,waypoint4))))
 (:action sample_rock,rover1,rover1store,waypoint6
  :precondition (and (at,rover1,waypoint6) (empty,rover1store) (at_rock_sample,waypoint6))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint6) (not (empty,rover1store)) (not (at_rock_sample,waypoint6))))
 (:action sample_rock,rover1,rover1store,waypoint7
  :precondition (and (at,rover1,waypoint7) (empty,rover1store) (at_rock_sample,waypoint7))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint7) (not (empty,rover1store)) (not (at_rock_sample,waypoint7))))
 (:action drop,rover0,rover0store
  :precondition (full,rover0store)
  :effect (and (empty,rover0store) (not (full,rover0store))))
 (:action drop,rover1,rover1store
  :precondition (full,rover1store)
  :effect (and (empty,rover1store) (not (full,rover1store))))
 (:action drop,rover2,rover2store
  :precondition (full,rover2store)
  :effect (and (empty,rover2store) (not (full,rover2store))))
 (:action drop,rover3,rover3store
  :precondition (full,rover3store)
  :effect (and (empty,rover3store) (not (full,rover3store))))
 (:action calibrate,rover0,camera3,objective1,waypoint0
  :precondition (at,rover0,waypoint0)
  :effect (calibrated,camera3,rover0))
 (:action calibrate,rover0,camera3,objective1,waypoint1
  :precondition (at,rover0,waypoint1)
  :effect (calibrated,camera3,rover0))
 (:action calibrate,rover0,camera3,objective1,waypoint2
  :precondition (at,rover0,waypoint2)
  :effect (calibrated,camera3,rover0))
 (:action calibrate,rover1,camera0,objective1,waypoint0
  :precondition (at,rover1,waypoint0)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective1,waypoint1
  :precondition (at,rover1,waypoint1)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective1,waypoint2
  :precondition (at,rover1,waypoint2)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover2,camera1,objective0,waypoint0
  :precondition (at,rover2,waypoint0)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover2,camera1,objective0,waypoint1
  :precondition (at,rover2,waypoint1)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover2,camera1,objective0,waypoint2
  :precondition (at,rover2,waypoint2)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover2,camera1,objective0,waypoint3
  :precondition (at,rover2,waypoint3)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover2,camera1,objective0,waypoint4
  :precondition (at,rover2,waypoint4)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover2,camera1,objective0,waypoint5
  :precondition (at,rover2,waypoint5)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover3,camera2,objective0,waypoint0
  :precondition (at,rover3,waypoint0)
  :effect (calibrated,camera2,rover3))
 (:action calibrate,rover3,camera2,objective0,waypoint1
  :precondition (at,rover3,waypoint1)
  :effect (calibrated,camera2,rover3))
 (:action calibrate,rover3,camera2,objective0,waypoint2
  :precondition (at,rover3,waypoint2)
  :effect (calibrated,camera2,rover3))
 (:action calibrate,rover3,camera2,objective0,waypoint3
  :precondition (at,rover3,waypoint3)
  :effect (calibrated,camera2,rover3))
 (:action calibrate,rover3,camera2,objective0,waypoint4
  :precondition (at,rover3,waypoint4)
  :effect (calibrated,camera2,rover3))
 (:action calibrate,rover3,camera2,objective0,waypoint5
  :precondition (at,rover3,waypoint5)
  :effect (calibrated,camera2,rover3))
 (:action take_image,rover0,waypoint0,objective0,camera3,colour
  :precondition (and (at,rover0,waypoint0) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover0,waypoint0,objective1,camera3,colour
  :precondition (and (at,rover0,waypoint0) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover0,waypoint0,objective2,camera3,colour
  :precondition (and (at,rover0,waypoint0) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover0,waypoint1,objective0,camera3,colour
  :precondition (and (at,rover0,waypoint1) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover0,waypoint1,objective1,camera3,colour
  :precondition (and (at,rover0,waypoint1) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover0,waypoint1,objective2,camera3,colour
  :precondition (and (at,rover0,waypoint1) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover0,waypoint2,objective0,camera3,colour
  :precondition (and (at,rover0,waypoint2) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover0,waypoint2,objective1,camera3,colour
  :precondition (and (at,rover0,waypoint2) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover0,waypoint2,objective2,camera3,colour
  :precondition (and (at,rover0,waypoint2) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover0,waypoint3,objective0,camera3,colour
  :precondition (and (at,rover0,waypoint3) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover0,waypoint3,objective2,camera3,colour
  :precondition (and (at,rover0,waypoint3) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover0,waypoint4,objective0,camera3,colour
  :precondition (and (at,rover0,waypoint4) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover0,waypoint4,objective2,camera3,colour
  :precondition (and (at,rover0,waypoint4) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover0,waypoint5,objective0,camera3,colour
  :precondition (and (at,rover0,waypoint5) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover0,waypoint5,objective2,camera3,colour
  :precondition (and (at,rover0,waypoint5) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover0,waypoint6,objective2,camera3,colour
  :precondition (and (at,rover0,waypoint6) (calibrated,camera3,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera3,rover0))))
 (:action take_image,rover1,waypoint0,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint0,objective0,camera0,low_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint0,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint0,objective1,camera0,low_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint0,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint0,objective2,camera0,low_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint1,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint1,objective0,camera0,low_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint1,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint1,objective1,camera0,low_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint1,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint1,objective2,camera0,low_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint2,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint2,objective0,camera0,low_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint2,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint2,objective1,camera0,low_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint2,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint2,objective2,camera0,low_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint3,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint3) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint3,objective0,camera0,low_res
  :precondition (and (at,rover1,waypoint3) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint3,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint3) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint3,objective2,camera0,low_res
  :precondition (and (at,rover1,waypoint3) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint4,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint4) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint4,objective0,camera0,low_res
  :precondition (and (at,rover1,waypoint4) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint4,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint4) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint4,objective2,camera0,low_res
  :precondition (and (at,rover1,waypoint4) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint5,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint5) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint5,objective0,camera0,low_res
  :precondition (and (at,rover1,waypoint5) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint5,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint5) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint5,objective2,camera0,low_res
  :precondition (and (at,rover1,waypoint5) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint6,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint6) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint6,objective2,camera0,low_res
  :precondition (and (at,rover1,waypoint6) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,low_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover2,waypoint0,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective0,camera1,high_res
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective1,camera1,colour
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective1,camera1,high_res
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective2,camera1,high_res
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective0,camera1,high_res
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective1,camera1,colour
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective1,camera1,high_res
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective2,camera1,high_res
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective0,camera1,high_res
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective1,camera1,colour
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective1,camera1,high_res
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective2,camera1,high_res
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective0,camera1,high_res
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective2,camera1,high_res
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective0,camera1,high_res
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective2,camera1,high_res
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective0,camera1,high_res
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective2,camera1,high_res
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint6,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint6) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint6,objective2,camera1,high_res
  :precondition (and (at,rover2,waypoint6) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,high_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover3,waypoint0,objective0,camera2,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera2,rover3))))
 (:action take_image,rover3,waypoint0,objective1,camera2,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera2,rover3))))
 (:action take_image,rover3,waypoint0,objective2,camera2,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera2,rover3))))
 (:action take_image,rover3,waypoint1,objective0,camera2,high_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera2,rover3))))
 (:action take_image,rover3,waypoint1,objective1,camera2,high_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera2,rover3))))
 (:action take_image,rover3,waypoint1,objective2,camera2,high_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera2,rover3))))
 (:action take_image,rover3,waypoint2,objective0,camera2,high_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera2,rover3))))
 (:action take_image,rover3,waypoint2,objective1,camera2,high_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera2,rover3))))
 (:action take_image,rover3,waypoint2,objective2,camera2,high_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera2,rover3))))
 (:action take_image,rover3,waypoint3,objective0,camera2,high_res
  :precondition (and (at,rover3,waypoint3) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera2,rover3))))
 (:action take_image,rover3,waypoint3,objective2,camera2,high_res
  :precondition (and (at,rover3,waypoint3) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera2,rover3))))
 (:action take_image,rover3,waypoint4,objective0,camera2,high_res
  :precondition (and (at,rover3,waypoint4) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera2,rover3))))
 (:action take_image,rover3,waypoint4,objective2,camera2,high_res
  :precondition (and (at,rover3,waypoint4) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera2,rover3))))
 (:action take_image,rover3,waypoint5,objective0,camera2,high_res
  :precondition (and (at,rover3,waypoint5) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera2,rover3))))
 (:action take_image,rover3,waypoint5,objective2,camera2,high_res
  :precondition (and (at,rover3,waypoint5) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera2,rover3))))
 (:action take_image,rover3,waypoint6,objective2,camera2,high_res
  :precondition (and (at,rover3,waypoint6) (calibrated,camera2,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera2,rover3))))
 (:action communicate_soil_data,rover0,general,waypoint0,waypoint0,waypoint1
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover0,general,waypoint0,waypoint2,waypoint1
  :precondition (and (at,rover0,waypoint2) (have_soil_analysis,rover0,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover0,general,waypoint0,waypoint3,waypoint1
  :precondition (and (at,rover0,waypoint3) (have_soil_analysis,rover0,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover0,general,waypoint0,waypoint4,waypoint1
  :precondition (and (at,rover0,waypoint4) (have_soil_analysis,rover0,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover0,general,waypoint0,waypoint5,waypoint1
  :precondition (and (at,rover0,waypoint5) (have_soil_analysis,rover0,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover0,general,waypoint0,waypoint7,waypoint1
  :precondition (and (at,rover0,waypoint7) (have_soil_analysis,rover0,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover0,general,waypoint1,waypoint0,waypoint1
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover0,general,waypoint1,waypoint2,waypoint1
  :precondition (and (at,rover0,waypoint2) (have_soil_analysis,rover0,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover0,general,waypoint1,waypoint3,waypoint1
  :precondition (and (at,rover0,waypoint3) (have_soil_analysis,rover0,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover0,general,waypoint1,waypoint4,waypoint1
  :precondition (and (at,rover0,waypoint4) (have_soil_analysis,rover0,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover0,general,waypoint1,waypoint5,waypoint1
  :precondition (and (at,rover0,waypoint5) (have_soil_analysis,rover0,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover0,general,waypoint1,waypoint7,waypoint1
  :precondition (and (at,rover0,waypoint7) (have_soil_analysis,rover0,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover0,general,waypoint3,waypoint0,waypoint1
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover0,general,waypoint3,waypoint2,waypoint1
  :precondition (and (at,rover0,waypoint2) (have_soil_analysis,rover0,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover0,general,waypoint3,waypoint3,waypoint1
  :precondition (and (at,rover0,waypoint3) (have_soil_analysis,rover0,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover0,general,waypoint3,waypoint4,waypoint1
  :precondition (and (at,rover0,waypoint4) (have_soil_analysis,rover0,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover0,general,waypoint3,waypoint5,waypoint1
  :precondition (and (at,rover0,waypoint5) (have_soil_analysis,rover0,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover0,general,waypoint3,waypoint7,waypoint1
  :precondition (and (at,rover0,waypoint7) (have_soil_analysis,rover0,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover0,general,waypoint6,waypoint0,waypoint1
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover0,general,waypoint6,waypoint2,waypoint1
  :precondition (and (at,rover0,waypoint2) (have_soil_analysis,rover0,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover0,general,waypoint6,waypoint3,waypoint1
  :precondition (and (at,rover0,waypoint3) (have_soil_analysis,rover0,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover0,general,waypoint6,waypoint4,waypoint1
  :precondition (and (at,rover0,waypoint4) (have_soil_analysis,rover0,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover0,general,waypoint6,waypoint5,waypoint1
  :precondition (and (at,rover0,waypoint5) (have_soil_analysis,rover0,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover0,general,waypoint6,waypoint7,waypoint1
  :precondition (and (at,rover0,waypoint7) (have_soil_analysis,rover0,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover1,general,waypoint0,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_soil_analysis,rover1,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover1,general,waypoint0,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_soil_analysis,rover1,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover1,general,waypoint0,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_soil_analysis,rover1,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover1,general,waypoint0,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_soil_analysis,rover1,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover1,general,waypoint0,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_soil_analysis,rover1,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover1,general,waypoint0,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_soil_analysis,rover1,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover1,general,waypoint1,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_soil_analysis,rover1,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover1,general,waypoint1,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_soil_analysis,rover1,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover1,general,waypoint1,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_soil_analysis,rover1,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover1,general,waypoint1,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_soil_analysis,rover1,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover1,general,waypoint1,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_soil_analysis,rover1,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover1,general,waypoint1,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_soil_analysis,rover1,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover1,general,waypoint3,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_soil_analysis,rover1,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover1,general,waypoint3,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_soil_analysis,rover1,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover1,general,waypoint3,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_soil_analysis,rover1,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover1,general,waypoint3,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_soil_analysis,rover1,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover1,general,waypoint3,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_soil_analysis,rover1,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover1,general,waypoint3,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_soil_analysis,rover1,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover1,general,waypoint6,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_soil_analysis,rover1,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover1,general,waypoint6,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_soil_analysis,rover1,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover1,general,waypoint6,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_soil_analysis,rover1,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover1,general,waypoint6,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_soil_analysis,rover1,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover1,general,waypoint6,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_soil_analysis,rover1,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover1,general,waypoint6,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_soil_analysis,rover1,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover2,general,waypoint0,waypoint0,waypoint1
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover2,general,waypoint0,waypoint2,waypoint1
  :precondition (and (at,rover2,waypoint2) (have_soil_analysis,rover2,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover2,general,waypoint0,waypoint3,waypoint1
  :precondition (and (at,rover2,waypoint3) (have_soil_analysis,rover2,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover2,general,waypoint0,waypoint4,waypoint1
  :precondition (and (at,rover2,waypoint4) (have_soil_analysis,rover2,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover2,general,waypoint0,waypoint5,waypoint1
  :precondition (and (at,rover2,waypoint5) (have_soil_analysis,rover2,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover2,general,waypoint0,waypoint7,waypoint1
  :precondition (and (at,rover2,waypoint7) (have_soil_analysis,rover2,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover2,general,waypoint1,waypoint0,waypoint1
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover2,general,waypoint1,waypoint2,waypoint1
  :precondition (and (at,rover2,waypoint2) (have_soil_analysis,rover2,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover2,general,waypoint1,waypoint3,waypoint1
  :precondition (and (at,rover2,waypoint3) (have_soil_analysis,rover2,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover2,general,waypoint1,waypoint4,waypoint1
  :precondition (and (at,rover2,waypoint4) (have_soil_analysis,rover2,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover2,general,waypoint1,waypoint5,waypoint1
  :precondition (and (at,rover2,waypoint5) (have_soil_analysis,rover2,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover2,general,waypoint1,waypoint7,waypoint1
  :precondition (and (at,rover2,waypoint7) (have_soil_analysis,rover2,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover2,general,waypoint3,waypoint0,waypoint1
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover2,general,waypoint3,waypoint2,waypoint1
  :precondition (and (at,rover2,waypoint2) (have_soil_analysis,rover2,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover2,general,waypoint3,waypoint3,waypoint1
  :precondition (and (at,rover2,waypoint3) (have_soil_analysis,rover2,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover2,general,waypoint3,waypoint4,waypoint1
  :precondition (and (at,rover2,waypoint4) (have_soil_analysis,rover2,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover2,general,waypoint3,waypoint5,waypoint1
  :precondition (and (at,rover2,waypoint5) (have_soil_analysis,rover2,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover2,general,waypoint3,waypoint7,waypoint1
  :precondition (and (at,rover2,waypoint7) (have_soil_analysis,rover2,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover2,general,waypoint6,waypoint0,waypoint1
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover2,general,waypoint6,waypoint2,waypoint1
  :precondition (and (at,rover2,waypoint2) (have_soil_analysis,rover2,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover2,general,waypoint6,waypoint3,waypoint1
  :precondition (and (at,rover2,waypoint3) (have_soil_analysis,rover2,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover2,general,waypoint6,waypoint4,waypoint1
  :precondition (and (at,rover2,waypoint4) (have_soil_analysis,rover2,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover2,general,waypoint6,waypoint5,waypoint1
  :precondition (and (at,rover2,waypoint5) (have_soil_analysis,rover2,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover2,general,waypoint6,waypoint7,waypoint1
  :precondition (and (at,rover2,waypoint7) (have_soil_analysis,rover2,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover3,general,waypoint0,waypoint0,waypoint1
  :precondition (and (at,rover3,waypoint0) (have_soil_analysis,rover3,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover3,general,waypoint0,waypoint2,waypoint1
  :precondition (and (at,rover3,waypoint2) (have_soil_analysis,rover3,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover3,general,waypoint0,waypoint3,waypoint1
  :precondition (and (at,rover3,waypoint3) (have_soil_analysis,rover3,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover3,general,waypoint0,waypoint4,waypoint1
  :precondition (and (at,rover3,waypoint4) (have_soil_analysis,rover3,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover3,general,waypoint0,waypoint5,waypoint1
  :precondition (and (at,rover3,waypoint5) (have_soil_analysis,rover3,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover3,general,waypoint0,waypoint7,waypoint1
  :precondition (and (at,rover3,waypoint7) (have_soil_analysis,rover3,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover3,general,waypoint1,waypoint0,waypoint1
  :precondition (and (at,rover3,waypoint0) (have_soil_analysis,rover3,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover3,general,waypoint1,waypoint2,waypoint1
  :precondition (and (at,rover3,waypoint2) (have_soil_analysis,rover3,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover3,general,waypoint1,waypoint3,waypoint1
  :precondition (and (at,rover3,waypoint3) (have_soil_analysis,rover3,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover3,general,waypoint1,waypoint4,waypoint1
  :precondition (and (at,rover3,waypoint4) (have_soil_analysis,rover3,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover3,general,waypoint1,waypoint5,waypoint1
  :precondition (and (at,rover3,waypoint5) (have_soil_analysis,rover3,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover3,general,waypoint1,waypoint7,waypoint1
  :precondition (and (at,rover3,waypoint7) (have_soil_analysis,rover3,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover3,general,waypoint3,waypoint0,waypoint1
  :precondition (and (at,rover3,waypoint0) (have_soil_analysis,rover3,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover3,general,waypoint3,waypoint2,waypoint1
  :precondition (and (at,rover3,waypoint2) (have_soil_analysis,rover3,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover3,general,waypoint3,waypoint3,waypoint1
  :precondition (and (at,rover3,waypoint3) (have_soil_analysis,rover3,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover3,general,waypoint3,waypoint4,waypoint1
  :precondition (and (at,rover3,waypoint4) (have_soil_analysis,rover3,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover3,general,waypoint3,waypoint5,waypoint1
  :precondition (and (at,rover3,waypoint5) (have_soil_analysis,rover3,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover3,general,waypoint3,waypoint7,waypoint1
  :precondition (and (at,rover3,waypoint7) (have_soil_analysis,rover3,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover3,general,waypoint6,waypoint0,waypoint1
  :precondition (and (at,rover3,waypoint0) (have_soil_analysis,rover3,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover3,general,waypoint6,waypoint2,waypoint1
  :precondition (and (at,rover3,waypoint2) (have_soil_analysis,rover3,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover3,general,waypoint6,waypoint3,waypoint1
  :precondition (and (at,rover3,waypoint3) (have_soil_analysis,rover3,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover3,general,waypoint6,waypoint4,waypoint1
  :precondition (and (at,rover3,waypoint4) (have_soil_analysis,rover3,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover3,general,waypoint6,waypoint5,waypoint1
  :precondition (and (at,rover3,waypoint5) (have_soil_analysis,rover3,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_soil_data,rover3,general,waypoint6,waypoint7,waypoint1
  :precondition (and (at,rover3,waypoint7) (have_soil_analysis,rover3,waypoint6))
  :effect (communicated_soil_data,waypoint6))
 (:action communicate_rock_data,rover1,general,waypoint0,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint0))
  :effect (communicated_rock_data,waypoint0))
 (:action communicate_rock_data,rover1,general,waypoint0,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_rock_analysis,rover1,waypoint0))
  :effect (communicated_rock_data,waypoint0))
 (:action communicate_rock_data,rover1,general,waypoint0,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_rock_analysis,rover1,waypoint0))
  :effect (communicated_rock_data,waypoint0))
 (:action communicate_rock_data,rover1,general,waypoint0,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_rock_analysis,rover1,waypoint0))
  :effect (communicated_rock_data,waypoint0))
 (:action communicate_rock_data,rover1,general,waypoint0,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_rock_analysis,rover1,waypoint0))
  :effect (communicated_rock_data,waypoint0))
 (:action communicate_rock_data,rover1,general,waypoint0,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_rock_analysis,rover1,waypoint0))
  :effect (communicated_rock_data,waypoint0))
 (:action communicate_rock_data,rover1,general,waypoint1,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover1,general,waypoint1,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_rock_analysis,rover1,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover1,general,waypoint1,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_rock_analysis,rover1,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover1,general,waypoint1,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_rock_analysis,rover1,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover1,general,waypoint1,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_rock_analysis,rover1,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover1,general,waypoint1,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_rock_analysis,rover1,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover1,general,waypoint2,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover1,general,waypoint2,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_rock_analysis,rover1,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover1,general,waypoint2,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_rock_analysis,rover1,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover1,general,waypoint2,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_rock_analysis,rover1,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover1,general,waypoint2,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_rock_analysis,rover1,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover1,general,waypoint2,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_rock_analysis,rover1,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover1,general,waypoint4,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover1,general,waypoint4,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_rock_analysis,rover1,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover1,general,waypoint4,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_rock_analysis,rover1,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover1,general,waypoint4,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_rock_analysis,rover1,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover1,general,waypoint4,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_rock_analysis,rover1,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover1,general,waypoint4,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_rock_analysis,rover1,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover1,general,waypoint6,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover1,general,waypoint6,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_rock_analysis,rover1,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover1,general,waypoint6,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_rock_analysis,rover1,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover1,general,waypoint6,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_rock_analysis,rover1,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover1,general,waypoint6,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_rock_analysis,rover1,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover1,general,waypoint6,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_rock_analysis,rover1,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover1,general,waypoint7,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover1,general,waypoint7,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_rock_analysis,rover1,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover1,general,waypoint7,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_rock_analysis,rover1,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover1,general,waypoint7,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_rock_analysis,rover1,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover1,general,waypoint7,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_rock_analysis,rover1,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover1,general,waypoint7,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_rock_analysis,rover1,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_image_data,rover0,general,objective0,colour,waypoint0,waypoint1
  :precondition (and (at,rover0,waypoint0) (have_image,rover0,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover0,general,objective0,colour,waypoint2,waypoint1
  :precondition (and (at,rover0,waypoint2) (have_image,rover0,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover0,general,objective0,colour,waypoint3,waypoint1
  :precondition (and (at,rover0,waypoint3) (have_image,rover0,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover0,general,objective0,colour,waypoint4,waypoint1
  :precondition (and (at,rover0,waypoint4) (have_image,rover0,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover0,general,objective0,colour,waypoint5,waypoint1
  :precondition (and (at,rover0,waypoint5) (have_image,rover0,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover0,general,objective0,colour,waypoint7,waypoint1
  :precondition (and (at,rover0,waypoint7) (have_image,rover0,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover0,general,objective1,colour,waypoint0,waypoint1
  :precondition (and (at,rover0,waypoint0) (have_image,rover0,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover0,general,objective1,colour,waypoint2,waypoint1
  :precondition (and (at,rover0,waypoint2) (have_image,rover0,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover0,general,objective1,colour,waypoint3,waypoint1
  :precondition (and (at,rover0,waypoint3) (have_image,rover0,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover0,general,objective1,colour,waypoint4,waypoint1
  :precondition (and (at,rover0,waypoint4) (have_image,rover0,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover0,general,objective1,colour,waypoint5,waypoint1
  :precondition (and (at,rover0,waypoint5) (have_image,rover0,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover0,general,objective1,colour,waypoint7,waypoint1
  :precondition (and (at,rover0,waypoint7) (have_image,rover0,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover0,general,objective2,colour,waypoint0,waypoint1
  :precondition (and (at,rover0,waypoint0) (have_image,rover0,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover0,general,objective2,colour,waypoint2,waypoint1
  :precondition (and (at,rover0,waypoint2) (have_image,rover0,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover0,general,objective2,colour,waypoint3,waypoint1
  :precondition (and (at,rover0,waypoint3) (have_image,rover0,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover0,general,objective2,colour,waypoint4,waypoint1
  :precondition (and (at,rover0,waypoint4) (have_image,rover0,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover0,general,objective2,colour,waypoint5,waypoint1
  :precondition (and (at,rover0,waypoint5) (have_image,rover0,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover0,general,objective2,colour,waypoint7,waypoint1
  :precondition (and (at,rover0,waypoint7) (have_image,rover0,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover1,general,objective0,high_res,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_image,rover1,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover1,general,objective0,high_res,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_image,rover1,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover1,general,objective0,high_res,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_image,rover1,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover1,general,objective0,high_res,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_image,rover1,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover1,general,objective0,high_res,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_image,rover1,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover1,general,objective0,high_res,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_image,rover1,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover1,general,objective0,low_res,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_image,rover1,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover1,general,objective0,low_res,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_image,rover1,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover1,general,objective0,low_res,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_image,rover1,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover1,general,objective0,low_res,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_image,rover1,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover1,general,objective0,low_res,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_image,rover1,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover1,general,objective0,low_res,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_image,rover1,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover1,general,objective1,high_res,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_image,rover1,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover1,general,objective1,high_res,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_image,rover1,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover1,general,objective1,high_res,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_image,rover1,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover1,general,objective1,high_res,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_image,rover1,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover1,general,objective1,high_res,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_image,rover1,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover1,general,objective1,high_res,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_image,rover1,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover1,general,objective1,low_res,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_image,rover1,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover1,general,objective1,low_res,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_image,rover1,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover1,general,objective1,low_res,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_image,rover1,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover1,general,objective1,low_res,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_image,rover1,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover1,general,objective1,low_res,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_image,rover1,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover1,general,objective1,low_res,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_image,rover1,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover1,general,objective2,high_res,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_image,rover1,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover1,general,objective2,high_res,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_image,rover1,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover1,general,objective2,high_res,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_image,rover1,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover1,general,objective2,high_res,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_image,rover1,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover1,general,objective2,high_res,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_image,rover1,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover1,general,objective2,high_res,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_image,rover1,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover1,general,objective2,low_res,waypoint0,waypoint1
  :precondition (and (at,rover1,waypoint0) (have_image,rover1,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover1,general,objective2,low_res,waypoint2,waypoint1
  :precondition (and (at,rover1,waypoint2) (have_image,rover1,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover1,general,objective2,low_res,waypoint3,waypoint1
  :precondition (and (at,rover1,waypoint3) (have_image,rover1,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover1,general,objective2,low_res,waypoint4,waypoint1
  :precondition (and (at,rover1,waypoint4) (have_image,rover1,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover1,general,objective2,low_res,waypoint5,waypoint1
  :precondition (and (at,rover1,waypoint5) (have_image,rover1,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover1,general,objective2,low_res,waypoint7,waypoint1
  :precondition (and (at,rover1,waypoint7) (have_image,rover1,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover2,general,objective0,colour,waypoint0,waypoint1
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover2,general,objective0,colour,waypoint2,waypoint1
  :precondition (and (at,rover2,waypoint2) (have_image,rover2,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover2,general,objective0,colour,waypoint3,waypoint1
  :precondition (and (at,rover2,waypoint3) (have_image,rover2,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover2,general,objective0,colour,waypoint4,waypoint1
  :precondition (and (at,rover2,waypoint4) (have_image,rover2,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover2,general,objective0,colour,waypoint5,waypoint1
  :precondition (and (at,rover2,waypoint5) (have_image,rover2,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover2,general,objective0,colour,waypoint7,waypoint1
  :precondition (and (at,rover2,waypoint7) (have_image,rover2,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover2,general,objective0,high_res,waypoint0,waypoint1
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover2,general,objective0,high_res,waypoint2,waypoint1
  :precondition (and (at,rover2,waypoint2) (have_image,rover2,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover2,general,objective0,high_res,waypoint3,waypoint1
  :precondition (and (at,rover2,waypoint3) (have_image,rover2,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover2,general,objective0,high_res,waypoint4,waypoint1
  :precondition (and (at,rover2,waypoint4) (have_image,rover2,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover2,general,objective0,high_res,waypoint5,waypoint1
  :precondition (and (at,rover2,waypoint5) (have_image,rover2,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover2,general,objective0,high_res,waypoint7,waypoint1
  :precondition (and (at,rover2,waypoint7) (have_image,rover2,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover2,general,objective1,colour,waypoint0,waypoint1
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover2,general,objective1,colour,waypoint2,waypoint1
  :precondition (and (at,rover2,waypoint2) (have_image,rover2,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover2,general,objective1,colour,waypoint3,waypoint1
  :precondition (and (at,rover2,waypoint3) (have_image,rover2,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover2,general,objective1,colour,waypoint4,waypoint1
  :precondition (and (at,rover2,waypoint4) (have_image,rover2,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover2,general,objective1,colour,waypoint5,waypoint1
  :precondition (and (at,rover2,waypoint5) (have_image,rover2,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover2,general,objective1,colour,waypoint7,waypoint1
  :precondition (and (at,rover2,waypoint7) (have_image,rover2,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover2,general,objective1,high_res,waypoint0,waypoint1
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover2,general,objective1,high_res,waypoint2,waypoint1
  :precondition (and (at,rover2,waypoint2) (have_image,rover2,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover2,general,objective1,high_res,waypoint3,waypoint1
  :precondition (and (at,rover2,waypoint3) (have_image,rover2,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover2,general,objective1,high_res,waypoint4,waypoint1
  :precondition (and (at,rover2,waypoint4) (have_image,rover2,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover2,general,objective1,high_res,waypoint5,waypoint1
  :precondition (and (at,rover2,waypoint5) (have_image,rover2,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover2,general,objective1,high_res,waypoint7,waypoint1
  :precondition (and (at,rover2,waypoint7) (have_image,rover2,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover2,general,objective2,colour,waypoint0,waypoint1
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover2,general,objective2,colour,waypoint2,waypoint1
  :precondition (and (at,rover2,waypoint2) (have_image,rover2,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover2,general,objective2,colour,waypoint3,waypoint1
  :precondition (and (at,rover2,waypoint3) (have_image,rover2,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover2,general,objective2,colour,waypoint4,waypoint1
  :precondition (and (at,rover2,waypoint4) (have_image,rover2,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover2,general,objective2,colour,waypoint5,waypoint1
  :precondition (and (at,rover2,waypoint5) (have_image,rover2,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover2,general,objective2,colour,waypoint7,waypoint1
  :precondition (and (at,rover2,waypoint7) (have_image,rover2,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover2,general,objective2,high_res,waypoint0,waypoint1
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover2,general,objective2,high_res,waypoint2,waypoint1
  :precondition (and (at,rover2,waypoint2) (have_image,rover2,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover2,general,objective2,high_res,waypoint3,waypoint1
  :precondition (and (at,rover2,waypoint3) (have_image,rover2,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover2,general,objective2,high_res,waypoint4,waypoint1
  :precondition (and (at,rover2,waypoint4) (have_image,rover2,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover2,general,objective2,high_res,waypoint5,waypoint1
  :precondition (and (at,rover2,waypoint5) (have_image,rover2,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover2,general,objective2,high_res,waypoint7,waypoint1
  :precondition (and (at,rover2,waypoint7) (have_image,rover2,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover3,general,objective0,high_res,waypoint0,waypoint1
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover3,general,objective0,high_res,waypoint2,waypoint1
  :precondition (and (at,rover3,waypoint2) (have_image,rover3,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover3,general,objective0,high_res,waypoint3,waypoint1
  :precondition (and (at,rover3,waypoint3) (have_image,rover3,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover3,general,objective0,high_res,waypoint4,waypoint1
  :precondition (and (at,rover3,waypoint4) (have_image,rover3,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover3,general,objective0,high_res,waypoint5,waypoint1
  :precondition (and (at,rover3,waypoint5) (have_image,rover3,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover3,general,objective0,high_res,waypoint7,waypoint1
  :precondition (and (at,rover3,waypoint7) (have_image,rover3,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover3,general,objective1,high_res,waypoint0,waypoint1
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover3,general,objective1,high_res,waypoint2,waypoint1
  :precondition (and (at,rover3,waypoint2) (have_image,rover3,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover3,general,objective1,high_res,waypoint3,waypoint1
  :precondition (and (at,rover3,waypoint3) (have_image,rover3,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover3,general,objective1,high_res,waypoint4,waypoint1
  :precondition (and (at,rover3,waypoint4) (have_image,rover3,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover3,general,objective1,high_res,waypoint5,waypoint1
  :precondition (and (at,rover3,waypoint5) (have_image,rover3,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover3,general,objective1,high_res,waypoint7,waypoint1
  :precondition (and (at,rover3,waypoint7) (have_image,rover3,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover3,general,objective2,high_res,waypoint0,waypoint1
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover3,general,objective2,high_res,waypoint2,waypoint1
  :precondition (and (at,rover3,waypoint2) (have_image,rover3,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover3,general,objective2,high_res,waypoint3,waypoint1
  :precondition (and (at,rover3,waypoint3) (have_image,rover3,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover3,general,objective2,high_res,waypoint4,waypoint1
  :precondition (and (at,rover3,waypoint4) (have_image,rover3,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover3,general,objective2,high_res,waypoint5,waypoint1
  :precondition (and (at,rover3,waypoint5) (have_image,rover3,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover3,general,objective2,high_res,waypoint7,waypoint1
  :precondition (and (at,rover3,waypoint7) (have_image,rover3,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
;; action partitions
)