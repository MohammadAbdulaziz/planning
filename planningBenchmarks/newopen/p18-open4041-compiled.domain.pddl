;; rover::roverprob4621, problem open #4041
;; selected constraints: {o3, o11, o14}
;; value = 285, penalty = 13886
(define (domain rover)
 (:predicates (at,rover0,waypoint0) (at,rover0,waypoint2) (at,rover0,waypoint12) (at,rover0,waypoint13) (at,rover0,waypoint1) (at,rover0,waypoint6) (at,rover0,waypoint7) (at,rover0,waypoint9) (at,rover0,waypoint14) (at,rover0,waypoint18) (at,rover0,waypoint3) (at,rover0,waypoint4) (at,rover0,waypoint5) (at,rover0,waypoint10) (at,rover0,waypoint15) (at,rover0,waypoint19) (at,rover0,waypoint11) (at,rover0,waypoint8) (at,rover0,waypoint16) (at,rover1,waypoint0) (at,rover1,waypoint2) (at,rover1,waypoint9) (at,rover1,waypoint13) (at,rover1,waypoint16) (at,rover1,waypoint1) (at,rover1,waypoint12) (at,rover1,waypoint3) (at,rover1,waypoint6) (at,rover1,waypoint4) (at,rover1,waypoint11) (at,rover1,waypoint5) (at,rover1,waypoint7) (at,rover1,waypoint8) (at,rover1,waypoint10) (at,rover1,waypoint14) (at,rover1,waypoint18) (at,rover1,waypoint19) (at,rover1,waypoint15) (at,rover1,waypoint17) (at,rover2,waypoint0) (at,rover2,waypoint1) (at,rover2,waypoint9) (at,rover2,waypoint13) (at,rover2,waypoint16) (at,rover2,waypoint17) (at,rover2,waypoint18) (at,rover2,waypoint2) (at,rover2,waypoint6) (at,rover2,waypoint11) (at,rover2,waypoint12) (at,rover2,waypoint15) (at,rover2,waypoint3) (at,rover2,waypoint4) (at,rover2,waypoint5) (at,rover2,waypoint7) (at,rover2,waypoint10) (at,rover2,waypoint14) (at,rover2,waypoint19) (at,rover3,waypoint0) (at,rover3,waypoint1) (at,rover3,waypoint2) (at,rover3,waypoint9) (at,rover3,waypoint12) (at,rover3,waypoint17) (at,rover3,waypoint18) (at,rover3,waypoint6) (at,rover3,waypoint7) (at,rover3,waypoint15) (at,rover3,waypoint14) (at,rover3,waypoint3) (at,rover3,waypoint4) (at,rover3,waypoint5) (at,rover3,waypoint10) (at,rover3,waypoint8) (at,rover3,waypoint11) (at,rover3,waypoint16) (at,rover3,waypoint19) (at,rover3,waypoint13) (at,rover4,waypoint0) (at,rover4,waypoint9) (at,rover4,waypoint1) (at,rover4,waypoint15) (at,rover4,waypoint2) (at,rover4,waypoint14) (at,rover4,waypoint3) (at,rover4,waypoint6) (at,rover4,waypoint7) (at,rover4,waypoint10) (at,rover4,waypoint11) (at,rover4,waypoint13) (at,rover4,waypoint4) (at,rover4,waypoint5) (at,rover4,waypoint12) (at,rover4,waypoint8) (at,rover4,waypoint19) (at,rover4,waypoint16) (at,rover4,waypoint17) (at,rover4,waypoint18) (at,rover5,waypoint0) (at,rover5,waypoint1) (at,rover5,waypoint12) (at,rover5,waypoint13) (at,rover5,waypoint17) (at,rover5,waypoint18) (at,rover5,waypoint2) (at,rover5,waypoint6) (at,rover5,waypoint7) (at,rover5,waypoint11) (at,rover5,waypoint15) (at,rover5,waypoint3) (at,rover5,waypoint4) (at,rover5,waypoint5) (at,rover5,waypoint10) (at,rover5,waypoint8) (at,rover5,waypoint9) (at,rover5,waypoint14) (at,rover5,waypoint19) (at,rover5,waypoint16) (at_soil_sample,waypoint0) (empty,rover0store) (full,rover0store) (have_soil_analysis,rover0,waypoint0) (at_soil_sample,waypoint3) (have_soil_analysis,rover0,waypoint3) (at_soil_sample,waypoint8) (have_soil_analysis,rover0,waypoint8) (at_soil_sample,waypoint9) (have_soil_analysis,rover0,waypoint9) (at_soil_sample,waypoint10) (have_soil_analysis,rover0,waypoint10) (at_soil_sample,waypoint11) (have_soil_analysis,rover0,waypoint11) (at_soil_sample,waypoint12) (have_soil_analysis,rover0,waypoint12) (at_soil_sample,waypoint13) (have_soil_analysis,rover0,waypoint13) (at_soil_sample,waypoint14) (have_soil_analysis,rover0,waypoint14) (at_soil_sample,waypoint15) (have_soil_analysis,rover0,waypoint15) (at_soil_sample,waypoint17) (at_soil_sample,waypoint18) (have_soil_analysis,rover0,waypoint18) (at_soil_sample,waypoint19) (have_soil_analysis,rover0,waypoint19) (empty,rover2store) (full,rover2store) (have_soil_analysis,rover2,waypoint0) (have_soil_analysis,rover2,waypoint3) (have_soil_analysis,rover2,waypoint9) (have_soil_analysis,rover2,waypoint10) (have_soil_analysis,rover2,waypoint11) (have_soil_analysis,rover2,waypoint12) (have_soil_analysis,rover2,waypoint13) (have_soil_analysis,rover2,waypoint14) (have_soil_analysis,rover2,waypoint15) (have_soil_analysis,rover2,waypoint17) (have_soil_analysis,rover2,waypoint18) (have_soil_analysis,rover2,waypoint19) (empty,rover4store) (full,rover4store) (have_soil_analysis,rover4,waypoint0) (have_soil_analysis,rover4,waypoint3) (have_soil_analysis,rover4,waypoint8) (have_soil_analysis,rover4,waypoint9) (have_soil_analysis,rover4,waypoint10) (have_soil_analysis,rover4,waypoint11) (have_soil_analysis,rover4,waypoint12) (have_soil_analysis,rover4,waypoint13) (have_soil_analysis,rover4,waypoint14) (have_soil_analysis,rover4,waypoint15) (have_soil_analysis,rover4,waypoint17) (have_soil_analysis,rover4,waypoint18) (have_soil_analysis,rover4,waypoint19) (at_rock_sample,waypoint1) (have_rock_analysis,rover0,waypoint1) (at_rock_sample,waypoint2) (have_rock_analysis,rover0,waypoint2) (at_rock_sample,waypoint4) (have_rock_analysis,rover0,waypoint4) (at_rock_sample,waypoint5) (have_rock_analysis,rover0,waypoint5) (at_rock_sample,waypoint6) (have_rock_analysis,rover0,waypoint6) (at_rock_sample,waypoint7) (have_rock_analysis,rover0,waypoint7) (at_rock_sample,waypoint9) (have_rock_analysis,rover0,waypoint9) (at_rock_sample,waypoint12) (have_rock_analysis,rover0,waypoint12) (at_rock_sample,waypoint13) (have_rock_analysis,rover0,waypoint13) (at_rock_sample,waypoint17) (at_rock_sample,waypoint18) (have_rock_analysis,rover0,waypoint18) (empty,rover1store) (full,rover1store) (have_rock_analysis,rover1,waypoint1) (have_rock_analysis,rover1,waypoint2) (have_rock_analysis,rover1,waypoint4) (have_rock_analysis,rover1,waypoint5) (have_rock_analysis,rover1,waypoint6) (have_rock_analysis,rover1,waypoint7) (have_rock_analysis,rover1,waypoint9) (have_rock_analysis,rover1,waypoint12) (have_rock_analysis,rover1,waypoint13) (have_rock_analysis,rover1,waypoint17) (have_rock_analysis,rover1,waypoint18) (empty,rover3store) (full,rover3store) (have_rock_analysis,rover3,waypoint1) (have_rock_analysis,rover3,waypoint2) (have_rock_analysis,rover3,waypoint4) (have_rock_analysis,rover3,waypoint5) (have_rock_analysis,rover3,waypoint6) (have_rock_analysis,rover3,waypoint7) (have_rock_analysis,rover3,waypoint9) (have_rock_analysis,rover3,waypoint12) (have_rock_analysis,rover3,waypoint13) (have_rock_analysis,rover3,waypoint17) (have_rock_analysis,rover3,waypoint18) (empty,rover5store) (full,rover5store) (have_rock_analysis,rover5,waypoint1) (have_rock_analysis,rover5,waypoint2) (have_rock_analysis,rover5,waypoint4) (have_rock_analysis,rover5,waypoint5) (have_rock_analysis,rover5,waypoint6) (have_rock_analysis,rover5,waypoint7) (have_rock_analysis,rover5,waypoint9) (have_rock_analysis,rover5,waypoint12) (have_rock_analysis,rover5,waypoint13) (have_rock_analysis,rover5,waypoint17) (have_rock_analysis,rover5,waypoint18) (calibrated,camera5,rover0) (calibrated,camera0,rover1) (calibrated,camera1,rover2) (calibrated,camera3,rover3) (calibrated,camera4,rover3) (calibrated,camera2,rover4) (calibrated,camera6,rover5) (have_image,rover0,objective0,colour) (have_image,rover0,objective1,colour) (have_image,rover0,objective2,colour) (have_image,rover0,objective3,colour) (have_image,rover0,objective4,colour) (have_image,rover0,objective5,colour) (have_image,rover0,objective6,colour) (have_image,rover1,objective0,high_res) (have_image,rover1,objective1,high_res) (have_image,rover1,objective2,high_res) (have_image,rover1,objective3,high_res) (have_image,rover1,objective4,high_res) (have_image,rover1,objective5,high_res) (have_image,rover1,objective6,high_res) (have_image,rover2,objective0,colour) (have_image,rover2,objective0,low_res) (have_image,rover2,objective1,colour) (have_image,rover2,objective1,low_res) (have_image,rover2,objective2,colour) (have_image,rover2,objective2,low_res) (have_image,rover2,objective3,colour) (have_image,rover2,objective3,low_res) (have_image,rover2,objective4,colour) (have_image,rover2,objective4,low_res) (have_image,rover2,objective5,colour) (have_image,rover2,objective5,low_res) (have_image,rover2,objective6,colour) (have_image,rover2,objective6,low_res) (have_image,rover3,objective0,colour) (have_image,rover3,objective0,high_res) (have_image,rover3,objective0,low_res) (have_image,rover3,objective1,colour) (have_image,rover3,objective1,high_res) (have_image,rover3,objective1,low_res) (have_image,rover3,objective2,colour) (have_image,rover3,objective2,high_res) (have_image,rover3,objective2,low_res) (have_image,rover3,objective3,colour) (have_image,rover3,objective3,high_res) (have_image,rover3,objective3,low_res) (have_image,rover3,objective4,colour) (have_image,rover3,objective4,high_res) (have_image,rover3,objective4,low_res) (have_image,rover3,objective5,colour) (have_image,rover3,objective5,high_res) (have_image,rover3,objective5,low_res) (have_image,rover3,objective6,colour) (have_image,rover3,objective6,high_res) (have_image,rover3,objective6,low_res) (have_image,rover4,objective0,colour) (have_image,rover4,objective0,high_res) (have_image,rover4,objective1,colour) (have_image,rover4,objective1,high_res) (have_image,rover4,objective2,colour) (have_image,rover4,objective2,high_res) (have_image,rover4,objective3,colour) (have_image,rover4,objective3,high_res) (have_image,rover4,objective4,colour) (have_image,rover4,objective4,high_res) (have_image,rover4,objective5,colour) (have_image,rover4,objective5,high_res) (have_image,rover4,objective6,colour) (have_image,rover4,objective6,high_res) (have_image,rover5,objective0,high_res) (have_image,rover5,objective0,low_res) (have_image,rover5,objective1,high_res) (have_image,rover5,objective1,low_res) (have_image,rover5,objective2,high_res) (have_image,rover5,objective2,low_res) (have_image,rover5,objective3,high_res) (have_image,rover5,objective3,low_res) (have_image,rover5,objective4,high_res) (have_image,rover5,objective4,low_res) (have_image,rover5,objective5,high_res) (have_image,rover5,objective5,low_res) (have_image,rover5,objective6,high_res) (have_image,rover5,objective6,low_res) (communicated_soil_data,waypoint0) (communicated_soil_data,waypoint3) (communicated_soil_data,waypoint8) (communicated_soil_data,waypoint9) (communicated_soil_data,waypoint10) (communicated_soil_data,waypoint11) (communicated_soil_data,waypoint12) (communicated_soil_data,waypoint13) (communicated_soil_data,waypoint14) (communicated_soil_data,waypoint15) (communicated_soil_data,waypoint17) (communicated_soil_data,waypoint18) (communicated_soil_data,waypoint19) (communicated_rock_data,waypoint1) (communicated_rock_data,waypoint2) (communicated_rock_data,waypoint4) (communicated_rock_data,waypoint5) (communicated_rock_data,waypoint6) (communicated_rock_data,waypoint7) (communicated_rock_data,waypoint9) (communicated_rock_data,waypoint12) (communicated_rock_data,waypoint13) (communicated_rock_data,waypoint17) (communicated_rock_data,waypoint18) (communicated_image_data,objective0,colour) (communicated_image_data,objective0,high_res) (communicated_image_data,objective0,low_res) (communicated_image_data,objective1,colour) (communicated_image_data,objective1,high_res) (communicated_image_data,objective1,low_res) (communicated_image_data,objective2,colour) (communicated_image_data,objective2,high_res) (communicated_image_data,objective2,low_res) (communicated_image_data,objective3,colour) (communicated_image_data,objective3,high_res) (communicated_image_data,objective3,low_res) (communicated_image_data,objective4,colour) (communicated_image_data,objective4,high_res) (communicated_image_data,objective4,low_res) (communicated_image_data,objective5,colour) (communicated_image_data,objective5,high_res) (communicated_image_data,objective5,low_res) (communicated_image_data,objective6,colour) (communicated_image_data,objective6,high_res) (communicated_image_data,objective6,low_res) (first_o3) (first_o11) (first_o14))
 (:action navigate,rover0,waypoint0,waypoint2
  :precondition (and (at,rover0,waypoint0) (first_o11))
  :effect (and (at,rover0,waypoint2) (not (at,rover0,waypoint0))))
 (:action navigate,rover0,waypoint0,waypoint12
  :precondition (at,rover0,waypoint0)
  :effect (and (at,rover0,waypoint12) (not (at,rover0,waypoint0))))
 (:action navigate,rover0,waypoint0,waypoint13
  :precondition (at,rover0,waypoint0)
  :effect (and (at,rover0,waypoint13) (not (at,rover0,waypoint0))))
 (:action navigate,rover0,waypoint1,waypoint6
  :precondition (at,rover0,waypoint1)
  :effect (and (at,rover0,waypoint6) (not (at,rover0,waypoint1))))
 (:action navigate,rover0,waypoint2,waypoint0
  :precondition (at,rover0,waypoint2)
  :effect (and (at,rover0,waypoint0) (not (at,rover0,waypoint2)) (not (first_o11))))
 (:action navigate,rover0,waypoint2,waypoint6
  :precondition (at,rover0,waypoint2)
  :effect (and (at,rover0,waypoint6) (not (at,rover0,waypoint2)) (not (first_o11))))
 (:action navigate,rover0,waypoint2,waypoint7
  :precondition (at,rover0,waypoint2)
  :effect (and (at,rover0,waypoint7) (not (at,rover0,waypoint2)) (not (first_o11))))
 (:action navigate,rover0,waypoint2,waypoint9
  :precondition (at,rover0,waypoint2)
  :effect (and (at,rover0,waypoint9) (not (at,rover0,waypoint2)) (not (first_o11))))
 (:action navigate,rover0,waypoint2,waypoint14
  :precondition (at,rover0,waypoint2)
  :effect (and (at,rover0,waypoint14) (not (at,rover0,waypoint2)) (not (first_o11))))
 (:action navigate,rover0,waypoint2,waypoint18
  :precondition (at,rover0,waypoint2)
  :effect (and (at,rover0,waypoint18) (not (at,rover0,waypoint2)) (not (first_o11))))
 (:action navigate,rover0,waypoint3,waypoint7
  :precondition (at,rover0,waypoint3)
  :effect (and (at,rover0,waypoint7) (not (at,rover0,waypoint3))))
 (:action navigate,rover0,waypoint4,waypoint9
  :precondition (at,rover0,waypoint4)
  :effect (and (at,rover0,waypoint9) (not (at,rover0,waypoint4))))
 (:action navigate,rover0,waypoint5,waypoint9
  :precondition (at,rover0,waypoint5)
  :effect (and (at,rover0,waypoint9) (not (at,rover0,waypoint5))))
 (:action navigate,rover0,waypoint5,waypoint10
  :precondition (at,rover0,waypoint5)
  :effect (and (at,rover0,waypoint10) (not (at,rover0,waypoint5))))
 (:action navigate,rover0,waypoint6,waypoint1
  :precondition (at,rover0,waypoint6)
  :effect (and (at,rover0,waypoint1) (not (at,rover0,waypoint6))))
 (:action navigate,rover0,waypoint6,waypoint2
  :precondition (and (at,rover0,waypoint6) (first_o11))
  :effect (and (at,rover0,waypoint2) (not (at,rover0,waypoint6))))
 (:action navigate,rover0,waypoint6,waypoint15
  :precondition (at,rover0,waypoint6)
  :effect (and (at,rover0,waypoint15) (not (at,rover0,waypoint6))))
 (:action navigate,rover0,waypoint6,waypoint19
  :precondition (at,rover0,waypoint6)
  :effect (and (at,rover0,waypoint19) (not (at,rover0,waypoint6))))
 (:action navigate,rover0,waypoint7,waypoint2
  :precondition (and (at,rover0,waypoint7) (first_o11))
  :effect (and (at,rover0,waypoint2) (not (at,rover0,waypoint7))))
 (:action navigate,rover0,waypoint7,waypoint3
  :precondition (at,rover0,waypoint7)
  :effect (and (at,rover0,waypoint3) (not (at,rover0,waypoint7))))
 (:action navigate,rover0,waypoint7,waypoint11
  :precondition (at,rover0,waypoint7)
  :effect (and (at,rover0,waypoint11) (not (at,rover0,waypoint7))))
 (:action navigate,rover0,waypoint8,waypoint9
  :precondition (at,rover0,waypoint8)
  :effect (and (at,rover0,waypoint9) (not (at,rover0,waypoint8))))
 (:action navigate,rover0,waypoint9,waypoint2
  :precondition (and (at,rover0,waypoint9) (first_o11))
  :effect (and (at,rover0,waypoint2) (not (at,rover0,waypoint9))))
 (:action navigate,rover0,waypoint9,waypoint4
  :precondition (at,rover0,waypoint9)
  :effect (and (at,rover0,waypoint4) (not (at,rover0,waypoint9))))
 (:action navigate,rover0,waypoint9,waypoint5
  :precondition (at,rover0,waypoint9)
  :effect (and (at,rover0,waypoint5) (not (at,rover0,waypoint9))))
 (:action navigate,rover0,waypoint9,waypoint8
  :precondition (at,rover0,waypoint9)
  :effect (and (at,rover0,waypoint8) (not (at,rover0,waypoint9))))
 (:action navigate,rover0,waypoint10,waypoint5
  :precondition (at,rover0,waypoint10)
  :effect (and (at,rover0,waypoint5) (not (at,rover0,waypoint10))))
 (:action navigate,rover0,waypoint11,waypoint7
  :precondition (at,rover0,waypoint11)
  :effect (and (at,rover0,waypoint7) (not (at,rover0,waypoint11))))
 (:action navigate,rover0,waypoint12,waypoint0
  :precondition (at,rover0,waypoint12)
  :effect (and (at,rover0,waypoint0) (not (at,rover0,waypoint12))))
 (:action navigate,rover0,waypoint13,waypoint0
  :precondition (at,rover0,waypoint13)
  :effect (and (at,rover0,waypoint0) (not (at,rover0,waypoint13))))
 (:action navigate,rover0,waypoint13,waypoint16
  :precondition (at,rover0,waypoint13)
  :effect (and (at,rover0,waypoint16) (not (at,rover0,waypoint13))))
 (:action navigate,rover0,waypoint14,waypoint2
  :precondition (and (at,rover0,waypoint14) (first_o11))
  :effect (and (at,rover0,waypoint2) (not (at,rover0,waypoint14))))
 (:action navigate,rover0,waypoint15,waypoint6
  :precondition (at,rover0,waypoint15)
  :effect (and (at,rover0,waypoint6) (not (at,rover0,waypoint15))))
 (:action navigate,rover0,waypoint16,waypoint13
  :precondition (at,rover0,waypoint16)
  :effect (and (at,rover0,waypoint13) (not (at,rover0,waypoint16))))
 (:action navigate,rover0,waypoint18,waypoint2
  :precondition (and (at,rover0,waypoint18) (first_o11))
  :effect (and (at,rover0,waypoint2) (not (at,rover0,waypoint18))))
 (:action navigate,rover0,waypoint19,waypoint6
  :precondition (at,rover0,waypoint19)
  :effect (and (at,rover0,waypoint6) (not (at,rover0,waypoint19))))
 (:action navigate,rover1,waypoint0,waypoint2
  :precondition (at,rover1,waypoint0)
  :effect (and (at,rover1,waypoint2) (not (at,rover1,waypoint0))))
 (:action navigate,rover1,waypoint0,waypoint9
  :precondition (at,rover1,waypoint0)
  :effect (and (at,rover1,waypoint9) (not (at,rover1,waypoint0))))
 (:action navigate,rover1,waypoint0,waypoint13
  :precondition (at,rover1,waypoint0)
  :effect (and (at,rover1,waypoint13) (not (at,rover1,waypoint0))))
 (:action navigate,rover1,waypoint0,waypoint16
  :precondition (at,rover1,waypoint0)
  :effect (and (at,rover1,waypoint16) (not (at,rover1,waypoint0))))
 (:action navigate,rover1,waypoint1,waypoint12
  :precondition (at,rover1,waypoint1)
  :effect (and (at,rover1,waypoint12) (not (at,rover1,waypoint1))))
 (:action navigate,rover1,waypoint2,waypoint0
  :precondition (at,rover1,waypoint2)
  :effect (and (at,rover1,waypoint0) (not (at,rover1,waypoint2))))
 (:action navigate,rover1,waypoint3,waypoint6
  :precondition (at,rover1,waypoint3)
  :effect (and (at,rover1,waypoint6) (not (at,rover1,waypoint3))))
 (:action navigate,rover1,waypoint3,waypoint9
  :precondition (at,rover1,waypoint3)
  :effect (and (at,rover1,waypoint9) (not (at,rover1,waypoint3))))
 (:action navigate,rover1,waypoint4,waypoint9
  :precondition (at,rover1,waypoint4)
  :effect (and (at,rover1,waypoint9) (not (at,rover1,waypoint4))))
 (:action navigate,rover1,waypoint4,waypoint11
  :precondition (at,rover1,waypoint4)
  :effect (and (at,rover1,waypoint11) (not (at,rover1,waypoint4))))
 (:action navigate,rover1,waypoint5,waypoint7
  :precondition (at,rover1,waypoint5)
  :effect (and (at,rover1,waypoint7) (not (at,rover1,waypoint5))))
 (:action navigate,rover1,waypoint6,waypoint3
  :precondition (at,rover1,waypoint6)
  :effect (and (at,rover1,waypoint3) (not (at,rover1,waypoint6))))
 (:action navigate,rover1,waypoint7,waypoint5
  :precondition (at,rover1,waypoint7)
  :effect (and (at,rover1,waypoint5) (not (at,rover1,waypoint7))))
 (:action navigate,rover1,waypoint7,waypoint9
  :precondition (at,rover1,waypoint7)
  :effect (and (at,rover1,waypoint9) (not (at,rover1,waypoint7))))
 (:action navigate,rover1,waypoint8,waypoint10
  :precondition (at,rover1,waypoint8)
  :effect (and (at,rover1,waypoint10) (not (at,rover1,waypoint8))))
 (:action navigate,rover1,waypoint9,waypoint0
  :precondition (at,rover1,waypoint9)
  :effect (and (at,rover1,waypoint0) (not (at,rover1,waypoint9))))
 (:action navigate,rover1,waypoint9,waypoint3
  :precondition (at,rover1,waypoint9)
  :effect (and (at,rover1,waypoint3) (not (at,rover1,waypoint9))))
 (:action navigate,rover1,waypoint9,waypoint4
  :precondition (at,rover1,waypoint9)
  :effect (and (at,rover1,waypoint4) (not (at,rover1,waypoint9))))
 (:action navigate,rover1,waypoint9,waypoint7
  :precondition (at,rover1,waypoint9)
  :effect (and (at,rover1,waypoint7) (not (at,rover1,waypoint9))))
 (:action navigate,rover1,waypoint9,waypoint10
  :precondition (at,rover1,waypoint9)
  :effect (and (at,rover1,waypoint10) (not (at,rover1,waypoint9))))
 (:action navigate,rover1,waypoint9,waypoint12
  :precondition (at,rover1,waypoint9)
  :effect (and (at,rover1,waypoint12) (not (at,rover1,waypoint9))))
 (:action navigate,rover1,waypoint9,waypoint14
  :precondition (at,rover1,waypoint9)
  :effect (and (at,rover1,waypoint14) (not (at,rover1,waypoint9))))
 (:action navigate,rover1,waypoint9,waypoint18
  :precondition (at,rover1,waypoint9)
  :effect (and (at,rover1,waypoint18) (not (at,rover1,waypoint9))))
 (:action navigate,rover1,waypoint9,waypoint19
  :precondition (at,rover1,waypoint9)
  :effect (and (at,rover1,waypoint19) (not (at,rover1,waypoint9))))
 (:action navigate,rover1,waypoint10,waypoint8
  :precondition (at,rover1,waypoint10)
  :effect (and (at,rover1,waypoint8) (not (at,rover1,waypoint10))))
 (:action navigate,rover1,waypoint10,waypoint9
  :precondition (at,rover1,waypoint10)
  :effect (and (at,rover1,waypoint9) (not (at,rover1,waypoint10))))
 (:action navigate,rover1,waypoint11,waypoint4
  :precondition (at,rover1,waypoint11)
  :effect (and (at,rover1,waypoint4) (not (at,rover1,waypoint11))))
 (:action navigate,rover1,waypoint12,waypoint1
  :precondition (at,rover1,waypoint12)
  :effect (and (at,rover1,waypoint1) (not (at,rover1,waypoint12))))
 (:action navigate,rover1,waypoint12,waypoint9
  :precondition (at,rover1,waypoint12)
  :effect (and (at,rover1,waypoint9) (not (at,rover1,waypoint12))))
 (:action navigate,rover1,waypoint13,waypoint0
  :precondition (at,rover1,waypoint13)
  :effect (and (at,rover1,waypoint0) (not (at,rover1,waypoint13))))
 (:action navigate,rover1,waypoint14,waypoint9
  :precondition (at,rover1,waypoint14)
  :effect (and (at,rover1,waypoint9) (not (at,rover1,waypoint14))))
 (:action navigate,rover1,waypoint14,waypoint15
  :precondition (at,rover1,waypoint14)
  :effect (and (at,rover1,waypoint15) (not (at,rover1,waypoint14))))
 (:action navigate,rover1,waypoint15,waypoint14
  :precondition (at,rover1,waypoint15)
  :effect (and (at,rover1,waypoint14) (not (at,rover1,waypoint15))))
 (:action navigate,rover1,waypoint16,waypoint0
  :precondition (at,rover1,waypoint16)
  :effect (and (at,rover1,waypoint0) (not (at,rover1,waypoint16))))
 (:action navigate,rover1,waypoint17,waypoint19
  :precondition (at,rover1,waypoint17)
  :effect (and (at,rover1,waypoint19) (not (at,rover1,waypoint17))))
 (:action navigate,rover1,waypoint18,waypoint9
  :precondition (at,rover1,waypoint18)
  :effect (and (at,rover1,waypoint9) (not (at,rover1,waypoint18))))
 (:action navigate,rover1,waypoint19,waypoint9
  :precondition (at,rover1,waypoint19)
  :effect (and (at,rover1,waypoint9) (not (at,rover1,waypoint19))))
 (:action navigate,rover1,waypoint19,waypoint17
  :precondition (at,rover1,waypoint19)
  :effect (and (at,rover1,waypoint17) (not (at,rover1,waypoint19))))
 (:action navigate,rover2,waypoint0,waypoint1
  :precondition (at,rover2,waypoint0)
  :effect (and (at,rover2,waypoint1) (not (at,rover2,waypoint0))))
 (:action navigate,rover2,waypoint0,waypoint9
  :precondition (at,rover2,waypoint0)
  :effect (and (at,rover2,waypoint9) (not (at,rover2,waypoint0))))
 (:action navigate,rover2,waypoint0,waypoint13
  :precondition (at,rover2,waypoint0)
  :effect (and (at,rover2,waypoint13) (not (at,rover2,waypoint0))))
 (:action navigate,rover2,waypoint0,waypoint16
  :precondition (at,rover2,waypoint0)
  :effect (and (at,rover2,waypoint16) (not (at,rover2,waypoint0))))
 (:action navigate,rover2,waypoint0,waypoint17
  :precondition (at,rover2,waypoint0)
  :effect (and (at,rover2,waypoint17) (not (at,rover2,waypoint0))))
 (:action navigate,rover2,waypoint0,waypoint18
  :precondition (at,rover2,waypoint0)
  :effect (and (at,rover2,waypoint18) (not (at,rover2,waypoint0))))
 (:action navigate,rover2,waypoint1,waypoint0
  :precondition (at,rover2,waypoint1)
  :effect (and (at,rover2,waypoint0) (not (at,rover2,waypoint1))))
 (:action navigate,rover2,waypoint1,waypoint2
  :precondition (at,rover2,waypoint1)
  :effect (and (at,rover2,waypoint2) (not (at,rover2,waypoint1))))
 (:action navigate,rover2,waypoint1,waypoint6
  :precondition (at,rover2,waypoint1)
  :effect (and (at,rover2,waypoint6) (not (at,rover2,waypoint1))))
 (:action navigate,rover2,waypoint1,waypoint11
  :precondition (at,rover2,waypoint1)
  :effect (and (at,rover2,waypoint11) (not (at,rover2,waypoint1))))
 (:action navigate,rover2,waypoint1,waypoint12
  :precondition (at,rover2,waypoint1)
  :effect (and (at,rover2,waypoint12) (not (at,rover2,waypoint1))))
 (:action navigate,rover2,waypoint1,waypoint15
  :precondition (at,rover2,waypoint1)
  :effect (and (at,rover2,waypoint15) (not (at,rover2,waypoint1))))
 (:action navigate,rover2,waypoint2,waypoint1
  :precondition (at,rover2,waypoint2)
  :effect (and (at,rover2,waypoint1) (not (at,rover2,waypoint2))))
 (:action navigate,rover2,waypoint3,waypoint9
  :precondition (at,rover2,waypoint3)
  :effect (and (at,rover2,waypoint9) (not (at,rover2,waypoint3))))
 (:action navigate,rover2,waypoint4,waypoint9
  :precondition (at,rover2,waypoint4)
  :effect (and (at,rover2,waypoint9) (not (at,rover2,waypoint4))))
 (:action navigate,rover2,waypoint5,waypoint9
  :precondition (at,rover2,waypoint5)
  :effect (and (at,rover2,waypoint9) (not (at,rover2,waypoint5))))
 (:action navigate,rover2,waypoint6,waypoint1
  :precondition (at,rover2,waypoint6)
  :effect (and (at,rover2,waypoint1) (not (at,rover2,waypoint6))))
 (:action navigate,rover2,waypoint7,waypoint9
  :precondition (at,rover2,waypoint7)
  :effect (and (at,rover2,waypoint9) (not (at,rover2,waypoint7))))
 (:action navigate,rover2,waypoint9,waypoint0
  :precondition (at,rover2,waypoint9)
  :effect (and (at,rover2,waypoint0) (not (at,rover2,waypoint9))))
 (:action navigate,rover2,waypoint9,waypoint3
  :precondition (at,rover2,waypoint9)
  :effect (and (at,rover2,waypoint3) (not (at,rover2,waypoint9))))
 (:action navigate,rover2,waypoint9,waypoint4
  :precondition (at,rover2,waypoint9)
  :effect (and (at,rover2,waypoint4) (not (at,rover2,waypoint9))))
 (:action navigate,rover2,waypoint9,waypoint5
  :precondition (at,rover2,waypoint9)
  :effect (and (at,rover2,waypoint5) (not (at,rover2,waypoint9))))
 (:action navigate,rover2,waypoint9,waypoint7
  :precondition (at,rover2,waypoint9)
  :effect (and (at,rover2,waypoint7) (not (at,rover2,waypoint9))))
 (:action navigate,rover2,waypoint9,waypoint10
  :precondition (at,rover2,waypoint9)
  :effect (and (at,rover2,waypoint10) (not (at,rover2,waypoint9))))
 (:action navigate,rover2,waypoint9,waypoint14
  :precondition (at,rover2,waypoint9)
  :effect (and (at,rover2,waypoint14) (not (at,rover2,waypoint9))))
 (:action navigate,rover2,waypoint9,waypoint19
  :precondition (at,rover2,waypoint9)
  :effect (and (at,rover2,waypoint19) (not (at,rover2,waypoint9))))
 (:action navigate,rover2,waypoint10,waypoint9
  :precondition (at,rover2,waypoint10)
  :effect (and (at,rover2,waypoint9) (not (at,rover2,waypoint10))))
 (:action navigate,rover2,waypoint11,waypoint1
  :precondition (at,rover2,waypoint11)
  :effect (and (at,rover2,waypoint1) (not (at,rover2,waypoint11))))
 (:action navigate,rover2,waypoint12,waypoint1
  :precondition (at,rover2,waypoint12)
  :effect (and (at,rover2,waypoint1) (not (at,rover2,waypoint12))))
 (:action navigate,rover2,waypoint13,waypoint0
  :precondition (at,rover2,waypoint13)
  :effect (and (at,rover2,waypoint0) (not (at,rover2,waypoint13))))
 (:action navigate,rover2,waypoint14,waypoint9
  :precondition (at,rover2,waypoint14)
  :effect (and (at,rover2,waypoint9) (not (at,rover2,waypoint14))))
 (:action navigate,rover2,waypoint15,waypoint1
  :precondition (at,rover2,waypoint15)
  :effect (and (at,rover2,waypoint1) (not (at,rover2,waypoint15))))
 (:action navigate,rover2,waypoint16,waypoint0
  :precondition (at,rover2,waypoint16)
  :effect (and (at,rover2,waypoint0) (not (at,rover2,waypoint16))))
 (:action navigate,rover2,waypoint17,waypoint0
  :precondition (at,rover2,waypoint17)
  :effect (and (at,rover2,waypoint0) (not (at,rover2,waypoint17))))
 (:action navigate,rover2,waypoint18,waypoint0
  :precondition (at,rover2,waypoint18)
  :effect (and (at,rover2,waypoint0) (not (at,rover2,waypoint18))))
 (:action navigate,rover2,waypoint19,waypoint9
  :precondition (at,rover2,waypoint19)
  :effect (and (at,rover2,waypoint9) (not (at,rover2,waypoint19))))
 (:action navigate,rover3,waypoint0,waypoint1
  :precondition (at,rover3,waypoint0)
  :effect (and (at,rover3,waypoint1) (not (at,rover3,waypoint0))))
 (:action navigate,rover3,waypoint0,waypoint2
  :precondition (at,rover3,waypoint0)
  :effect (and (at,rover3,waypoint2) (not (at,rover3,waypoint0))))
 (:action navigate,rover3,waypoint0,waypoint9
  :precondition (at,rover3,waypoint0)
  :effect (and (at,rover3,waypoint9) (not (at,rover3,waypoint0))))
 (:action navigate,rover3,waypoint0,waypoint12
  :precondition (at,rover3,waypoint0)
  :effect (and (at,rover3,waypoint12) (not (at,rover3,waypoint0))))
 (:action navigate,rover3,waypoint0,waypoint17
  :precondition (at,rover3,waypoint0)
  :effect (and (at,rover3,waypoint17) (not (at,rover3,waypoint0))))
 (:action navigate,rover3,waypoint0,waypoint18
  :precondition (at,rover3,waypoint0)
  :effect (and (at,rover3,waypoint18) (not (at,rover3,waypoint0))))
 (:action navigate,rover3,waypoint1,waypoint0
  :precondition (at,rover3,waypoint1)
  :effect (and (at,rover3,waypoint0) (not (at,rover3,waypoint1))))
 (:action navigate,rover3,waypoint1,waypoint6
  :precondition (at,rover3,waypoint1)
  :effect (and (at,rover3,waypoint6) (not (at,rover3,waypoint1))))
 (:action navigate,rover3,waypoint1,waypoint7
  :precondition (at,rover3,waypoint1)
  :effect (and (at,rover3,waypoint7) (not (at,rover3,waypoint1))))
 (:action navigate,rover3,waypoint1,waypoint15
  :precondition (at,rover3,waypoint1)
  :effect (and (at,rover3,waypoint15) (not (at,rover3,waypoint1))))
 (:action navigate,rover3,waypoint2,waypoint0
  :precondition (at,rover3,waypoint2)
  :effect (and (at,rover3,waypoint0) (not (at,rover3,waypoint2))))
 (:action navigate,rover3,waypoint2,waypoint14
  :precondition (at,rover3,waypoint2)
  :effect (and (at,rover3,waypoint14) (not (at,rover3,waypoint2))))
 (:action navigate,rover3,waypoint3,waypoint9
  :precondition (at,rover3,waypoint3)
  :effect (and (at,rover3,waypoint9) (not (at,rover3,waypoint3))))
 (:action navigate,rover3,waypoint4,waypoint18
  :precondition (at,rover3,waypoint4)
  :effect (and (at,rover3,waypoint18) (not (at,rover3,waypoint4))))
 (:action navigate,rover3,waypoint5,waypoint9
  :precondition (at,rover3,waypoint5)
  :effect (and (at,rover3,waypoint9) (not (at,rover3,waypoint5))))
 (:action navigate,rover3,waypoint6,waypoint1
  :precondition (at,rover3,waypoint6)
  :effect (and (at,rover3,waypoint1) (not (at,rover3,waypoint6))))
 (:action navigate,rover3,waypoint7,waypoint1
  :precondition (at,rover3,waypoint7)
  :effect (and (at,rover3,waypoint1) (not (at,rover3,waypoint7))))
 (:action navigate,rover3,waypoint7,waypoint10
  :precondition (at,rover3,waypoint7)
  :effect (and (at,rover3,waypoint10) (not (at,rover3,waypoint7))))
 (:action navigate,rover3,waypoint8,waypoint9
  :precondition (at,rover3,waypoint8)
  :effect (and (at,rover3,waypoint9) (not (at,rover3,waypoint8))))
 (:action navigate,rover3,waypoint9,waypoint0
  :precondition (at,rover3,waypoint9)
  :effect (and (at,rover3,waypoint0) (not (at,rover3,waypoint9))))
 (:action navigate,rover3,waypoint9,waypoint3
  :precondition (at,rover3,waypoint9)
  :effect (and (at,rover3,waypoint3) (not (at,rover3,waypoint9))))
 (:action navigate,rover3,waypoint9,waypoint5
  :precondition (at,rover3,waypoint9)
  :effect (and (at,rover3,waypoint5) (not (at,rover3,waypoint9))))
 (:action navigate,rover3,waypoint9,waypoint8
  :precondition (at,rover3,waypoint9)
  :effect (and (at,rover3,waypoint8) (not (at,rover3,waypoint9))))
 (:action navigate,rover3,waypoint10,waypoint7
  :precondition (at,rover3,waypoint10)
  :effect (and (at,rover3,waypoint7) (not (at,rover3,waypoint10))))
 (:action navigate,rover3,waypoint11,waypoint16
  :precondition (at,rover3,waypoint11)
  :effect (and (at,rover3,waypoint16) (not (at,rover3,waypoint11))))
 (:action navigate,rover3,waypoint11,waypoint18
  :precondition (at,rover3,waypoint11)
  :effect (and (at,rover3,waypoint18) (not (at,rover3,waypoint11))))
 (:action navigate,rover3,waypoint12,waypoint0
  :precondition (at,rover3,waypoint12)
  :effect (and (at,rover3,waypoint0) (not (at,rover3,waypoint12))))
 (:action navigate,rover3,waypoint12,waypoint19
  :precondition (at,rover3,waypoint12)
  :effect (and (at,rover3,waypoint19) (not (at,rover3,waypoint12))))
 (:action navigate,rover3,waypoint13,waypoint16
  :precondition (at,rover3,waypoint13)
  :effect (and (at,rover3,waypoint16) (not (at,rover3,waypoint13))))
 (:action navigate,rover3,waypoint14,waypoint2
  :precondition (at,rover3,waypoint14)
  :effect (and (at,rover3,waypoint2) (not (at,rover3,waypoint14))))
 (:action navigate,rover3,waypoint15,waypoint1
  :precondition (at,rover3,waypoint15)
  :effect (and (at,rover3,waypoint1) (not (at,rover3,waypoint15))))
 (:action navigate,rover3,waypoint16,waypoint11
  :precondition (at,rover3,waypoint16)
  :effect (and (at,rover3,waypoint11) (not (at,rover3,waypoint16))))
 (:action navigate,rover3,waypoint16,waypoint13
  :precondition (at,rover3,waypoint16)
  :effect (and (at,rover3,waypoint13) (not (at,rover3,waypoint16))))
 (:action navigate,rover3,waypoint17,waypoint0
  :precondition (at,rover3,waypoint17)
  :effect (and (at,rover3,waypoint0) (not (at,rover3,waypoint17))))
 (:action navigate,rover3,waypoint18,waypoint0
  :precondition (at,rover3,waypoint18)
  :effect (and (at,rover3,waypoint0) (not (at,rover3,waypoint18))))
 (:action navigate,rover3,waypoint18,waypoint4
  :precondition (at,rover3,waypoint18)
  :effect (and (at,rover3,waypoint4) (not (at,rover3,waypoint18))))
 (:action navigate,rover3,waypoint18,waypoint11
  :precondition (at,rover3,waypoint18)
  :effect (and (at,rover3,waypoint11) (not (at,rover3,waypoint18))))
 (:action navigate,rover3,waypoint19,waypoint12
  :precondition (at,rover3,waypoint19)
  :effect (and (at,rover3,waypoint12) (not (at,rover3,waypoint19))))
 (:action navigate,rover4,waypoint0,waypoint9
  :precondition (at,rover4,waypoint0)
  :effect (and (at,rover4,waypoint9) (not (at,rover4,waypoint0))))
 (:action navigate,rover4,waypoint1,waypoint15
  :precondition (at,rover4,waypoint1)
  :effect (and (at,rover4,waypoint15) (not (at,rover4,waypoint1))))
 (:action navigate,rover4,waypoint2,waypoint14
  :precondition (at,rover4,waypoint2)
  :effect (and (at,rover4,waypoint14) (not (at,rover4,waypoint2))))
 (:action navigate,rover4,waypoint3,waypoint6
  :precondition (and (at,rover4,waypoint3) (first_o3))
  :effect (and (at,rover4,waypoint6) (not (at,rover4,waypoint3))))
 (:action navigate,rover4,waypoint3,waypoint7
  :precondition (at,rover4,waypoint3)
  :effect (and (at,rover4,waypoint7) (not (at,rover4,waypoint3))))
 (:action navigate,rover4,waypoint3,waypoint9
  :precondition (at,rover4,waypoint3)
  :effect (and (at,rover4,waypoint9) (not (at,rover4,waypoint3))))
 (:action navigate,rover4,waypoint3,waypoint10
  :precondition (at,rover4,waypoint3)
  :effect (and (at,rover4,waypoint10) (not (at,rover4,waypoint3))))
 (:action navigate,rover4,waypoint3,waypoint11
  :precondition (at,rover4,waypoint3)
  :effect (and (at,rover4,waypoint11) (not (at,rover4,waypoint3))))
 (:action navigate,rover4,waypoint3,waypoint13
  :precondition (at,rover4,waypoint3)
  :effect (and (at,rover4,waypoint13) (not (at,rover4,waypoint3))))
 (:action navigate,rover4,waypoint3,waypoint15
  :precondition (at,rover4,waypoint3)
  :effect (and (at,rover4,waypoint15) (not (at,rover4,waypoint3))))
 (:action navigate,rover4,waypoint4,waypoint9
  :precondition (at,rover4,waypoint4)
  :effect (and (at,rover4,waypoint9) (not (at,rover4,waypoint4))))
 (:action navigate,rover4,waypoint5,waypoint6
  :precondition (and (at,rover4,waypoint5) (first_o3))
  :effect (and (at,rover4,waypoint6) (not (at,rover4,waypoint5))))
 (:action navigate,rover4,waypoint6,waypoint3
  :precondition (at,rover4,waypoint6)
  :effect (and (at,rover4,waypoint3) (not (at,rover4,waypoint6)) (not (first_o3))))
 (:action navigate,rover4,waypoint6,waypoint5
  :precondition (at,rover4,waypoint6)
  :effect (and (at,rover4,waypoint5) (not (at,rover4,waypoint6)) (not (first_o3))))
 (:action navigate,rover4,waypoint6,waypoint12
  :precondition (at,rover4,waypoint6)
  :effect (and (at,rover4,waypoint12) (not (at,rover4,waypoint6)) (not (first_o3))))
 (:action navigate,rover4,waypoint6,waypoint14
  :precondition (at,rover4,waypoint6)
  :effect (and (at,rover4,waypoint14) (not (at,rover4,waypoint6)) (not (first_o3))))
 (:action navigate,rover4,waypoint7,waypoint3
  :precondition (at,rover4,waypoint7)
  :effect (and (at,rover4,waypoint3) (not (at,rover4,waypoint7))))
 (:action navigate,rover4,waypoint8,waypoint9
  :precondition (at,rover4,waypoint8)
  :effect (and (at,rover4,waypoint9) (not (at,rover4,waypoint8))))
 (:action navigate,rover4,waypoint9,waypoint0
  :precondition (at,rover4,waypoint9)
  :effect (and (at,rover4,waypoint0) (not (at,rover4,waypoint9))))
 (:action navigate,rover4,waypoint9,waypoint3
  :precondition (at,rover4,waypoint9)
  :effect (and (at,rover4,waypoint3) (not (at,rover4,waypoint9))))
 (:action navigate,rover4,waypoint9,waypoint4
  :precondition (at,rover4,waypoint9)
  :effect (and (at,rover4,waypoint4) (not (at,rover4,waypoint9))))
 (:action navigate,rover4,waypoint9,waypoint8
  :precondition (at,rover4,waypoint9)
  :effect (and (at,rover4,waypoint8) (not (at,rover4,waypoint9))))
 (:action navigate,rover4,waypoint9,waypoint19
  :precondition (at,rover4,waypoint9)
  :effect (and (at,rover4,waypoint19) (not (at,rover4,waypoint9))))
 (:action navigate,rover4,waypoint10,waypoint3
  :precondition (at,rover4,waypoint10)
  :effect (and (at,rover4,waypoint3) (not (at,rover4,waypoint10))))
 (:action navigate,rover4,waypoint10,waypoint16
  :precondition (at,rover4,waypoint10)
  :effect (and (at,rover4,waypoint16) (not (at,rover4,waypoint10))))
 (:action navigate,rover4,waypoint11,waypoint3
  :precondition (at,rover4,waypoint11)
  :effect (and (at,rover4,waypoint3) (not (at,rover4,waypoint11))))
 (:action navigate,rover4,waypoint11,waypoint17
  :precondition (at,rover4,waypoint11)
  :effect (and (at,rover4,waypoint17) (not (at,rover4,waypoint11))))
 (:action navigate,rover4,waypoint11,waypoint18
  :precondition (at,rover4,waypoint11)
  :effect (and (at,rover4,waypoint18) (not (at,rover4,waypoint11))))
 (:action navigate,rover4,waypoint12,waypoint6
  :precondition (and (at,rover4,waypoint12) (first_o3))
  :effect (and (at,rover4,waypoint6) (not (at,rover4,waypoint12))))
 (:action navigate,rover4,waypoint13,waypoint3
  :precondition (at,rover4,waypoint13)
  :effect (and (at,rover4,waypoint3) (not (at,rover4,waypoint13))))
 (:action navigate,rover4,waypoint14,waypoint2
  :precondition (at,rover4,waypoint14)
  :effect (and (at,rover4,waypoint2) (not (at,rover4,waypoint14))))
 (:action navigate,rover4,waypoint14,waypoint6
  :precondition (and (at,rover4,waypoint14) (first_o3))
  :effect (and (at,rover4,waypoint6) (not (at,rover4,waypoint14))))
 (:action navigate,rover4,waypoint15,waypoint1
  :precondition (at,rover4,waypoint15)
  :effect (and (at,rover4,waypoint1) (not (at,rover4,waypoint15))))
 (:action navigate,rover4,waypoint15,waypoint3
  :precondition (at,rover4,waypoint15)
  :effect (and (at,rover4,waypoint3) (not (at,rover4,waypoint15))))
 (:action navigate,rover4,waypoint16,waypoint10
  :precondition (at,rover4,waypoint16)
  :effect (and (at,rover4,waypoint10) (not (at,rover4,waypoint16))))
 (:action navigate,rover4,waypoint17,waypoint11
  :precondition (at,rover4,waypoint17)
  :effect (and (at,rover4,waypoint11) (not (at,rover4,waypoint17))))
 (:action navigate,rover4,waypoint18,waypoint11
  :precondition (at,rover4,waypoint18)
  :effect (and (at,rover4,waypoint11) (not (at,rover4,waypoint18))))
 (:action navigate,rover4,waypoint19,waypoint9
  :precondition (at,rover4,waypoint19)
  :effect (and (at,rover4,waypoint9) (not (at,rover4,waypoint19))))
 (:action navigate,rover5,waypoint0,waypoint1
  :precondition (at,rover5,waypoint0)
  :effect (and (at,rover5,waypoint1) (not (at,rover5,waypoint0))))
 (:action navigate,rover5,waypoint0,waypoint12
  :precondition (at,rover5,waypoint0)
  :effect (and (at,rover5,waypoint12) (not (at,rover5,waypoint0))))
 (:action navigate,rover5,waypoint0,waypoint13
  :precondition (at,rover5,waypoint0)
  :effect (and (at,rover5,waypoint13) (not (at,rover5,waypoint0))))
 (:action navigate,rover5,waypoint0,waypoint17
  :precondition (at,rover5,waypoint0)
  :effect (and (at,rover5,waypoint17) (not (at,rover5,waypoint0))))
 (:action navigate,rover5,waypoint0,waypoint18
  :precondition (at,rover5,waypoint0)
  :effect (and (at,rover5,waypoint18) (not (at,rover5,waypoint0))))
 (:action navigate,rover5,waypoint1,waypoint0
  :precondition (at,rover5,waypoint1)
  :effect (and (at,rover5,waypoint0) (not (at,rover5,waypoint1))))
 (:action navigate,rover5,waypoint1,waypoint2
  :precondition (at,rover5,waypoint1)
  :effect (and (at,rover5,waypoint2) (not (at,rover5,waypoint1))))
 (:action navigate,rover5,waypoint1,waypoint6
  :precondition (at,rover5,waypoint1)
  :effect (and (at,rover5,waypoint6) (not (at,rover5,waypoint1))))
 (:action navigate,rover5,waypoint1,waypoint7
  :precondition (at,rover5,waypoint1)
  :effect (and (at,rover5,waypoint7) (not (at,rover5,waypoint1))))
 (:action navigate,rover5,waypoint1,waypoint11
  :precondition (at,rover5,waypoint1)
  :effect (and (at,rover5,waypoint11) (not (at,rover5,waypoint1))))
 (:action navigate,rover5,waypoint1,waypoint15
  :precondition (at,rover5,waypoint1)
  :effect (and (at,rover5,waypoint15) (not (at,rover5,waypoint1))))
 (:action navigate,rover5,waypoint2,waypoint1
  :precondition (at,rover5,waypoint2)
  :effect (and (at,rover5,waypoint1) (not (at,rover5,waypoint2))))
 (:action navigate,rover5,waypoint3,waypoint7
  :precondition (at,rover5,waypoint3)
  :effect (and (at,rover5,waypoint7) (not (at,rover5,waypoint3))))
 (:action navigate,rover5,waypoint4,waypoint12
  :precondition (at,rover5,waypoint4)
  :effect (and (at,rover5,waypoint12) (not (at,rover5,waypoint4))))
 (:action navigate,rover5,waypoint5,waypoint12
  :precondition (at,rover5,waypoint5)
  :effect (and (at,rover5,waypoint12) (not (at,rover5,waypoint5))))
 (:action navigate,rover5,waypoint6,waypoint1
  :precondition (at,rover5,waypoint6)
  :effect (and (at,rover5,waypoint1) (not (at,rover5,waypoint6))))
 (:action navigate,rover5,waypoint7,waypoint1
  :precondition (at,rover5,waypoint7)
  :effect (and (at,rover5,waypoint1) (not (at,rover5,waypoint7))))
 (:action navigate,rover5,waypoint7,waypoint3
  :precondition (at,rover5,waypoint7)
  :effect (and (at,rover5,waypoint3) (not (at,rover5,waypoint7))))
 (:action navigate,rover5,waypoint7,waypoint10
  :precondition (at,rover5,waypoint7)
  :effect (and (at,rover5,waypoint10) (not (at,rover5,waypoint7))))
 (:action navigate,rover5,waypoint8,waypoint12
  :precondition (at,rover5,waypoint8)
  :effect (and (at,rover5,waypoint12) (not (at,rover5,waypoint8))))
 (:action navigate,rover5,waypoint9,waypoint12
  :precondition (at,rover5,waypoint9)
  :effect (and (at,rover5,waypoint12) (not (at,rover5,waypoint9))))
 (:action navigate,rover5,waypoint10,waypoint7
  :precondition (at,rover5,waypoint10)
  :effect (and (at,rover5,waypoint7) (not (at,rover5,waypoint10))))
 (:action navigate,rover5,waypoint11,waypoint1
  :precondition (at,rover5,waypoint11)
  :effect (and (at,rover5,waypoint1) (not (at,rover5,waypoint11))))
 (:action navigate,rover5,waypoint12,waypoint0
  :precondition (at,rover5,waypoint12)
  :effect (and (at,rover5,waypoint0) (not (at,rover5,waypoint12))))
 (:action navigate,rover5,waypoint12,waypoint4
  :precondition (at,rover5,waypoint12)
  :effect (and (at,rover5,waypoint4) (not (at,rover5,waypoint12))))
 (:action navigate,rover5,waypoint12,waypoint5
  :precondition (at,rover5,waypoint12)
  :effect (and (at,rover5,waypoint5) (not (at,rover5,waypoint12))))
 (:action navigate,rover5,waypoint12,waypoint8
  :precondition (at,rover5,waypoint12)
  :effect (and (at,rover5,waypoint8) (not (at,rover5,waypoint12))))
 (:action navigate,rover5,waypoint12,waypoint9
  :precondition (at,rover5,waypoint12)
  :effect (and (at,rover5,waypoint9) (not (at,rover5,waypoint12))))
 (:action navigate,rover5,waypoint12,waypoint14
  :precondition (at,rover5,waypoint12)
  :effect (and (at,rover5,waypoint14) (not (at,rover5,waypoint12))))
 (:action navigate,rover5,waypoint12,waypoint19
  :precondition (at,rover5,waypoint12)
  :effect (and (at,rover5,waypoint19) (not (at,rover5,waypoint12))))
 (:action navigate,rover5,waypoint13,waypoint0
  :precondition (at,rover5,waypoint13)
  :effect (and (at,rover5,waypoint0) (not (at,rover5,waypoint13))))
 (:action navigate,rover5,waypoint13,waypoint16
  :precondition (at,rover5,waypoint13)
  :effect (and (at,rover5,waypoint16) (not (at,rover5,waypoint13))))
 (:action navigate,rover5,waypoint14,waypoint12
  :precondition (at,rover5,waypoint14)
  :effect (and (at,rover5,waypoint12) (not (at,rover5,waypoint14))))
 (:action navigate,rover5,waypoint15,waypoint1
  :precondition (at,rover5,waypoint15)
  :effect (and (at,rover5,waypoint1) (not (at,rover5,waypoint15))))
 (:action navigate,rover5,waypoint16,waypoint13
  :precondition (at,rover5,waypoint16)
  :effect (and (at,rover5,waypoint13) (not (at,rover5,waypoint16))))
 (:action navigate,rover5,waypoint17,waypoint0
  :precondition (at,rover5,waypoint17)
  :effect (and (at,rover5,waypoint0) (not (at,rover5,waypoint17))))
 (:action navigate,rover5,waypoint18,waypoint0
  :precondition (at,rover5,waypoint18)
  :effect (and (at,rover5,waypoint0) (not (at,rover5,waypoint18))))
 (:action navigate,rover5,waypoint19,waypoint12
  :precondition (at,rover5,waypoint19)
  :effect (and (at,rover5,waypoint12) (not (at,rover5,waypoint19))))
 (:action sample_soil,rover0,rover0store,waypoint0
  :precondition (and (at,rover0,waypoint0) (at_soil_sample,waypoint0) (empty,rover0store))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint0) (not (at_soil_sample,waypoint0)) (not (empty,rover0store))))
 (:action sample_soil,rover0,rover0store,waypoint3
  :precondition (and (at,rover0,waypoint3) (empty,rover0store) (at_soil_sample,waypoint3))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint3) (not (empty,rover0store)) (not (at_soil_sample,waypoint3))))
 (:action sample_soil,rover0,rover0store,waypoint8
  :precondition (and (at,rover0,waypoint8) (empty,rover0store) (at_soil_sample,waypoint8))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint8) (not (empty,rover0store)) (not (at_soil_sample,waypoint8))))
 (:action sample_soil,rover0,rover0store,waypoint9
  :precondition (and (at,rover0,waypoint9) (empty,rover0store) (at_soil_sample,waypoint9))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint9) (not (empty,rover0store)) (not (at_soil_sample,waypoint9))))
 (:action sample_soil,rover0,rover0store,waypoint10
  :precondition (and (at,rover0,waypoint10) (empty,rover0store) (at_soil_sample,waypoint10))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint10) (not (empty,rover0store)) (not (at_soil_sample,waypoint10))))
 (:action sample_soil,rover0,rover0store,waypoint11
  :precondition (and (at,rover0,waypoint11) (empty,rover0store) (at_soil_sample,waypoint11))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint11) (not (empty,rover0store)) (not (at_soil_sample,waypoint11))))
 (:action sample_soil,rover0,rover0store,waypoint12
  :precondition (and (at,rover0,waypoint12) (empty,rover0store) (at_soil_sample,waypoint12))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint12) (not (empty,rover0store)) (not (at_soil_sample,waypoint12))))
 (:action sample_soil,rover0,rover0store,waypoint13
  :precondition (and (at,rover0,waypoint13) (empty,rover0store) (at_soil_sample,waypoint13))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint13) (not (empty,rover0store)) (not (at_soil_sample,waypoint13))))
 (:action sample_soil,rover0,rover0store,waypoint14
  :precondition (and (at,rover0,waypoint14) (empty,rover0store) (at_soil_sample,waypoint14))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint14) (not (empty,rover0store)) (not (at_soil_sample,waypoint14))))
 (:action sample_soil,rover0,rover0store,waypoint15
  :precondition (and (at,rover0,waypoint15) (empty,rover0store) (at_soil_sample,waypoint15))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint15) (not (empty,rover0store)) (not (at_soil_sample,waypoint15))))
 (:action sample_soil,rover0,rover0store,waypoint18
  :precondition (and (at,rover0,waypoint18) (empty,rover0store) (at_soil_sample,waypoint18))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint18) (not (empty,rover0store)) (not (at_soil_sample,waypoint18))))
 (:action sample_soil,rover0,rover0store,waypoint19
  :precondition (and (at,rover0,waypoint19) (empty,rover0store) (at_soil_sample,waypoint19))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint19) (not (empty,rover0store)) (not (at_soil_sample,waypoint19))))
 (:action sample_soil,rover2,rover2store,waypoint0
  :precondition (and (at,rover2,waypoint0) (at_soil_sample,waypoint0) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint0) (not (at_soil_sample,waypoint0)) (not (empty,rover2store)) (not (first_o14))))
 (:action sample_soil,rover2,rover2store,waypoint3
  :precondition (and (at,rover2,waypoint3) (at_soil_sample,waypoint3) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint3) (not (at_soil_sample,waypoint3)) (not (empty,rover2store)) (not (first_o14))))
 (:action sample_soil,rover2,rover2store,waypoint9
  :precondition (and (at,rover2,waypoint9) (at_soil_sample,waypoint9) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint9) (not (at_soil_sample,waypoint9)) (not (empty,rover2store)) (not (first_o14))))
 (:action sample_soil,rover2,rover2store,waypoint10
  :precondition (and (at,rover2,waypoint10) (at_soil_sample,waypoint10) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint10) (not (at_soil_sample,waypoint10)) (not (empty,rover2store)) (not (first_o14))))
 (:action sample_soil,rover2,rover2store,waypoint11
  :precondition (and (at,rover2,waypoint11) (at_soil_sample,waypoint11) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint11) (not (at_soil_sample,waypoint11)) (not (empty,rover2store)) (not (first_o14))))
 (:action sample_soil,rover2,rover2store,waypoint12
  :precondition (and (at,rover2,waypoint12) (at_soil_sample,waypoint12) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint12) (not (at_soil_sample,waypoint12)) (not (empty,rover2store)) (not (first_o14))))
 (:action sample_soil,rover2,rover2store,waypoint13
  :precondition (and (at,rover2,waypoint13) (at_soil_sample,waypoint13) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint13) (not (at_soil_sample,waypoint13)) (not (empty,rover2store)) (not (first_o14))))
 (:action sample_soil,rover2,rover2store,waypoint14
  :precondition (and (at,rover2,waypoint14) (at_soil_sample,waypoint14) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint14) (not (at_soil_sample,waypoint14)) (not (empty,rover2store)) (not (first_o14))))
 (:action sample_soil,rover2,rover2store,waypoint15
  :precondition (and (at,rover2,waypoint15) (at_soil_sample,waypoint15) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint15) (not (at_soil_sample,waypoint15)) (not (empty,rover2store)) (not (first_o14))))
 (:action sample_soil,rover2,rover2store,waypoint17
  :precondition (and (at,rover2,waypoint17) (at_soil_sample,waypoint17) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint17) (not (at_soil_sample,waypoint17)) (not (empty,rover2store)) (not (first_o14))))
 (:action sample_soil,rover2,rover2store,waypoint18
  :precondition (and (at,rover2,waypoint18) (at_soil_sample,waypoint18) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint18) (not (at_soil_sample,waypoint18)) (not (empty,rover2store)) (not (first_o14))))
 (:action sample_soil,rover2,rover2store,waypoint19
  :precondition (and (at,rover2,waypoint19) (at_soil_sample,waypoint19) (empty,rover2store))
  :effect (and (full,rover2store) (have_soil_analysis,rover2,waypoint19) (not (at_soil_sample,waypoint19)) (not (empty,rover2store)) (not (first_o14))))
 (:action sample_soil,rover4,rover4store,waypoint0
  :precondition (and (at,rover4,waypoint0) (at_soil_sample,waypoint0) (empty,rover4store))
  :effect (and (full,rover4store) (have_soil_analysis,rover4,waypoint0) (not (at_soil_sample,waypoint0)) (not (empty,rover4store))))
 (:action sample_soil,rover4,rover4store,waypoint3
  :precondition (and (at,rover4,waypoint3) (at_soil_sample,waypoint3) (empty,rover4store))
  :effect (and (full,rover4store) (have_soil_analysis,rover4,waypoint3) (not (at_soil_sample,waypoint3)) (not (empty,rover4store))))
 (:action sample_soil,rover4,rover4store,waypoint8
  :precondition (and (at,rover4,waypoint8) (at_soil_sample,waypoint8) (empty,rover4store))
  :effect (and (full,rover4store) (have_soil_analysis,rover4,waypoint8) (not (at_soil_sample,waypoint8)) (not (empty,rover4store))))
 (:action sample_soil,rover4,rover4store,waypoint9
  :precondition (and (at,rover4,waypoint9) (at_soil_sample,waypoint9) (empty,rover4store))
  :effect (and (full,rover4store) (have_soil_analysis,rover4,waypoint9) (not (at_soil_sample,waypoint9)) (not (empty,rover4store))))
 (:action sample_soil,rover4,rover4store,waypoint10
  :precondition (and (at,rover4,waypoint10) (at_soil_sample,waypoint10) (empty,rover4store))
  :effect (and (full,rover4store) (have_soil_analysis,rover4,waypoint10) (not (at_soil_sample,waypoint10)) (not (empty,rover4store))))
 (:action sample_soil,rover4,rover4store,waypoint11
  :precondition (and (at,rover4,waypoint11) (at_soil_sample,waypoint11) (empty,rover4store))
  :effect (and (full,rover4store) (have_soil_analysis,rover4,waypoint11) (not (at_soil_sample,waypoint11)) (not (empty,rover4store))))
 (:action sample_soil,rover4,rover4store,waypoint12
  :precondition (and (at,rover4,waypoint12) (at_soil_sample,waypoint12) (empty,rover4store))
  :effect (and (full,rover4store) (have_soil_analysis,rover4,waypoint12) (not (at_soil_sample,waypoint12)) (not (empty,rover4store))))
 (:action sample_soil,rover4,rover4store,waypoint13
  :precondition (and (at,rover4,waypoint13) (at_soil_sample,waypoint13) (empty,rover4store))
  :effect (and (full,rover4store) (have_soil_analysis,rover4,waypoint13) (not (at_soil_sample,waypoint13)) (not (empty,rover4store))))
 (:action sample_soil,rover4,rover4store,waypoint14
  :precondition (and (at,rover4,waypoint14) (at_soil_sample,waypoint14) (empty,rover4store))
  :effect (and (full,rover4store) (have_soil_analysis,rover4,waypoint14) (not (at_soil_sample,waypoint14)) (not (empty,rover4store))))
 (:action sample_soil,rover4,rover4store,waypoint15
  :precondition (and (at,rover4,waypoint15) (at_soil_sample,waypoint15) (empty,rover4store))
  :effect (and (full,rover4store) (have_soil_analysis,rover4,waypoint15) (not (at_soil_sample,waypoint15)) (not (empty,rover4store))))
 (:action sample_soil,rover4,rover4store,waypoint17
  :precondition (and (at,rover4,waypoint17) (at_soil_sample,waypoint17) (empty,rover4store))
  :effect (and (full,rover4store) (have_soil_analysis,rover4,waypoint17) (not (at_soil_sample,waypoint17)) (not (empty,rover4store))))
 (:action sample_soil,rover4,rover4store,waypoint18
  :precondition (and (at,rover4,waypoint18) (at_soil_sample,waypoint18) (empty,rover4store))
  :effect (and (full,rover4store) (have_soil_analysis,rover4,waypoint18) (not (at_soil_sample,waypoint18)) (not (empty,rover4store))))
 (:action sample_soil,rover4,rover4store,waypoint19
  :precondition (and (at,rover4,waypoint19) (at_soil_sample,waypoint19) (empty,rover4store))
  :effect (and (full,rover4store) (have_soil_analysis,rover4,waypoint19) (not (at_soil_sample,waypoint19)) (not (empty,rover4store))))
 (:action sample_rock,rover0,rover0store,waypoint1
  :precondition (and (at,rover0,waypoint1) (empty,rover0store) (at_rock_sample,waypoint1))
  :effect (and (full,rover0store) (have_rock_analysis,rover0,waypoint1) (not (empty,rover0store)) (not (at_rock_sample,waypoint1))))
 (:action sample_rock,rover0,rover0store,waypoint2
  :precondition (and (at,rover0,waypoint2) (empty,rover0store) (at_rock_sample,waypoint2))
  :effect (and (full,rover0store) (have_rock_analysis,rover0,waypoint2) (not (empty,rover0store)) (not (at_rock_sample,waypoint2))))
 (:action sample_rock,rover0,rover0store,waypoint4
  :precondition (and (at,rover0,waypoint4) (empty,rover0store) (at_rock_sample,waypoint4))
  :effect (and (full,rover0store) (have_rock_analysis,rover0,waypoint4) (not (empty,rover0store)) (not (at_rock_sample,waypoint4))))
 (:action sample_rock,rover0,rover0store,waypoint5
  :precondition (and (at,rover0,waypoint5) (empty,rover0store) (at_rock_sample,waypoint5))
  :effect (and (full,rover0store) (have_rock_analysis,rover0,waypoint5) (not (empty,rover0store)) (not (at_rock_sample,waypoint5))))
 (:action sample_rock,rover0,rover0store,waypoint6
  :precondition (and (at,rover0,waypoint6) (empty,rover0store) (at_rock_sample,waypoint6))
  :effect (and (full,rover0store) (have_rock_analysis,rover0,waypoint6) (not (empty,rover0store)) (not (at_rock_sample,waypoint6))))
 (:action sample_rock,rover0,rover0store,waypoint7
  :precondition (and (at,rover0,waypoint7) (empty,rover0store) (at_rock_sample,waypoint7))
  :effect (and (full,rover0store) (have_rock_analysis,rover0,waypoint7) (not (empty,rover0store)) (not (at_rock_sample,waypoint7))))
 (:action sample_rock,rover0,rover0store,waypoint9
  :precondition (and (at,rover0,waypoint9) (empty,rover0store) (at_rock_sample,waypoint9))
  :effect (and (full,rover0store) (have_rock_analysis,rover0,waypoint9) (not (empty,rover0store)) (not (at_rock_sample,waypoint9))))
 (:action sample_rock,rover0,rover0store,waypoint12
  :precondition (and (at,rover0,waypoint12) (empty,rover0store) (at_rock_sample,waypoint12))
  :effect (and (full,rover0store) (have_rock_analysis,rover0,waypoint12) (not (empty,rover0store)) (not (at_rock_sample,waypoint12))))
 (:action sample_rock,rover0,rover0store,waypoint13
  :precondition (and (at,rover0,waypoint13) (empty,rover0store) (at_rock_sample,waypoint13))
  :effect (and (full,rover0store) (have_rock_analysis,rover0,waypoint13) (not (empty,rover0store)) (not (at_rock_sample,waypoint13))))
 (:action sample_rock,rover0,rover0store,waypoint18
  :precondition (and (at,rover0,waypoint18) (empty,rover0store) (at_rock_sample,waypoint18))
  :effect (and (full,rover0store) (have_rock_analysis,rover0,waypoint18) (not (empty,rover0store)) (not (at_rock_sample,waypoint18))))
 (:action sample_rock,rover1,rover1store,waypoint1
  :precondition (and (at,rover1,waypoint1) (at_rock_sample,waypoint1) (empty,rover1store))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint1) (not (at_rock_sample,waypoint1)) (not (empty,rover1store))))
 (:action sample_rock,rover1,rover1store,waypoint2
  :precondition (and (at,rover1,waypoint2) (at_rock_sample,waypoint2) (empty,rover1store))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint2) (not (at_rock_sample,waypoint2)) (not (empty,rover1store))))
 (:action sample_rock,rover1,rover1store,waypoint4
  :precondition (and (at,rover1,waypoint4) (at_rock_sample,waypoint4) (empty,rover1store))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint4) (not (at_rock_sample,waypoint4)) (not (empty,rover1store))))
 (:action sample_rock,rover1,rover1store,waypoint5
  :precondition (and (at,rover1,waypoint5) (at_rock_sample,waypoint5) (empty,rover1store))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint5) (not (at_rock_sample,waypoint5)) (not (empty,rover1store))))
 (:action sample_rock,rover1,rover1store,waypoint6
  :precondition (and (at,rover1,waypoint6) (at_rock_sample,waypoint6) (empty,rover1store))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint6) (not (at_rock_sample,waypoint6)) (not (empty,rover1store))))
 (:action sample_rock,rover1,rover1store,waypoint7
  :precondition (and (at,rover1,waypoint7) (at_rock_sample,waypoint7) (empty,rover1store))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint7) (not (at_rock_sample,waypoint7)) (not (empty,rover1store))))
 (:action sample_rock,rover1,rover1store,waypoint9
  :precondition (and (at,rover1,waypoint9) (at_rock_sample,waypoint9) (empty,rover1store))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint9) (not (at_rock_sample,waypoint9)) (not (empty,rover1store))))
 (:action sample_rock,rover1,rover1store,waypoint12
  :precondition (and (at,rover1,waypoint12) (at_rock_sample,waypoint12) (empty,rover1store))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint12) (not (at_rock_sample,waypoint12)) (not (empty,rover1store))))
 (:action sample_rock,rover1,rover1store,waypoint13
  :precondition (and (at,rover1,waypoint13) (at_rock_sample,waypoint13) (empty,rover1store))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint13) (not (at_rock_sample,waypoint13)) (not (empty,rover1store))))
 (:action sample_rock,rover1,rover1store,waypoint17
  :precondition (and (at,rover1,waypoint17) (at_rock_sample,waypoint17) (empty,rover1store))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint17) (not (at_rock_sample,waypoint17)) (not (empty,rover1store))))
 (:action sample_rock,rover1,rover1store,waypoint18
  :precondition (and (at,rover1,waypoint18) (at_rock_sample,waypoint18) (empty,rover1store))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint18) (not (at_rock_sample,waypoint18)) (not (empty,rover1store))))
 (:action sample_rock,rover3,rover3store,waypoint1
  :precondition (and (at,rover3,waypoint1) (at_rock_sample,waypoint1) (empty,rover3store))
  :effect (and (full,rover3store) (have_rock_analysis,rover3,waypoint1) (not (at_rock_sample,waypoint1)) (not (empty,rover3store))))
 (:action sample_rock,rover3,rover3store,waypoint2
  :precondition (and (at,rover3,waypoint2) (at_rock_sample,waypoint2) (empty,rover3store))
  :effect (and (full,rover3store) (have_rock_analysis,rover3,waypoint2) (not (at_rock_sample,waypoint2)) (not (empty,rover3store))))
 (:action sample_rock,rover3,rover3store,waypoint4
  :precondition (and (at,rover3,waypoint4) (at_rock_sample,waypoint4) (empty,rover3store))
  :effect (and (full,rover3store) (have_rock_analysis,rover3,waypoint4) (not (at_rock_sample,waypoint4)) (not (empty,rover3store))))
 (:action sample_rock,rover3,rover3store,waypoint5
  :precondition (and (at,rover3,waypoint5) (at_rock_sample,waypoint5) (empty,rover3store))
  :effect (and (full,rover3store) (have_rock_analysis,rover3,waypoint5) (not (at_rock_sample,waypoint5)) (not (empty,rover3store))))
 (:action sample_rock,rover3,rover3store,waypoint6
  :precondition (and (at,rover3,waypoint6) (at_rock_sample,waypoint6) (empty,rover3store))
  :effect (and (full,rover3store) (have_rock_analysis,rover3,waypoint6) (not (at_rock_sample,waypoint6)) (not (empty,rover3store))))
 (:action sample_rock,rover3,rover3store,waypoint7
  :precondition (and (at,rover3,waypoint7) (at_rock_sample,waypoint7) (empty,rover3store))
  :effect (and (full,rover3store) (have_rock_analysis,rover3,waypoint7) (not (at_rock_sample,waypoint7)) (not (empty,rover3store))))
 (:action sample_rock,rover3,rover3store,waypoint9
  :precondition (and (at,rover3,waypoint9) (at_rock_sample,waypoint9) (empty,rover3store))
  :effect (and (full,rover3store) (have_rock_analysis,rover3,waypoint9) (not (at_rock_sample,waypoint9)) (not (empty,rover3store))))
 (:action sample_rock,rover3,rover3store,waypoint12
  :precondition (and (at,rover3,waypoint12) (at_rock_sample,waypoint12) (empty,rover3store))
  :effect (and (full,rover3store) (have_rock_analysis,rover3,waypoint12) (not (at_rock_sample,waypoint12)) (not (empty,rover3store))))
 (:action sample_rock,rover3,rover3store,waypoint13
  :precondition (and (at,rover3,waypoint13) (at_rock_sample,waypoint13) (empty,rover3store))
  :effect (and (full,rover3store) (have_rock_analysis,rover3,waypoint13) (not (at_rock_sample,waypoint13)) (not (empty,rover3store))))
 (:action sample_rock,rover3,rover3store,waypoint17
  :precondition (and (at,rover3,waypoint17) (at_rock_sample,waypoint17) (empty,rover3store))
  :effect (and (full,rover3store) (have_rock_analysis,rover3,waypoint17) (not (at_rock_sample,waypoint17)) (not (empty,rover3store))))
 (:action sample_rock,rover3,rover3store,waypoint18
  :precondition (and (at,rover3,waypoint18) (at_rock_sample,waypoint18) (empty,rover3store))
  :effect (and (full,rover3store) (have_rock_analysis,rover3,waypoint18) (not (at_rock_sample,waypoint18)) (not (empty,rover3store))))
 (:action sample_rock,rover5,rover5store,waypoint1
  :precondition (and (at,rover5,waypoint1) (at_rock_sample,waypoint1) (empty,rover5store))
  :effect (and (full,rover5store) (have_rock_analysis,rover5,waypoint1) (not (at_rock_sample,waypoint1)) (not (empty,rover5store))))
 (:action sample_rock,rover5,rover5store,waypoint2
  :precondition (and (at,rover5,waypoint2) (at_rock_sample,waypoint2) (empty,rover5store))
  :effect (and (full,rover5store) (have_rock_analysis,rover5,waypoint2) (not (at_rock_sample,waypoint2)) (not (empty,rover5store))))
 (:action sample_rock,rover5,rover5store,waypoint4
  :precondition (and (at,rover5,waypoint4) (at_rock_sample,waypoint4) (empty,rover5store))
  :effect (and (full,rover5store) (have_rock_analysis,rover5,waypoint4) (not (at_rock_sample,waypoint4)) (not (empty,rover5store))))
 (:action sample_rock,rover5,rover5store,waypoint5
  :precondition (and (at,rover5,waypoint5) (at_rock_sample,waypoint5) (empty,rover5store))
  :effect (and (full,rover5store) (have_rock_analysis,rover5,waypoint5) (not (at_rock_sample,waypoint5)) (not (empty,rover5store))))
 (:action sample_rock,rover5,rover5store,waypoint6
  :precondition (and (at,rover5,waypoint6) (at_rock_sample,waypoint6) (empty,rover5store))
  :effect (and (full,rover5store) (have_rock_analysis,rover5,waypoint6) (not (at_rock_sample,waypoint6)) (not (empty,rover5store))))
 (:action sample_rock,rover5,rover5store,waypoint7
  :precondition (and (at,rover5,waypoint7) (at_rock_sample,waypoint7) (empty,rover5store))
  :effect (and (full,rover5store) (have_rock_analysis,rover5,waypoint7) (not (at_rock_sample,waypoint7)) (not (empty,rover5store))))
 (:action sample_rock,rover5,rover5store,waypoint9
  :precondition (and (at,rover5,waypoint9) (at_rock_sample,waypoint9) (empty,rover5store))
  :effect (and (full,rover5store) (have_rock_analysis,rover5,waypoint9) (not (at_rock_sample,waypoint9)) (not (empty,rover5store))))
 (:action sample_rock,rover5,rover5store,waypoint12
  :precondition (and (at,rover5,waypoint12) (at_rock_sample,waypoint12) (empty,rover5store))
  :effect (and (full,rover5store) (have_rock_analysis,rover5,waypoint12) (not (at_rock_sample,waypoint12)) (not (empty,rover5store))))
 (:action sample_rock,rover5,rover5store,waypoint13
  :precondition (and (at,rover5,waypoint13) (at_rock_sample,waypoint13) (empty,rover5store))
  :effect (and (full,rover5store) (have_rock_analysis,rover5,waypoint13) (not (at_rock_sample,waypoint13)) (not (empty,rover5store))))
 (:action sample_rock,rover5,rover5store,waypoint17
  :precondition (and (at,rover5,waypoint17) (at_rock_sample,waypoint17) (empty,rover5store))
  :effect (and (full,rover5store) (have_rock_analysis,rover5,waypoint17) (not (at_rock_sample,waypoint17)) (not (empty,rover5store))))
 (:action sample_rock,rover5,rover5store,waypoint18
  :precondition (and (at,rover5,waypoint18) (at_rock_sample,waypoint18) (empty,rover5store))
  :effect (and (full,rover5store) (have_rock_analysis,rover5,waypoint18) (not (at_rock_sample,waypoint18)) (not (empty,rover5store))))
 (:action drop,rover0,rover0store
  :precondition (full,rover0store)
  :effect (and (empty,rover0store) (not (full,rover0store))))
 (:action drop,rover1,rover1store
  :precondition (full,rover1store)
  :effect (and (empty,rover1store) (not (full,rover1store))))
 (:action drop,rover2,rover2store
  :precondition (and (full,rover2store) (first_o14))
  :effect (and (empty,rover2store) (not (full,rover2store))))
 (:action drop,rover3,rover3store
  :precondition (full,rover3store)
  :effect (and (empty,rover3store) (not (full,rover3store))))
 (:action drop,rover4,rover4store
  :precondition (full,rover4store)
  :effect (and (empty,rover4store) (not (full,rover4store))))
 (:action drop,rover5,rover5store
  :precondition (full,rover5store)
  :effect (and (empty,rover5store) (not (full,rover5store))))
 (:action calibrate,rover0,camera5,objective2,waypoint0
  :precondition (at,rover0,waypoint0)
  :effect (calibrated,camera5,rover0))
 (:action calibrate,rover0,camera5,objective2,waypoint1
  :precondition (at,rover0,waypoint1)
  :effect (calibrated,camera5,rover0))
 (:action calibrate,rover0,camera5,objective2,waypoint2
  :precondition (at,rover0,waypoint2)
  :effect (calibrated,camera5,rover0))
 (:action calibrate,rover0,camera5,objective2,waypoint3
  :precondition (at,rover0,waypoint3)
  :effect (calibrated,camera5,rover0))
 (:action calibrate,rover0,camera5,objective2,waypoint4
  :precondition (at,rover0,waypoint4)
  :effect (calibrated,camera5,rover0))
 (:action calibrate,rover0,camera5,objective2,waypoint5
  :precondition (at,rover0,waypoint5)
  :effect (calibrated,camera5,rover0))
 (:action calibrate,rover0,camera5,objective2,waypoint6
  :precondition (at,rover0,waypoint6)
  :effect (calibrated,camera5,rover0))
 (:action calibrate,rover0,camera5,objective2,waypoint7
  :precondition (at,rover0,waypoint7)
  :effect (calibrated,camera5,rover0))
 (:action calibrate,rover0,camera5,objective2,waypoint8
  :precondition (at,rover0,waypoint8)
  :effect (calibrated,camera5,rover0))
 (:action calibrate,rover0,camera5,objective2,waypoint9
  :precondition (at,rover0,waypoint9)
  :effect (calibrated,camera5,rover0))
 (:action calibrate,rover0,camera5,objective2,waypoint10
  :precondition (at,rover0,waypoint10)
  :effect (calibrated,camera5,rover0))
 (:action calibrate,rover0,camera5,objective2,waypoint11
  :precondition (at,rover0,waypoint11)
  :effect (calibrated,camera5,rover0))
 (:action calibrate,rover0,camera5,objective2,waypoint12
  :precondition (at,rover0,waypoint12)
  :effect (calibrated,camera5,rover0))
 (:action calibrate,rover0,camera5,objective2,waypoint13
  :precondition (at,rover0,waypoint13)
  :effect (calibrated,camera5,rover0))
 (:action calibrate,rover0,camera5,objective2,waypoint14
  :precondition (at,rover0,waypoint14)
  :effect (calibrated,camera5,rover0))
 (:action calibrate,rover1,camera0,objective4,waypoint0
  :precondition (at,rover1,waypoint0)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective4,waypoint1
  :precondition (at,rover1,waypoint1)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective4,waypoint2
  :precondition (at,rover1,waypoint2)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective4,waypoint3
  :precondition (at,rover1,waypoint3)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective4,waypoint4
  :precondition (at,rover1,waypoint4)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective4,waypoint5
  :precondition (at,rover1,waypoint5)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective4,waypoint6
  :precondition (at,rover1,waypoint6)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective4,waypoint7
  :precondition (at,rover1,waypoint7)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective4,waypoint8
  :precondition (at,rover1,waypoint8)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective4,waypoint9
  :precondition (at,rover1,waypoint9)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective4,waypoint10
  :precondition (at,rover1,waypoint10)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective4,waypoint11
  :precondition (at,rover1,waypoint11)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective4,waypoint12
  :precondition (at,rover1,waypoint12)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective4,waypoint13
  :precondition (at,rover1,waypoint13)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective4,waypoint14
  :precondition (at,rover1,waypoint14)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover1,camera0,objective4,waypoint15
  :precondition (at,rover1,waypoint15)
  :effect (calibrated,camera0,rover1))
 (:action calibrate,rover2,camera1,objective6,waypoint0
  :precondition (at,rover2,waypoint0)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover2,camera1,objective6,waypoint1
  :precondition (at,rover2,waypoint1)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover2,camera1,objective6,waypoint2
  :precondition (at,rover2,waypoint2)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover2,camera1,objective6,waypoint3
  :precondition (at,rover2,waypoint3)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover2,camera1,objective6,waypoint4
  :precondition (at,rover2,waypoint4)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover2,camera1,objective6,waypoint5
  :precondition (at,rover2,waypoint5)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover2,camera1,objective6,waypoint6
  :precondition (at,rover2,waypoint6)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover2,camera1,objective6,waypoint7
  :precondition (at,rover2,waypoint7)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover2,camera1,objective6,waypoint9
  :precondition (at,rover2,waypoint9)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover2,camera1,objective6,waypoint10
  :precondition (at,rover2,waypoint10)
  :effect (calibrated,camera1,rover2))
 (:action calibrate,rover3,camera3,objective6,waypoint0
  :precondition (at,rover3,waypoint0)
  :effect (calibrated,camera3,rover3))
 (:action calibrate,rover3,camera3,objective6,waypoint1
  :precondition (at,rover3,waypoint1)
  :effect (calibrated,camera3,rover3))
 (:action calibrate,rover3,camera3,objective6,waypoint2
  :precondition (at,rover3,waypoint2)
  :effect (calibrated,camera3,rover3))
 (:action calibrate,rover3,camera3,objective6,waypoint3
  :precondition (at,rover3,waypoint3)
  :effect (calibrated,camera3,rover3))
 (:action calibrate,rover3,camera3,objective6,waypoint4
  :precondition (at,rover3,waypoint4)
  :effect (calibrated,camera3,rover3))
 (:action calibrate,rover3,camera3,objective6,waypoint5
  :precondition (at,rover3,waypoint5)
  :effect (calibrated,camera3,rover3))
 (:action calibrate,rover3,camera3,objective6,waypoint6
  :precondition (at,rover3,waypoint6)
  :effect (calibrated,camera3,rover3))
 (:action calibrate,rover3,camera3,objective6,waypoint7
  :precondition (at,rover3,waypoint7)
  :effect (calibrated,camera3,rover3))
 (:action calibrate,rover3,camera3,objective6,waypoint8
  :precondition (at,rover3,waypoint8)
  :effect (calibrated,camera3,rover3))
 (:action calibrate,rover3,camera3,objective6,waypoint9
  :precondition (at,rover3,waypoint9)
  :effect (calibrated,camera3,rover3))
 (:action calibrate,rover3,camera3,objective6,waypoint10
  :precondition (at,rover3,waypoint10)
  :effect (calibrated,camera3,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint0
  :precondition (at,rover3,waypoint0)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint1
  :precondition (at,rover3,waypoint1)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint2
  :precondition (at,rover3,waypoint2)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint3
  :precondition (at,rover3,waypoint3)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint4
  :precondition (at,rover3,waypoint4)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint5
  :precondition (at,rover3,waypoint5)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint6
  :precondition (at,rover3,waypoint6)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint7
  :precondition (at,rover3,waypoint7)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint8
  :precondition (at,rover3,waypoint8)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint9
  :precondition (at,rover3,waypoint9)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint10
  :precondition (at,rover3,waypoint10)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint11
  :precondition (at,rover3,waypoint11)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint12
  :precondition (at,rover3,waypoint12)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint13
  :precondition (at,rover3,waypoint13)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint14
  :precondition (at,rover3,waypoint14)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover3,camera4,objective4,waypoint15
  :precondition (at,rover3,waypoint15)
  :effect (calibrated,camera4,rover3))
 (:action calibrate,rover4,camera2,objective0,waypoint0
  :precondition (at,rover4,waypoint0)
  :effect (calibrated,camera2,rover4))
 (:action calibrate,rover4,camera2,objective0,waypoint1
  :precondition (at,rover4,waypoint1)
  :effect (calibrated,camera2,rover4))
 (:action calibrate,rover4,camera2,objective0,waypoint2
  :precondition (at,rover4,waypoint2)
  :effect (calibrated,camera2,rover4))
 (:action calibrate,rover4,camera2,objective0,waypoint3
  :precondition (at,rover4,waypoint3)
  :effect (calibrated,camera2,rover4))
 (:action calibrate,rover4,camera2,objective0,waypoint4
  :precondition (at,rover4,waypoint4)
  :effect (calibrated,camera2,rover4))
 (:action calibrate,rover4,camera2,objective0,waypoint5
  :precondition (at,rover4,waypoint5)
  :effect (calibrated,camera2,rover4))
 (:action calibrate,rover4,camera2,objective0,waypoint6
  :precondition (at,rover4,waypoint6)
  :effect (calibrated,camera2,rover4))
 (:action calibrate,rover4,camera2,objective0,waypoint7
  :precondition (at,rover4,waypoint7)
  :effect (calibrated,camera2,rover4))
 (:action calibrate,rover4,camera2,objective0,waypoint8
  :precondition (at,rover4,waypoint8)
  :effect (calibrated,camera2,rover4))
 (:action calibrate,rover4,camera2,objective0,waypoint9
  :precondition (at,rover4,waypoint9)
  :effect (calibrated,camera2,rover4))
 (:action calibrate,rover4,camera2,objective0,waypoint10
  :precondition (at,rover4,waypoint10)
  :effect (calibrated,camera2,rover4))
 (:action calibrate,rover5,camera6,objective6,waypoint0
  :precondition (at,rover5,waypoint0)
  :effect (calibrated,camera6,rover5))
 (:action calibrate,rover5,camera6,objective6,waypoint1
  :precondition (at,rover5,waypoint1)
  :effect (calibrated,camera6,rover5))
 (:action calibrate,rover5,camera6,objective6,waypoint2
  :precondition (at,rover5,waypoint2)
  :effect (calibrated,camera6,rover5))
 (:action calibrate,rover5,camera6,objective6,waypoint3
  :precondition (at,rover5,waypoint3)
  :effect (calibrated,camera6,rover5))
 (:action calibrate,rover5,camera6,objective6,waypoint4
  :precondition (at,rover5,waypoint4)
  :effect (calibrated,camera6,rover5))
 (:action calibrate,rover5,camera6,objective6,waypoint5
  :precondition (at,rover5,waypoint5)
  :effect (calibrated,camera6,rover5))
 (:action calibrate,rover5,camera6,objective6,waypoint6
  :precondition (at,rover5,waypoint6)
  :effect (calibrated,camera6,rover5))
 (:action calibrate,rover5,camera6,objective6,waypoint7
  :precondition (at,rover5,waypoint7)
  :effect (calibrated,camera6,rover5))
 (:action calibrate,rover5,camera6,objective6,waypoint8
  :precondition (at,rover5,waypoint8)
  :effect (calibrated,camera6,rover5))
 (:action calibrate,rover5,camera6,objective6,waypoint9
  :precondition (at,rover5,waypoint9)
  :effect (calibrated,camera6,rover5))
 (:action calibrate,rover5,camera6,objective6,waypoint10
  :precondition (at,rover5,waypoint10)
  :effect (calibrated,camera6,rover5))
 (:action take_image,rover0,waypoint0,objective0,camera5,colour
  :precondition (and (at,rover0,waypoint0) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint0,objective1,camera5,colour
  :precondition (and (at,rover0,waypoint0) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint0,objective2,camera5,colour
  :precondition (and (at,rover0,waypoint0) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint0,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint0) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint0,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint0) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint0,objective5,camera5,colour
  :precondition (and (at,rover0,waypoint0) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective5,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint0,objective6,camera5,colour
  :precondition (and (at,rover0,waypoint0) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective6,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint1,objective0,camera5,colour
  :precondition (and (at,rover0,waypoint1) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint1,objective1,camera5,colour
  :precondition (and (at,rover0,waypoint1) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint1,objective2,camera5,colour
  :precondition (and (at,rover0,waypoint1) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint1,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint1) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint1,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint1) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint1,objective5,camera5,colour
  :precondition (and (at,rover0,waypoint1) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective5,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint1,objective6,camera5,colour
  :precondition (and (at,rover0,waypoint1) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective6,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint2,objective0,camera5,colour
  :precondition (and (at,rover0,waypoint2) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint2,objective1,camera5,colour
  :precondition (and (at,rover0,waypoint2) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint2,objective2,camera5,colour
  :precondition (and (at,rover0,waypoint2) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint2,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint2) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint2,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint2) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint2,objective5,camera5,colour
  :precondition (and (at,rover0,waypoint2) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective5,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint2,objective6,camera5,colour
  :precondition (and (at,rover0,waypoint2) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective6,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint3,objective0,camera5,colour
  :precondition (and (at,rover0,waypoint3) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint3,objective1,camera5,colour
  :precondition (and (at,rover0,waypoint3) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint3,objective2,camera5,colour
  :precondition (and (at,rover0,waypoint3) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint3,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint3) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint3,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint3) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint3,objective6,camera5,colour
  :precondition (and (at,rover0,waypoint3) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective6,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint4,objective0,camera5,colour
  :precondition (and (at,rover0,waypoint4) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint4,objective1,camera5,colour
  :precondition (and (at,rover0,waypoint4) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint4,objective2,camera5,colour
  :precondition (and (at,rover0,waypoint4) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint4,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint4) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint4,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint4) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint4,objective6,camera5,colour
  :precondition (and (at,rover0,waypoint4) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective6,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint5,objective0,camera5,colour
  :precondition (and (at,rover0,waypoint5) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint5,objective1,camera5,colour
  :precondition (and (at,rover0,waypoint5) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint5,objective2,camera5,colour
  :precondition (and (at,rover0,waypoint5) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint5,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint5) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint5,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint5) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint5,objective6,camera5,colour
  :precondition (and (at,rover0,waypoint5) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective6,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint6,objective0,camera5,colour
  :precondition (and (at,rover0,waypoint6) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint6,objective1,camera5,colour
  :precondition (and (at,rover0,waypoint6) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint6,objective2,camera5,colour
  :precondition (and (at,rover0,waypoint6) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint6,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint6) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint6,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint6) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint6,objective6,camera5,colour
  :precondition (and (at,rover0,waypoint6) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective6,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint7,objective0,camera5,colour
  :precondition (and (at,rover0,waypoint7) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint7,objective1,camera5,colour
  :precondition (and (at,rover0,waypoint7) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint7,objective2,camera5,colour
  :precondition (and (at,rover0,waypoint7) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint7,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint7) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint7,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint7) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint7,objective6,camera5,colour
  :precondition (and (at,rover0,waypoint7) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective6,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint8,objective0,camera5,colour
  :precondition (and (at,rover0,waypoint8) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint8,objective1,camera5,colour
  :precondition (and (at,rover0,waypoint8) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint8,objective2,camera5,colour
  :precondition (and (at,rover0,waypoint8) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint8,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint8) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint8,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint8) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint8,objective6,camera5,colour
  :precondition (and (at,rover0,waypoint8) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective6,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint9,objective0,camera5,colour
  :precondition (and (at,rover0,waypoint9) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint9,objective1,camera5,colour
  :precondition (and (at,rover0,waypoint9) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint9,objective2,camera5,colour
  :precondition (and (at,rover0,waypoint9) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint9,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint9) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint9,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint9) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint9,objective6,camera5,colour
  :precondition (and (at,rover0,waypoint9) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective6,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint10,objective0,camera5,colour
  :precondition (and (at,rover0,waypoint10) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective0,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint10,objective1,camera5,colour
  :precondition (and (at,rover0,waypoint10) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint10,objective2,camera5,colour
  :precondition (and (at,rover0,waypoint10) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint10,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint10) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint10,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint10) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint10,objective6,camera5,colour
  :precondition (and (at,rover0,waypoint10) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective6,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint11,objective1,camera5,colour
  :precondition (and (at,rover0,waypoint11) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint11,objective2,camera5,colour
  :precondition (and (at,rover0,waypoint11) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint11,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint11) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint11,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint11) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint12,objective1,camera5,colour
  :precondition (and (at,rover0,waypoint12) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective1,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint12,objective2,camera5,colour
  :precondition (and (at,rover0,waypoint12) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint12,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint12) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint12,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint12) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint13,objective2,camera5,colour
  :precondition (and (at,rover0,waypoint13) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint13,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint13) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint13,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint13) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint14,objective2,camera5,colour
  :precondition (and (at,rover0,waypoint14) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective2,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint14,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint14) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint14,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint14) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint15,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint15) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint15,objective4,camera5,colour
  :precondition (and (at,rover0,waypoint15) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective4,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint16,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint16) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint18,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint18) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover0,waypoint19,objective3,camera5,colour
  :precondition (and (at,rover0,waypoint19) (calibrated,camera5,rover0))
  :effect (and (have_image,rover0,objective3,colour) (not (calibrated,camera5,rover0))))
 (:action take_image,rover1,waypoint0,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint0,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint0,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint0,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint0,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint0,objective5,camera0,high_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective5,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint0,objective6,camera0,high_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective6,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint1,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint1,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint1,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint1,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint1,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint1,objective5,camera0,high_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective5,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint1,objective6,camera0,high_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective6,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint2,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint2,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint2,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint2,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint2,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint2,objective5,camera0,high_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective5,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint2,objective6,camera0,high_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective6,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint3,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint3) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint3,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint3) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint3,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint3) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint3,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint3) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint3,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint3) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint3,objective6,camera0,high_res
  :precondition (and (at,rover1,waypoint3) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective6,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint4,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint4) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint4,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint4) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint4,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint4) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint4,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint4) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint4,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint4) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint4,objective6,camera0,high_res
  :precondition (and (at,rover1,waypoint4) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective6,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint5,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint5) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint5,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint5) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint5,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint5) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint5,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint5) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint5,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint5) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint5,objective6,camera0,high_res
  :precondition (and (at,rover1,waypoint5) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective6,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint6,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint6) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint6,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint6) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint6,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint6) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint6,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint6) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint6,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint6) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint6,objective6,camera0,high_res
  :precondition (and (at,rover1,waypoint6) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective6,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint7,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint7) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint7,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint7) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint7,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint7) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint7,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint7) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint7,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint7) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint7,objective6,camera0,high_res
  :precondition (and (at,rover1,waypoint7) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective6,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint8,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint8) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint8,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint8) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint8,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint8) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint8,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint8) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint8,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint8) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint8,objective6,camera0,high_res
  :precondition (and (at,rover1,waypoint8) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective6,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint9,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint9) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint9,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint9) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint9,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint9) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint9,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint9) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint9,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint9) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint9,objective6,camera0,high_res
  :precondition (and (at,rover1,waypoint9) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective6,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint10,objective0,camera0,high_res
  :precondition (and (at,rover1,waypoint10) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint10,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint10) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint10,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint10) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint10,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint10) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint10,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint10) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint10,objective6,camera0,high_res
  :precondition (and (at,rover1,waypoint10) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective6,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint11,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint11) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint11,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint11) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint11,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint11) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint11,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint11) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint12,objective1,camera0,high_res
  :precondition (and (at,rover1,waypoint12) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint12,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint12) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint12,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint12) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint12,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint12) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint13,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint13) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint13,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint13) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint13,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint13) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint14,objective2,camera0,high_res
  :precondition (and (at,rover1,waypoint14) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint14,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint14) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint14,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint14) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint15,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint15) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint15,objective4,camera0,high_res
  :precondition (and (at,rover1,waypoint15) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective4,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint16,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint16) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint17,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint17) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint18,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint18) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover1,waypoint19,objective3,camera0,high_res
  :precondition (and (at,rover1,waypoint19) (calibrated,camera0,rover1))
  :effect (and (have_image,rover1,objective3,high_res) (not (calibrated,camera0,rover1))))
 (:action take_image,rover2,waypoint0,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective0,camera1,low_res
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective1,camera1,colour
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective1,camera1,low_res
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective2,camera1,low_res
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective4,camera1,colour
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective4,camera1,low_res
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective5,camera1,colour
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective5,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective5,camera1,low_res
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective5,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective6,camera1,colour
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint0,objective6,camera1,low_res
  :precondition (and (at,rover2,waypoint0) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective0,camera1,low_res
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective1,camera1,colour
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective1,camera1,low_res
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective2,camera1,low_res
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective4,camera1,colour
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective4,camera1,low_res
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective5,camera1,colour
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective5,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective5,camera1,low_res
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective5,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective6,camera1,colour
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint1,objective6,camera1,low_res
  :precondition (and (at,rover2,waypoint1) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective0,camera1,low_res
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective1,camera1,colour
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective1,camera1,low_res
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective2,camera1,low_res
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective4,camera1,colour
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective4,camera1,low_res
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective5,camera1,colour
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective5,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective5,camera1,low_res
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective5,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective6,camera1,colour
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint2,objective6,camera1,low_res
  :precondition (and (at,rover2,waypoint2) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective0,camera1,low_res
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective1,camera1,colour
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective1,camera1,low_res
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective2,camera1,low_res
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective4,camera1,colour
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective4,camera1,low_res
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective6,camera1,colour
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint3,objective6,camera1,low_res
  :precondition (and (at,rover2,waypoint3) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective0,camera1,low_res
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective1,camera1,colour
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective1,camera1,low_res
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective2,camera1,low_res
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective4,camera1,colour
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective4,camera1,low_res
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective6,camera1,colour
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint4,objective6,camera1,low_res
  :precondition (and (at,rover2,waypoint4) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective0,camera1,low_res
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective1,camera1,colour
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective1,camera1,low_res
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective2,camera1,low_res
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective4,camera1,colour
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective4,camera1,low_res
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective6,camera1,colour
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint5,objective6,camera1,low_res
  :precondition (and (at,rover2,waypoint5) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint6,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint6) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint6,objective0,camera1,low_res
  :precondition (and (at,rover2,waypoint6) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint6,objective1,camera1,colour
  :precondition (and (at,rover2,waypoint6) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint6,objective1,camera1,low_res
  :precondition (and (at,rover2,waypoint6) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint6,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint6) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint6,objective2,camera1,low_res
  :precondition (and (at,rover2,waypoint6) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint6,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint6) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint6,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint6) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint6,objective4,camera1,colour
  :precondition (and (at,rover2,waypoint6) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint6,objective4,camera1,low_res
  :precondition (and (at,rover2,waypoint6) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint6,objective6,camera1,colour
  :precondition (and (at,rover2,waypoint6) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint6,objective6,camera1,low_res
  :precondition (and (at,rover2,waypoint6) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint7,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint7) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint7,objective0,camera1,low_res
  :precondition (and (at,rover2,waypoint7) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint7,objective1,camera1,colour
  :precondition (and (at,rover2,waypoint7) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint7,objective1,camera1,low_res
  :precondition (and (at,rover2,waypoint7) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint7,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint7) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint7,objective2,camera1,low_res
  :precondition (and (at,rover2,waypoint7) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint7,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint7) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint7,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint7) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint7,objective4,camera1,colour
  :precondition (and (at,rover2,waypoint7) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint7,objective4,camera1,low_res
  :precondition (and (at,rover2,waypoint7) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint7,objective6,camera1,colour
  :precondition (and (at,rover2,waypoint7) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint7,objective6,camera1,low_res
  :precondition (and (at,rover2,waypoint7) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint9,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint9) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint9,objective0,camera1,low_res
  :precondition (and (at,rover2,waypoint9) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint9,objective1,camera1,colour
  :precondition (and (at,rover2,waypoint9) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint9,objective1,camera1,low_res
  :precondition (and (at,rover2,waypoint9) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint9,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint9) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint9,objective2,camera1,low_res
  :precondition (and (at,rover2,waypoint9) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint9,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint9) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint9,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint9) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint9,objective4,camera1,colour
  :precondition (and (at,rover2,waypoint9) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint9,objective4,camera1,low_res
  :precondition (and (at,rover2,waypoint9) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint9,objective6,camera1,colour
  :precondition (and (at,rover2,waypoint9) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint9,objective6,camera1,low_res
  :precondition (and (at,rover2,waypoint9) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint10,objective0,camera1,colour
  :precondition (and (at,rover2,waypoint10) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint10,objective0,camera1,low_res
  :precondition (and (at,rover2,waypoint10) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective0,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint10,objective1,camera1,colour
  :precondition (and (at,rover2,waypoint10) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint10,objective1,camera1,low_res
  :precondition (and (at,rover2,waypoint10) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint10,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint10) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint10,objective2,camera1,low_res
  :precondition (and (at,rover2,waypoint10) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint10,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint10) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint10,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint10) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint10,objective4,camera1,colour
  :precondition (and (at,rover2,waypoint10) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint10,objective4,camera1,low_res
  :precondition (and (at,rover2,waypoint10) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint10,objective6,camera1,colour
  :precondition (and (at,rover2,waypoint10) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint10,objective6,camera1,low_res
  :precondition (and (at,rover2,waypoint10) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective6,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint11,objective1,camera1,colour
  :precondition (and (at,rover2,waypoint11) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint11,objective1,camera1,low_res
  :precondition (and (at,rover2,waypoint11) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint11,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint11) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint11,objective2,camera1,low_res
  :precondition (and (at,rover2,waypoint11) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint11,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint11) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint11,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint11) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint11,objective4,camera1,colour
  :precondition (and (at,rover2,waypoint11) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint11,objective4,camera1,low_res
  :precondition (and (at,rover2,waypoint11) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint12,objective1,camera1,colour
  :precondition (and (at,rover2,waypoint12) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint12,objective1,camera1,low_res
  :precondition (and (at,rover2,waypoint12) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective1,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint12,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint12) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint12,objective2,camera1,low_res
  :precondition (and (at,rover2,waypoint12) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint12,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint12) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint12,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint12) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint12,objective4,camera1,colour
  :precondition (and (at,rover2,waypoint12) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint12,objective4,camera1,low_res
  :precondition (and (at,rover2,waypoint12) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint13,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint13) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint13,objective2,camera1,low_res
  :precondition (and (at,rover2,waypoint13) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint13,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint13) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint13,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint13) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint13,objective4,camera1,colour
  :precondition (and (at,rover2,waypoint13) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint13,objective4,camera1,low_res
  :precondition (and (at,rover2,waypoint13) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint14,objective2,camera1,colour
  :precondition (and (at,rover2,waypoint14) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint14,objective2,camera1,low_res
  :precondition (and (at,rover2,waypoint14) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint14,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint14) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint14,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint14) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint14,objective4,camera1,colour
  :precondition (and (at,rover2,waypoint14) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint14,objective4,camera1,low_res
  :precondition (and (at,rover2,waypoint14) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint15,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint15) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint15,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint15) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint15,objective4,camera1,colour
  :precondition (and (at,rover2,waypoint15) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint15,objective4,camera1,low_res
  :precondition (and (at,rover2,waypoint15) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective4,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint16,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint16) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint16,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint16) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint17,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint17) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint17,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint17) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint18,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint18) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint18,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint18) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint19,objective3,camera1,colour
  :precondition (and (at,rover2,waypoint19) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,colour) (not (calibrated,camera1,rover2))))
 (:action take_image,rover2,waypoint19,objective3,camera1,low_res
  :precondition (and (at,rover2,waypoint19) (calibrated,camera1,rover2))
  :effect (and (have_image,rover2,objective3,low_res) (not (calibrated,camera1,rover2))))
 (:action take_image,rover3,waypoint0,objective0,camera3,colour
  :precondition (and (at,rover3,waypoint0) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint0,objective0,camera4,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint0,objective0,camera4,low_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint0,objective1,camera3,colour
  :precondition (and (at,rover3,waypoint0) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective1,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint0,objective1,camera4,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint0,objective1,camera4,low_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint0,objective2,camera3,colour
  :precondition (and (at,rover3,waypoint0) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint0,objective2,camera4,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint0,objective2,camera4,low_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint0,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint0) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint0,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint0,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint0,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint0) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint0,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint0,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint0,objective5,camera3,colour
  :precondition (and (at,rover3,waypoint0) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective5,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint0,objective5,camera4,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective5,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint0,objective5,camera4,low_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective5,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint0,objective6,camera3,colour
  :precondition (and (at,rover3,waypoint0) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective6,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint0,objective6,camera4,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint0,objective6,camera4,low_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint1,objective0,camera3,colour
  :precondition (and (at,rover3,waypoint1) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint1,objective0,camera4,high_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint1,objective0,camera4,low_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint1,objective1,camera3,colour
  :precondition (and (at,rover3,waypoint1) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective1,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint1,objective1,camera4,high_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint1,objective1,camera4,low_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint1,objective2,camera3,colour
  :precondition (and (at,rover3,waypoint1) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint1,objective2,camera4,high_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint1,objective2,camera4,low_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint1,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint1) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint1,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint1,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint1,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint1) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint1,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint1,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint1,objective5,camera3,colour
  :precondition (and (at,rover3,waypoint1) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective5,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint1,objective5,camera4,high_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective5,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint1,objective5,camera4,low_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective5,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint1,objective6,camera3,colour
  :precondition (and (at,rover3,waypoint1) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective6,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint1,objective6,camera4,high_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint1,objective6,camera4,low_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint2,objective0,camera3,colour
  :precondition (and (at,rover3,waypoint2) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint2,objective0,camera4,high_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint2,objective0,camera4,low_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint2,objective1,camera3,colour
  :precondition (and (at,rover3,waypoint2) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective1,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint2,objective1,camera4,high_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint2,objective1,camera4,low_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint2,objective2,camera3,colour
  :precondition (and (at,rover3,waypoint2) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint2,objective2,camera4,high_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint2,objective2,camera4,low_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint2,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint2) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint2,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint2,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint2,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint2) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint2,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint2,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint2,objective5,camera3,colour
  :precondition (and (at,rover3,waypoint2) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective5,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint2,objective5,camera4,high_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective5,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint2,objective5,camera4,low_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective5,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint2,objective6,camera3,colour
  :precondition (and (at,rover3,waypoint2) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective6,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint2,objective6,camera4,high_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint2,objective6,camera4,low_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint3,objective0,camera3,colour
  :precondition (and (at,rover3,waypoint3) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint3,objective0,camera4,high_res
  :precondition (and (at,rover3,waypoint3) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint3,objective0,camera4,low_res
  :precondition (and (at,rover3,waypoint3) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint3,objective1,camera3,colour
  :precondition (and (at,rover3,waypoint3) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective1,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint3,objective1,camera4,high_res
  :precondition (and (at,rover3,waypoint3) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint3,objective1,camera4,low_res
  :precondition (and (at,rover3,waypoint3) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint3,objective2,camera3,colour
  :precondition (and (at,rover3,waypoint3) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint3,objective2,camera4,high_res
  :precondition (and (at,rover3,waypoint3) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint3,objective2,camera4,low_res
  :precondition (and (at,rover3,waypoint3) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint3,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint3) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint3,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint3) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint3,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint3) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint3,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint3) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint3,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint3) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint3,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint3) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint3,objective6,camera3,colour
  :precondition (and (at,rover3,waypoint3) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective6,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint3,objective6,camera4,high_res
  :precondition (and (at,rover3,waypoint3) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint3,objective6,camera4,low_res
  :precondition (and (at,rover3,waypoint3) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint4,objective0,camera3,colour
  :precondition (and (at,rover3,waypoint4) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint4,objective0,camera4,high_res
  :precondition (and (at,rover3,waypoint4) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint4,objective0,camera4,low_res
  :precondition (and (at,rover3,waypoint4) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint4,objective1,camera3,colour
  :precondition (and (at,rover3,waypoint4) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective1,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint4,objective1,camera4,high_res
  :precondition (and (at,rover3,waypoint4) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint4,objective1,camera4,low_res
  :precondition (and (at,rover3,waypoint4) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint4,objective2,camera3,colour
  :precondition (and (at,rover3,waypoint4) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint4,objective2,camera4,high_res
  :precondition (and (at,rover3,waypoint4) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint4,objective2,camera4,low_res
  :precondition (and (at,rover3,waypoint4) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint4,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint4) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint4,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint4) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint4,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint4) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint4,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint4) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint4,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint4) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint4,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint4) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint4,objective6,camera3,colour
  :precondition (and (at,rover3,waypoint4) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective6,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint4,objective6,camera4,high_res
  :precondition (and (at,rover3,waypoint4) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint4,objective6,camera4,low_res
  :precondition (and (at,rover3,waypoint4) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint5,objective0,camera3,colour
  :precondition (and (at,rover3,waypoint5) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint5,objective0,camera4,high_res
  :precondition (and (at,rover3,waypoint5) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint5,objective0,camera4,low_res
  :precondition (and (at,rover3,waypoint5) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint5,objective1,camera3,colour
  :precondition (and (at,rover3,waypoint5) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective1,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint5,objective1,camera4,high_res
  :precondition (and (at,rover3,waypoint5) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint5,objective1,camera4,low_res
  :precondition (and (at,rover3,waypoint5) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint5,objective2,camera3,colour
  :precondition (and (at,rover3,waypoint5) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint5,objective2,camera4,high_res
  :precondition (and (at,rover3,waypoint5) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint5,objective2,camera4,low_res
  :precondition (and (at,rover3,waypoint5) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint5,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint5) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint5,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint5) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint5,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint5) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint5,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint5) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint5,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint5) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint5,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint5) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint5,objective6,camera3,colour
  :precondition (and (at,rover3,waypoint5) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective6,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint5,objective6,camera4,high_res
  :precondition (and (at,rover3,waypoint5) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint5,objective6,camera4,low_res
  :precondition (and (at,rover3,waypoint5) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint6,objective0,camera3,colour
  :precondition (and (at,rover3,waypoint6) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint6,objective0,camera4,high_res
  :precondition (and (at,rover3,waypoint6) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint6,objective0,camera4,low_res
  :precondition (and (at,rover3,waypoint6) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint6,objective1,camera3,colour
  :precondition (and (at,rover3,waypoint6) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective1,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint6,objective1,camera4,high_res
  :precondition (and (at,rover3,waypoint6) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint6,objective1,camera4,low_res
  :precondition (and (at,rover3,waypoint6) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint6,objective2,camera3,colour
  :precondition (and (at,rover3,waypoint6) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint6,objective2,camera4,high_res
  :precondition (and (at,rover3,waypoint6) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint6,objective2,camera4,low_res
  :precondition (and (at,rover3,waypoint6) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint6,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint6) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint6,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint6) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint6,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint6) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint6,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint6) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint6,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint6) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint6,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint6) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint6,objective6,camera3,colour
  :precondition (and (at,rover3,waypoint6) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective6,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint6,objective6,camera4,high_res
  :precondition (and (at,rover3,waypoint6) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint6,objective6,camera4,low_res
  :precondition (and (at,rover3,waypoint6) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint7,objective0,camera3,colour
  :precondition (and (at,rover3,waypoint7) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint7,objective0,camera4,high_res
  :precondition (and (at,rover3,waypoint7) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint7,objective0,camera4,low_res
  :precondition (and (at,rover3,waypoint7) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint7,objective1,camera3,colour
  :precondition (and (at,rover3,waypoint7) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective1,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint7,objective1,camera4,high_res
  :precondition (and (at,rover3,waypoint7) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint7,objective1,camera4,low_res
  :precondition (and (at,rover3,waypoint7) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint7,objective2,camera3,colour
  :precondition (and (at,rover3,waypoint7) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint7,objective2,camera4,high_res
  :precondition (and (at,rover3,waypoint7) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint7,objective2,camera4,low_res
  :precondition (and (at,rover3,waypoint7) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint7,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint7) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint7,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint7) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint7,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint7) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint7,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint7) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint7,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint7) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint7,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint7) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint7,objective6,camera3,colour
  :precondition (and (at,rover3,waypoint7) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective6,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint7,objective6,camera4,high_res
  :precondition (and (at,rover3,waypoint7) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint7,objective6,camera4,low_res
  :precondition (and (at,rover3,waypoint7) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint8,objective0,camera3,colour
  :precondition (and (at,rover3,waypoint8) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint8,objective0,camera4,high_res
  :precondition (and (at,rover3,waypoint8) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint8,objective0,camera4,low_res
  :precondition (and (at,rover3,waypoint8) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint8,objective1,camera3,colour
  :precondition (and (at,rover3,waypoint8) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective1,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint8,objective1,camera4,high_res
  :precondition (and (at,rover3,waypoint8) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint8,objective1,camera4,low_res
  :precondition (and (at,rover3,waypoint8) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint8,objective2,camera3,colour
  :precondition (and (at,rover3,waypoint8) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint8,objective2,camera4,high_res
  :precondition (and (at,rover3,waypoint8) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint8,objective2,camera4,low_res
  :precondition (and (at,rover3,waypoint8) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint8,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint8) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint8,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint8) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint8,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint8) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint8,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint8) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint8,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint8) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint8,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint8) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint8,objective6,camera3,colour
  :precondition (and (at,rover3,waypoint8) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective6,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint8,objective6,camera4,high_res
  :precondition (and (at,rover3,waypoint8) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint8,objective6,camera4,low_res
  :precondition (and (at,rover3,waypoint8) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint9,objective0,camera3,colour
  :precondition (and (at,rover3,waypoint9) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint9,objective0,camera4,high_res
  :precondition (and (at,rover3,waypoint9) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint9,objective0,camera4,low_res
  :precondition (and (at,rover3,waypoint9) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint9,objective1,camera3,colour
  :precondition (and (at,rover3,waypoint9) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective1,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint9,objective1,camera4,high_res
  :precondition (and (at,rover3,waypoint9) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint9,objective1,camera4,low_res
  :precondition (and (at,rover3,waypoint9) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint9,objective2,camera3,colour
  :precondition (and (at,rover3,waypoint9) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint9,objective2,camera4,high_res
  :precondition (and (at,rover3,waypoint9) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint9,objective2,camera4,low_res
  :precondition (and (at,rover3,waypoint9) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint9,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint9) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint9,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint9) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint9,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint9) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint9,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint9) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint9,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint9) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint9,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint9) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint9,objective6,camera3,colour
  :precondition (and (at,rover3,waypoint9) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective6,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint9,objective6,camera4,high_res
  :precondition (and (at,rover3,waypoint9) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint9,objective6,camera4,low_res
  :precondition (and (at,rover3,waypoint9) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint10,objective0,camera3,colour
  :precondition (and (at,rover3,waypoint10) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint10,objective0,camera4,high_res
  :precondition (and (at,rover3,waypoint10) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint10,objective0,camera4,low_res
  :precondition (and (at,rover3,waypoint10) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective0,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint10,objective1,camera3,colour
  :precondition (and (at,rover3,waypoint10) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective1,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint10,objective1,camera4,high_res
  :precondition (and (at,rover3,waypoint10) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint10,objective1,camera4,low_res
  :precondition (and (at,rover3,waypoint10) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint10,objective2,camera3,colour
  :precondition (and (at,rover3,waypoint10) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint10,objective2,camera4,high_res
  :precondition (and (at,rover3,waypoint10) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint10,objective2,camera4,low_res
  :precondition (and (at,rover3,waypoint10) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint10,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint10) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint10,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint10) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint10,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint10) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint10,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint10) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint10,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint10) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint10,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint10) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint10,objective6,camera3,colour
  :precondition (and (at,rover3,waypoint10) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective6,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint10,objective6,camera4,high_res
  :precondition (and (at,rover3,waypoint10) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint10,objective6,camera4,low_res
  :precondition (and (at,rover3,waypoint10) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective6,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint11,objective1,camera3,colour
  :precondition (and (at,rover3,waypoint11) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective1,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint11,objective1,camera4,high_res
  :precondition (and (at,rover3,waypoint11) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint11,objective1,camera4,low_res
  :precondition (and (at,rover3,waypoint11) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint11,objective2,camera3,colour
  :precondition (and (at,rover3,waypoint11) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint11,objective2,camera4,high_res
  :precondition (and (at,rover3,waypoint11) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint11,objective2,camera4,low_res
  :precondition (and (at,rover3,waypoint11) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint11,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint11) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint11,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint11) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint11,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint11) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint11,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint11) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint11,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint11) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint11,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint11) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint12,objective1,camera3,colour
  :precondition (and (at,rover3,waypoint12) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective1,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint12,objective1,camera4,high_res
  :precondition (and (at,rover3,waypoint12) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint12,objective1,camera4,low_res
  :precondition (and (at,rover3,waypoint12) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective1,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint12,objective2,camera3,colour
  :precondition (and (at,rover3,waypoint12) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint12,objective2,camera4,high_res
  :precondition (and (at,rover3,waypoint12) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint12,objective2,camera4,low_res
  :precondition (and (at,rover3,waypoint12) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint12,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint12) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint12,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint12) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint12,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint12) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint12,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint12) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint12,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint12) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint12,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint12) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint13,objective2,camera3,colour
  :precondition (and (at,rover3,waypoint13) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint13,objective2,camera4,high_res
  :precondition (and (at,rover3,waypoint13) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint13,objective2,camera4,low_res
  :precondition (and (at,rover3,waypoint13) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint13,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint13) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint13,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint13) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint13,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint13) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint13,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint13) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint13,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint13) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint13,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint13) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint14,objective2,camera3,colour
  :precondition (and (at,rover3,waypoint14) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint14,objective2,camera4,high_res
  :precondition (and (at,rover3,waypoint14) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint14,objective2,camera4,low_res
  :precondition (and (at,rover3,waypoint14) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint14,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint14) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint14,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint14) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint14,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint14) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint14,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint14) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint14,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint14) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint14,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint14) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint15,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint15) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint15,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint15) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint15,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint15) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint15,objective4,camera3,colour
  :precondition (and (at,rover3,waypoint15) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective4,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint15,objective4,camera4,high_res
  :precondition (and (at,rover3,waypoint15) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint15,objective4,camera4,low_res
  :precondition (and (at,rover3,waypoint15) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective4,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint16,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint16) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint16,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint16) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint16,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint16) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint17,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint17) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint17,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint17) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint17,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint17) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint18,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint18) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint18,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint18) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint18,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint18) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint19,objective3,camera3,colour
  :precondition (and (at,rover3,waypoint19) (calibrated,camera3,rover3))
  :effect (and (have_image,rover3,objective3,colour) (not (calibrated,camera3,rover3))))
 (:action take_image,rover3,waypoint19,objective3,camera4,high_res
  :precondition (and (at,rover3,waypoint19) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,high_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover3,waypoint19,objective3,camera4,low_res
  :precondition (and (at,rover3,waypoint19) (calibrated,camera4,rover3))
  :effect (and (have_image,rover3,objective3,low_res) (not (calibrated,camera4,rover3))))
 (:action take_image,rover4,waypoint0,objective0,camera2,colour
  :precondition (and (at,rover4,waypoint0) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint0,objective0,camera2,high_res
  :precondition (and (at,rover4,waypoint0) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint0,objective1,camera2,colour
  :precondition (and (at,rover4,waypoint0) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint0,objective1,camera2,high_res
  :precondition (and (at,rover4,waypoint0) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint0,objective2,camera2,colour
  :precondition (and (at,rover4,waypoint0) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint0,objective2,camera2,high_res
  :precondition (and (at,rover4,waypoint0) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint0,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint0) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint0,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint0) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint0,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint0) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint0,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint0) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint0,objective5,camera2,colour
  :precondition (and (at,rover4,waypoint0) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective5,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint0,objective5,camera2,high_res
  :precondition (and (at,rover4,waypoint0) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective5,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint0,objective6,camera2,colour
  :precondition (and (at,rover4,waypoint0) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint0,objective6,camera2,high_res
  :precondition (and (at,rover4,waypoint0) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint1,objective0,camera2,colour
  :precondition (and (at,rover4,waypoint1) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint1,objective0,camera2,high_res
  :precondition (and (at,rover4,waypoint1) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint1,objective1,camera2,colour
  :precondition (and (at,rover4,waypoint1) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint1,objective1,camera2,high_res
  :precondition (and (at,rover4,waypoint1) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint1,objective2,camera2,colour
  :precondition (and (at,rover4,waypoint1) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint1,objective2,camera2,high_res
  :precondition (and (at,rover4,waypoint1) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint1,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint1) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint1,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint1) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint1,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint1) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint1,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint1) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint1,objective5,camera2,colour
  :precondition (and (at,rover4,waypoint1) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective5,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint1,objective5,camera2,high_res
  :precondition (and (at,rover4,waypoint1) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective5,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint1,objective6,camera2,colour
  :precondition (and (at,rover4,waypoint1) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint1,objective6,camera2,high_res
  :precondition (and (at,rover4,waypoint1) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint2,objective0,camera2,colour
  :precondition (and (at,rover4,waypoint2) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint2,objective0,camera2,high_res
  :precondition (and (at,rover4,waypoint2) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint2,objective1,camera2,colour
  :precondition (and (at,rover4,waypoint2) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint2,objective1,camera2,high_res
  :precondition (and (at,rover4,waypoint2) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint2,objective2,camera2,colour
  :precondition (and (at,rover4,waypoint2) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint2,objective2,camera2,high_res
  :precondition (and (at,rover4,waypoint2) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint2,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint2) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint2,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint2) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint2,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint2) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint2,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint2) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint2,objective5,camera2,colour
  :precondition (and (at,rover4,waypoint2) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective5,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint2,objective5,camera2,high_res
  :precondition (and (at,rover4,waypoint2) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective5,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint2,objective6,camera2,colour
  :precondition (and (at,rover4,waypoint2) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint2,objective6,camera2,high_res
  :precondition (and (at,rover4,waypoint2) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint3,objective0,camera2,colour
  :precondition (and (at,rover4,waypoint3) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint3,objective0,camera2,high_res
  :precondition (and (at,rover4,waypoint3) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint3,objective1,camera2,colour
  :precondition (and (at,rover4,waypoint3) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint3,objective1,camera2,high_res
  :precondition (and (at,rover4,waypoint3) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint3,objective2,camera2,colour
  :precondition (and (at,rover4,waypoint3) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint3,objective2,camera2,high_res
  :precondition (and (at,rover4,waypoint3) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint3,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint3) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint3,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint3) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint3,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint3) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint3,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint3) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint3,objective6,camera2,colour
  :precondition (and (at,rover4,waypoint3) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint3,objective6,camera2,high_res
  :precondition (and (at,rover4,waypoint3) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint4,objective0,camera2,colour
  :precondition (and (at,rover4,waypoint4) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint4,objective0,camera2,high_res
  :precondition (and (at,rover4,waypoint4) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint4,objective1,camera2,colour
  :precondition (and (at,rover4,waypoint4) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint4,objective1,camera2,high_res
  :precondition (and (at,rover4,waypoint4) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint4,objective2,camera2,colour
  :precondition (and (at,rover4,waypoint4) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint4,objective2,camera2,high_res
  :precondition (and (at,rover4,waypoint4) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint4,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint4) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint4,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint4) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint4,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint4) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint4,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint4) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint4,objective6,camera2,colour
  :precondition (and (at,rover4,waypoint4) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint4,objective6,camera2,high_res
  :precondition (and (at,rover4,waypoint4) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint5,objective0,camera2,colour
  :precondition (and (at,rover4,waypoint5) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint5,objective0,camera2,high_res
  :precondition (and (at,rover4,waypoint5) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint5,objective1,camera2,colour
  :precondition (and (at,rover4,waypoint5) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint5,objective1,camera2,high_res
  :precondition (and (at,rover4,waypoint5) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint5,objective2,camera2,colour
  :precondition (and (at,rover4,waypoint5) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint5,objective2,camera2,high_res
  :precondition (and (at,rover4,waypoint5) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint5,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint5) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint5,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint5) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint5,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint5) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint5,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint5) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint5,objective6,camera2,colour
  :precondition (and (at,rover4,waypoint5) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint5,objective6,camera2,high_res
  :precondition (and (at,rover4,waypoint5) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint6,objective0,camera2,colour
  :precondition (and (at,rover4,waypoint6) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint6,objective0,camera2,high_res
  :precondition (and (at,rover4,waypoint6) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint6,objective1,camera2,colour
  :precondition (and (at,rover4,waypoint6) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint6,objective1,camera2,high_res
  :precondition (and (at,rover4,waypoint6) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint6,objective2,camera2,colour
  :precondition (and (at,rover4,waypoint6) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint6,objective2,camera2,high_res
  :precondition (and (at,rover4,waypoint6) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint6,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint6) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint6,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint6) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint6,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint6) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint6,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint6) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint6,objective6,camera2,colour
  :precondition (and (at,rover4,waypoint6) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint6,objective6,camera2,high_res
  :precondition (and (at,rover4,waypoint6) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint7,objective0,camera2,colour
  :precondition (and (at,rover4,waypoint7) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint7,objective0,camera2,high_res
  :precondition (and (at,rover4,waypoint7) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint7,objective1,camera2,colour
  :precondition (and (at,rover4,waypoint7) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint7,objective1,camera2,high_res
  :precondition (and (at,rover4,waypoint7) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint7,objective2,camera2,colour
  :precondition (and (at,rover4,waypoint7) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint7,objective2,camera2,high_res
  :precondition (and (at,rover4,waypoint7) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint7,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint7) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint7,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint7) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint7,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint7) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint7,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint7) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint7,objective6,camera2,colour
  :precondition (and (at,rover4,waypoint7) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint7,objective6,camera2,high_res
  :precondition (and (at,rover4,waypoint7) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint8,objective0,camera2,colour
  :precondition (and (at,rover4,waypoint8) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint8,objective0,camera2,high_res
  :precondition (and (at,rover4,waypoint8) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint8,objective1,camera2,colour
  :precondition (and (at,rover4,waypoint8) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint8,objective1,camera2,high_res
  :precondition (and (at,rover4,waypoint8) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint8,objective2,camera2,colour
  :precondition (and (at,rover4,waypoint8) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint8,objective2,camera2,high_res
  :precondition (and (at,rover4,waypoint8) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint8,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint8) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint8,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint8) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint8,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint8) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint8,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint8) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint8,objective6,camera2,colour
  :precondition (and (at,rover4,waypoint8) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint8,objective6,camera2,high_res
  :precondition (and (at,rover4,waypoint8) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint9,objective0,camera2,colour
  :precondition (and (at,rover4,waypoint9) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint9,objective0,camera2,high_res
  :precondition (and (at,rover4,waypoint9) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint9,objective1,camera2,colour
  :precondition (and (at,rover4,waypoint9) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint9,objective1,camera2,high_res
  :precondition (and (at,rover4,waypoint9) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint9,objective2,camera2,colour
  :precondition (and (at,rover4,waypoint9) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint9,objective2,camera2,high_res
  :precondition (and (at,rover4,waypoint9) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint9,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint9) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint9,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint9) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint9,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint9) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint9,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint9) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint9,objective6,camera2,colour
  :precondition (and (at,rover4,waypoint9) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint9,objective6,camera2,high_res
  :precondition (and (at,rover4,waypoint9) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint10,objective0,camera2,colour
  :precondition (and (at,rover4,waypoint10) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint10,objective0,camera2,high_res
  :precondition (and (at,rover4,waypoint10) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective0,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint10,objective1,camera2,colour
  :precondition (and (at,rover4,waypoint10) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint10,objective1,camera2,high_res
  :precondition (and (at,rover4,waypoint10) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint10,objective2,camera2,colour
  :precondition (and (at,rover4,waypoint10) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint10,objective2,camera2,high_res
  :precondition (and (at,rover4,waypoint10) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint10,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint10) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint10,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint10) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint10,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint10) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint10,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint10) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint10,objective6,camera2,colour
  :precondition (and (at,rover4,waypoint10) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint10,objective6,camera2,high_res
  :precondition (and (at,rover4,waypoint10) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective6,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint11,objective1,camera2,colour
  :precondition (and (at,rover4,waypoint11) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint11,objective1,camera2,high_res
  :precondition (and (at,rover4,waypoint11) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint11,objective2,camera2,colour
  :precondition (and (at,rover4,waypoint11) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint11,objective2,camera2,high_res
  :precondition (and (at,rover4,waypoint11) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint11,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint11) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint11,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint11) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint11,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint11) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint11,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint11) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint12,objective1,camera2,colour
  :precondition (and (at,rover4,waypoint12) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint12,objective1,camera2,high_res
  :precondition (and (at,rover4,waypoint12) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective1,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint12,objective2,camera2,colour
  :precondition (and (at,rover4,waypoint12) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint12,objective2,camera2,high_res
  :precondition (and (at,rover4,waypoint12) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint12,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint12) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint12,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint12) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint12,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint12) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint12,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint12) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint13,objective2,camera2,colour
  :precondition (and (at,rover4,waypoint13) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint13,objective2,camera2,high_res
  :precondition (and (at,rover4,waypoint13) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint13,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint13) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint13,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint13) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint13,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint13) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint13,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint13) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint14,objective2,camera2,colour
  :precondition (and (at,rover4,waypoint14) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint14,objective2,camera2,high_res
  :precondition (and (at,rover4,waypoint14) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective2,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint14,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint14) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint14,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint14) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint14,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint14) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint14,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint14) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint15,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint15) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint15,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint15) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint15,objective4,camera2,colour
  :precondition (and (at,rover4,waypoint15) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint15,objective4,camera2,high_res
  :precondition (and (at,rover4,waypoint15) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective4,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint16,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint16) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint16,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint16) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint17,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint17) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint17,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint17) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint18,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint18) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint18,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint18) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint19,objective3,camera2,colour
  :precondition (and (at,rover4,waypoint19) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,colour) (not (calibrated,camera2,rover4))))
 (:action take_image,rover4,waypoint19,objective3,camera2,high_res
  :precondition (and (at,rover4,waypoint19) (calibrated,camera2,rover4))
  :effect (and (have_image,rover4,objective3,high_res) (not (calibrated,camera2,rover4))))
 (:action take_image,rover5,waypoint0,objective0,camera6,high_res
  :precondition (and (at,rover5,waypoint0) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint0,objective0,camera6,low_res
  :precondition (and (at,rover5,waypoint0) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint0,objective1,camera6,high_res
  :precondition (and (at,rover5,waypoint0) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint0,objective1,camera6,low_res
  :precondition (and (at,rover5,waypoint0) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint0,objective2,camera6,high_res
  :precondition (and (at,rover5,waypoint0) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint0,objective2,camera6,low_res
  :precondition (and (at,rover5,waypoint0) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint0,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint0) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint0,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint0) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint0,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint0) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint0,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint0) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint0,objective5,camera6,high_res
  :precondition (and (at,rover5,waypoint0) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective5,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint0,objective5,camera6,low_res
  :precondition (and (at,rover5,waypoint0) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective5,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint0,objective6,camera6,high_res
  :precondition (and (at,rover5,waypoint0) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint0,objective6,camera6,low_res
  :precondition (and (at,rover5,waypoint0) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint1,objective0,camera6,high_res
  :precondition (and (at,rover5,waypoint1) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint1,objective0,camera6,low_res
  :precondition (and (at,rover5,waypoint1) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint1,objective1,camera6,high_res
  :precondition (and (at,rover5,waypoint1) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint1,objective1,camera6,low_res
  :precondition (and (at,rover5,waypoint1) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint1,objective2,camera6,high_res
  :precondition (and (at,rover5,waypoint1) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint1,objective2,camera6,low_res
  :precondition (and (at,rover5,waypoint1) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint1,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint1) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint1,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint1) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint1,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint1) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint1,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint1) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint1,objective5,camera6,high_res
  :precondition (and (at,rover5,waypoint1) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective5,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint1,objective5,camera6,low_res
  :precondition (and (at,rover5,waypoint1) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective5,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint1,objective6,camera6,high_res
  :precondition (and (at,rover5,waypoint1) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint1,objective6,camera6,low_res
  :precondition (and (at,rover5,waypoint1) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint2,objective0,camera6,high_res
  :precondition (and (at,rover5,waypoint2) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint2,objective0,camera6,low_res
  :precondition (and (at,rover5,waypoint2) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint2,objective1,camera6,high_res
  :precondition (and (at,rover5,waypoint2) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint2,objective1,camera6,low_res
  :precondition (and (at,rover5,waypoint2) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint2,objective2,camera6,high_res
  :precondition (and (at,rover5,waypoint2) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint2,objective2,camera6,low_res
  :precondition (and (at,rover5,waypoint2) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint2,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint2) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint2,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint2) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint2,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint2) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint2,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint2) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint2,objective5,camera6,high_res
  :precondition (and (at,rover5,waypoint2) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective5,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint2,objective5,camera6,low_res
  :precondition (and (at,rover5,waypoint2) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective5,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint2,objective6,camera6,high_res
  :precondition (and (at,rover5,waypoint2) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint2,objective6,camera6,low_res
  :precondition (and (at,rover5,waypoint2) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint3,objective0,camera6,high_res
  :precondition (and (at,rover5,waypoint3) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint3,objective0,camera6,low_res
  :precondition (and (at,rover5,waypoint3) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint3,objective1,camera6,high_res
  :precondition (and (at,rover5,waypoint3) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint3,objective1,camera6,low_res
  :precondition (and (at,rover5,waypoint3) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint3,objective2,camera6,high_res
  :precondition (and (at,rover5,waypoint3) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint3,objective2,camera6,low_res
  :precondition (and (at,rover5,waypoint3) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint3,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint3) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint3,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint3) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint3,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint3) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint3,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint3) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint3,objective6,camera6,high_res
  :precondition (and (at,rover5,waypoint3) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint3,objective6,camera6,low_res
  :precondition (and (at,rover5,waypoint3) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint4,objective0,camera6,high_res
  :precondition (and (at,rover5,waypoint4) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint4,objective0,camera6,low_res
  :precondition (and (at,rover5,waypoint4) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint4,objective1,camera6,high_res
  :precondition (and (at,rover5,waypoint4) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint4,objective1,camera6,low_res
  :precondition (and (at,rover5,waypoint4) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint4,objective2,camera6,high_res
  :precondition (and (at,rover5,waypoint4) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint4,objective2,camera6,low_res
  :precondition (and (at,rover5,waypoint4) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint4,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint4) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint4,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint4) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint4,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint4) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint4,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint4) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint4,objective6,camera6,high_res
  :precondition (and (at,rover5,waypoint4) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint4,objective6,camera6,low_res
  :precondition (and (at,rover5,waypoint4) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint5,objective0,camera6,high_res
  :precondition (and (at,rover5,waypoint5) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint5,objective0,camera6,low_res
  :precondition (and (at,rover5,waypoint5) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint5,objective1,camera6,high_res
  :precondition (and (at,rover5,waypoint5) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint5,objective1,camera6,low_res
  :precondition (and (at,rover5,waypoint5) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint5,objective2,camera6,high_res
  :precondition (and (at,rover5,waypoint5) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint5,objective2,camera6,low_res
  :precondition (and (at,rover5,waypoint5) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint5,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint5) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint5,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint5) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint5,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint5) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint5,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint5) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint5,objective6,camera6,high_res
  :precondition (and (at,rover5,waypoint5) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint5,objective6,camera6,low_res
  :precondition (and (at,rover5,waypoint5) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint6,objective0,camera6,high_res
  :precondition (and (at,rover5,waypoint6) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint6,objective0,camera6,low_res
  :precondition (and (at,rover5,waypoint6) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint6,objective1,camera6,high_res
  :precondition (and (at,rover5,waypoint6) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint6,objective1,camera6,low_res
  :precondition (and (at,rover5,waypoint6) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint6,objective2,camera6,high_res
  :precondition (and (at,rover5,waypoint6) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint6,objective2,camera6,low_res
  :precondition (and (at,rover5,waypoint6) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint6,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint6) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint6,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint6) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint6,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint6) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint6,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint6) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint6,objective6,camera6,high_res
  :precondition (and (at,rover5,waypoint6) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint6,objective6,camera6,low_res
  :precondition (and (at,rover5,waypoint6) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint7,objective0,camera6,high_res
  :precondition (and (at,rover5,waypoint7) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint7,objective0,camera6,low_res
  :precondition (and (at,rover5,waypoint7) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint7,objective1,camera6,high_res
  :precondition (and (at,rover5,waypoint7) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint7,objective1,camera6,low_res
  :precondition (and (at,rover5,waypoint7) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint7,objective2,camera6,high_res
  :precondition (and (at,rover5,waypoint7) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint7,objective2,camera6,low_res
  :precondition (and (at,rover5,waypoint7) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint7,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint7) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint7,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint7) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint7,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint7) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint7,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint7) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint7,objective6,camera6,high_res
  :precondition (and (at,rover5,waypoint7) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint7,objective6,camera6,low_res
  :precondition (and (at,rover5,waypoint7) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint8,objective0,camera6,high_res
  :precondition (and (at,rover5,waypoint8) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint8,objective0,camera6,low_res
  :precondition (and (at,rover5,waypoint8) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint8,objective1,camera6,high_res
  :precondition (and (at,rover5,waypoint8) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint8,objective1,camera6,low_res
  :precondition (and (at,rover5,waypoint8) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint8,objective2,camera6,high_res
  :precondition (and (at,rover5,waypoint8) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint8,objective2,camera6,low_res
  :precondition (and (at,rover5,waypoint8) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint8,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint8) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint8,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint8) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint8,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint8) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint8,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint8) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint8,objective6,camera6,high_res
  :precondition (and (at,rover5,waypoint8) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint8,objective6,camera6,low_res
  :precondition (and (at,rover5,waypoint8) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint9,objective0,camera6,high_res
  :precondition (and (at,rover5,waypoint9) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint9,objective0,camera6,low_res
  :precondition (and (at,rover5,waypoint9) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint9,objective1,camera6,high_res
  :precondition (and (at,rover5,waypoint9) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint9,objective1,camera6,low_res
  :precondition (and (at,rover5,waypoint9) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint9,objective2,camera6,high_res
  :precondition (and (at,rover5,waypoint9) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint9,objective2,camera6,low_res
  :precondition (and (at,rover5,waypoint9) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint9,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint9) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint9,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint9) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint9,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint9) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint9,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint9) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint9,objective6,camera6,high_res
  :precondition (and (at,rover5,waypoint9) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint9,objective6,camera6,low_res
  :precondition (and (at,rover5,waypoint9) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint10,objective0,camera6,high_res
  :precondition (and (at,rover5,waypoint10) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint10,objective0,camera6,low_res
  :precondition (and (at,rover5,waypoint10) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective0,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint10,objective1,camera6,high_res
  :precondition (and (at,rover5,waypoint10) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint10,objective1,camera6,low_res
  :precondition (and (at,rover5,waypoint10) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint10,objective2,camera6,high_res
  :precondition (and (at,rover5,waypoint10) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint10,objective2,camera6,low_res
  :precondition (and (at,rover5,waypoint10) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint10,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint10) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint10,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint10) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint10,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint10) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint10,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint10) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint10,objective6,camera6,high_res
  :precondition (and (at,rover5,waypoint10) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint10,objective6,camera6,low_res
  :precondition (and (at,rover5,waypoint10) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective6,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint11,objective1,camera6,high_res
  :precondition (and (at,rover5,waypoint11) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint11,objective1,camera6,low_res
  :precondition (and (at,rover5,waypoint11) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint11,objective2,camera6,high_res
  :precondition (and (at,rover5,waypoint11) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint11,objective2,camera6,low_res
  :precondition (and (at,rover5,waypoint11) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint11,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint11) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint11,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint11) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint11,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint11) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint11,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint11) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint12,objective1,camera6,high_res
  :precondition (and (at,rover5,waypoint12) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint12,objective1,camera6,low_res
  :precondition (and (at,rover5,waypoint12) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective1,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint12,objective2,camera6,high_res
  :precondition (and (at,rover5,waypoint12) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint12,objective2,camera6,low_res
  :precondition (and (at,rover5,waypoint12) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint12,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint12) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint12,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint12) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint12,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint12) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint12,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint12) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint13,objective2,camera6,high_res
  :precondition (and (at,rover5,waypoint13) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint13,objective2,camera6,low_res
  :precondition (and (at,rover5,waypoint13) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint13,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint13) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint13,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint13) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint13,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint13) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint13,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint13) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint14,objective2,camera6,high_res
  :precondition (and (at,rover5,waypoint14) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint14,objective2,camera6,low_res
  :precondition (and (at,rover5,waypoint14) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective2,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint14,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint14) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint14,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint14) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint14,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint14) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint14,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint14) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint15,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint15) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint15,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint15) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint15,objective4,camera6,high_res
  :precondition (and (at,rover5,waypoint15) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint15,objective4,camera6,low_res
  :precondition (and (at,rover5,waypoint15) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective4,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint16,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint16) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint16,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint16) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint17,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint17) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint17,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint17) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint18,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint18) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint18,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint18) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint19,objective3,camera6,high_res
  :precondition (and (at,rover5,waypoint19) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,high_res) (not (calibrated,camera6,rover5))))
 (:action take_image,rover5,waypoint19,objective3,camera6,low_res
  :precondition (and (at,rover5,waypoint19) (calibrated,camera6,rover5))
  :effect (and (have_image,rover5,objective3,low_res) (not (calibrated,camera6,rover5))))
 (:action communicate_soil_data,rover0,general,waypoint0,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover0,general,waypoint0,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_soil_analysis,rover0,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover0,general,waypoint0,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_soil_analysis,rover0,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover0,general,waypoint3,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover0,general,waypoint3,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_soil_analysis,rover0,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover0,general,waypoint3,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_soil_analysis,rover0,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover0,general,waypoint8,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint8))
  :effect (communicated_soil_data,waypoint8))
 (:action communicate_soil_data,rover0,general,waypoint8,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_soil_analysis,rover0,waypoint8))
  :effect (communicated_soil_data,waypoint8))
 (:action communicate_soil_data,rover0,general,waypoint8,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_soil_analysis,rover0,waypoint8))
  :effect (communicated_soil_data,waypoint8))
 (:action communicate_soil_data,rover0,general,waypoint9,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint9))
  :effect (communicated_soil_data,waypoint9))
 (:action communicate_soil_data,rover0,general,waypoint9,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_soil_analysis,rover0,waypoint9))
  :effect (communicated_soil_data,waypoint9))
 (:action communicate_soil_data,rover0,general,waypoint9,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_soil_analysis,rover0,waypoint9))
  :effect (communicated_soil_data,waypoint9))
 (:action communicate_soil_data,rover0,general,waypoint10,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint10))
  :effect (communicated_soil_data,waypoint10))
 (:action communicate_soil_data,rover0,general,waypoint10,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_soil_analysis,rover0,waypoint10))
  :effect (communicated_soil_data,waypoint10))
 (:action communicate_soil_data,rover0,general,waypoint10,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_soil_analysis,rover0,waypoint10))
  :effect (communicated_soil_data,waypoint10))
 (:action communicate_soil_data,rover0,general,waypoint11,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint11))
  :effect (communicated_soil_data,waypoint11))
 (:action communicate_soil_data,rover0,general,waypoint11,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_soil_analysis,rover0,waypoint11))
  :effect (communicated_soil_data,waypoint11))
 (:action communicate_soil_data,rover0,general,waypoint11,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_soil_analysis,rover0,waypoint11))
  :effect (communicated_soil_data,waypoint11))
 (:action communicate_soil_data,rover0,general,waypoint12,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint12))
  :effect (communicated_soil_data,waypoint12))
 (:action communicate_soil_data,rover0,general,waypoint12,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_soil_analysis,rover0,waypoint12))
  :effect (communicated_soil_data,waypoint12))
 (:action communicate_soil_data,rover0,general,waypoint12,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_soil_analysis,rover0,waypoint12))
  :effect (communicated_soil_data,waypoint12))
 (:action communicate_soil_data,rover0,general,waypoint13,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint13))
  :effect (communicated_soil_data,waypoint13))
 (:action communicate_soil_data,rover0,general,waypoint13,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_soil_analysis,rover0,waypoint13))
  :effect (communicated_soil_data,waypoint13))
 (:action communicate_soil_data,rover0,general,waypoint13,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_soil_analysis,rover0,waypoint13))
  :effect (communicated_soil_data,waypoint13))
 (:action communicate_soil_data,rover0,general,waypoint14,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint14))
  :effect (communicated_soil_data,waypoint14))
 (:action communicate_soil_data,rover0,general,waypoint14,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_soil_analysis,rover0,waypoint14))
  :effect (communicated_soil_data,waypoint14))
 (:action communicate_soil_data,rover0,general,waypoint14,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_soil_analysis,rover0,waypoint14))
  :effect (communicated_soil_data,waypoint14))
 (:action communicate_soil_data,rover0,general,waypoint15,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint15))
  :effect (communicated_soil_data,waypoint15))
 (:action communicate_soil_data,rover0,general,waypoint15,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_soil_analysis,rover0,waypoint15))
  :effect (communicated_soil_data,waypoint15))
 (:action communicate_soil_data,rover0,general,waypoint15,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_soil_analysis,rover0,waypoint15))
  :effect (communicated_soil_data,waypoint15))
 (:action communicate_soil_data,rover0,general,waypoint18,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint18))
  :effect (communicated_soil_data,waypoint18))
 (:action communicate_soil_data,rover0,general,waypoint18,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_soil_analysis,rover0,waypoint18))
  :effect (communicated_soil_data,waypoint18))
 (:action communicate_soil_data,rover0,general,waypoint18,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_soil_analysis,rover0,waypoint18))
  :effect (communicated_soil_data,waypoint18))
 (:action communicate_soil_data,rover0,general,waypoint19,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_soil_analysis,rover0,waypoint19))
  :effect (communicated_soil_data,waypoint19))
 (:action communicate_soil_data,rover0,general,waypoint19,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_soil_analysis,rover0,waypoint19))
  :effect (communicated_soil_data,waypoint19))
 (:action communicate_soil_data,rover0,general,waypoint19,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_soil_analysis,rover0,waypoint19))
  :effect (communicated_soil_data,waypoint19))
 (:action communicate_soil_data,rover2,general,waypoint0,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover2,general,waypoint0,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_soil_analysis,rover2,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover2,general,waypoint0,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_soil_analysis,rover2,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover2,general,waypoint3,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover2,general,waypoint3,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_soil_analysis,rover2,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover2,general,waypoint3,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_soil_analysis,rover2,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover2,general,waypoint9,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint9))
  :effect (communicated_soil_data,waypoint9))
 (:action communicate_soil_data,rover2,general,waypoint9,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_soil_analysis,rover2,waypoint9))
  :effect (communicated_soil_data,waypoint9))
 (:action communicate_soil_data,rover2,general,waypoint9,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_soil_analysis,rover2,waypoint9))
  :effect (communicated_soil_data,waypoint9))
 (:action communicate_soil_data,rover2,general,waypoint10,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint10))
  :effect (communicated_soil_data,waypoint10))
 (:action communicate_soil_data,rover2,general,waypoint10,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_soil_analysis,rover2,waypoint10))
  :effect (communicated_soil_data,waypoint10))
 (:action communicate_soil_data,rover2,general,waypoint10,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_soil_analysis,rover2,waypoint10))
  :effect (communicated_soil_data,waypoint10))
 (:action communicate_soil_data,rover2,general,waypoint11,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint11))
  :effect (communicated_soil_data,waypoint11))
 (:action communicate_soil_data,rover2,general,waypoint11,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_soil_analysis,rover2,waypoint11))
  :effect (communicated_soil_data,waypoint11))
 (:action communicate_soil_data,rover2,general,waypoint11,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_soil_analysis,rover2,waypoint11))
  :effect (communicated_soil_data,waypoint11))
 (:action communicate_soil_data,rover2,general,waypoint12,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint12))
  :effect (communicated_soil_data,waypoint12))
 (:action communicate_soil_data,rover2,general,waypoint12,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_soil_analysis,rover2,waypoint12))
  :effect (communicated_soil_data,waypoint12))
 (:action communicate_soil_data,rover2,general,waypoint12,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_soil_analysis,rover2,waypoint12))
  :effect (communicated_soil_data,waypoint12))
 (:action communicate_soil_data,rover2,general,waypoint13,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint13))
  :effect (communicated_soil_data,waypoint13))
 (:action communicate_soil_data,rover2,general,waypoint13,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_soil_analysis,rover2,waypoint13))
  :effect (communicated_soil_data,waypoint13))
 (:action communicate_soil_data,rover2,general,waypoint13,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_soil_analysis,rover2,waypoint13))
  :effect (communicated_soil_data,waypoint13))
 (:action communicate_soil_data,rover2,general,waypoint14,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint14))
  :effect (communicated_soil_data,waypoint14))
 (:action communicate_soil_data,rover2,general,waypoint14,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_soil_analysis,rover2,waypoint14))
  :effect (communicated_soil_data,waypoint14))
 (:action communicate_soil_data,rover2,general,waypoint14,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_soil_analysis,rover2,waypoint14))
  :effect (communicated_soil_data,waypoint14))
 (:action communicate_soil_data,rover2,general,waypoint15,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint15))
  :effect (communicated_soil_data,waypoint15))
 (:action communicate_soil_data,rover2,general,waypoint15,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_soil_analysis,rover2,waypoint15))
  :effect (communicated_soil_data,waypoint15))
 (:action communicate_soil_data,rover2,general,waypoint15,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_soil_analysis,rover2,waypoint15))
  :effect (communicated_soil_data,waypoint15))
 (:action communicate_soil_data,rover2,general,waypoint17,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint17))
  :effect (communicated_soil_data,waypoint17))
 (:action communicate_soil_data,rover2,general,waypoint17,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_soil_analysis,rover2,waypoint17))
  :effect (communicated_soil_data,waypoint17))
 (:action communicate_soil_data,rover2,general,waypoint17,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_soil_analysis,rover2,waypoint17))
  :effect (communicated_soil_data,waypoint17))
 (:action communicate_soil_data,rover2,general,waypoint18,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint18))
  :effect (communicated_soil_data,waypoint18))
 (:action communicate_soil_data,rover2,general,waypoint18,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_soil_analysis,rover2,waypoint18))
  :effect (communicated_soil_data,waypoint18))
 (:action communicate_soil_data,rover2,general,waypoint18,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_soil_analysis,rover2,waypoint18))
  :effect (communicated_soil_data,waypoint18))
 (:action communicate_soil_data,rover2,general,waypoint19,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_soil_analysis,rover2,waypoint19))
  :effect (communicated_soil_data,waypoint19))
 (:action communicate_soil_data,rover2,general,waypoint19,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_soil_analysis,rover2,waypoint19))
  :effect (communicated_soil_data,waypoint19))
 (:action communicate_soil_data,rover2,general,waypoint19,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_soil_analysis,rover2,waypoint19))
  :effect (communicated_soil_data,waypoint19))
 (:action communicate_soil_data,rover4,general,waypoint0,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_soil_analysis,rover4,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover4,general,waypoint0,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_soil_analysis,rover4,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover4,general,waypoint0,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_soil_analysis,rover4,waypoint0))
  :effect (communicated_soil_data,waypoint0))
 (:action communicate_soil_data,rover4,general,waypoint3,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_soil_analysis,rover4,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover4,general,waypoint3,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_soil_analysis,rover4,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover4,general,waypoint3,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_soil_analysis,rover4,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover4,general,waypoint8,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_soil_analysis,rover4,waypoint8))
  :effect (communicated_soil_data,waypoint8))
 (:action communicate_soil_data,rover4,general,waypoint8,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_soil_analysis,rover4,waypoint8))
  :effect (communicated_soil_data,waypoint8))
 (:action communicate_soil_data,rover4,general,waypoint8,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_soil_analysis,rover4,waypoint8))
  :effect (communicated_soil_data,waypoint8))
 (:action communicate_soil_data,rover4,general,waypoint9,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_soil_analysis,rover4,waypoint9))
  :effect (communicated_soil_data,waypoint9))
 (:action communicate_soil_data,rover4,general,waypoint9,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_soil_analysis,rover4,waypoint9))
  :effect (communicated_soil_data,waypoint9))
 (:action communicate_soil_data,rover4,general,waypoint9,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_soil_analysis,rover4,waypoint9))
  :effect (communicated_soil_data,waypoint9))
 (:action communicate_soil_data,rover4,general,waypoint10,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_soil_analysis,rover4,waypoint10))
  :effect (communicated_soil_data,waypoint10))
 (:action communicate_soil_data,rover4,general,waypoint10,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_soil_analysis,rover4,waypoint10))
  :effect (communicated_soil_data,waypoint10))
 (:action communicate_soil_data,rover4,general,waypoint10,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_soil_analysis,rover4,waypoint10))
  :effect (communicated_soil_data,waypoint10))
 (:action communicate_soil_data,rover4,general,waypoint11,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_soil_analysis,rover4,waypoint11))
  :effect (communicated_soil_data,waypoint11))
 (:action communicate_soil_data,rover4,general,waypoint11,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_soil_analysis,rover4,waypoint11))
  :effect (communicated_soil_data,waypoint11))
 (:action communicate_soil_data,rover4,general,waypoint11,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_soil_analysis,rover4,waypoint11))
  :effect (communicated_soil_data,waypoint11))
 (:action communicate_soil_data,rover4,general,waypoint12,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_soil_analysis,rover4,waypoint12))
  :effect (communicated_soil_data,waypoint12))
 (:action communicate_soil_data,rover4,general,waypoint12,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_soil_analysis,rover4,waypoint12))
  :effect (communicated_soil_data,waypoint12))
 (:action communicate_soil_data,rover4,general,waypoint12,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_soil_analysis,rover4,waypoint12))
  :effect (communicated_soil_data,waypoint12))
 (:action communicate_soil_data,rover4,general,waypoint13,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_soil_analysis,rover4,waypoint13))
  :effect (communicated_soil_data,waypoint13))
 (:action communicate_soil_data,rover4,general,waypoint13,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_soil_analysis,rover4,waypoint13))
  :effect (communicated_soil_data,waypoint13))
 (:action communicate_soil_data,rover4,general,waypoint13,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_soil_analysis,rover4,waypoint13))
  :effect (communicated_soil_data,waypoint13))
 (:action communicate_soil_data,rover4,general,waypoint14,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_soil_analysis,rover4,waypoint14))
  :effect (communicated_soil_data,waypoint14))
 (:action communicate_soil_data,rover4,general,waypoint14,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_soil_analysis,rover4,waypoint14))
  :effect (communicated_soil_data,waypoint14))
 (:action communicate_soil_data,rover4,general,waypoint14,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_soil_analysis,rover4,waypoint14))
  :effect (communicated_soil_data,waypoint14))
 (:action communicate_soil_data,rover4,general,waypoint15,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_soil_analysis,rover4,waypoint15))
  :effect (communicated_soil_data,waypoint15))
 (:action communicate_soil_data,rover4,general,waypoint15,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_soil_analysis,rover4,waypoint15))
  :effect (communicated_soil_data,waypoint15))
 (:action communicate_soil_data,rover4,general,waypoint15,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_soil_analysis,rover4,waypoint15))
  :effect (communicated_soil_data,waypoint15))
 (:action communicate_soil_data,rover4,general,waypoint17,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_soil_analysis,rover4,waypoint17))
  :effect (communicated_soil_data,waypoint17))
 (:action communicate_soil_data,rover4,general,waypoint17,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_soil_analysis,rover4,waypoint17))
  :effect (communicated_soil_data,waypoint17))
 (:action communicate_soil_data,rover4,general,waypoint17,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_soil_analysis,rover4,waypoint17))
  :effect (communicated_soil_data,waypoint17))
 (:action communicate_soil_data,rover4,general,waypoint18,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_soil_analysis,rover4,waypoint18))
  :effect (communicated_soil_data,waypoint18))
 (:action communicate_soil_data,rover4,general,waypoint18,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_soil_analysis,rover4,waypoint18))
  :effect (communicated_soil_data,waypoint18))
 (:action communicate_soil_data,rover4,general,waypoint18,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_soil_analysis,rover4,waypoint18))
  :effect (communicated_soil_data,waypoint18))
 (:action communicate_soil_data,rover4,general,waypoint19,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_soil_analysis,rover4,waypoint19))
  :effect (communicated_soil_data,waypoint19))
 (:action communicate_soil_data,rover4,general,waypoint19,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_soil_analysis,rover4,waypoint19))
  :effect (communicated_soil_data,waypoint19))
 (:action communicate_soil_data,rover4,general,waypoint19,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_soil_analysis,rover4,waypoint19))
  :effect (communicated_soil_data,waypoint19))
 (:action communicate_rock_data,rover0,general,waypoint1,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_rock_analysis,rover0,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover0,general,waypoint1,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_rock_analysis,rover0,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover0,general,waypoint1,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_rock_analysis,rover0,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover0,general,waypoint2,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_rock_analysis,rover0,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover0,general,waypoint2,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_rock_analysis,rover0,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover0,general,waypoint2,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_rock_analysis,rover0,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover0,general,waypoint4,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_rock_analysis,rover0,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover0,general,waypoint4,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_rock_analysis,rover0,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover0,general,waypoint4,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_rock_analysis,rover0,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover0,general,waypoint5,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_rock_analysis,rover0,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover0,general,waypoint5,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_rock_analysis,rover0,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover0,general,waypoint5,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_rock_analysis,rover0,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover0,general,waypoint6,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_rock_analysis,rover0,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover0,general,waypoint6,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_rock_analysis,rover0,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover0,general,waypoint6,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_rock_analysis,rover0,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover0,general,waypoint7,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_rock_analysis,rover0,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover0,general,waypoint7,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_rock_analysis,rover0,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover0,general,waypoint7,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_rock_analysis,rover0,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover0,general,waypoint9,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_rock_analysis,rover0,waypoint9))
  :effect (communicated_rock_data,waypoint9))
 (:action communicate_rock_data,rover0,general,waypoint9,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_rock_analysis,rover0,waypoint9))
  :effect (communicated_rock_data,waypoint9))
 (:action communicate_rock_data,rover0,general,waypoint9,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_rock_analysis,rover0,waypoint9))
  :effect (communicated_rock_data,waypoint9))
 (:action communicate_rock_data,rover0,general,waypoint12,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_rock_analysis,rover0,waypoint12))
  :effect (communicated_rock_data,waypoint12))
 (:action communicate_rock_data,rover0,general,waypoint12,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_rock_analysis,rover0,waypoint12))
  :effect (communicated_rock_data,waypoint12))
 (:action communicate_rock_data,rover0,general,waypoint12,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_rock_analysis,rover0,waypoint12))
  :effect (communicated_rock_data,waypoint12))
 (:action communicate_rock_data,rover0,general,waypoint13,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_rock_analysis,rover0,waypoint13))
  :effect (communicated_rock_data,waypoint13))
 (:action communicate_rock_data,rover0,general,waypoint13,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_rock_analysis,rover0,waypoint13))
  :effect (communicated_rock_data,waypoint13))
 (:action communicate_rock_data,rover0,general,waypoint13,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_rock_analysis,rover0,waypoint13))
  :effect (communicated_rock_data,waypoint13))
 (:action communicate_rock_data,rover0,general,waypoint18,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_rock_analysis,rover0,waypoint18))
  :effect (communicated_rock_data,waypoint18))
 (:action communicate_rock_data,rover0,general,waypoint18,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_rock_analysis,rover0,waypoint18))
  :effect (communicated_rock_data,waypoint18))
 (:action communicate_rock_data,rover0,general,waypoint18,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_rock_analysis,rover0,waypoint18))
  :effect (communicated_rock_data,waypoint18))
 (:action communicate_rock_data,rover1,general,waypoint1,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover1,general,waypoint1,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_rock_analysis,rover1,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover1,general,waypoint1,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_rock_analysis,rover1,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover1,general,waypoint2,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover1,general,waypoint2,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_rock_analysis,rover1,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover1,general,waypoint2,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_rock_analysis,rover1,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover1,general,waypoint4,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover1,general,waypoint4,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_rock_analysis,rover1,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover1,general,waypoint4,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_rock_analysis,rover1,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover1,general,waypoint5,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover1,general,waypoint5,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_rock_analysis,rover1,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover1,general,waypoint5,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_rock_analysis,rover1,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover1,general,waypoint6,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover1,general,waypoint6,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_rock_analysis,rover1,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover1,general,waypoint6,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_rock_analysis,rover1,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover1,general,waypoint7,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover1,general,waypoint7,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_rock_analysis,rover1,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover1,general,waypoint7,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_rock_analysis,rover1,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover1,general,waypoint9,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint9))
  :effect (communicated_rock_data,waypoint9))
 (:action communicate_rock_data,rover1,general,waypoint9,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_rock_analysis,rover1,waypoint9))
  :effect (communicated_rock_data,waypoint9))
 (:action communicate_rock_data,rover1,general,waypoint9,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_rock_analysis,rover1,waypoint9))
  :effect (communicated_rock_data,waypoint9))
 (:action communicate_rock_data,rover1,general,waypoint12,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint12))
  :effect (communicated_rock_data,waypoint12))
 (:action communicate_rock_data,rover1,general,waypoint12,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_rock_analysis,rover1,waypoint12))
  :effect (communicated_rock_data,waypoint12))
 (:action communicate_rock_data,rover1,general,waypoint12,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_rock_analysis,rover1,waypoint12))
  :effect (communicated_rock_data,waypoint12))
 (:action communicate_rock_data,rover1,general,waypoint13,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint13))
  :effect (communicated_rock_data,waypoint13))
 (:action communicate_rock_data,rover1,general,waypoint13,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_rock_analysis,rover1,waypoint13))
  :effect (communicated_rock_data,waypoint13))
 (:action communicate_rock_data,rover1,general,waypoint13,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_rock_analysis,rover1,waypoint13))
  :effect (communicated_rock_data,waypoint13))
 (:action communicate_rock_data,rover1,general,waypoint17,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint17))
  :effect (communicated_rock_data,waypoint17))
 (:action communicate_rock_data,rover1,general,waypoint17,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_rock_analysis,rover1,waypoint17))
  :effect (communicated_rock_data,waypoint17))
 (:action communicate_rock_data,rover1,general,waypoint17,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_rock_analysis,rover1,waypoint17))
  :effect (communicated_rock_data,waypoint17))
 (:action communicate_rock_data,rover1,general,waypoint18,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_rock_analysis,rover1,waypoint18))
  :effect (communicated_rock_data,waypoint18))
 (:action communicate_rock_data,rover1,general,waypoint18,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_rock_analysis,rover1,waypoint18))
  :effect (communicated_rock_data,waypoint18))
 (:action communicate_rock_data,rover1,general,waypoint18,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_rock_analysis,rover1,waypoint18))
  :effect (communicated_rock_data,waypoint18))
 (:action communicate_rock_data,rover3,general,waypoint1,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_rock_analysis,rover3,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover3,general,waypoint1,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_rock_analysis,rover3,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover3,general,waypoint1,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_rock_analysis,rover3,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover3,general,waypoint2,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_rock_analysis,rover3,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover3,general,waypoint2,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_rock_analysis,rover3,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover3,general,waypoint2,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_rock_analysis,rover3,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover3,general,waypoint4,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_rock_analysis,rover3,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover3,general,waypoint4,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_rock_analysis,rover3,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover3,general,waypoint4,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_rock_analysis,rover3,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover3,general,waypoint5,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_rock_analysis,rover3,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover3,general,waypoint5,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_rock_analysis,rover3,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover3,general,waypoint5,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_rock_analysis,rover3,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover3,general,waypoint6,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_rock_analysis,rover3,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover3,general,waypoint6,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_rock_analysis,rover3,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover3,general,waypoint6,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_rock_analysis,rover3,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover3,general,waypoint7,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_rock_analysis,rover3,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover3,general,waypoint7,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_rock_analysis,rover3,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover3,general,waypoint7,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_rock_analysis,rover3,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover3,general,waypoint9,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_rock_analysis,rover3,waypoint9))
  :effect (communicated_rock_data,waypoint9))
 (:action communicate_rock_data,rover3,general,waypoint9,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_rock_analysis,rover3,waypoint9))
  :effect (communicated_rock_data,waypoint9))
 (:action communicate_rock_data,rover3,general,waypoint9,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_rock_analysis,rover3,waypoint9))
  :effect (communicated_rock_data,waypoint9))
 (:action communicate_rock_data,rover3,general,waypoint12,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_rock_analysis,rover3,waypoint12))
  :effect (communicated_rock_data,waypoint12))
 (:action communicate_rock_data,rover3,general,waypoint12,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_rock_analysis,rover3,waypoint12))
  :effect (communicated_rock_data,waypoint12))
 (:action communicate_rock_data,rover3,general,waypoint12,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_rock_analysis,rover3,waypoint12))
  :effect (communicated_rock_data,waypoint12))
 (:action communicate_rock_data,rover3,general,waypoint13,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_rock_analysis,rover3,waypoint13))
  :effect (communicated_rock_data,waypoint13))
 (:action communicate_rock_data,rover3,general,waypoint13,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_rock_analysis,rover3,waypoint13))
  :effect (communicated_rock_data,waypoint13))
 (:action communicate_rock_data,rover3,general,waypoint13,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_rock_analysis,rover3,waypoint13))
  :effect (communicated_rock_data,waypoint13))
 (:action communicate_rock_data,rover3,general,waypoint17,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_rock_analysis,rover3,waypoint17))
  :effect (communicated_rock_data,waypoint17))
 (:action communicate_rock_data,rover3,general,waypoint17,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_rock_analysis,rover3,waypoint17))
  :effect (communicated_rock_data,waypoint17))
 (:action communicate_rock_data,rover3,general,waypoint17,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_rock_analysis,rover3,waypoint17))
  :effect (communicated_rock_data,waypoint17))
 (:action communicate_rock_data,rover3,general,waypoint18,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_rock_analysis,rover3,waypoint18))
  :effect (communicated_rock_data,waypoint18))
 (:action communicate_rock_data,rover3,general,waypoint18,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_rock_analysis,rover3,waypoint18))
  :effect (communicated_rock_data,waypoint18))
 (:action communicate_rock_data,rover3,general,waypoint18,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_rock_analysis,rover3,waypoint18))
  :effect (communicated_rock_data,waypoint18))
 (:action communicate_rock_data,rover5,general,waypoint1,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_rock_analysis,rover5,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover5,general,waypoint1,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_rock_analysis,rover5,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover5,general,waypoint1,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_rock_analysis,rover5,waypoint1))
  :effect (communicated_rock_data,waypoint1))
 (:action communicate_rock_data,rover5,general,waypoint2,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_rock_analysis,rover5,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover5,general,waypoint2,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_rock_analysis,rover5,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover5,general,waypoint2,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_rock_analysis,rover5,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover5,general,waypoint4,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_rock_analysis,rover5,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover5,general,waypoint4,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_rock_analysis,rover5,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover5,general,waypoint4,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_rock_analysis,rover5,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover5,general,waypoint5,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_rock_analysis,rover5,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover5,general,waypoint5,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_rock_analysis,rover5,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover5,general,waypoint5,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_rock_analysis,rover5,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover5,general,waypoint6,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_rock_analysis,rover5,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover5,general,waypoint6,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_rock_analysis,rover5,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover5,general,waypoint6,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_rock_analysis,rover5,waypoint6))
  :effect (communicated_rock_data,waypoint6))
 (:action communicate_rock_data,rover5,general,waypoint7,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_rock_analysis,rover5,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover5,general,waypoint7,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_rock_analysis,rover5,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover5,general,waypoint7,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_rock_analysis,rover5,waypoint7))
  :effect (communicated_rock_data,waypoint7))
 (:action communicate_rock_data,rover5,general,waypoint9,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_rock_analysis,rover5,waypoint9))
  :effect (communicated_rock_data,waypoint9))
 (:action communicate_rock_data,rover5,general,waypoint9,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_rock_analysis,rover5,waypoint9))
  :effect (communicated_rock_data,waypoint9))
 (:action communicate_rock_data,rover5,general,waypoint9,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_rock_analysis,rover5,waypoint9))
  :effect (communicated_rock_data,waypoint9))
 (:action communicate_rock_data,rover5,general,waypoint12,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_rock_analysis,rover5,waypoint12))
  :effect (communicated_rock_data,waypoint12))
 (:action communicate_rock_data,rover5,general,waypoint12,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_rock_analysis,rover5,waypoint12))
  :effect (communicated_rock_data,waypoint12))
 (:action communicate_rock_data,rover5,general,waypoint12,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_rock_analysis,rover5,waypoint12))
  :effect (communicated_rock_data,waypoint12))
 (:action communicate_rock_data,rover5,general,waypoint13,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_rock_analysis,rover5,waypoint13))
  :effect (communicated_rock_data,waypoint13))
 (:action communicate_rock_data,rover5,general,waypoint13,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_rock_analysis,rover5,waypoint13))
  :effect (communicated_rock_data,waypoint13))
 (:action communicate_rock_data,rover5,general,waypoint13,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_rock_analysis,rover5,waypoint13))
  :effect (communicated_rock_data,waypoint13))
 (:action communicate_rock_data,rover5,general,waypoint17,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_rock_analysis,rover5,waypoint17))
  :effect (communicated_rock_data,waypoint17))
 (:action communicate_rock_data,rover5,general,waypoint17,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_rock_analysis,rover5,waypoint17))
  :effect (communicated_rock_data,waypoint17))
 (:action communicate_rock_data,rover5,general,waypoint17,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_rock_analysis,rover5,waypoint17))
  :effect (communicated_rock_data,waypoint17))
 (:action communicate_rock_data,rover5,general,waypoint18,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_rock_analysis,rover5,waypoint18))
  :effect (communicated_rock_data,waypoint18))
 (:action communicate_rock_data,rover5,general,waypoint18,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_rock_analysis,rover5,waypoint18))
  :effect (communicated_rock_data,waypoint18))
 (:action communicate_rock_data,rover5,general,waypoint18,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_rock_analysis,rover5,waypoint18))
  :effect (communicated_rock_data,waypoint18))
 (:action communicate_image_data,rover0,general,objective0,colour,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_image,rover0,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover0,general,objective0,colour,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_image,rover0,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover0,general,objective0,colour,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_image,rover0,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover0,general,objective1,colour,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_image,rover0,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover0,general,objective1,colour,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_image,rover0,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover0,general,objective1,colour,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_image,rover0,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover0,general,objective2,colour,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_image,rover0,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover0,general,objective2,colour,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_image,rover0,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover0,general,objective2,colour,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_image,rover0,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover0,general,objective3,colour,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_image,rover0,objective3,colour))
  :effect (communicated_image_data,objective3,colour))
 (:action communicate_image_data,rover0,general,objective3,colour,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_image,rover0,objective3,colour))
  :effect (communicated_image_data,objective3,colour))
 (:action communicate_image_data,rover0,general,objective3,colour,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_image,rover0,objective3,colour))
  :effect (communicated_image_data,objective3,colour))
 (:action communicate_image_data,rover0,general,objective4,colour,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_image,rover0,objective4,colour))
  :effect (communicated_image_data,objective4,colour))
 (:action communicate_image_data,rover0,general,objective4,colour,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_image,rover0,objective4,colour))
  :effect (communicated_image_data,objective4,colour))
 (:action communicate_image_data,rover0,general,objective4,colour,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_image,rover0,objective4,colour))
  :effect (communicated_image_data,objective4,colour))
 (:action communicate_image_data,rover0,general,objective5,colour,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_image,rover0,objective5,colour))
  :effect (communicated_image_data,objective5,colour))
 (:action communicate_image_data,rover0,general,objective5,colour,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_image,rover0,objective5,colour))
  :effect (communicated_image_data,objective5,colour))
 (:action communicate_image_data,rover0,general,objective5,colour,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_image,rover0,objective5,colour))
  :effect (communicated_image_data,objective5,colour))
 (:action communicate_image_data,rover0,general,objective6,colour,waypoint0,waypoint17
  :precondition (and (at,rover0,waypoint0) (have_image,rover0,objective6,colour))
  :effect (communicated_image_data,objective6,colour))
 (:action communicate_image_data,rover0,general,objective6,colour,waypoint11,waypoint17
  :precondition (and (at,rover0,waypoint11) (have_image,rover0,objective6,colour))
  :effect (communicated_image_data,objective6,colour))
 (:action communicate_image_data,rover0,general,objective6,colour,waypoint19,waypoint17
  :precondition (and (at,rover0,waypoint19) (have_image,rover0,objective6,colour))
  :effect (communicated_image_data,objective6,colour))
 (:action communicate_image_data,rover1,general,objective0,high_res,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_image,rover1,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover1,general,objective0,high_res,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_image,rover1,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover1,general,objective0,high_res,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_image,rover1,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover1,general,objective1,high_res,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_image,rover1,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover1,general,objective1,high_res,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_image,rover1,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover1,general,objective1,high_res,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_image,rover1,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover1,general,objective2,high_res,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_image,rover1,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover1,general,objective2,high_res,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_image,rover1,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover1,general,objective2,high_res,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_image,rover1,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover1,general,objective3,high_res,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_image,rover1,objective3,high_res))
  :effect (communicated_image_data,objective3,high_res))
 (:action communicate_image_data,rover1,general,objective3,high_res,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_image,rover1,objective3,high_res))
  :effect (communicated_image_data,objective3,high_res))
 (:action communicate_image_data,rover1,general,objective3,high_res,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_image,rover1,objective3,high_res))
  :effect (communicated_image_data,objective3,high_res))
 (:action communicate_image_data,rover1,general,objective4,high_res,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_image,rover1,objective4,high_res))
  :effect (communicated_image_data,objective4,high_res))
 (:action communicate_image_data,rover1,general,objective4,high_res,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_image,rover1,objective4,high_res))
  :effect (communicated_image_data,objective4,high_res))
 (:action communicate_image_data,rover1,general,objective4,high_res,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_image,rover1,objective4,high_res))
  :effect (communicated_image_data,objective4,high_res))
 (:action communicate_image_data,rover1,general,objective5,high_res,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_image,rover1,objective5,high_res))
  :effect (communicated_image_data,objective5,high_res))
 (:action communicate_image_data,rover1,general,objective5,high_res,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_image,rover1,objective5,high_res))
  :effect (communicated_image_data,objective5,high_res))
 (:action communicate_image_data,rover1,general,objective5,high_res,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_image,rover1,objective5,high_res))
  :effect (communicated_image_data,objective5,high_res))
 (:action communicate_image_data,rover1,general,objective6,high_res,waypoint0,waypoint17
  :precondition (and (at,rover1,waypoint0) (have_image,rover1,objective6,high_res))
  :effect (communicated_image_data,objective6,high_res))
 (:action communicate_image_data,rover1,general,objective6,high_res,waypoint11,waypoint17
  :precondition (and (at,rover1,waypoint11) (have_image,rover1,objective6,high_res))
  :effect (communicated_image_data,objective6,high_res))
 (:action communicate_image_data,rover1,general,objective6,high_res,waypoint19,waypoint17
  :precondition (and (at,rover1,waypoint19) (have_image,rover1,objective6,high_res))
  :effect (communicated_image_data,objective6,high_res))
 (:action communicate_image_data,rover2,general,objective0,colour,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover2,general,objective0,colour,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_image,rover2,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover2,general,objective0,colour,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_image,rover2,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover2,general,objective0,low_res,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover2,general,objective0,low_res,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_image,rover2,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover2,general,objective0,low_res,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_image,rover2,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover2,general,objective1,colour,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover2,general,objective1,colour,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_image,rover2,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover2,general,objective1,colour,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_image,rover2,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover2,general,objective1,low_res,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover2,general,objective1,low_res,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_image,rover2,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover2,general,objective1,low_res,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_image,rover2,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover2,general,objective2,colour,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover2,general,objective2,colour,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_image,rover2,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover2,general,objective2,colour,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_image,rover2,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover2,general,objective2,low_res,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover2,general,objective2,low_res,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_image,rover2,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover2,general,objective2,low_res,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_image,rover2,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover2,general,objective3,colour,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective3,colour))
  :effect (communicated_image_data,objective3,colour))
 (:action communicate_image_data,rover2,general,objective3,colour,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_image,rover2,objective3,colour))
  :effect (communicated_image_data,objective3,colour))
 (:action communicate_image_data,rover2,general,objective3,colour,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_image,rover2,objective3,colour))
  :effect (communicated_image_data,objective3,colour))
 (:action communicate_image_data,rover2,general,objective3,low_res,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective3,low_res))
  :effect (communicated_image_data,objective3,low_res))
 (:action communicate_image_data,rover2,general,objective3,low_res,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_image,rover2,objective3,low_res))
  :effect (communicated_image_data,objective3,low_res))
 (:action communicate_image_data,rover2,general,objective3,low_res,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_image,rover2,objective3,low_res))
  :effect (communicated_image_data,objective3,low_res))
 (:action communicate_image_data,rover2,general,objective4,colour,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective4,colour))
  :effect (communicated_image_data,objective4,colour))
 (:action communicate_image_data,rover2,general,objective4,colour,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_image,rover2,objective4,colour))
  :effect (communicated_image_data,objective4,colour))
 (:action communicate_image_data,rover2,general,objective4,colour,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_image,rover2,objective4,colour))
  :effect (communicated_image_data,objective4,colour))
 (:action communicate_image_data,rover2,general,objective4,low_res,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective4,low_res))
  :effect (communicated_image_data,objective4,low_res))
 (:action communicate_image_data,rover2,general,objective4,low_res,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_image,rover2,objective4,low_res))
  :effect (communicated_image_data,objective4,low_res))
 (:action communicate_image_data,rover2,general,objective4,low_res,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_image,rover2,objective4,low_res))
  :effect (communicated_image_data,objective4,low_res))
 (:action communicate_image_data,rover2,general,objective5,colour,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective5,colour))
  :effect (communicated_image_data,objective5,colour))
 (:action communicate_image_data,rover2,general,objective5,colour,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_image,rover2,objective5,colour))
  :effect (communicated_image_data,objective5,colour))
 (:action communicate_image_data,rover2,general,objective5,colour,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_image,rover2,objective5,colour))
  :effect (communicated_image_data,objective5,colour))
 (:action communicate_image_data,rover2,general,objective5,low_res,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective5,low_res))
  :effect (communicated_image_data,objective5,low_res))
 (:action communicate_image_data,rover2,general,objective5,low_res,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_image,rover2,objective5,low_res))
  :effect (communicated_image_data,objective5,low_res))
 (:action communicate_image_data,rover2,general,objective5,low_res,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_image,rover2,objective5,low_res))
  :effect (communicated_image_data,objective5,low_res))
 (:action communicate_image_data,rover2,general,objective6,colour,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective6,colour))
  :effect (communicated_image_data,objective6,colour))
 (:action communicate_image_data,rover2,general,objective6,colour,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_image,rover2,objective6,colour))
  :effect (communicated_image_data,objective6,colour))
 (:action communicate_image_data,rover2,general,objective6,colour,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_image,rover2,objective6,colour))
  :effect (communicated_image_data,objective6,colour))
 (:action communicate_image_data,rover2,general,objective6,low_res,waypoint0,waypoint17
  :precondition (and (at,rover2,waypoint0) (have_image,rover2,objective6,low_res))
  :effect (communicated_image_data,objective6,low_res))
 (:action communicate_image_data,rover2,general,objective6,low_res,waypoint11,waypoint17
  :precondition (and (at,rover2,waypoint11) (have_image,rover2,objective6,low_res))
  :effect (communicated_image_data,objective6,low_res))
 (:action communicate_image_data,rover2,general,objective6,low_res,waypoint19,waypoint17
  :precondition (and (at,rover2,waypoint19) (have_image,rover2,objective6,low_res))
  :effect (communicated_image_data,objective6,low_res))
 (:action communicate_image_data,rover3,general,objective0,colour,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover3,general,objective0,colour,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover3,general,objective0,colour,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover3,general,objective0,high_res,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover3,general,objective0,high_res,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover3,general,objective0,high_res,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover3,general,objective0,low_res,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover3,general,objective0,low_res,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover3,general,objective0,low_res,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover3,general,objective1,colour,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover3,general,objective1,colour,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover3,general,objective1,colour,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover3,general,objective1,high_res,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover3,general,objective1,high_res,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover3,general,objective1,high_res,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover3,general,objective1,low_res,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover3,general,objective1,low_res,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover3,general,objective1,low_res,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover3,general,objective2,colour,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover3,general,objective2,colour,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover3,general,objective2,colour,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover3,general,objective2,high_res,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover3,general,objective2,high_res,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover3,general,objective2,high_res,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover3,general,objective2,low_res,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover3,general,objective2,low_res,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover3,general,objective2,low_res,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover3,general,objective3,colour,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective3,colour))
  :effect (communicated_image_data,objective3,colour))
 (:action communicate_image_data,rover3,general,objective3,colour,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective3,colour))
  :effect (communicated_image_data,objective3,colour))
 (:action communicate_image_data,rover3,general,objective3,colour,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective3,colour))
  :effect (communicated_image_data,objective3,colour))
 (:action communicate_image_data,rover3,general,objective3,high_res,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective3,high_res))
  :effect (communicated_image_data,objective3,high_res))
 (:action communicate_image_data,rover3,general,objective3,high_res,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective3,high_res))
  :effect (communicated_image_data,objective3,high_res))
 (:action communicate_image_data,rover3,general,objective3,high_res,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective3,high_res))
  :effect (communicated_image_data,objective3,high_res))
 (:action communicate_image_data,rover3,general,objective3,low_res,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective3,low_res))
  :effect (communicated_image_data,objective3,low_res))
 (:action communicate_image_data,rover3,general,objective3,low_res,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective3,low_res))
  :effect (communicated_image_data,objective3,low_res))
 (:action communicate_image_data,rover3,general,objective3,low_res,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective3,low_res))
  :effect (communicated_image_data,objective3,low_res))
 (:action communicate_image_data,rover3,general,objective4,colour,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective4,colour))
  :effect (communicated_image_data,objective4,colour))
 (:action communicate_image_data,rover3,general,objective4,colour,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective4,colour))
  :effect (communicated_image_data,objective4,colour))
 (:action communicate_image_data,rover3,general,objective4,colour,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective4,colour))
  :effect (communicated_image_data,objective4,colour))
 (:action communicate_image_data,rover3,general,objective4,high_res,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective4,high_res))
  :effect (communicated_image_data,objective4,high_res))
 (:action communicate_image_data,rover3,general,objective4,high_res,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective4,high_res))
  :effect (communicated_image_data,objective4,high_res))
 (:action communicate_image_data,rover3,general,objective4,high_res,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective4,high_res))
  :effect (communicated_image_data,objective4,high_res))
 (:action communicate_image_data,rover3,general,objective4,low_res,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective4,low_res))
  :effect (communicated_image_data,objective4,low_res))
 (:action communicate_image_data,rover3,general,objective4,low_res,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective4,low_res))
  :effect (communicated_image_data,objective4,low_res))
 (:action communicate_image_data,rover3,general,objective4,low_res,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective4,low_res))
  :effect (communicated_image_data,objective4,low_res))
 (:action communicate_image_data,rover3,general,objective5,colour,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective5,colour))
  :effect (communicated_image_data,objective5,colour))
 (:action communicate_image_data,rover3,general,objective5,colour,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective5,colour))
  :effect (communicated_image_data,objective5,colour))
 (:action communicate_image_data,rover3,general,objective5,colour,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective5,colour))
  :effect (communicated_image_data,objective5,colour))
 (:action communicate_image_data,rover3,general,objective5,high_res,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective5,high_res))
  :effect (communicated_image_data,objective5,high_res))
 (:action communicate_image_data,rover3,general,objective5,high_res,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective5,high_res))
  :effect (communicated_image_data,objective5,high_res))
 (:action communicate_image_data,rover3,general,objective5,high_res,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective5,high_res))
  :effect (communicated_image_data,objective5,high_res))
 (:action communicate_image_data,rover3,general,objective5,low_res,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective5,low_res))
  :effect (communicated_image_data,objective5,low_res))
 (:action communicate_image_data,rover3,general,objective5,low_res,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective5,low_res))
  :effect (communicated_image_data,objective5,low_res))
 (:action communicate_image_data,rover3,general,objective5,low_res,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective5,low_res))
  :effect (communicated_image_data,objective5,low_res))
 (:action communicate_image_data,rover3,general,objective6,colour,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective6,colour))
  :effect (communicated_image_data,objective6,colour))
 (:action communicate_image_data,rover3,general,objective6,colour,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective6,colour))
  :effect (communicated_image_data,objective6,colour))
 (:action communicate_image_data,rover3,general,objective6,colour,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective6,colour))
  :effect (communicated_image_data,objective6,colour))
 (:action communicate_image_data,rover3,general,objective6,high_res,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective6,high_res))
  :effect (communicated_image_data,objective6,high_res))
 (:action communicate_image_data,rover3,general,objective6,high_res,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective6,high_res))
  :effect (communicated_image_data,objective6,high_res))
 (:action communicate_image_data,rover3,general,objective6,high_res,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective6,high_res))
  :effect (communicated_image_data,objective6,high_res))
 (:action communicate_image_data,rover3,general,objective6,low_res,waypoint0,waypoint17
  :precondition (and (at,rover3,waypoint0) (have_image,rover3,objective6,low_res))
  :effect (communicated_image_data,objective6,low_res))
 (:action communicate_image_data,rover3,general,objective6,low_res,waypoint11,waypoint17
  :precondition (and (at,rover3,waypoint11) (have_image,rover3,objective6,low_res))
  :effect (communicated_image_data,objective6,low_res))
 (:action communicate_image_data,rover3,general,objective6,low_res,waypoint19,waypoint17
  :precondition (and (at,rover3,waypoint19) (have_image,rover3,objective6,low_res))
  :effect (communicated_image_data,objective6,low_res))
 (:action communicate_image_data,rover4,general,objective0,colour,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_image,rover4,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover4,general,objective0,colour,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_image,rover4,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover4,general,objective0,colour,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_image,rover4,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover4,general,objective0,high_res,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_image,rover4,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover4,general,objective0,high_res,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_image,rover4,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover4,general,objective0,high_res,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_image,rover4,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover4,general,objective1,colour,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_image,rover4,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover4,general,objective1,colour,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_image,rover4,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover4,general,objective1,colour,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_image,rover4,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover4,general,objective1,high_res,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_image,rover4,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover4,general,objective1,high_res,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_image,rover4,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover4,general,objective1,high_res,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_image,rover4,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover4,general,objective2,colour,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_image,rover4,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover4,general,objective2,colour,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_image,rover4,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover4,general,objective2,colour,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_image,rover4,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover4,general,objective2,high_res,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_image,rover4,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover4,general,objective2,high_res,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_image,rover4,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover4,general,objective2,high_res,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_image,rover4,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover4,general,objective3,colour,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_image,rover4,objective3,colour))
  :effect (communicated_image_data,objective3,colour))
 (:action communicate_image_data,rover4,general,objective3,colour,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_image,rover4,objective3,colour))
  :effect (communicated_image_data,objective3,colour))
 (:action communicate_image_data,rover4,general,objective3,colour,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_image,rover4,objective3,colour))
  :effect (communicated_image_data,objective3,colour))
 (:action communicate_image_data,rover4,general,objective3,high_res,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_image,rover4,objective3,high_res))
  :effect (communicated_image_data,objective3,high_res))
 (:action communicate_image_data,rover4,general,objective3,high_res,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_image,rover4,objective3,high_res))
  :effect (communicated_image_data,objective3,high_res))
 (:action communicate_image_data,rover4,general,objective3,high_res,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_image,rover4,objective3,high_res))
  :effect (communicated_image_data,objective3,high_res))
 (:action communicate_image_data,rover4,general,objective4,colour,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_image,rover4,objective4,colour))
  :effect (communicated_image_data,objective4,colour))
 (:action communicate_image_data,rover4,general,objective4,colour,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_image,rover4,objective4,colour))
  :effect (communicated_image_data,objective4,colour))
 (:action communicate_image_data,rover4,general,objective4,colour,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_image,rover4,objective4,colour))
  :effect (communicated_image_data,objective4,colour))
 (:action communicate_image_data,rover4,general,objective4,high_res,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_image,rover4,objective4,high_res))
  :effect (communicated_image_data,objective4,high_res))
 (:action communicate_image_data,rover4,general,objective4,high_res,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_image,rover4,objective4,high_res))
  :effect (communicated_image_data,objective4,high_res))
 (:action communicate_image_data,rover4,general,objective4,high_res,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_image,rover4,objective4,high_res))
  :effect (communicated_image_data,objective4,high_res))
 (:action communicate_image_data,rover4,general,objective5,colour,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_image,rover4,objective5,colour))
  :effect (communicated_image_data,objective5,colour))
 (:action communicate_image_data,rover4,general,objective5,colour,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_image,rover4,objective5,colour))
  :effect (communicated_image_data,objective5,colour))
 (:action communicate_image_data,rover4,general,objective5,colour,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_image,rover4,objective5,colour))
  :effect (communicated_image_data,objective5,colour))
 (:action communicate_image_data,rover4,general,objective5,high_res,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_image,rover4,objective5,high_res))
  :effect (communicated_image_data,objective5,high_res))
 (:action communicate_image_data,rover4,general,objective5,high_res,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_image,rover4,objective5,high_res))
  :effect (communicated_image_data,objective5,high_res))
 (:action communicate_image_data,rover4,general,objective5,high_res,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_image,rover4,objective5,high_res))
  :effect (communicated_image_data,objective5,high_res))
 (:action communicate_image_data,rover4,general,objective6,colour,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_image,rover4,objective6,colour))
  :effect (communicated_image_data,objective6,colour))
 (:action communicate_image_data,rover4,general,objective6,colour,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_image,rover4,objective6,colour))
  :effect (communicated_image_data,objective6,colour))
 (:action communicate_image_data,rover4,general,objective6,colour,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_image,rover4,objective6,colour))
  :effect (communicated_image_data,objective6,colour))
 (:action communicate_image_data,rover4,general,objective6,high_res,waypoint0,waypoint17
  :precondition (and (at,rover4,waypoint0) (have_image,rover4,objective6,high_res))
  :effect (communicated_image_data,objective6,high_res))
 (:action communicate_image_data,rover4,general,objective6,high_res,waypoint11,waypoint17
  :precondition (and (at,rover4,waypoint11) (have_image,rover4,objective6,high_res))
  :effect (communicated_image_data,objective6,high_res))
 (:action communicate_image_data,rover4,general,objective6,high_res,waypoint19,waypoint17
  :precondition (and (at,rover4,waypoint19) (have_image,rover4,objective6,high_res))
  :effect (communicated_image_data,objective6,high_res))
 (:action communicate_image_data,rover5,general,objective0,high_res,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_image,rover5,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover5,general,objective0,high_res,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_image,rover5,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover5,general,objective0,high_res,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_image,rover5,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover5,general,objective0,low_res,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_image,rover5,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover5,general,objective0,low_res,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_image,rover5,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover5,general,objective0,low_res,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_image,rover5,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover5,general,objective1,high_res,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_image,rover5,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover5,general,objective1,high_res,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_image,rover5,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover5,general,objective1,high_res,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_image,rover5,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover5,general,objective1,low_res,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_image,rover5,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover5,general,objective1,low_res,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_image,rover5,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover5,general,objective1,low_res,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_image,rover5,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover5,general,objective2,high_res,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_image,rover5,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover5,general,objective2,high_res,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_image,rover5,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover5,general,objective2,high_res,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_image,rover5,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover5,general,objective2,low_res,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_image,rover5,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover5,general,objective2,low_res,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_image,rover5,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover5,general,objective2,low_res,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_image,rover5,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover5,general,objective3,high_res,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_image,rover5,objective3,high_res))
  :effect (communicated_image_data,objective3,high_res))
 (:action communicate_image_data,rover5,general,objective3,high_res,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_image,rover5,objective3,high_res))
  :effect (communicated_image_data,objective3,high_res))
 (:action communicate_image_data,rover5,general,objective3,high_res,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_image,rover5,objective3,high_res))
  :effect (communicated_image_data,objective3,high_res))
 (:action communicate_image_data,rover5,general,objective3,low_res,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_image,rover5,objective3,low_res))
  :effect (communicated_image_data,objective3,low_res))
 (:action communicate_image_data,rover5,general,objective3,low_res,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_image,rover5,objective3,low_res))
  :effect (communicated_image_data,objective3,low_res))
 (:action communicate_image_data,rover5,general,objective3,low_res,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_image,rover5,objective3,low_res))
  :effect (communicated_image_data,objective3,low_res))
 (:action communicate_image_data,rover5,general,objective4,high_res,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_image,rover5,objective4,high_res))
  :effect (communicated_image_data,objective4,high_res))
 (:action communicate_image_data,rover5,general,objective4,high_res,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_image,rover5,objective4,high_res))
  :effect (communicated_image_data,objective4,high_res))
 (:action communicate_image_data,rover5,general,objective4,high_res,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_image,rover5,objective4,high_res))
  :effect (communicated_image_data,objective4,high_res))
 (:action communicate_image_data,rover5,general,objective4,low_res,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_image,rover5,objective4,low_res))
  :effect (communicated_image_data,objective4,low_res))
 (:action communicate_image_data,rover5,general,objective4,low_res,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_image,rover5,objective4,low_res))
  :effect (communicated_image_data,objective4,low_res))
 (:action communicate_image_data,rover5,general,objective4,low_res,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_image,rover5,objective4,low_res))
  :effect (communicated_image_data,objective4,low_res))
 (:action communicate_image_data,rover5,general,objective5,high_res,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_image,rover5,objective5,high_res))
  :effect (communicated_image_data,objective5,high_res))
 (:action communicate_image_data,rover5,general,objective5,high_res,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_image,rover5,objective5,high_res))
  :effect (communicated_image_data,objective5,high_res))
 (:action communicate_image_data,rover5,general,objective5,high_res,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_image,rover5,objective5,high_res))
  :effect (communicated_image_data,objective5,high_res))
 (:action communicate_image_data,rover5,general,objective5,low_res,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_image,rover5,objective5,low_res))
  :effect (communicated_image_data,objective5,low_res))
 (:action communicate_image_data,rover5,general,objective5,low_res,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_image,rover5,objective5,low_res))
  :effect (communicated_image_data,objective5,low_res))
 (:action communicate_image_data,rover5,general,objective5,low_res,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_image,rover5,objective5,low_res))
  :effect (communicated_image_data,objective5,low_res))
 (:action communicate_image_data,rover5,general,objective6,high_res,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_image,rover5,objective6,high_res))
  :effect (communicated_image_data,objective6,high_res))
 (:action communicate_image_data,rover5,general,objective6,high_res,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_image,rover5,objective6,high_res))
  :effect (communicated_image_data,objective6,high_res))
 (:action communicate_image_data,rover5,general,objective6,high_res,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_image,rover5,objective6,high_res))
  :effect (communicated_image_data,objective6,high_res))
 (:action communicate_image_data,rover5,general,objective6,low_res,waypoint0,waypoint17
  :precondition (and (at,rover5,waypoint0) (have_image,rover5,objective6,low_res))
  :effect (communicated_image_data,objective6,low_res))
 (:action communicate_image_data,rover5,general,objective6,low_res,waypoint11,waypoint17
  :precondition (and (at,rover5,waypoint11) (have_image,rover5,objective6,low_res))
  :effect (communicated_image_data,objective6,low_res))
 (:action communicate_image_data,rover5,general,objective6,low_res,waypoint19,waypoint17
  :precondition (and (at,rover5,waypoint19) (have_image,rover5,objective6,low_res))
  :effect (communicated_image_data,objective6,low_res))
;; action partitions
)