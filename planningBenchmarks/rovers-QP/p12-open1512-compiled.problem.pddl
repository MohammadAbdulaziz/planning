(define (problem roverprob5146)
 (:domain rover)
 (:init (at,rover0,waypoint4) (at,rover1,waypoint4) (at,rover2,waypoint7) (at,rover3,waypoint7) (at_soil_sample,waypoint0) (empty,rover1store) (empty,rover2store) (empty,rover3store) (at_rock_sample,waypoint0) (at_rock_sample,waypoint2) (at_rock_sample,waypoint3) (at_rock_sample,waypoint5) (at_rock_sample,waypoint6) (at_rock_sample,waypoint7) (first_o5))
 (:goal (and (communicated_soil_data,waypoint0) (communicated_rock_data,waypoint3) (communicated_rock_data,waypoint6) (communicated_image_data,objective1,high_res) (communicated_image_data,objective2,low_res) (communicated_image_data,objective3,low_res)))
)
