(define (problem roverprob2312)
 (:domain rover)
 (:init (at,rover0,waypoint1) (at,rover1,waypoint4) (empty,rover1store) (at_soil_sample,waypoint1) (at_soil_sample,waypoint2) (at_soil_sample,waypoint3) (at_soil_sample,waypoint4) (at_soil_sample,waypoint5) (at_rock_sample,waypoint0) (empty,rover0store) (at_rock_sample,waypoint2) (at_rock_sample,waypoint3) (at_rock_sample,waypoint5) (first_o4) (first_o5))
 (:goal (and (communicated_soil_data,waypoint1) (communicated_soil_data,waypoint2) (communicated_soil_data,waypoint4) (communicated_soil_data,waypoint5) (communicated_rock_data,waypoint0) (communicated_rock_data,waypoint2) (communicated_rock_data,waypoint3) (communicated_image_data,objective0,colour) (communicated_image_data,objective0,low_res) (communicated_image_data,objective1,low_res) (ok_e4)))
)
