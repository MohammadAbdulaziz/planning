(define (problem roverprob1425)
 (:domain rover)
 (:init (at,rover0,waypoint1) (at,rover1,waypoint4) (at,rover2,waypoint0) (at,rover3,waypoint2) (empty,rover0store) (at_soil_sample,waypoint3) (at_soil_sample,waypoint4) (at_soil_sample,waypoint6) (at_soil_sample,waypoint9) (empty,rover1store) (at_rock_sample,waypoint1) (at_rock_sample,waypoint3) (at_rock_sample,waypoint4) (at_rock_sample,waypoint5) (at_rock_sample,waypoint8))
 (:goal (and (communicated_soil_data,waypoint3) (communicated_soil_data,waypoint6) (communicated_rock_data,waypoint4) (communicated_rock_data,waypoint5) (communicated_rock_data,waypoint8) (communicated_image_data,objective0,colour) (communicated_image_data,objective0,low_res) (communicated_image_data,objective2,low_res) (ok_e10)))
)
