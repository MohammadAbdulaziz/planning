(define (problem ROVERPROB4135)
 (:domain ROVER)
 (:init  (EMPTY--ROVER3STORE) (AT--ROVER3--WAYPOINT4) (EMPTY--ROVER2STORE) (AT--ROVER2--WAYPOINT6) (AT--ROVER1--WAYPOINT6) (EMPTY--ROVER0STORE) (AT--ROVER0--WAYPOINT4) (AT_ROCK_SAMPLE--WAYPOINT10) (AT_SOIL_SAMPLE--WAYPOINT10) (AT_ROCK_SAMPLE--WAYPOINT9) (AT_ROCK_SAMPLE--WAYPOINT8) (AT_SOIL_SAMPLE--WAYPOINT8) (AT_SOIL_SAMPLE--WAYPOINT7) (AT_SOIL_SAMPLE--WAYPOINT5) (AT_SOIL_SAMPLE--WAYPOINT4) (AT_ROCK_SAMPLE--WAYPOINT2) (AT_SOIL_SAMPLE--WAYPOINT2) (AT_ROCK_SAMPLE--WAYPOINT1) (AT_SOIL_SAMPLE--WAYPOINT0))
 (:goal (and (COMMUNICATED_IMAGE_DATA--OBJECTIVE1--HIGH_RES) (COMMUNICATED_IMAGE_DATA--OBJECTIVE1--LOW_RES) (COMMUNICATED_ROCK_DATA--WAYPOINT8) (COMMUNICATED_ROCK_DATA--WAYPOINT1) (COMMUNICATED_ROCK_DATA--WAYPOINT2) (COMMUNICATED_SOIL_DATA--WAYPOINT0) (COMMUNICATED_SOIL_DATA--WAYPOINT10) (COMMUNICATED_SOIL_DATA--WAYPOINT8) (COMMUNICATED_SOIL_DATA--WAYPOINT2) (COMMUNICATED_SOIL_DATA--WAYPOINT5)))
)
