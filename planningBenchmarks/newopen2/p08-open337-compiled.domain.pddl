;; rover::roverprob1423, problem open #337
;; selected constraints: {e8, o0, o3}
;; value = 88, penalty = 1512
(define (domain rover)
 (:predicates (at,rover0,waypoint0) (at,rover0,waypoint1) (at,rover0,waypoint2) (at,rover0,waypoint3) (at,rover0,waypoint4) (at,rover0,waypoint5) (at,rover1,waypoint0) (at,rover1,waypoint1) (at,rover1,waypoint2) (at,rover1,waypoint3) (at,rover1,waypoint5) (at,rover1,waypoint4) (at,rover2,waypoint0) (at,rover2,waypoint2) (at,rover2,waypoint3) (at,rover2,waypoint1) (at,rover2,waypoint4) (at,rover2,waypoint5) (at,rover3,waypoint0) (at,rover3,waypoint1) (at,rover3,waypoint3) (at,rover3,waypoint2) (at,rover3,waypoint5) (at,rover3,waypoint4) (empty,rover0store) (full,rover0store) (at_soil_sample,waypoint1) (have_soil_analysis,rover0,waypoint1) (at_soil_sample,waypoint3) (have_soil_analysis,rover0,waypoint3) (at_soil_sample,waypoint4) (have_soil_analysis,rover0,waypoint4) (empty,rover3store) (full,rover3store) (have_soil_analysis,rover3,waypoint1) (have_soil_analysis,rover3,waypoint3) (have_soil_analysis,rover3,waypoint4) (at_rock_sample,waypoint2) (have_rock_analysis,rover0,waypoint2) (at_rock_sample,waypoint3) (have_rock_analysis,rover0,waypoint3) (at_rock_sample,waypoint4) (have_rock_analysis,rover0,waypoint4) (at_rock_sample,waypoint5) (have_rock_analysis,rover0,waypoint5) (empty,rover1store) (full,rover1store) (have_rock_analysis,rover1,waypoint2) (have_rock_analysis,rover1,waypoint3) (have_rock_analysis,rover1,waypoint4) (have_rock_analysis,rover1,waypoint5) (empty,rover2store) (full,rover2store) (have_rock_analysis,rover2,waypoint2) (have_rock_analysis,rover2,waypoint3) (have_rock_analysis,rover2,waypoint4) (have_rock_analysis,rover2,waypoint5) (have_rock_analysis,rover3,waypoint2) (have_rock_analysis,rover3,waypoint3) (have_rock_analysis,rover3,waypoint4) (have_rock_analysis,rover3,waypoint5) (calibrated,camera2,rover1) (calibrated,camera3,rover2) (calibrated,camera0,rover3) (calibrated,camera1,rover3) (have_image,rover1,objective0,colour) (have_image,rover1,objective0,high_res) (have_image,rover1,objective0,low_res) (have_image,rover1,objective1,colour) (have_image,rover1,objective1,high_res) (have_image,rover1,objective1,low_res) (have_image,rover1,objective2,colour) (have_image,rover1,objective2,high_res) (have_image,rover1,objective2,low_res) (have_image,rover2,objective0,colour) (have_image,rover2,objective0,low_res) (have_image,rover2,objective1,colour) (have_image,rover2,objective1,low_res) (have_image,rover2,objective2,colour) (have_image,rover2,objective2,low_res) (have_image,rover3,objective0,colour) (have_image,rover3,objective0,high_res) (have_image,rover3,objective0,low_res) (have_image,rover3,objective1,colour) (have_image,rover3,objective1,high_res) (have_image,rover3,objective1,low_res) (have_image,rover3,objective2,colour) (have_image,rover3,objective2,high_res) (have_image,rover3,objective2,low_res) (communicated_soil_data,waypoint1) (communicated_soil_data,waypoint3) (communicated_soil_data,waypoint4) (communicated_rock_data,waypoint2) (communicated_rock_data,waypoint3) (communicated_rock_data,waypoint4) (communicated_rock_data,waypoint5) (communicated_image_data,objective0,colour) (communicated_image_data,objective0,high_res) (communicated_image_data,objective0,low_res) (communicated_image_data,objective1,colour) (communicated_image_data,objective1,high_res) (communicated_image_data,objective1,low_res) (communicated_image_data,objective2,colour) (communicated_image_data,objective2,high_res) (communicated_image_data,objective2,low_res) (ok_e8) (first_o0) (first_o3))
 (:action navigate,rover0,waypoint0,waypoint1
  :precondition (at,rover0,waypoint0)
  :effect (and (at,rover0,waypoint1) (not (at,rover0,waypoint0))))
 (:action navigate,rover0,waypoint0,waypoint2
  :precondition (and (at,rover0,waypoint0) (first_o3))
  :effect (and (at,rover0,waypoint2) (not (at,rover0,waypoint0))))
 (:action navigate,rover0,waypoint1,waypoint0
  :precondition (at,rover0,waypoint1)
  :effect (and (at,rover0,waypoint0) (not (at,rover0,waypoint1))))
 (:action navigate,rover0,waypoint2,waypoint0
  :precondition (at,rover0,waypoint2)
  :effect (and (at,rover0,waypoint0) (not (at,rover0,waypoint2)) (not (first_o3))))
 (:action navigate,rover0,waypoint2,waypoint3
  :precondition (at,rover0,waypoint2)
  :effect (and (at,rover0,waypoint3) (not (at,rover0,waypoint2)) (not (first_o3))))
 (:action navigate,rover0,waypoint2,waypoint4
  :precondition (at,rover0,waypoint2)
  :effect (and (at,rover0,waypoint4) (not (at,rover0,waypoint2)) (not (first_o3))))
 (:action navigate,rover0,waypoint3,waypoint2
  :precondition (and (at,rover0,waypoint3) (first_o3))
  :effect (and (at,rover0,waypoint2) (not (at,rover0,waypoint3))))
 (:action navigate,rover0,waypoint3,waypoint5
  :precondition (at,rover0,waypoint3)
  :effect (and (at,rover0,waypoint5) (ok_e8) (not (at,rover0,waypoint3))))
 (:action navigate,rover0,waypoint4,waypoint2
  :precondition (and (at,rover0,waypoint4) (first_o3))
  :effect (and (at,rover0,waypoint2) (not (at,rover0,waypoint4))))
 (:action navigate,rover0,waypoint5,waypoint3
  :precondition (at,rover0,waypoint5)
  :effect (and (at,rover0,waypoint3) (not (at,rover0,waypoint5))))
 (:action navigate,rover1,waypoint0,waypoint1
  :precondition (at,rover1,waypoint0)
  :effect (and (at,rover1,waypoint1) (not (at,rover1,waypoint0))))
 (:action navigate,rover1,waypoint0,waypoint2
  :precondition (at,rover1,waypoint0)
  :effect (and (at,rover1,waypoint2) (not (at,rover1,waypoint0))))
 (:action navigate,rover1,waypoint1,waypoint0
  :precondition (at,rover1,waypoint1)
  :effect (and (at,rover1,waypoint0) (not (at,rover1,waypoint1))))
 (:action navigate,rover1,waypoint2,waypoint0
  :precondition (at,rover1,waypoint2)
  :effect (and (at,rover1,waypoint0) (not (at,rover1,waypoint2))))
 (:action navigate,rover1,waypoint2,waypoint3
  :precondition (at,rover1,waypoint2)
  :effect (and (at,rover1,waypoint3) (not (at,rover1,waypoint2))))
 (:action navigate,rover1,waypoint2,waypoint5
  :precondition (at,rover1,waypoint2)
  :effect (and (at,rover1,waypoint5) (not (at,rover1,waypoint2))))
 (:action navigate,rover1,waypoint3,waypoint2
  :precondition (at,rover1,waypoint3)
  :effect (and (at,rover1,waypoint2) (not (at,rover1,waypoint3))))
 (:action navigate,rover1,waypoint3,waypoint4
  :precondition (at,rover1,waypoint3)
  :effect (and (at,rover1,waypoint4) (not (at,rover1,waypoint3))))
 (:action navigate,rover1,waypoint4,waypoint3
  :precondition (at,rover1,waypoint4)
  :effect (and (at,rover1,waypoint3) (not (at,rover1,waypoint4))))
 (:action navigate,rover1,waypoint5,waypoint2
  :precondition (at,rover1,waypoint5)
  :effect (and (at,rover1,waypoint2) (not (at,rover1,waypoint5))))
 (:action navigate,rover2,waypoint0,waypoint2
  :precondition (at,rover2,waypoint0)
  :effect (and (at,rover2,waypoint2) (not (at,rover2,waypoint0))))
 (:action navigate,rover2,waypoint0,waypoint3
  :precondition (at,rover2,waypoint0)
  :effect (and (at,rover2,waypoint3) (not (at,rover2,waypoint0))))
 (:action navigate,rover2,waypoint1,waypoint2
  :precondition (at,rover2,waypoint1)
  :effect (and (at,rover2,waypoint2) (not (at,rover2,waypoint1))))
 (:action navigate,rover2,waypoint2,waypoint0
  :precondition (at,rover2,waypoint2)
  :effect (and (at,rover2,waypoint0) (not (at,rover2,waypoint2))))
 (:action navigate,rover2,waypoint2,waypoint1
  :precondition (at,rover2,waypoint2)
  :effect (and (at,rover2,waypoint1) (not (at,rover2,waypoint2))))
 (:action navigate,rover2,waypoint2,waypoint4
  :precondition (at,rover2,waypoint2)
  :effect (and (at,rover2,waypoint4) (not (at,rover2,waypoint2))))
 (:action navigate,rover2,waypoint2,waypoint5
  :precondition (at,rover2,waypoint2)
  :effect (and (at,rover2,waypoint5) (not (at,rover2,waypoint2))))
 (:action navigate,rover2,waypoint3,waypoint0
  :precondition (at,rover2,waypoint3)
  :effect (and (at,rover2,waypoint0) (not (at,rover2,waypoint3))))
 (:action navigate,rover2,waypoint4,waypoint2
  :precondition (at,rover2,waypoint4)
  :effect (and (at,rover2,waypoint2) (not (at,rover2,waypoint4))))
 (:action navigate,rover2,waypoint5,waypoint2
  :precondition (at,rover2,waypoint5)
  :effect (and (at,rover2,waypoint2) (not (at,rover2,waypoint5))))
 (:action navigate,rover3,waypoint0,waypoint1
  :precondition (at,rover3,waypoint0)
  :effect (and (at,rover3,waypoint1) (not (at,rover3,waypoint0))))
 (:action navigate,rover3,waypoint0,waypoint3
  :precondition (and (at,rover3,waypoint0) (first_o0))
  :effect (and (at,rover3,waypoint3) (not (at,rover3,waypoint0))))
 (:action navigate,rover3,waypoint1,waypoint0
  :precondition (at,rover3,waypoint1)
  :effect (and (at,rover3,waypoint0) (not (at,rover3,waypoint1))))
 (:action navigate,rover3,waypoint2,waypoint3
  :precondition (and (at,rover3,waypoint2) (first_o0))
  :effect (and (at,rover3,waypoint3) (not (at,rover3,waypoint2))))
 (:action navigate,rover3,waypoint2,waypoint5
  :precondition (at,rover3,waypoint2)
  :effect (and (at,rover3,waypoint5) (not (at,rover3,waypoint2))))
 (:action navigate,rover3,waypoint3,waypoint0
  :precondition (at,rover3,waypoint3)
  :effect (and (at,rover3,waypoint0) (not (at,rover3,waypoint3)) (not (first_o0))))
 (:action navigate,rover3,waypoint3,waypoint2
  :precondition (at,rover3,waypoint3)
  :effect (and (at,rover3,waypoint2) (not (at,rover3,waypoint3)) (not (first_o0))))
 (:action navigate,rover3,waypoint3,waypoint4
  :precondition (at,rover3,waypoint3)
  :effect (and (at,rover3,waypoint4) (not (at,rover3,waypoint3)) (not (first_o0))))
 (:action navigate,rover3,waypoint4,waypoint3
  :precondition (and (at,rover3,waypoint4) (first_o0))
  :effect (and (at,rover3,waypoint3) (not (at,rover3,waypoint4))))
 (:action navigate,rover3,waypoint5,waypoint2
  :precondition (at,rover3,waypoint5)
  :effect (and (at,rover3,waypoint2) (not (at,rover3,waypoint5))))
 (:action sample_soil,rover0,rover0store,waypoint1
  :precondition (and (at,rover0,waypoint1) (empty,rover0store) (at_soil_sample,waypoint1))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint1) (not (empty,rover0store)) (not (at_soil_sample,waypoint1))))
 (:action sample_soil,rover0,rover0store,waypoint3
  :precondition (and (at,rover0,waypoint3) (empty,rover0store) (at_soil_sample,waypoint3))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint3) (not (empty,rover0store)) (not (at_soil_sample,waypoint3))))
 (:action sample_soil,rover0,rover0store,waypoint4
  :precondition (and (at,rover0,waypoint4) (empty,rover0store) (at_soil_sample,waypoint4))
  :effect (and (full,rover0store) (have_soil_analysis,rover0,waypoint4) (not (empty,rover0store)) (not (at_soil_sample,waypoint4))))
 (:action sample_soil,rover3,rover3store,waypoint1
  :precondition (and (at,rover3,waypoint1) (at_soil_sample,waypoint1) (empty,rover3store))
  :effect (and (full,rover3store) (have_soil_analysis,rover3,waypoint1) (not (at_soil_sample,waypoint1)) (not (empty,rover3store))))
 (:action sample_soil,rover3,rover3store,waypoint3
  :precondition (and (at,rover3,waypoint3) (at_soil_sample,waypoint3) (empty,rover3store))
  :effect (and (full,rover3store) (have_soil_analysis,rover3,waypoint3) (not (at_soil_sample,waypoint3)) (not (empty,rover3store))))
 (:action sample_soil,rover3,rover3store,waypoint4
  :precondition (and (at,rover3,waypoint4) (at_soil_sample,waypoint4) (empty,rover3store))
  :effect (and (full,rover3store) (have_soil_analysis,rover3,waypoint4) (not (at_soil_sample,waypoint4)) (not (empty,rover3store))))
 (:action sample_rock,rover0,rover0store,waypoint2
  :precondition (and (at,rover0,waypoint2) (empty,rover0store) (at_rock_sample,waypoint2))
  :effect (and (full,rover0store) (have_rock_analysis,rover0,waypoint2) (not (empty,rover0store)) (not (at_rock_sample,waypoint2))))
 (:action sample_rock,rover0,rover0store,waypoint3
  :precondition (and (at,rover0,waypoint3) (empty,rover0store) (at_rock_sample,waypoint3))
  :effect (and (full,rover0store) (have_rock_analysis,rover0,waypoint3) (not (empty,rover0store)) (not (at_rock_sample,waypoint3))))
 (:action sample_rock,rover0,rover0store,waypoint4
  :precondition (and (at,rover0,waypoint4) (empty,rover0store) (at_rock_sample,waypoint4))
  :effect (and (full,rover0store) (have_rock_analysis,rover0,waypoint4) (not (empty,rover0store)) (not (at_rock_sample,waypoint4))))
 (:action sample_rock,rover0,rover0store,waypoint5
  :precondition (and (at,rover0,waypoint5) (empty,rover0store) (at_rock_sample,waypoint5))
  :effect (and (full,rover0store) (have_rock_analysis,rover0,waypoint5) (not (empty,rover0store)) (not (at_rock_sample,waypoint5))))
 (:action sample_rock,rover1,rover1store,waypoint2
  :precondition (and (at,rover1,waypoint2) (at_rock_sample,waypoint2) (empty,rover1store))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint2) (not (at_rock_sample,waypoint2)) (not (empty,rover1store))))
 (:action sample_rock,rover1,rover1store,waypoint3
  :precondition (and (at,rover1,waypoint3) (at_rock_sample,waypoint3) (empty,rover1store))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint3) (not (at_rock_sample,waypoint3)) (not (empty,rover1store))))
 (:action sample_rock,rover1,rover1store,waypoint4
  :precondition (and (at,rover1,waypoint4) (at_rock_sample,waypoint4) (empty,rover1store))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint4) (not (at_rock_sample,waypoint4)) (not (empty,rover1store))))
 (:action sample_rock,rover1,rover1store,waypoint5
  :precondition (and (at,rover1,waypoint5) (at_rock_sample,waypoint5) (empty,rover1store))
  :effect (and (full,rover1store) (have_rock_analysis,rover1,waypoint5) (not (at_rock_sample,waypoint5)) (not (empty,rover1store))))
 (:action sample_rock,rover2,rover2store,waypoint2
  :precondition (and (at,rover2,waypoint2) (at_rock_sample,waypoint2) (empty,rover2store))
  :effect (and (full,rover2store) (have_rock_analysis,rover2,waypoint2) (not (at_rock_sample,waypoint2)) (not (empty,rover2store))))
 (:action sample_rock,rover2,rover2store,waypoint3
  :precondition (and (at,rover2,waypoint3) (at_rock_sample,waypoint3) (empty,rover2store))
  :effect (and (full,rover2store) (have_rock_analysis,rover2,waypoint3) (not (at_rock_sample,waypoint3)) (not (empty,rover2store))))
 (:action sample_rock,rover2,rover2store,waypoint4
  :precondition (and (at,rover2,waypoint4) (at_rock_sample,waypoint4) (empty,rover2store))
  :effect (and (full,rover2store) (have_rock_analysis,rover2,waypoint4) (not (at_rock_sample,waypoint4)) (not (empty,rover2store))))
 (:action sample_rock,rover2,rover2store,waypoint5
  :precondition (and (at,rover2,waypoint5) (at_rock_sample,waypoint5) (empty,rover2store))
  :effect (and (full,rover2store) (have_rock_analysis,rover2,waypoint5) (not (at_rock_sample,waypoint5)) (not (empty,rover2store))))
 (:action sample_rock,rover3,rover3store,waypoint2
  :precondition (and (at,rover3,waypoint2) (empty,rover3store) (at_rock_sample,waypoint2))
  :effect (and (full,rover3store) (have_rock_analysis,rover3,waypoint2) (not (empty,rover3store)) (not (at_rock_sample,waypoint2))))
 (:action sample_rock,rover3,rover3store,waypoint3
  :precondition (and (at,rover3,waypoint3) (empty,rover3store) (at_rock_sample,waypoint3))
  :effect (and (full,rover3store) (have_rock_analysis,rover3,waypoint3) (not (empty,rover3store)) (not (at_rock_sample,waypoint3))))
 (:action sample_rock,rover3,rover3store,waypoint4
  :precondition (and (at,rover3,waypoint4) (empty,rover3store) (at_rock_sample,waypoint4))
  :effect (and (full,rover3store) (have_rock_analysis,rover3,waypoint4) (not (empty,rover3store)) (not (at_rock_sample,waypoint4))))
 (:action sample_rock,rover3,rover3store,waypoint5
  :precondition (and (at,rover3,waypoint5) (empty,rover3store) (at_rock_sample,waypoint5))
  :effect (and (full,rover3store) (have_rock_analysis,rover3,waypoint5) (not (empty,rover3store)) (not (at_rock_sample,waypoint5))))
 (:action drop,rover0,rover0store
  :precondition (full,rover0store)
  :effect (and (empty,rover0store) (not (full,rover0store))))
 (:action drop,rover1,rover1store
  :precondition (full,rover1store)
  :effect (and (empty,rover1store) (not (full,rover1store))))
 (:action drop,rover2,rover2store
  :precondition (full,rover2store)
  :effect (and (empty,rover2store) (not (full,rover2store))))
 (:action drop,rover3,rover3store
  :precondition (full,rover3store)
  :effect (and (empty,rover3store) (not (full,rover3store))))
 (:action calibrate,rover1,camera2,objective0,waypoint0
  :precondition (at,rover1,waypoint0)
  :effect (calibrated,camera2,rover1))
 (:action calibrate,rover1,camera2,objective0,waypoint1
  :precondition (at,rover1,waypoint1)
  :effect (calibrated,camera2,rover1))
 (:action calibrate,rover1,camera2,objective0,waypoint2
  :precondition (at,rover1,waypoint2)
  :effect (calibrated,camera2,rover1))
 (:action calibrate,rover2,camera3,objective1,waypoint0
  :precondition (at,rover2,waypoint0)
  :effect (calibrated,camera3,rover2))
 (:action calibrate,rover3,camera0,objective1,waypoint0
  :precondition (at,rover3,waypoint0)
  :effect (calibrated,camera0,rover3))
 (:action calibrate,rover3,camera1,objective0,waypoint0
  :precondition (at,rover3,waypoint0)
  :effect (calibrated,camera1,rover3))
 (:action calibrate,rover3,camera1,objective0,waypoint1
  :precondition (at,rover3,waypoint1)
  :effect (calibrated,camera1,rover3))
 (:action calibrate,rover3,camera1,objective0,waypoint2
  :precondition (at,rover3,waypoint2)
  :effect (calibrated,camera1,rover3))
 (:action take_image,rover1,waypoint0,objective0,camera2,colour
  :precondition (and (at,rover1,waypoint0) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective0,colour) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint0,objective0,camera2,high_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint0,objective0,camera2,low_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective0,low_res) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint0,objective1,camera2,colour
  :precondition (and (at,rover1,waypoint0) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective1,colour) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint0,objective1,camera2,high_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective1,high_res) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint0,objective1,camera2,low_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective1,low_res) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint0,objective2,camera2,colour
  :precondition (and (at,rover1,waypoint0) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective2,colour) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint0,objective2,camera2,high_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint0,objective2,camera2,low_res
  :precondition (and (at,rover1,waypoint0) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective2,low_res) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint1,objective0,camera2,colour
  :precondition (and (at,rover1,waypoint1) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective0,colour) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint1,objective0,camera2,high_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint1,objective0,camera2,low_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective0,low_res) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint1,objective2,camera2,colour
  :precondition (and (at,rover1,waypoint1) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective2,colour) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint1,objective2,camera2,high_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint1,objective2,camera2,low_res
  :precondition (and (at,rover1,waypoint1) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective2,low_res) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint2,objective0,camera2,colour
  :precondition (and (at,rover1,waypoint2) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective0,colour) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint2,objective0,camera2,high_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective0,high_res) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint2,objective0,camera2,low_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective0,low_res) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint2,objective2,camera2,colour
  :precondition (and (at,rover1,waypoint2) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective2,colour) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint2,objective2,camera2,high_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective2,high_res) (not (calibrated,camera2,rover1))))
 (:action take_image,rover1,waypoint2,objective2,camera2,low_res
  :precondition (and (at,rover1,waypoint2) (calibrated,camera2,rover1))
  :effect (and (have_image,rover1,objective2,low_res) (not (calibrated,camera2,rover1))))
 (:action take_image,rover2,waypoint0,objective0,camera3,colour
  :precondition (and (at,rover2,waypoint0) (calibrated,camera3,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera3,rover2))))
 (:action take_image,rover2,waypoint0,objective0,camera3,low_res
  :precondition (and (at,rover2,waypoint0) (calibrated,camera3,rover2))
  :effect (and (have_image,rover2,objective0,low_res) (not (calibrated,camera3,rover2))))
 (:action take_image,rover2,waypoint0,objective1,camera3,colour
  :precondition (and (at,rover2,waypoint0) (calibrated,camera3,rover2))
  :effect (and (have_image,rover2,objective1,colour) (not (calibrated,camera3,rover2))))
 (:action take_image,rover2,waypoint0,objective1,camera3,low_res
  :precondition (and (at,rover2,waypoint0) (calibrated,camera3,rover2))
  :effect (and (have_image,rover2,objective1,low_res) (not (calibrated,camera3,rover2))))
 (:action take_image,rover2,waypoint0,objective2,camera3,colour
  :precondition (and (at,rover2,waypoint0) (calibrated,camera3,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera3,rover2))))
 (:action take_image,rover2,waypoint0,objective2,camera3,low_res
  :precondition (and (at,rover2,waypoint0) (calibrated,camera3,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera3,rover2))))
 (:action take_image,rover2,waypoint1,objective0,camera3,colour
  :precondition (and (at,rover2,waypoint1) (calibrated,camera3,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera3,rover2))))
 (:action take_image,rover2,waypoint1,objective0,camera3,low_res
  :precondition (and (at,rover2,waypoint1) (calibrated,camera3,rover2))
  :effect (and (have_image,rover2,objective0,low_res) (not (calibrated,camera3,rover2))))
 (:action take_image,rover2,waypoint1,objective2,camera3,colour
  :precondition (and (at,rover2,waypoint1) (calibrated,camera3,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera3,rover2))))
 (:action take_image,rover2,waypoint1,objective2,camera3,low_res
  :precondition (and (at,rover2,waypoint1) (calibrated,camera3,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera3,rover2))))
 (:action take_image,rover2,waypoint2,objective0,camera3,colour
  :precondition (and (at,rover2,waypoint2) (calibrated,camera3,rover2))
  :effect (and (have_image,rover2,objective0,colour) (not (calibrated,camera3,rover2))))
 (:action take_image,rover2,waypoint2,objective0,camera3,low_res
  :precondition (and (at,rover2,waypoint2) (calibrated,camera3,rover2))
  :effect (and (have_image,rover2,objective0,low_res) (not (calibrated,camera3,rover2))))
 (:action take_image,rover2,waypoint2,objective2,camera3,colour
  :precondition (and (at,rover2,waypoint2) (calibrated,camera3,rover2))
  :effect (and (have_image,rover2,objective2,colour) (not (calibrated,camera3,rover2))))
 (:action take_image,rover2,waypoint2,objective2,camera3,low_res
  :precondition (and (at,rover2,waypoint2) (calibrated,camera3,rover2))
  :effect (and (have_image,rover2,objective2,low_res) (not (calibrated,camera3,rover2))))
 (:action take_image,rover3,waypoint0,objective0,camera0,colour
  :precondition (and (at,rover3,waypoint0) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint0,objective0,camera0,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint0,objective0,camera0,low_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective0,low_res) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint0,objective0,camera1,colour
  :precondition (and (at,rover3,waypoint0) (calibrated,camera1,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera1,rover3))))
 (:action take_image,rover3,waypoint0,objective0,camera1,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera1,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera1,rover3))))
 (:action take_image,rover3,waypoint0,objective1,camera0,colour
  :precondition (and (at,rover3,waypoint0) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective1,colour) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint0,objective1,camera0,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint0,objective1,camera0,low_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective1,low_res) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint0,objective1,camera1,colour
  :precondition (and (at,rover3,waypoint0) (calibrated,camera1,rover3))
  :effect (and (have_image,rover3,objective1,colour) (not (calibrated,camera1,rover3))))
 (:action take_image,rover3,waypoint0,objective1,camera1,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera1,rover3))
  :effect (and (have_image,rover3,objective1,high_res) (not (calibrated,camera1,rover3))))
 (:action take_image,rover3,waypoint0,objective2,camera0,colour
  :precondition (and (at,rover3,waypoint0) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint0,objective2,camera0,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint0,objective2,camera0,low_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint0,objective2,camera1,colour
  :precondition (and (at,rover3,waypoint0) (calibrated,camera1,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera1,rover3))))
 (:action take_image,rover3,waypoint0,objective2,camera1,high_res
  :precondition (and (at,rover3,waypoint0) (calibrated,camera1,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera1,rover3))))
 (:action take_image,rover3,waypoint1,objective0,camera0,colour
  :precondition (and (at,rover3,waypoint1) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint1,objective0,camera0,high_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint1,objective0,camera0,low_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective0,low_res) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint1,objective0,camera1,colour
  :precondition (and (at,rover3,waypoint1) (calibrated,camera1,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera1,rover3))))
 (:action take_image,rover3,waypoint1,objective0,camera1,high_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera1,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera1,rover3))))
 (:action take_image,rover3,waypoint1,objective2,camera0,colour
  :precondition (and (at,rover3,waypoint1) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint1,objective2,camera0,high_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint1,objective2,camera0,low_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint1,objective2,camera1,colour
  :precondition (and (at,rover3,waypoint1) (calibrated,camera1,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera1,rover3))))
 (:action take_image,rover3,waypoint1,objective2,camera1,high_res
  :precondition (and (at,rover3,waypoint1) (calibrated,camera1,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera1,rover3))))
 (:action take_image,rover3,waypoint2,objective0,camera0,colour
  :precondition (and (at,rover3,waypoint2) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint2,objective0,camera0,high_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint2,objective0,camera0,low_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective0,low_res) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint2,objective0,camera1,colour
  :precondition (and (at,rover3,waypoint2) (calibrated,camera1,rover3))
  :effect (and (have_image,rover3,objective0,colour) (not (calibrated,camera1,rover3))))
 (:action take_image,rover3,waypoint2,objective0,camera1,high_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera1,rover3))
  :effect (and (have_image,rover3,objective0,high_res) (not (calibrated,camera1,rover3))))
 (:action take_image,rover3,waypoint2,objective2,camera0,colour
  :precondition (and (at,rover3,waypoint2) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint2,objective2,camera0,high_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint2,objective2,camera0,low_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera0,rover3))
  :effect (and (have_image,rover3,objective2,low_res) (not (calibrated,camera0,rover3))))
 (:action take_image,rover3,waypoint2,objective2,camera1,colour
  :precondition (and (at,rover3,waypoint2) (calibrated,camera1,rover3))
  :effect (and (have_image,rover3,objective2,colour) (not (calibrated,camera1,rover3))))
 (:action take_image,rover3,waypoint2,objective2,camera1,high_res
  :precondition (and (at,rover3,waypoint2) (calibrated,camera1,rover3))
  :effect (and (have_image,rover3,objective2,high_res) (not (calibrated,camera1,rover3))))
 (:action communicate_soil_data,rover0,general,waypoint1,waypoint1,waypoint0
  :precondition (and (at,rover0,waypoint1) (have_soil_analysis,rover0,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover0,general,waypoint1,waypoint2,waypoint0
  :precondition (and (at,rover0,waypoint2) (have_soil_analysis,rover0,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover0,general,waypoint1,waypoint3,waypoint0
  :precondition (and (at,rover0,waypoint3) (have_soil_analysis,rover0,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover0,general,waypoint1,waypoint4,waypoint0
  :precondition (and (at,rover0,waypoint4) (have_soil_analysis,rover0,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover0,general,waypoint3,waypoint1,waypoint0
  :precondition (and (at,rover0,waypoint1) (have_soil_analysis,rover0,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover0,general,waypoint3,waypoint2,waypoint0
  :precondition (and (at,rover0,waypoint2) (have_soil_analysis,rover0,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover0,general,waypoint3,waypoint3,waypoint0
  :precondition (and (at,rover0,waypoint3) (have_soil_analysis,rover0,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover0,general,waypoint3,waypoint4,waypoint0
  :precondition (and (at,rover0,waypoint4) (have_soil_analysis,rover0,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover0,general,waypoint4,waypoint1,waypoint0
  :precondition (and (at,rover0,waypoint1) (have_soil_analysis,rover0,waypoint4))
  :effect (communicated_soil_data,waypoint4))
 (:action communicate_soil_data,rover0,general,waypoint4,waypoint2,waypoint0
  :precondition (and (at,rover0,waypoint2) (have_soil_analysis,rover0,waypoint4))
  :effect (communicated_soil_data,waypoint4))
 (:action communicate_soil_data,rover0,general,waypoint4,waypoint3,waypoint0
  :precondition (and (at,rover0,waypoint3) (have_soil_analysis,rover0,waypoint4))
  :effect (communicated_soil_data,waypoint4))
 (:action communicate_soil_data,rover0,general,waypoint4,waypoint4,waypoint0
  :precondition (and (at,rover0,waypoint4) (have_soil_analysis,rover0,waypoint4))
  :effect (communicated_soil_data,waypoint4))
 (:action communicate_soil_data,rover3,general,waypoint1,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_soil_analysis,rover3,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover3,general,waypoint1,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_soil_analysis,rover3,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover3,general,waypoint1,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_soil_analysis,rover3,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover3,general,waypoint1,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_soil_analysis,rover3,waypoint1))
  :effect (communicated_soil_data,waypoint1))
 (:action communicate_soil_data,rover3,general,waypoint3,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_soil_analysis,rover3,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover3,general,waypoint3,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_soil_analysis,rover3,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover3,general,waypoint3,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_soil_analysis,rover3,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover3,general,waypoint3,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_soil_analysis,rover3,waypoint3))
  :effect (communicated_soil_data,waypoint3))
 (:action communicate_soil_data,rover3,general,waypoint4,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_soil_analysis,rover3,waypoint4))
  :effect (communicated_soil_data,waypoint4))
 (:action communicate_soil_data,rover3,general,waypoint4,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_soil_analysis,rover3,waypoint4))
  :effect (communicated_soil_data,waypoint4))
 (:action communicate_soil_data,rover3,general,waypoint4,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_soil_analysis,rover3,waypoint4))
  :effect (communicated_soil_data,waypoint4))
 (:action communicate_soil_data,rover3,general,waypoint4,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_soil_analysis,rover3,waypoint4))
  :effect (communicated_soil_data,waypoint4))
 (:action communicate_rock_data,rover0,general,waypoint2,waypoint1,waypoint0
  :precondition (and (at,rover0,waypoint1) (have_rock_analysis,rover0,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover0,general,waypoint2,waypoint2,waypoint0
  :precondition (and (at,rover0,waypoint2) (have_rock_analysis,rover0,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover0,general,waypoint2,waypoint3,waypoint0
  :precondition (and (at,rover0,waypoint3) (have_rock_analysis,rover0,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover0,general,waypoint2,waypoint4,waypoint0
  :precondition (and (at,rover0,waypoint4) (have_rock_analysis,rover0,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover0,general,waypoint3,waypoint1,waypoint0
  :precondition (and (at,rover0,waypoint1) (have_rock_analysis,rover0,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover0,general,waypoint3,waypoint2,waypoint0
  :precondition (and (at,rover0,waypoint2) (have_rock_analysis,rover0,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover0,general,waypoint3,waypoint3,waypoint0
  :precondition (and (at,rover0,waypoint3) (have_rock_analysis,rover0,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover0,general,waypoint3,waypoint4,waypoint0
  :precondition (and (at,rover0,waypoint4) (have_rock_analysis,rover0,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover0,general,waypoint4,waypoint1,waypoint0
  :precondition (and (at,rover0,waypoint1) (have_rock_analysis,rover0,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover0,general,waypoint4,waypoint2,waypoint0
  :precondition (and (at,rover0,waypoint2) (have_rock_analysis,rover0,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover0,general,waypoint4,waypoint3,waypoint0
  :precondition (and (at,rover0,waypoint3) (have_rock_analysis,rover0,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover0,general,waypoint4,waypoint4,waypoint0
  :precondition (and (at,rover0,waypoint4) (have_rock_analysis,rover0,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover0,general,waypoint5,waypoint1,waypoint0
  :precondition (and (at,rover0,waypoint1) (have_rock_analysis,rover0,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover0,general,waypoint5,waypoint2,waypoint0
  :precondition (and (at,rover0,waypoint2) (have_rock_analysis,rover0,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover0,general,waypoint5,waypoint3,waypoint0
  :precondition (and (at,rover0,waypoint3) (have_rock_analysis,rover0,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover0,general,waypoint5,waypoint4,waypoint0
  :precondition (and (at,rover0,waypoint4) (have_rock_analysis,rover0,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover1,general,waypoint2,waypoint1,waypoint0
  :precondition (and (at,rover1,waypoint1) (have_rock_analysis,rover1,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover1,general,waypoint2,waypoint2,waypoint0
  :precondition (and (at,rover1,waypoint2) (have_rock_analysis,rover1,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover1,general,waypoint2,waypoint3,waypoint0
  :precondition (and (at,rover1,waypoint3) (have_rock_analysis,rover1,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover1,general,waypoint2,waypoint4,waypoint0
  :precondition (and (at,rover1,waypoint4) (have_rock_analysis,rover1,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover1,general,waypoint3,waypoint1,waypoint0
  :precondition (and (at,rover1,waypoint1) (have_rock_analysis,rover1,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover1,general,waypoint3,waypoint2,waypoint0
  :precondition (and (at,rover1,waypoint2) (have_rock_analysis,rover1,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover1,general,waypoint3,waypoint3,waypoint0
  :precondition (and (at,rover1,waypoint3) (have_rock_analysis,rover1,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover1,general,waypoint3,waypoint4,waypoint0
  :precondition (and (at,rover1,waypoint4) (have_rock_analysis,rover1,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover1,general,waypoint4,waypoint1,waypoint0
  :precondition (and (at,rover1,waypoint1) (have_rock_analysis,rover1,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover1,general,waypoint4,waypoint2,waypoint0
  :precondition (and (at,rover1,waypoint2) (have_rock_analysis,rover1,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover1,general,waypoint4,waypoint3,waypoint0
  :precondition (and (at,rover1,waypoint3) (have_rock_analysis,rover1,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover1,general,waypoint4,waypoint4,waypoint0
  :precondition (and (at,rover1,waypoint4) (have_rock_analysis,rover1,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover1,general,waypoint5,waypoint1,waypoint0
  :precondition (and (at,rover1,waypoint1) (have_rock_analysis,rover1,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover1,general,waypoint5,waypoint2,waypoint0
  :precondition (and (at,rover1,waypoint2) (have_rock_analysis,rover1,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover1,general,waypoint5,waypoint3,waypoint0
  :precondition (and (at,rover1,waypoint3) (have_rock_analysis,rover1,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover1,general,waypoint5,waypoint4,waypoint0
  :precondition (and (at,rover1,waypoint4) (have_rock_analysis,rover1,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover2,general,waypoint2,waypoint1,waypoint0
  :precondition (and (at,rover2,waypoint1) (have_rock_analysis,rover2,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover2,general,waypoint2,waypoint2,waypoint0
  :precondition (and (at,rover2,waypoint2) (have_rock_analysis,rover2,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover2,general,waypoint2,waypoint3,waypoint0
  :precondition (and (at,rover2,waypoint3) (have_rock_analysis,rover2,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover2,general,waypoint2,waypoint4,waypoint0
  :precondition (and (at,rover2,waypoint4) (have_rock_analysis,rover2,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover2,general,waypoint3,waypoint1,waypoint0
  :precondition (and (at,rover2,waypoint1) (have_rock_analysis,rover2,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover2,general,waypoint3,waypoint2,waypoint0
  :precondition (and (at,rover2,waypoint2) (have_rock_analysis,rover2,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover2,general,waypoint3,waypoint3,waypoint0
  :precondition (and (at,rover2,waypoint3) (have_rock_analysis,rover2,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover2,general,waypoint3,waypoint4,waypoint0
  :precondition (and (at,rover2,waypoint4) (have_rock_analysis,rover2,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover2,general,waypoint4,waypoint1,waypoint0
  :precondition (and (at,rover2,waypoint1) (have_rock_analysis,rover2,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover2,general,waypoint4,waypoint2,waypoint0
  :precondition (and (at,rover2,waypoint2) (have_rock_analysis,rover2,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover2,general,waypoint4,waypoint3,waypoint0
  :precondition (and (at,rover2,waypoint3) (have_rock_analysis,rover2,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover2,general,waypoint4,waypoint4,waypoint0
  :precondition (and (at,rover2,waypoint4) (have_rock_analysis,rover2,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover2,general,waypoint5,waypoint1,waypoint0
  :precondition (and (at,rover2,waypoint1) (have_rock_analysis,rover2,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover2,general,waypoint5,waypoint2,waypoint0
  :precondition (and (at,rover2,waypoint2) (have_rock_analysis,rover2,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover2,general,waypoint5,waypoint3,waypoint0
  :precondition (and (at,rover2,waypoint3) (have_rock_analysis,rover2,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover2,general,waypoint5,waypoint4,waypoint0
  :precondition (and (at,rover2,waypoint4) (have_rock_analysis,rover2,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover3,general,waypoint2,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_rock_analysis,rover3,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover3,general,waypoint2,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_rock_analysis,rover3,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover3,general,waypoint2,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_rock_analysis,rover3,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover3,general,waypoint2,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_rock_analysis,rover3,waypoint2))
  :effect (communicated_rock_data,waypoint2))
 (:action communicate_rock_data,rover3,general,waypoint3,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_rock_analysis,rover3,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover3,general,waypoint3,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_rock_analysis,rover3,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover3,general,waypoint3,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_rock_analysis,rover3,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover3,general,waypoint3,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_rock_analysis,rover3,waypoint3))
  :effect (communicated_rock_data,waypoint3))
 (:action communicate_rock_data,rover3,general,waypoint4,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_rock_analysis,rover3,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover3,general,waypoint4,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_rock_analysis,rover3,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover3,general,waypoint4,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_rock_analysis,rover3,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover3,general,waypoint4,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_rock_analysis,rover3,waypoint4))
  :effect (communicated_rock_data,waypoint4))
 (:action communicate_rock_data,rover3,general,waypoint5,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_rock_analysis,rover3,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover3,general,waypoint5,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_rock_analysis,rover3,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover3,general,waypoint5,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_rock_analysis,rover3,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_rock_data,rover3,general,waypoint5,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_rock_analysis,rover3,waypoint5))
  :effect (communicated_rock_data,waypoint5))
 (:action communicate_image_data,rover1,general,objective0,colour,waypoint1,waypoint0
  :precondition (and (at,rover1,waypoint1) (have_image,rover1,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover1,general,objective0,colour,waypoint2,waypoint0
  :precondition (and (at,rover1,waypoint2) (have_image,rover1,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover1,general,objective0,colour,waypoint3,waypoint0
  :precondition (and (at,rover1,waypoint3) (have_image,rover1,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover1,general,objective0,colour,waypoint4,waypoint0
  :precondition (and (at,rover1,waypoint4) (have_image,rover1,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover1,general,objective0,high_res,waypoint1,waypoint0
  :precondition (and (at,rover1,waypoint1) (have_image,rover1,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover1,general,objective0,high_res,waypoint2,waypoint0
  :precondition (and (at,rover1,waypoint2) (have_image,rover1,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover1,general,objective0,high_res,waypoint3,waypoint0
  :precondition (and (at,rover1,waypoint3) (have_image,rover1,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover1,general,objective0,high_res,waypoint4,waypoint0
  :precondition (and (at,rover1,waypoint4) (have_image,rover1,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover1,general,objective0,low_res,waypoint1,waypoint0
  :precondition (and (at,rover1,waypoint1) (have_image,rover1,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover1,general,objective0,low_res,waypoint2,waypoint0
  :precondition (and (at,rover1,waypoint2) (have_image,rover1,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover1,general,objective0,low_res,waypoint3,waypoint0
  :precondition (and (at,rover1,waypoint3) (have_image,rover1,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover1,general,objective0,low_res,waypoint4,waypoint0
  :precondition (and (at,rover1,waypoint4) (have_image,rover1,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover1,general,objective1,colour,waypoint1,waypoint0
  :precondition (and (at,rover1,waypoint1) (have_image,rover1,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover1,general,objective1,colour,waypoint2,waypoint0
  :precondition (and (at,rover1,waypoint2) (have_image,rover1,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover1,general,objective1,colour,waypoint3,waypoint0
  :precondition (and (at,rover1,waypoint3) (have_image,rover1,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover1,general,objective1,colour,waypoint4,waypoint0
  :precondition (and (at,rover1,waypoint4) (have_image,rover1,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover1,general,objective1,high_res,waypoint1,waypoint0
  :precondition (and (at,rover1,waypoint1) (have_image,rover1,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover1,general,objective1,high_res,waypoint2,waypoint0
  :precondition (and (at,rover1,waypoint2) (have_image,rover1,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover1,general,objective1,high_res,waypoint3,waypoint0
  :precondition (and (at,rover1,waypoint3) (have_image,rover1,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover1,general,objective1,high_res,waypoint4,waypoint0
  :precondition (and (at,rover1,waypoint4) (have_image,rover1,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover1,general,objective1,low_res,waypoint1,waypoint0
  :precondition (and (at,rover1,waypoint1) (have_image,rover1,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover1,general,objective1,low_res,waypoint2,waypoint0
  :precondition (and (at,rover1,waypoint2) (have_image,rover1,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover1,general,objective1,low_res,waypoint3,waypoint0
  :precondition (and (at,rover1,waypoint3) (have_image,rover1,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover1,general,objective1,low_res,waypoint4,waypoint0
  :precondition (and (at,rover1,waypoint4) (have_image,rover1,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover1,general,objective2,colour,waypoint1,waypoint0
  :precondition (and (at,rover1,waypoint1) (have_image,rover1,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover1,general,objective2,colour,waypoint2,waypoint0
  :precondition (and (at,rover1,waypoint2) (have_image,rover1,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover1,general,objective2,colour,waypoint3,waypoint0
  :precondition (and (at,rover1,waypoint3) (have_image,rover1,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover1,general,objective2,colour,waypoint4,waypoint0
  :precondition (and (at,rover1,waypoint4) (have_image,rover1,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover1,general,objective2,high_res,waypoint1,waypoint0
  :precondition (and (at,rover1,waypoint1) (have_image,rover1,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover1,general,objective2,high_res,waypoint2,waypoint0
  :precondition (and (at,rover1,waypoint2) (have_image,rover1,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover1,general,objective2,high_res,waypoint3,waypoint0
  :precondition (and (at,rover1,waypoint3) (have_image,rover1,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover1,general,objective2,high_res,waypoint4,waypoint0
  :precondition (and (at,rover1,waypoint4) (have_image,rover1,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover1,general,objective2,low_res,waypoint1,waypoint0
  :precondition (and (at,rover1,waypoint1) (have_image,rover1,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover1,general,objective2,low_res,waypoint2,waypoint0
  :precondition (and (at,rover1,waypoint2) (have_image,rover1,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover1,general,objective2,low_res,waypoint3,waypoint0
  :precondition (and (at,rover1,waypoint3) (have_image,rover1,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover1,general,objective2,low_res,waypoint4,waypoint0
  :precondition (and (at,rover1,waypoint4) (have_image,rover1,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover2,general,objective0,colour,waypoint1,waypoint0
  :precondition (and (at,rover2,waypoint1) (have_image,rover2,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover2,general,objective0,colour,waypoint2,waypoint0
  :precondition (and (at,rover2,waypoint2) (have_image,rover2,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover2,general,objective0,colour,waypoint3,waypoint0
  :precondition (and (at,rover2,waypoint3) (have_image,rover2,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover2,general,objective0,colour,waypoint4,waypoint0
  :precondition (and (at,rover2,waypoint4) (have_image,rover2,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover2,general,objective0,low_res,waypoint1,waypoint0
  :precondition (and (at,rover2,waypoint1) (have_image,rover2,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover2,general,objective0,low_res,waypoint2,waypoint0
  :precondition (and (at,rover2,waypoint2) (have_image,rover2,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover2,general,objective0,low_res,waypoint3,waypoint0
  :precondition (and (at,rover2,waypoint3) (have_image,rover2,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover2,general,objective0,low_res,waypoint4,waypoint0
  :precondition (and (at,rover2,waypoint4) (have_image,rover2,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover2,general,objective1,colour,waypoint1,waypoint0
  :precondition (and (at,rover2,waypoint1) (have_image,rover2,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover2,general,objective1,colour,waypoint2,waypoint0
  :precondition (and (at,rover2,waypoint2) (have_image,rover2,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover2,general,objective1,colour,waypoint3,waypoint0
  :precondition (and (at,rover2,waypoint3) (have_image,rover2,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover2,general,objective1,colour,waypoint4,waypoint0
  :precondition (and (at,rover2,waypoint4) (have_image,rover2,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover2,general,objective1,low_res,waypoint1,waypoint0
  :precondition (and (at,rover2,waypoint1) (have_image,rover2,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover2,general,objective1,low_res,waypoint2,waypoint0
  :precondition (and (at,rover2,waypoint2) (have_image,rover2,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover2,general,objective1,low_res,waypoint3,waypoint0
  :precondition (and (at,rover2,waypoint3) (have_image,rover2,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover2,general,objective1,low_res,waypoint4,waypoint0
  :precondition (and (at,rover2,waypoint4) (have_image,rover2,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover2,general,objective2,colour,waypoint1,waypoint0
  :precondition (and (at,rover2,waypoint1) (have_image,rover2,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover2,general,objective2,colour,waypoint2,waypoint0
  :precondition (and (at,rover2,waypoint2) (have_image,rover2,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover2,general,objective2,colour,waypoint3,waypoint0
  :precondition (and (at,rover2,waypoint3) (have_image,rover2,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover2,general,objective2,colour,waypoint4,waypoint0
  :precondition (and (at,rover2,waypoint4) (have_image,rover2,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover2,general,objective2,low_res,waypoint1,waypoint0
  :precondition (and (at,rover2,waypoint1) (have_image,rover2,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover2,general,objective2,low_res,waypoint2,waypoint0
  :precondition (and (at,rover2,waypoint2) (have_image,rover2,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover2,general,objective2,low_res,waypoint3,waypoint0
  :precondition (and (at,rover2,waypoint3) (have_image,rover2,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover2,general,objective2,low_res,waypoint4,waypoint0
  :precondition (and (at,rover2,waypoint4) (have_image,rover2,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover3,general,objective0,colour,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_image,rover3,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover3,general,objective0,colour,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_image,rover3,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover3,general,objective0,colour,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_image,rover3,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover3,general,objective0,colour,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_image,rover3,objective0,colour))
  :effect (communicated_image_data,objective0,colour))
 (:action communicate_image_data,rover3,general,objective0,high_res,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_image,rover3,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover3,general,objective0,high_res,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_image,rover3,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover3,general,objective0,high_res,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_image,rover3,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover3,general,objective0,high_res,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_image,rover3,objective0,high_res))
  :effect (communicated_image_data,objective0,high_res))
 (:action communicate_image_data,rover3,general,objective0,low_res,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_image,rover3,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover3,general,objective0,low_res,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_image,rover3,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover3,general,objective0,low_res,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_image,rover3,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover3,general,objective0,low_res,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_image,rover3,objective0,low_res))
  :effect (communicated_image_data,objective0,low_res))
 (:action communicate_image_data,rover3,general,objective1,colour,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_image,rover3,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover3,general,objective1,colour,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_image,rover3,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover3,general,objective1,colour,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_image,rover3,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover3,general,objective1,colour,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_image,rover3,objective1,colour))
  :effect (communicated_image_data,objective1,colour))
 (:action communicate_image_data,rover3,general,objective1,high_res,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_image,rover3,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover3,general,objective1,high_res,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_image,rover3,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover3,general,objective1,high_res,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_image,rover3,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover3,general,objective1,high_res,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_image,rover3,objective1,high_res))
  :effect (communicated_image_data,objective1,high_res))
 (:action communicate_image_data,rover3,general,objective1,low_res,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_image,rover3,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover3,general,objective1,low_res,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_image,rover3,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover3,general,objective1,low_res,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_image,rover3,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover3,general,objective1,low_res,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_image,rover3,objective1,low_res))
  :effect (communicated_image_data,objective1,low_res))
 (:action communicate_image_data,rover3,general,objective2,colour,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_image,rover3,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover3,general,objective2,colour,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_image,rover3,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover3,general,objective2,colour,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_image,rover3,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover3,general,objective2,colour,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_image,rover3,objective2,colour))
  :effect (communicated_image_data,objective2,colour))
 (:action communicate_image_data,rover3,general,objective2,high_res,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_image,rover3,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover3,general,objective2,high_res,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_image,rover3,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover3,general,objective2,high_res,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_image,rover3,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover3,general,objective2,high_res,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_image,rover3,objective2,high_res))
  :effect (communicated_image_data,objective2,high_res))
 (:action communicate_image_data,rover3,general,objective2,low_res,waypoint1,waypoint0
  :precondition (and (at,rover3,waypoint1) (have_image,rover3,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover3,general,objective2,low_res,waypoint2,waypoint0
  :precondition (and (at,rover3,waypoint2) (have_image,rover3,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover3,general,objective2,low_res,waypoint3,waypoint0
  :precondition (and (at,rover3,waypoint3) (have_image,rover3,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
 (:action communicate_image_data,rover3,general,objective2,low_res,waypoint4,waypoint0
  :precondition (and (at,rover3,waypoint4) (have_image,rover3,objective2,low_res))
  :effect (communicated_image_data,objective2,low_res))
;; action partitions
)