(define (problem gripper-x-1)
   (:domain gripper-typed)
   (:objects rooma roomb - room
             ball4 ball3 ball2 ball1 - ball
             left right - gripper)
   (:init (at-robby rooma)
          (free left)
          (free right)
          (at ball4 rooma)
          (at ball3 rooma)
          (at ball2 rooma)
          (at ball1 rooma))
   (:goal (forall (?b - ball) (at ?b roomb))))
