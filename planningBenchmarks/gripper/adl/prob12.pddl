(define (problem gripper-x-12)
   (:domain gripper-typed)
   (:objects rooma roomb - room
             ball26 ball25 ball24 ball23 ball22 ball21 ball20 ball19 ball18
             ball17 ball16 ball15 ball14 ball13 ball12 ball11 ball10 ball9
             ball8 ball7 ball6 ball5 ball4 ball3 ball2 ball1 - ball
             left right - gripper)
   (:init (at-robby rooma)
          (free left)
          (free right)
          (at ball26 rooma)
          (at ball25 rooma)
          (at ball24 rooma)
          (at ball23 rooma)
          (at ball22 rooma)
          (at ball21 rooma)
          (at ball20 rooma)
          (at ball19 rooma)
          (at ball18 rooma)
          (at ball17 rooma)
          (at ball16 rooma)
          (at ball15 rooma)
          (at ball14 rooma)
          (at ball13 rooma)
          (at ball12 rooma)
          (at ball11 rooma)
          (at ball10 rooma)
          (at ball9 rooma)
          (at ball8 rooma)
          (at ball7 rooma)
          (at ball6 rooma)
          (at ball5 rooma)
          (at ball4 rooma)
          (at ball3 rooma)
          (at ball2 rooma)
          (at ball1 rooma))
   (:goal (forall (?b - ball) (at ?b roomb))))
